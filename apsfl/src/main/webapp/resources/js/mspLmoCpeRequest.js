$(document).ready(function() {
	
	$('#lmoMspTabId').dataTable();
	
	$('#subWiseTable').dataTable();
	
	$('#msoDetailsWithAlignedLMOsTable').dataTable(
			{
				"scrollX":true,
				"drawCallback": function(settings, json) { $('.dataTables_scrollBody thead tr').css({visibility:'collapse'}); }
			});
	
	$('#lmoStockCountTable').dataTable();
	
	$('#msoWiseCpeStockCountTable').dataTable();
	
	$('#tenantWiseCpeStockTable').dataTable();
	
	$('#mspLmoTableId').dataTable({
		/*"paging":   false,*/
		"pageLength": 100,
        "ordering": false,
        "info":     false,
        /*"searching" : false*/
     });
	
	$(document).on("click","#addNewLmoId",function(event){
		var x = $('#sizeOfListId').val();
		var html = '<tr> <td hidden="hidden">'
			+'<input type="text"  name = "lmoCpeRequest['+x+'].msoLmoId" ></td>'
			+'<td><input  type="text"  class="form-control form-white lmoName" required="required" style="width : 100%" maxlength="250" ></td>'
			+'<td><input  type="text" value="0"  class="form-control form-white noEmi"  style="width : 100%" disabled="disabled" ></td>'
			+'<td><input  type="number" value="0" class="form-control form-white noEmi1" required="required" style="width : 100%" >'
			+'<td><input  type="text" value="0"  class="form-control form-white 36Emi"  style="width : 100%" disabled="disabled" ></td>'
			+'<td><input  type="number" value="0" class="form-control form-white 36Emi1" required="required"  style="width : 100%">'
			+'<td><input  type="text" value="0"  class="form-control form-white 48Emi"  style="width : 100%" disabled="disabled" ></td>'
			+'<td><input  type="number" value="0" class="form-control form-white 48Emi1" required="required" style="width : 75%" >'
			+'<a href="#" class="btn btn-sm btn-danger deleteRow" data-toggle="tooltip" data-rel="tooltip" data-original-title="Delete"><i class="icon-trash"></i></a></td>'
			+'</tr>';
		$('#mspLmoTableBodyId').append(html);
		$('#sizeOfListId').val(parseInt(x)+1);
	});
	
	$(document).on('click', '.deleteRow', function(event) {
		$(this).closest('tr').remove();
	 });
	
	$(".numbersOnly_sai").numeric({negative : false}).keyup(function (e) {
		var yourInput = $(this).val();
		re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
		var isSplChar = re.test(yourInput);
		if(isSplChar ) {
			yourInput = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
		}
		
		$(this).val(yourInput);   //update input with new value
	});
	
	
	$(document).on('click', '#lmoMsoButtonId', function(event) {
		var flag = false;
		var i = 0;
		$('#mspLmoTableId > tbody > tr').each(function() {
			flag = true;
			var msoLmoId = $(this).find('.msoLmoId').val();
			var lmoName = $(this).find('.lmoName').val();
			var noEmi = parseInt($(this).find('.noEmi').val());
			var noEmi1 = parseInt($(this).find('.noEmi1').val());
			var Emi36 = parseInt($(this).find('.36Emi').val());
			var Emi36_1 = parseInt($(this).find('.36Emi1').val());
			var Emi48 = parseInt($(this).find('.48Emi').val());
			var Emi48_1 = parseInt($(this).find('.48Emi1').val());
			
			if(lmoName != undefined){
				if(lmoName == ""){
					alert("LMO Name Required");
					flag = false;
					return false; 
				}else if($(this).find('.noEmi1').val().length > 5){
					alert(lmoName +"  UpFront CPEs should be less than 99999");
					flag = false;
					return false;
				}else if($(this).find('.36Emi1').val().length > 5){
					alert(lmoName +" CPE-36 should be less than 99999");
					flag = false;
					return false;
				}else if($(this).find('.48Emi1').val().length > 5){
					alert(lmoName +"  CPE-48 should be less than 99999");
					flag = false;
					return false;
				}else if(noEmi + noEmi1 < 0){
					alert(lmoName +"  4000 CPE Request Are Invalid");
					flag = false;
					return false; 
				}else if(Emi36 + Emi36_1 < 0){
					alert(lmoName +"  1700 CPE Request Are Invalid");
					flag = false;
					return false; 
				}else if(Emi48 + Emi48_1 < 0){
					alert(lmoName +"  500 CPE Request Are Invalid");
					flag = false;
					return false; 
				}
				 if(lmoName != null && lmoName != "") {
					 if(noEmi1 != 0 || Emi36_1 != 0 || Emi48_1 != 0 || msoLmoId == undefined){
						 $('#lmoMsoCpeFormId').append($('<input>').attr('type', 'hidden').attr('name', 'lmoCpeRequest['+i+'].msoLmoId').val(msoLmoId));
						 $('#lmoMsoCpeFormId').append($('<input>').attr('type', 'hidden').attr('name', 'lmoCpeRequest['+i+'].lmoName').val(lmoName));
						 $('#lmoMsoCpeFormId').append($('<input>').attr('type', 'hidden').attr('name', 'lmoCpeRequest['+i+'].noEmiDemandQty').val(noEmi1));
						 $('#lmoMsoCpeFormId').append($('<input>').attr('type', 'hidden').attr('name', 'lmoCpeRequest['+i+'].emi36DemandQty').val(Emi36_1));
						 $('#lmoMsoCpeFormId').append($('<input>').attr('type', 'hidden').attr('name', 'lmoCpeRequest['+i+'].emi48DemandQty').val(Emi48_1));
						  i++;
					 }
				 }
			}
		});
		if(flag)
			$('#lmoMsoCpeFormId').submit();
	 });
});

