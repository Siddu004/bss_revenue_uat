<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
$(document).ready(function() {
	$("#ponWithNoCafTable").dataTable({
		"bProcessing" : false,
		"bServerSide" : true,
		"sort" : "position",
		"sAjaxSource" : "ponWithNoCafPagination",
		"aoColumnDefs" : [
		                  {"sWidth": "10%" , "aTargets": [ 0 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 1 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 2 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 3 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 4 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 5 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 6 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 7 ] },
		            	],
		"aoColumns" : [
					{"mData" : "pop_name"},
					{"mData" : "lmocode"},
				    {"mData" : "portno" },
				    {"mData" : "tenantname" },
					{"mData" : "districtname"},
					{"mData" : "mandalname"},
					{"mData" : "villagename"},
			        {"mData" : "regoff_pocmob1"},
				]
			});
		});
</script>
<body>
	<section>
		BEGIN MAIN CONTENT
		<div class="main-content">
			BEGIN PAGE CONTENT
			<div class="page-content page-width">
				<div class="page-title">
					<h2>PON With Zero Caf Report</h2>
					<div class="breadcrumb-wrapper">
						<ol class="breadcrumb">
							<li><a href="./">Home</a></li>
						</ol>
					</div>
				</div>
				<div class="row main-row">
					<div class="col-lg-12">
						<div class="panel main-panel">
							<div class="panel-content main-panel-content">
								<div class="row" align="center">
									<div class="col-sm-12">
										<div class="row main-row">
											<div class="col-lg-12">
												<div class="panel main-panel">
													<div class="panel-content main-panel-content">
														<div class="row">

															<table class="table table-alt" id="ponWithNoCafTable">
																<thead>
																	<tr>
																		<th>POP NAME</th>	
																		<th>LMO CODE</th>	
																		<th>Port No</th>	
																		<th>Tenant Name</th>	
																		<th>District Name</th>
																		<th>Mandal Name</th>	
																		<th>Village Name</th>	
																		<th>Contact Person Mobile No</th>	
																	</tr>
																</thead>
															</table>
														<a href="./downloadPonWithNoCafReport"><INPUT TYPE="SUBMIT" VALUE="Download" class="btn btn-success"></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>