<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script src="./resources/js/mspLmoCpeRequest.js"></script>
<body>
	<section>
		<!-- BEGIN MAIN CONTENT -->
		<div class="main-content">
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content page-width">
				<div class="page-title">
					<h2>Substation and OLT Wise CAF Report</h2>

					<div class="breadcrumb-wrapper">
						<ol class="breadcrumb">
							<li><a href="./">Home</a></li>
						</ol>
					</div>
				</div>
				<div class="row main-row">
					<div class="col-lg-12">
						<div class="panel main-panel">
							<div class="panel-content main-panel-content">
								<div class="row" align="center">
									<div class="col-sm-12">
										<div class="row main-row">
											<div class="col-lg-12">
												<div class="panel main-panel">
													<div class="panel-content main-panel-content">
														<div class="row">

															<table class="table table-alt" id="subWiseTable">
																<thead>
																	<tr>
																		<th rowspan="2" >SNo</th>
																		<th rowspan="2" style="text-align:center;">POP Name & POP OLT</th>
																		<th rowspan="2" >District Name</th>
																		<th rowspan="2" >Mandal Name</th>
																		<th colspan="8" style="text-align:center;">PORT NO</th>
																		<th rowspan="2">Total</th>
																	</tr>
																	<tr>
																		<th>1</th>
																		<th>2</th>
																		<th>3</th>
																		<th>4</th>
																		<th>5</th>
																		<th>6</th>
																		<th>7</th>
																		<th>8</th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${list}" var="subCafList" varStatus="theCount">
																		<tr>
																			<td><c:out value="${theCount.count}"></c:out></td> 
																			<td><c:out value="${subCafList.popNameWithIP}"></c:out></td>
																			<td><c:out value="${subCafList.districtname}"></c:out></td>
																			<td><c:out value="${subCafList.mandalname}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount1}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount2}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount3}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount4}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount5}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount6}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount7}"></c:out></td>
																			<td><c:out value="${subCafList.cafCount8}"></c:out></td>
																			<td><c:out value="${subCafList.total}"></c:out></td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														<a href="./downloadSubWiseReport">	<INPUT TYPE="SUBMIT" VALUE="Download" class="btn btn-success"></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>