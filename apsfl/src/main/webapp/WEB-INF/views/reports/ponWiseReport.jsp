<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
$(document).ready(function() {
	$("#ponReportTable").dataTable({
		"bProcessing" : false,
		"bServerSide" : true,
		"sort" : "position",
		"sAjaxSource" : "ponWiseReportPagination",
		"aoColumnDefs" : [
		                  {"sWidth": "10%" , "aTargets": [ 0 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 1 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 2 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 3 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 4 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 5 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 6 ] }, 
		                  {"sWidth": "10%" , "aTargets": [ 7 ] },
		                  {"sWidth": "10%" , "aTargets": [ 8 ] },
		                  {"sWidth": "10%" , "aTargets": [ 9 ] },
		            	],
		"aoColumns" : [
					{"mData" : "pop_name"},
					{"mData" : "pop_olt_ipaddress"},
				    {"mData" : "portno" },
				    {"mData" : "lmocode" },
					{"mData" : "regoff_pocname"},
					{"mData" : "regoff_pocmob1"},
					{"mData" : "cafno"},
			        {"mData" : "districtname"},
					{"mData" : "mandalname"},
					{"mData" : "createdon"}
				]
			});
		});
</script>
<body>
	<section>
		BEGIN MAIN CONTENT
		<div class="main-content">
			BEGIN PAGE CONTENT
			<div class="page-content page-width">
				<div class="page-title">
					<h2>PON Wise CAF Count Report</h2>
					<div class="breadcrumb-wrapper">
						<ol class="breadcrumb">
							<li><a href="./">Home</a></li>
						</ol>
					</div>
				</div>
				<div class="row main-row">
					<div class="col-lg-12">
						<div class="panel main-panel">
							<div class="panel-content main-panel-content">
								<div class="row" align="center">
									<div class="col-sm-12">
										<div class="row main-row">
											<div class="col-lg-12">
												<div class="panel main-panel">
													<div class="panel-content main-panel-content">
														<div class="row">

															<table class="table table-alt" id="ponReportTable">
																<thead>
																	<tr>
																		<th>POP_NAME</th>	
																		<th>POP OLT</th>	
																		<th>Port No</th>	
																		<th>LMO Code</th>	
																		<th>Contact Person Name</th>	
																		<th>Contact Person Mobile</th>	
																		<th>CAF Count</th>	
																		<th>District Name</th>	
																		<th>Mandal Name</th>	
																		<th>Created ON</th>
																	</tr>
																</thead>
															</table>
														<a href="./downloadPonWiseReport"><INPUT TYPE="SUBMIT" VALUE="Download" class="btn btn-success"></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>