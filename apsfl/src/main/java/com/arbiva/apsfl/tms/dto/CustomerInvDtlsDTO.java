package com.arbiva.apsfl.tms.dto;

public class CustomerInvDtlsDTO {
	private String invno;
	private String custinvno;
	private String acctcafno; 
	private String pmntcustid;
	private String prodcode;
	private String chargecode;
	private String chargeamt;
	private String srvctax;
	private String invfromdate;
	private String invtodate;
	private String lmocode;
	private String msocode;
	private String invyr;
	private String invmn;
	private String enttype;	
//	private String custtype;
	private String apsflmonthlyshare;
	private String lmomonthlyshare;
	private String msomonthlyshare;
	private String apsflInvshare;
	private String lmoInvshare;
	public String getApsflInvshare() {
		return apsflInvshare;
	}
	public void setApsflInvshare(String apsflInvshare) {
		this.apsflInvshare = apsflInvshare;
	}
	public String getLmoInvshare() {
		return lmoInvshare;
	}
	public void setLmoInvshare(String lmoInvshare) {
		this.lmoInvshare = lmoInvshare;
	}
	public String getMsoInvshare() {
		return msoInvshare;
	}
	public void setMsoInvshare(String msoInvshare) {
		this.msoInvshare = msoInvshare;
	}
	public String getTotalcollected() {
		return totalcollected;
	}
	public void setTotalcollected(String totalcollected) {
		this.totalcollected = totalcollected;
	}
	private String msoInvshare;
	private String totalcollected;
	
	

	
	public String getApsflmonthlyshare() {
		return apsflmonthlyshare;
	}
	public void setApsflmonthlyshare(String apsflmonthlyshare) {
		this.apsflmonthlyshare = apsflmonthlyshare;
	}
	public String getLmomonthlyshare() {
		return lmomonthlyshare;
	}
	public void setLmomonthlyshare(String lmomonthlyshare) {
		this.lmomonthlyshare = lmomonthlyshare;
	}
	public String getMsomonthlyshare() {
		return msomonthlyshare;
	}
	public void setMsomonthlyshare(String msomonthlyshare) {
		this.msomonthlyshare = msomonthlyshare;
	}
	
	
	public String getEnttype() {
		return enttype;
	}
	public void setEnttype(String enttype) {
		this.enttype = enttype;
	}
	public String getInvyr() {
		return invyr;
	}
	public void setInvyr(String invyr) {
		this.invyr = invyr;
	}
	public String getInvmn() {
		return invmn;
	}
	public void setInvmn(String invmn) {
		this.invmn = invmn;
	}
	public String getLmocode() {
		return lmocode;
	}
	public void setLmocode(String lmocode) {
		this.lmocode = lmocode;
	}
	public String getMsocode() {
		return msocode;
	}
	public void setMsocode(String msocode) {
		this.msocode = msocode;
	}
	public String getInvfromdate() {
		return invfromdate;
	}
	public void setInvfromdate(String invfromdate) {
		this.invfromdate = invfromdate;
	}
	public String getInvtodate() {
		return invtodate;
	}
	public void setInvtodate(String invtodate) {
		this.invtodate = invtodate;
	}
	public String getInvno() {
		return invno;
	}
	public void setInvno(String invno) {
		this.invno = invno;
	}
	public String getCustinvno() {
		return custinvno;
	}
	public void setCustinvno(String custinvno) {
		this.custinvno = custinvno;
	}
	public String getAcctcafno() {
		return acctcafno;
	}
	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}
	public String getPmntcustid() {
		return pmntcustid;
	}
	public void setPmntcustid(String pmntcustid) {
		this.pmntcustid = pmntcustid;
	}
	public String getProdcode() {
		return prodcode;
	}
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}
	public String getChargecode() {
		return chargecode;
	}
	public void setChargecode(String chargecode) {
		this.chargecode = chargecode;
	}
	public String getChargeamt() {
		return chargeamt;
	}
	public void setChargeamt(String chargeamt) {
		this.chargeamt = chargeamt;
	}
	public String getSrvctax() {
		return srvctax;
	}
	public void setSrvctax(String srvctax) {
		this.srvctax = srvctax;
	}
	
}
