package com.arbiva.apsfl.tms.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.bo.CafWiseReportBO;
import com.arbiva.apfgc.tenant.bo.CafWiseRevenueOfLoginLmoBo;
import com.arbiva.apfgc.tenant.bo.DistrictWiseCafBO;
import com.arbiva.apfgc.tenant.bo.DistrictWiseCpeBO;
import com.arbiva.apfgc.tenant.bo.EnterpriseSubscriberBO;
import com.arbiva.apfgc.tenant.bo.TenantStockReportBO;
import com.arbiva.apfgc.tenant.bo.LmoStockCountBO;
import com.arbiva.apfgc.tenant.bo.MsoCafNotCpeStockBo;
import com.arbiva.apfgc.tenant.bo.MsoDetailsWithLmosBO;
import com.arbiva.apfgc.tenant.bo.MsoRevenueShareBO;
import com.arbiva.apfgc.tenant.bo.MsoWiseCpeBo;
import com.arbiva.apfgc.tenant.bo.PONWiseBo;
import com.arbiva.apfgc.tenant.bo.PONWithZeroCAFBO;
import com.arbiva.apfgc.tenant.bo.SubstationWiseCafBo;
import com.arbiva.apsfl.coms.dto.ComsHelperDTO;
import com.arbiva.apsfl.coms.dto.PageObject;
import com.arbiva.apsfl.tms.model.EmailMaster;
import com.arbiva.apsfl.tms.model.Tenant;
import com.arbiva.apsfl.util.CafEnumCodes;

@Repository
public class DemandNoteDaoImpl {
	
	private static final Logger logger = Logger.getLogger(DemandNoteDaoImpl.class);

	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	/* DAO Implementation: To get List of Mso by using tenant code */
	@SuppressWarnings("unchecked")
	public List<Object[]> getMsoList() {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select pml.mso_enrollment_number,t.tenantname,t.locoff_pocmob1, sum(pml.noemidemandqty), sum(pml.emi36demandqty), sum(pml.emi48demandqty),t.regoff_pocname , d.districtname, m.mandalname from portal_msp_lmos pml, tenants t, districts d , mandals m where d.districtuid = t.portal_districtid  and m.districtuid = t.portal_districtid  and m.mandalslno = t.portal_mandalid and pml.mso_enrollment_number = t.tenantcode group by mso_enrollment_number");
		List<Object[]> listMso = new ArrayList<>();
		try {
			listMso = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMso;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getLmoList(String tenantCode) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		queryString.append("select  lmo_name, noemidemandqty, emi36demandqty, emi48demandqty, lmo_mandal_name, lmo_district_name from portal_msp_lmos where mso_enrollment_number = :tenantCode ");
		List<Object[]> listMso = new ArrayList<>();
		try {
			query = getEntityManager().createNativeQuery(queryString.toString());
			query.setParameter("tenantCode", tenantCode);
			listMso = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMso;
	}

	@SuppressWarnings("unchecked")
	public List<MsoWiseCpeBo> getMsoWiseDemand() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<MsoWiseCpeBo> listMso = new ArrayList<>();
		
		queryString.append(" select a.mspcode,date_format(b.maxdate,'%d-%b-%Y') maxdate, a.mspname, a.districtname");
		queryString.append(" ,SUM(CASE WHEN a.dmddate=b.maxdate THEN qty0  ELSE 0 END) dqty0 ");
		queryString.append(" ,SUM(CASE WHEN a.dmddate=b.maxdate THEN qty36 ELSE 0 END) dqty36 ");
		queryString.append(" ,SUM(CASE WHEN a.dmddate=b.maxdate THEN qty48 ELSE 0 END) dqty48");
		queryString.append(" ,SUM(qty0) cqty0");
		queryString.append(" ,SUM(qty36) cqty36");
		queryString.append(" ,SUM(qty48) cqty48");
		queryString.append(" FROM (select mspcode,concat(tenantname,' (', ifnull(locoff_pocname,''),')' ) mspname,districtname,date(dmdtime) dmddate,noemidemandqty qty0,emi36demandqty qty36,emi48demandqty qty48  ");
		queryString.append("        from mspcpedmd m1, mspcpedmddtl m2, tenants t, districts d where m1.dmdid = m2.dmdid and m1.mspcode = t.tenantcode and t.portal_districtid = d.districtuid and t.tenantcode <> 'MSO99158' ");
		queryString.append("      ) a");
		queryString.append(" 	,(select m3.mspcode,MAX(DATE(m3.dmdtime)) maxdate from mspcpedmd m3, mspcpedmddtl m4, tenants t1 where m3.dmdid = m4.dmdid and m3.mspcode = t1.tenantcode  group by mspcode) b");
		queryString.append(" where a.mspcode=b.mspcode ");
		queryString.append(" group by a.mspcode,b.maxdate, a.mspname, a.districtname order by b.maxdate desc");
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),MsoWiseCpeBo.class);
			listMso = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMso;
	}
	
	@SuppressWarnings("unchecked")
	public List<DistrictWiseCpeBO> getDistrictWiseDemand() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<DistrictWiseCpeBO> listMso = new ArrayList<>();
		
		/*
		 * 
	SELECT  d.districtname
	     ,SUM(CASE WHEN m1.dmdtime       < ADDDATE(CURRENT_DATE(),-1) THEN m2.noemidemandqty  ELSE 0 END) d2qty0 
		 ,SUM(CASE WHEN m1.dmdtime       < ADDDATE(CURRENT_DATE(),-1) THEN m2.emi36demandqty  ELSE 0 END) d2qty36 
		 ,SUM(CASE WHEN m1.dmdtime       < ADDDATE(CURRENT_DATE(),-1) THEN m2.emi48demandqty  ELSE 0 END) d2qty48
	     ,SUM(CASE WHEN DATE(m1.dmdtime) = ADDDATE(CURRENT_DATE(),-1) THEN m2.noemidemandqty  ELSE 0 END) d1qty0 
		 ,SUM(CASE WHEN DATE(m1.dmdtime) = ADDDATE(CURRENT_DATE(),-1) THEN m2.emi36demandqty  ELSE 0 END) d1qty36 
		 ,SUM(CASE WHEN DATE(m1.dmdtime) = ADDDATE(CURRENT_DATE(),-1) THEN m2.emi48demandqty  ELSE 0 END) d1qty48
  		FROM mspcpedmd m1, mspcpedmddtl m2, tenants t, districts d 
 	 WHERE m1.dmdid = m2.dmdid and m1.mspcode = t.tenantcode and t.portal_districtid = d.districtuid AND t.tenantcode <> 'MSO99158'
 	GROUP BY d.districtname;
		 */
		
		queryString.append(" SELECT  d.districtname ");
		queryString.append(" ,SUM(CASE WHEN m1.dmdtime       < CURRENT_DATE() THEN m2.noemidemandqty  ELSE 0 END) d2qty0  ");
		queryString.append(" ,SUM(CASE WHEN m1.dmdtime       < CURRENT_DATE() THEN m2.emi36demandqty  ELSE 0 END) d2qty36 ");
		queryString.append(" ,SUM(CASE WHEN m1.dmdtime       < CURRENT_DATE() THEN m2.emi48demandqty  ELSE 0 END) d2qty48 ");
		queryString.append(" ,SUM(CASE WHEN DATE(m1.dmdtime) = CURRENT_DATE() THEN m2.noemidemandqty  ELSE 0 END) d1qty0  ");
		queryString.append(" ,SUM(CASE WHEN DATE(m1.dmdtime) = CURRENT_DATE() THEN m2.emi36demandqty  ELSE 0 END) d1qty36  ");
		queryString.append(" ,SUM(CASE WHEN DATE(m1.dmdtime) = CURRENT_DATE() THEN m2.emi48demandqty  ELSE 0 END) d1qty48 ");
		queryString.append(" FROM mspcpedmd m1, mspcpedmddtl m2, tenants t, districts d  ");
		queryString.append(" WHERE m1.dmdid = m2.dmdid and m1.mspcode = t.tenantcode and t.portal_districtid = d.districtuid AND t.tenantcode <> 'MSO99158'");
		queryString.append(" GROUP BY d.districtname");
		
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),DistrictWiseCpeBO.class);
			listMso = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMso;
	}

	@SuppressWarnings("unchecked")
	public List<CafWiseReportBO> getCafWiseDemand() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<CafWiseReportBO> list = new ArrayList<>();
		
		/*SELECT date_format(m.maxdate,'%d-%b-%Y') maxdate,a.districtname,a.villagename,a.lmoname
	     ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) hhday
	     ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL'                         THEN 1 ELSE 0 END) hhcum
	     ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) pvtday
	     ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'    AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) govtday
	     ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE'                         THEN 1 ELSE 0 END) pvtcum
	     ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'                            THEN 1 ELSE 0 END) govtcum
	 FROM (SELECT cf.cafdate,cf.inst_district,d.districtname,cf.inst_mandal,cf.inst_city_village, v.villagename,cf.lmocode,cf.custtypelov
	             ,CASE WHEN cf.custtypelov='ENTERPRISE' THEN (SELECT ec.enttypelov from entcustomers ec where ec.custid=cf.custid) ELSE '' END enttype
	             ,CONCAT(IFNULL(t.tenantname,''),'(',IFNULL(t.locoff_pocname,IFNULL(t.regoff_pocname,'')),')') lmoname
	         FROM cafs cf,districts d,villages v,tenants t
	        WHERE cf.inst_district = d.districtuid AND cf.inst_district=v.districtuid AND cf.inst_mandal=v.mandalslno AND cf.inst_city_village=v.villageslno
	          AND cf.lmocode = t.tenantcode
	         ) a
	     ,(SELECT inst_district,inst_mandal,inst_city_village,lmocode,MAX(cafdate) maxdate FROM cafs 
	        GROUP BY inst_district,inst_mandal,inst_city_village,lmocode) m
	WHERE a.lmocode = m.lmocode AND a.inst_district = m.inst_district AND a.inst_city_village=m.inst_city_village
	GROUP BY    m.maxdate,a.districtname,a.villagename,a.lmoname
	order by m.maxdate desc*/
		
/*		queryString.append("  SELECT date_format(m.maxdate,'%d-%b-%Y') maxdate,d.districtname,v.villagename, concat(t.tenantname, ' (', ifnull(t.regoff_pocname,''),')') tname");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) hhday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL'                         THEN 1 ELSE 0 END) hhcum");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) pvtday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'    AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) govtday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE'                         THEN 1 ELSE 0 END) pvtcum");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'                            THEN 1 ELSE 0 END) govtcum");
		queryString.append(" FROM (select cafdate,inst_district,inst_mandal,inst_city_village,lmocode,custtypelov,CASE WHEN custtypelov='ENTERPRISE' THEN (SELECT enttypelov from entcustomers ec where ec.custid=cf.custid) ELSE '' END enttype");
		queryString.append(" from cafs cf) a");		
		queryString.append(" ,(select lmocode,MAX(cafdate) maxdate FROM cafs GROUP bY lmocode) m");
		queryString.append(" ,districts d,villages v,tenants t");
		queryString.append(" WHERE a.inst_district = d.districtuid AND a.inst_district=v.districtuid AND a.inst_mandal=v.mandalslno AND a.inst_city_village=v.villageslno");
		queryString.append("  AND a.lmocode = m.lmocode AND a.lmocode = t.tenantcode");
		queryString.append(" GROUP BY    m.maxdate,d.districtname,v.villagename,tname  order by m.maxdate desc");*/
		
		queryString.append(" SELECT date_format(m.maxdate,'%d-%b-%Y') maxdate,a.districtname,a.villagename,a.lmoname tname");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) hhday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='INDIVIDUAL'                         THEN 1 ELSE 0 END) hhcum");
		queryString.append("  ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE' AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) pvtday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'    AND a.cafdate=m.maxdate THEN 1 ELSE 0 END) govtday");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='PRIVATE'                         THEN 1 ELSE 0 END) pvtcum");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='ENTERPRISE' AND a.enttype='GOVT'                            THEN 1 ELSE 0 END) govtcum");
		queryString.append("  FROM (SELECT cf.cafdate,cf.inst_district,d.districtname,cf.inst_mandal,cf.inst_city_village, v.villagename,cf.lmocode,cf.custtypelov");
		queryString.append("    ,CASE WHEN cf.custtypelov='ENTERPRISE' THEN (SELECT ec.enttypelov from entcustomers ec where ec.custid=cf.custid) ELSE '' END enttype");
		queryString.append("   ,CONCAT(IFNULL(t.tenantname,''),'(',IFNULL(t.locoff_pocname,IFNULL(t.regoff_pocname,'')),')') lmoname");
		queryString.append("  FROM cafs cf,districts d,villages v,tenants t, mandals m");
		queryString.append("   WHERE cf.inst_district = d.districtuid AND cf.inst_district=v.districtuid AND cf.inst_mandal=v.mandalslno AND cf.inst_city_village=v.villageslno");
		queryString.append("     AND cf.lmocode = t.tenantcode AND cf.status = 6 AND m.districtuid = d.districtuid AND m.mandalslno = cf.inst_mandal AND v.mandalslno = m.mandalslno ");
		queryString.append("    ) a");
		queryString.append(" ,(SELECT inst_district,inst_mandal,inst_city_village,lmocode,MAX(cafdate) maxdate FROM cafs WHERE status = 6  ");
		queryString.append("  GROUP BY inst_district,inst_mandal,inst_city_village,lmocode) m");
		queryString.append(" WHERE a.lmocode = m.lmocode AND a.inst_district = m.inst_district and a.inst_mandal = m.inst_mandal AND a.inst_city_village=m.inst_city_village");
		queryString.append(" GROUP BY    m.maxdate,a.districtname,a.villagename,a.lmoname");
		queryString.append(" order by m.maxdate desc");
		
		logger.info("Caf Query ===== "+ queryString.toString());
		
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),CafWiseReportBO.class);
			list = query.getResultList();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<EmailMaster> getEamilOfCafWiseReport(String val) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		queryString.append("select * from emailrptmst where rptname = :val ");
		List<EmailMaster> listMso = new ArrayList<>();
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(), EmailMaster.class);
			query.setParameter("val", val);
			listMso = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listMso;
	}

	public <T> T save(T t) {
		return getEntityManager().merge(t);
	}

	@SuppressWarnings("unchecked")
	public List<DistrictWiseCafBO> getDistrictWiseCafList() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<DistrictWiseCafBO> districtWiseCafList = new ArrayList<>();
		
		queryString.append(" SELECT a.districtname ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.CUST_TYPE_CODE.getCode()+"' AND a.actdate < CURRENT_DATE() THEN 1 ELSE 0 END) hhuptoday  ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.CUST_TYPE_CODE.getCode()+"' AND a.actdate = CURRENT_DATE() THEN 1 ELSE 0 END) hhonday ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' AND a.enttype='"+CafEnumCodes.PRIVATE_TYPE_CODE.getCode()+"' AND a.actdate < CURRENT_DATE() THEN 1 ELSE 0 END) pvtuptoday ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' AND a.enttype='"+CafEnumCodes.GOVT_TYPE_CODE.getCode()+"'    AND a.actdate < CURRENT_DATE() THEN 1 ELSE 0 END) govtuptoday  ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' AND a.enttype='"+CafEnumCodes.PRIVATE_TYPE_CODE.getCode()+"' AND a.actdate = CURRENT_DATE() THEN 1 ELSE 0 END) pvtonday  ");
		queryString.append(" ,SUM(CASE WHEN a.custtypelov='"+CafEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' AND a.enttype='"+CafEnumCodes.GOVT_TYPE_CODE.getCode()+"'    AND a.actdate = CURRENT_DATE() THEN 1 ELSE 0 END) govtonday ");
		queryString.append(" FROM (SELECT cf.actdate,d.districtname,cf.custtypelov  ");
		queryString.append(" ,CASE WHEN cf.custtypelov='"+CafEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' THEN (SELECT ec.enttypelov from entcustomers ec where ec.custid=cf.custid) ELSE '' END enttype ");
		queryString.append(" FROM cafs cf,districts d ");
		queryString.append(" WHERE cf.inst_district = d.districtuid and cf.status in (6)) a");
				//+ "not in ("+CafEnumCodes.CAF_BULK_UPLOAD_STATUS.getStatus()+", "+CafEnumCodes.CAF_BULK_UPLOAD_PENDING_STATUS.getStatus()+", "+CafEnumCodes.CAF_PENDING_STATUS.getStatus()+") ) a ");
		queryString.append(" GROUP BY a.districtname ORDER BY 1 ");
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),DistrictWiseCafBO.class);
			districtWiseCafList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return districtWiseCafList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EnterpriseSubscriberBO> getEnterpriseSubscriberList(PageObject pageObject) {
		List<EnterpriseSubscriberBO> enterpriseSubscriberList = new ArrayList<>();
		StringBuilder queryString = new StringBuilder();
		String finalQuery = null;
		String searchParameter = "";
		Query query = null;
		long count = 0l;
		try{
			queryString.append(" SELECT cf.custid, c.custname, ");
			queryString.append(" IFNULL((SELECT prnt.custname FROM customermst prnt WHERE prnt.custid=c.parentcustcode),c.custname) parentcustname, ");
			queryString.append(" COUNT(1) totalcafs, cf.lmocode, SUM(CASE WHEN cf.status = 6 THEN 1 ELSE 0 END) activecount, ");
			queryString.append(" SUM(CASE WHEN cf.status = 7 THEN 1 ELSE 0 END) suspendcount, ");
			queryString.append(" SUM(CASE WHEN cf.status = 8 THEN 1 ELSE 0 END) inactivecount, ");
			queryString.append(" SUM(CASE WHEN cf.status IN(88) THEN 1 ELSE 0 END) pendingforcafeditcount, ");
			queryString.append(" SUM(CASE WHEN cf.status IN(2) THEN 1 ELSE 0 END) pendingforprovisioncount, ");
			queryString.append(" SUM(CASE WHEN cf.status IN(89) THEN 1 ELSE 0 END) pendingforpaymentcount , t.tenantname ");
			queryString.append(" FROM cafs cf,customermst c , tenants t ");
			queryString.append(" WHERE cf.custid = c.custid AND cf.custtypelov='ENTERPRISE' AND c.enttypelov = 'GOVT' AND cf.lmocode=t.tenantcode ");
			
			StringBuilder whereClause = new StringBuilder("");
			StringBuilder orderByClause = new StringBuilder("");
			StringBuilder groupByClause = new StringBuilder("");
			if (pageObject != null) {
				searchParameter = pageObject.getSearchParameter();
					if(searchParameter != null && !searchParameter.isEmpty()){
						whereClause.append("and (custname like '%" + searchParameter + "%' or cf.lmocode like '%"+ searchParameter + "%' ");
						whereClause.append("or t.tenantname like '%" + searchParameter + "%' ) ");
					}
				orderByClause.append(" ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder());
				groupByClause.append(" GROUP BY cf.custid, c.custname, cf.lmocode ");
			    finalQuery = queryString.append(whereClause).append(groupByClause).append(orderByClause).toString();
				
				query = getEntityManager().createNativeQuery(finalQuery.toString(),EnterpriseSubscriberBO.class);
				count = query.getResultList().size();
				pageObject.setTotalDisplayCount(String.valueOf(count));
				
				query = getEntityManager().createNativeQuery(finalQuery.toString(),EnterpriseSubscriberBO.class);
				query.setFirstResult(pageObject.getMinSize());
				query.setMaxResults(pageObject.getMaxSize());
			}
			else{
				groupByClause.append(" GROUP BY cf.custid, c.custname, cf.lmocode ");
			    finalQuery = queryString.append(groupByClause).toString();
				query = getEntityManager().createNativeQuery(finalQuery.toString(),EnterpriseSubscriberBO.class);
			}
			enterpriseSubscriberList = query.getResultList();
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			query = null;
			queryString = null;
			query = null;
		}
		
		return enterpriseSubscriberList;
	}

	@SuppressWarnings("rawtypes")
	public String getBalanceStatus(int size, String profileId, String tenantCode) {
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String returnVal = null;
		try {
			logger.info("CpeDaoImpl :: updateCpeBlockedAmount() :: SATRT");
			 
			builder.append(" SELECT CASE WHEN   ((a.chramt <=0) OR (b.wallamt >= a.chramt)) THEN 'true' ELSE 'false' END FROM "
					+ " (SELECT (upfrontcharges * "+size+") chramt FROM cpecharges WHERE profile_id = "+profileId+" AND CURRENT_DATE BETWEEN effectivefrom AND effectiveto)a,"
					+ " (SELECT ( crlimitamt + walletamt + cperelamt - usedamt - cpeblkamt) AS wallamt FROM tenantswallet WHERE tenantcode = '"+tenantCode+"') b");
		 
			query = getEntityManager().createNativeQuery(builder.toString());
			
			List l = query.getResultList();
			
			returnVal = l.get(0).toString();
			logger.info("CpeDaoImpl :: updateCpeBlockedAmount() :: END");
		} catch (Exception e) {
			logger.error("CpeDaoImpl::updateCpeBlockedAmount() " + e);
		} finally {
			query = null;
			builder = null;
		}
		
		return returnVal;
	}
	
	public Tenant findByTenantId(Integer tenantid) {
		Tenant tenant = new Tenant();
		TypedQuery<Tenant> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName())
				.append(" WHERE tenantid=:tenantid");
		try {
			logger.info("START::findByTenantId()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			query.setParameter("tenantid", tenantid);
			tenant = query.getSingleResult();
			logger.info("END::findByTenantId()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findByTenantId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenant;
	}

	public String getCpeModelInfoBySrlNo(String cpeSrlNo) {
		String val = null;
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT cpe_model FROM cpe_profilemaster cp, cpestock cs WHERE cs.profile_id = cp.profile_id AND cs.cpeslno = '"+cpeSrlNo+"'");
			query = getEntityManager().createNativeQuery(builder.toString());
			val = (String) query.getSingleResult();
		return val;
	}
	
	@SuppressWarnings("unchecked")
	public List<PONWiseBo> getAllotedPONWithCaf(PageObject pageObject) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		String finalQuery = null;
		String searchParameter = "";
		List<PONWiseBo> listPON = new ArrayList<>();
		long count = 0l;
			try {
			queryString.append("SELECT olm.pop_name,olm.pop_olt_ipaddress,olt.portno,olt.lmocode,ts.regoff_pocname,ts.regoff_pocmob1,SUM(CASE WHEN cafno IS NOT NULL THEN 1 ELSE 0 END) cafno,ds.districtname,ms.mandalname,ts.createdon ");
			queryString.append("FROM oltportdtls olt, oltmaster olm ,");
			queryString.append("substations sub,districts ds,mandals ms ,cafs cf,tenants ts ");
			queryString.append("WHERE olt.pop_olt_serialno=olm.pop_olt_serialno ");
			queryString.append("AND olm.pop_substnuid=sub.substnuid ");
			queryString.append("AND sub.districtuid=ds.districtuid ");
			queryString.append("AND sub.mandalslno=ms.mandalslno ");
			queryString.append("AND cf.lmocode=ts.tenantcode ");
			queryString.append("AND ms.districtuid=ds.districtuid ");
			queryString.append("AND cf.lmocode=olt.lmocode ");
			queryString.append("AND olm.pop_olt_ipaddress=cf.oltipaddr ");
			queryString.append("AND olt.portno=cf.olt_portid ");
			
			StringBuilder whereClause = new StringBuilder("");
			StringBuilder orderByClause = new StringBuilder("");
			StringBuilder groupByClause = new StringBuilder("");
			if (pageObject != null) {
				searchParameter = pageObject.getSearchParameter();
					if(searchParameter != null && !searchParameter.isEmpty()){
						whereClause.append("and (olm.pop_name like '%" + searchParameter + "%' or olm.pop_olt_ipaddress like '%"+ searchParameter + "%' ");
						whereClause.append("or ts.regoff_pocname like '%" + searchParameter + "%' or ts.regoff_pocmob1 like '%" + searchParameter + "%' or ms.mandalname like '%" + searchParameter + "%'  ");
						whereClause.append("or olt.lmocode like '%" + searchParameter + "%' or ds.districtname like '%" + searchParameter + "%') ");
						
					}
				orderByClause.append(" ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder());
				groupByClause.append(" GROUP BY olm.pop_name,olm.pop_olt_ipaddress,olt.pop_olt_serialno,olt.portno,olt.lmocode,ds.districtname,ms.mandalname,ts.createdon,ts.regoff_pocname,ts.regoff_pocmob1 ");
			    finalQuery = queryString.append(whereClause).append(groupByClause).append(orderByClause).toString();
				
				query = getEntityManager().createNativeQuery(finalQuery.toString(),PONWiseBo.class);
				count = query.getResultList().size();
				pageObject.setTotalDisplayCount(String.valueOf(count));
				
				query = getEntityManager().createNativeQuery(finalQuery.toString(),PONWiseBo.class);
				query.setFirstResult(pageObject.getMinSize());
				query.setMaxResults(pageObject.getMaxSize());
			}
			else{
				groupByClause.append(" GROUP BY olm.pop_name,olm.pop_olt_ipaddress,olt.pop_olt_serialno,olt.portno,olt.lmocode,ds.districtname,ms.mandalname,ts.createdon,ts.regoff_pocname,ts.regoff_pocmob1 ");
			    finalQuery = queryString.append(groupByClause).toString();
				query = getEntityManager().createNativeQuery(finalQuery.toString(),PONWiseBo.class);
			}
			
			listPON = query.getResultList();
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			query = null;
			queryString = null;
			query = null;
		}
		return listPON;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SubstationWiseCafBo> getSubstationWisePONWithCaf() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<SubstationWiseCafBo> listSubstationWisePONWithCaf = new ArrayList<>();
		
		queryString.append("SELECT olm.pop_name,olm.pop_olt_ipaddress,ds.districtname,ms.mandalname,olt.portno,SUM(CASE WHEN cafno IS NOT NULL THEN 1 ELSE 0 END) cafno ");
		queryString.append("FROM oltportdtls olt, oltmaster olm , ");
		queryString.append("substations sub,districts ds,mandals ms ,cafs cf,tenants ts ");
		queryString.append("WHERE olt.pop_olt_serialno=olm.pop_olt_serialno ");
		queryString.append("AND olm.pop_substnuid=sub.substnuid ");
		queryString.append("AND sub.districtuid=ds.districtuid ");
		queryString.append("AND sub.mandalslno=ms.mandalslno ");
		queryString.append("AND cf.lmocode=ts.tenantcode ");
		queryString.append("AND ms.districtuid=ds.districtuid ");
		queryString.append("AND cf.lmocode=olt.lmocode ");
		queryString.append("AND olm.pop_olt_ipaddress=cf.oltipaddr ");
		queryString.append("AND olt.portno=cf.olt_portid ");
		queryString.append("GROUP BY olm.pop_name,olm.pop_olt_ipaddress,ds.districtname,ms.mandalname,olt.portno ");
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),SubstationWiseCafBo.class);
			listSubstationWisePONWithCaf = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listSubstationWisePONWithCaf;
	}
	
	@SuppressWarnings("unchecked")
	public List<PONWithZeroCAFBO> getPONWithZeroCAFBO(PageObject pageObject) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<PONWithZeroCAFBO> listPONWithZeroCAF = new ArrayList<>();
		String finalQuery = null;
		String searchParameter = "";
		long count = 0l;
		try {
			queryString.append("SELECT olm.pop_name,olt.lmocode,olt.portno,ts.tenantname,m.mandalname,d.districtname,v.villagename,ts.regoff_pocmob1 FROM ");
			queryString.append("oltportdtls olt,oltmaster olm,substations sub,tenants ts,mandals m,districts d,villages v,users u ");
			queryString.append("WHERE   olm.pop_substnuid=sub.substnuid ");
			queryString.append("AND olt.pop_olt_serialno=olm.pop_olt_serialno ");
			queryString.append("AND olt.l3slotsused IS NULL ");
			queryString.append("AND olt.lmocode LIKE 'LMO%' "); 
			queryString.append("AND olt.lmocode=ts.tenantcode ");
			queryString.append("AND u.tenantcode=olt.lmocode ");
			queryString.append("AND ts.portal_districtid=d.districtuid ");
			queryString.append("AND ts.portal_mandalid=m.mandalslno ");
			queryString.append("AND ts.portal_districtid=m.districtuid ");
			queryString.append("AND ts.portal_villageid =v.villageslno "); 
			queryString.append("AND ts.portal_mandalid=v.mandalslno ");
			queryString.append("AND ts.portal_districtid= v.districtuid ");
			
			StringBuilder whereClause = new StringBuilder("");
			StringBuilder orderByClause = new StringBuilder("");
			StringBuilder groupByClause = new StringBuilder("");
			if (pageObject != null) {
				searchParameter = pageObject.getSearchParameter();
					if(searchParameter != null && !searchParameter.isEmpty()){
						whereClause.append("and (olm.pop_name like '%" + searchParameter + "%' or olt.lmocode like '%"+ searchParameter + "%' ");
						whereClause.append("or olt.portno like '%" + searchParameter + "%' or ts.tenantname like '%" + searchParameter + "%'  ");
						whereClause.append("or m.mandalname like '%" + searchParameter + "%' or d.districtname like '%" + searchParameter + "%' ");
						whereClause.append("or v.villagename like '%" + searchParameter + "%' or ts.regoff_pocmob1 like '%" + searchParameter + "%') ");
						
					}
					orderByClause.append(" ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder());
					groupByClause.append(" GROUP BY olm.pop_name,olt.lmocode,olt.portno,ts.tenantname,m.mandalname,d.districtname,v.villagename ");
				    finalQuery = queryString.append(whereClause).append(groupByClause).append(orderByClause).toString();
				    
				    query = getEntityManager().createNativeQuery(finalQuery.toString(),PONWithZeroCAFBO.class);
					count = query.getResultList().size();
					pageObject.setTotalDisplayCount(String.valueOf(count));
					
					query.setFirstResult(pageObject.getMinSize());
					query.setMaxResults(pageObject.getMaxSize());
				}else{
					groupByClause.append(" GROUP BY olm.pop_name,olt.lmocode,olt.portno,ts.tenantname,m.mandalname,d.districtname,v.villagename ");
				    finalQuery = queryString.append(groupByClause).toString();
					query = getEntityManager().createNativeQuery(finalQuery.toString(),PONWithZeroCAFBO.class);
				}
		
				listPONWithZeroCAF = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			query = null;
			queryString = null;
			query = null;
		}
		return listPONWithZeroCAF;
	}
	
	//LMO Wise Stock Count
	@SuppressWarnings("unchecked")
	public List<LmoStockCountBO> getlmoWiseStockCount() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<LmoStockCountBO> lmoWiseStockCount = new ArrayList<>();
		
		queryString.append("SELECT ts.tenantcode,ts.tenantname,ts.regoff_pocname AS LMO_Name,ts.regoff_pocmob1 AS LMO_MobileNO,ds.districtname as districtname,ms.mandalname as Lmo_Mandalname,vs.villagename as Lmo_Villagename, ");
		queryString.append("SUM(CASE WHEN cs.STATUS = 4 THEN 1 ELSE 0 END) CAF_Done_Stock_Count , ");
		queryString.append("SUM(CASE WHEN cs.STATUS = 3  THEN 1 ELSE 0 END) AS CAF_Not_Stock_Count ");  
		queryString.append("FROM CPESTOCK cs ");
		queryString.append("LEFT JOIN tenants ts ON   cs.LMOCODE=ts.tenantcode AND ts.tenanttypelov='LMO' ");
		queryString.append("LEFT JOIN districts ds ON ds.districtuid=ts.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=ts.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND "); 
		queryString.append("ts.portal_villageid=vs.villageslno AND vs.districtuid=ts.portal_districtid AND vs.mandalslno=ts.portal_mandalid ");
		queryString.append("where ts.tenantcode is not null AND cs.lmocode like '%LMO%' ");
		queryString.append("GROUP BY ts.tenantcode,ts.tenantname,ts.regoff_pocname,ts.regoff_pocmob1 ");
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),LmoStockCountBO.class);
			lmoWiseStockCount = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lmoWiseStockCount;
	}
	
	//MSO Wise LMO Details
	@SuppressWarnings("unchecked")
	public List<MsoDetailsWithLmosBO> getMsoWiseLmoDetails() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<MsoDetailsWithLmosBO> listMsoWiseLmo = new ArrayList<>();

		queryString.append("SELECT DISTINCT TE.tenantname AS MSONAME,REPLACE(REPLACE(SUBSTRING_INDEX(r.partnerlist, ',', -1),']',''),' ','') MSO,  ");
		queryString.append("TE.regoff_pocname AS MSO_ContactName,TE.regoff_pocmob1 AS MSO_MobileNo,ds.districtname AS mso_district,ms.mandalname AS mso_mandal,vs.villagename AS mso_villagename,TE.createdon AS mso_createdon,T.tenantname AS LMONAME, ");
		queryString.append("REPLACE(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(r.partnerlist, ',', 2), ',', -1), ']', '' ),' ','') LMO, T.regoff_pocname AS LMO_ContactName, ");
		queryString.append("T.regoff_pocmob1 AS LMO_MobileNo,dt.districtname AS lmo_district,mt.mandalname AS lmo_mandal,vt.villagename AS lmo_villagename, T.createdon AS lmo_createdon ");
		queryString.append("FROM RSAGRMNTS r "); 
		queryString.append("LEFT JOIN tenants T ON T.tenantcode = REPLACE(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(r.partnerlist, ',', 2), ',', -1), ']', '' ),' ','') ");
		queryString.append("LEFT JOIN tenants TE ON TE.tenantcode = REPLACE(REPLACE(SUBSTRING_INDEX(r.partnerlist, ',', -1),']',''),' ','') ");
		queryString.append("LEFT JOIN districts ds ON ds.districtuid=te.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=te.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND te.portal_villageid=vs.villageslno AND vs.districtuid=te.portal_districtid AND vs.mandalslno=te.portal_mandalid ");
		queryString.append("LEFT JOIN districts dt ON dt.districtuid=t.portal_districtid ");
		queryString.append("LEFT JOIN mandals mt ON mt.mandalslno=t.portal_mandalid AND dt.districtuid=mt.districtuid ");
		queryString.append("LEFT JOIN villages vt ON vt.districtuid=dt.districtuid AND vt.mandalslno=mt.mandalslno AND t.portal_villageid=vt.villageslno AND vt.districtuid=t.portal_districtid AND vt.mandalslno=t.portal_mandalid ");
		queryString.append("WHERE partnerlist LIKE '%MSO%' ");
		queryString.append("GROUP BY TE.tenantname,MSO,TE.regoff_pocname,TE.regoff_pocmob1,T.tenantname,LMO,T.regoff_pocname,T.regoff_pocmob1, ");
		queryString.append("ds.districtname,ms.mandalname,dt.districtname,mt.mandalname,vs.villagename,vt.villagename,TE.createdon,T.createdon ");
		queryString.append("ORDER BY TE.tenantname ");

		try {
			query = getEntityManager().createNativeQuery(queryString.toString(), MsoDetailsWithLmosBO.class);
			listMsoWiseLmo = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getMsoWiseLmoDetails" + e);
		}
		return listMsoWiseLmo;
	}
	
	//LMO CPE Stock
	@SuppressWarnings("unchecked")
	public List<TenantStockReportBO> getLMOStockReport(String tenantCode,String status) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<TenantStockReportBO> lmoStockReport = new ArrayList<>();
		
		queryString.append("select cs.cpeslno,cs.cpemacaddr,cpm.cpetypelov AS cpe_profile,cs.batchdate,cs.cafno,ts.tenantcode,ds.districtname,ms.mandalname,vs.villagename from cpestock cs ");
		queryString.append("LEFT JOIN cpe_profilemaster cpm ON cs.profile_id=cpm.profile_id ");
		queryString.append("LEFT JOIN tenants ts ON   cs.LMOCODE=ts.tenantcode AND ts.tenanttypelov='LMO' ");
		queryString.append("LEFT JOIN districts ds ON ds.districtuid=ts.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=ts.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND ");
		queryString.append("ts.portal_villageid=vs.villageslno AND vs.districtuid=ts.portal_districtid AND vs.mandalslno=ts.portal_mandalid ");
		queryString.append("where ts.tenantcode is not null AND cs.lmocode like '%LMO%' AND cs.lmocode='"+tenantCode+"' and cs.status='"+status+"' ");
		
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),TenantStockReportBO.class);
			lmoStockReport = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lmoStockReport;
	}
	
	//MSO CPE Stock
	@SuppressWarnings("unchecked")
	public List<TenantStockReportBO> getMSOStockReport(String tenantCode,String status) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<TenantStockReportBO> msoStockReport = new ArrayList<>();
		
		queryString.append("SELECT cs.cpeslno,cs.cpemacaddr,cpm.cpetypelov AS cpe_profile,cs.batchdate,cs.cafno,ts.tenantcode,ds.districtname,ms.mandalname,vs.villagename ");
		queryString.append("FROM CPESTOCK cs ");
		queryString.append("LEFT JOIN cpe_profilemaster cpm ON cs.profile_id=cpm.profile_id ");
		queryString.append("LEFT JOIN tenants ts ON   cs.MSPCODE=ts.tenantcode AND ts.tenanttypelov='MSP' ");
		queryString.append("LEFT JOIN districts ds ON ds.districtuid=ts.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=ts.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND  ");
		queryString.append("ts.portal_villageid=vs.villageslno AND vs.districtuid=ts.portal_districtid AND vs.mandalslno=ts.portal_mandalid ");
		queryString.append("WHERE ts.tenantcode IS NOT NULL AND cs.MSPCODE LIKE '%MSO%' AND cs.lmocode IS NULL AND cs.MSPCODE='"+tenantCode+"' AND cs.status = '"+status+"' ");
		
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(),TenantStockReportBO.class);
			msoStockReport = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msoStockReport;
	}
	
	//Mso Wise Stock Count
	@SuppressWarnings("unchecked")
	public List<MsoCafNotCpeStockBo> getMsoWiseCpeStock() {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<MsoCafNotCpeStockBo> MsoCafNotCpeStockList = new ArrayList<>();

		queryString.append("SELECT ts.tenantcode,ts.tenantname,ts.regoff_pocname AS MSO_Name,ts.regoff_pocmob1 AS MSO_MobileNO,ds.districtname AS districtname,ms.mandalname AS Mso_Mandalname,vs.villagename AS Mso_Villagename, ");
		//queryString.append("-- SUM(CASE WHEN cs.STATUS = 4 THEN 1 ELSE 0 END) CAF_Done_Stock_Count , ");
		queryString.append("SUM(CASE WHEN cs.STATUS = 2  THEN 1 ELSE 0 END) AS CAF_Not_Stock_Count  ");
		queryString.append(" FROM CPESTOCK cs ");
		queryString.append("LEFT JOIN tenants ts ON   cs.MSPCODE=ts.tenantcode AND ts.tenanttypelov='MSP' ");
		queryString.append(" LEFT JOIN districts ds ON ds.districtuid=ts.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=ts.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND  ");
		queryString.append("ts.portal_villageid=vs.villageslno AND vs.districtuid=ts.portal_districtid AND vs.mandalslno=ts.portal_mandalid ");
		queryString.append("WHERE ts.tenantcode IS NOT NULL AND cs.MSPCODE LIKE '%MSO%' AND cs.lmocode IS NULL ");
		queryString.append("GROUP BY ts.tenantcode,ts.tenantname,ts.regoff_pocname,ts.regoff_pocmob1 ");

		try {
			query = getEntityManager().createNativeQuery(queryString.toString(), MsoCafNotCpeStockBo.class);
			MsoCafNotCpeStockList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getMsoWiseCpeStockDetails" + e);
		}
		return MsoCafNotCpeStockList;
	}
	
	//LMO Details of Loggedin MSO
	@SuppressWarnings("unchecked")
	public List<MsoDetailsWithLmosBO> getLmoDetailsOfLoggedinMso(String msoCode) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<MsoDetailsWithLmosBO> listMsoWiseLmo = new ArrayList<>();

		queryString.append("SELECT DISTINCT TE.tenantname AS MSONAME,REPLACE(REPLACE(SUBSTRING_INDEX(r.partnerlist, ',', -1),']',''),' ','') MSO,  ");
		queryString.append("TE.regoff_pocname AS MSO_ContactName,TE.regoff_pocmob1 AS MSO_MobileNo,ds.districtname AS mso_district,ms.mandalname AS mso_mandal,vs.villagename AS mso_villagename,TE.createdon AS mso_createdon,T.tenantname AS LMONAME, ");
		queryString.append("REPLACE(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(r.partnerlist, ',', 2), ',', -1), ']', '' ),' ','') LMO, T.regoff_pocname AS LMO_ContactName, ");
		queryString.append("T.regoff_pocmob1 AS LMO_MobileNo,dt.districtname AS lmo_district,mt.mandalname AS lmo_mandal,vt.villagename AS lmo_villagename, T.createdon AS lmo_createdon ");
		queryString.append("FROM RSAGRMNTS r "); 
		queryString.append("LEFT JOIN tenants T ON T.tenantcode = REPLACE(REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(r.partnerlist, ',', 2), ',', -1), ']', '' ),' ','') ");
		queryString.append("LEFT JOIN tenants TE ON TE.tenantcode = REPLACE(REPLACE(SUBSTRING_INDEX(r.partnerlist, ',', -1),']',''),' ','') ");
		queryString.append("LEFT JOIN districts ds ON ds.districtuid=te.portal_districtid ");
		queryString.append("LEFT JOIN mandals ms ON ms.mandalslno=te.portal_mandalid AND ds.districtuid=ms.districtuid ");
		queryString.append("LEFT JOIN villages vs ON vs.districtuid=ds.districtuid AND vs.mandalslno=ms.mandalslno AND te.portal_villageid=vs.villageslno AND vs.districtuid=te.portal_districtid AND vs.mandalslno=te.portal_mandalid ");
		queryString.append("LEFT JOIN districts dt ON dt.districtuid=t.portal_districtid ");
		queryString.append("LEFT JOIN mandals mt ON mt.mandalslno=t.portal_mandalid AND dt.districtuid=mt.districtuid ");
		queryString.append("LEFT JOIN villages vt ON vt.districtuid=dt.districtuid AND vt.mandalslno=mt.mandalslno AND t.portal_villageid=vt.villageslno AND vt.districtuid=t.portal_districtid AND vt.mandalslno=t.portal_mandalid ");
		queryString.append("WHERE partnerlist LIKE '%MSO%' AND TE.tenantcode = :msoCode ");
		queryString.append("GROUP BY TE.tenantname,MSO,TE.regoff_pocname,TE.regoff_pocmob1,T.tenantname,LMO,T.regoff_pocname,T.regoff_pocmob1, ");
		queryString.append("ds.districtname,ms.mandalname,dt.districtname,mt.mandalname,vs.villagename,vt.villagename,TE.createdon,T.createdon ");
		queryString.append("ORDER BY TE.tenantname ");

		try {
			query = getEntityManager().createNativeQuery(queryString.toString(), MsoDetailsWithLmosBO.class);
			query.setParameter("msoCode", msoCode);
			listMsoWiseLmo = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getLmoDetailsOfLoggedinMso()" + e);
		}
		return listMsoWiseLmo;
	}
	
	//LMO Caf Details of Login MSO
	@SuppressWarnings("unchecked")
	public List<ComsHelperDTO> getLmoCafDetailsOfLoginMso(String lmoCode) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<ComsHelperDTO> lmoCafsMsolog = new ArrayList<>();

		queryString.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode, cf.contactmob, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, ");
		queryString.append("cf.cpeslno, CASE WHEN cf.status = 6 THEN 'Active' WHEN cf.status = 2 THEN 'Pending For Provisioning'  WHEN cf.status = 7 THEN 'Suspended' ");
		queryString.append("WHEN cf.status = 8 THEN 'De-activated' END STATUS, IFNULL(DATE_FORMAT(cf.actdate, '%d/%m/%Y'), 'NA') actdate, cust.aadharno, ");
		queryString.append("cf.apsfluniqueid FROM cafs cf, customermst cust WHERE cf.custid = cust.custid AND cf.lmocode = :lmoCode ");

		try {
			query = getEntityManager().createNativeQuery(queryString.toString());
			query.setParameter("lmoCode", lmoCode);
			lmoCafsMsolog = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getLmoDetailsOfLoggedinMso()" + e);
		}
		return lmoCafsMsolog;
	}
	
	//MSO Revenue share Details
	@SuppressWarnings("unchecked")
	@Transactional
	public List<MsoRevenueShareBO> getRevenueShareDetailsOfLoginMso(String year,String month, String tenantCode) {
		StringBuilder queryString = new StringBuilder();
		Query query = null;
		List<MsoRevenueShareBO> revenueList = new ArrayList<>();
		
		if(tenantCode.startsWith("MSO")){
			queryString.append("select lmocode,apsflinv_share,msoinv_share,lmoinv_share,(apsflinv_share + msoinv_share + lmoinv_share) as totalbill,lmo_total_collected,(apsflinv_share + msoinv_share + lmoinv_share-lmo_total_collected) as yettocollect ");
			queryString.append("from rev_summarytable where msocode =:tenantCode and invyr =:year and invmn =:month ");
		}else{
			queryString.append("select lmocode,apsflinv_share,msoinv_share,lmoinv_share,(apsflinv_share + msoinv_share + lmoinv_share) as totalbill,lmo_total_collected,(apsflinv_share + msoinv_share + lmoinv_share-lmo_total_collected) as yettocollect ");
			queryString.append("from rev_summarytable where lmocode =:tenantCode and invyr =:year and invmn =:month group by lmocode ");
		}
		
		try {
			query = getEntityManager().createNativeQuery(queryString.toString(), MsoRevenueShareBO.class);
			query.setParameter("year", year);
			query.setParameter("month", month);
			query.setParameter("tenantCode", tenantCode);
			revenueList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getLmoDetailsOfLoggedinMso()" + e);
		}
		return revenueList;
	}
	
	
	//MSO Revenue share Details
		@SuppressWarnings("unchecked")
		@Transactional
		public String getTotalPreviousBalance(String year,String month, String tenantCode) {
			StringBuilder queryString = new StringBuilder();
			Query query = null;
			String totalPrevBal="";
			
				queryString.append("SELECT sum(cu.prevbal) as totalsum FROM custinv cu,cafs cf WHERE cu.prevpaidamt = '0'  and (cu.invamt + cu.srvctax+cu.prevbal-cu.prevpymtreceived) > '0' ");
				queryString.append(" AND cu.pmntcustid = cf.custid AND  lmocode =:tenantCode and invyr =:year and invmn =:month and cf.custid is not null GROUP BY cf.lmocode");
       
			
			
			try {
				query = getEntityManager().createNativeQuery(queryString.toString());
				query.setParameter("year", year);
				query.setParameter("month", month);
				query.setParameter("tenantCode", tenantCode);
				totalPrevBal = query.getResultList().get(0).toString();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("getTotalPreviousBalance()" + e);
			}
			return totalPrevBal;
		}
	
	
	
	
	
	
	
	
	
	// login LMO Revenue Caf wise in Details 
		@SuppressWarnings("unchecked")
		public List<CafWiseRevenueOfLoginLmoBo> getRevenueDetailsCafwiseOfLoginLmo(String year,String month, String lmoCode) {
			logger.error("getRevenueCafwiseDetailsOfLoginLmo():start");
			StringBuilder queryString = new StringBuilder();
			Query query = null;
			List<CafWiseRevenueOfLoginLmoBo> lmoCafwiserevnue = new ArrayList<>();

			/*queryString.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode, cf.contactmob, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, ");
			queryString.append("cf.cpeslno, CASE WHEN cf.status = 6 THEN 'Active' WHEN cf.status = 2 THEN 'Pending For Provisioning'  WHEN cf.status = 7 THEN 'Suspended' ");
			queryString.append("WHEN cf.status = 8 THEN 'De-activated' END STATUS, IFNULL(DATE_FORMAT(cf.actdate, '%d/%m/%Y'), 'NA') actdate, cust.aadharno, ");
			queryString.append("cf.apsfluniqueid FROM cafs cf, customermst cust WHERE cf.custid = cust.custid AND cf.lmocode = :lmoCode ");*/
			
			
			queryString.append(" SELECT DISTINCT cf.cafno as cafno,cf.lmocode as lmocode,c.custname as custname,");
			queryString.append(" DATE_FORMAT(cf.cafdate,'%d/%m/%Y') cafDate,cf.contactmob as contactmob,c.depbal as depbal, ");
			queryString.append(" IFNULL((SELECT SUM(apsflshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='2017' AND invmn='12' GROUP BY invs.cafno),0.00) AS APSFLShare,");
			queryString.append(" IFNULL((SELECT SUM(msoshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='2017' AND invmn='12' GROUP BY invs.cafno),0.00) AS MSOShare,");
			queryString.append(" IFNULL((SELECT SUM(lmoshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='2017' AND invmn='12' GROUP BY invs.cafno),0.00) AS LMOShare,");
			queryString.append(" IFNULL((SELECT SUM(invamt+srvctax) FROM cafinv WHERE pmntcustid=cf.pmntcustid AND acctcafno=cf.cafno AND invyr='2017' AND invmn='12' ),0.00) AS total,");
			queryString.append(" IFNULL((SELECT prevbal FROM custinv cus WHERE cus.prevpaidamt='0' AND (cus.invamt + cus.srvctax+cus.prevbal-cus.prevpymtreceived) > '0' AND cus.pmntcustid=cf.pmntcustid AND invyr='2017' AND invmn='12' GROUP BY cus.pmntcustid),'0.00') AS PreviousBalance ");
			queryString.append(" FROM cafs cf, cafinv cfv,customermst c ,wipstages w WHERE cf.custid = c.custid AND cf.cafno=cfv.acctcafno " );
			queryString.append(" AND cfv.invyr=:invyr AND invmn=:invmn AND w.appcode = 'COMS' AND w.statuscode = cf.status  AND cf.lmocode=:lmocode");
			

			try {
				query = getEntityManager().createNativeQuery(queryString.toString(),CafWiseRevenueOfLoginLmoBo.class);
				query.setParameter("lmocode", lmoCode);
				query.setParameter("invyr", year);
				query.setParameter("invmn", month);
				lmoCafwiserevnue = query.getResultList();
				logger.error("getRevenueDetailsCafwiseOfLoginLmo():END");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("getRevenueDetailsCafwiseOfLoginLmo()" + e);
			}
			return lmoCafwiserevnue;
		}
	
}
