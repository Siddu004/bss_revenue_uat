package com.arbiva.apsfl.reports.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SubstationWiseCafVO implements Serializable {

	private static final long serialVersionUID = 7526278040677829801L;

	@Id
	@Column(name = "popNameWithOLT")
	private String popNameWithIP;
	@Column(name="districtname")
	private String districtname;
	@Column(name="mandalname")
	private String mandalname;
	@Column(name = "cafCount1")
	private long cafCount1;
	@Column(name = "cafCount2")
	private long cafCount2;
	@Column(name = "cafCount3")
	private long cafCount3;
	@Column(name = "cafCount4")
	private long cafCount4;
	@Column(name = "cafCount5")
	private long cafCount5;
	@Column(name = "cafCount6")
	private long cafCount6;
	@Column(name = "cafCount7")
	private long cafCount7;
	@Column(name = "cafCount8")
	private long cafCount8;
	@Column(name = "total")
	private long total;


	public String getPopNameWithIP() {
		return popNameWithIP;
	}

	public void setPopNameWithIP(String popNameWithIP) {
		this.popNameWithIP = popNameWithIP;
	}
	
	public String getDistrictname() {
		return districtname;
	}

	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}

	public String getMandalname() {
		return mandalname;
	}

	public void setMandalname(String mandalname) {
		this.mandalname = mandalname;
	}

	public long getCafCount1() {
		return cafCount1;
	}

	public void setCafCount1(long cafCount1) {
		this.cafCount1 = cafCount1;
	}

	public long getCafCount2() {
		return cafCount2;
	}

	public void setCafCount2(long cafCount2) {
		this.cafCount2 = cafCount2;
	}

	public long getCafCount3() {
		return cafCount3;
	}

	public void setCafCount3(long cafCount3) {
		this.cafCount3 = cafCount3;
	}

	public long getCafCount4() {
		return cafCount4;
	}

	public void setCafCount4(long cafCount4) {
		this.cafCount4 = cafCount4;
	}

	public long getCafCount5() {
		return cafCount5;
	}

	public void setCafCount5(long cafCount5) {
		this.cafCount5 = cafCount5;
	}

	public long getCafCount6() {
		return cafCount6;
	}

	public void setCafCount6(long cafCount6) {
		this.cafCount6 = cafCount6;
	}

	public long getCafCount7() {
		return cafCount7;
	}

	public void setCafCount7(long cafCount7) {
		this.cafCount7 = cafCount7;
	}

	public long getCafCount8() {
		return cafCount8;
	}

	public void setCafCount8(long cafCount8) {
		this.cafCount8 = cafCount8;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

}
