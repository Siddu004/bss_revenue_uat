package com.arbiva.apsfl.controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.arbiva.apfgc.tenant.bo.CafWiseReportBO;
import com.arbiva.apfgc.tenant.bo.DistrictWiseCafBO;
import com.arbiva.apfgc.tenant.bo.DistrictWiseCpeBO;
import com.arbiva.apfgc.tenant.bo.EnterpriseSubscriberBO;
import com.arbiva.apfgc.tenant.bo.TenantStockReportBO;
import com.arbiva.apfgc.tenant.bo.LmoStockCountBO;
import com.arbiva.apfgc.tenant.bo.MsoCafNotCpeStockBo;
import com.arbiva.apfgc.tenant.bo.MsoDetailsWithLmosBO;
import com.arbiva.apfgc.tenant.bo.MsoWiseCpeBo;
import com.arbiva.apfgc.tenant.bo.PONWiseBo;
import com.arbiva.apfgc.tenant.bo.PONWithZeroCAFBO;
import com.arbiva.apsfl.coms.dto.PageObject;
import com.arbiva.apsfl.reports.vo.MsoListVo;
import com.arbiva.apsfl.reports.vo.ReportsDTO;
import com.arbiva.apsfl.reports.vo.SubstationWiseCafVO;
import com.arbiva.apsfl.tms.serviceImpl.DemandNoteServiceImpl;
import com.arbiva.apsfl.util.DateUtill;
import com.arbiva.apsfl.util.PaginationObject;

@SuppressWarnings("deprecation")
@Controller
public class DemandNoteReportController {

	@Autowired
	DemandNoteServiceImpl demandNoteServiceImpl;

	@RequestMapping(value = "/msoDetails", method = RequestMethod.GET)
	public String getMsoDetails(Model msoModel) {
		try {
			List<MsoListVo> listMso = new ArrayList<>();
			listMso = demandNoteServiceImpl.getMsoList();
			msoModel.addAttribute("msoList", listMso);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "msoList";
	}

	@RequestMapping(value = "/downloadMsoDetails", method = RequestMethod.GET)
	public void downloadMsoDetails(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		List<MsoListVo> listMso = new ArrayList<>();
		listMso = demandNoteServiceImpl.getMsoList();
		if (download) {
			DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("MSO Wise CPE Request Report");
			Row header = sheet.createRow(4);
			HSSFRow aRow = sheet.createRow(5);
			HSSFRow row = sheet.createRow(0);// Title
			HSSFRow row1 = sheet.createRow(1);// Report name
			HSSFRow row2 = sheet.createRow(2);// Dates

			Calendar cal = Calendar.getInstance();
			String currDate = sdf.format(cal.getTime());
			try (ServletOutputStream out = response.getOutputStream();
					InputStream my_banner_image = this.getClass().getClassLoader().getResourceAsStream("/APSFL.png")) {

				byte[] bytes = IOUtils.toByteArray(my_banner_image);
				int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
				my_banner_image.close();
				HSSFPatriarch drawing = sheet.createDrawingPatriarch();
				ClientAnchor my_anchor = new HSSFClientAnchor();
				my_anchor.setDx1(0);
				my_anchor.setDy1(0);
				my_anchor.setDx2(0);
				my_anchor.setDy2(0);
				my_anchor.setCol1(0);
				my_anchor.setRow1(0);
				my_anchor.setCol2(1);
				my_anchor.setRow2(1);
				HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
				my_picture.resize();

				CellStyle style = workbook.createCellStyle();
				style.setAlignment(CellStyle.ALIGN_CENTER);
				sheet.setDefaultColumnWidth(30);
				Font font = workbook.createFont();
				font.setFontName("Arial");
				style.setFillForegroundColor(HSSFColor.WHITE.index);
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.BLACK.index);
				style.setFont(font);

				/* For creating first row */
				Cell cell = row.createCell(2);
				cell.setCellValue("Andhra Pradesh State Fibernet Ltd");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 3));
				row.setHeightInPoints(20);
				cell.setCellStyle(style);

				/* For creating second row */
				cell = row1.createCell(2);
				cell.setCellValue("MSO Wise CPE Request Report");
				sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
				cell.setCellStyle(style);

				cell = row2.createCell(0);
				cell.setCellValue("Generated On");
				cell.setCellStyle(style);

				cell = row2.createCell(1);
				cell.setCellValue(currDate);
				cell.setCellStyle(style);

				header.createCell(0).setCellValue("MSO Code");
				header.getCell(0).setCellStyle(style);

				header.createCell(1).setCellValue("MSO Company  Name");
				header.getCell(1).setCellStyle(style);

				header.createCell(2).setCellValue("MSO Contact Name");
				header.getCell(2).setCellStyle(style);

				header.createCell(3).setCellValue("MSO Contact No");
				header.getCell(3).setCellStyle(style);

				header.createCell(4).setCellValue("District Name ");
				header.getCell(4).setCellStyle(style);

				header.createCell(5).setCellValue("Mandal Name");
				header.getCell(5).setCellStyle(style);

				header.createCell(6).setCellValue("Upfront Cpe's");
				header.getCell(6).setCellStyle(style);

				header.createCell(7).setCellValue("CPE-36 Months(Rs.1700)");
				header.getCell(7).setCellStyle(style);

				header.createCell(8).setCellValue("CPE-48 Months(Rs.500)");
				header.getCell(8).setCellStyle(style);

				// create data rows
				int rowCount = 5;

				for (MsoListVo obj : listMso) {
					aRow = sheet.createRow(rowCount++);
					aRow.createCell(0).setCellValue(obj.getTenantCode());
					aRow.createCell(1).setCellValue(obj.getTenantName());
					aRow.createCell(2).setCellValue(obj.getRegOfficePocName());
					aRow.createCell(3).setCellValue(obj.getPocMobileNumber());
					aRow.createCell(4).setCellValue(obj.getDistrictName());
					aRow.createCell(5).setCellValue(obj.getMandalName());
					aRow.createCell(6).setCellValue(obj.getEmiDemandQuantity());
					aRow.createCell(7).setCellValue(obj.getEmi36DemandQuantity());
					aRow.createCell(8).setCellValue(obj.getEmi48DemandQuantity());
				}

				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=MSO Wise CPE Request Report.xls");
				workbook.write(out);
				System.out.println("Excel written successfully..");

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				workbook = null;
				sheet = null;
				header = null;
				aRow = null;
			}
		}
	}

	@RequestMapping(value = "/lmoDetails", method = RequestMethod.GET)
	public String getLmoDetails(Model msoModel, @RequestParam("tenantcode") String tenantCode) {
		List<MsoListVo> listMso = new ArrayList<>();
		listMso = demandNoteServiceImpl.getLmoList(tenantCode);
		msoModel.addAttribute("lmoList", listMso);
		msoModel.addAttribute("tenantCode", tenantCode);

		return "lmoList";
	}

	@RequestMapping(value = "/downLoadLmoDetails", method = RequestMethod.GET)
	public void downLoadLmoDetails(@RequestParam("tenantcode") String tenantCode,
			@RequestParam(value = "download", required = false) boolean download, HttpServletRequest request,
			HttpServletResponse response) {

		List<MsoListVo> listMso = new ArrayList<>();
		listMso = demandNoteServiceImpl.getLmoList(tenantCode);

		if (download) {
			DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("LMO Wise CPE Request Report");
			Row header = sheet.createRow(4);
			HSSFRow aRow = sheet.createRow(5);
			HSSFRow row = sheet.createRow(0);// Title
			HSSFRow row1 = sheet.createRow(1);// Report name
			HSSFRow row2 = sheet.createRow(2);// Dates

			Calendar cal = Calendar.getInstance();
			String currDate = sdf.format(cal.getTime());
			try (ServletOutputStream out = response.getOutputStream();
					InputStream my_banner_image = this.getClass().getClassLoader().getResourceAsStream("/APSFL.png")) {

				byte[] bytes = IOUtils.toByteArray(my_banner_image);
				int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
				my_banner_image.close();
				HSSFPatriarch drawing = sheet.createDrawingPatriarch();
				ClientAnchor my_anchor = new HSSFClientAnchor();
				my_anchor.setDx1(0);
				my_anchor.setDy1(0);
				my_anchor.setDx2(0);
				my_anchor.setDy2(0);
				my_anchor.setCol1(0);
				my_anchor.setRow1(0);
				my_anchor.setCol2(1);
				my_anchor.setRow2(1);
				HSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
				my_picture.resize();

				CellStyle style = workbook.createCellStyle();
				style.setAlignment(CellStyle.ALIGN_CENTER);
				sheet.setDefaultColumnWidth(30);
				Font font = workbook.createFont();
				font.setFontName("Arial");
				style.setFillForegroundColor(HSSFColor.WHITE.index);
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.BLACK.index);
				style.setFont(font);

				/* For creating first row */
				Cell cell = row.createCell(2);
				cell.setCellValue("Andhra Pradesh State Fibernet Ltd");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 3));
				row.setHeightInPoints(20);
				cell.setCellStyle(style);

				/* For creating second row */
				cell = row1.createCell(2);
				cell.setCellValue("LMO Wise CPE Request Details");
				sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
				cell.setCellStyle(style);

				cell = row2.createCell(0);
				cell.setCellValue("Generated On");
				cell.setCellStyle(style);

				cell = row2.createCell(1);
				cell.setCellValue(currDate);
				cell.setCellStyle(style);

				header.createCell(0).setCellValue("LMO Name");
				header.getCell(0).setCellStyle(style);

				header.createCell(1).setCellValue("District Name");
				header.getCell(1).setCellStyle(style);

				header.createCell(2).setCellValue("Mandal Name");
				header.getCell(2).setCellStyle(style);

				header.createCell(3).setCellValue("Upfront Cpe's");
				header.getCell(3).setCellStyle(style);

				header.createCell(4).setCellValue("CPE-36 Months(Rs.1700)");
				header.getCell(4).setCellStyle(style);

				header.createCell(5).setCellValue("CPE-48 Months(Rs.500)");
				header.getCell(5).setCellStyle(style);

				// create data rows
				int rowCount = 5;

				for (MsoListVo obj : listMso) {
					aRow = sheet.createRow(rowCount++);
					aRow.createCell(0).setCellValue(obj.getTenantName());
					aRow.createCell(1).setCellValue(obj.getDistrictName());
					aRow.createCell(2).setCellValue(obj.getMandalName());
					aRow.createCell(3).setCellValue(obj.getEmiDemandQuantity());
					aRow.createCell(4).setCellValue(obj.getEmi36DemandQuantity());
					aRow.createCell(5).setCellValue(obj.getEmi48DemandQuantity());
				}

				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=LMO WiseCPE Request Report.xls");
				workbook.write(out);
				System.out.println("Excel written successfully..");

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				workbook = null;
				sheet = null;
				header = null;
				aRow = null;
			}
		}
	}

	@RequestMapping(value = "/msoWiseDemand", method = RequestMethod.GET)
	public String msoDemand(Model msoModel) {
		try {
			List<MsoWiseCpeBo> listMso = new ArrayList<>();
			listMso = demandNoteServiceImpl.getMsoWiseDemand();
			msoModel.addAttribute("list", listMso);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "msoWiseDemand";
	}

	@RequestMapping(value = "/downloadMsoWiseDemand", method = RequestMethod.GET)
	public void downloadMsoWiseDemand(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getMsoWiseExcelFile();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=MSO Wise Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}

	@RequestMapping(value = "/districtWiseDemand", method = RequestMethod.GET)
	public String districtWiseDemand(Model msoModel) {
		try {
			List<DistrictWiseCpeBO> listMso = new ArrayList<>();
			listMso = demandNoteServiceImpl.getDistrictWiseDemand();
			msoModel.addAttribute("list", listMso);
			msoModel.addAttribute("currentDate", DateUtill.getSTRING_dd_MMM_YYYY());
			msoModel.addAttribute("previousDate", DateUtill.getPreviousSTRING_dd_MMM_YYYY());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "districtWiseDemand";
	}

	@RequestMapping(value = "/downloadDistrictWiseDemand", method = RequestMethod.GET)
	public void downloadDistrictWiseDemand(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getDistrictWiseExcelFile();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=District Wise Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}

	@RequestMapping(value = "/cafWiseDemand", method = RequestMethod.GET)
	public String cpeWiseDemand(Model msoModel) {
		try {
			List<CafWiseReportBO> list = new ArrayList<>();
			list = demandNoteServiceImpl.getCafWiseDemand();
			msoModel.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "cafReport";
	}

	@RequestMapping(value = "/downloadCafWiseReport", method = RequestMethod.GET)
	public void downloadCafWiseReport(HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getCafWiseExcelFile();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=CAF Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	@RequestMapping(value = "/districtWiseCafReport", method = RequestMethod.GET)
	public String districtWiseCpeReports(Model model, HttpServletRequest request) {
		try {
			List<DistrictWiseCafBO> districtWiseCafList = new ArrayList<>();
			districtWiseCafList = demandNoteServiceImpl.getDistrictWiseCafList();
			model.addAttribute("districtWiseCafList", districtWiseCafList);
			model.addAttribute("currentDate", DateUtill.getSTRING_dd_MMM_YYYY());
			model.addAttribute("previousDate", DateUtill.getPreviousSTRING_dd_MMM_YYYY());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return "distWiseCafReport";
	}

	@RequestMapping(value = "/downloadDistrictWiseCafReport", method = RequestMethod.GET)
	public void downloadDistrictWiseCpeReports(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getDistrictWiseCafExcelFile();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=District Wise CAF Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	
	@RequestMapping(value = "/entSubscriberListReport", method = RequestMethod.GET)
	public String getBasedOnActivationDate(HttpServletRequest request, HttpServletResponse response, Model uiModel) throws IOException {
		return "entSubscriberReport";
	}
	
	@RequestMapping(value = "/entSubscriberListReportPagination", method = RequestMethod.GET)
	@ResponseBody public PaginationObject<EnterpriseSubscriberBO> entSubscriberListReportPagination(HttpServletRequest request, 
										HttpServletResponse response, Model uiModel) throws IOException {

		PaginationObject<EnterpriseSubscriberBO> dataPageObject = new PaginationObject<EnterpriseSubscriberBO>();
		ReportsDTO reportDto = new ReportsDTO();

		try {
			Integer pageDisplayLength = 10;
			Integer pageNumber = 1;
			String sdir = "desc";
			String sortColumn = "";
			int sortParameter = 0;
			pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
			
			if (null != request.getParameter("iDisplayStart"))
				pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
			if (request.getParameter("iSortCol_0") != null) {
				sortParameter = Integer.parseInt(request.getParameter("iSortCol_0"));
			}
			sortColumn = EnterpriseSubscriberBO.UsersDataSearchParams.getColumns("COLUMN_" + sortParameter);
			if (request.getParameter("sSortDir_0") != null)
				sdir = request.getParameter("sSortDir_0");

			// Fetch search parameter
			String searchParameter = request.getParameter("sSearch");
			PageObject pageObject = new PageObject();
			pageObject.setMinSize((pageNumber - 1) * pageDisplayLength);
			pageObject.setMaxSize(pageDisplayLength);
			pageObject.setSortColumn(sortColumn);
			pageObject.setSortOrder(sdir);
			pageObject.setSearchParameter(searchParameter);
			
			reportDto.setPageObject(pageObject);
			reportDto = demandNoteServiceImpl.getEnterpriseSubscriberListReport(pageObject);
			dataPageObject.setAaData(reportDto.getEntSubscriberList());
			dataPageObject.setiTotalDisplayRecords(Long.parseLong(reportDto.getCount()));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}

		return dataPageObject;
	}
	
	
	@RequestMapping(value = "/downloadEntSubscriberReport", method = RequestMethod.GET)
	public void downloadEntSubscriberReport(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getEntSubscriberExcelFile(request, response);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=entsubscriberreport.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	@RequestMapping(value = "/ponWiseReport", method = RequestMethod.GET)
	public String getPonWiseReport(HttpServletRequest request, HttpServletResponse response, Model uiModel) throws IOException {
		return "ponWiseReport";
	}
	
	
	@RequestMapping(value = "/ponWiseReportPagination", method = RequestMethod.GET)
	@ResponseBody public PaginationObject<PONWiseBo> getPonWiseReportPagination(HttpServletRequest request, 
										HttpServletResponse response, Model uiModel) throws IOException {

		PaginationObject<PONWiseBo> dataPageObject = new PaginationObject<PONWiseBo>();
		ReportsDTO reportDto = new ReportsDTO();

		try {
			Integer pageDisplayLength = 10;
			Integer pageNumber = 1;
			String sdir = "desc";
			String sortColumn = "";
			int sortParameter = 0;
			pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
			
			if (null != request.getParameter("iDisplayStart"))
				pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
			if (request.getParameter("iSortCol_0") != null) {
				sortParameter = Integer.parseInt(request.getParameter("iSortCol_0"));
			}
			sortColumn = PONWiseBo.UsersDataSearchParams.getColumns("COLUMN_" + sortParameter);
			if (request.getParameter("sSortDir_0") != null)
				sdir = request.getParameter("sSortDir_0");

			// Fetch search parameter
			String searchParameter = request.getParameter("sSearch");
			PageObject pageObject = new PageObject();
			pageObject.setMinSize((pageNumber - 1) * pageDisplayLength);
			pageObject.setMaxSize(pageDisplayLength);
			pageObject.setSortColumn(sortColumn);
			pageObject.setSortOrder(sdir);
			pageObject.setSearchParameter(searchParameter);
			
			reportDto.setPageObject(pageObject);
			reportDto = demandNoteServiceImpl.getAllotedPONWithCaf(pageObject);
			dataPageObject.setAaData(reportDto.getPonWiseList());
			dataPageObject.setiTotalDisplayRecords(Long.parseLong(reportDto.getCount()));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}

		return dataPageObject;
	}
	
	@RequestMapping(value = "/downloadPonWiseReport", method = RequestMethod.GET)
	public void downloadPonWiseReport(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.downloadPONWiseExcel(request, response);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=ponWiseRepost.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	@RequestMapping(value = "/ponWithNoCafReport", method = RequestMethod.GET)
	public String getPonWithNoCafReport(HttpServletRequest request, HttpServletResponse response, Model uiModel) throws IOException {
		return "ponWithNoCafReport";
	}
	
	
	@RequestMapping(value = "/ponWithNoCafPagination", method = RequestMethod.GET)
	@ResponseBody public PaginationObject<PONWithZeroCAFBO> getponWithNoCafPagination(HttpServletRequest request, 
										HttpServletResponse response, Model uiModel) throws IOException {

		PaginationObject<PONWithZeroCAFBO> dataPageObject = new PaginationObject<PONWithZeroCAFBO>();
		ReportsDTO reportDto = new ReportsDTO();

		try {
			Integer pageDisplayLength = 10;
			Integer pageNumber = 1;
			String sdir = "desc";
			String sortColumn = "";
			int sortParameter = 0;
			pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
			
			if (null != request.getParameter("iDisplayStart"))
				pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / pageDisplayLength) + 1;
			if (request.getParameter("iSortCol_0") != null) {
				sortParameter = Integer.parseInt(request.getParameter("iSortCol_0"));
			}
			sortColumn = PONWithZeroCAFBO.UsersDataSearchParams.getColumns("COLUMN_" + sortParameter);
			if (request.getParameter("sSortDir_0") != null)
				sdir = request.getParameter("sSortDir_0");

			// Fetch search parameter
			String searchParameter = request.getParameter("sSearch");
			PageObject pageObject = new PageObject();
			pageObject.setMinSize((pageNumber - 1) * pageDisplayLength);
			pageObject.setMaxSize(pageDisplayLength);
			pageObject.setSortColumn(sortColumn);
			pageObject.setSortOrder(sdir);
			pageObject.setSearchParameter(searchParameter);
			
			reportDto.setPageObject(pageObject);
			reportDto = demandNoteServiceImpl.getPONWithZeroCAFBO(pageObject);
			dataPageObject.setAaData(reportDto.getPonWithZeroCafList());
			dataPageObject.setiTotalDisplayRecords(Long.parseLong(reportDto.getCount()));
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}

		return dataPageObject;
	}
	
	@RequestMapping(value = "/downloadPonWithNoCafReport", method = RequestMethod.GET)
	public void downloadPonWithNoCafReport(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.downloadPONWithNoCafExcel(request, response);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=ponWithNoCaf.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	@RequestMapping(value = "/substationCafReport", method = RequestMethod.GET)
	public String substationCafReport(Model model) {
		try {
			List<SubstationWiseCafVO> list = new ArrayList<>();
			list = demandNoteServiceImpl.getSubstationWisePONWithCaf();
			
			model.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "substationCafReport";
	}
	
	@RequestMapping(value = "/downloadSubWiseReport", method = RequestMethod.GET)
	public void downloadSubWiseReport(HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.downloadSubWiseExcelFile(request, response);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=CAF Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	//LMO Wise Stock Count
	@RequestMapping(value = "/lmoWiseStockCount", method = RequestMethod.GET)
	public String getLmoWiseStockCount(Model msoModel) {
		try {
			List<LmoStockCountBO> list = new ArrayList<>();
			list = demandNoteServiceImpl.getlmoWiseStockCount();
			msoModel.addAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "lmoWiseStockCount";
	}
	
	// download LMO wise stock Count
	@RequestMapping(value = "/downloadlmoWiseStockCount", method = RequestMethod.GET)
	public void downloadlmoWiseStockCount(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getLmoStockCountExcel();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=LMO Wise Stock Count.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	// MSO wise LMO 
	@RequestMapping(value = "/msoDetailsWithAlignedLMOs", method = RequestMethod.GET)
	public String msoDetailsWithAlignedLMOs(Model model, HttpServletRequest request) {
		try {
			List<MsoDetailsWithLmosBO> lmoMsoList = new ArrayList<>();
			lmoMsoList = demandNoteServiceImpl.getMsoWiseLmoDetails();
			model.addAttribute("lmoMsoList", lmoMsoList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return "msoDetailsWithAlignedLMOs";
	}

	// MSO wise LMO report
	@RequestMapping(value = "/downloadMsoWiseLmoDetails", method = RequestMethod.GET)
	public void downloadMsoWiseLmoDetails(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getMsoWiseLmoExcel();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=MSO Wise LMO Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
	
	//tenant wise CPE Stock
	@RequestMapping(value = "/tenantWiseCpeStock", method = RequestMethod.GET)
	public String tenantCpeStockReport(Model model,  @RequestParam(value = "tenantCode") String tenantCode,
			@RequestParam(value = "status") String status) {
		try {
			List<TenantStockReportBO> tenantStock = new ArrayList<>();
			tenantStock = demandNoteServiceImpl.getTenantStockReport(tenantCode,status);
			model.addAttribute("tenantStock", tenantStock);
			model.addAttribute("tenantCode", tenantCode);
			model.addAttribute("status", status);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return "tenantWiseCpeStock";
	}
		
	// Tenant wise CPE stock report
	@RequestMapping(value = "/downloadTenantWiseCpeStock", method = RequestMethod.GET)
	public void downloadTenantCPEStock(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response, 
			@RequestParam(value = "tenantCode", required = false) String tenantCode,
			@RequestParam(value = "status", required = false) String status) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getTenantStockExcel(tenantCode, status);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=Tenant CPE Stock.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
		
	// MSO wise CPE Stock method
	@RequestMapping(value = "/msoWiseCpeStock", method = RequestMethod.GET)
	public String MsoWiseCpeStockDetails(Model model, HttpServletRequest request) {
		try {
			List<MsoCafNotCpeStockBo> msoCafNotCpeStockList = new ArrayList<>();
			msoCafNotCpeStockList = demandNoteServiceImpl.getMsoWiseCpeStockDetails();
			model.addAttribute("msoCafNotCpeStockList", msoCafNotCpeStockList);
			/*
			 * model.addAttribute("currentDate",
			 * DateUtill.getSTRING_dd_MMM_YYYY());
			 * model.addAttribute("previousDate",
			 * DateUtill.getPreviousSTRING_dd_MMM_YYYY());
			 */
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
		return "msoWiseCpeStock";
	}

	// MSO wise CPE stock report
	@RequestMapping(value = "/downloadMsoCafNOtCpeStock", method = RequestMethod.GET)
	public void downloadMsoCafNotCpeStockListExcel(@RequestParam(value = "download", required = false) boolean download,
			HttpServletRequest request, HttpServletResponse response) {
		HSSFWorkbook workbook = null;
		try (ServletOutputStream out = response.getOutputStream()) {
			workbook = demandNoteServiceImpl.getMsoCafNotCpeStockListExcel();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=MSO Caf Not CpeStock Report.xls");
			workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			workbook = null;
		}
	}
		
}
