package com.arbiva.apfgc.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.filter.GenericFilterBean;

public class SessionSharingFilter extends GenericFilterBean  {
	
	static final String REDIRECT_LOGIN = "/apfgc/preLogin.do";
	static final String APFGC_USER = "apfgc_user";
	public static final String LOGOUT_URL = "/apfgc/logout.do";

	@Override
	public void doFilter(ServletRequest serRequest, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) serRequest;
		
		boolean isRestService = httpRequest.getRequestURI().contains("/rest/");
		
		if (!isRestService) {
			
			HttpSession session = httpRequest.getSession();
			String loginID = (String) session.getAttribute("loginID");
			if (loginID != null) {
				chain.doFilter(httpRequest, response);
			} else {
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect(LOGOUT_URL);
			}
		}else{
			chain.doFilter(httpRequest, response);
		}
	}


}
