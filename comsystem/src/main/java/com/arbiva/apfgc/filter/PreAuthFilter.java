package com.arbiva.apfgc.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.filter.GenericFilterBean;

public class PreAuthFilter extends GenericFilterBean {

	static final String REDIRECT_LOGIN = "/apfgc/preLogin.do";
	static final String APFGC_USER = "apfgc_user";
	private static final Logger logger = Logger.getLogger(PreAuthFilter.class);

	@Override
	public void doFilter(ServletRequest serRequest, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) serRequest;
		HttpServletRequest httpRequest = (HttpServletRequest) serRequest;
		boolean isRestService = httpRequest.getRequestURI().contains("/rest/");
		if (!isRestService) {
			String loginID = getUserNameFromCookies(serRequest);
			if (loginID != null) {
				
				HttpSession session = request.getSession(true);
				session.setAttribute("preLoginID", loginID);
				chain.doFilter(request, response);
			} else {
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect(REDIRECT_LOGIN);
			}
		}else{
			chain.doFilter(request, response);
		}
	}

	public static String getUserNameFromCookies(ServletRequest serRequest) {
		HttpServletRequest request = (HttpServletRequest) serRequest;
		Cookie[] cookies = request.getCookies();
		String userName = null;
		if (cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user")) {
					userName = cookie.getValue();
				}
				logger.info("Cookies  :::"+cookie);
			}
		}

		return userName;
	}

	/*private RedirectView redirect() {

		return new RedirectView(REDIRECT_LOGIN, true);
	}*/

}