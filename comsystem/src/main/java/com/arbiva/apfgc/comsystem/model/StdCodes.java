/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "stdcodes")
public class StdCodes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "stdcode")
	private String stdCode;

	@Column(name = "stdname")
	private String stdName;

	@Column(name = "usedseries")
	private String usedSeries;

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public String getUsedSeries() {
		return usedSeries;
	}

	public void setUsedSeries(String usedSeries) {
		this.usedSeries = usedSeries;
	}

}
