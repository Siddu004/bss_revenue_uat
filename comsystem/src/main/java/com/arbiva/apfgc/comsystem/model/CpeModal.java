/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cpe_profilemaster")
public class CpeModal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="profile_id")
	private Long profileId;
	
	@Column(name="cpe_model")
	private String cpeModel;
	
	@Column(name="cpetypelov")
	private String cpetypeLov;
	
	@Column(name="cpe_profilename")
	private String cpeProfileName;
	
	@Column(name="cpe_modeldetails")
	private String cpeModelDetails;

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getCpeModel() {
		return cpeModel;
	}

	public void setCpeModel(String cpeModel) {
		this.cpeModel = cpeModel;
	}

	public String getCpeProfileName() {
		return cpeProfileName;
	}

	public void setCpeProfileName(String cpeProfileName) {
		this.cpeProfileName = cpeProfileName;
	}

	public String getCpeModelDetails() {
		return cpeModelDetails;
	}

	public void setCpeModelDetails(String cpeModelDetails) {
		this.cpeModelDetails = cpeModelDetails;
	}

	public String getCpetypeLov() {
		return cpetypeLov;
	}

	public void setCpetypeLov(String cpetypeLov) {
		this.cpetypeLov = cpetypeLov;
	}
	
}
