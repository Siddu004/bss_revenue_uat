/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.comsystem.dao.impl.TaxRegionMappingDao;
import com.arbiva.apfgc.comsystem.model.ChargeCodes;
import com.arbiva.apfgc.comsystem.model.ChargeTaxes;
import com.arbiva.apfgc.comsystem.model.TaxMaster;
import com.arbiva.apfgc.comsystem.model.TaxRegionMapping;
import com.arbiva.apfgc.comsystem.service.TaxRegionMappingService;

/**
 * @author Lakshman
 *
 */

@Service
public class TaxRegionMappingServiceImpl implements TaxRegionMappingService {
	
	private static final Logger logger = Logger.getLogger(TaxRegionMappingServiceImpl.class);
	
	@Autowired
	TaxRegionMappingDao taxRegionMappingDao;

	@Override
	public List<TaxRegionMapping> getAllTaxRegionMappingList() {
		List<TaxRegionMapping> taxRegionMappingList = new ArrayList<>();
		try {
			taxRegionMappingList = taxRegionMappingDao.getAllTaxRegionMappingList();
		} catch (Exception e) {
			logger.error("TaxRegionMappingServiceImpl :: getAllTaxRegionMappingList()" + e);
			e.printStackTrace();
		} finally {

		}
		return taxRegionMappingList;
	}

	@Override
	public List<TaxMaster> getAllTaxMasterList() {
		List<TaxMaster> TaxMasterList = new ArrayList<>();
		try {
			TaxMasterList = taxRegionMappingDao.getAllTaxMasterList();
		} catch (Exception e) {
			logger.error("TaxRegionMappingServiceImpl :: getAllTaxMasterList()" + e);
			e.printStackTrace();
		} finally {

		}
		return TaxMasterList;
	}

	@Override
	public List<ChargeCodes> getAllChargeCodesList() {
		List<ChargeCodes> chargeCodesList = new ArrayList<>();
		try {
			chargeCodesList = taxRegionMappingDao.getAllChargeCodesList();
		} catch (Exception e) {
			logger.error("TaxRegionMappingServiceImpl :: getAllChargeCodesList()" + e);
			e.printStackTrace();
		} finally {

		}
		return chargeCodesList;
	}

	@Override
	public List<ChargeTaxes> getAllChargeTaxesList() {
		List<ChargeTaxes> chargeTaxesList = new ArrayList<>();
		try {
			chargeTaxesList = taxRegionMappingDao.getAllChargeTaxesList();
		} catch (Exception e) {
			logger.error("TaxRegionMappingServiceImpl :: getAllChargeTaxesList()" + e);
			e.printStackTrace();
		} finally {

		}
		return chargeTaxesList;
	}

}
