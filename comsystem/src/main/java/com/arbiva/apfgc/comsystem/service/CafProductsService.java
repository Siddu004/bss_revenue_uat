/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;

/**
 * @author Arbiva
 *
 */
public interface CafProductsService {

	public abstract CafProducts createCafProducts(CustomerCafVO customerCafVO, Long cafNo, String loginID, String custCode, Long stbCafNo, String stbPkgCode, Float paidAmount, Long addPkgCafNo) throws Exception;

	public CafProducts findCafProductByCafNoAndMspCodeAndProdCode(Long cafNo, String mspCode, String prodcode);
	
	public abstract List<CafProducts> getCafProducts(Long cafNo);

	public abstract void updateCafProducts(PaymentVO paymentVO, String loginID);
	
	public abstract void DeletePreviewsPkg(Long cafNo);

	public abstract void createMSOPackages(CustomerCafVO customerCafVO, Long stbCafNo);

	public abstract List<ProductsVO> getCafServicesListByProdcode(String pkgCodes);
	
}
