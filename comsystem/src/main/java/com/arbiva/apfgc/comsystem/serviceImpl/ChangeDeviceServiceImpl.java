package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.ChangeDeviceDaoImpl;
import com.arbiva.apfgc.comsystem.dto.ChangeOnuDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeOnuHelperDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeStbDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeStbHelperDTO;
import com.arbiva.apfgc.comsystem.dto.PublicIpAddressDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;
import com.arbiva.apfgc.comsystem.model.CpeStock;
import com.arbiva.apfgc.comsystem.model.Lovs;
import com.arbiva.apfgc.comsystem.model.Phonenos;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;
import com.arbiva.apfgc.provision.models.ChangeDevice;

/**
 * @author Srinivas V
 * @since Feb 08 2017
 * 
 */

@Service
public class ChangeDeviceServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(ChangeDeviceServiceImpl.class);

	@Autowired
	ChangeDeviceDaoImpl changeDeviceDaoImpl;
	
	@Autowired
	LovsServiceImpl lovsService;

	@Autowired
	private ProvisioningBusinessServiceImpl provisionBusnServiceImpl;

	/**
	 * @param aadhar
	 * @param tenantCode
	 * @param tenatType 
	 * @return customer list
	 */
	public List<Object[]> getCafList(String aadhar, String tenantCode, String tenatType) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafList(aadhar, tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCaf;
	}
	
	public List<Object[]> getCafListByTrackId(String trackId, String tenantCode, String tenatType) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafListByTrackId(trackId, tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCaf;
	}

	/**
	 * @param mobileNo
	 * @param tenantCode
	 * @return customers List
	 */
	public List<Object[]> getCafListWithMobileNO(String mobileNo, String tenantCode,String tenatType) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafListWithMobileNO(mobileNo, tenantCode ,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCaf;
	}

	/**
	 * @param cafNumber
	 * @param tenantCode
	 * @return customer details
	 */
	public List<Object[]> getCafsByCafNo(String cafNumber, String tenantCode,String tenatType) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafsByCafNo(cafNumber, tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCaf;
	}

	/**
	 * @param tenantCode
	 * @return List of Customers based on Login
	 */
	public List<Object[]> getCafsByTenantCode(String tenantCode,String tenatType) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafsByTenantCode(tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCaf;
	}
	/**
	 * @param cafNum
	 * @return List of STB details
	 */
	public ChangeStbHelperDTO getStbDetailsByCafNo(String cafNum) {
		List<Object[]> stbDetails = null;
		ChangeStbHelperDTO cStbHelper = new ChangeStbHelperDTO();
		List<ChangeStbDTO> listChangeStb = new ArrayList<>();
		List<Lovs> lovListChangeReason= new ArrayList<>();
		try {
			stbDetails = changeDeviceDaoImpl.getStbDetailsByCafNo(cafNum);
			lovListChangeReason=lovsService.getLovValuesByLovName("IPTV_CHANGE_REASONS");
			for (Object[] obj : stbDetails) {
				ChangeStbDTO csDto = new ChangeStbDTO();
				csDto.setParentCafno(obj[0] != null ? obj[0].toString() : "");
				csDto.setStbCafNo(obj[1] != null ? obj[1].toString() : "");
				csDto.setOldStbMacAddr(obj[2] != null ? obj[2].toString() : "");
				csDto.setNwSubCode(obj[3] != null ? obj[3].toString() : "");
				csDto.setCustId(obj[4] != null ? obj[4].toString() : "");
				csDto.setOldStbSerialNum(obj[5] != null ? obj[5].toString() : "");
				csDto.setOldStbProfileId(obj[6] != null ? obj[6].toString() : "");
				listChangeStb.add(csDto);
			}

			cStbHelper.setChangeStbList(listChangeStb);
			cStbHelper.setLovChangeReasonlist(lovListChangeReason);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cStbHelper;
	}
	
	/**
	 * @param newMacAdd
	 * @param tenantCode
	 * @return  Status of New STB 
	 */
	public String getNewStdStatus(String newStbserial, String tenantCode) {
		String  statusMessage = null ;
		try {
			statusMessage = changeDeviceDaoImpl.getNewStdStatus(newStbserial, tenantCode);
			if(statusMessage == null || statusMessage.isEmpty()){
				statusMessage  = "New STB Serial Number Invalid. Please enter valid Serial Number !!!";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return statusMessage;
	}

	/**
	 * @param tenantCode
	 * @param changeStbDto
	 * @return Update Status 
	 */
	@Transactional
	public String updateStbMac(String tenantCode, ChangeStbDTO changeStbDto) {
		String statusMessage[] = null;
		String status="";
		String newStbMac = changeStbDto.getNewStbMacAddr();
		String oldstbmac = changeStbDto.getOldStbMacAddr();
		String stbCafNo = changeStbDto.getStbCafNo();
		String changePurpose = changeStbDto.getChangePurpose();
		String nwSubCode = changeStbDto.getNwSubCode();
		String custId = changeStbDto.getCustId();
		String paretnCafNo = changeStbDto.getParentCafno();
		String newStdSerial = changeStbDto.getNewStbSerialNum();
		String statusflag = "";
		try {
			statusflag = changeDeviceDaoImpl.getNewStdStatus(newStdSerial, tenantCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(statusflag == null || statusflag.isEmpty())
		  {
			status="<span style='color: red;'> New STB Mac address Invalid !!! </span>";
		} else {
			try {
				statusMessage = changeDeviceDaoImpl.updateStbMac(oldstbmac, newStbMac, stbCafNo, changePurpose, tenantCode,paretnCafNo);
				if(statusMessage[0].equalsIgnoreCase("STB Mac Address Updated Successfully!!!"))
				{
				ChangeDevice cDevice = new ChangeDevice();
				cDevice.setAcctcafno(Long.parseLong(paretnCafNo));
				cDevice.setStbcafno(Long.parseLong(stbCafNo));
				cDevice.setCustomerid(Long.parseLong(custId));
				cDevice.setIncpemacaddr(newStbMac);
				cDevice.setOutcpemacaddr(oldstbmac);
				cDevice.setIncpeslno(statusMessage[1]);
				cDevice.setOutcpeslno(changeStbDto.getOldStbSerialNum());
				cDevice.setInprofile_id(Long.parseLong(statusMessage[2]));
				cDevice.setOutprofile_id(Long.parseLong(changeStbDto.getOldStbProfileId()));
				cDevice.setNwsubscode(nwSubCode);
				provisionBusnServiceImpl.changeCpeProvForSTB(cDevice);
				}
				status=statusMessage[0];
			} catch (Exception e) {
				e.printStackTrace();
				
			}
		}
		return status;
	}

	 
	/**
	 * @param cafNum
	 * @return Onu details based on caf number
	 */
	public ChangeOnuHelperDTO getOnuDetailsByCafNo(String cafNum) {
		List<Object[]> onuDetails = new ArrayList<>();
		List<ChangeOnuDTO> changeOnuList = new ArrayList<>();
		List<Lovs> lovListChangeReason= new ArrayList<>();
	     ChangeOnuHelperDTO chelper= new ChangeOnuHelperDTO();
		try {
			onuDetails = changeDeviceDaoImpl.getOnuDetailsByCafNo(cafNum);
			lovListChangeReason = lovsService.getLovValuesByLovName("IPTV_CHANGE_REASONS");
			for (Object[] obj : onuDetails) {
				ChangeOnuDTO changeOnuDTO = new ChangeOnuDTO();
				 
					changeOnuDTO.setCafNumberOnuChange(obj[0] == null ? "" : obj[0].toString());
					changeOnuDTO.setCafsCustId(obj[1] == null ? "" : obj[1].toString());
					changeOnuDTO.setOldOnuSerialNum(obj[2] == null ? "" : obj[2].toString());
					changeOnuDTO.setOldCpeOnuMacAddress(obj[3] == null ? "" : obj[3].toString());
					changeOnuDTO.setOnuModel(obj[4] == null ? "" : obj[4].toString());
					changeOnuDTO.setOnuProfileName(obj[5] == null ? "" : obj[5].toString());
					changeOnuDTO.setNwSubCode(obj[6] == null ? "" : obj[6].toString());
					changeOnuDTO.setProfileIdOnu(obj[7] == null ? "" : obj[7].toString());
					changeOnuList.add(changeOnuDTO);
			}
			 chelper.setLovChangeReasonlist(lovListChangeReason);
			 chelper.setChangeOnuList(changeOnuList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return chelper;
	}

	/**
	 * @param changeOnuDTO Input Old ONU serial and new ONU serial and Tenant Code
	 * @return Status on Update
	 */
	@Transactional
	public String updateOnuMac(ChangeOnuDTO changeOnuDTO) {
		String statusMessage[]=null;
		String status="";
		String statusflag = "";
		try {
			statusflag = changeDeviceDaoImpl.getNewOnuStatus(changeOnuDTO.getNewOnuSerialNum(),
					changeOnuDTO.getTenantCodeOnuChange(), changeOnuDTO.getOldOnuSerialNum());
			if(statusflag == null || statusflag.isEmpty()){
				status = "<span style='color: red;'>New ONU Mac address Invalid!!!</span>";
			} else {
				statusMessage = changeDeviceDaoImpl.updateOnuMac(changeOnuDTO.getOldOnuSerialNum(),
						changeOnuDTO.getNewOnuSerialNum(), changeOnuDTO.getChangePurpose(),
						changeOnuDTO.getCafNumberOnuChange(), changeOnuDTO.getTenantCodeOnuChange());
			if(statusMessage[0].equalsIgnoreCase("ONU Mac Address Updated Successfully!!!"))
			{
				ChangeDevice cDevice = new ChangeDevice();
				cDevice.setAcctcafno(Long.parseLong(changeOnuDTO.getCafNumberOnuChange()));
				cDevice.setCustomerid(Long.parseLong(changeOnuDTO.getCafsCustId()));
				cDevice.setIncpemacaddr(statusMessage[1]);
				cDevice.setOutcpemacaddr(changeOnuDTO.getOldCpeOnuMacAddress());
				cDevice.setIncpeslno(changeOnuDTO.getNewOnuSerialNum());
				cDevice.setOutcpeslno(changeOnuDTO.getOldOnuSerialNum());
				cDevice.setInprofile_id(Integer.parseInt(changeOnuDTO.getProfileIdOnu()));
				cDevice.setOutprofile_id(Integer.parseInt(changeOnuDTO.getProfileIdOnu()));
				cDevice.setNwsubscode(changeOnuDTO.getNwSubCode());
				provisionBusnServiceImpl.changeCpeProvForONU(cDevice);
			}
				status=statusMessage[0];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	/**
	 * @param changeOnuDTO like new ONU serail number and old ONU serial Number and tenant Code
	 * @return Status
	 */
	public String getNewOnuStatus(ChangeOnuDTO changeOnuDTO) {
		String statusMessage = "";
		 
		try {
			statusMessage = changeDeviceDaoImpl.getNewOnuStatus(changeOnuDTO.getNewOnuSerialNum(),
					changeOnuDTO.getTenantCodeOnuChange(), changeOnuDTO.getOldOnuSerialNum());
			if(statusMessage == null || statusMessage.isEmpty()){
 			statusMessage  = "New ONU Serial Number Invalid. Please enter valid Serial Number !!! ";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return statusMessage;
	}

	
	@Transactional
	public void resourceRelease() {
		
		List<Caf> cafsList = new ArrayList<>();
		CpeStock cpeStock = null;
		List<CafSTBs> cafStbList = new ArrayList<>();
		List<CafSrvcPhoneNos> cafPhoneNoList = new ArrayList<>();
		Phonenos phoneNos = null;
		
		try{
			/* Getting All CAF's from  OLPROVREQUESTS */
			cafsList = changeDeviceDaoImpl.findAllTerminatedCafs();
			
			for (Caf caf : cafsList) {

				/* Getting ONU and updating status to corresponding MSO/LMO */
				cpeStock = changeDeviceDaoImpl.findOnuByOnuSerialNo(caf.getCpeslNo());
				if(cpeStock != null)
					this.updateCpeStock(cpeStock);
				
				/* Getting List Of STB's */
				cafStbList = changeDeviceDaoImpl.findCafStbsByCafId(caf.getCafNo());
				for(CafSTBs CafSTBs : cafStbList){
					
					/* Getting STB and updating status to corresponding MSO/LMO */
					cpeStock = changeDeviceDaoImpl.findStbByStbMacAddress(CafSTBs.getStbmacAddr());
					if(cpeStock != null)
						this.updateCpeStock(cpeStock);
				}
				
				/* Getting List Of Phone Numbers */
				cafPhoneNoList = changeDeviceDaoImpl.finsCafPhoneNosByCafNo(caf.getCafNo());
				for(CafSrvcPhoneNos cafSrvcPhoneNos : cafPhoneNoList){
					/* Getting Phone Number and updating status 1 */
					phoneNos = changeDeviceDaoImpl.findPhoneNosByCafSrvcPhNo(cafSrvcPhoneNos.getPhoneNo());
					if(phoneNos != null){
						phoneNos.setStatus(1);
						changeDeviceDaoImpl.save(phoneNos);
					}
				
				}
				
			}
			
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		
	}
	
	public void updateCpeStock(CpeStock cpeStock) {
		
		if (cpeStock.getLmoCode() != null && !cpeStock.getLmoCode().equalsIgnoreCase("")) {
			cpeStock.setStatus(3);
			cpeStock.setModifiedon(Calendar.getInstance());
			changeDeviceDaoImpl.save(cpeStock);
		} else if (cpeStock.getMspCode() != null && !cpeStock.getMspCode().equalsIgnoreCase("")) {
			cpeStock.setStatus(2);
			cpeStock.setModifiedon(Calendar.getInstance());
			changeDeviceDaoImpl.save(cpeStock);
		} else {
			cpeStock.setStatus(1);
			cpeStock.setModifiedon(Calendar.getInstance());
			changeDeviceDaoImpl.save(cpeStock);
		}
	}

	public List<Object[]> getCafsListPuIP(PublicIpAddressDTO publicIpAddressDTO) {
		List<Object[]> listCaf = null;
		try {
			listCaf = changeDeviceDaoImpl.getCafsListPuIP(publicIpAddressDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return listCaf;
	}

	public List<Object[]> getIpAddress(PublicIpAddressDTO publicIpAddressDTO) {
		List<Object[]> ipAddress = null;
		try {
			ipAddress = changeDeviceDaoImpl.getIpAddress(publicIpAddressDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return ipAddress;
	}
	@Transactional
	public String updateIpAddress(PublicIpAddressDTO publicIpAddressDTo) {
		String statusmsg="";
		try
		{
			statusmsg=changeDeviceDaoImpl.updateIpAddress(publicIpAddressDTo);
			if(statusmsg.contains("Successfully"))
			{
			@SuppressWarnings("unused")
			String status = provisionBusnServiceImpl.requestForAAA(publicIpAddressDTo.getCafNum());
			}
		}catch(Exception e)
		{
			LOGGER.error(e.getMessage());
		}
		
		return statusmsg;
	}
	
	@Transactional
	public void releaseIpAddress(String cafNo) {
try{
		changeDeviceDaoImpl.releaseIpAddress(cafNo);
}catch(Exception e)
{
	LOGGER.error(e.getMessage());
}
	}
}
