package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class LmoDetailsBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	
	
	@Column(name = "regbal")
	private BigDecimal regBal;
	
	public BigDecimal getRegBal() {
		return regBal;
	}

	public void setRegBal(BigDecimal regBal) {
		this.regBal = regBal;
	}
	
	@Column(name = "lmocode")
	private String lmoCode;




	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	@Column(name = "pmntamt")
	private BigDecimal pmntAmt;
	
	public BigDecimal getPmntAmt() {
		return pmntAmt;
	}

	public void setPmntAmt(BigDecimal pmntAmt) {
		this.pmntAmt = pmntAmt;
	}

}
