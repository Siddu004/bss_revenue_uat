package com.arbiva.apfgc.comsystem.vo;

public class HSICummSumPMonthVO {

	private String currentMonthUploadSize;

	private String currentMonthDownloadSize;
	private String prevMonthUploadSize;
	private String preMonthDownloadSize;

	public String getCurrentMonthUploadSize() {
		return currentMonthUploadSize;
	}

	public void setCurrentMonthUploadSize(String currentMonthUploadSize) {
		this.currentMonthUploadSize = currentMonthUploadSize;
	}

	public String getCurrentMonthDownloadSize() {
		return currentMonthDownloadSize;
	}

	public void setCurrentMonthDownloadSize(String currentMonthDownloadSize) {
		this.currentMonthDownloadSize = currentMonthDownloadSize;
	}

	public String getPrevMonthUploadSize() {
		return prevMonthUploadSize;
	}

	public void setPrevMonthUploadSize(String prevMonthUploadSize) {
		this.prevMonthUploadSize = prevMonthUploadSize;
	}

	public String getPreMonthDownloadSize() {
		return preMonthDownloadSize;
	}

	public void setPreMonthDownloadSize(String preMonthDownloadSize) {
		this.preMonthDownloadSize = preMonthDownloadSize;
	}

}
