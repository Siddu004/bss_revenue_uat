/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="billcycles")
public class BillCycle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "actvnuptoday")
	private int actvnupToday;
	
	@Column(name = "billrunday")
	private int billrunDay;
	
	@Column(name = "maxcapacity")
	private Long maxCapacity;
	
	@Column(name = "currcount")
	private Long currCount;
	
	@Column(name = "status")
	private int status;

	public int getActvnupToday() {
		return actvnupToday;
	}

	public void setActvnupToday(int actvnupToday) {
		this.actvnupToday = actvnupToday;
	}

	public int getBillrunDay() {
		return billrunDay;
	}

	public void setBillrunDay(int billrunDay) {
		this.billrunDay = billrunDay;
	}

	public Long getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(Long maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public Long getCurrCount() {
		return currCount;
	}

	public void setCurrCount(Long currCount) {
		this.currCount = currCount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
