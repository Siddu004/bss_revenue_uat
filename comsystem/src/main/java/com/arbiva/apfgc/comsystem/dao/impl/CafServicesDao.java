/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;

/**
 * @author Arbiva
 *
 */
@Repository
public class CafServicesDao {//extends JpaSpecificationExecutor<CafServices>, JpaRepository<CafServices, CafServicesPK> {

	private static final Logger LOGGER = Logger.getLogger(CafServicesDao.class);
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCafServices(CafServices cafServices) {
		getEntityManager().remove(cafServices);
		getEntityManager().flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<CafServices> findAllCafServices() {
		return (List<CafServices>)getEntityManager().createQuery("Select cafServices from " + CafServices.class.getSimpleName() + " cafServices").getResultList();
	}
	
	public void saveCafServices(CafServices cafServices) {
		getEntityManager().merge(cafServices);
	}
	
	public void createCafServices(CafServices cafServices) {
		getEntityManager().merge(cafServices);
	}

	public boolean updateCafServices(String prodCafNo, String prodCode, String loginId, String ipAddress) {
		Query query = null;
		StringBuilder builder = new StringBuilder();
		boolean changePkgProvisionStatus = false;
		try {
			String modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			if(loginId.isEmpty() && ipAddress.isEmpty()) {
				builder.append(" update cafsrvcs set status = "+ComsEnumCodes.CAF_PROCESS_DEACTIVE_STATUS.getStatus()+", ");
				builder.append(" modifiedon = '"+modifiedOn+"', deactdate = '"+modifiedOn+"' where cafno = "+prodCafNo+" and prodcode = '"+prodCode+"' ");
			} else {
				builder.append(" update cafsrvcs set status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+", modifiedby = '"+loginId+"', ");
				builder.append(" modifiedon = '"+modifiedOn+"', deactdate = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+prodCafNo+" and prodcode = '"+prodCode+"' ");
			}
			query = getEntityManager() .createNativeQuery(builder.toString());
			int x = query.executeUpdate();
			if(x > 0) {
				changePkgProvisionStatus = true;
			}

		} catch(Exception e) {
			LOGGER.error("EXCEPTION::updateCafServices() " + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
			ipAddress = null;
		}
		return changePkgProvisionStatus;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSrvcCodeAndCoreSrvcCodeByCafNo(Long cafNo) {
		Query query = null;
		StringBuilder builder = null;
		List<Object[]> srvcAndCoreSrvcCodeList = new ArrayList<>();
		try {
			builder = new StringBuilder("select cs.srvccode, s.coresrvccode from cafsrvcs cs, srvcs s where cafno = "+cafNo+" and cs.srvccode = s.srvccode and current_date() between s.effectivefrom and s.effectiveto ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			srvcAndCoreSrvcCodeList = query.getResultList();

		} catch(Exception e) {
			LOGGER.error("EXCEPTION::getSrvcCodeAndCoreSrvcCodeByCafNo() " + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return srvcAndCoreSrvcCodeList;
	}

	public CafServices getCafServicesByCafnoAndSrvcCode(Long cafNo, String srvcCode) {
		CafServices cafServices = new CafServices();
		StringBuilder builder = new StringBuilder(" FROM ").append(CafServices.class.getSimpleName()).append(" WHERE cafno=:cafNo and srvccode=:srvccode");
			try {
				LOGGER.info("START::getCafServicesByCafnoAndSrvcCode()");
				TypedQuery<CafServices> query = getEntityManager().createQuery(builder.toString(), CafServices.class);
				query.setParameter("cafNo", cafNo);
				query.setParameter("srvccode", srvcCode);
				cafServices = query.getSingleResult();
				LOGGER.info("END::getCafServicesByCafnoAndSrvcCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getCafServicesByCafnoAndSrvcCode() " + e);
			}
		return cafServices;
	}
}
