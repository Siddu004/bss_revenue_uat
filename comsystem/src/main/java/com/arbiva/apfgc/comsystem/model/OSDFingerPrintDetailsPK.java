/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class OSDFingerPrintDetailsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long requestId;
	
	private Integer jsonSeqno;

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Integer getJsonSeqno() {
		return jsonSeqno;
	}

	public void setJsonSeqno(Integer jsonSeqno) {
		this.jsonSeqno = jsonSeqno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jsonSeqno == null) ? 0 : jsonSeqno.hashCode());
		result = prime * result + ((requestId == null) ? 0 : requestId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OSDFingerPrintDetailsPK other = (OSDFingerPrintDetailsPK) obj;
		if (jsonSeqno == null) {
			if (other.jsonSeqno != null)
				return false;
		} else if (!jsonSeqno.equals(other.jsonSeqno))
			return false;
		if (requestId == null) {
			if (other.requestId != null)
				return false;
		} else if (!requestId.equals(other.requestId))
			return false;
		return true;
	}
	
}
