package com.arbiva.apfgc.comsystem.model;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * 
 * @author Lakshman
 * 
 */
@Entity
@Table(name = "olpayments")
public class OLPayment extends Base {

	public OLPayment() {

	}

	public OLPayment(PaymentVO paymentVO) throws ParseException {
		this.aadharNo = paymentVO.getAadharNumber();
		this.pmntAmt = paymentVO.getPaidAmount();
		this.pmntDate = Calendar.getInstance();
		this.pmntMode = paymentVO.getPaymentMode();
		this.pmntBank = paymentVO.getBankName();
		this.pmntBranch = paymentVO.getBranchName();
		this.setCreatedOn(Calendar.getInstance());
		this.setModifiedOn(Calendar.getInstance());
		if(paymentVO.getDdDate() != null) {
			if(!paymentVO.getDdDate().isEmpty()) {
				this.pmntrefDate = DateUtill.stringtoDate(paymentVO.getDdDate());
			}
		}
	}

	public OLPayment(Caf caf, BigDecimal balance, Long paymentId, Long prodCafNo) {
		this.setPmntId(paymentId);
		this.setAadharNo(caf.getAadharNo());
		this.setAcctCafNo(caf.getCafNo());
		this.setCreatedOn(Calendar.getInstance());
		this.setCustId(caf.getCustId());
		this.setDistrictuid(Integer.parseInt(caf.getInstDistrict()));
		this.setModifiedOn(Calendar.getInstance());
		this.setPmntAmt(Float.parseFloat(balance.toString()));
		this.setStatus(1);
	}

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pmntid")
	private Long pmntId;

	@Column(name = "custdistuid")
	private Integer districtuid;
	
	@Column(name = "acctcafno")
	private long acctCafNo;

	@Column(name = "aadharno")
	private String aadharNo;
	
	@Column(name = "pmntcustid")
	private Long custId; 

	@Column(name = "pmntmodelov")
	private String pmntMode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pmntdate", nullable = false)
	private Calendar pmntDate;
	
	@Column(name = "pmntrefno")
	private String pmntRefNo;
	
	@Column(name = "pmntbank")
	private String pmntBank;
	
	@Column(name = "pmntbranch")
	private String pmntBranch;
	
	@Column(name = "pmntbranchifsc")
	private String pmntBranchIFSC;
	
	@Column(name = "pmntrefdate")
	private Date pmntrefDate;

	@Column(name = "pmntamt")
	private float pmntAmt;

	@Column(name = "deactivatedby")
	private String deActivatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deactivatedon")
	private Date deActivatedOn;

	@Column(name = "deactivatedipaddr")
	private String deActivatedIpAddr;

	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	@JoinColumn(name = "pmntcustid", referencedColumnName = "custid", nullable = false, insertable=false, updatable=false)
	private Customer customerId;
	
	public String getPmntBank() {
		return pmntBank;
	}

	public void setPmntBank(String pmntBank) {
		this.pmntBank = pmntBank;
	}

	public String getPmntBranch() {
		return pmntBranch;
	}

	public void setPmntBranch(String pmntBranch) {
		this.pmntBranch = pmntBranch;
	}

	public String getPmntBranchIFSC() {
		return pmntBranchIFSC;
	}

	public void setPmntBranchIFSC(String pmntBranchIFSC) {
		this.pmntBranchIFSC = pmntBranchIFSC;
	}

	public Date getPmntrefDate() {
		return pmntrefDate;
	}

	public void setPmntrefDate(Date pmntrefDate) {
		this.pmntrefDate = pmntrefDate;
	}
	
	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	public Long getPmntId() {
		return pmntId;
	}

	public void setPmntId(Long pmntId) {
		this.pmntId = pmntId;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public long getAcctCafNo() {
		return acctCafNo;
	}

	public void setAcctCafNo(long acctCafNo) {
		this.acctCafNo = acctCafNo;
	}
	
	public String getPmntMode() {
		return pmntMode;
	}

	public void setPmntMode(String pmntMode) {
		this.pmntMode = pmntMode;
	}

	public String getPmntRefNo() {
		return pmntRefNo;
	}

	public void setPmntRefNo(String pmntRefNo) {
		this.pmntRefNo = pmntRefNo;
	}

	public Calendar getPmntDate() {
		return pmntDate;
	}

	public void setPmntDate(Calendar pmntDate) {
		this.pmntDate = pmntDate;
	}

	public float getPmntAmt() {
		return pmntAmt;
	}

	public void setPmntAmt(float pmntAmt) {
		this.pmntAmt = pmntAmt;
	}

	public String getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(String deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public Date getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(Date deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public String getDeActivatedIpAddr() {
		return deActivatedIpAddr;
	}

	public void setDeActivatedIpAddr(String deActivatedIpAddr) {
		this.deActivatedIpAddr = deActivatedIpAddr;
	}

	public Integer getDistrictuid() {
		return districtuid;
	}

	public void setDistrictuid(Integer districtuid) {
		this.districtuid = districtuid;
	}
	
}
