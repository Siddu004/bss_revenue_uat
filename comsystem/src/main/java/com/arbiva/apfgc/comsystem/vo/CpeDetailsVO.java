package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

public class CpeDetailsVO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String longtitude;
	private String lattitude;
	private String oltId;
	private String portNo;
	private String subStationName;
	private String address;
	private String village;
	private String mandal;
	private String district;
	private String cpeModel;
	private String cpeSrlNo;
	private String cpeInstChrge;
	private String cpeCost;
	private String pin;
	private String address2;
	private String cpeInstCount;
	private String cpeMacAddress;
	private String cpeLease;
	private String iptvBoxCafNo;
	private String iptvBoxModel;
	private String iptvBoxOwnerShip;
	private String iptvBoxCharge;
	private String iptvBoxInstCharge;
	private String iptvBoxEmiCount;
	private String iptvBoxSrlNo;
	private String iptvBoxMacId;
	
	
	
	public String getLongtitude() {
		return longtitude;
	}
	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getOltId() {
		return oltId;
	}
	public void setOltId(String oltId) {
		this.oltId = oltId;
	}
	public String getPortNo() {
		return portNo;
	}
	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}
	public String getSubStationName() {
		return subStationName;
	}
	public void setSubStationName(String subStationName) {
		this.subStationName = subStationName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getMandal() {
		return mandal;
	}
	public void setMandal(String mandal) {
		this.mandal = mandal;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCpeModel() {
		return cpeModel;
	}
	public void setCpeModel(String cpeModel) {
		this.cpeModel = cpeModel;
	}
	public String getCpeSrlNo() {
		return cpeSrlNo;
	}
	public void setCpeSrlNo(String cpeSrlNo) {
		this.cpeSrlNo = cpeSrlNo;
	}
	public String getCpeInstChrge() {
		return cpeInstChrge;
	}
	public void setCpeInstChrge(String cpeInstChrge) {
		this.cpeInstChrge = cpeInstChrge;
	}
	public String getCpeCost() {
		return cpeCost;
	}
	public void setCpeCost(String cpeCost) {
		this.cpeCost = cpeCost;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCpeInstCount() {
		return cpeInstCount;
	}
	public void setCpeInstCount(String cpeInstCount) {
		this.cpeInstCount = cpeInstCount;
	}
	public String getCpeMacAddress() {
		return cpeMacAddress;
	}
	public void setCpeMacAddress(String cpeMacAddress) {
		this.cpeMacAddress = cpeMacAddress;
	}
	public String getCpeLease() {
		return cpeLease;
	}
	public void setCpeLease(String cpeLease) {
		this.cpeLease = cpeLease;
	}
	public String getIptvBoxCafNo() {
		return iptvBoxCafNo;
	}
	public void setIptvBoxCafNo(String iptvBoxCafNo) {
		this.iptvBoxCafNo = iptvBoxCafNo;
	}
	public String getIptvBoxModel() {
		return iptvBoxModel;
	}
	public void setIptvBoxModel(String iptvBoxModel) {
		this.iptvBoxModel = iptvBoxModel;
	}
	public String getIptvBoxOwnerShip() {
		return iptvBoxOwnerShip;
	}
	public void setIptvBoxOwnerShip(String iptvBoxOwnerShip) {
		this.iptvBoxOwnerShip = iptvBoxOwnerShip;
	}
	public String getIptvBoxCharge() {
		return iptvBoxCharge;
	}
	public void setIptvBoxCharge(String iptvBoxCharge) {
		this.iptvBoxCharge = iptvBoxCharge;
	}
	public String getIptvBoxInstCharge() {
		return iptvBoxInstCharge;
	}
	public void setIptvBoxInstCharge(String iptvBoxInstCharge) {
		this.iptvBoxInstCharge = iptvBoxInstCharge;
	}
	public String getIptvBoxEmiCount() {
		return iptvBoxEmiCount;
	}
	public void setIptvBoxEmiCount(String iptvBoxEmiCount) {
		this.iptvBoxEmiCount = iptvBoxEmiCount;
	}
	public String getIptvBoxSrlNo() {
		return iptvBoxSrlNo;
	}
	public void setIptvBoxSrlNo(String iptvBoxSrlNo) {
		this.iptvBoxSrlNo = iptvBoxSrlNo;
	}
	public String getIptvBoxMacId() {
		return iptvBoxMacId;
	}
	public void setIptvBoxMacId(String iptvBoxMacId) {
		this.iptvBoxMacId = iptvBoxMacId;
	}
	
    
}
