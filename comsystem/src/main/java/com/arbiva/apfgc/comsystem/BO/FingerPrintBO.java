/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.arbiva.apfgc.comsystem.dto.PageObject;

/**
 * @author Lakshman
 *
 */
@Entity
public class FingerPrintBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private String cafNo;

	@Column(name = "aadharno")
	private String aadharNo;

	@Column(name = "custname")
	private String custName;

	@Column(name = "lname")
	private String lName;

	@Column(name = "stbcafno")
	private String stbCafno;

	@Column(name = "cafdate")
	private String cafDate;

	@Column(name = "cpeplace")
	private String cpePlace;

	@Column(name = "stbslno")
	private String stbSlno;
	
	@Column(name = "stbmacaddr")
	private String stbMacaddr;
	
	@Column(name = "nwsubscode")
	private String nwsubsCode;
	
	@Transient
	private String district;
	
	@Transient
	private String mandal;
	
	@Transient
	private String village;
	
	@Transient
	private String selectCaf;
	
	@Transient
	private PageObject pageObject;

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getStbCafno() {
		return stbCafno;
	}

	public void setStbCafno(String stbCafno) {
		this.stbCafno = stbCafno;
	}

	public String getCafDate() {
		return cafDate;
	}

	public void setCafDate(String cafDate) {
		this.cafDate = cafDate;
	}

	public String getCpePlace() {
		return cpePlace;
	}

	public void setCpePlace(String cpePlace) {
		this.cpePlace = cpePlace;
	}

	public String getStbSlno() {
		return stbSlno;
	}

	public void setStbSlno(String stbSlno) {
		this.stbSlno = stbSlno;
	}

	public String getStbMacaddr() {
		return stbMacaddr;
	}

	public void setStbMacaddr(String stbMacaddr) {
		this.stbMacaddr = stbMacaddr;
	}

	public String getNwsubsCode() {
		return nwsubsCode;
	}

	public void setNwsubsCode(String nwsubsCode) {
		this.nwsubsCode = nwsubsCode;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMandal() {
		return mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSelectCaf() {
		return selectCaf;
	}

	public void setSelectCaf(String selectCaf) {
		this.selectCaf = selectCaf;
	}

	public PageObject getPageObject() {
		return pageObject;
	}

	public void setPageObject(PageObject pageObject) {
		this.pageObject = pageObject;
	}
	
}
