/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "cafsrvcphonenos")
@IdClass(CafSrvcPhoneNosPK.class)
public class CafSrvcPhoneNos extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "cafno")
	private Long cafNo;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectiveFrom;
	
	@Id
	@Column(name = "phoneno")
	private String phoneNo;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "passwrd")
	private String password;
	
	@Transient
	private String telePhoneNos;
	
	@Transient
	private String telePhonePwds;
	
	@ManyToOne
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable=false, updatable=false)
	private Caf cafNumber;
	
	public Caf getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(Caf cafNumber) {
		this.cafNumber = cafNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}

	public String getTelePhoneNos() {
		return telePhoneNos;
	}

	public void setTelePhoneNos(String telePhoneNos) {
		this.telePhoneNos = telePhoneNos;
	}

	public String getTelePhonePwds() {
		return telePhonePwds;
	}

	public void setTelePhonePwds(String telePhonePwds) {
		this.telePhonePwds = telePhonePwds;
	}
	
}
