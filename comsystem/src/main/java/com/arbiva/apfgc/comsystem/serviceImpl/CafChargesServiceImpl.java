/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafChargesDao;
import com.arbiva.apfgc.comsystem.model.CafCharges;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafChargesServiceImpl implements CafChargesService {

	private static final Logger logger = Logger.getLogger(CafChargesServiceImpl.class);

	@Autowired
	CafChargesDao cafChargesDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Override
	@Transactional
	public void deleteCafCharges(CafCharges cafCharges) {
		try {
			cafChargesDao.deleteCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl :: deleteCafCharges()" + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	public CafCharges findByCafNo(Long cafNo) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafChargesDao.findByCafNo(cafNo);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl :: findByCafNo()" + e);
			e.printStackTrace();
		} finally {

		}
		return cafCharges;
	}

	@Override
	public List<CafCharges> findAllCafCharges() {
		List<CafCharges> cafChargesList = new ArrayList<>();
		try {
			cafChargesList = cafChargesDao.findAllCafCharges();
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl :: findAllCafCharges()" + e);
			e.printStackTrace();
		}
		return cafChargesList;
	}

	@Override
	@Transactional
	public void saveCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(cafNo);
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setParentCafno(cafNo);
			if (customerCafVO.getInstallmentCount().equalsIgnoreCase("0")) {
				cafCharges.setChargeCode(ComsEnumCodes.ONU_COST_CODE.getCode());
				cafCharges.setChargeAmount(Float.parseFloat(customerCafVO.getCpePrice()));
				cafCharges.setTotalEmiCount(0);
			} else if (!customerCafVO.getInstallmentCount().equalsIgnoreCase("0")) {
				cafCharges.setChargeCode(ComsEnumCodes.ONU_INSTALLATION_CODE.getCode());
				cafCharges.setChargeAmount(Float.parseFloat(customerCafVO.getOnuEmiPrice()));
				cafCharges.setTotalEmiCount(Integer.parseInt(customerCafVO.getInstallmentCount()));
				Integer emiStartMonth = DateUtill.getEmiDate(Calendar.getInstance().getTime());
				Integer emiEndMonth = DateUtill.getEmiMonthAndYear(Integer.parseInt(customerCafVO.getInstallmentCount()) - 1);
				cafCharges.setEmiStartMonth(emiStartMonth);
				cafCharges.setEmiEndMonth(emiEndMonth);
			}
			cafCharges.setCreatedBy(loginID);
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setComplEmiCount(0);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafCharges.setStbcafno(0l);
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::saveCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	@Transactional
	public void createCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(cafNo);
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			cafCharges.setParentCafno(cafNo);
			cafCharges.setChargeCode(ComsEnumCodes.CPE_INSTALLATION_CODE.getCode());
			cafCharges.setChargeAmount(customerCafVO.getInstCharge());
			cafCharges.setCreatedBy(loginID);
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setTotalEmiCount(0);
			cafCharges.setComplEmiCount(0);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafCharges.setStbcafno(0l);
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::createCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	@Transactional
	public void createExtraCableCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(cafNo);
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			cafCharges.setParentCafno(cafNo);
			cafCharges.setChargeCode(ComsEnumCodes.CPE_EXTRACABLE_CHARGE_CODE.getCode());
			cafCharges.setChargeAmount(Float.parseFloat(customerCafVO.getCableCharge()));
			cafCharges.setCreatedBy(loginID);
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setTotalEmiCount(0);
			cafCharges.setComplEmiCount(0);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafCharges.setStbcafno(0l);
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::createExtraCableCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	@Transactional
	public void saveStbCafCharges(CustomerCafVO customerCafVO, Long stbCafNo, IPTVBoxVO iPTVBoxVO) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(customerCafVO.getCafNo());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			cafCharges.setParentCafno(customerCafVO.getCafNo());
			if (iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
				cafCharges.setChargeCode(ComsEnumCodes.IPTV_COST_CODE.getCode());
				cafCharges.setChargeAmount(Float.parseFloat(iPTVBoxVO.getStbPrice()));
				cafCharges.setTotalEmiCount(0);
			} else if (!iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
				cafCharges.setChargeCode(ComsEnumCodes.IPTV_INSTALLATION_CODE.getCode());
				cafCharges.setChargeAmount(Float.parseFloat(iPTVBoxVO.getStbEmiPrice()));
				cafCharges.setTotalEmiCount(Integer.parseInt(iPTVBoxVO.getStbInstallmentCount()));
				Integer emiStartMonth = DateUtill.getEmiDate(Calendar.getInstance().getTime());
				Integer emiEndMonth = DateUtill.getEmiMonthAndYear(Integer.parseInt(iPTVBoxVO.getStbInstallmentCount()) - 1);
				cafCharges.setEmiStartMonth(emiStartMonth);
				cafCharges.setEmiEndMonth(emiEndMonth);
			}
			cafCharges.setCreatedBy(customerCafVO.getLoginId());
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setComplEmiCount(0);
			cafCharges.setStbcafno(stbCafNo);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::saveStbCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	@Transactional
	public void updateCafCharges(PaymentVO paymentVO, String loginID) {
		try {
			cafChargesDao.updateCafCharges(paymentVO, loginID);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::updateCafCharges() " + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	@Transactional
	public void saveEntTaxCafCharges(CustomerCafVO customerCafVO, Long stbCafNo) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(customerCafVO.getCafNo());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			cafCharges.setParentCafno(customerCafVO.getCafNo());
			cafCharges.setChargeCode(ComsEnumCodes.CPE_ENTTAX_CODE.getCode());
			cafCharges.setChargeAmount(0.0f);
			cafCharges.setCreatedBy(customerCafVO.getLoginId());
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setTotalEmiCount(0);
			cafCharges.setComplEmiCount(0);
			cafCharges.setStbcafno(stbCafNo);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::createExtraCableCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
	}

	@Override
	@Transactional
	public void deleteCafChargesByCafNo(Long cafNo) {
		try {
			cafChargesDao.deleteCafChargesByCafNo(cafNo);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::deleteCafChargesByCafNo() " + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	@Transactional
	public void saveUpfrontStbCafCharges(CustomerCafVO customerCafVO, Long stbCafNo, IPTVBoxVO iPTVBoxVO) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(customerCafVO.getCafNo());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			cafCharges.setParentCafno(customerCafVO.getCafNo());
			cafCharges.setChargeCode(ComsEnumCodes.IPTV_UPFRONT_CODE.getCode());
			cafCharges.setChargeAmount(Float.parseFloat(iPTVBoxVO.getStbPrice()));
			cafCharges.setTotalEmiCount(0);
			cafCharges.setCreatedBy(customerCafVO.getLoginId());
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setComplEmiCount(0);
			cafCharges.setStbcafno(stbCafNo);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::saveUpfrontStbCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
		
	}

	@Override
	@Transactional
	public void saveUpfrontCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, long longValue) {
		CafCharges cafCharges = new CafCharges();
		try {
			cafCharges.setCafno(cafNo);
			cafCharges.setChargeDate(Calendar.getInstance().getTime());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafCharges.setPmntCustId(customerCafVO.getCustId().longValue());
			} else {
				cafCharges.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafCharges.setParentCafno(cafNo);
			cafCharges.setChargeCode(ComsEnumCodes.ONU_UPFRONT_CODE.getCode());
			cafCharges.setChargeAmount(Float.parseFloat(customerCafVO.getCpePrice()));
			cafCharges.setTotalEmiCount(0);
			cafCharges.setCreatedBy(loginID);
			cafCharges.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafCharges.setCreatedOn(Calendar.getInstance());
			cafCharges.setModifiedOn(Calendar.getInstance());
			cafCharges.setComplEmiCount(0);
			cafCharges.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafCharges.setStbcafno(0l);
			cafChargesDao.saveCafCharges(cafCharges);
		} catch (Exception e) {
			logger.error("CafChargesServiceImpl::saveUpfrontCafCharges() " + e);
			e.printStackTrace();
		} finally {
			cafCharges = null;
		}
		
	}

}
