/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.arbiva.apfgc.comsystem.util.DateUtill;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cafprods")
@IdClass(CafProductsPK.class)
public class CafProducts extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private Long cafno;
	
	@Id
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Id
	@Column(name = "prodcode")
	private String prodCode;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "rsagruid")
	private Long rsagruid;
	
	@Column(name = "minlockdays")
	private Integer minlockDays;
	
	@Column(name = "mspcode")
	private String mspCode;
	
	@Column(name = "expdate")
	private Date expDate;
	
	@Column(name = "priority")
	private int priority;
	
	@Column(name = "pmntcustid")
	private Long pmntCustId;
	
	@Transient
	private String prodName;
	
	@ManyToOne
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable=false, updatable=false)
	private Caf cafNumber;
	
	public CafProducts(){
		
	}
	public CafProducts(Caf caf, Object[] product) {
		
		this.setCafno(caf.getCafNo());
		this.setCreatedOn(Calendar.getInstance());
		this.setMinlockDays(Integer.parseInt(product[2].toString()));
		this.setModifiedOn(Calendar.getInstance());
		this.setPmntCustId(caf.getPmntCustId());
		this.setPriority(2);
		this.setProdCode(product[0].toString());
		this.setRsagruid(0L);
		this.setStatus(2);
		this.setTenantCode(product[3].toString());
	}

	public CafProducts(Long cafNo, Caf caf, OlIptvPkeSales olIptvPkeSales,  Products product, String rsAgrmtId) throws IOException, ParseException {
		this.setCafno(cafNo);
		this.setCreatedOn(Calendar.getInstance());
		this.setModifiedOn(Calendar.getInstance());
		this.setParentCafno(olIptvPkeSales.getAcctcafno());
		this.setPmntCustId(caf.getPmntCustId());
		this.setProdCode(product.getId().getProdcode());
		this.setTenantCode("APSFL");
		this.setStatus(2);
		this.setPriority(1);
		this.setMinlockDays(product.getLockInDays());
		this.setExpDate(product.getDurationDays() == 0 ? null : DateUtill.GetOneTimeServiceDate(product.getDurationDays()));
		this.setRsagruid(Long.parseLong(rsAgrmtId));
	}

	public Caf getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(Caf cafNumber) {
		this.cafNumber = cafNumber;
	}
	
	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}
	
	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public Integer getMinlockDays() {
		return minlockDays;
	}
	
	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Long getRsagruid() {
		return rsagruid;
	}

	public void setRsagruid(Long rsagruid) {
		this.rsagruid = rsagruid;
	}

	public void setMinlockDays(Integer minlockDays) {
		this.minlockDays = minlockDays;
	}
	
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Long getPmntCustId() {
		return pmntCustId;
	}

	public void setPmntCustId(Long pmntCustId) {
		this.pmntCustId = pmntCustId;
	}

	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
}
