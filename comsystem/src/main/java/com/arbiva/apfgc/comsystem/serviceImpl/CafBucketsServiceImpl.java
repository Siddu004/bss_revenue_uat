/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.comsystem.dao.impl.CafBucketsDao;
import com.arbiva.apfgc.comsystem.model.CafBuckets;

/**
 * @author Lakshman
 *
 */
@Service
public class CafBucketsServiceImpl {

	private static final Logger logger = Logger.getLogger(CafBucketsServiceImpl.class);

	@Autowired
	CafBucketsDao cafBucketsDao;

	public List<CafBuckets> findAllCafBuckets() {
		List<CafBuckets> cafBucketsList = new ArrayList<CafBuckets>();
		try {
			cafBucketsList = cafBucketsDao.findAllCafBuckets();
		} catch (Exception e) {
			logger.error("CafBucketsServiceImpl :: findAllCafBuckets()" + e);
			e.printStackTrace();
		} finally {

		}
		return cafBucketsList;
	}

	public CafBuckets saveCafBuckets(CafBuckets cafBuckets) {
		try {
			cafBuckets = cafBucketsDao.saveCafBuckets(cafBuckets);
		} catch (Exception e) {
			logger.error("CafBucketsServiceImpl :: saveCafBuckets()" + e);
			e.printStackTrace();
		} finally {

		}
		return cafBuckets;
	}
}
