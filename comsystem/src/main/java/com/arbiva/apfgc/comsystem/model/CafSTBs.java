/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "cafstbs")
public class CafSTBs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "stbcafno")
	private Long stbcafno;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "nwsubscode")
	private String nwSubsCode;
	
	@Column(name = "stbslno")
	private String stbSrlNo;

	@Column(name = "stbprofileid")
	private Long stbProfileId;

	@Column(name = "stbmacaddr")
	private String stbmacAddr;

	@Column(name = "stbipaddr")
	private String stbIpAddr;

	@Column(name = "stbleaseyn")
	private Character stbLeaseyn;

	@Column(name = "stbplace")
	private String stbPlace;

	@Column(name = "stbtenantcode")
	private String stbTenantcode;

	@Column(name = "stblockedmons")
	private int stbLockedmons;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "stblockeddate")
	private Date stbLockedDate;
	
	@Column(name = "stbchangehist")
	private String stbChangeHistory;
	
	@Column(name = "stbstatus", columnDefinition="tinyint(1)")
	private int stbStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDON")
	private Calendar createdOn;

	@Column(name = "CREATEDBY")
	private String createdBy;

	@Column(name = "CREATEDIPADDR")
	private String createdIPAddr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIEDON")
	private Calendar modifiedOn;

	@Column(name = "MODIFIEDBY")
	private String modifiedBy;

	@Column(name = "MODIFIEDIPADDR")
	private String modifiedIPAddr;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deactivatedon")
	private Calendar deactivatedOn;

	@Column(name = "deactivatedby")
	private String deactivatedBy;

	@Column(name = "deactivatedipaddr")
	private String deactivatedIpAddr;
	
	@ManyToOne
	@JoinColumn(name = "stbprofileid", referencedColumnName = "profile_id", nullable = false, insertable = false, updatable = false)
	private CpeModal stbModel;
	
	@ManyToOne
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable = false, updatable = false)
	private Caf cafNo;

	public CpeModal getStbModel() {
		return stbModel;
	}

	public void setStbModel(CpeModal stbModel) {
		this.stbModel = stbModel;
	}

	public Caf getCafNo() {
		return cafNo;
	}

	public void setCafNo(Caf cafNo) {
		this.cafNo = cafNo;
	}

	public Long getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(Long stbcafno) {
		this.stbcafno = stbcafno;
	}

	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}

	public String getNwSubsCode() {
		return nwSubsCode;
	}

	public void setNwSubsCode(String nwSubsCode) {
		this.nwSubsCode = nwSubsCode;
	}

	public String getStbSrlNo() {
		return stbSrlNo;
	}

	public void setStbSrlNo(String stbSrlNo) {
		this.stbSrlNo = stbSrlNo;
	}

	public Long getStbProfileId() {
		return stbProfileId;
	}

	public void setStbProfileId(Long stbProfileId) {
		this.stbProfileId = stbProfileId;
	}

	public String getStbmacAddr() {
		return stbmacAddr;
	}

	public void setStbmacAddr(String stbmacAddr) {
		this.stbmacAddr = stbmacAddr;
	}

	public String getStbIpAddr() {
		return stbIpAddr;
	}

	public void setStbIpAddr(String stbIpAddr) {
		this.stbIpAddr = stbIpAddr;
	}

	public Character getStbLeaseyn() {
		return stbLeaseyn;
	}

	public void setStbLeaseyn(Character stbLeaseyn) {
		this.stbLeaseyn = stbLeaseyn;
	}

	public String getStbPlace() {
		return stbPlace;
	}

	public void setStbPlace(String stbPlace) {
		this.stbPlace = stbPlace;
	}

	public String getStbTenantcode() {
		return stbTenantcode;
	}

	public void setStbTenantcode(String stbTenantcode) {
		this.stbTenantcode = stbTenantcode;
	}

	public int getStbLockedmons() {
		return stbLockedmons;
	}

	public void setStbLockedmons(int stbLockedmons) {
		this.stbLockedmons = stbLockedmons;
	}

	public Date getStbLockedDate() {
		return stbLockedDate;
	}

	public void setStbLockedDate(Date stbLockedDate) {
		this.stbLockedDate = stbLockedDate;
	}

	public String getStbChangeHistory() {
		return stbChangeHistory;
	}

	public void setStbChangeHistory(String stbChangeHistory) {
		this.stbChangeHistory = stbChangeHistory;
	}

	public int getStbStatus() {
		return stbStatus;
	}

	public void setStbStatus(int stbStatus) {
		this.stbStatus = stbStatus;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedIPAddr() {
		return createdIPAddr;
	}

	public void setCreatedIPAddr(String createdIPAddr) {
		this.createdIPAddr = createdIPAddr;
	}

	public Calendar getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Calendar modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddr() {
		return modifiedIPAddr;
	}

	public void setModifiedIPAddr(String modifiedIPAddr) {
		this.modifiedIPAddr = modifiedIPAddr;
	}

	public Calendar getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(Calendar deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpAddr() {
		return deactivatedIpAddr;
	}

	public void setDeactivatedIpAddr(String deactivatedIpAddr) {
		this.deactivatedIpAddr = deactivatedIpAddr;
	}
	
}
