/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="taxregionmapping")
@IdClass(TaxRegionMappingPK.class)
public class TaxRegionMapping extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "pincode")
	private String pinCode;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectiveFrom;
	
	@Column(name = "effectiveto")
	private Date effectiveTo;
	
	@Column(name = "regioncode")
	private String regionCode;

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	
}
