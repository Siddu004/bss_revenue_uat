/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafServicesDao;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafServicesServiceImpl implements CafServicesService {

	private static final Logger logger = Logger.getLogger(CafServicesServiceImpl.class);
	
	@Autowired
	CafServicesDao cafServicesDao;
	
	@Autowired
	CafService cafService;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Override
	@Transactional
	public void createCafServices(CustomerCafVO customerCafVO, Long cafNo, String loginID, String pmntCustId, Long stbCafNo, String stbPkgCode, Long addPkgCafNo) {
		CafServices cafServices = null;
		List<ProductsVO> selectedProdList = new ArrayList<>();
		List<AddtionalServicesVO> serviceList = new ArrayList<>();
		try {
			selectedProdList = customerCafVO.getProducts();
			for(ProductsVO products : selectedProdList) {
				serviceList = products.getServicesList();
				for(AddtionalServicesVO services : serviceList) {
					if(products.getProdType().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode())) {
						cafServices = new CafServices();
						cafServices.setCafno(customerCafVO.getFlag() != null && (customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()))? addPkgCafNo : cafNo);
						cafServices.setParentCafno(cafNo);
						cafServices.setCreatedBy(loginID);
						cafServices.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
						cafServices.setCreatedOn(Calendar.getInstance());
						cafServices.setMinlockDays(services.getLockInPeriod() == null ? 0 : services.getLockInPeriod());
						cafServices.setModifiedOn(Calendar.getInstance());
						cafServices.setTenantCode(products.getTenantcode());
						cafServices.setProdCode(products.getProdcode());
						cafServices.setSrvcCode(services.getServiceCode());
						cafServices.setStatus(!services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) && (customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode()))? ComsEnumCodes.Caf_Provision_status.getStatus() : ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
						cafServices.setFeatureCodes(services.getFeatureCodes());
						if(services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_VPN.getCode())) {
							cafServices.setVpnSrvcName(customerCafVO.getVpnService());
						}
						if(services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode())) {
							cafServices.setStbCafNo(stbCafNo);
						} else {
							cafServices.setStbCafNo(0l);
						}
						cafServices.setPmntCustId(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? customerCafVO.getCustId().longValue() : Long.parseLong(pmntCustId));
						cafServicesDao.saveCafServices(cafServices);
					} else if((products.getProdType().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) && services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode())) || (products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) && services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()))) {
						if((stbPkgCode != null && stbPkgCode.indexOf(products.getProdcode()) != -1)) {
							cafServices = new CafServices();
							if(products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
								Date oneTimeExpDate = DateUtill.GetOneTimeServiceDate(products.getOneTimeProdDuration());
								cafServices.setExpDate(oneTimeExpDate);
							}
							cafServices.setCafno(customerCafVO.getFlag() != null && (customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()))? addPkgCafNo : cafNo);
							cafServices.setParentCafno(cafNo);
							cafServices.setCreatedBy(loginID);
							cafServices.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
							cafServices.setCreatedOn(Calendar.getInstance());
							cafServices.setMinlockDays(services.getLockInPeriod() == null ? 0 : services.getLockInPeriod());
							cafServices.setModifiedOn(Calendar.getInstance());
							cafServices.setTenantCode(products.getTenantcode());
							cafServices.setProdCode(products.getProdcode());
							cafServices.setSrvcCode(services.getServiceCode());
							cafServices.setStatus(!services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) && (customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode()))? ComsEnumCodes.Caf_Provision_status.getStatus() : ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
							cafServices.setFeatureCodes(services.getFeatureCodes());
							cafServices.setStbCafNo(stbCafNo);
							cafServices.setPmntCustId(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? customerCafVO.getCustId().longValue() : Long.parseLong(pmntCustId));
							cafServicesDao.saveCafServices(cafServices);
						}
					} else {
						cafServices = new CafServices();
						if(products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
							Date oneTimeExpDate = DateUtill.GetOneTimeServiceDate(products.getOneTimeProdDuration());
							cafServices.setExpDate(oneTimeExpDate);
						}
						cafServices.setCafno(customerCafVO.getFlag() != null && (customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()))? addPkgCafNo : cafNo);
						cafServices.setParentCafno(cafNo);
						cafServices.setCreatedBy(loginID);
						cafServices.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
						cafServices.setCreatedOn(Calendar.getInstance());
						cafServices.setMinlockDays(services.getLockInPeriod() == null ? 0 : services.getLockInPeriod());
						cafServices.setModifiedOn(Calendar.getInstance());
						cafServices.setTenantCode(products.getTenantcode());
						cafServices.setProdCode(products.getProdcode());
						cafServices.setSrvcCode(services.getServiceCode());
						cafServices.setStatus(!services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) && (customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode()))? ComsEnumCodes.Caf_Provision_status.getStatus() : ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
						cafServices.setFeatureCodes(services.getFeatureCodes());
						if(services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_VPN.getCode())) {
							cafServices.setVpnSrvcName(customerCafVO.getVpnService());
						}
						cafServices.setPmntCustId(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? customerCafVO.getCustId().longValue() : Long.parseLong(pmntCustId));
						cafServices.setStbCafNo(0l);
						cafServicesDao.saveCafServices(cafServices);
					}
				}
			}
		} catch(Exception e) {
			logger.error("CafServicesServiceImpl::createCafServices() " + e);
			e.printStackTrace();
		} finally {
			selectedProdList = null;
			serviceList = null;
			cafServices = null;
		}
	}
	
	@Override
	public List<Object[]> getSrvcCodeAndCoreSrvcCodeByCafNo(Long cafNo) {
		List<Object[]> srvcAndCoreSrvcCodeList = new ArrayList<>();
		try {
			srvcAndCoreSrvcCodeList = cafServicesDao.getSrvcCodeAndCoreSrvcCodeByCafNo(cafNo);
		} catch (Exception e) {
			logger.error("CafServicesServiceImpl::getSrvcCodeAndCoreSrvcCodeByCafNo() " + e);
			e.printStackTrace();
		} finally {

		}
		return srvcAndCoreSrvcCodeList;
	}

	@Override
	public CafServices getCafServicesByCafnoAndSrvcCode(Long cafNo, String srvcCode) {
		CafServices cafServices = new CafServices();
		try {
			cafServices = cafServicesDao.getCafServicesByCafnoAndSrvcCode(cafNo, srvcCode);
		} catch (Exception e) {
			logger.error("CafServicesServiceImpl::getCafServicesByCafnoAndSrvcCode() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafServices;
	}

	@Override
	@Transactional
	public boolean updateCafServices(String prodCafNo, String prodCode, String loginId, String ipAddress) {
		boolean changePkgProvisionStatus = false;
		try {
			changePkgProvisionStatus = cafServicesDao.updateCafServices(prodCafNo, prodCode, loginId, ipAddress);
		} catch (Exception e) {
			logger.error("CafServicesServiceImpl::updateCafServices() " + e);
			e.printStackTrace();
		} finally {

		}
		return changePkgProvisionStatus;
	}

}
