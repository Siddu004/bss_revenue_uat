/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
public class CustomerCafBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private String cafNo;

	@Column(name = "depbal")
	private String depBal;

	@Column(name = "regbal")
	private String regBal;

	@Column(name = "lmocode")
	private String lmoCode;

	@Column(name = "custname")
	private String custName;

	@Column(name = "mname")
	private String mName;

	@Column(name = "lname")
	private String lName;

	@Column(name = "custid")
	private String custId;

	@Column(name = "cpeslno")
	private String cpeSrlNo;

	@Column(name = "status")
	private String status;

	@Column(name = "actdate")
	private String actdate;

	@Column(name = "aadharno")
	private String aadharNo;

	@Column(name = "apsfluniqueid")
	private String apsflUniqueId;

	@Transient
	private String tenantType;

	@Transient
	private String tenantCode;

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getApsflUniqueId() {
		return apsflUniqueId;
	}

	public void setApsflUniqueId(String apsflUniqueId) {
		this.apsflUniqueId = apsflUniqueId;
	}

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getDepBal() {
		return depBal;
	}

	public void setDepBal(String depBal) {
		this.depBal = depBal;
	}

	public String getRegBal() {
		return regBal;
	}

	public void setRegBal(String regBal) {
		this.regBal = regBal;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}
	
	public String getCpeSrlNo() {
		return cpeSrlNo;
	}

	public void setCpeSrlNo(String cpeSrlNo) {
		this.cpeSrlNo = cpeSrlNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActdate() {
		return actdate;
	}

	public void setActdate(String actdate) {
		this.actdate = actdate;
	}
}
