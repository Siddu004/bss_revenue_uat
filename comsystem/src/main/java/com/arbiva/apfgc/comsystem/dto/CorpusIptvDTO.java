package com.arbiva.apfgc.comsystem.dto;

import java.util.Map;

public class CorpusIptvDTO {
	
	private Map<String, Map<String, Object>> map;
	private String prodCafNo =null;
	
	private String cafNo ;
	
	private String prodCode ;
	
	private String stbCafNo ;
	
	private String nwSubsCode ;
	
	public String getCafNo() {
		return cafNo;
	}
	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getStbCafNo() {
		return stbCafNo;
	}
	public void setStbCafNo(String stbCafNo) {
		this.stbCafNo = stbCafNo;
	}
	public String getNwSubsCode() {
		return nwSubsCode;
	}
	public void setNwSubsCode(String nwSubsCode) {
		this.nwSubsCode = nwSubsCode;
	}
	public Map<String, Map<String, Object>> getMap() {
		return map;
	}
	public void setMap(Map<String, Map<String, Object>> map) {
		this.map = map;
	}
	public String getProdCafNo() {
		return prodCafNo;
	}
	public void setProdCafNo(String prodCafNo) {
		this.prodCafNo = prodCafNo;
	}
	
	

}
