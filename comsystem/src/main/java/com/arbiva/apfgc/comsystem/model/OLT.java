/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Arbiva
 *
 */
@Entity
@Table(name="oltmaster")
public class OLT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="pop_id")
	private Long popId;
	
	@Column(name="pop_name")
	private String popName;
	
	@Column(name="pop_type")
	private String popType;
	
	@Column(name="pop_olt_serialno")
	private String popOltSerialno;
	
	@Column(name="pop_olt_ipaddress")
	private String popOltIpaddress;
	
	@Column(name="pop_oltlabelno")
	private String pop_oltlabelno;
	
	@Column(name="oltaccessnode")
	private String oltAccessNode;
	
	@Column(name="status")
	private int status;
	
	@Column(name="pop_substnuid")
	private String popSubstnuid;
	
	public String getPop_oltlabelno() {
		return pop_oltlabelno;
	}

	public void setPop_oltlabelno(String pop_oltlabelno) {
		this.pop_oltlabelno = pop_oltlabelno;
	}

	@ManyToOne
	@JoinColumn(name = "pop_substnuid", referencedColumnName = "substnuid", nullable = false, insertable=false, updatable=false )
	private Substations substation;
	
	public String getPopSubstnuid() {
		return popSubstnuid;
	}

	public void setPopSubstnuid(String popSubstnuid) {
		this.popSubstnuid = popSubstnuid;
	}

	public Long getPopId() {
		return popId;
	}

	public void setPopId(Long popId) {
		this.popId = popId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Substations getSubstation() {
		return substation;
	}

	public void setSubstation(Substations substation) {
		this.substation = substation;
	}

	public String getPopName() {
		return popName;
	}

	public void setPopName(String popName) {
		this.popName = popName;
	}

	public String getPopType() {
		return popType;
	}

	public void setPopType(String popType) {
		this.popType = popType;
	}

	public String getPopOltSerialno() {
		return popOltSerialno;
	}

	public void setPopOltSerialno(String popOltSerialno) {
		this.popOltSerialno = popOltSerialno;
	}

	public String getPopOltIpaddress() {
		return popOltIpaddress;
	}

	public void setPopOltIpaddress(String popOltIpaddress) {
		this.popOltIpaddress = popOltIpaddress;
	}

	public String getOltAccessNode() {
		return oltAccessNode;
	}

	public void setOltAccessNode(String oltAccessNode) {
		this.oltAccessNode = oltAccessNode;
	}
}
