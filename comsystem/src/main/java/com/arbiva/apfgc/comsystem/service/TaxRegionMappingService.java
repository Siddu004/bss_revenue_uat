/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.model.ChargeCodes;
import com.arbiva.apfgc.comsystem.model.ChargeTaxes;
import com.arbiva.apfgc.comsystem.model.TaxMaster;
import com.arbiva.apfgc.comsystem.model.TaxRegionMapping;

/**
 * @author Lakshman
 *
 */
public interface TaxRegionMappingService {
	
	public abstract List<TaxRegionMapping> getAllTaxRegionMappingList();
	
	public abstract List<TaxMaster> getAllTaxMasterList();
	
	public abstract List<ChargeCodes> getAllChargeCodesList();
	
	public abstract List<ChargeTaxes> getAllChargeTaxesList();

}
