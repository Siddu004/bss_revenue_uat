/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lakshman
 *
 */
public class CorpusFingerPrint implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String module;
	
	private String command;
	
	private List<String> subscriberCodes;
	
	private DataMap dataMap;
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	
	public List<String> getSubscriberCodes() {
		return subscriberCodes;
	}
	public void setSubscriberCodes(List<String> subscriberCodes) {
		this.subscriberCodes = subscriberCodes;
	}
	public DataMap getDataMap() {
		return dataMap;
	}
	public void setDataMap(DataMap dataMap) {
		this.dataMap = dataMap;
	}
}
