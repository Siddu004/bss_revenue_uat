/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class BillPaymentBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pmntmodelov")
	private String pmntmode;

	@Column(name = "pmntamt")
	private String pmntamt;
	
	@Column(name = "pmntdate")
	private String pmntdate;

	public String getPmntmode() {
		return pmntmode;
	}

	public void setPmntmode(String pmntmode) {
		this.pmntmode = pmntmode;
	}

	public String getPmntamt() {
		return pmntamt;
	}

	public void setPmntamt(String pmntamt) {
		this.pmntamt = pmntamt;
	}

	public String getPmntdate() {
		return pmntdate;
	}

	public void setPmntdate(String pmntdate) {
		this.pmntdate = pmntdate;
	}
	
}
