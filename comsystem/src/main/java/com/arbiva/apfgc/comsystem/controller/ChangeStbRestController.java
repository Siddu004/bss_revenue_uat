package com.arbiva.apfgc.comsystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.comsystem.dto.ChangeOnuDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeOnuHelperDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeStbDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeStbHelperDTO;
import com.arbiva.apfgc.comsystem.dto.PublicIpAddressDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.serviceImpl.ChangeDeviceServiceImpl;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;

/**
 * @author Srinivas V
 * @since Feb 08 2017
 */
@RestController
public class ChangeStbRestController {
	
	private static final Logger LOGGER = Logger.getLogger(ChangeStbRestController.class);

	@Autowired
	ChangeDeviceServiceImpl cafDetails;
	
	@Autowired
	CafService cafService;
	
	@Autowired
	ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;

	/**
	 * Customers list based on aadhar Number
	 * @param aadhar
	 * @param tenantCode
	 * @return
	 */
	@RequestMapping(value = "/getCafsByAadharNo", method = RequestMethod.GET)
	public List<Object[]> getstbDetails(@RequestParam("aadharNo") String aadhar,
			@RequestParam("tenantCode") String tenantCode,
			@RequestParam("tenatType") String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		try {
			caflist = cafDetails.getCafList(aadhar, tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}
	
	@RequestMapping(value = "/getCafListByTrackId", method = RequestMethod.GET)
	public List<Object[]> getCafListByTrackId(@RequestParam("trackId") String trackId,
			@RequestParam("tenantCode") String tenantCode,
			@RequestParam("tenatType") String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		try {
			caflist = cafDetails.getCafListByTrackId(trackId, tenantCode,tenatType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * Customers list based on Mobile number
	 * @param mobileNo
	 * @param tenantCode
	 * @return
	 */
	@RequestMapping(value = "/getCafsByMobileNo", method = RequestMethod.GET)
	public List<Object[]> getCafsByMobileNo(@RequestParam("mobileNumber") String mobileNo,
			@RequestParam("tenantCode") String tenantCode,
			@RequestParam("tenatType") String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		try {
			caflist = cafDetails.getCafListWithMobileNO(mobileNo, tenantCode,tenatType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * Customer Details based on CAF number
	 * @param cafNumber
	 * @param tenantCode
	 * @return customer Details
	 */
	@RequestMapping(value = "/getCafsByCafNo", method = RequestMethod.GET)
	public List<Object[]> getCafsByCafNo(@RequestParam("cafNumber") String cafNumber,
			@RequestParam("tenantCode") String tenantCode,
			@RequestParam("tenatType") String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		try {
			caflist = cafDetails.getCafsByCafNo(cafNumber, tenantCode,tenatType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * Get Customers list based on Login 
	 * @param tenantCode
	 * @return --customer details
	 */
	@RequestMapping(value = "/getCafsByTenantCode", method = RequestMethod.GET)
	public List<Object[]> getCafsByTenantCode(@RequestParam("tenantCode") String tenantCode,
			@RequestParam("tenatType") String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		try {
			caflist = cafDetails.getCafsByTenantCode(tenantCode,tenatType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * Get the details of STB based on CAF number
	 * @param cafNum
	 * @return list of STB's if available
	 */
	@RequestMapping(value = "/getStbDetailsByCafNo", method = RequestMethod.GET)
	public ChangeStbHelperDTO getStbDetailsByCafNo(@RequestParam("cafNum") String cafNum) {

		ChangeStbHelperDTO cStbHelper = null;
		try {
			cStbHelper = cafDetails.getStbDetailsByCafNo(cafNum);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cStbHelper;
	}

	/**
	 * Update STB details with new STb details and change status of STB
	 * @param tenantCode
	 * @param changeStbDto
	 * @return Status
	 */
	@RequestMapping(value = "/updateStbMacNumber", method = RequestMethod.POST)
	public String updateStbMacNumber(@RequestParam("tenantCode") String tenantCode,
			@RequestBody ChangeStbDTO changeStbDto) {
		String statusMessage = "";
		statusMessage = cafDetails.updateStbMac(tenantCode, changeStbDto);
		return statusMessage;
	}
	
	/**
	 * new STB status like  available to LMO or LMo Partner MSO
	 * @param newMacAdd
	 * @param tenantCode
	 * @return status of STB details
	 */
	@RequestMapping(value = "/getNewStdStatus", method = RequestMethod.GET)
	public String  getNewStdStatus(@RequestParam("newStbserial") String newStbserial,
			@RequestParam("tenantCode") String tenantCode) {

		String statusmsg =null;
		try {
			statusmsg  = cafDetails.getNewStdStatus(newStbserial, tenantCode);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusmsg;
	}
	
	/* ONU Mac Address Change */

	/**
	 * Display customers based on aadhar number 
	 * @param changeOnuDto
	 * @return  List of Customers
	 */
	@RequestMapping(value = "/getCafsByAadharNumber", method = RequestMethod.POST)
	public List<Object[]> getCafDetails(@RequestBody ChangeOnuDTO changeOnuDto) {
		List<Object[]> caflist = new ArrayList<>();
		try {

			caflist = cafDetails.getCafList(changeOnuDto.getAadharNumberOnuChange(),
					changeOnuDto.getTenantCodeOnuChange(),changeOnuDto.getTenantType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;

	}

	/**
	 * Display Customer List based on Mobile number
	 * @param changeOnuDto
	 * @return lsit of  customers
	 */
	@RequestMapping(value = "/getCafsByMobileNumber", method = RequestMethod.POST)
	public List<Object[]> getOnuDetailsByMobile(@RequestBody ChangeOnuDTO changeOnuDto) {
		List<Object[]> caflist = new ArrayList<>();
		try {

			caflist = cafDetails.getCafListWithMobileNO(changeOnuDto.getMobileNumberOnuChange(),
					changeOnuDto.getTenantCodeOnuChange(),changeOnuDto.getTenantType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;

	}

	/**
	 * Display Customer details based on caf number
	 * @param changeOnuDto
	 * @return customer details
	 */
	@RequestMapping(value = "/getCafsByCafNumber", method = RequestMethod.POST)
	public List<Object[]> getOnuDetailsByCaf(@RequestBody ChangeOnuDTO changeOnuDto) {
		List<Object[]> caflist = new ArrayList<>();
		try {

			caflist = cafDetails.getCafsByCafNo(changeOnuDto.getCafNumberOnuChange(),
					changeOnuDto.getTenantCodeOnuChange(),changeOnuDto.getTenantType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;

	}

	/**
	 * List customers Based on login
	 * @param changeOnuDto
	 * @return customers list
	 */
	@RequestMapping(value = "/getCafsByTenant", method = RequestMethod.POST)
	public List<Object[]> getOnuDetailsByTenant(@RequestBody ChangeOnuDTO changeOnuDto) {
		List<Object[]> caflist = new ArrayList<>();
		try {

			caflist = cafDetails.getCafsByTenantCode(changeOnuDto.getTenantCodeOnuChange(),changeOnuDto.getTenantType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;

	}
	
	/**
	 * To get ONU details using CAF number
	 * @param cafNum
	 * @return -- ONU details
	 */
	@RequestMapping(value = "/getOnuDetailsByCafNo", method = RequestMethod.GET)
	public ChangeOnuHelperDTO getOnuDetailsByCafNo(@RequestParam("cafNumber") String cafNum) {
		ChangeOnuHelperDTO changeOnuHelperDTO = null;
		 
		try {
			changeOnuHelperDTO = cafDetails.getOnuDetailsByCafNo(cafNum);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return changeOnuHelperDTO;
	}
	
	/**
	 * THis method Updates ONU with New ONU serial and Mac Aaddress
	 * @param changeOnuDTO
	 * @return -- updated status message
	 */
	@RequestMapping(value = "/updateOnuChangeDetails", method = RequestMethod.POST)
	public String updateStbMacNumber(@RequestBody ChangeOnuDTO changeOnuDTO) {
		String statusMessage = "";
		try {
		statusMessage = cafDetails.updateOnuMac(changeOnuDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusMessage;
	}
	
	/**
	 * This  method is used to get Status Of new ONU serial valid for LMO or LMO partner MSO
	 * @param changeOnuDTO
	 * @return --Status Message
	 */
	@RequestMapping(value = "/getNewOnuStatus", method = RequestMethod.POST)
	public String getNewOnuStatus(@RequestBody ChangeOnuDTO changeOnuDTO) {

		String statusmsg = "";
		try {
			statusmsg = cafDetails.getNewOnuStatus(changeOnuDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusmsg;
	}
	
	/**
	 * This  method is used to get Terminate the CAFs
	 * @param cafs List 
	 * @return --Status Message
	 */
	@RequestMapping(value = "/terminateCafs")
	public  String terminateCafs(@RequestParam(value="cafsList") String[] cafs){
		
		String statusMessage = "";
		try{
			for (String cafNo : cafs){
				provisioningBusinessServiceImpl.processDeleteRequests(cafNo);
				Caf caf = cafService.findByCafNo(Long.parseLong(cafNo));
				caf.setStatus(81);
				cafService.save(caf);
				cafDetails.releaseIpAddress(cafNo);
			}
				
			statusMessage = "Success";
		}catch(Exception e){
			statusMessage = e.getMessage();
		}
		return statusMessage;
	}
	
	/**
	 * This  method is used to resource release
	 * @param cafs List 
	 * @return --Status Message
	 */
	@RequestMapping(value = "/resourceRelease")
	public  String resourceRelease(){
		
		try{
			cafDetails.resourceRelease();
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		
		return "";
	}
	 

	/**
	 * Display Customer details based on caf number
	 * @param PublicIpAddressDTO
	 * @return customer details
	 */
	@RequestMapping(value = "/getCafByCaf", method = RequestMethod.POST)
	public List<Object[]> getCafByCaf(@RequestBody PublicIpAddressDTO publicIpAddressDTO) {
		List<Object[]> caflist = new ArrayList<>();
		try {

			caflist = cafDetails.getCafsByCafNo(publicIpAddressDTO.getCafNum(),
					publicIpAddressDTO.getTenantCode(),publicIpAddressDTO.getTenantType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;

	}
	
	/**
	 * @param publicIpAddressDTO
	 * @return
	 */
	@RequestMapping(value="/getCafsListPuIP", method= RequestMethod.POST)
	public List<Object[]> getCafsListPuIP(@RequestBody PublicIpAddressDTO publicIpAddressDTO)
	{
		List<Object[]> cafList = new ArrayList<>();
		try
		{
			cafList=cafDetails.getCafsListPuIP(publicIpAddressDTO);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cafList;
		
	}
	
	@RequestMapping(value="/getIpAddress", method=RequestMethod.POST)
	public List<Object[]> getIpAddress(@RequestBody PublicIpAddressDTO publicIpAddressDTO)
	{
		List<Object[]> ipAddresss = new ArrayList<>();
		try
		{
			ipAddresss=cafDetails.getIpAddress(publicIpAddressDTO);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return ipAddresss;
	}

	@RequestMapping(value="/updateIpAddress", method=RequestMethod.POST)
	public String updateIpAddress(@RequestBody PublicIpAddressDTO publicIpAddressDTo){
		
		String statusmsg="";
		try
		{
			statusmsg=cafDetails.updateIpAddress(publicIpAddressDTo);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return statusmsg;
	}
}