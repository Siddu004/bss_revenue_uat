/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class OLTPortDetailsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String popOltSerialno;
	
	private int cardId;
	
	private Integer portNo;

	public String getPopOltSerialno() {
		return popOltSerialno;
	}

	public void setPopOltSerialno(String popOltSerialno) {
		this.popOltSerialno = popOltSerialno;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Integer getPortNo() {
		return portNo;
	}

	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cardId;
		result = prime * result + ((popOltSerialno == null) ? 0 : popOltSerialno.hashCode());
		result = prime * result + ((portNo == null) ? 0 : portNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OLTPortDetailsPK other = (OLTPortDetailsPK) obj;
		if (cardId != other.cardId)
			return false;
		if (popOltSerialno == null) {
			if (other.popOltSerialno != null)
				return false;
		} else if (!popOltSerialno.equals(other.popOltSerialno))
			return false;
		if (portNo == null) {
			if (other.portNo != null)
				return false;
		} else if (!portNo.equals(other.portNo))
			return false;
		return true;
	}
	
}
