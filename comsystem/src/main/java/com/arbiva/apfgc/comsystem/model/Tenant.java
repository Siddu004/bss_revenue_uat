package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tenants")
public class Tenant implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Tenant() {
		
	}

	@Id
	@Column(name = "tenantId", unique = true, nullable = false)
	private Integer tenantId;
	
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Column(name = "tenantname")
	private String name;
	
	@Column(name = "aadharcardno")
	private String aadharCardNo;
	
	@Column(name = "tenanttypelov")
	private String tenantTypeLov;
	
	@Column(name = "panno")
	private String panNo;
	
	@Column(name = "tanno")
	private String tanNo;
	
	@Column(name = "tinno")
	private String tinNo;
	
	@Column(name = "gstno")
	private String gstNo;
	
	@Column(name = "vatno")
	private String vatNo;
	
	@Column(name = "regoff_addr1")
	private String address1;
	
	@Column(name = "regoff_addr2")
	private String address2;
	
	@Column(name = "regoff_locality")
	private String locality;
	
	@Column(name = "regoff_area")
	private String area;
	
	@Column(name = "regoff_city")
	private String city;
	
	@Column(name = "regoff_state")
	private String stateName;
	
	@Column(name = "regoff_pin")
	private String pincode;
	
	@Column(name = "regoff_stdcode")
	private String stdcode;
	
	@Column(name = "regoff_landline1")
	private String landline1;
	
	@Column(name = "regoff_landline2")
	private String landline2;
	
	@Column(name = "regoff_email1")
	private String emailId1;
	
	@Column(name = "regoff_email2")
	private String emailId2;
	
	@Column(name = "regoff_fax1")
	private String fax1;
	
	@Column(name = "regoff_fax2")
	private String fax2;
	
	@Column(name = "regoff_pocname")
	private String pocName;
	
	@Column(name = "regoff_pocmob1")
	private String pocMobileNo1;
	
	@Column(name = "regoff_pocmob2")
	private String pocMobileNo2;
	
	@Column(name = "locoff_addr1")
	private String localOfficeAddress1;
	
	@Column(name = "locoff_addr2")
	private String localOfficeAddress2;
	
	@Column(name = "locoff_locality")
	private String localOfficeLocality;
	
	@Column(name = "locoff_area")
	private String localOfficeArea;
	
	@Column(name ="locoff_city")
	private String localOfficeCity;
	
	@Column(name = "locoff_state")
	private String localOfficeStateName;
	
	@Column(name = "locoff_pin")
	private String localOfficePincode;
	
	@Column(name = "locoff_stdcode")
	private String localOfficestdcode;
	
	@Column(name = "locoff_landline1")
	private String localOfficeLandline1;
	
	@Column(name = "locoff_landline2")
	private String localOfficeLandline2;
	
	@Column(name = "locoff_email1")
	private String localOfficeEmailId1;
	
	@Column(name = "locoff_email2")
	private String localOfficeEmailId2;
	
	@Column(name = "locoff_fax1")
	private String localOfficeFax1;
	
	@Column(name = "locoff_fax2")
	private String localOfficeFax2;
	
	@Column(name = "locoff_pocname")
	private String localOfficePocName;
	
	@Column(name = "locoff_pocmob1")
	private String localOfficePocMobileNo1;
	
	@Column(name = "locoff_pocmob2")
	private String localOfficePocMobileNo2;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private String modifiedBy;
	
	private @Column(name = "modifiedipaddr")
	String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private Calendar deactivatedDate;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpAddress;
	
	/*@OneToMany(targetEntity=TenantLmoMspAgreement.class, mappedBy="tenantMsp", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<TenantLmoMspAgreement> tenantLmoMspAgreementsMsp;
	
	@OneToMany(targetEntity=TenantLmoMspAgreement.class, mappedBy="tenantLmo", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<TenantLmoMspAgreement> tenantLmoMspAgreementsLmo;

	public List<TenantLmoMspAgreement> getTenantLmoMspAgreementsMsp() {
		return tenantLmoMspAgreementsMsp;
	}

	public void setTenantLmoMspAgreementsMsp(List<TenantLmoMspAgreement> tenantLmoMspAgreementsMsp) {
		this.tenantLmoMspAgreementsMsp = tenantLmoMspAgreementsMsp;
	}

	public List<TenantLmoMspAgreement> getTenantLmoMspAgreementsLmo() {
		return tenantLmoMspAgreementsLmo;
	}

	public void setTenantLmoMspAgreementsLmo(List<TenantLmoMspAgreement> tenantLmoMspAgreementsLmo) {
		this.tenantLmoMspAgreementsLmo = tenantLmoMspAgreementsLmo;
	}*/

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getTenantTypeLov() {
		return tenantTypeLov;
	}

	public void setTenantTypeLov(String tenantTypeLov) {
		this.tenantTypeLov = tenantTypeLov;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getTanNo() {
		return tanNo;
	}

	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}

	public String getTinNo() {
		return tinNo;
	}

	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getVatNo() {
		return vatNo;
	}

	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}


	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getLocalOfficeCity() {
		return localOfficeCity;
	}

	public void setLocalOfficeCity(String localOfficeCity) {
		this.localOfficeCity = localOfficeCity;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getStdcode() {
		return stdcode;
	}

	public void setStdcode(String stdcode) {
		this.stdcode = stdcode;
	}

	public String getLandline1() {
		return landline1;
	}

	public void setLandline1(String landline1) {
		this.landline1 = landline1;
	}

	public String getLandline2() {
		return landline2;
	}

	public void setLandline2(String landline2) {
		this.landline2 = landline2;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getEmailId2() {
		return emailId2;
	}

	public void setEmailId2(String emailId2) {
		this.emailId2 = emailId2;
	}

	public String getFax1() {
		return fax1;
	}

	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}

	public String getFax2() {
		return fax2;
	}

	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getPocMobileNo1() {
		return pocMobileNo1;
	}

	public void setPocMobileNo1(String pocMobileNo1) {
		this.pocMobileNo1 = pocMobileNo1;
	}

	public String getPocMobileNo2() {
		return pocMobileNo2;
	}

	public void setPocMobileNo2(String pocMobileNo2) {
		this.pocMobileNo2 = pocMobileNo2;
	}

	public String getLocalOfficeAddress1() {
		return localOfficeAddress1;
	}

	public void setLocalOfficeAddress1(String localOfficeAddress1) {
		this.localOfficeAddress1 = localOfficeAddress1;
	}

	public String getLocalOfficeAddress2() {
		return localOfficeAddress2;
	}

	public void setLocalOfficeAddress2(String localOfficeAddress2) {
		this.localOfficeAddress2 = localOfficeAddress2;
	}

	public String getLocalOfficeLocality() {
		return localOfficeLocality;
	}

	public void setLocalOfficeLocality(String localOfficeLocality) {
		this.localOfficeLocality = localOfficeLocality;
	}

	public String getLocalOfficeArea() {
		return localOfficeArea;
	}

	public void setLocalOfficeArea(String localOfficeArea) {
		this.localOfficeArea = localOfficeArea;
	}

	public String getLocalOfficePincode() {
		return localOfficePincode;
	}

	public void setLocalOfficePincode(String localOfficePincode) {
		this.localOfficePincode = localOfficePincode;
	}

	public String getLocalOfficestdcode() {
		return localOfficestdcode;
	}

	public void setLocalOfficestdcode(String localOfficestdcode) {
		this.localOfficestdcode = localOfficestdcode;
	}

	public String getLocalOfficeLandline1() {
		return localOfficeLandline1;
	}

	public void setLocalOfficeLandline1(String localOfficeLandline1) {
		this.localOfficeLandline1 = localOfficeLandline1;
	}

	public String getLocalOfficeLandline2() {
		return localOfficeLandline2;
	}

	public void setLocalOfficeLandline2(String localOfficeLandline2) {
		this.localOfficeLandline2 = localOfficeLandline2;
	}

	public String getLocalOfficeEmailId1() {
		return localOfficeEmailId1;
	}

	public void setLocalOfficeEmailId1(String localOfficeEmailId1) {
		this.localOfficeEmailId1 = localOfficeEmailId1;
	}

	public String getLocalOfficeEmailId2() {
		return localOfficeEmailId2;
	}

	public void setLocalOfficeEmailId2(String localOfficeEmailId2) {
		this.localOfficeEmailId2 = localOfficeEmailId2;
	}

	public String getLocalOfficeFax1() {
		return localOfficeFax1;
	}

	public void setLocalOfficeFax1(String localOfficeFax1) {
		this.localOfficeFax1 = localOfficeFax1;
	}

	public String getLocalOfficeFax2() {
		return localOfficeFax2;
	}

	public void setLocalOfficeFax2(String localOfficeFax2) {
		this.localOfficeFax2 = localOfficeFax2;
	}

	public String getLocalOfficePocName() {
		return localOfficePocName;
	}

	public void setLocalOfficePocName(String localOfficePocName) {
		this.localOfficePocName = localOfficePocName;
	}

	public String getLocalOfficePocMobileNo1() {
		return localOfficePocMobileNo1;
	}

	public void setLocalOfficePocMobileNo1(String localOfficePocMobileNo1) {
		this.localOfficePocMobileNo1 = localOfficePocMobileNo1;
	}

	public String getLocalOfficePocMobileNo2() {
		return localOfficePocMobileNo2;
	}

	public void setLocalOfficePocMobileNo2(String localOfficePocMobileNo2) {
		this.localOfficePocMobileNo2 = localOfficePocMobileNo2;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getLocalOfficeStateName() {
		return localOfficeStateName;
	}

	public void setLocalOfficeStateName(String localOfficeStateName) {
		this.localOfficeStateName = localOfficeStateName;
	}
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Calendar getDeactivatedDate() {
		return deactivatedDate;
	}
	public void setDeactivatedDate(Calendar deactivatedDate) {
		this.deactivatedDate = deactivatedDate;
	}
	public String getDeactivatedBy() {
		return deactivatedBy;
	}
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}
	public String getDeactivatedIpAddress() {
		return deactivatedIpAddress;
	}
	public void setDeactivatedIpAddress(String deactivatedIpAddress) {
		this.deactivatedIpAddress = deactivatedIpAddress;
	}
	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

}
