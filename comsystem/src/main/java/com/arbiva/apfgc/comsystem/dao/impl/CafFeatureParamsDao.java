/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafFeatureParams;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Repository
public class CafFeatureParamsDao {
	
	private static final Logger LOGGER = Logger.getLogger(CafFeatureParamsDao.class);

	@Autowired
	HttpServletRequest httpServletRequest;
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public CafFeatureParams saveCafFeatureParams(CafFeatureParams cafFeatureParams) {
		return getEntityManager().merge(cafFeatureParams);
	}
	
	public CafFeatureParams getCafFeatureParamsByValues(Long cafNo, String srvcCode, String featureCode, String paramCode, Date effectiveFrom) {
		CafFeatureParams cafFeatureParams = new CafFeatureParams();
		StringBuilder builder = new StringBuilder(" FROM ").append(CafFeatureParams.class.getSimpleName()).append(" WHERE cafNo=:cafNo and srvccode=:srvccode and featurecode=:featurecode and prmcode=:prmcode and effectivefrom=:effectivefrom ");
		TypedQuery<CafFeatureParams> query = null;
			try {
				LOGGER.info("START::findByCustomerCode()");
				query = getEntityManager().createQuery(builder.toString(), CafFeatureParams.class);
				query.setParameter("cafNo", cafNo);
				query.setParameter("srvcCode", srvcCode);
				query.setParameter("featureCode", featureCode);
				query.setParameter("paramCode", paramCode);
				query.setParameter("effectiveFrom", effectiveFrom);
				cafFeatureParams = query.getSingleResult();
				LOGGER.info("END::findByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByCustomerCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cafFeatureParams;
	}

	public void updateCafFeatureParams(PaymentVO paymentVO, String loginID) {
		try {
			String modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			String ipAddress = null;
			StringBuilder builder = null;
			Query query = null;
			if(paymentVO.getIpAddress() != null) {
				ipAddress = paymentVO.getIpAddress();
			} else {
				ipAddress = httpServletRequest.getRemoteAddr();
			}
			if(paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
				builder = new StringBuilder("update caffeatureprms set status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			} else {
				builder = new StringBuilder("update caffeatureprms set status = "+ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			}
			
			try {
				query = getEntityManager() .createNativeQuery(builder.toString());
				query.executeUpdate();
			} catch(Exception ex){
				LOGGER.error("EXCEPTION::updateCafFeatureParams() " + ex);
			}finally{
				query = null;
				builder = null;
				ipAddress = null;
			}
		} catch(Exception e) {
			LOGGER.error("EXCEPTION::updateCafFeatureParams() " + e);
			e.printStackTrace();
		}
	}
}
