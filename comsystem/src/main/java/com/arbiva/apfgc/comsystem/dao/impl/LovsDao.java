/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.Lovs;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;

/**
 * @author Lakshman
 *
 */
@Component("lovsDao")
public class LovsDao {

	private static final Logger LOGGER = Logger.getLogger(LovsDao.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<Lovs> getLovValuesByLovName(String lovName) {
		List<Lovs> lovs = new ArrayList<Lovs>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Lovs.class.getSimpleName()).append(" WHERE lovname =:lovName");
		TypedQuery<Lovs> query = null;
		try {
			LOGGER.info("START::findEnterpriseTypeByLov()");
			query = getEntityManager().createQuery(builder.toString(), Lovs.class);
			query.setParameter("lovName", lovName);
			lovs = query.getResultList();
			LOGGER.info("END::findEnterpriseTypeByLov()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findEnterpriseTypeByLov() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return lovs;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getTenantTypeBySI(String TenantType) {
		List<Object[]> TenantTypeList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT tenantcode, tenantname FROM tenants WHERE tenanttypelov = '"+TenantType+"' ");
		Query query = null;
		try {
			LOGGER.info("START::findEnterpriseTypeByLov()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			TenantTypeList = query.getResultList();
			LOGGER.info("END::findEnterpriseTypeByLov()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findEnterpriseTypeByLov() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return TenantTypeList;
	}
	
	public boolean CheckAPSFLCode(String ApsflCode) {
		StringBuilder builder = new StringBuilder("SELECT * FROM cafs WHERE apsfluniqueid = '"+ApsflCode+"' ");
		Query query = null;
		boolean status = false;
		try {
			LOGGER.info("START::findEnterpriseTypeByLov()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			if(query.getResultList().isEmpty()) {
				status = true;
			}
			LOGGER.info("END::findEnterpriseTypeByLov()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findEnterpriseTypeByLov() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}
	
	public List<Lovs> findAllLovs() {
		 List<Lovs> lovs = new ArrayList<Lovs>();
		  StringBuilder builder = new StringBuilder(" FROM ").append(Lovs.class.getSimpleName());
		  TypedQuery<Lovs> query = null;
			try {
				LOGGER.info("START::findAllLovs()");
				query = getEntityManager().createQuery(builder.toString(), Lovs.class);
				lovs = query.getResultList();
				LOGGER.info("END::findAllLovs()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findAllLovs() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return lovs;
	}

	public boolean checkLmocodeValidation(String lmocode) {
		StringBuilder builder = new StringBuilder("SELECT tenantcode FROM tenants WHERE tenantcode = '"+lmocode+"' and tenanttypelov = '"+ComsEnumCodes.SI_Tenant_Type.getCode()+"' ");
		Query query = null;
		boolean status = false;
		try {
			LOGGER.info("START::checkLmocodeValidation()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			if(!query.getResultList().isEmpty()) {
				status = true;
			}
			LOGGER.info("END::checkLmocodeValidation()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::checkLmocodeValidation() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}

	public List<Districts> getDistrictListByDistrictName(String district) {
		List<Districts> DistrictList = new ArrayList<>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Districts.class.getSimpleName()).append(" WHERE districtname =:district");
		TypedQuery<Districts> query = null;
		try {
			LOGGER.info("START::getDistrictListByDistrictName()");
			query = getEntityManager().createQuery(builder.toString(), Districts.class);
			query.setParameter("district", district);
			DistrictList = query.getResultList();
			LOGGER.info("END::getDistrictListByDistrictName()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getDistrictListByDistrictName() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return DistrictList;
	}

	public List<Mandals> getmandalListByDistrictIdAndMandalName(Integer districtUid, String mandal) {
		List<Mandals> MandalList = new ArrayList<>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Mandals.class.getSimpleName()).append(" WHERE districtuid =:district and mandalname =:mandal ");
		TypedQuery<Mandals> query = null;
		try {
			LOGGER.info("START::getmandalListByDistrictIdAndMandalName()");
			query = getEntityManager().createQuery(builder.toString(), Mandals.class);
			query.setParameter("district", districtUid);
			query.setParameter("mandal", mandal);
			MandalList = query.getResultList();
			LOGGER.info("END::getmandalListByDistrictIdAndMandalName()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getmandalListByDistrictIdAndMandalName() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return MandalList;
	}

	public String getDistrictsByTenantCode(String tenantCode) {
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String districts = "";
		try {
			LOGGER.info("START::getDistrictsByTenantCode()");
			builder.append(" select configval from configitems where configname = '"+tenantCode+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			districts = query.getSingleResult().toString();
			LOGGER.info("END::getDistrictsByTenantCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getDistrictsByTenantCode() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return districts;
	}

	public String getMobileVersion(String versionName) {
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String version = "";
		try {
			LOGGER.info("START::getMobileVersion()");
			builder.append(" select configval from configitems where configname = '"+versionName+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			version = query.getSingleResult().toString();
			LOGGER.info("END::getMobileVersion()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getMobileVersion() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return version;
	}
}
