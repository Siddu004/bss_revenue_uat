/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.model.TenantWallet;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Component("tenantWalletDao")
public class TenantWalletDaoImpl {

	@Autowired
	HttpServletRequest httpServletRequest;
	
	private static final Logger LOGGER = Logger.getLogger(TenantWalletDaoImpl.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public TenantWallet findByLmoCode(String lmoCode) {
		TenantWallet tenantWallet =new TenantWallet();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantWallet.class.getSimpleName()).append(" WHERE tenantcode=:lmoCode");
		TypedQuery<TenantWallet> query = null;
			try {
				LOGGER.info("START::findByLmoCode()");
				query = getEntityManager().createQuery(builder.toString(), TenantWallet.class);
				query.setParameter("lmoCode", lmoCode);
				tenantWallet = query.getSingleResult();
				LOGGER.info("END::findByLmoCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByLmoCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return tenantWallet;
	}
	
	public void updateTenantWallet(TenantWallet tenantWallet) {
		getEntityManager().merge(tenantWallet);
	}

	@SuppressWarnings("unchecked")
	public List<String> getLMOsubstations(String tenantCode) {
		 List<String> subStnList = new ArrayList<>();
		  StringBuilder builder =  new StringBuilder ("select substnuid FROM tenantbusareas WHERE tenantcode = '"+tenantCode+"'");
		  Query query = null;
		  try {
				 query = getEntityManager().createNativeQuery(builder.toString());
				 subStnList = query.getResultList();
				} catch (Exception e) {
					LOGGER.error("EXCEPTION::viewAllPkgAgree() " + e);
				}finally{
					query = null;
					builder = null;
				}		 
		  return subStnList;
	}

	@Transactional
	public void updateCustinv(PaymentVO paymentVO) {
		// TODO Auto-generated method stub
		 Query query = null;
		try{
			LOGGER.info("START::getCard()");
			query=getEntityManager().createNativeQuery("update custinv set prevpaidamt=1 where    pmntcustid='"+paymentVO.getCustId()+"' and invtdate<='"+paymentVO.getDdDate()+"'");		
			int result = query.executeUpdate();
			
			query=getEntityManager().createNativeQuery("update custinv inv1,(select prevpymtreceived,  pmntcustid,invtdate,YEAR(invtdate) ,Month(invtdate),YEAR(invtdate-INTERVAL 1 MONTH)) prevyr,MONTH(invtdate-INTERVAL 1 MONTH))prevmn from custinv where pmntcustid='"+paymentVO.getCustId()+"' and invtdate='"+paymentVO.getDdDate()+"')inv2 "
			+"set prevpymtreceived="+paymentVO.getPaidAmount()+"+"+"prevpymtreceived "
					+" where   invtdate>'"+paymentVO.getDdDate()+"'");	
			query=getEntityManager().createNativeQuery("	UPDATE custinv  SET  prevpymtreceived ="+paymentVO.getPaidAmount()+" prevpymtreceived "  + "prevpymtreceived WHERE invtdate >'"+paymentVO.getDdDate()+"' and pmntcustid='"+paymentVO.getCustId()+"'");
			
			result=query.executeUpdate();
			
			
		}catch(Exception e){
			LOGGER.info("Error in::getCard()");
		}
		
	}

	
	
}
