/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.LovsDao;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.Lovs;
import com.arbiva.apfgc.comsystem.model.Mandals;

/**
 * @author Lakshman
 *
 */
@Service("lovsService")
@Transactional
public class LovsServiceImpl {

	private static final Logger logger = Logger.getLogger(LovsServiceImpl.class);

	@Autowired
	LovsDao lovsDao;

	public List<Lovs> findAllLovs() {
		List<Lovs> lovsList = new ArrayList<Lovs>();
		try {
			lovsList = lovsDao.findAllLovs();
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: findAllLovs()" + e);
			e.printStackTrace();
		} finally {

		}
		return lovsList;
	}

	public List<Lovs> getLovValuesByLovName(String lovName) {
		List<Lovs> lovsList = new ArrayList<Lovs>();
		try {
			lovsList = lovsDao.getLovValuesByLovName(lovName);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getLovValuesByLovName()" + e);
			e.printStackTrace();
		} finally {

		}
		return lovsList;
	}
	
	public List<Object[]> getTenantTypeBySI(String tenantType) {
		List<Object[]> TenantTypeList = new ArrayList<>();
		try {
			TenantTypeList = lovsDao.getTenantTypeBySI(tenantType);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getLovValuesByLovName()" + e);
			e.printStackTrace();
		} finally {

		}
		return TenantTypeList;
	}
	
	public boolean CheckAPSFLCode(String ApsflCode) {
		boolean status = false;
		try {
			status = lovsDao.CheckAPSFLCode(ApsflCode);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getLovValuesByLovName()" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	public boolean checkLmocodeValidation(String lmocode) {
		boolean status = false;
		try {
			status = lovsDao.checkLmocodeValidation(lmocode);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: checkLmocodeValidation()" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	public List<Districts> getDistrictListByDistrictName(String district) {
		List<Districts> DistrictList = new ArrayList<>();
		try {
			DistrictList = lovsDao.getDistrictListByDistrictName(district);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getDistrictListByDistrictName()" + e);
			e.printStackTrace();
		} finally {

		}
		return DistrictList;
	}

	public List<Mandals> getmandalListByDistrictIdAndMandalName(Integer districtUid, String mandal) {
		List<Mandals> MandalList = new ArrayList<>();
		try {
			MandalList = lovsDao.getmandalListByDistrictIdAndMandalName(districtUid, mandal);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getmandalListByDistrictIdAndMandalName()" + e);
			e.printStackTrace();
		} finally {

		}
		return MandalList;
	}

	public String getDistrictsByTenantCode(String tenantCode) {
		String districts = "";
		try {
			districts = lovsDao.getDistrictsByTenantCode(tenantCode);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getDistrictsByTenantCode()" + e);
			e.printStackTrace();
		} finally {

		}
		return districts;
	}

	public String getMobileVersion(String versionName) {
		String version = "";
		try {
			version = lovsDao.getMobileVersion(versionName);
		} catch (Exception e) {
			logger.error("LovsServiceImpl :: getMobileVersion()" + e);
			e.printStackTrace();
		} finally {

		}
		return version;
	}
}
