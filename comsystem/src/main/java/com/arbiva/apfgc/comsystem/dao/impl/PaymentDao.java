/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.model.OLPayment;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;


/**
 * @author Lakshman
 *
 */
@Repository
public class PaymentDao {
	
	private EntityManager em;
	private static final Logger LOGGER = Logger.getLogger(PaymentDao.class);

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deletePayments(OLPayment oLPayment) {
		getEntityManager().remove(oLPayment);
		getEntityManager().flush();
	}

	public OLPayment findByPaymentNo(Long pmntId) {
		return getEntityManager().find(OLPayment.class,pmntId);
	}

	@SuppressWarnings("unchecked")
	public List<OLPayment> findAllPayments() {
		return (List<OLPayment>)getEntityManager().createQuery("Select payment from " + OLPayment.class.getSimpleName() + " payment").getResultList();
	}

	public void savePayment(OLPayment oLPayment) {
		getEntityManager().merge(oLPayment);
	}

	public void createPayment(OLPayment oLPayment) {
		getEntityManager().merge(oLPayment);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getRecentPayment(Long cafNo) {
		List<Object[]> paymentList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select pmntamt, pmntmodelov, pmntrefno, DATE_FORMAT(pmntdate,'%d/%m/%Y') from payments where acctcafno = "+cafNo+" order by createdon desc ");
			try {
				LOGGER.info("START::getRecentPayment()");
				query = getEntityManager() .createNativeQuery(builder.toString());
				paymentList = query.getResultList();
				LOGGER.info("END::getRecentPayment()");
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				query = null;
				builder = null;
			}
		  return paymentList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getTaxAmounts(String srvcCode, String chrgCode, String region, String taxLevelFlag) {
		List<Object[]> taxsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("START::getTaxAmounts()");
				if(taxLevelFlag.equalsIgnoreCase("1")) {
					builder.append("SELECT CASE WHEN txm.perc_or_abs =  'P' THEN txm.taxperc ELSE 0 END as taxper, "); 
					builder.append(" CASE WHEN txm.perc_or_abs <> 'P' THEN txm.taxperc ELSE 0 END as absval, txm.taxcode FROM srvcs s, "); 
					builder.append(" srvctaxes st , taxmast txm WHERE s.srvccode='"+srvcCode+"' AND s.status = 1 AND CURRENT_DATE() BETWEEN s.effectivefrom ");
					builder.append(" AND s.effectiveto AND s.coresrvccode = st.coresrvccode AND st.status=1 ");
					builder.append(" AND st.effectivefrom = (SELECT MAX(st1.effectivefrom) FROM srvctaxes st1 "); 
					builder.append(" WHERE st1.coresrvccode=st.coresrvccode AND st1.taxcode=st.taxcode AND st1.effectivefrom <= CURRENT_DATE()) ");
					builder.append(" AND st.taxcode = txm.taxcode AND txm.regioncode = '"+region+"'  AND txm.status = 1 ");
					builder.append(" AND CURRENT_DATE() BETWEEN txm.effectivefrom AND txm.effectiveto ");
				} else if(taxLevelFlag.equalsIgnoreCase("2")) {
					builder.append("SELECT CASE WHEN txm.perc_or_abs =  'P' THEN txm.taxperc ELSE 0 END as taxper, ");
					builder.append(" CASE WHEN txm.perc_or_abs <> 'P' THEN txm.taxperc ELSE 0 END as absval, txm.taxcode ");
					builder.append(" FROM chargetaxes ctx , taxmast txm  WHERE ctx.chargecode='"+chrgCode+"' AND ctx.status = 1 ");
					builder.append(" AND ctx.effectivefrom = (SELECT MAX(ctx1.effectivefrom) FROM chargetaxes ctx1 WHERE ");
					builder.append(" ctx1.chargecode=ctx.chargecode AND ctx1.taxcode=ctx.taxcode AND ctx1.effectivefrom <= CURRENT_DATE()) ");
					builder.append(" AND ctx.taxcode = txm.taxcode AND txm.regioncode = '"+region+"' AND txm.status = 1 AND CURRENT_DATE() BETWEEN txm.effectivefrom AND txm.effectiveto ");
				}
				query = getEntityManager() .createNativeQuery(builder.toString());
				taxsList = query.getResultList();
				LOGGER.info("END::getTaxAmounts()");
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				query = null;
				builder = null;
			}
		  return taxsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getStbInfo(Long cafNo) {
		List<Object[]> stbInfoList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT cfs.stbcafno AS IPTVBox_CafNo, ");
				builder.append(" cp.cpe_model AS IPTVBox_Model,cfs.stbleaseyn AS IPTVBox_Ownership, ");
				builder.append("  (SELECT chargeamt FROM cafcharges WHERE  stbcafno = cfs.stbcafno AND chargecode='STBCOST') AS IPTVBox_Charge, ");
				builder.append("  (SELECT chargeamt FROM cafcharges WHERE  stbcafno = cfs.stbcafno AND chargecode='STBEMI') AS IPTV_InstChrg, ");
				builder.append("  (SELECT totalemicnt FROM cafcharges WHERE  stbcafno = cfs.stbcafno AND chargecode='STBEMI') AS IPTVBox_EMI_Count,");
				builder.append("  cfs.stbslno AS IPTVBox_SrlNo,cfs.stbmacaddr AS IPTVBox_MacId, cfs.nwsubscode ");
				builder.append("  FROM cafstbs cfs , cpe_profilemaster cp WHERE cfs.stbprofileid = cp.profile_id ");
				builder.append("  AND cfs.stbprofileid=cp.profile_id AND cfs.parentcafno = "+cafNo+" ");
			try {
				LOGGER.info("START::getRecentPayment()");
				query = getEntityManager() .createNativeQuery(builder.toString());
				stbInfoList = query.getResultList();
				LOGGER.info("END::getRecentPayment()");
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				query = null;
				builder = null;
			}
		  return stbInfoList;
	}

	//added by @chaitanya_xyz
	
	public int getCard(String popslno) {
		String query ="";
		int cardnum=-1;
		int[] seq=new int[]{0,2,3,4,2,3,4,2,3,4};
		try{
			LOGGER.info("START::getCard()");
			query=getEntityManager().createNativeQuery("select pop_oltlabelno from oltmaster where pop_olt_serialno = "+popslno).getSingleResult().toString();
		cardnum=(Integer.parseInt(query.toString()));
			//1,2,3(91,92,93,94-1,  95-1,96,97)
			// 4-1
			//1-8---same ,91>
			if(cardnum<=8){
				return 1;
			}
			else{//91,92,93-2,3,4 && 94,95,96,-2,3,4
				cardnum=cardnum%10;	
			}
			
		}catch(Exception e){
			LOGGER.info("Error in::getCard()");
		}
		finally{
			LOGGER.info("End::getCard()");
		}
		return seq[cardnum];
		
	}
	public void updateCard(long cafno,int cardnum){
		try{
			LOGGER.info("START::getCard()");
			getEntityManager().createNativeQuery("UPDATE cafs SET olt_cardno = "+cardnum+"  WHERE cafno= "+cafno);			
		}catch(Exception e){
			LOGGER.info("Error in::getCard()");
		}
	}

	public int getCardByCaf(String cafno) {
		int cardnum=1;
		String query;
		try{
			LOGGER.info("START::getCardByCaf()");
			query=getEntityManager().createNativeQuery("select olt_cardno from cafs  WHERE cafno= "+cafno).getSingleResult().toString();			
			cardnum=Integer.parseInt(query);
		}catch(Exception e){
			LOGGER.info("Error in::getCardByCaf()");
		}
		return cardnum;
	}
	
	
	
	@Transactional
	public  String getIpaddressBySerialNumberAndPopId(String popId, String oltSerialNumber){
		
		String ipAddress=null;
		Query query = null;
		StringBuilder builder = new StringBuilder("select pop_olt_ipaddress from oltmaster where pop_olt_serialno ='"+oltSerialNumber+"'"+"  and pop_substnuid='" + popId+"'" );
			try {
				LOGGER.info("START::getIpaddressBySerialNumberAndPopId()");
				query = getEntityManager() .createNativeQuery(builder.toString());
				ipAddress = (String) query.getResultList().get(0);
				LOGGER.info("END::getIpaddressBySerialNumberAndPopId()");
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				query = null;
				builder = null;
			}
		  return ipAddress;
		
	}

	public boolean getStatus(String ipAddress) {
		// TODO Auto-generated method stub
		boolean status=false;
		
		try{
			LOGGER.info("START::getStatus()");			
			status=getEntityManager().createNativeQuery("select * from failedolts where ipaddr = '"+ipAddress+"'").getResultList().isEmpty();			
			LOGGER.info("from paymentDao"+status);
		}catch(Exception e){

			LOGGER.info(" "+e.getMessage());
		}
		return status;
		
	}

	/*public PaymentVO getCafDetails(Long cafno,String date) {
		// TODO Auto-generated method stub
		Object[] query;
		String yr= date+"-031";
		PaymentVO paymentvo=new PaymentVO();
		try{
			LOGGER.info("START::getCafDetails()");
			query=(Object[]) getEntityManager().createNativeQuery("select c.aadharno,c.cafno,mst.custid,mst.custname,c.custtypelov,inst_district,mst.pocmob1,sum(invamt) AS regbal from custinv inv,cafs c,customermst mst where mst.custid=c.custid and c.cafno='"+cafno+"' and invtdate<='"+yr+"'  and c.custid=inv.pmntcustid and curdate()>='"+yr+"' group by c.cafno ").getSingleResult();			
			paymentvo.setAadharNumber(query[0].toString());
			paymentvo.setCafNo(cafno);
			paymentvo.setCustId(Long.valueOf(query[2].toString()));
			paymentvo.setCustomerName(query[3].toString());
			paymentvo.setCustType(query[4].toString());
			paymentvo.setDistrict(query[5].toString());
			if(query[6]!=null) paymentvo.setMobileNo(query[6].toString());
			paymentvo.setPayment(query[7].toString());
			paymentvo.setPaidAmount(Float.valueOf(query[7].toString()));
			paymentvo.setPaymentMode("CASH");
			
			
			
		}catch(Exception e){
			LOGGER.info("Error in::getCafDetails()");
		}
		return paymentvo;
	}*/ //Siddharth_7_5_2018
	
	

	public PaymentVO getCafDetails(Long cafno,String date) {
		// TODO Auto-generated method stub
		Object[] query = null;
		//String yr= date+"-031";
		String string = date;
		String[] parts = string.split("-");
		String yr = parts[0]; // 004
		String mn = parts[1]; // 034556
		PaymentVO paymentvo=new PaymentVO();
		String mnprev=String.valueOf(Integer.parseInt(mn)-1);
		String prevyr=yr;
		String PrevPaid=null;
		
		/*if(Integer.parseInt(mn)==1)
		{	mnprev="12";
		prevyr=String.valueOf(Integer.parseInt(yr)-1);
		} */
		try{
			LOGGER.info("START::getCafDetails()");
			//PrevPaid=getPrevPaid(mnprev,cafno.toString(),prevyr);
			
		// 	(PrevPaid.equals("0"))		
				query=(Object[]) getEntityManager().createNativeQuery("select c.aadharno,c.cafno,mst.custid,mst.custname,c.custtypelov,inst_district,mst.pocmob1,sum(invamt+prevbal+srvctax-prevpymtreceived) AS regbal from custinv inv,cafs c,customermst mst where mst.custid=c.custid and c.cafno='"+cafno+"' and inv.invyr='"+yr+"' and inv.invmn='"+mn+"' and c.custid=inv.pmntcustid group by c.cafno ").getSingleResult();			
					//	else if (PrevPaid.equals("1"))
			//query=(Object[]) getEntityManager().createNativeQuery("select c.aadharno,c.cafno,mst.custid,mst.custname,c.custtypelov,inst_district,mst.pocmob1,sum(invamt+srvctax) AS regbal from custinv inv,cafs c,customermst mst where mst.custid=c.custid and c.cafno='"+cafno+"' and inv.invyr='"+yr+"' and inv.invmn='"+mn+"' and c.custid=inv.pmntcustid  group by c.cafno ").getSingleResult();
			
			
			paymentvo.setAadharNumber(query[0].toString());
			paymentvo.setCafNo(cafno);
			paymentvo.setCustId(Long.valueOf(query[2].toString()));
			paymentvo.setCustomerName(query[3].toString());
			paymentvo.setCustType(query[4].toString());
			paymentvo.setDistrict(query[5].toString());
			if(query[6]!=null) paymentvo.setMobileNo(query[6].toString());
			paymentvo.setPayment(query[7].toString());
			paymentvo.setPaidAmount(Float.valueOf(query[7].toString()));
			paymentvo.setPaymentMode("CASH");
			
			
			
		}catch(Exception e){
			LOGGER.info("Error in::getCafDetails()");
		}
		return paymentvo;
	}


/*public String getPrevPaid(String mnprev, String cafno, String prevyr ) {
		
		List<BulkCustomerDTO> cafList = new ArrayList<BulkCustomerDTO>();
		Integer cafList1 ;
		StringBuilder builder = new StringBuilder("");
		// String yr= from+"-31";
		Query query = null;
		
		
		String prevPaidFlag = null;
		
		
		
		//if prev =t then 
		try {
			
			
			
				builder.append( "select inv.prevpaidamt from custinv inv,cafs c,customermst mst where mst.custid=c.custid and c.cafno='"+cafno+"' and inv.invyr='"+prevyr+"' and inv.invmn='"+mnprev+"' and c.custid=inv.pmntcustid  group by c.cafno ");
						
				query = getEntityManager() .createNativeQuery(builder.toString());
			cafList1 =  (Integer) query.getResultList().get(0);
			
			prevPaidFlag=cafList1.toString();
			
		} catch(Exception ex){
			// logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		
		return prevPaidFlag ;
	}
	*/
	
	
	
	
	
	
}


