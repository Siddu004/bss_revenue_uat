/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class OSDDataMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String position;
	
	private String fontType;
	
	private String fontSize;
	
	private String fontColor;
	
	private String bgColor;
	
	private String duration;
	
	private String message;
	
	private String userCanCloseMessage;

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFontType() {
		return fontType;
	}

	public void setFontType(String fontType) {
		this.fontType = fontType;
	}

	public String getFontSize() {
		return fontSize;
	}

	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserCanCloseMessage() {
		return userCanCloseMessage;
	}

	public void setUserCanCloseMessage(String userCanCloseMessage) {
		this.userCanCloseMessage = userCanCloseMessage;
	}
	
}
