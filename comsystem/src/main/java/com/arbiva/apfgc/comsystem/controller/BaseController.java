package com.arbiva.apfgc.comsystem.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Controller;

import com.arbiva.apfgc.comsystem.exception.CafException;
import com.arbiva.apfgc.comsystem.util.CafErrorCodes;
import com.arbiva.apfgc.comsystem.vo.TenantJsonResponseVo;


@Controller
public class BaseController {
	
public TenantJsonResponseVo handleExceptions(Exception exc, TenantJsonResponseVo tenantJsonResponseVo) {
		
		if (exc instanceof CafException) {
			
			tenantJsonResponseVo.setErrorCode(((CafException) exc).getErrorCode());
			tenantJsonResponseVo.setErrorMessage(exc.getMessage());
			tenantJsonResponseVo.setDesc(((CafException) exc).getErrorDescription());
			tenantJsonResponseVo.setOutPut("failure");
			
		} else {
			
			tenantJsonResponseVo.setOutPut("failure");
			tenantJsonResponseVo.setErrorCode(((CafException) exc).getErrorCode());
			tenantJsonResponseVo.setErrorMessage(exc.getMessage());
			tenantJsonResponseVo.setDesc(((CafException) exc).getErrorDescription());
			
		}
		return tenantJsonResponseVo;
	}
	
	public ResponseBuilder handleRestExceptions(Exception exc,ResponseBuilder builder) {
		if (exc instanceof CafException) {
			builder = Response.status(Status.BAD_REQUEST).entity((CafException) exc);
		} else {
			builder = Response.status(Status.INTERNAL_SERVER_ERROR).entity(new CafException(CafErrorCodes.TENT001, exc.getMessage()));
		}
		return builder;
	}

}
