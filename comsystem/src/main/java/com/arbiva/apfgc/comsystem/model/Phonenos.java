/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "phonenos")
public class Phonenos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "phoneno")
	private String phoneNo;

	@Column(name = "stdcode")
	private String stdCode;

	@Column(name = "series")
	private String series;
	
	@Column(name = "fancyflag")
	private int fancyFlag;

	@Column(name = "status")
	private int status;

	@Column(name = "createdon")
	private Calendar createdOn;

	@Column(name = "createdby")
	private String createdBy;

	@Column(name = "createdipaddr")
	private String createdIpaddr;

	@Column(name = "allocatedon")
	private Calendar allocatedOn;

	@Column(name = "allocatedby")
	private String allocatedBy;

	@Column(name = "allocatedipaddr")
	private String allocatedIpaddr;

	@Column(name = "releasedon")
	private Calendar releasedOn;
	
	@Column(name = "releasedby")
	private String releasedBy;

	@Column(name = "releasedipaddr")
	private String releasedIpaddr;
	
	@ManyToOne
	@JoinColumn(name = "stdcode", referencedColumnName = "stdcode", nullable = false, insertable=false, updatable=false)
	private StdCodes stdCodes;

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedIpaddr() {
		return createdIpaddr;
	}

	public void setCreatedIpaddr(String createdIpaddr) {
		this.createdIpaddr = createdIpaddr;
	}

	public Calendar getAllocatedOn() {
		return allocatedOn;
	}

	public void setAllocatedOn(Calendar allocatedOn) {
		this.allocatedOn = allocatedOn;
	}

	public String getAllocatedBy() {
		return allocatedBy;
	}

	public void setAllocatedBy(String allocatedBy) {
		this.allocatedBy = allocatedBy;
	}

	public String getAllocatedIpaddr() {
		return allocatedIpaddr;
	}

	public void setAllocatedIpaddr(String allocatedIpaddr) {
		this.allocatedIpaddr = allocatedIpaddr;
	}

	public Calendar getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(Calendar releasedOn) {
		this.releasedOn = releasedOn;
	}

	public String getReleasedBy() {
		return releasedBy;
	}

	public void setReleasedBy(String releasedBy) {
		this.releasedBy = releasedBy;
	}

	public String getReleasedIpaddr() {
		return releasedIpaddr;
	}

	public void setReleasedIpaddr(String releasedIpaddr) {
		this.releasedIpaddr = releasedIpaddr;
	}

	public StdCodes getStdCodes() {
		return stdCodes;
	}

	public void setStdCodes(StdCodes stdCodes) {
		this.stdCodes = stdCodes;
	}

	public int getFancyFlag() {
		return fancyFlag;
	}

	public void setFancyFlag(int fancyFlag) {
		this.fancyFlag = fancyFlag;
	}
	
}
