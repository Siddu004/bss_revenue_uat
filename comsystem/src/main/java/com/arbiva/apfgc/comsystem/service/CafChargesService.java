/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.model.CafCharges;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
public interface CafChargesService {
	
	public abstract void deleteCafCharges(CafCharges cafCharges);
	
	public abstract CafCharges findByCafNo(Long cafNo);

	public abstract List<CafCharges> findAllCafCharges();

	public abstract void saveCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId);
	
	public abstract void createCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId);

	public abstract void createExtraCableCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, Long custId);

	public abstract void updateCafCharges(PaymentVO paymentVO, String loginID);

	public abstract void saveStbCafCharges(CustomerCafVO customerCafVO, Long stbCafNo, IPTVBoxVO iPTVBoxVO);

	public abstract void saveEntTaxCafCharges(CustomerCafVO customerCafVO, Long stbCafNo);

	public abstract void deleteCafChargesByCafNo(Long cafNo);

	public abstract void saveUpfrontStbCafCharges(CustomerCafVO customerCafVO, Long cafNo, IPTVBoxVO iPTVBoxVO);

	public abstract void saveUpfrontCafCharges(CustomerCafVO customerCafVO, Long cafNo, String loginID, long longValue);

}
