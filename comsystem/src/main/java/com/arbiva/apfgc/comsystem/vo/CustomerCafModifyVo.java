/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Arbiva
 *
 */
public class CustomerCafModifyVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cafno;

	private String aadharno;

	private String titlelov;

	private String fname;

	private String mname;

	private String lname;

	private String fhname;

	private String actualdob;

	private String gender;

	private String email1;

	private String pocName;

	private String billfreqLov;

	private String inst_addr1;

	private String inst_addr2;

	private String inst_locality;

	private String inst_district;

	private String inst_mandal;

	private String inst_city_village;

	private String inst_pin;

	private String pocMob1;

	private String pocMob2;

	private String stdCode;

	private String landLine1;

	private String lattitude;

	private String longitude;

	private String cpeplace;

	private String status;

	private String cust_id;
	
	private String popSubStnCode;
	
	private String cafStatus;
	
	private String popDistrict;
	
	private String popMandal;
	
	private String region;
	
	private String popDistrictName;
	
	private String popMandalName;
	
	private String inst_districtName;

	private String inst_mandalName;

	private String inst_city_villageName;
	
	private String popName;
	
	private String apsflUniqueId;
	
	private String contactPersonName;
	
	private String contactPersonEmail;
	
	private String contactPersonMobileNo;
	
	private String custType;
	
	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getContactPersonMobileNo() {
		return contactPersonMobileNo;
	}

	public void setContactPersonMobileNo(String contactPersonMobileNo) {
		this.contactPersonMobileNo = contactPersonMobileNo;
	}

	public String getApsflUniqueId() {
		return apsflUniqueId;
	}

	public void setApsflUniqueId(String apsflUniqueId) {
		this.apsflUniqueId = apsflUniqueId;
	}

	public String getPopDistrictName() {
		return popDistrictName;
	}

	public void setPopDistrictName(String popDistrictName) {
		this.popDistrictName = popDistrictName;
	}

	public String getPopMandalName() {
		return popMandalName;
	}

	public void setPopMandalName(String popMandalName) {
		this.popMandalName = popMandalName;
	}

	public String getInst_districtName() {
		return inst_districtName;
	}

	public void setInst_districtName(String inst_districtName) {
		this.inst_districtName = inst_districtName;
	}

	public String getInst_mandalName() {
		return inst_mandalName;
	}

	public void setInst_mandalName(String inst_mandalName) {
		this.inst_mandalName = inst_mandalName;
	}

	public String getInst_city_villageName() {
		return inst_city_villageName;
	}

	public void setInst_city_villageName(String inst_city_villageName) {
		this.inst_city_villageName = inst_city_villageName;
	}

	public String getPopName() {
		return popName;
	}

	public void setPopName(String popName) {
		this.popName = popName;
	}

	public String getPopDistrict() {
		return popDistrict;
	}

	public void setPopDistrict(String popDistrict) {
		this.popDistrict = popDistrict;
	}

	public String getPopMandal() {
		return popMandal;
	}

	public void setPopMandal(String popMandal) {
		this.popMandal = popMandal;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPopSubStnCode() {
		return popSubStnCode;
	}

	public void setPopSubStnCode(String popSubStnCode) {
		this.popSubStnCode = popSubStnCode;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCafno() {
		return cafno;
	}

	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	public String getAadharno() {
		return aadharno;
	}

	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}

	public String getTitlelov() {
		return titlelov;
	}

	public void setTitlelov(String titlelov) {
		this.titlelov = titlelov;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getFhname() {
		return fhname;
	}

	public void setFhname(String fhname) {
		this.fhname = fhname;
	}

	public String getActualdob() {
		return actualdob;
	}

	public void setActualdob(String actualdob) {
		this.actualdob = actualdob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getBillfreqLov() {
		return billfreqLov;
	}

	public void setBillfreqLov(String billfreqLov) {
		this.billfreqLov = billfreqLov;
	}

	public String getInst_addr1() {
		return inst_addr1;
	}

	public void setInst_addr1(String inst_addr1) {
		this.inst_addr1 = inst_addr1;
	}

	public String getInst_addr2() {
		return inst_addr2;
	}

	public void setInst_addr2(String inst_addr2) {
		this.inst_addr2 = inst_addr2;
	}

	public String getInst_locality() {
		return inst_locality;
	}

	public void setInst_locality(String inst_locality) {
		this.inst_locality = inst_locality;
	}

	public String getInst_district() {
		return inst_district;
	}

	public void setInst_district(String inst_district) {
		this.inst_district = inst_district;
	}

	public String getInst_mandal() {
		return inst_mandal;
	}

	public void setInst_mandal(String inst_mandal) {
		this.inst_mandal = inst_mandal;
	}

	public String getInst_city_village() {
		return inst_city_village;
	}

	public void setInst_city_village(String inst_city_village) {
		this.inst_city_village = inst_city_village;
	}

	public String getInst_pin() {
		return inst_pin;
	}

	public void setInst_pin(String inst_pin) {
		this.inst_pin = inst_pin;
	}

	public String getPocMob1() {
		return pocMob1;
	}

	public void setPocMob1(String pocMob1) {
		this.pocMob1 = pocMob1;
	}

	public String getPocMob2() {
		return pocMob2;
	}

	public void setPocMob2(String pocMob2) {
		this.pocMob2 = pocMob2;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getLandLine1() {
		return landLine1;
	}

	public void setLandLine1(String landLine1) {
		this.landLine1 = landLine1;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCpeplace() {
		return cpeplace;
	}

	public void setCpeplace(String cpeplace) {
		this.cpeplace = cpeplace;
	}

	public String getCafStatus() {
		return cafStatus;
	}

	public void setCafStatus(String cafStatus) {
		this.cafStatus = cafStatus;
	}
	
}
