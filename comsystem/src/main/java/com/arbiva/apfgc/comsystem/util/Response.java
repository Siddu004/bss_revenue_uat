package com.arbiva.apfgc.comsystem.util;

public enum Response {

	statusCode("Status Code"),
	statusMessage("Status Message"),
	responseStatus("Response Status"),
	availableBal("Available Balance"),
	Code200("200"),
	Code401("401"),
	Code402("402"),
	Code403("403"),
	Code500("500"),
	
	PACKAGECODES("Package Codes"),
	PACKAGECODE("Package Code"),
	PACKAGENAME("Package Name"),
	CHANNELSLIST("Channel Codes"),
	PRICE("Price"),
	TAX("Tax"),
	TOTALPRICE("Total Price"),
	PACKAGELIST("Package List"),
	OTP("OTP"),
	SUBSCRIBER_CODE("Subscriber Code"),
	ACTIVATION("Activation Charge"),
	DEPOSIT("Security Deposit"),
	RECURRING("Monthly Charge"),
	REFERENCENO("Reference ID"),
	DEACTIVATIONSTATUS("8");
	
	

	
	String name;
	
	Response(){}
	Response(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
