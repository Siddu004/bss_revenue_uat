package com.arbiva.apfgc.comsystem.vo;

public class InvoiceAmountDtls {
	
	private String currentMonthIndInvAmount;
	
	private String currentMonthEntInvAmount;
	
	private String prevMonthIndInvAmount;
	
	private String prevMonthEntInvAmount;

	public String getCurrentMonthIndInvAmount() {
		return currentMonthIndInvAmount;
	}

	public void setCurrentMonthIndInvAmount(String currentMonthIndInvAmount) {
		this.currentMonthIndInvAmount = currentMonthIndInvAmount;
	}

	public String getCurrentMonthEntInvAmount() {
		return currentMonthEntInvAmount;
	}

	public void setCurrentMonthEntInvAmount(String currentMonthEntInvAmount) {
		this.currentMonthEntInvAmount = currentMonthEntInvAmount;
	}

	public String getPrevMonthIndInvAmount() {
		return prevMonthIndInvAmount;
	}

	public void setPrevMonthIndInvAmount(String prevMonthIndInvAmount) {
		this.prevMonthIndInvAmount = prevMonthIndInvAmount;
	}

	public String getPrevMonthEntInvAmount() {
		return prevMonthEntInvAmount;
	}

	public void setPrevMonthEntInvAmount(String prevMonthEntInvAmount) {
		this.prevMonthEntInvAmount = prevMonthEntInvAmount;
	}
	

		
}
