/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.util.List;

import com.arbiva.apfgc.comsystem.BO.PackageBO;

/**
 * @author Lakshman
 *
 */
public class BillSummaryVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String monthlyCharges;;

	private String telephoneUsageCharges;
	
	private String internetUsageCharges;

	private String valueAddedCharges;

	private String oneTimeCharges;

	private String adjustmentCharges;
	
	private String TotalTax;
	
	private String lateFee;

	private String totalCharge;
	
	private String serviceTax;

	private String swatchTax;

	private String kisanTax;
	
	private String entTax;
	
	private List<PackageBO> packageList;
	
	private String lastPaymentMode;
	
	private String lastPaymentAmount;
	
	private String lastPaymentDate;

	private String statusCode;
	
	private String description;

	public String getMonthlyCharges() {
		return monthlyCharges;
	}

	public void setMonthlyCharges(String monthlyCharges) {
		this.monthlyCharges = monthlyCharges;
	}

	public String getTelephoneUsageCharges() {
		return telephoneUsageCharges;
	}

	public void setTelephoneUsageCharges(String telephoneUsageCharges) {
		this.telephoneUsageCharges = telephoneUsageCharges;
	}

	public String getInternetUsageCharges() {
		return internetUsageCharges;
	}

	public void setInternetUsageCharges(String internetUsageCharges) {
		this.internetUsageCharges = internetUsageCharges;
	}

	public String getValueAddedCharges() {
		return valueAddedCharges;
	}

	public void setValueAddedCharges(String valueAddedCharges) {
		this.valueAddedCharges = valueAddedCharges;
	}

	public String getOneTimeCharges() {
		return oneTimeCharges;
	}

	public void setOneTimeCharges(String oneTimeCharges) {
		this.oneTimeCharges = oneTimeCharges;
	}

	public String getAdjustmentCharges() {
		return adjustmentCharges;
	}

	public void setAdjustmentCharges(String adjustmentCharges) {
		this.adjustmentCharges = adjustmentCharges;
	}

	public String getTotalTax() {
		return TotalTax;
	}

	public void setTotalTax(String totalTax) {
		TotalTax = totalTax;
	}

	public String getLateFee() {
		return lateFee;
	}

	public void setLateFee(String lateFee) {
		this.lateFee = lateFee;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(String swatchTax) {
		this.swatchTax = swatchTax;
	}

	public String getKisanTax() {
		return kisanTax;
	}

	public void setKisanTax(String kisanTax) {
		this.kisanTax = kisanTax;
	}

	public String getEntTax() {
		return entTax;
	}

	public void setEntTax(String entTax) {
		this.entTax = entTax;
	}
	
	public List<PackageBO> getPackageList() {
		return packageList;
	}

	public void setPackageList(List<PackageBO> packageList) {
		this.packageList = packageList;
	}

	public String getLastPaymentMode() {
		return lastPaymentMode;
	}

	public void setLastPaymentMode(String lastPaymentMode) {
		this.lastPaymentMode = lastPaymentMode;
	}

	public String getLastPaymentAmount() {
		return lastPaymentAmount;
	}

	public void setLastPaymentAmount(String lastPaymentAmount) {
		this.lastPaymentAmount = lastPaymentAmount;
	}

	public String getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
