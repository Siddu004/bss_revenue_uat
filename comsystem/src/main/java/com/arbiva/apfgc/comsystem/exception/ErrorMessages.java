package com.arbiva.apfgc.comsystem.exception;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "errorcodes")
public class ErrorMessages implements java.io.Serializable{ 
		
		private static final long serialVersionUID = 1L;
		
		@Id
		@Column(name="errorcode")
		private int errorCode;
		
		@Column(name="errordesc")
		private String errorDesc;
		
		@Column(name="status")
		private Byte status;
		
		
		public int getErrorCode() {
			return errorCode;
		}
		
		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		public String getErrorDesc() {
			return errorDesc;
		}
		public void setErrorDesc(String errorDesc) {
			this.errorDesc = errorDesc;
		}
		public Byte getStatus() {
			return status;
		}
		public void setStatus(Byte status) {
			this.status = status;
		}

}

	
