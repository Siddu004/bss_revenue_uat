package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;
import java.util.List;

import com.arbiva.apfgc.comsystem.vo.AadharDtlsVO;


public class AadharHelperDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<AadharDtlsVO> aadharDtlsVO;
	
	private int cafActiveCount;
	private int cafTotalCount;

	public int getCafActiveCount() {
		return cafActiveCount;
	}

	public void setCafActiveCount(int cafActiveCount) {
		this.cafActiveCount = cafActiveCount;
	}

	public int getCafTotalCount() {
		return cafTotalCount;
	}

	public void setCafTotalCount(int cafTotalCount) {
		this.cafTotalCount = cafTotalCount;
	}

	public List<AadharDtlsVO> getAadharDtlsVO() {
		return aadharDtlsVO;
	}
	
	public void setAadharDtlsVO(List<AadharDtlsVO> aadharDtlsVO) {
		this.aadharDtlsVO = aadharDtlsVO;
	}


	
}
