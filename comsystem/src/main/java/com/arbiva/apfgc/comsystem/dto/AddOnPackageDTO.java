package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class AddOnPackageDTO  implements Serializable{
	
	private static final long serialVersionUID = -3558576606482230776L;
	
	private String acctCafNo;
	
	private String month;
	
	private String year;
	
	private BigDecimal downSize;
	
	private BigDecimal uplSize;
	

	public String getAcctCafNo() {
		return acctCafNo;
	}

	public void setAcctCafNo(String acctCafNo) {
		this.acctCafNo = acctCafNo;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public BigDecimal getDownSize() {
		return downSize;
	}

	public void setDownSize(BigDecimal downSize) {
		this.downSize = downSize;
	}

	public BigDecimal getUplSize() {
		return uplSize;
	}

	public void setUplSize(BigDecimal uplSize) {
		this.uplSize = uplSize;
	}

}
