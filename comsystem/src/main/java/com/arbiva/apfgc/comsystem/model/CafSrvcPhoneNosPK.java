/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lakshman
 *
 */
public class CafSrvcPhoneNosPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long cafNo;
	
	private Date effectiveFrom;
	
	private String phoneNo;

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cafNo == null) ? 0 : cafNo.hashCode());
		result = prime * result + ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
		result = prime * result + ((phoneNo == null) ? 0 : phoneNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafSrvcPhoneNosPK other = (CafSrvcPhoneNosPK) obj;
		if (cafNo == null) {
			if (other.cafNo != null)
				return false;
		} else if (!cafNo.equals(other.cafNo))
			return false;
		if (effectiveFrom == null) {
			if (other.effectiveFrom != null)
				return false;
		} else if (!effectiveFrom.equals(other.effectiveFrom))
			return false;
		if (phoneNo == null) {
			if (other.phoneNo != null)
				return false;
		} else if (!phoneNo.equals(other.phoneNo))
			return false;
		return true;
	}
	
}
