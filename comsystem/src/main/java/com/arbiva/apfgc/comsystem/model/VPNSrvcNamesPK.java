/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class VPNSrvcNamesPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String substnUid;
	
	private String oltSerialNo;
	
	private String vpnsrvcName;

	public String getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(String substnUid) {
		this.substnUid = substnUid;
	}

	public String getOltSerialNo() {
		return oltSerialNo;
	}

	public void setOltSerialNo(String oltSerialNo) {
		this.oltSerialNo = oltSerialNo;
	}

	public String getVpnsrvcName() {
		return vpnsrvcName;
	}

	public void setVpnsrvcName(String vpnsrvcName) {
		this.vpnsrvcName = vpnsrvcName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((oltSerialNo == null) ? 0 : oltSerialNo.hashCode());
		result = prime * result + ((substnUid == null) ? 0 : substnUid.hashCode());
		result = prime * result + ((vpnsrvcName == null) ? 0 : vpnsrvcName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VPNSrvcNamesPK other = (VPNSrvcNamesPK) obj;
		if (oltSerialNo == null) {
			if (other.oltSerialNo != null)
				return false;
		} else if (!oltSerialNo.equals(other.oltSerialNo))
			return false;
		if (substnUid == null) {
			if (other.substnUid != null)
				return false;
		} else if (!substnUid.equals(other.substnUid))
			return false;
		if (vpnsrvcName == null) {
			if (other.vpnsrvcName != null)
				return false;
		} else if (!vpnsrvcName.equals(other.vpnsrvcName))
			return false;
		return true;
	}
	
}
