package com.arbiva.apfgc.comsystem.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;


public class FileUtil {
	
	private static final Logger logger = Logger.getLogger(FileUtil.class);
	
	public static void saveImage(String fileName, String basepath, byte[] bs) throws IOException{
		
		try {
			byte[] bytes = bs;
			// Creating the directory to store file
			// String rootPath = System.getProperty("user.home");
			File directory = new File(basepath);
			if (!directory.exists())
				directory.mkdirs();

			// Create the file on server
			File serverFile = new File(directory.getAbsolutePath()
					+ File.separator + fileName);
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch(Exception e) {
			logger.error("FileUtil::saveImage() " + e);
			e.printStackTrace();
		}
		logger.info("The Documents Stored in to folder.");
	}
	
	public static String GetLockPeriodDate(String month) throws IOException {
		 Calendar calendar = Calendar.getInstance();
		 calendar.add(Calendar.MONTH, Integer.parseInt(month));
		 Date loockPeriodMonth = calendar.getTime();
		 return new SimpleDateFormat("MM/dd/yyyy").format(loockPeriodMonth);
	}
	
	public static String saveImage(String fileName, String basepath, byte[] bs, String loginId, String docType) throws IOException{
		String path = null;
		try {
			byte[] bytes = bs;
			// Creating the directory to store file
			// String rootPath = System.getProperty("user.home");
			File directory = new File(basepath+ File.separator + loginId + File.separator + docType);
			if (!directory.exists())
				directory.mkdirs();

			// Create the file on server
			 path = new String(directory.getAbsolutePath()+ File.separator + fileName);
			File serverFile = new File(path);
			if(bs==null)
			{
				serverFile.createNewFile();
				logger.error("FileUtil::saveImage() ");
			}
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch(Exception e) {
			logger.error("FileUtil::saveImage() " + e);
			e.printStackTrace();
		}
		logger.info("The Documents Stored in to folder.");
		return path;
	}
	
	public static Float precision(int decimalPlace, Float value) {

	    BigDecimal bd = new BigDecimal(Float.toString(value));
	    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
	    return bd.floatValue();
	  }
	
	public static String NumberCheck(String input) {
		String status = "";
		try {
			if(input.matches("[0-9]+")) {
				status = "true";
			} else {
				status = "false";
			}
		} catch(Exception e) {
			logger.info("The Exception is FileUtil :: NumberCheck " + e);
			e.printStackTrace();
		}
	    return status;
	  }

	public static String mspCodeSplitMethod(String lmoMspCodes) {
		String lmoAgrMspCodes = "";
		try {
			String[] mspCodes = lmoMspCodes.split(",");
			StringBuilder agrmntMspcodes = new StringBuilder();
			int count = 0;
			for (String agrMspCode : mspCodes) {
				count++;
				if (count != mspCodes.length) {
					agrmntMspcodes.append("'").append(agrMspCode).append("'").append(",");
				} else {
					agrmntMspcodes.append("'").append(agrMspCode).append("'");
				}
			}
			lmoAgrMspCodes = agrmntMspcodes.toString();
		} catch(Exception e) {
			logger.info("The Exception is FileUtil :: NumberCheck " + e);
			e.printStackTrace();
		}
	    return lmoAgrMspCodes;
	  }
	
	public static String BulkUploadValidation(EntCafStage entCafStage) throws IOException {
		String status = "false";
		String numberStatus = "false";
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = null;
		if(entCafStage.getContactEmail() != null) {
		matcher = pattern.matcher(entCafStage.getContactEmail());
		}
		
		String regexAdd =  "^[a-zA-Z0-9&#,/ -^.]*$";
		Pattern patternAdd = Pattern.compile(regexAdd);
		Matcher matcherAdd = null;
		if(entCafStage.getLocation() != null) {
		matcherAdd = patternAdd.matcher(entCafStage.getLocation());
		}
		
		if(entCafStage.getContactMobileNo() != null) {
			numberStatus = FileUtil.NumberCheck(entCafStage.getContactMobileNo());
		}
		if (entCafStage.getApsflUniqueId() == null || entCafStage.getApsflUniqueId().length() > 10) {
			status = "APSFL Code More Than 10 Charecters. OR APSFL Code is Empty";
		} else if (entCafStage.getLocation()== null || entCafStage.getLocation().length() > 255 || !matcherAdd.matches()) {
			status = "ONT Location More Than 255 Charecters. OR ONT Location is Empty OR Should contains Special Symbols like #&/-  only";
		} else if(entCafStage.getDistrict()== null || entCafStage.getDistrict().equalsIgnoreCase("null") || entCafStage.getDistrict().isEmpty() || entCafStage.getDistrict().length() > 100) {
			status = "District More Than 100 Charecters. OR District is Empty";
		} else if(entCafStage.getMandal()== null || entCafStage.getMandal().length() > 100) {
			status = "Mandal More Than 100 Charecters. OR Mandal is Empty";
		} else if(entCafStage.getContactMobileNo()== null || entCafStage.getContactMobileNo().length() != 10 || numberStatus.equalsIgnoreCase("false")) {
			status = "MobileNo More Than 10 digits OR not a number.";
		} else if( entCafStage.getContactName() == null || entCafStage.getContactName().length() > 255) {
			status = "ContactName More Than 255 Charecters. OR ContactName is Empty";
		} else if(entCafStage.getContactEmail() != null && (entCafStage.getContactEmail().length() > 100 || !matcher.matches())) {
			status = "EmailId More Than 100 Charecters OR Not an Valid EmailId.";
		} else if(entCafStage.getPmntrespFlag() == null || entCafStage.getPmntrespFlag().isEmpty()) {
			status = "PaymentResponsibleFlag is Empty.";
		} else if(String.valueOf(entCafStage.getLongitude()).length() > 17) {
			status = "Longitude More Than 17 Charecters.";
		} else if(String.valueOf(entCafStage.getLattitude()).length() > 17) {
			status = "Lattitude More Than 17 Charecters.";
		} else {
			status = "true";
		}	
		return status;
	}

	public static Map<String, Integer> getListSize(List<IPTVBoxVO> iptvBoxList, String prodcodes) {
		Map<String, Integer> packagesMap = new HashMap<>();
		try {
			for(IPTVBoxVO iptvBoxVO : iptvBoxList) {
				if(iptvBoxVO.getStbSerialNo() != null && iptvBoxVO.getMacAddress() != null && !iptvBoxVO.getStbSerialNo().isEmpty() && !iptvBoxVO.getMacAddress().isEmpty()) {
					String[] iptvPkgCodes = iptvBoxVO.getIptvSrvcCodes().split(",");
					for(String prodcode : iptvPkgCodes) {
						if(prodcodes.indexOf(prodcode) != -1) {
							Integer i = packagesMap.get(prodcode);
							if(i != null) {
								i++;
								packagesMap.put(prodcode, i);
							} else {
								packagesMap.put(prodcode, 1);
							}
						}
					}
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return packagesMap;
	}
}
