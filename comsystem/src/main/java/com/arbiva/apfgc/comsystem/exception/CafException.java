package com.arbiva.apfgc.comsystem.exception;

import com.arbiva.apfgc.comsystem.util.CafErrorCodes;

public class CafException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int errorCode;
	private String errorDescription;
	private String message;
	
	public CafException(){
		super();
	}
	
	


	/**
	 * @param TENT003
	 * @param string
	 */
	public CafException(CafErrorCodes prod003, String message) {
		
		this.errorCode = prod003.getCode();
		this.errorDescription = prod003.getDescription();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
}
