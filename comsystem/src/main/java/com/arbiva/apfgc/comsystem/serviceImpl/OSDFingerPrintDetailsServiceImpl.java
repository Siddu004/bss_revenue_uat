/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.OSDFingerPrintDetailsDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.OSDFingerPrintDetails;
import com.arbiva.apfgc.comsystem.service.OSDFingerPrintDetailsService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CorpusResponseVO;
import com.google.gson.Gson;

/**
 * @author Arbiva
 *
 */
@Service
public class OSDFingerPrintDetailsServiceImpl implements OSDFingerPrintDetailsService {
	
	private static final Logger logger = Logger.getLogger(OSDFingerPrintDetailsServiceImpl.class);

	@Autowired
	OSDFingerPrintDetailsDao oSDFingerPrintDetailsDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	

	@Override
	@Transactional
	public OSDFingerPrintDetails saveOSDFingerPrintDetails(String subscribers, String requestJson, String responseJson, String loginID, Integer jsonSeqNo, String fpType, String trackingId, String message ) {
		OSDFingerPrintDetails oSDFingerPrintDetails = new OSDFingerPrintDetails();
		Long fpRequestId = null;
		try {
			fpRequestId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.FP_REQUEST_ID.getCode());
			oSDFingerPrintDetails.setCreatedBy(loginID);
			oSDFingerPrintDetails.setCreatedIpaddr(httpServletRequest.getRemoteAddr());
			oSDFingerPrintDetails.setCreatedOn(Calendar.getInstance());
			oSDFingerPrintDetails.setJsonSeqno(jsonSeqNo);
			oSDFingerPrintDetails.setRequestId(fpRequestId);
			oSDFingerPrintDetails.setDisplayMessage(message);
			oSDFingerPrintDetails.setRequestJson(requestJson);
			oSDFingerPrintDetails.setResponseJson(responseJson);
			oSDFingerPrintDetails.setRequestDate(Calendar.getInstance());
			oSDFingerPrintDetails.setRequestType(fpType);
			oSDFingerPrintDetails.setResponseId(trackingId);
			oSDFingerPrintDetails.setStatus(1);
			oSDFingerPrintDetails.setSubScriptionnos(subscribers);
			oSDFingerPrintDetails = oSDFingerPrintDetailsDao.saveOSDFingerPrintDetails(oSDFingerPrintDetails);
			logger.info("The OSDFingerPrintDetails Inserted Successfully ");
		} catch(Exception e) {
			logger.error("The Exception is OSDFingerPrintDetailsServiceImpl :: saveOSDFingerPrintDetails" +e);
			e.printStackTrace();
		} finally {
			fpRequestId = null;
		}
		return oSDFingerPrintDetails;
	}


	@Override
	@Transactional
	public String updateOSDFingerPrintDetails(CorpusResponseVO corpusResponseVO) {
		String status = "false";
		String responseJson = "";
		Gson gson = new Gson();
		try {
			responseJson = gson.toJson(corpusResponseVO);
			status = oSDFingerPrintDetailsDao.updateOSDFingerPrintDetails(corpusResponseVO.getTrackingId(), responseJson);
		} catch(Exception e) {
			logger.error("The Exception is OSDFingerPrintDetailsServiceImpl :: updateOSDFingerPrintDetails" +e);
			e.printStackTrace();
		} finally {
			responseJson = null;
			gson = null;
		}
		return status;
	}

}
