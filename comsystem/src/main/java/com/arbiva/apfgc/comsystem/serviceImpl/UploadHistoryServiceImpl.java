/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dao.impl.UploadHistoryDao;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.Substations;
import com.arbiva.apfgc.comsystem.model.UploadHistory;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.service.CPEInformationService;
import com.arbiva.apfgc.comsystem.service.UploadHistoryService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.VPNServiceExcelUploadVO;
import com.arbiva.apfgc.comsystem.vo.VPNSrvcVO;

/**
 * @author Lakshman
 *
 */
@Service
public class UploadHistoryServiceImpl implements UploadHistoryService {
	
	@Autowired
	UploadHistoryDao uploadHistoryDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CPEInformationService cpeInformationService;
	
	private static final Logger logger = Logger.getLogger(UploadHistoryServiceImpl.class);

	@Override
	public List<EntCafStage> getAllEntCafStageByStatus(Long uploadId) {
		List<EntCafStage> entCafStagesList = new ArrayList<>();
		try {
			entCafStagesList = uploadHistoryDao.findAllEntCafStageByStatus(uploadId);
		} catch (Exception e) {
			logger.error("UploadHistoryServiceImpl :: getAllEntCafStage()" + e);
			e.printStackTrace();
		} finally {

		}
		return entCafStagesList;
	}

	@Override
	@Transactional
	public UploadHistory saveUploadHistory(CustomerCafVO customerCafVO) {
		UploadHistory uploadHistory = new UploadHistory();
		EntCafStage entCafStage = null;
		Long recordNo = 1l;
		try {
			Long uploadId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.ENT_Caf_UploadId.getCode());
			uploadHistory.setUploadId(uploadId);
			uploadHistory.setUploadDate(Calendar.getInstance());
			uploadHistory.setCorrectedRecords(Long.parseLong("0"));
			uploadHistory.setFileName(customerCafVO.getFileName());
			uploadHistory.setFileSize(customerCafVO.getFileSize());
			uploadHistory.setNoofCols(Integer.parseInt(customerCafVO.getNoOfCols()));
			uploadHistory.setNoofRows(Long.parseLong(customerCafVO.getNoOfRows()));
			uploadHistory.setStatus(ComsEnumCodes.PENDING_FOR_PAYMENT_STATUS.getStatus());
			uploadHistory.setSuccessRecords(Long.parseLong("0"));
			uploadHistory.setUploadedBy(customerCafVO.getLoginId());
			uploadHistory.setUploadType(ComsEnumCodes.ENT_Upload_caf_Type.getCode());
			uploadHistory = uploadHistoryDao.saveUploadHistory(uploadHistory);
			if(uploadHistory.getUploadId() != null) {
				for(Caf caf : customerCafVO.getCafList()) {
					entCafStage = new EntCafStage();
					entCafStage.setContactEmail(caf.getContactEmail());
					entCafStage.setContactMobileNo(caf.getContactmobileNo());
					entCafStage.setContactName(caf.getContactPerson());
					if(caf.getInstAddress1().contains(",")) {
						entCafStage.setDistrict(caf.getInstAddress1().split(",")[0]);
						entCafStage.setMandal(caf.getInstAddress1().split(",")[1]);
					} else {
						entCafStage.setDistrict(caf.getInstAddress1());
						entCafStage.setMandal(null);
					}
					entCafStage.setLattitude(String.valueOf(caf.getLattitude()));
					entCafStage.setLongitude(String.valueOf(caf.getLongitude()));
					entCafStage.setLmocode(caf.getLmoCode());
					entCafStage.setLocation(caf.getCpePlace());
					entCafStage.setPmntrespFlag(caf.getPaymentResponsible());
					entCafStage.setStatus(ComsEnumCodes.ENT_Caf_Pending.getCode());
					entCafStage.setUploadId(uploadHistory.getUploadId());
					entCafStage.setUploadRecNo(recordNo);
					entCafStage.setApsflUniqueId(caf.getApsflUniqueId());
					entCafStage = uploadHistoryDao.saveEntCafStage(entCafStage);
					recordNo++;
				}
			}
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl::saveCafAccount() " + e);
			e.printStackTrace();
		} finally {
			
		}
		return uploadHistory;
	}

	@Override
	@Transactional
	public void updateUploadHistory(Long uploadId, Long successRecordCount) {
		try {
			uploadHistoryDao.updateUploadHistory(uploadId, successRecordCount);
		} catch (Exception e) {
			logger.error("UploadHistoryServiceImpl :: updateUploadHistory()" + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	@Transactional
	public void updateEntCafStage(EntCafStage entCafStage, String flag) {
		String status = "";
		try {
			if(!flag.equalsIgnoreCase("true")) {
				status = ComsEnumCodes.ENT_Caf_Rejected.getCode();
				uploadHistoryDao.updateEntCafStage(entCafStage.getUploadId(), entCafStage.getUploadRecNo(), flag, status);
			} else {
				status = ComsEnumCodes.ENT_Caf_Activation.getCode();
				uploadHistoryDao.updateEntCafStage(entCafStage.getUploadId(), entCafStage.getUploadRecNo(), "NA", status);
			}
		} catch (Exception e) {
			logger.error("UploadHistoryServiceImpl :: updateUploadHistory()" + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	@Transactional
	public UploadHistory saveUploadHistory(VPNServiceExcelUploadVO vpnServiceExcelUploadVO) {
		UploadHistory uploadHistory = new UploadHistory();
		VPNSrvcNamesStage vpnSrvcNamesStage = null;
		Long recordNo = 1l;
		try {
			Long uploadId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.ENT_Caf_UploadId.getCode());
			uploadHistory.setUploadId(uploadId);
			uploadHistory.setUploadDate(Calendar.getInstance());
			uploadHistory.setCorrectedRecords(Long.parseLong("0"));
			uploadHistory.setFileName(vpnServiceExcelUploadVO.getFileName());
			uploadHistory.setFileSize(vpnServiceExcelUploadVO.getFileSize());
			uploadHistory.setNoofCols(Integer.parseInt(vpnServiceExcelUploadVO.getNoofCols()));
			uploadHistory.setNoofRows(Long.parseLong(vpnServiceExcelUploadVO.getNoofRows()));
			uploadHistory.setStatus(ComsEnumCodes.PENDING_FOR_PAYMENT_STATUS.getStatus());
			uploadHistory.setSuccessRecords(Long.parseLong("0"));
			uploadHistory.setUploadedBy(vpnServiceExcelUploadVO.getLoginId());
			uploadHistory.setUploadType(ComsEnumCodes.VPN_Upload_Type.getCode());
			uploadHistory = uploadHistoryDao.saveUploadHistory(uploadHistory);
			if(uploadHistory.getUploadId() != null) {
				for(VPNSrvcVO vpnSrvcVO : vpnServiceExcelUploadVO.getVpnSrvcList()) {
					vpnSrvcNamesStage = new VPNSrvcNamesStage();
					List<Substations> substationsList = cpeInformationService.getAllSubStations(vpnSrvcVO.getSubstnUid());
					vpnSrvcNamesStage.setSubstnUid(vpnSrvcVO.getSubstnUid());
					vpnSrvcNamesStage.setCreatedBy(vpnServiceExcelUploadVO.getLoginId());
					vpnSrvcNamesStage.setCreatedOn(Calendar.getInstance());
					vpnSrvcNamesStage.setOltSerialNo(vpnSrvcVO.getOltSerialNo());
					vpnSrvcNamesStage.setVpnsrvcName(vpnSrvcVO.getVpnSrvcName());
					if(substationsList.size() > 0) {
						Substations substations = substationsList.get(0);
						vpnSrvcNamesStage.setDistrictUid(substations.getDistrictUid());
						vpnSrvcNamesStage.setMandalSlno(substations.getMandalSlno());
					}
					vpnSrvcNamesStage.setStatus(ComsEnumCodes.ENT_Caf_Pending.getCode());
					vpnSrvcNamesStage.setUploadId(uploadHistory.getUploadId());
					vpnSrvcNamesStage.setUploadRecNo(recordNo);
					vpnSrvcNamesStage = uploadHistoryDao.saveVPNSrvcNamesStage(vpnSrvcNamesStage);
					recordNo++;
				}
			}
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl::saveCafAccount() " + e);
			e.printStackTrace();
		} finally {
			
		}
		return uploadHistory;
	}

	@Override
	public List<VPNSrvcNamesStage> getAllVPNSrvcNamesStageStatus(Long uploadId) {
		List<VPNSrvcNamesStage> vpnSrvcNamesStageList = new ArrayList<>();
		try {
			vpnSrvcNamesStageList = uploadHistoryDao.getAllVPNSrvcNamesStageStatus(uploadId);
		} catch (Exception e) {
			logger.error("UploadHistoryServiceImpl :: getAllVPNSrvcNamesStageStatus()" + e);
			e.printStackTrace();
		} finally {

		}
		return vpnSrvcNamesStageList;
	}

	@Override
	@Transactional
	public void updateVPNSrvcNamesStage(VPNSrvcNamesStage vpnSrvcNamesStage, String status) {
		VPNSrvcNamesStage vpnSrvcNamesStageObj = new VPNSrvcNamesStage();
		try {
			vpnSrvcNamesStageObj = uploadHistoryDao.updateVPNSrvcNamesStage(vpnSrvcNamesStage.getUploadId(), vpnSrvcNamesStage.getUploadRecNo());
			if(!status.equalsIgnoreCase("true")) {
				vpnSrvcNamesStageObj.setStatus(ComsEnumCodes.ENT_Caf_Rejected.getCode());
				vpnSrvcNamesStageObj.setRemarks(status);
				vpnSrvcNamesStageObj = uploadHistoryDao.saveVPNSrvcNamesStage(vpnSrvcNamesStageObj);
			} else {
				vpnSrvcNamesStageObj.setStatus(ComsEnumCodes.ENT_Caf_Activation.getCode());
				vpnSrvcNamesStageObj = uploadHistoryDao.saveVPNSrvcNamesStage(vpnSrvcNamesStageObj);
			}
		} catch (Exception e) {
			logger.error("UploadHistoryServiceImpl :: updateUploadHistory()" + e);
			e.printStackTrace();
		} finally {

		}
	}

}
