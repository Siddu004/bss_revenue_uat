/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class TerminatePkgBOPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String prodCafNo;
	
	private String prodCode;
	
	private String tenantCode;
	
	private String stbSrlNo;

	public String getProdCafNo() {
		return prodCafNo;
	}

	public void setProdCafNo(String prodCafNo) {
		this.prodCafNo = prodCafNo;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getStbSrlNo() {
		return stbSrlNo;
	}

	public void setStbSrlNo(String stbSrlNo) {
		this.stbSrlNo = stbSrlNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prodCafNo == null) ? 0 : prodCafNo.hashCode());
		result = prime * result + ((prodCode == null) ? 0 : prodCode.hashCode());
		result = prime * result + ((stbSrlNo == null) ? 0 : stbSrlNo.hashCode());
		result = prime * result + ((tenantCode == null) ? 0 : tenantCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TerminatePkgBOPK other = (TerminatePkgBOPK) obj;
		if (prodCafNo == null) {
			if (other.prodCafNo != null)
				return false;
		} else if (!prodCafNo.equals(other.prodCafNo))
			return false;
		if (prodCode == null) {
			if (other.prodCode != null)
				return false;
		} else if (!prodCode.equals(other.prodCode))
			return false;
		if (stbSrlNo == null) {
			if (other.stbSrlNo != null)
				return false;
		} else if (!stbSrlNo.equals(other.stbSrlNo))
			return false;
		if (tenantCode == null) {
			if (other.tenantCode != null)
				return false;
		} else if (!tenantCode.equals(other.tenantCode))
			return false;
		return true;
	}

}
