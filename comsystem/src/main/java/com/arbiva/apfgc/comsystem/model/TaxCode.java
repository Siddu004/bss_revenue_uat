package com.arbiva.apfgc.comsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author srinivasa
 *
 */
@Entity
@Table(name = "TAXCODES")
public class TaxCode extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAXCODE")
	private String taxCode;

	@Column(name = "TAXNAME")
	private String taxName;

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

}
