/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import com.arbiva.apfgc.comsystem.model.Phonenos;

/**
 * @author Lakshman
 *
 */
public interface PhonenosService {

	public abstract Phonenos updatePhonenos(String phoneNo, String loginId, String ipAddress);
	
	public abstract Phonenos findByPhoneno(String phoneNo);
}
