/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.EnterpriseCustomerVO;

/**
 * @author Arbiva
 *
 */
public interface EnterpriseCustomerService {
	
	public abstract void deleteEnterpriseCustomers(Customer enterpriseCustomer);
	
	public abstract Customer findByEnterpriseCustomerId(Long custId);

	public abstract List<Object[]> findAllEnterpriseCustomers(String tenantCode, String tenantType);
	
	public abstract List<Customer> findAllParentAndChildEnterpriseCustomers(String tenantCode);

	public Customer saveEnterpriseCustomer(Customer enterpriseCustomer);
	
	public String findByCustomerCode(String custCode);

	public abstract Customer saveEnterpriseCustomer(EnterpriseCustomerVO enterpriseCustomerVO, String loginID, String tenantCode);

	public abstract List<Customer> findByEntParentCustCodeAndLmocode(String customerCode, String tenantCode);

	public abstract Customer saveEnterpriseCustomer(CustomerCafVO customerCafVO, EntCafStage entCafStage, String district, String mandal);

	public abstract List<Customer> findAllEntCustomers(String tenantCode);
	
	public abstract ComsHelperDTO findEnterpriseCustomers(ComsHelperDTO comsHelperDTO);

}
