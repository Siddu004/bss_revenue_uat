package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChangeBasePackage implements Serializable {

	private static final long serialVersionUID = -3558576606482230776L;

	private String acctCafNo;

	private String month;

	private String year;

	private BigDecimal oldDownSize;

	private BigDecimal oldUplSize;
	
	private BigDecimal newDownSize;

	private BigDecimal newUplSize;

	public String getAcctCafNo() {
		return acctCafNo;
	}

	public void setAcctCafNo(String acctCafNo) {
		this.acctCafNo = acctCafNo;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public BigDecimal getOldDownSize() {
		return oldDownSize;
	}

	public void setOldDownSize(BigDecimal oldDownSize) {
		this.oldDownSize = oldDownSize;
	}

	public BigDecimal getOldUplSize() {
		return oldUplSize;
	}

	public void setOldUplSize(BigDecimal oldUplSize) {
		this.oldUplSize = oldUplSize;
	}

	public BigDecimal getNewDownSize() {
		return newDownSize;
	}

	public void setNewDownSize(BigDecimal newDownSize) {
		this.newDownSize = newDownSize;
	}

	public BigDecimal getNewUplSize() {
		return newUplSize;
	}

	public void setNewUplSize(BigDecimal newUplSize) {
		this.newUplSize = newUplSize;
	}
}
