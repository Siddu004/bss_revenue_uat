/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafCharges;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Repository
public class CafChargesDao {

	private static final Logger logger = Logger.getLogger(CafChargesDao.class);
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCafCharges(CafCharges cafCharges) {
		getEntityManager().remove(cafCharges);
		getEntityManager().flush();
	}

	public CafCharges findByCafNo(Long cafNo) {
		CafCharges cafCharges = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(CafCharges.class.getSimpleName()).append(" WHERE cafno=:cafNo");
		TypedQuery<CafCharges> query = null;
			try {
				logger.info("START::findByLmoCode()");
				query = getEntityManager().createQuery(builder.toString(), CafCharges.class);
				query.setParameter("cafNo", cafNo);
				cafCharges = query.getSingleResult();
				logger.info("END::findByCafNo()");
			} catch (Exception e) {
				logger.error("EXCEPTION::findByCafNo() " + e);
			}
			finally{
				builder = null;
				query = null;
			}
		  return cafCharges;
	}

	@SuppressWarnings("unchecked")
	public List<CafCharges> findAllCafCharges() {
		return (List<CafCharges>) getEntityManager().createQuery("Select cafCharges from " + CafCharges.class.getSimpleName() + " cafCharges").getResultList();
	}

	public void saveCafCharges(CafCharges cafCharges) {
		getEntityManager().merge(cafCharges);
	}

	public void updateCafCharges(PaymentVO paymentVO, String loginID) {
		String modifiedOn = null;
		String ipAddress = null;
		StringBuilder builder = null;
		Query query = null;
		try {
			modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			if(paymentVO.getIpAddress() != null) {
				ipAddress = paymentVO.getIpAddress();
			} else {
				ipAddress = httpServletRequest.getRemoteAddr();
			}
			if(paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
				builder = new StringBuilder("update cafcharges set status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			} else {
				builder = new StringBuilder("update cafcharges set status = "+ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			}
			
			try {
				query = getEntityManager() .createNativeQuery(builder.toString());
				query.executeUpdate();
			} catch(Exception ex){
				logger.error("EXCEPTION::updateCafCharges() " + ex);
			}
		} catch(Exception e) {
			logger.error("EXCEPTION::updateCafCharges() " + e);
			e.printStackTrace();
		}
		finally{
			modifiedOn = null;
			ipAddress = null;
			builder = null;
			query = null;
		}
	}

	public void deleteCafChargesByCafNo(Long cafNo) {
		StringBuilder builder = null;
		Query query = null;
		try {
			builder = new StringBuilder("delete from cafcharges where cafno = "+cafNo+" ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();
		} catch(Exception e) {
			logger.error("EXCEPTION::deleteCafChargesByCafNo() " + e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
	}
}
