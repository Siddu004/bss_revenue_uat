/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cpecharges")
@IdClass(CpechargesPK.class)
public class Cpecharges implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "profile_id")
	private Long profileId;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectiveFrom;
	
	@Column(name = "emicount")
	private Integer emiCount;
	
	@Column(name = "effectiveto")
	private Date effectiveTo;
	
	@Column(name = "custcost")
	private BigDecimal custCost;
	
	@Column(name = "custrent")
	private BigDecimal custRent;
	
	@Column(name = "instcharges")
	private BigDecimal instcharges;
	
	@Column(name = "purchasecost")
	private BigDecimal purchaseCost;
	
	@Column(name = "emiamt")
	private BigDecimal emiAmount;
	
	@Column(name = "upfrontcharges")
	private BigDecimal upFrontCharges;
	
	@Transient
	private String onuTaxAmount;
	
	@Transient
	private String iptvTaxAmount;
	
	@Transient
	private String installationTaxAmount;
	
	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id", nullable = false, insertable=false, updatable=false)
	private CpeModal cpeModal;
	
	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public BigDecimal getCustCost() {
		return custCost;
	}

	public void setCustCost(BigDecimal custCost) {
		this.custCost = custCost;
	}

	public BigDecimal getCustRent() {
		return custRent;
	}

	public void setCustRent(BigDecimal custRent) {
		this.custRent = custRent;
	}

	public BigDecimal getInstcharges() {
		return instcharges;
	}

	public void setInstcharges(BigDecimal instcharges) {
		this.instcharges = instcharges;
	}

	public BigDecimal getPurchaseCost() {
		return purchaseCost;
	}

	public void setPurchaseCost(BigDecimal purchaseCost) {
		this.purchaseCost = purchaseCost;
	}

	public CpeModal getCpeModal() {
		return cpeModal;
	}

	public void setCpeModal(CpeModal cpeModal) {
		this.cpeModal = cpeModal;
	}

	public Integer getEmiCount() {
		return emiCount;
	}

	public void setEmiCount(Integer emiCount) {
		this.emiCount = emiCount;
	}

	public BigDecimal getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(BigDecimal emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getOnuTaxAmount() {
		return onuTaxAmount;
	}

	public void setOnuTaxAmount(String onuTaxAmount) {
		this.onuTaxAmount = onuTaxAmount;
	}

	public String getIptvTaxAmount() {
		return iptvTaxAmount;
	}

	public void setIptvTaxAmount(String iptvTaxAmount) {
		this.iptvTaxAmount = iptvTaxAmount;
	}

	public String getInstallationTaxAmount() {
		return installationTaxAmount;
	}

	public void setInstallationTaxAmount(String installationTaxAmount) {
		this.installationTaxAmount = installationTaxAmount;
	}

	public BigDecimal getUpFrontCharges() {
		return upFrontCharges;
	}

	public void setUpFrontCharges(BigDecimal upFrontCharges) {
		this.upFrontCharges = upFrontCharges;
	}
	
}
