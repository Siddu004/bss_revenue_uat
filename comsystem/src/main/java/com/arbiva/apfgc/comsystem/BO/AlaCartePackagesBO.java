package com.arbiva.apfgc.comsystem.BO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class AlaCartePackagesBO {
	
	@Id
	@Column(name="prodcode")
	private String prodcode;
	
	@Column(name="prodname")
	private String prodname;
	
	@Column(name="price")
	private String price;
	
	@Column(name="featurecodes")
	private String featurecodes;
	
	@Column(name="Recurring")
	private String recurring;
	
	@Column(name="Activation")
	private String activation;
	
	@Column(name="Depsoits")
	private String depsoits;
	

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getProdname() {
		return prodname;
	}

	public void setProdname(String prodname) {
		this.prodname = prodname;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFeaturecodes() {
		return featurecodes;
	}

	public void setFeaturecodes(String featurecodes) {
		this.featurecodes = featurecodes;
	}

	public String getRecurring() {
		return recurring;
	}

	public void setRecurring(String recurring) {
		this.recurring = recurring;
	}

	public String getActivation() {
		return activation;
	}

	public void setActivation(String activation) {
		this.activation = activation;
	}

	public String getDepsoits() {
		return depsoits;
	}

	public void setDepsoits(String depsoits) {
		this.depsoits = depsoits;
	}
	

}
