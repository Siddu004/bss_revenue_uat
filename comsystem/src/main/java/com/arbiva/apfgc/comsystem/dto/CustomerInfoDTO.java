package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;
import java.util.List;

public class CustomerInfoDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String success;
	private String successMessage;
	private String custId;
	private String custType;
	private String aadharNo;
	private String fname;
	private String lname;
	private String email;
	private String mobile;
	private String lmo;
	private String lastlogin;
	
	private List<LCODetailsDTO> lcoList;
	
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getAadharNo() {
		return aadharNo;
	}
	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLmo() {
		return lmo;
	}
	public void setLmo(String lmo) {
		this.lmo = lmo;
	}
	public String getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getSuccessMessage() {
		return successMessage;
	}
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}
	public List<LCODetailsDTO> getLcoList() {
		return lcoList;
	}
	public void setLcoList(List<LCODetailsDTO> lcoList) {
		this.lcoList = lcoList;
	}
	
}
