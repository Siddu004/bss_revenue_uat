/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
/**
 * @author Arbiva
 *
 */
@Entity
@Table(name="cafcharges")
@IdClass(CafChargesPK.class)
public class CafCharges extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "cafno")
	private Long cafno;
	
	@Id
	@Column(name = "chargecode")
	private String chargeCode;
	
	@Id
	@Column(name = "stbcafno")
	private Long stbcafno;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "chargedate")
	private Date chargeDate;
	
	@Column(name = "chargeamt")
	private Float chargeAmount;
	
	@Column(name = "lastinvmon")
	private Integer lastInvMon;
	
	@Column(name = "pmntcustid")
	private Long pmntCustId;
	
	@Column(name = "totalemicnt")
	private Integer totalEmiCount;
	
	@Column(name = "complemicnt")
	private Integer complEmiCount;
	
	@Column(name = "emistartmon")
	private Integer emiStartMonth;
	
	@Column(name = "emiendmon")
	private Integer emiEndMonth;
	
	@Column(name = "emirecentmon")
	private Integer emiRecentMonth;
	
	@ManyToOne
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable=false, updatable=false)
	private Caf cafNumber;
	
	@ManyToOne
	@JoinColumn(name = "chargecode", referencedColumnName = "chargecode", nullable = false, insertable=false, updatable=false)
	private ChargeCodes chargeCodes;
	
	public Caf getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(Caf cafNumber) {
		this.cafNumber = cafNumber;
	}

	public ChargeCodes getChargeCodes() {
		return chargeCodes;
	}

	public void setChargeCodes(ChargeCodes chargeCodes) {
		this.chargeCodes = chargeCodes;
	}

	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	
	public Date getChargeDate() {
		return chargeDate;
	}

	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	public Float getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(Float chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public Long getPmntCustId() {
		return pmntCustId;
	}

	public void setPmntCustId(Long pmntCustId) {
		this.pmntCustId = pmntCustId;
	}

	public Integer getLastInvMon() {
		return lastInvMon;
	}

	public void setLastInvMon(Integer lastInvMon) {
		this.lastInvMon = lastInvMon;
	}

	public Integer getTotalEmiCount() {
		return totalEmiCount;
	}

	public void setTotalEmiCount(Integer totalEmiCount) {
		this.totalEmiCount = totalEmiCount;
	}

	public Integer getComplEmiCount() {
		return complEmiCount;
	}

	public void setComplEmiCount(Integer complEmiCount) {
		this.complEmiCount = complEmiCount;
	}

	public Integer getEmiStartMonth() {
		return emiStartMonth;
	}

	public void setEmiStartMonth(Integer emiStartMonth) {
		this.emiStartMonth = emiStartMonth;
	}

	public Integer getEmiEndMonth() {
		return emiEndMonth;
	}

	public void setEmiEndMonth(Integer emiEndMonth) {
		this.emiEndMonth = emiEndMonth;
	}

	public Integer getEmiRecentMonth() {
		return emiRecentMonth;
	}

	public void setEmiRecentMonth(Integer emiRecentMonth) {
		this.emiRecentMonth = emiRecentMonth;
	}

	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}
	
	public Long getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(Long stbcafno) {
		this.stbcafno = stbcafno;
	}

}
