package com.arbiva.apfgc.comsystem.BO;

public class CafCountDetailsdaywiseDTO {
	
	private long today;
	private long yesterday;
	private long last_seven_days;
	private long last_one_month;
	private long last_three_months;
	private long last_six_months;
	
	public long getToday() {
		return today;
	}
	public void setToday(long today) {
		this.today = today;
	}
	public long getYesterday() {
		return yesterday;
	}
	public void setYesterday(long yesterday) {
		this.yesterday = yesterday;
	}
	public long getLast_seven_days() {
		return last_seven_days;
	}
	public void setLast_seven_days(long last_seven_days) {
		this.last_seven_days = last_seven_days;
	}
	public long getLast_one_month() {
		return last_one_month;
	}
	public void setLast_one_month(long last_one_month) {
		this.last_one_month = last_one_month;
	}
	public long getLast_three_months() {
		return last_three_months;
	}
	public void setLast_three_months(long last_three_months) {
		this.last_three_months = last_three_months;
	}
	public long getLast_six_months() {
		return last_six_months;
	}
	public void setLast_six_months(long last_six_months) {
		this.last_six_months = last_six_months;
	}
}
