/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class CafOldPackagesBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "prodcode")
	private String prodCode;

	@Column(name = "tenantcode")
	private String tenantCode;

	@Column(name = "rsagruid")
	private String agruniqueid;

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getAgruniqueid() {
		return agruniqueid;
	}

	public void setAgruniqueid(String agruniqueid) {
		this.agruniqueid = agruniqueid;
	}

}
