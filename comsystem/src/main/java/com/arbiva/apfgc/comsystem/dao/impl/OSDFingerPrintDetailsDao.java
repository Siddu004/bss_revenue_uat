/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.OSDFingerPrintDetails;

/**
 * @author Lakshman
 *
 */
@Repository
public class OSDFingerPrintDetailsDao {
	
	private static final Logger logger = Logger.getLogger(OSDFingerPrintDetailsDao.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public OSDFingerPrintDetails saveOSDFingerPrintDetails(OSDFingerPrintDetails oSDFingerPrintDetails) {
		try {
			oSDFingerPrintDetails = getEntityManager().merge(oSDFingerPrintDetails);
		} catch(Exception e) {
			oSDFingerPrintDetails = null;
			logger.error("The Exception is OSDFingerPrintDetailsDao :: saveOSDFingerPrintDetails" +e);
			e.printStackTrace();
		}
		return oSDFingerPrintDetails;
	}

	public String updateOSDFingerPrintDetails(String trackingId, String responseJson) {
		String status = "false";
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String preResponseJson = "";
		try {
			query = getEntityManager() .createNativeQuery("select response_json from osdfngprndtl where responseid = '"+trackingId+"' ");
			preResponseJson = query.getSingleResult().toString()+" "+responseJson;
			builder = new StringBuilder("update osdfngprndtl set response_json = '"+preResponseJson+"' where responseid = '"+trackingId+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			int result = query.executeUpdate();
			if(result == 1) {
				status = "true";
			} else {
				status = "false";
			}
		} catch(Exception e) {
			logger.error("EXCEPTION is OSDFingerPrintDetailsDao :: updateOSDFingerPrintDetails() " + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
			preResponseJson = null;
		}
		return status;
	}

}
