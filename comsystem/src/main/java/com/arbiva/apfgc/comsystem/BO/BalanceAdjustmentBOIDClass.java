package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

public class BalanceAdjustmentBOIDClass implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String custId;
	
	private String custinvno;
	
	private String acctcafno;
	
	private String invMonth;
	
	private String invYear;

	public String getInvYear() {
		return invYear;
	}

	public void setInvYear(String invYear) {
		this.invYear = invYear;
	}

	public String getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustinvno() {
		return custinvno;
	}

	public void setCustinvno(String custinvno) {
		this.custinvno = custinvno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acctcafno == null) ? 0 : acctcafno.hashCode());
		result = prime * result + ((custId == null) ? 0 : custId.hashCode());
		result = prime * result + ((custinvno == null) ? 0 : custinvno.hashCode());
		result = prime * result + ((invMonth == null) ? 0 : invMonth.hashCode());
		result = prime * result + ((invYear == null) ? 0 : invYear.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BalanceAdjustmentBOIDClass other = (BalanceAdjustmentBOIDClass) obj;
		if (acctcafno == null) {
			if (other.acctcafno != null)
				return false;
		} else if (!acctcafno.equals(other.acctcafno))
			return false;
		if (custId == null) {
			if (other.custId != null)
				return false;
		} else if (!custId.equals(other.custId))
			return false;
		if (custinvno == null) {
			if (other.custinvno != null)
				return false;
		} else if (!custinvno.equals(other.custinvno))
			return false;
		if (invMonth == null) {
			if (other.invMonth != null)
				return false;
		} else if (!invMonth.equals(other.invMonth))
			return false;
		if (invYear == null) {
			if (other.invYear != null)
				return false;
		} else if (!invYear.equals(other.invYear))
			return false;
		return true;
	}
	
}
