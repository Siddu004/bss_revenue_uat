/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.comsystem.BO.BillPaymentBO;
import com.arbiva.apfgc.comsystem.BO.BillSummaryBO;
import com.arbiva.apfgc.comsystem.BO.CorpusPreviousBillBO;
import com.arbiva.apfgc.comsystem.BO.PackageBO;
import com.arbiva.apfgc.comsystem.BO.PreviousBillBO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.model.CustCafHist;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;

/**
 * @author Lakshman
 *
 */
@Component("customerDao")
public class CustomerDao {

	private static final Logger LOGGER = Logger.getLogger(CustomerDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCustomers(Customer customer) {
		getEntityManager().remove(customer);
		getEntityManager().flush();
	}

	public Customer findByCustomerId(Long custId) {
		Customer customer = new Customer();
		try {
			customer = getEntityManager().find(Customer.class,custId);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return customer;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findCustomers(String tenantCode, String tenantType) {
		List<Object[]> customerList = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		/*Query query = getEntityManager() .createNativeQuery("select tenanttypelov from tenants where tenantcode = '"+tenantCode+"'");
		String tenantType = query.getSingleResult().toString();*/
		if(tenantType.trim().equalsIgnoreCase("APSFL")) {
			builder.append("select c.fname, c.lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, c.lmocode ");
			builder.append("from customers c, villages v where v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
			builder.append(" UNION select c.custname, '' as lname, c.regncode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' enttypelov, c.lmocode ");
			builder.append("from entcustomers c, villages v where v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.enttypelov != '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
		} else if(tenantType.equalsIgnoreCase("LMO")) {
			builder.append("select c.fname, c.lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, c.lmocode ");
			builder.append("from customers c, villages v where lmocode = '"+tenantCode+"' and v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
			builder.append(" UNION select c.custname, '' as lname, c.regncode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' enttypelov, c.lmocode ");
			builder.append("from entcustomers c, villages v where lmocode = '"+tenantCode+"' and v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.enttypelov != '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
		} else {
			builder.append("select c.fname, c.lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, c.lmocode from customers c, villages v ");
			builder.append("WHERE lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = 'LMO' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = 'MSP'  ");
			builder.append("AND partnercode = '"+tenantCode+"')) and v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
			builder.append(" UNION select c.custname, '' as lname, c.regncode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' enttypelov, c.lmocode from entcustomers c, villages v ");
			builder.append("WHERE lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = 'LMO' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = 'MSP'  ");
			builder.append("AND partnercode = '"+tenantCode+"')) and v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.status = 2 and c.enttypelov != '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
		}
		try {
			LOGGER.info("START::findCustomers()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			customerList = query.getResultList();
			LOGGER.info("END::findCustomers()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCustomers() " + e);
		}finally{
			query = null;
			builder = null;
		}
	  return customerList;
	}
	

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllCustomers(String tenantCode, String tenantType) {
		List<Object[]> customerList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		/*Query query = getEntityManager() .createNativeQuery("select tenanttypelov from tenants where tenantcode = '"+tenantCode+"'");
		String tenantType = query.getSingleResult().toString();*/
		if(tenantType.equalsIgnoreCase("APSFL")) {
			builder = new StringBuilder("select c.fname, c.lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, (SELECT GROUP_CONCAT(cp.phoneno) phoneno FROM cafsrvcphonenos cp, cafs cf WHERE cf.cafno = cp.cafno and c.custid = cf.custid and cf.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"') phoneNo from customers c, ");
			builder.append("villages v where v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' and c.status = 2 ");
		} 
		if(tenantType.equalsIgnoreCase("LMO")) {
			builder = new StringBuilder("select c.fname, c.lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, (SELECT GROUP_CONCAT(cp.phoneno) phoneno FROM cafsrvcphonenos cp, cafs cf WHERE cf.cafno = cp.cafno and c.custid = cf.custid and cf.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"') phoneNo from customers c, ");
			builder.append("villages v where v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district and c.lmocode = '"+tenantCode+"' and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' and c.status = 2 ");
		}
		try {
			LOGGER.info("START::findAllCustomers()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			customerList = query.getResultList();
			LOGGER.info("END::findAllCustomers()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllCustomers() " + e);
		}finally{
			query = null;
			builder = null;
			tenantType = null;
		}
	  return customerList;
	}

	public Customer saveCustomer(Customer customer) {
		return getEntityManager().merge(customer);
	}

	public Customer createCustomer(Customer customer) {
		return getEntityManager().merge(customer);
	}
	
	public Customer findByCustomerCode(String custCode) {
		List<Customer> customersList = new ArrayList<>();
		Customer customer = null;
		TypedQuery<Customer> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE custid = '"+custCode+"' OR custcode = '"+custCode+"' ");
			try {
				LOGGER.info("START::findByCustomerCode()");
				query = getEntityManager().createQuery(builder.toString(), Customer.class);
				customersList = query.getResultList();
				if(customersList.size() > 0 ) {
					customer = customersList.get(0);
				}
				LOGGER.info("END::findByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByCustomerCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return customer;
	}
	
	public Object[] findParticularCustomer(Long custId, String custType) {
		Object[] object = null;
		StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("START::findAllCustomers()");
				if(custType.equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					builder.append("select c.fname, c.lname, c.fhname, c.actualdob, c.gender, c.email1, pocmob1, c.billfreqlov, c.custtypelov , c.lmocode ");
					builder.append(" from customers c where c.custid = "+custId+" and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
				} else if(custType.equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode())) {
					builder.append("select c.custname, 'NA' as lname, 'NA' as fhname, c.dateofinc, 'NA' as gender, ");
					builder.append(" c.email1, c.pocmob1, c.billfreqlov, c.enttypelov, c.lmocode from entcustomers c where c.custid = "+custId+" and c.enttypelov != '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
				}
				Query query = getEntityManager().createNativeQuery(builder.toString());
				object = (Object[]) query.getSingleResult();
				LOGGER.info("END::findAllCustomers()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findAllCustomers() " + e);
			}finally{
				builder = null;
			}
		  return object;
	}
	
	
	public Customer findParticularCustomers(String custCode) {
		Customer customer = null;
		TypedQuery<Customer> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE custcode=:custCode");
			try {
				LOGGER.info("START::findByCustomerCode()");
				query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("custCode", custCode);
				customer = query.getSingleResult();
				LOGGER.info("END::findByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByCustomerCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return customer;
	}
	
	public List<Customer> findByParentCustomerCode(String customerCode) {
		List<Customer> customerList = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE parentcustcode=:custCode");
			try {
				LOGGER.info("START::findByParentCustomerCode()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("custCode", customerCode);
				customerList = query.getResultList();
				LOGGER.info("END::findByParentCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByParentCustomerCode() " + e);
			}finally{
				builder = null;
			}
		  return customerList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findByParentCustomerCode(String customerCode, String tenantCode) {
		List<Object[]> customerList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("select c.custname, '' as lname, c.regncode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.enttypelov, c.custid, (SELECT GROUP_CONCAT(cp.phoneno) phoneno FROM cafsrvcphonenos cp, cafs cf WHERE cf.cafno = cp.cafno and c.custid = cf.custid and cf.custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"') phoneNo from entcustomers c, ");
		builder.append(" villages v where c.lmocode = '"+tenantCode+"' and c.custid ='"+customerCode+"' and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ");
		builder.append(" and v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district ");
			try {
				LOGGER.info("START::findAllCustomers()");
				Query query = getEntityManager().createNativeQuery(builder.toString());
				customerList = query.getResultList();
				LOGGER.info("END::findAllCustomers()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findAllCustomers() " + e);
			}finally{
				builder = null;
			}
		  return customerList;
	}

	public Customer findByParentCustCode(String custCode) {
		Customer customer = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE parentcustcode =:custCode");
			try {
				LOGGER.info("START::findByParentCustCode()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("custCode", custCode);
				customer = query.getSingleResult();
				LOGGER.info("END::findByParentCustCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByParentCustCode() " + e);
			}finally{
				builder = null;
			}
		  return customer;
	}
	
	
	public Object[] findCustomerBalance(String subscribercode) {
		Object[] obj = null;
		StringBuilder builder = new StringBuilder("");

		builder.append("select distinct c.parentcafno, ca.regbal, ca.districtuid");
		builder.append(" from cafsrvcs cs, cafs c, cafacct ca");
		builder.append(" where nwsubscode = :subscribercode and c.cafno = cs.cafno and c.parentcafno = ca.acctcafno");
		try {
			LOGGER.info("START::findCustomerBalance()");
			Query query = getEntityManager().createQuery(builder.toString());
			query.setParameter("subscribercode", subscribercode);
			obj = (Object[]) query.getSingleResult();
			LOGGER.info("END::findCustomerBalance()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCustomerBalance() " + e);
		}finally{
			builder = null;
		}
		return obj;
	}
	
	
	
	
	public Object[] findPackageByPackageCode(String packageCode) {
		Object[] product = null;
		StringBuilder builder = new StringBuilder("");
			builder.append(" WHERE parentcustcode =:custCode");
		try {
			LOGGER.info("START::findPackageByPackageCode()");
			Query query = getEntityManager().createQuery(builder.toString());
			query.setParameter("custCode", packageCode);
			product = (Object[]) query.getSingleResult();
			LOGGER.info("END::findPackageByPackageCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findPackageByPackageCode() " + e);
		}finally{
			builder = null;
		}
		return product;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<CafServices> findDuplicateProduct(String packageCode, String pCafNo) {
		List<CafServices> list = new ArrayList<CafServices>();
		StringBuilder builder = new StringBuilder("");
		builder.append(" select * from cafsrvcs cs, cafs c where cs.srvccode = :packageCode and c.cafno = cs.cafno and c.parentcafno = :pCafNo");
		Query query  = null;
		try {
			LOGGER.info("START::findDuplicateProduct()");
			query = getEntityManager().createNativeQuery(builder.toString(),CafServices.class);
			query.setParameter("packageCode", packageCode);
			query.setParameter("pCafNo", pCafNo);
			list = query.getResultList();
			LOGGER.info("END::findDuplicateProduct()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findDuplicateProduct() " + e);
		}finally{
			builder = null;
			query  = null;
		}
		return list;
	}

	public String getBlackListCustomerByAadharNo(String aadharNumber) {
		String status = "false";
		String custCode = "";
		/*StringBuilder builder = new StringBuilder("SELECT custcode FROM customers where status = "+ComsEnumCodes.CUSTOMER_BLACKLIST_STATUS.getStatus()+" and custcode = '"+aadharNumber+"' ");*/
		StringBuilder builder = new StringBuilder("SELECT custcode FROM customers where custcode = '"+aadharNumber+"' ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			custCode = query.getSingleResult().toString();
			status = !custCode.isEmpty() ? "true" : "false";
		} catch(Exception ex){
			LOGGER.error("EXCEPTION::getBlackListCustomerByAadharNo() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}
	
	/*public List<UploadHistory> findCafBulkUploadErrors() {
		List<UploadHistory> cafErrorList = new ArrayList<>(); 
		TypedQuery<UploadHistory> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(UploadHistory.class.getSimpleName()).append(" WHERE noofrows != successrecs and uploadtype = :uploadtype ");
			try {
				LOGGER.info("START :: findCafBulkUploadErrors()");
				query = getEntityManager().createQuery(builder.toString(), UploadHistory.class);
				query.setParameter("uploadtype", ComsEnumCodes.ENT_Upload_caf_Type.getCode());
				cafErrorList = query.getResultList();
				LOGGER.info("END :: findCafBulkUploadErrors()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: findCafBulkUploadErrors() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cafErrorList;
	}*/
	
	public List<EntCafStage> stageCafBulkUploadErrors(String uploadId) {
		List<EntCafStage> cafErrorList = new ArrayList<>(); 
		TypedQuery<EntCafStage> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(EntCafStage.class.getSimpleName()).append(" WHERE uploadId = :uploadId and status = :status");
			try {
				LOGGER.info("START :: findCafBulkUploadErrors()");
				query = getEntityManager().createQuery(builder.toString(), EntCafStage.class);
				query.setParameter("uploadId", Long.parseLong(uploadId));
				query.setParameter("status", ComsEnumCodes.ENT_Caf_Rejected.getCode());
				cafErrorList = query.getResultList();
				LOGGER.info("END :: findCafBulkUploadErrors()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: findCafBulkUploadErrors() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cafErrorList;
	}
	
	/*public List<UploadHistory> findVPNUploadErrors() {
		List<UploadHistory> cafErrorList = new ArrayList<>(); 
		TypedQuery<UploadHistory> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(UploadHistory.class.getSimpleName()).append(" WHERE noofrows != successrecs and uploadtype = :uploadtype");
			try {
				LOGGER.info("START :: findCafBulkUploadErrors()");
				query = getEntityManager().createQuery(builder.toString(), UploadHistory.class);
				query.setParameter("uploadtype", ComsEnumCodes.VPN_Upload_Type.getCode());
				cafErrorList = query.getResultList();
				LOGGER.info("END :: findCafBulkUploadErrors()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: findCafBulkUploadErrors() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cafErrorList;
	}*/

	public List<VPNSrvcNamesStage> stageVPNUploadErrors(String uploadId) {
		List<VPNSrvcNamesStage> cafErrorList = new ArrayList<>(); 
		TypedQuery<VPNSrvcNamesStage> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(VPNSrvcNamesStage.class.getSimpleName()).append(" WHERE uploadId = :uploadId and status = :status");
			try {
				LOGGER.info("START :: findCafBulkUploadErrors()");
				query = getEntityManager().createQuery(builder.toString(), VPNSrvcNamesStage.class);
				query.setParameter("uploadId", Long.parseLong(uploadId));
				query.setParameter("status", ComsEnumCodes.ENT_Caf_Rejected.getCode());
				cafErrorList = query.getResultList();
				LOGGER.info("END :: findCafBulkUploadErrors()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: findCafBulkUploadErrors() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cafErrorList;
	}
	
	public CustCafHist saveCustCafHist(CustCafHist custCafHist) {
		try {
			getEntityManager().merge(custCafHist);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION :: saveCustCafHist() " + e);
			e.printStackTrace();
		}
		return custCafHist;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findCustomers(String tenantCode, String tenantType, PageObject pageObject, String custType, String custId) {
		List<Object[]> customerList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String whereClause = "";
		long count = 0l;
		Query countQuery = null;
		try {
			LOGGER.info("START::findCustomers()");
			if(!tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode().trim())) {
				whereClause = "and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" and c.lmocode = '"+tenantCode+"' ";
			} else {
				whereClause = "and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ";
			}
			
			if(custType != null && !custType.isEmpty() && custType.equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				whereClause = whereClause + "and c.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ";
			} else if(custType != null && !custType.isEmpty() && custType.equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && custId != null && !custId.isEmpty()) {
				whereClause = whereClause + "and c.custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' and c.custid = "+custId+" ";
			}
			
			builder = new StringBuilder("select c.custname, IFNULL(c.lname,'') lname, c.custcode, c.addr1, c.addr2, c.locality, v.villagename, c.pocmob1, c.custid, c.custtypelov, c.aadharno, (SELECT GROUP_CONCAT(cp.phoneno) phoneno FROM cafsrvcphonenos cp, cafs cf WHERE cf.cafno = cp.cafno and c.custid = cf.custid and cf.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"') phoneNo from customermst c, ");
			builder.append("villages v where v.villageslno = c.city_village and v.mandalslno = c.mandal and v.districtuid = c.district "+whereClause+" ");
			
			String like = "AND (custname LIKE '%" + pageObject.getSearchParameter() + "%' OR lname LIKE '%" + pageObject.getSearchParameter() + "%' OR addr1 LIKE '%" + pageObject.getSearchParameter() + "%'  OR pocmob1 LIKE '%" + pageObject.getSearchParameter() + "%'  OR aadharno LIKE '%" + pageObject.getSearchParameter() + "%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString());
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			customerList = query.getResultList();
			
			LOGGER.info("END::findCustomers()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return customerList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafBulkUploadOrVPNUploadErrors(PageObject pageObject,String uploadType) {
		List<Object[]> cafErrorList = new ArrayList<>(); 
		Query query = null;
		long count = 0l;
		Query countQuery = null;
		StringBuilder builder = new StringBuilder();
		String searchParameter = "";
		try {
			builder = new StringBuilder(" SELECT uploadid,filename,IFNULL(DATE_FORMAT(uploaddate,'%d/%m/%Y'), 'NA'),noofrows,successrecs FROM uploadhist WHERE ");
					  builder.append(" noofrows != successrecs AND uploadtype='"+uploadType+"'");
			
			if(pageObject.getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
				} catch(Exception e) {
					searchParameter = pageObject.getSearchParameter();
				}
			} else {
				searchParameter = pageObject.getSearchParameter();
			}
			String like = "AND (uploadid LIKE '%"+searchParameter+"%' OR filename LIKE '%"+searchParameter+"%'  OR noofrows LIKE '%"+searchParameter+"%'  OR successrecs LIKE '%"+searchParameter+"%' OR uploaddate LIKE '%"+searchParameter+"%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString());
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			cafErrorList = query.getResultList();
			
			LOGGER.info("END::getVPNUploadErrors()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getVPNUploadErrors() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cafErrorList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCustDtlsByLoginid(String loginId) {
		
		Query query = null;
		StringBuilder builder;
		List<Object[]> list = new ArrayList<>();
		try {
			LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
			builder = new StringBuilder("SELECT DISTINCT cust.custid,cust.custtypelov,cust.aadharno,cust.custname,cust.lname,cust.email1,cust.pocmob1,cf.lmocode ");
			builder.append("FROM customermst cust,cafs cf WHERE cust.custname = '"+loginId+"' OR cust.lname = '"+loginId+"' AND cf.custid = cust.custid");
			query = getEntityManager().createNativeQuery(builder.toString());
			list = query.getResultList();
			LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
		}catch(Exception e) {
			LOGGER.error("CustomerDao :: getCustDtlsByLoginid() :: "+e);
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getNearestLCO(String districtId, String mandalId, String villageId) {
		Query query = null;
		StringBuilder builder;
		List<Object[]> list = new ArrayList<>();
		try {
			LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
			builder = new StringBuilder("SELECT tb.tenantcode,tenantname,regoff_addr1,regoff_addr2,regoff_locality,regoff_area,regoff_city, ");
			builder.append(" regoff_state,regoff_pin,regoff_email1,regoff_pocname,regoff_pocmob1 FROM tenantbusareas tb,tenants t ");
			builder.append(" WHERE tb.tenantcode=t.tenantcode AND tenanttypelov = 'LMO' AND districtuid="+districtId+" AND mandalslno="+mandalId+" AND villageslno="+villageId+" ");
			query = getEntityManager().createNativeQuery(builder.toString());
			list = query.getResultList();
			LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
		}catch(Exception e) {
			LOGGER.error("CustomerDao :: getCustDtlsByLoginid() :: "+e);
			e.printStackTrace();
		}
		return list;
	}

	public Customer findByCustomerCodeCSS(String aadharNo) {
		
		List<Customer> customersList = new ArrayList<>();
		Customer customer = null;
		TypedQuery<Customer> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE aadharno = '"+aadharNo+"' ");
			try {
				LOGGER.info("START::findByCustomerCode()");
				query = getEntityManager().createQuery(builder.toString(), Customer.class);
				customersList = query.getResultList();
				if(customersList.size() > 0 ) {
					customer = customersList.get(0);
				}
				LOGGER.info("END::findByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByCustomerCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return customer;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getLmoDetails(String aadharNo) {
			
			List<Object[]> customersList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
				builder = new StringBuilder("SELECT cf.cafno,cust.custname,cust.lname,t.tenantname,cust.pocmob1,cust.email1 FROM cafs cf,tenants t,customermst cust ");
				builder.append(" WHERE cf.aadharno = '"+aadharNo+"' AND cf.lmocode = t.tenantcode AND cf.aadharno = cust.aadharno;");
				query = getEntityManager().createNativeQuery(builder.toString());
				customersList = query.getResultList();
				LOGGER.info("CustomerDao :: getCustDtlsByLoginid() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustDtlsByLoginid() :: "+e);
				e.printStackTrace();
			}
			
			return customersList;
		}

	@SuppressWarnings("unchecked")
	public List<BigInteger> getCafNos(String aadharNo) {
			
			List<BigInteger> cafList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getCafNos() :: Start");
				builder = new StringBuilder("SELECT cf.cafno FROM cafs cf where  cf.aadharno = '"+aadharNo+"' ");
				query = getEntityManager().createNativeQuery(builder.toString());
				cafList = query.getResultList();
				LOGGER.info("CustomerDao :: getCafNos() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCafNos() :: "+e);
				e.printStackTrace();
			}
			
			return cafList;
		}

		
		@SuppressWarnings("unchecked")
		public List<Object[]> getHsiUsageDtls(BigInteger cafNo,int year,int month) {
			
			List<Object[]> hsiList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getHsiUsageDtls() :: Start");
				builder = new StringBuilder("SELECT cf.cafno,cust.custname,cust.lname,ROUND((hc.dnldsize + hc.upldsize)/1024/1024/1024,3) AS usagelimit,ROUND((hc.bstrupldsize+hc.bstrdnldsize)/1024/1024/1024,3) AS totallimit  ");
				builder.append(" FROM cafs cf,customermst cust,hsicumusage hc ");
				builder.append(" WHERE cf.cafno = hc.acctcafno AND cf.cafno = "+cafNo+" AND usageyyyy="+year+" AND usagemm = "+month+" AND cf.custid = cust.custid; ");
				query = getEntityManager().createNativeQuery(builder.toString());
				hsiList = query.getResultList();
				LOGGER.info("CustomerDao :: getHsiUsageDtls() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getHsiUsageDtls() :: "+e);
				e.printStackTrace();
			}
			
			return hsiList;
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getTTDtls(BigInteger cafNo) {
			
			List<Object[]> ttList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getTTDtls() :: Start");
				builder = new StringBuilder("SELECT cafno,ws.statusdesc,COUNT(1)   ");
				builder.append(" FROM tt_tickets tt,wipstages ws WHERE ws.appcode='TT' AND ws.statuscode=tt.status ");
				builder.append(" AND cafno='"+cafNo+"' GROUP BY ws.statusdesc; ");
				query = getEntityManager().createNativeQuery(builder.toString());
				ttList = query.getResultList();
				LOGGER.info("CustomerDao :: getTTDtls() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getTTDtls() :: "+e);
				e.printStackTrace();
			}
			
			return ttList;
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getPmntDtls(BigInteger cafNo) {
			
			List<Object[]> pmntList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getPmntDtls() :: Start");
				builder = new StringBuilder("SELECT cf.cafno,cust.custname,cust.lname,pmnt.pmntamt,DATE_FORMAT(pmnt.pmntdate,'%d-%m-%Y') FROM cafs cf,customermst cust,payments pmnt  ");
				builder.append(" WHERE cf.cafno= "+cafNo+" AND cf.custid = cust.custid AND cf.cafno = pmnt.acctcafno AND cust.custid = pmnt.pmntcustid ORDER BY pmntdate DESC LIMIT 1 ");
				query = getEntityManager().createNativeQuery(builder.toString());
				pmntList = query.getResultList();
				LOGGER.info("CustomerDao :: getPmntDtls() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getPmntDtls() :: "+e);
				e.printStackTrace();
			}
			
			return pmntList;
		}

		@SuppressWarnings("unchecked")
		public List<Object[]> getCustomerDetailsByNWSubscriberCode(String nwSubscriberCode) {
			List<Object[]> customerList = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" SELECT c.custid, CONCAT(c.custname, ' ', ifnull(c.lname, '')) customerName, c.pocmob1, c.regbal + c.chargedbal dueAmount, cf.cafno, c.lastinvid "); 
				builder.append(" FROM customermst c, cafstbs cfs, cafs cf WHERE c.custid = cf.custid AND cf.cafno = cfs.parentcafno  AND cfs.nwsubscode = '"+nwSubscriberCode+"' ");
				query = getEntityManager().createNativeQuery(builder.toString());
				customerList = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return customerList;
		}

		@SuppressWarnings("unchecked")
		public List<Object[]> getCustomerInvoiceDetails(String custId, String lastInvNo) {
			List<Object[]> customerInvoiceObject = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerInvoiceDetails() :: Start");
				builder.append(" select ci.custinvno, date_format(ci.invdate, '%d/%m/%Y') invdate, ci.invamt + ci.srvctax + ci.swatchtax + ci.kisantax + ci.enttax invAmount ");
				builder.append(" from custinv ci  where ci.pmntcustid = "+custId+" and ci.custinvno = "+lastInvNo+" ");
				query = getEntityManager().createNativeQuery(builder.toString());
				customerInvoiceObject = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerInvoiceDetails() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerInvoiceDetails() :: "+e);
				e.printStackTrace();
			}
			return customerInvoiceObject;
		}

		@SuppressWarnings("unchecked")
		public List<Object[]> getCustomerHSIDetails(String nwsubscribercode) {
			List<Object[]> customerHSIObject = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerHSIDetails() :: Start");
				builder.append(" select ROUND((hu.dnldsize + hu.upldsize) / 1024 / 1024 / 1024, 2) hsiusage, ROUND((hu.basednldsize + hu.baseupldsize) / 1024 / 1024 / 1024, 2) hsilimit ");
				builder.append(" , case when status = 2 then 'Y' else 'N' end status from hsicumusage hu, cafstbs cfs where ");
				builder.append(" hu.usageyyyy = date_format(curdate(), '%Y') AND hu.usagemm = date_format(curdate(), '%m') ");
				builder.append(" AND hu.acctcafno = cfs.parentcafno AND cfs.nwsubscode='"+nwsubscribercode+"' ");
				query = getEntityManager().createNativeQuery(builder.toString());
				customerHSIObject = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerHSIDetails() :: Start");
			} catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerHSIDetails() :: "+e);
				e.printStackTrace();
			}
			return customerHSIObject;
		}

		@SuppressWarnings("unchecked")
		public List<Object[]> getCustomerPhoneDetails(String nwsubscribercode) {
			List<Object[]> customerPhoneObject = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerPhoneDetails() :: Start");
				builder.append(" SELECT SUM(units) FROM phonecumusage p, cafstbs cfs ");
				builder.append(" WHERE p.usageyyyy = date_format(curdate(), '%Y') AND p.usagemm = date_format(curdate(), '%m') ");
				builder.append(" AND p.acctcafno = cfs.parentcafno AND cfs.nwsubscode = '"+nwsubscribercode+"' GROUP BY acctcafno, customerid, usageyyyy, usagemm ");
				query = getEntityManager().createNativeQuery(builder.toString());
				customerPhoneObject = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerPhoneDetails() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerPhoneDetails() :: "+e);
				e.printStackTrace();
			}
			return customerPhoneObject;
		}

		public CorpusPreviousBillBO getCustomerBillDetailsByNWSubscriberCode(String nwSubscriberCode) {
			CorpusPreviousBillBO customerObject = new CorpusPreviousBillBO();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" SELECT cf.pmntcustid custid, upper(CONCAT(c.custname, ' ', ifnull(c.lname, ''), ' ', ifnull(c.mname, ''))) customerName, "); 
				builder.append(" UPPER(CONCAT(c.addr1, ' ', ifnull(c.addr2, ''), ' ', ifnull(c.locality, ''), ' ', ifnull(v.villagename, ''), ' ', ifnull(m.mandalname, ''), ' ', ifnull(d.districtname, ''), ' ', ifnull(c.pin, ''))) address, ");
				builder.append(" cp.phoneno, cf.cafno FROM customermst c, cafstbs cfs, cafs cf, villages v, mandals m, districts d, cafsrvcphonenos cp ");
				builder.append(" WHERE c.custid = cf.custid AND cf.cafno = cfs.parentcafno  AND cfs.nwsubscode='"+nwSubscriberCode+"' and c.district = d.districtuid ");
				builder.append(" and c.mandal = m.mandalslno and c.district = m.districtuid  and c.city_village = v.villageslno and c.mandal = v.mandalslno ");
				builder.append(" and c.district = v.districtuid and cp.parentcafno = cf.cafno ");
				query = getEntityManager().createNativeQuery(builder.toString(), CorpusPreviousBillBO.class);
				customerObject = (CorpusPreviousBillBO) query.getSingleResult();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return customerObject;
		}

		@SuppressWarnings("unchecked")
		public List<PreviousBillBO> getPreviousBillDetails(String customerId) {
			List<PreviousBillBO> billList = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" SELECT cui.prevbal, cui.prevpaid, (cui.prevbal - cui.prevpaid) balamnt, cui.custinvno, date_format(cui.invdate, '%d-%m-%Y') invDate, date_format(cui.invduedate, '%d-%m-%Y') invDueDate, "); 
				builder.append(" concat(date_format(cui.invfdate, '%d-%m-%Y'), ' to ', date_format(cui.invtdate, '%d-%m-%Y')) billperiod, 0 adjustments, ");
				builder.append(" cui.invamt + cui.srvctax + cui.swatchtax + cui.kisantax + cui.enttaX invamt, (cui.invamt + cui.srvctax + cui.swatchtax + cui.kisantax + cui.enttaX + cui.prevbal - cui.prevpaid - 0) payableamt ");
				builder.append(" FROM custinv cui WHERE cui.invmn IN (MONTH(NOW())-1,MONTH(NOW())-3,MONTH(NOW())-2) and pmntcustid = "+customerId+" ");
				query = getEntityManager().createNativeQuery(builder.toString(), PreviousBillBO.class);
				billList = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return billList;
		}

		public BillSummaryBO getBillSummary(String billNo) {
			BillSummaryBO billSummaryBO = new BillSummaryBO();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" SELECT SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode <> 'VOD'    THEN chargeamt ELSE 0 END) onetimecharges "); 
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode <> 'VOD'    THEN srvctax   ELSE 0 END) onetimesrvctax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode <> 'VOD'    THEN swatchtax ELSE 0 END) onetimeswtax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode <> 'VOD'    THEN kisantax  ELSE 0 END) onetimekktax "); 
				builder.append("  ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode <> 'VOD'    THEN enttax    ELSE 0 END) onetimeenttax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode =  'VOD'    THEN chargeamt ELSE 0 END) vodcharges ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode =  'VOD'    THEN srvctax   ELSE 0 END) vodsrvctax "); 
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode =  'VOD'    THEN swatchtax ELSE 0 END) vodswtax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode =  'VOD'    THEN kisantax  ELSE 0 END) vodkktax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='PURCHASE' AND ccd.chargecode =  'VOD'    THEN enttax    ELSE 0 END) vodenttax "); 
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargetypeflag IN(1,8) THEN chargeamt ELSE 0 END) rentalcharges ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargetypeflag IN(1,8) THEN srvctax   ELSE 0 END) rentalsrvctax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargetypeflag IN(1,8) THEN swatchtax ELSE 0 END) rentalswtax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargetypeflag IN(1,8) THEN kisantax  ELSE 0 END) rentalskktax "); 
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargetypeflag IN(1,8) THEN enttax    ELSE 0 END) rentalenttax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargecode IN('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE') THEN chargeamt ELSE 0 END) tpusagecharges ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargecode IN('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE') THEN srvctax   ELSE 0 END) tpsrvctax "); 
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargecode IN('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE') THEN swatchtax ELSE 0 END) tpswtax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargecode IN('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE') THEN kisantax  ELSE 0 END) tpkktax ");
				builder.append(" ,SUM(CASE WHEN ccd.collectionpoint='INVOICE'  AND ccd.chargecode IN('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE') THEN enttax    ELSE 0 END) tpenttax "); 
				builder.append(" FROM cafinvdtls invd, chargecodes ccd ");
				builder.append(" WHERE ccd.chargecode=invd.chargecode  ");
				builder.append(" AND invd.custinvno IS NOT NULL "); 
				builder.append(" AND invd.custinvno = "+billNo+"  ");
				query = getEntityManager().createNativeQuery(builder.toString(), BillSummaryBO.class);
				billSummaryBO = (BillSummaryBO) query.getSingleResult();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return billSummaryBO;
		}

		@SuppressWarnings("unchecked")
		public List<PackageBO> getPackageList(String accountNo) {
			List<PackageBO> packageList = new ArrayList<>();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" select p.prodname, MIN(cs.actdate) actdate from cafsrvcs cs, products p where cs.actdate between p.effectivefrom and p.effectiveto "); 
				builder.append(" and cs.tenantcode  = p.tenantcode and cs.prodcode = p.prodcode and cs.status IN (6,7) and cs.parentcafno = "+accountNo+" GROUP BY p.prodname ");
				query = getEntityManager().createNativeQuery(builder.toString(), PackageBO.class);
				packageList = query.getResultList();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return packageList;
		}

		public BillPaymentBO getLastPaymentDetails(String customerId) {
			BillPaymentBO billPaymentBO = new BillPaymentBO();
			Query query = null;
			StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
				builder.append(" select  pmntmodelov, pmntamt, date_format(pmntdate, '%d-%m-%Y') pmntdate from payments where pmntcustid = "+customerId+" order by pmntid desc limit 1 "); 
				query = getEntityManager().createNativeQuery(builder.toString(), BillPaymentBO.class);
				billPaymentBO = (BillPaymentBO) query.getSingleResult();
				LOGGER.info("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: Start");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getCustomerDetailsByNWSubscriberCode() :: "+e);
				e.printStackTrace();
			}
			return billPaymentBO;
		}
		
		
		/** 
		 * 
		 * @author vinay
		 * @param cutomerid
		 * */
		@SuppressWarnings("unchecked")
		public List<Object[]> getEnterpriseHsiUsageDtls(String custid,int year,int month) {
			
			List<Object[]> hsiList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getHsiUsageDtls() :: Start");
				builder = new StringBuilder("SELECT cf.cafno,cust.custname,cust.lname,ROUND((hc.dnldsize + hc.upldsize)/1024/1024/1024,3) AS currentUsage,ROUND((hc.bstrupldsize+hc.bstrdnldsize)/1024/1024/1024,3) AS totallimit  ");
				builder.append(" FROM cafs cf,customermst cust,hsicumusage hc ");
				builder.append(" WHERE cf.cafno = hc.acctcafno AND cust.custid = "+custid+" AND usageyyyy="+year+" AND usagemm = "+month+" AND cf.custid = cust.custid; ");
				query = getEntityManager().createNativeQuery(builder.toString());
				hsiList = query.getResultList();
				LOGGER.info("CustomerDao :: getHsiUsageDtls() :: End");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getHsiUsageDtls() :: "+e);
				e.printStackTrace();
			}
			
			return hsiList;
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getEnterprisePhoneUsageDtls(String custid,int year,int month) {
			
			List<Object[]> phoneUsageList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getEnterprisePhoneUsageDtls() :: Start");
				builder = new StringBuilder("select acctcafno,units,charge  ");
				builder.append(" from phonecumusage ");
				builder.append(" where customerid = "+custid+" AND usageyyyy="+year+" AND usagemm = "+month+";");
				query = getEntityManager().createNativeQuery(builder.toString());
				phoneUsageList = query.getResultList();
				LOGGER.info("CustomerDao :: getEnterprisePhoneUsageDtls() :: End");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getEnterprisePhoneUsageDtls() :: "+e);
				e.printStackTrace();
			}
			
			return phoneUsageList;
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getPhoneUsageDtls(String acctcaf,int year,int month) {
			
			List<Object[]> phoneUsageList = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("Fetching phone details from phoneusage table");
				builder = new StringBuilder("select cldpartyaddr,starttime,endtime,calldurn,billunits,callcharge  ");
				builder.append(" from phoneusage ");
				builder.append(" where acctcafno = "+acctcaf+" AND usageyyyy="+year+" AND usagemm = "+month+";");
				query = getEntityManager().createNativeQuery(builder.toString());
				phoneUsageList = query.getResultList();
				LOGGER.info("Successfully fetched Phone details");
			}catch(Exception e) {
				LOGGER.error("Error while fetching phone usage details: "+e);
				e.printStackTrace();
			}
			
			return phoneUsageList;
		}
		
		
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getEnterpriseCafDtls(String custid) {
			
			List<Object[]> cafDetails = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getEnterpriseCafDtls() :: Start");
				builder = new StringBuilder("SELECT cafno, contactperson, contactemail, contactmob, cpeprofileid, agorahsisubscode  ");
				builder.append(" FROM cafs  ");
				builder.append(" WHERE STATUS = 6 AND custid = "+custid+";");
				query = getEntityManager().createNativeQuery(builder.toString());
				cafDetails = query.getResultList();
				LOGGER.info("CustomerDao :: getEnterpriseCafDtls() :: End");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getEnterpriseCafDtls() :: "+e);
				e.printStackTrace();
			}
			
			return cafDetails;
		}
		
		@SuppressWarnings("unchecked")
		public List<Object[]> getEnterpriseCutomerDtls(String custid) {
			
			List<Object[]> cafDetails = new ArrayList<>();
			Query query = null;
			StringBuilder builder;
			try {
				LOGGER.info("CustomerDao :: getEnterpriseCutomerDtls() :: Start");
				builder = new StringBuilder("SELECT custid, custname NAME, pocname, pocmob1, addr1, addr2, villagename, mandalname, districtname  ");
				builder.append(" FROM customermst mst, districts d, mandals m, villages v  ");
				builder.append(" WHERE 1=1 AND D.DISTRICTUID = mst.district AND m.mandalslno = mst.mandal AND D.DISTRICTUID = m.DISTRICTUID AND m.mandalslno = v.mandalslno AND D.DISTRICTUID = v.DISTRICTUID AND v.villageslno = mst.city_village  ");
				builder.append(" AND custid = "+custid+";");
				query = getEntityManager().createNativeQuery(builder.toString());
				cafDetails = query.getResultList();
				LOGGER.info("CustomerDao :: getEnterpriseCutomerDtls() :: End");
			}catch(Exception e) {
				LOGGER.error("CustomerDao :: getEnterpriseCutomerDtls() :: "+e);
				e.printStackTrace();
			}
			
			return cafDetails;
		}	
		
		
}

