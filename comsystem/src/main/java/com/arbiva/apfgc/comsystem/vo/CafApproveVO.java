/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class CafApproveVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mspCode;
	
	private String prodCode;
	
	private String cafNo;
	
	private String srvcCode;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
