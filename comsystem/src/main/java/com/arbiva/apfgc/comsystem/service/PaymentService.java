/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;
import java.util.Map;

import com.arbiva.apfgc.comsystem.vo.CafDetailsVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentDetailsVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;
import com.arbiva.apfgc.comsystem.vo.TaxesVO;

/**
 * @author Lakshman
 *
 */
public interface PaymentService {
	
	//modified by chaitanya_xyz
	
	public abstract String createPayment(PaymentVO paymentVO, String loginID, String onuNumber,int cardnum) throws Exception;

	public abstract CafDetailsVO getRecentPayment(Long cafNo);

	public abstract void saveMonthlyPayment(PaymentVO paymentVO, String loginId) throws Exception;
	
	public String generateChargesAndWorkorderProcessPayments(PaymentVO paymentVO);
	
	public TaxesVO getTaxAmounts(String srvcCode, String chrgCode, String region, String taxLevelFlag, String chrgAmount);

	//modified by chaitanya_xyz
	
	public abstract String saveEnterpriseCustomerPayment(PaymentVO paymentVO, String loginId, String onuNumber,int cardnum) throws Exception;

	public abstract String processWOPayments(PaymentVO paymentVO);

	public abstract Map<String, String> getCafUsage(String yyyy, String mm, String cafNo);

	public abstract List<PaymentDetailsVO> getTTPayment(Long cafNo);

	public abstract String saveAddpackage(PaymentVO paymentVO, List<ProductsVO> changePkgList, String message) throws Exception;

	public abstract String generateChargesAndAddPackagePayments(CustomerCafVO customerCafVO, Long addPkgCafNo, Float paidAmount, Long stbCafNo);
	
	public abstract String generateChargesForReverseSecDeposit(CustomerCafVO customerCafVO, List<ProductsVO> changePkgList);

	public abstract String processCSSWOPayments(PaymentVO paymentVO);
	
	//added by @chaitanya_XYZ
	
	public abstract int getCardDetails(String popslno);
	
	public abstract void updateCafCard(long cafno,int cardnum);



	public void saveBulkMonthlyPayment(PaymentVO paymentVO, String loginId) throws Exception;

	public abstract PaymentVO getCafDetails(Long valueOf,String date);

	public abstract Boolean checkOltstatus(String oltSrlNo);
	
	public abstract int getCardDetailsByCaf(String cafno);
	public abstract boolean checkOltstatus(String popId, String popMandal);
}
