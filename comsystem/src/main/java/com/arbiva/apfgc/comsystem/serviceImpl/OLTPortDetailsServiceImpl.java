/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.OLTPortDetailsDao;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;

/**
 * @author Lakshman
 *
 */
@Service
public class OLTPortDetailsServiceImpl {

	private static final Logger logger = Logger.getLogger(OLTPortDetailsServiceImpl.class);

	@Autowired
	OLTPortDetailsDao oltPortDetailsDao;
	

	public OLTPortDetails saveOLTPortDetails(OLTPortDetails oLTPortDetails) {
		OLTPortDetails olPortDetails = new OLTPortDetails();
		try {
			olPortDetails = oltPortDetailsDao.saveOLTPortDetails(oLTPortDetails);
		} catch (Exception e) {
			logger.error("OLTPortDetailsServiceImpl :: saveOLTPortDetails()" + e);
			e.printStackTrace();
		} finally {

		}
		return olPortDetails;
	}

	public List<OLTPortDetails> getAllOLTPortDetailsByTenantCode(String tenantCode) {
		List<OLTPortDetails> olPortDetailsList = new ArrayList<>();
		try {
			olPortDetailsList = oltPortDetailsDao.getAllOLTPortDetailsByTenantCode(tenantCode);
		} catch (Exception e) {
			logger.error("OLTPortDetailsServiceImpl :: getAllOLTPortDetailsByTenantCode()" + e);
			e.printStackTrace();
		} finally {

		}
		return olPortDetailsList;
	}

	@Transactional
	public OLTPortDetails updateOLTPortDetails(CustomerCafVO customerCafVO,int cardnum) {
		OLTPortDetails olPortDetails = null;
		String usedSlots = "";
		String splitterValue = customerCafVO.getL1Slot()+"-"+customerCafVO.getL2Slot()+"-"+customerCafVO.getL3Slot()+",";
		try {
			
			olPortDetails = oltPortDetailsDao.getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId(customerCafVO.getOltId(),cardnum, Integer.parseInt(customerCafVO.getOltPortId()), splitterValue);
			if(olPortDetails != null) {
				usedSlots = splitterValue+olPortDetails.getL3SlotsUsed();
			} else {
				usedSlots = splitterValue;
			}
			olPortDetails.setL3SlotsUsed(usedSlots);
			olPortDetails = oltPortDetailsDao.saveOLTPortDetails(olPortDetails);
		} catch (Exception e) {
			logger.error("OLTPortDetailsServiceImpl :: saveOLTPortDetails()" + e);
			e.printStackTrace();
		} finally {
			usedSlots = null;
		}
		return olPortDetails;
	}

	public String checkSplitterValue(String oltSrlNo,int cardnum,Integer oltPort, String portSlotValue) {
		String status = "false";
		OLTPortDetails oltPortDetails = null;
		try {
			oltPortDetails = oltPortDetailsDao.getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId(oltSrlNo, cardnum, oltPort, portSlotValue);
			status = oltPortDetails == null ? "true" : "false";
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getCpeModelByProfileId()" + e);
			e.printStackTrace();
		} finally {
			oltPortDetails = null;
		}
		return status;

	}
}
