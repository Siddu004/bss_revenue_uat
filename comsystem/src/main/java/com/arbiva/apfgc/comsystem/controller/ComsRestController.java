package com.arbiva.apfgc.comsystem.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.comsystem.BO.BalAdjCafinvDtslBO;
import com.arbiva.apfgc.comsystem.BO.BalanceAdjustmentBO;
import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdaywiseDTO;
import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdistrictwiseDTO;
import com.arbiva.apfgc.comsystem.BO.CafOldPackagesBO;
import com.arbiva.apfgc.comsystem.BO.ChangePkgBO;
import com.arbiva.apfgc.comsystem.BO.CorpusPreviousBillBO;
import com.arbiva.apfgc.comsystem.BO.CountdetailsCustomerwiseDTO;
import com.arbiva.apfgc.comsystem.BO.CpeChargeDetailsBO;
import com.arbiva.apfgc.comsystem.BO.CustomerDetailsBO;
import com.arbiva.apfgc.comsystem.BO.Details;
import com.arbiva.apfgc.comsystem.BO.FingerPrintBO;
import com.arbiva.apfgc.comsystem.BO.HsiBO;
import com.arbiva.apfgc.comsystem.BO.MonthlyPaymentBO;
import com.arbiva.apfgc.comsystem.BO.NoOfSTBsBO;
import com.arbiva.apfgc.comsystem.BO.OntIdCustTypeBO;
import com.arbiva.apfgc.comsystem.BO.SubstationsBO;
import com.arbiva.apfgc.comsystem.BO.TenantsBO;
import com.arbiva.apfgc.comsystem.BO.TerminatePkgBO;
import com.arbiva.apfgc.comsystem.BO.VillagesBO;
import com.arbiva.apfgc.comsystem.dto.AadharDTO;
import com.arbiva.apfgc.comsystem.dto.AddOnPackageDTO;
import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.dto.ChangeBasePackage;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.ComsmobilehelperDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.model.ChargeCodes;
import com.arbiva.apfgc.comsystem.model.ChargeTaxes;
import com.arbiva.apfgc.comsystem.model.CpeModal;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.FeatureParams;
import com.arbiva.apfgc.comsystem.model.Lovs;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.model.MonthlyPayment;
import com.arbiva.apfgc.comsystem.model.MultiAction;
import com.arbiva.apfgc.comsystem.model.OLT;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;
import com.arbiva.apfgc.comsystem.model.SrvcFeatures;
import com.arbiva.apfgc.comsystem.model.StatusCodes;
import com.arbiva.apfgc.comsystem.model.Substations;
import com.arbiva.apfgc.comsystem.model.TaxMaster;
import com.arbiva.apfgc.comsystem.model.TaxRegionMapping;
import com.arbiva.apfgc.comsystem.model.UploadHistory;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.model.Villages;
import com.arbiva.apfgc.comsystem.util.ApplictionAutoWires;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.util.FileUtil;
import com.arbiva.apfgc.comsystem.util.PaginationObject;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.BillSummaryVO;
import com.arbiva.apfgc.comsystem.vo.CafAndCpeChargesVO;
import com.arbiva.apfgc.comsystem.vo.CafDetailsVO;
import com.arbiva.apfgc.comsystem.vo.CafsVO;
import com.arbiva.apfgc.comsystem.vo.CorpusBillVO;
import com.arbiva.apfgc.comsystem.vo.CorpusResponceStatus;
import com.arbiva.apfgc.comsystem.vo.CorpusResponseVO;
import com.arbiva.apfgc.comsystem.vo.CpeInformationVO;
import com.arbiva.apfgc.comsystem.vo.CpeStockVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafModifyVo;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.EnterpriseCustomerVO;
import com.arbiva.apfgc.comsystem.vo.FeatureParamsVO;
import com.arbiva.apfgc.comsystem.vo.FilterPackagesVO;
import com.arbiva.apfgc.comsystem.vo.FingerPrintJson;
import com.arbiva.apfgc.comsystem.vo.HSICummSumPMonthVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthly;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthlyCustViewVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryPreviousMonthVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.InvoiceAmountDtls;
import com.arbiva.apfgc.comsystem.vo.PaymentDetailsVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;
import com.arbiva.apfgc.comsystem.vo.Region;
import com.arbiva.apfgc.comsystem.vo.SearchDataVO;
import com.arbiva.apfgc.comsystem.vo.SplitterVO;
import com.arbiva.apfgc.comsystem.vo.TenantBusinessAreas;
import com.arbiva.apfgc.comsystem.vo.TenantVO;
import com.arbiva.apfgc.comsystem.vo.TenantWalletVO;
import com.arbiva.apfgc.comsystem.vo.VPNServiceExcelUploadVO;
import com.google.gson.Gson;
import com.xyzinnotech.bss.sms.dto.SmsDTO;
import com.xyzinnotech.bss.sms.service.SmsApi;

@RestController
public class ComsRestController {

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	ApplictionAutoWires aw;
	
	@Autowired
	SmsApi smsApi;
	
	@Value("${CAT-USERNAME}")
	private String comUserName;

	@Value("${CAT-PWD}")
	private String comPwd;
	
	@Value("${HSI-USERNAME}")
	private String hsiUserName;

	@Value("${HSI-PWD}")
	private String hsiPwd;

	@Value("${catURL}")
	private String catURL;
	
	@Value("${hsiAddonURL}")
	private String hsiAddonURL;
	
	@Value("${hsiBaseURL}")
	private String hsiBaseURL;

	@Value("${state}")
	private String state;
	
	private static final Logger logger = Logger.getLogger(ComsRestController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Object[]> home(@RequestParam(value = "tenantCode") String tenantCode) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: home() :: START");
			customerList = aw.getCafService().getCustomerInformationByTenantCode(tenantCode);
			logger.info("ComsRestController :: home() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: home()" + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}
	
	@RequestMapping(value = "/checkDuplicateRegNo", method = RequestMethod.GET)
	public String checkDuplicateRegNo(@RequestParam(value = "regNo", required = false) String regNo) {
		String status = "false";
		try {
			logger.info("ComsRestController :: checkDuplicateRegNo() :: START");
			status = aw.getEnterpriseCustomerService().findByCustomerCode(regNo);
			logger.info("ComsRestController :: checkDuplicateRegNo() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: checkDuplicateRegNo" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/checkExistAPSFLCode", method = RequestMethod.GET)
	public boolean checkExistAPSFLCode(@RequestParam(value = "apsflUniqueId", required = false) String apsflUniqueId) {
		boolean status = false;
		try {
			logger.info("ComsRestController :: checkExistAPSFLCode() :: START");
			status = aw.getLovsService().CheckAPSFLCode(apsflUniqueId);
			logger.info("ComsRestController :: checkExistAPSFLCode() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: checkExistAPSFLCode" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	@RequestMapping(value = "/individualCafFPActivationPagination", method = RequestMethod.POST)
	public PaginationObject<FingerPrintBO> individualCafFPActivationPagination(@RequestBody PageObject pageObject) {
		PaginationObject<FingerPrintBO> fpPaginationObject = new PaginationObject<FingerPrintBO>();
		try {
			logger.info("ComsRestController :: individualCafFPActivation() :: START");
			fpPaginationObject.setAaData(aw.getCafService().getIndividualCafFPActivationInformation(pageObject));
			fpPaginationObject.setiTotalDisplayRecords(Long.parseLong(pageObject.getTotalDisplayCount()));
			logger.info("ComsRestController :: individualCafFPActivation() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: individualCafFPActivation()" + e);
			e.printStackTrace();
		} finally {

		}
		return fpPaginationObject;
	}
	
	@RequestMapping(value = "/groupCafFPActivationDetails", method = RequestMethod.POST)
	public PaginationObject<FingerPrintBO> groupCafFPActivationDetails(@RequestBody FingerPrintBO fingerPrintBO) {
		PaginationObject<FingerPrintBO> fpPaginationObject = new PaginationObject<FingerPrintBO>();
		try {
			logger.info("ComsRestController :: groupCafFPActivationDetails() :: START");
			if(fingerPrintBO.getSelectCaf().equalsIgnoreCase("Yes")) {
				fpPaginationObject.setAaData(aw.getCafService().getIndividualCafFPActivationInformation(fingerPrintBO.getPageObject()));
				fpPaginationObject.setiTotalDisplayRecords(Long.parseLong(fingerPrintBO.getPageObject().getTotalDisplayCount()));
			} else {
				fpPaginationObject.setAaData(aw.getCafService().getGroupCafFPActivationInformation(fingerPrintBO));
				fpPaginationObject.setiTotalDisplayRecords(Long.parseLong(fingerPrintBO.getPageObject().getTotalDisplayCount()));
			}
			logger.info("ComsRestController :: groupCafFPActivationDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: groupCafFPActivationDetails()" + e);
			e.printStackTrace();
		} finally {

		}
		return fpPaginationObject;
	}
	
	@RequestMapping(value = "/groupCafFPActivation", method = RequestMethod.GET)
	public List<Districts> getCafsByLocation() {
		List<Districts> districtList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: groupCafFPActivation() :: START");
			districtList = aw.getCpeInformationService().getAllDistricts();
			logger.info("ComsRestController :: groupCafFPActivation() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: groupCafFPActivation" + e);
			e.printStackTrace();
		} finally {

		}
		return districtList;
	}
	
	@RequestMapping(value = "/checkBlackListCustomer", method = RequestMethod.GET)
	public String checkBlackListCustomer(@RequestParam(value = "aadharNumber", required = false) String aadharNumber) {
		String status = "false";
		try {
			logger.info("ComsRestController :: checkBlackListCustomer() :: START");
			status = aw.getCustomerService().getBlackListCustomerByAadharNo(aadharNumber);
			logger.info("ComsRestController :: checkBlackListCustomer() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: checkBlackListCustomer" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/checkExistCustomer", method = RequestMethod.GET)
	public Customer checkExistCustomer(@RequestParam(value = "aadharNo", required = false) String aadharNo) {
		Customer customer = null;
		try {
			logger.info("ComsRestController :: checkExistCustomer() :: START");
			customer = aw.getCustomerService().findByCustomerCode(aadharNo);
			if (customer != null) {
				customer.setDob(DateUtill.dateToString(customer.getDateofinc()));
				logger.info("ComsRestController :: checkExistCustomer() :: END");
			}
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: checkExistCustomer" + e);
			e.printStackTrace();
		} finally {
			
		}
		return customer;
	}
	
	@RequestMapping(value = "/multiactionsearch", method = RequestMethod.GET)
	public ComsHelperDTO multiActionSearchPage(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
 			logger.info("ComsRestController :: multiActionSearchPage() :: START");
			List<StatusCodes> statusCodesList = aw.getStatusCodesService().findStatusCodesName();
			//List<Districts> districtLst = aw.getCpeInformationService().getSILMODistricts(tenantCode);
			List<Districts> districtLst = aw.getCpeInformationService().getAllDistricts();
			comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.CUSTOMER_TYPES.getCode()));
			comsHelperDTO.setCustomerSubTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.ENTERPRISE_TYPES.getCode()));
			comsHelperDTO.setDistrictList(districtLst);
			comsHelperDTO.setStatusCodesList(statusCodesList);
			logger.info("ComsRestController :: multiActionSearchPage() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: multiActionSearchPage" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getSearchPackageDetails", method = RequestMethod.GET)
	public ComsHelperDTO getSearchPackageDetails(@RequestParam(value = "cafNumber", required = false) Long cafNumber,
			@RequestParam(value = "tenantName", required = false) String tenantName,
			@RequestParam(value = "tenantCode", required = false) String tenantCode,
			@RequestParam(value = "tenantType", required = false) String tenantType) throws Exception {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		CustomerCafVO customerCafVO = new CustomerCafVO();
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		List<Substations> substationsList = new ArrayList<>();
		List<ProductsVO> productsList = new ArrayList<>();
		List<ProductsVO> baseProductList = new ArrayList<>();
		List<ProductsVO> addOnProductList = new ArrayList<>();
		List<ProductsVO> oneTimeProductList = new ArrayList<>();
		Set<String> mspCodes = new HashSet<>();
		String subStnCodes = "";
		String subStCodes = "";
		String regionCode = "";
		String jsonObject = "";
		String lmoAgrmntMspCodes = "";
		try {
			logger.info("ComsRestController :: getSearchPackageDetails() :: START");
			customerCafVO = aw.getCafService().getPackageInformation(cafNumber, tenantType);
			subStnCodes = aw.getTenantWalletService().getLMOsubstations(tenantCode);
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
				substationsList = aw.getCpeInformationService().getSubstationsByDistrictIdAndMandalId(Integer.parseInt(customerCafVO.getPopDistrict()), Integer.parseInt(customerCafVO.getPopMandal()));
			} else {
				subStCodes = FileUtil.mspCodeSplitMethod(subStnCodes);
				substationsList = aw.getCpeInformationService().getAllSubStations(subStCodes);
			}
			comsHelperDto.setSubStationsList(substationsList);
			comsHelperDto.setOltList(aw.getCpeInformationService().getOLTSLNOBySubstationsrlno(customerCafVO.getPopId()));
			comsHelperDto.setCpeModalsList(aw.getCpeInformationService().getAllCpeModalsByCpeType(ComsEnumCodes.ONU_TYPE.getCode()));
			comsHelperDto.setStbModelsList(aw.getCpeInformationService().getAllCpeModalsByCpeType(ComsEnumCodes.IPTV_TYPE.getCode()));
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(tenantCode);
			regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(),customerCafVO.getCity());
			regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
			jsonObject = getProducts(customerCafVO.getCustType(), tenantCode, regionCode, null, null);
			productsList = mapper.readValue(jsonObject, TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
			for (ProductsVO products : productsList) {
				mspCodes.add(products.getMspCode());
				if (products.getProdType().equalsIgnoreCase("B")) {
					baseProductList.add(products);
				} else if (products.getProdType().equalsIgnoreCase("A")) {
					addOnProductList.add(products);
				} else if (products.getProdType().equalsIgnoreCase("O")){
					oneTimeProductList.add(products);
				}
			}
			StringBuilder agrMspCodes = new StringBuilder();
			for (String mspcode : mspCodes) {
				agrMspCodes.append(mspcode).append(",");
			}
			lmoAgrmntMspCodes = FileUtil.mspCodeSplitMethod(agrMspCodes.toString());
			comsHelperDto.setProductsList(productsList);
			comsHelperDto.setBaseProductList(baseProductList);
			comsHelperDto.setAddOnProductList(addOnProductList);
			comsHelperDto.setOneTimeProductList(oneTimeProductList);
			comsHelperDto.setCustomerCafVO(customerCafVO);
			comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
			comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDto.setAlert(tenantWalletVO.getAlert());
			comsHelperDto.setFlag(tenantWalletVO.getFlag());
			comsHelperDto.setLmoAgrmntMspCodes(lmoAgrmntMspCodes);
			comsHelperDto.setTelephoneConnectionsList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.TELEPHONE_CONNECTIONS.getCode()));
			logger.info("ComsRestController :: getSearchPackageDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::getSearchPackageDetails() " + e);
			e.printStackTrace();
		} finally {
			customerCafVO = null;
			subStnCodes = null;
			subStCodes = null;
			tenantWalletVO = null;
			productsList = null;
			baseProductList = null;
			addOnProductList = null;
			oneTimeProductList = null;
			mspCodes = null;
			regionCode = null;
			jsonObject = null;
			lmoAgrmntMspCodes = null;
			substationsList = null;
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/createcustomer", method = RequestMethod.POST)
	public ComsHelperDTO getProducts(@RequestBody EnterpriseCustomerVO enterpriseCustomerVO ) {

		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Customer customer = null;
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		String subStnCodes = "";
		String subStCodes = "";
		try {
			logger.info("ComsRestController :: createcustomer() :: START");
			if (enterpriseCustomerVO.getCustId() != null) {
				customer = aw.getCustomerService().findByCustomerCode(enterpriseCustomerVO.getCustId());
			}
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(enterpriseCustomerVO.getTenantCode());
			subStnCodes = aw.getTenantWalletService().getLMOsubstations(enterpriseCustomerVO.getTenantCode());
			subStCodes = FileUtil.mspCodeSplitMethod(subStnCodes);
			comsHelperDTO.setSubStationsList(aw.getCpeInformationService().getAllSubStations(subStCodes));
			comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
			comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.TITLES.getCode()));
			comsHelperDTO.setCafNo(aw.getStoredProcedureDAO().executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode()).toString());
			if (customer != null) {
				comsHelperDTO.setCustomer(customer);
				comsHelperDTO.setDistrict(aw.getCpeInformationService().getDistrictByDistrictId(Integer.parseInt(customer.getDistrict())));
				comsHelperDTO.setMandal(aw.getCpeInformationService().getMandalsByDistrictIdAndMandalSrlNo(Integer.parseInt(customer.getDistrict()), Integer.parseInt(customer.getMandal())));
				comsHelperDTO.setTenantBusinessAreasList(aw.getCpeInformationService().getAllVillagesBySubstnId(subStCodes, enterpriseCustomerVO.getTenantCode()));
			}
			comsHelperDTO.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDTO.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDTO.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDTO.setAlert(tenantWalletVO.getAlert());
			comsHelperDTO.setFlag(tenantWalletVO.getFlag());
			logger.info("ComsRestController :: createcustomer() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::createcustomer() " + e);
			e.printStackTrace();
		} finally {
			customer = null;
			subStnCodes = null;
			subStCodes = null;
			tenantWalletVO = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/showCustomersPage", method = RequestMethod.POST)
	public ComsHelperDTO showCustomersPage(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: showcustomers() :: START");
			comsHelperDTO = aw.getCustomerService().findAllCustomers(comsHelperDTO);
			logger.info("ComsRestController :: showcustomers() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: showcustomers" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}

	@RequestMapping(value = "/viewcustomersPage", method = RequestMethod.POST)
	public ComsHelperDTO viewcustomersPage(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: showcustomers() :: START");
			comsHelperDTO = aw.getCafService().findCafDaoByCafNo(comsHelperDTO);
			logger.info("ComsRestController :: showcustomers() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: showcustomers" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/cafdetails", method = RequestMethod.GET)
	public ComsHelperDTO getProducts(@RequestParam(value = "custId", required = false) Long custId,
			@RequestParam(value = "custType" , required = false) String custType) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Customer customer = new Customer();
		List<Object[]> cafDaoList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getProducts() :: START");
			customer = aw.getCustomerService().findParticularCustomer(custId,custType);
			cafDaoList = aw.getCafService().findCafByCafNo(custId);
			comsHelperDTO.setCustomer(customer);
			comsHelperDTO.setCafDaoList(cafDaoList);
			logger.info("ComsRestController :: getProducts() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: getProducts" + e);
			e.printStackTrace();
		} finally {
			customer = null;
			cafDaoList = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getCafProducts", method = RequestMethod.GET)
	public List<CafProducts> getCafProducts(@RequestParam(value = "cafNo", required = false) Long cafNo) {
		List<CafProducts> cafProductsList = new ArrayList<CafProducts>();
		try {
			logger.info("ComsRestController :: getCafProducts() :: START");
			cafProductsList = aw.getCafProductsService().getCafProducts(cafNo);
			logger.info("ComsRestController :: getCafProducts() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: getCafProducts" + e);
			e.printStackTrace();
		}
		return cafProductsList;
	}
	
	@RequestMapping(value = "/createBulkPaymentDetails", method = RequestMethod.POST)
	public ComsHelperDTO createBulkPaymentDetails(@RequestBody ComsHelperDTO caflist) {
		ComsHelperDTO cmd = new ComsHelperDTO();
		PaymentVO paymentvo=new PaymentVO();
		String[] bcd=caflist.getBulkcustlist();
		for(int i=0;i<bcd.length;i++) {
			paymentvo=aw.getPaymentService().getCafDetails(Long.valueOf(bcd[i]),caflist.getEffectiveDate().toString());
			paymentvo.setLmoCode(caflist.getLmoCode());
			paymentvo.setLoginId(caflist.getLmoCode());
			paymentvo.setTenantCode(caflist.getLmoCode());
			paymentvo.setCustomerCafVO(new CustomerCafVO());
			paymentvo.setTenantType("LMO");
			paymentvo.setDdDate(caflist.getEffectiveDate());
			cmd=createPaymentDetails(paymentvo);
		}
		
		return cmd;
	
	}
	
	@RequestMapping(value = "/createPaymentDetails", method = RequestMethod.POST)
	public ComsHelperDTO createPaymentDetails(@RequestBody PaymentVO paymentVO) {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		List<ProductsVO> changePkgList = new ArrayList<>();
		List<HsiBO> hsiList = new ArrayList<>();
		String message = "";
		String jsonObject = "";
		String hsiStatus = "HSI";
		String srvcCode = "";
		String regionCode = "";
		Gson gson = new Gson();
		String paymentJson = "";
		String onuNumber = "";
		// added by chaitanya_xyz
		int cardnum=1;
		if(paymentVO.getOltSrlNo()!=null && !paymentVO.getOltSrlNo().equals("")){
			cardnum=aw.getPaymentService().getCardDetails(paymentVO.getOltSrlNo());
			aw.getPaymentService().updateCafCard(paymentVO.getCafNo(), cardnum);
			if(cardnum==1)paymentVO.setOltPortNo(paymentVO.getOltPortNo());
		}else{
			if(paymentVO.getCafNo()!=null)cardnum=aw.getPaymentService().getCardDetailsByCaf(paymentVO.getCafNo().toString());
		}
		try {
			logger.info("ComsRestController :: createPaymentDetails() :: START");
			paymentJson = gson.toJson(paymentVO);
			logger.info("The Save Payment Json is :" + paymentJson);
			if (paymentVO.getPayment() != null) {
				aw.getPaymentService().saveMonthlyPayment(paymentVO, paymentVO.getLoginId());
				comsHelperDto.setFlag("The Monthly Payment Created Successfully.");
				SmsDTO smsDto = new SmsDTO();
				smsDto.setMobileNo(paymentVO.getMobileNo());
				String msg = "Dear Customer, We have received Cash Payment of Rs "+paymentVO.getPaidAmount()+" for your APSFL Customer ID: "+paymentVO.getCustId()+". Thank You";
				smsDto.setMsg(msg);
				smsApi.sendSMS(smsDto);
				logger.info("Message sent Successfully.");
				return comsHelperDto;
				
			} else if(paymentVO.getCustomerCafVO().getChangePkgFlag() != null && (paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) 
						|| paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode()))) {
				if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
					ChangePkgBO changePkgBO = paymentVO.getCustomerCafVO().getChangePkgBO();
					regionCode = aw.getCafService().getRegionCodeByPinCode(paymentVO.getCustomerCafVO().getDistrict(), paymentVO.getCustomerCafVO().getMandal(), paymentVO.getCustomerCafVO().getCity());
					regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
					jsonObject = getProducts(paymentVO.getCustomerCafVO().getCustType(), paymentVO.getCustomerCafVO().getLmoCode(), regionCode, changePkgBO.getProdCode(), changePkgBO.getActDate());
					changePkgList = mapper.readValue(jsonObject,TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
				}
				if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
					if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
						AddOnPackageDTO addOnPackageDTO = new AddOnPackageDTO();
						for(ProductsVO products : paymentVO.getCustomerCafVO().getProducts()) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						hsiList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								addOnPackageDTO.setDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								addOnPackageDTO.setUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						addOnPackageDTO.setMonth(DateUtill.getCurrentMonth().toString());
						addOnPackageDTO.setYear(DateUtill.getYear().toString());
						addOnPackageDTO.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo().toString());
						hsiStatus = saveAddonHsiParameters(addOnPackageDTO);
					} else if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
						ChangeBasePackage changeBasePackage = new ChangeBasePackage();
						for(ProductsVO products : changePkgList) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						hsiList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								changeBasePackage.setOldDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								changeBasePackage.setOldUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						
						for(ProductsVO products : paymentVO.getCustomerCafVO().getProducts()) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						List<HsiBO> hsiNewList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiNewList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								changeBasePackage.setNewDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								changeBasePackage.setNewUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						changeBasePackage.setMonth(DateUtill.getCurrentMonth().toString());
						changeBasePackage.setYear(DateUtill.getYear().toString());
						changeBasePackage.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo().toString());
						hsiStatus = saveChangeBaseHsiParameters(changeBasePackage);
					}
				} 
				if(hsiStatus.equalsIgnoreCase("Success") || hsiStatus.equalsIgnoreCase("HSI")) {
					message="Success";
					String phoneNo = aw.getPaymentService().saveAddpackage(paymentVO, changePkgList, message);
					message = !phoneNo.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "Your Order Created Successfully.";
					comsHelperDto.setFlag(message);
					
					if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
						message = aw.getProvisioningBusinessServiceImpl().basePackageChange(paymentVO.getCustomerCafVO().getProdCafNo(), paymentVO.getCustomerCafVO().getProdCode());
						if(message.equalsIgnoreCase("success")) {
							aw.getCafServicesService().updateCafServices(paymentVO.getCustomerCafVO().getCafNo().toString(), paymentVO.getCustomerCafVO().getChangePkgBO().getProdCode(), paymentVO.getCustomerCafVO().getLoginId(), paymentVO.getCustomerCafVO().getIpAddress());
							logger.info("The Cafsrvcs table updated successfully");
						}
					}
					/*String phoneNo = aw.getPaymentService().saveAddpackage(paymentVO, changePkgList, message);
					message = !phoneNo.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "Your Order Created Successfully.";
					comsHelperDto.setFlag(message);*/
					
					if(paymentVO.getPaidAmount() != null && paymentVO.getPaidAmount() > 0) {
						String result = aw.getPaymentService().processWOPayments(paymentVO);
						if(result.equalsIgnoreCase("0")) {
							logger.info("WorkOrder Process Payments and Provisioning Request Jsons Created Successfully");
						}
					} else {
						if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
							aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(paymentVO.getCustomerCafVO().getProdCafNo(), 1);
							logger.info("Provisioning Request Jsons Created Successfully");
						}
					}
				} else {
					comsHelperDto.setFlag(hsiStatus);
				}
			} else {
				if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) < 0 ) {
					List<IPTVBoxVO> iptvBoxVOList = new ArrayList<>();
					CustomerCafVO customerCafVO = paymentVO.getCustomerCafVO();
					customerCafVO.setIptvBoxList(iptvBoxVOList);
					paymentVO.setCustomerCafVO(customerCafVO);
				}
				paymentJson = gson.toJson(paymentVO);
				logger.info("The Save Payment Json is :" + paymentJson);
				if (paymentVO.getFeasibility().equalsIgnoreCase("N")) {
					aw.getCustomerService().updateCustomer(paymentVO, paymentVO.getLoginId(),cardnum);
					comsHelperDto.setFlag("Feasibility Status has been Updated Successfully.");
				} else if (paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
					onuNumber = aw.getStoredProcedureDAO().executePullOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), cardnum, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()));
					if (FileUtil.NumberCheck(onuNumber).equalsIgnoreCase("true")) {
						/*aw.getCafAccountService().saveCafAccount(paymentVO.getCustomerCafVO(), paymentVO.getLoginId());
						logger.info("The cafAccount Details Created Successfully");*/
						
						if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
							String phoneNo = aw.getPaymentService().saveEnterpriseCustomerPayment(paymentVO, paymentVO.getLoginId(), onuNumber,cardnum);
							message = !phoneNo.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "Your Order Created Successfully.";
							comsHelperDto.setFlag(message);
							
							aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(paymentVO.getCafNo().toString(), 1);
							logger.info("Provisioning Request Jsons Created Successfully");
						} else {
							String phoneNo = aw.getPaymentService().createPayment(paymentVO, paymentVO.getLoginId(), onuNumber,cardnum);
							message = !phoneNo.isEmpty() ? "The Payment Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "The Payment Created Successfully.";
							comsHelperDto.setFlag(message);
							
							String result = aw.getPaymentService().processWOPayments(paymentVO);
							if(result.equalsIgnoreCase("0")) {
								logger.info("WorkOrder Process Payments and Provisioning Request Jsons Created Successfully");
							}
						}
					} else {
						comsHelperDto.setFlag(onuNumber);
					}
				}
			}
			logger.info("ComsRestController :: createPaymentDetails() :: END");
		} catch (Exception e) {
			if(!onuNumber.isEmpty()) {
				String exception = aw.getStoredProcedureDAO().executePushOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), 1, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()), Integer.parseInt(onuNumber));
				if(exception.equalsIgnoreCase("0"))
					logger.info("ONU Id RoleBacked Successfully.");
				logger.error("ComsRestController::createPaymentDetails() " + e);
			}
			e.printStackTrace();
			comsHelperDto.setFlag(e.getMessage() + " so we can't process Payment right now");
		} finally {
			
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/getEnterPriseList", method = RequestMethod.GET)
	public List<Object[]> getEnterPriseList(@RequestParam(value = "tenantCode", required = false) String tenantCode, @RequestParam(value = "tenantType", required = false) String tenantType) {
		List<Object[]> enterpriseCustomerList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getEnterPriseList() :: START");
			if(!tenantType.equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode()))
				enterpriseCustomerList = aw.getEnterpriseCustomerService().findAllEnterpriseCustomers(tenantCode, tenantType);
			logger.info("ComsRestController :: getEnterPriseList() :: END");
		} catch (Exception e) {
			logger.error("CustomerController::getEnterPriseList() " + e);
			e.printStackTrace();
		} finally {

		}
		return enterpriseCustomerList;
	}
	
	@RequestMapping(value = "/getCustomerList", method = RequestMethod.GET)
	public List<Object[]> getCustomerList(@RequestParam(value = "tenantCode", required = false) String tenantCode, @RequestParam(value = "tenantType", required = false) String tenantType) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getEnterPriseList() :: START");
			customerList = aw.getCustomerService().findAllCustomers(tenantCode, tenantType);
			logger.info("ComsRestController :: getEnterPriseList() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::getCustomerList() " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@RequestMapping(value = "/searchCafDetails", method = RequestMethod.POST)
	public PaginationObject<CafAndCpeChargesVO> searchCafProductsDetails(@RequestBody SearchDataVO searchDataVO) {
		PaginationObject<CafAndCpeChargesVO> multiDataPageObject= new PaginationObject<>();
		try {
			logger.info("ComsRestController :: searchCafDetails() :: START");
			MultiAction multiAction = searchDataVO.getMultiAction();
			PageObject pageObject = searchDataVO.getPageObject();
			
			multiDataPageObject.setAaData(aw.getCafService().searchCafDetails(multiAction, pageObject));
			//Set Total display record
			multiDataPageObject.setiTotalDisplayRecords(Long.parseLong(multiDataPageObject.getAaData().get(0).getTotalDisplayCount()));
			multiDataPageObject.setiTotalRecords(Long.parseLong(multiDataPageObject.getAaData().get(0).getTotalDisplayCount()));
			
			logger.info("ComsRestController :: searchCafDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::searchCafDetails() " + e);
			e.printStackTrace();
		} finally {
		}
		return multiDataPageObject;
	}
	
	//18/1/2018
	
	@RequestMapping(value = "/searchMonthlyPaymentCafDetails", method = RequestMethod.POST)
	public ComsHelperDTO searchMonthlyPaymentCafDetails(@RequestBody MonthlyPayment monthlyPayment) {
		List<MonthlyPaymentBO> monthlyPaymentList = new ArrayList<>();
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		try {
			logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: START");
			if ((monthlyPayment.getMobileNo()==null) || (monthlyPayment.getMobileNo().isEmpty()))
			{
				monthlyPaymentList = aw.getCafService().getMonthlyCafDetails1(monthlyPayment.getCafNo(), monthlyPayment.getTenantCode(), null);
			}
			else
			{
				monthlyPaymentList = aw.getCafService().getMonthlyCafDetails(monthlyPayment.getMobileNo(), monthlyPayment.getTenantCode(), null);

			}
			comsHelperDto.setMonthlyPayment(monthlyPayment);
			comsHelperDto.setMonthlyPaymentList(monthlyPaymentList);
			comsHelperDto.setLmoName(monthlyPayment.getTenantName());
			comsHelperDto.setLmoCode(monthlyPayment.getTenantCode());
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(monthlyPayment.getTenantCode());
			comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
			comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDto.setAlert(tenantWalletVO.getAlert());
			comsHelperDto.setFlag(tenantWalletVO.getFlag());
			logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::searchMonthlyPaymentCafDetails() " + e);
			e.printStackTrace();
		} finally {
			monthlyPaymentList = null;
			tenantWalletVO = null;
		}
		return comsHelperDto;
	}
	

	@RequestMapping(value = "/createCustomerCafDetails", method = RequestMethod.POST)
	public ComsHelperDTO createCustomerCafDetails(@RequestBody CustomerCafVO customerCafVO) throws Exception {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		List<Object[]> customerList = new ArrayList<>();
		Customer customer = null;
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		List<Substations> substationsList = new ArrayList<>();
		List<ProductsVO> productsList = new ArrayList<>();
		List<ProductsVO> baseProductList = new ArrayList<>();
		List<ProductsVO> addOnProductList = new ArrayList<>();
		List<ProductsVO> oneTimeProductList = new ArrayList<>();
		Set<String> mspCodes = new HashSet<String>();
		StringBuilder agrMspCodes = new StringBuilder();
		String subStnCodes = "";
		String subStCodes = "";
		String regionCode = "";
		String jsonObject = "";
		String lmoAgrmntMspCodes = "";
		String coreSrvcCode = "";
		String iptvPackageCode = "";
		Gson gson = new Gson();
		try {
			logger.info("ComsRestController :: createCustomerCafDetails() :: START");
			String cafJson = gson.toJson(customerCafVO);
			logger.info(cafJson);
			customer = aw.getCustomerService().createCustomers(customerCafVO, customerCafVO.getLoginId(), customerCafVO.getLmoCode());
			if (customer != null || customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) ) {
				//customerCafVO.setStatus("" + customer.getStatus());
				coreSrvcCode = aw.getCafService().getCoreSrvcCodeByCafNo(customerCafVO.getCafNo());
				regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(),customerCafVO.getCity());
				regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
				jsonObject = getProducts(customerCafVO.getCustType(), customerCafVO.getLmoCode(), regionCode, null, null);
				productsList = mapper.readValue(jsonObject,TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
				customerCafVO.setCoreSrvcCode(coreSrvcCode);
				tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(customerCafVO.getLmoCode());
				if(customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
					substationsList = aw.getCpeInformationService().getSubstationsByDistrictIdAndMandalId(Integer.parseInt(customerCafVO.getPopDistrict()), Integer.parseInt(customerCafVO.getPopMandal()));
					List<Object[]> objectCafProductsList = aw.getCafProductsDao().getCafProducts(customerCafVO.getCafNo());
					for(Object[] object : objectCafProductsList) {
						if(object[2] != null && !object[2].toString().equalsIgnoreCase("B")) {
							if(iptvPackageCode.isEmpty()) {
								iptvPackageCode = object[0].toString()+"^"+object[1].toString();
							} else {
								iptvPackageCode = iptvPackageCode+","+object[0].toString()+"^"+object[1].toString();
							}
						}
					}
					customerCafVO.setIptvPackage(iptvPackageCode);
					Object[] filterPkgDtls = aw.getCafProductsDao().getFilterPackageDetails(customerCafVO.getCafNo());
					customerCafVO.setProdCode(filterPkgDtls[0].toString());
					customerCafVO.setTenantCode(filterPkgDtls[1].toString());
					customerCafVO.setAgruniqueid(filterPkgDtls[2].toString());
					for (ProductsVO products : productsList) {
						mspCodes.add(products.getMspCode());
					}
					for (String mspcode : mspCodes) {
						agrMspCodes.append(mspcode).append(",");
					}
					lmoAgrmntMspCodes = FileUtil.mspCodeSplitMethod(agrMspCodes.toString());
				} else {
					subStnCodes = aw.getTenantWalletService().getLMOsubstations(customerCafVO.getLmoCode());
					subStCodes = FileUtil.mspCodeSplitMethod(subStnCodes);
					substationsList = aw.getCpeInformationService().getAllSubStations(subStCodes);
					for (ProductsVO products : productsList) {
						if (products.getProdType().equalsIgnoreCase("B")) {
							mspCodes.add(products.getMspCode());
							baseProductList.add(products);
						} else if (products.getProdType().equalsIgnoreCase("A")) {
							addOnProductList.add(products);
						} else if (products.getProdType().equalsIgnoreCase("O")) {
							oneTimeProductList.add(products);
						}
					}
					for (String mspcode : mspCodes) {
						agrMspCodes.append(mspcode).append(",");
					}
					lmoAgrmntMspCodes = FileUtil.mspCodeSplitMethod(agrMspCodes.toString());

					comsHelperDto.setBaseProductList(baseProductList);
					comsHelperDto.setAddOnProductList(addOnProductList);
					comsHelperDto.setOneTimeProductList(oneTimeProductList);
				}
				comsHelperDto.setProductsList(productsList);
				comsHelperDto.setSubStationsList(substationsList);
				comsHelperDto.setOltList(aw.getCpeInformationService().getOLTSLNOBySubstationsrlno(customerCafVO.getPopId()));
				comsHelperDto.setCpeModalsList(aw.getCpeInformationService().getAllCpeModalsByCpeType(ComsEnumCodes.ONU_TYPE.getCode()));
				comsHelperDto.setStbModelsList(aw.getCpeInformationService().getAllCpeModalsByCpeType(ComsEnumCodes.IPTV_TYPE.getCode()));
				comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
				comsHelperDto.setTelephoneConnectionsList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.TELEPHONE_CONNECTIONS.getCode()));
				comsHelperDto.setCustomer(customer);
				comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
				comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
				comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
				comsHelperDto.setAlert(tenantWalletVO.getAlert());
				comsHelperDto.setFlag(tenantWalletVO.getFlag());
				comsHelperDto.setLmoAgrmntMspCodes(lmoAgrmntMspCodes);
				comsHelperDto.setMessage(customerCafVO.getCustId() == null ? "The Customer Caf Information Inserted Successfully" : "The Customer Caf Information Updated Successfully");
				customerCafVO.setCustId(customer != null ? customer.getCustId().intValue() : Integer.parseInt(customerCafVO.getEntCustomerCode()));
				comsHelperDto.setCustomerCafVO(customerCafVO);
			} else {
				customerList = aw.getCustomerService().findAllCustomers(customerCafVO.getLmoCode(), customerCafVO.getTenantType());
				comsHelperDto.setCustomerList(customerList);
				comsHelperDto.setMessage("The Customer Caf Creation Failed");
			}
			logger.info("ComsRestController :: createCustomerCafDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::createCustomerCafDetails() " + e);
			e.printStackTrace();
		} finally {
			customerList = null;
			subStnCodes = null;
			subStCodes = null;
			tenantWalletVO = null;
			productsList = null;
			baseProductList = null;
			addOnProductList = null;
			oneTimeProductList = null;
			mspCodes = null;
			regionCode = null;
			jsonObject = null;
			lmoAgrmntMspCodes = null;
			agrMspCodes = null;
			customer = null;
			substationsList = null;
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/saveLMOCafDetails", method = RequestMethod.POST)
	public ComsHelperDTO saveLMOCafDetails(@RequestBody CustomerCafVO customerCafVO) throws Exception {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		Caf caf = new Caf();
		List<StatusCodes> statusCodesList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: saveLMOCafDetails() :: START");
			//int cardnum=aw.getPaymentService().getCardDetails(customerCafVO.getOltId());
			caf = aw.getCafService().updateCafDetails(customerCafVO);
			statusCodesList = aw.getStatusCodesService().findStatusCodesName();
			comsHelperDto.setMessage(caf.getCafNo() != null ? "Caf Updated Successfully" : "Caf Updation Failed");
			comsHelperDto.setStatusCodesList(statusCodesList);
			logger.info("ComsRestController :: saveLMOCafDetails() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::saveLMOCafDetails() " + e);
			e.printStackTrace();
		} finally {
			caf = null;
			statusCodesList = null;
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/createPackages", method = RequestMethod.POST)
	public ComsHelperDTO createSelectedPackages(@RequestBody CustomerCafVO customerCafVO) throws Exception {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		String cpeModel = "";
		try {
			logger.info("ComsRestController :: createSelectedPackages() :: START");
			if(customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
				ChangePkgBO changePkgBO = aw.getCafService().getProdCodeForCaf(customerCafVO.getProdCafNo() != null ? Long.parseLong(customerCafVO.getProdCafNo()) : 0l);
				customerCafVO.setChangePkgBO(changePkgBO);
				String regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
				regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
				String secDeposit = aw.getCafService().getSecurityDepositByProdcode(customerCafVO.getProdCafNo(), changePkgBO.getProdCode(), regionCode);
				BigDecimal secAmount = new BigDecimal("-1").multiply(new BigDecimal(secDeposit));
				comsHelperDto.setSecDeposit(secAmount.toString());
			}
			Map<String, Integer> packagesMap = FileUtil.getListSize(customerCafVO.getIptvBoxList(), customerCafVO.getProdCode());
			filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
			cpeInformationVO = aw.getCpeInformationService().getCpeChargesInformation(customerCafVO);
			comsHelperDto.setCpeInformationVO(cpeInformationVO);
			comsHelperDto.setEffectiveDate(DateUtill.dateToString(new Date()));
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(customerCafVO.getLmoCode());
			comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
			comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDto.setAlert(tenantWalletVO.getAlert());
			comsHelperDto.setFlag(tenantWalletVO.getFlag());
			customerCafVO.setProducts(filterPackagesVO.getSelectedPackagesList());
			comsHelperDto.setCafProductsList(filterPackagesVO.getCafAndCpeChargesList());
			comsHelperDto.setTotalAmount(filterPackagesVO.getTotalAmount());
			if(customerCafVO.getCpeModal() != null) {
			cpeModel = aw.getCpeInformationService().getCpeModelByProfileId(Integer.parseInt(customerCafVO.getCpeModal()));
			}
			comsHelperDto.setCpeModel(cpeModel);
			comsHelperDto.setEffectiveDate(DateUtill.dateToString(new Date()));
			comsHelperDto.setCustomerCafVO(customerCafVO);
			if(customerCafVO.getCpeModal() == null) {
			comsHelperDto.setCustBal(aw.getCafService().getCustomerBalance(customerCafVO.getCustId(), customerCafVO.getLmoCode()));
			}
			logger.info("ComsRestController :: createSelectedPackages() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::createSelectedPackages() " + e);
			e.printStackTrace();
		} finally {
			filterPackagesVO = null;
			cpeInformationVO = null;
			tenantWalletVO = null;
			cpeModel = null;
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/getCafBulkUploadWorkorderForm", method = RequestMethod.POST)
	public ComsHelperDTO getCafBulkUploadWorkorderForm(@RequestBody CustomerCafVO customerCafVO) throws Exception {
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		String cpeModel = "";
		try {
			logger.info("ComsRestController :: createSelectedPackages() :: START");
			Map<String, Integer> packagesMap = FileUtil.getListSize(customerCafVO.getIptvBoxList(), customerCafVO.getProdCode());
			filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
			customerCafVO.setProducts(filterPackagesVO.getSelectedPackagesList());
			cpeInformationVO = aw.getCpeInformationService().getCpeChargesInformation(customerCafVO);
			comsHelperDto.setCpeInformationVO(cpeInformationVO);
			comsHelperDto.setEffectiveDate(DateUtill.dateToString(new Date()));
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(customerCafVO.getLmoCode());
			comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
			comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDto.setAlert(tenantWalletVO.getAlert());
			comsHelperDto.setFlag(tenantWalletVO.getFlag());
			comsHelperDto.setCafProductsList(filterPackagesVO.getCafAndCpeChargesList());
			comsHelperDto.setTotalAmount(filterPackagesVO.getTotalAmount());
			cpeModel = aw.getCpeInformationService().getCpeModelByProfileId(Integer.parseInt(customerCafVO.getCpeModal()));
			comsHelperDto.setCpeModel(cpeModel);
			comsHelperDto.setEffectiveDate(DateUtill.dateToString(new Date()));
			comsHelperDto.setCustomerCafVO(customerCafVO);
			logger.info("ComsRestController :: createSelectedPackages() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::createSelectedPackages() " + e);
			e.printStackTrace();
		} finally {
			cpeInformationVO = null;
			tenantWalletVO = null;
			cpeModel = null;
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/getAadharDetails", method = RequestMethod.GET)
	public AadharDTO getAadharDetails(@RequestParam(value = "aadharNumber") String aadharNumber) {
		AadharDTO aadharInfo = new AadharDTO();
		try {
			logger.info("ComsRestController :: getAadharDetails() :: START");
			aadharInfo = aw.getCafService().getAadharDetails(aadharNumber);
			if(aadharInfo.getDob().indexOf("-") != -1) {
				Date dob = DateUtill.aadharStringtoDateFormat2(aadharInfo.getDob());
				aadharInfo.setDob(DateUtill.dateToString(dob));
			} else {
				Date dob = DateUtill.stringtoDateFormat(aadharInfo.getDob());
				aadharInfo.setDob(DateUtill.dateToString(dob));
			}
			logger.info("ComsRestController :: getAadharDetails() :: END");
		} catch (Exception e) {
			logger.error(":::ComsRestController -- getAadharDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return aadharInfo;
	}
	
	@RequestMapping(value = "/getAllCafDetails", method = RequestMethod.GET)
	public CafDetailsVO getAllCafDetails(@RequestParam(value = "cafNo") Long cafNo, @RequestParam(value = "custId", required = false) String custId) {
		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		try {
			logger.info("ComsRestController :: getAllCafDetails() :: START");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(cafNo);
			logger.info("ComsRestController :: getAllCafDetails() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getAllCafDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cafDetailsVO;
	}
	
	@RequestMapping(value = "/getFingerPrintDetails", method = RequestMethod.POST)
	public String getFingerPrintDetails(@RequestBody FingerPrintJson fingerPrintJson) {
		String status = "";
		try {
			logger.info("ComsRestController :: getAadharDetails() :: START");
			if(Objects.nonNull(fingerPrintJson.getGlchecked())) {
				String[] subsCodeList = aw.getCafService().getGolbalOrGroupSubCodes(fingerPrintJson.getDistrict(), fingerPrintJson.getMandal(), fingerPrintJson.getVillage());
				fingerPrintJson.setSelectSubScribersCodes(subsCodeList);
			}
			status = aw.getCafService().corpusCall(fingerPrintJson);
			logger.info("ComsRestController :: getAadharDetails() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getFingerPrintDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/DeActivateCaf")
	public String DeActivateCaf(@RequestParam(value = "cafNo", required = false) Long cafNo) {
		String status = "";
		try {
			logger.info("ComsRestController :: DeActivateCaf() :: START");
			status = aw.getCafService().deActivateCaf(cafNo);
			logger.info("ComsRestController :: DeActivateCaf() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- DeActivateCaf :::" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/getFeatureParamsByFeatureCodes")
	public List<FeatureParamsVO> getFeatureParamsByFeatureCodes(
			@RequestParam(value = "featureCodes", required = false) String featureCodes) {
		List<FeatureParamsVO> featureParamList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getFeatureParamsByFeatureCodes() :: START");
			featureParamList = aw.getCafService().getFeatureParamList(featureCodes);
			logger.info("ComsRestController :: getFeatureParamsByFeatureCodes() :: END");
		} catch (Exception e) {
			logger.error(":::ComsRestController -- getFeatureParamsByFeatureCodes() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return featureParamList;
	}
	
	@RequestMapping(value = "/getDistrictByDistrictId")
	public Districts getDistrictByDistrictId(@RequestParam(value = "districtId", required = false) Integer districtId) {
		Districts districts = new Districts();
		try {
			logger.info("ComsRestController :: getDistrictByDistrictId() :: START");
			districts = aw.getCpeInformationService().getDistrictByDistrictId(districtId);
			logger.info("ComsRestController :: getDistrictByDistrictId() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: getDistrictByDistrictId() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return districts;
	}
	
	@RequestMapping(value = "/getMandalsByDistrictId")
	public List<Mandals> getMandalsByDistrictId(
			@RequestParam(value = "districtId", required = false) Integer districtId) {
		List<Mandals> mandalsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getDistrictByDistrictId() :: START");
			mandalsList = aw.getCpeInformationService().getMandalsByDistrictId(districtId);
			logger.info("ComsRestController :: getDistrictByDistrictId() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: getDistrictByDistrictId() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return mandalsList;
	}
	
	@RequestMapping(value = "/getMandalsByDistrictIdAndMandalSrlNo")
	public Mandals getMandalsByDistrictIdAndMandalSrlNo(
			@RequestParam(value = "districtId", required = false) Integer districtId,
			@RequestParam(value = "mandalSrlNo", required = false) Integer mandalSrlNo) {
		Mandals mandals = new Mandals();
		try {
			logger.info("ComsRestController :: getMandalsByDistrictIdAndMandalSrlNo() :: START");
			mandals = aw.getCpeInformationService().getMandalsByDistrictIdAndMandalSrlNo(districtId, mandalSrlNo);
			logger.info("ComsRestController :: getMandalsByDistrictIdAndMandalSrlNo() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: getMandalsByDistrictIdAndMandalSrlNo() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return mandals;
	}
	
	@RequestMapping(value = "/getVillagesByDistrictIdAndMandalId")
	public List<Villages> getVillagesByDistrictIdAndMandalId(
			@RequestParam(value = "districtId", required = false) Integer districtId,
			@RequestParam(value = "mandalId", required = false) Integer mandalId) {
		List<Villages> villagesList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getVillagesByDistrictIdAndMandalId() :: START");
			villagesList = aw.getCpeInformationService().getVillagesByDistrictAndMandal(districtId, mandalId);
			logger.info("ComsRestController :: getVillagesByDistrictIdAndMandalId() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: getVillagesByDistrictIdAndMandalId() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return villagesList;
	}
	
	@RequestMapping(value = "/getVillagesByVillageId")
	public Villages getVillagesByVillageId(@RequestParam(value = "villageSlno", required = false) Integer villageSlno,
			@RequestParam(value = "mandalSlno", required = false) Integer mandalSlno,
			@RequestParam(value = "districtUid", required = false) Integer districtUid) {
		Villages villages = new Villages();
		try {
			logger.info("ComsRestController :: getVillagesByVillageId() :: START");
			villages = aw.getCpeInformationService().getVillagesByVillageId(villageSlno, mandalSlno, districtUid);
			logger.info("ComsRestController :: getVillagesByVillageId() :: END");
		} catch (Exception e) {
			logger.info(":::CustomerController -- getVillagesByVillageId :::" + e);
			e.printStackTrace();
		} finally {
			
		}
		return villages;
	}
	
	@RequestMapping(value = "/getVillagesBySubstationId")
	public List<TenantBusinessAreas> getVillagesBySubstationId(
			@RequestParam(value = "subStnSlno", required = false) String subStnSlno,
			@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<TenantBusinessAreas> tenantBusinessAreasList = new ArrayList<>();
		String subStCodes = "";
		try {
			logger.info("ComsRestController :: getVillagesBySubstationId() :: START");
			subStCodes = FileUtil.mspCodeSplitMethod(subStnSlno);
			tenantBusinessAreasList = aw.getCpeInformationService().getAllVillagesBySubstnId(subStCodes, tenantCode);
			logger.info("ComsRestController :: getVillagesBySubstationId() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getVillagesBySubstationId :::" + e);
			e.printStackTrace();
		} finally {
			subStCodes = null;
		}
		return tenantBusinessAreasList;
	}
	
	@RequestMapping(value = "/getSubstationsByDistrictIdAndMandalId")
	public List<Substations> getSubstationsByDistrictIdAndMandalId(
			@RequestParam(value = "districtId", required = false) Integer districtId,
			@RequestParam(value = "mandalId", required = false) Integer mandalId) {
		List<Substations> substationsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getSubstationsByDistrictIdAndMandalId() :: START");
			substationsList = aw.getCpeInformationService().getSubstationsByDistrictIdAndMandalId(districtId, mandalId);
			logger.info("ComsRestController :: getSubstationsByDistrictIdAndMandalId() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getSubstationsByDistrictIdAndMandalId :::" + e);
			e.printStackTrace();
		} finally {

		}
		return substationsList;
	}
	
	@RequestMapping(value = "/getOLTSLNOBySubstationsrlno")
	public List<OLT> getOLTSLNOBySubstationsrlno(
			@RequestParam(value = "subStnSlno", required = false) String subStnSlno) {
		List<OLT> oltsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getOLTSLNOBySubstationsrlno() :: START");
			oltsList = aw.getCpeInformationService().getOLTSLNOBySubstationsrlno(subStnSlno);
			logger.info("ComsRestController :: getOLTSLNOBySubstationsrlno() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getOLTSLNOBySubstationsrlno :::" + e);
			e.printStackTrace();
		} finally {

		}
		return oltsList;
	}

	@RequestMapping(value = "/getOLTPopIdByOltSrlNo")
	public List<OLTPortDetails> getOLTPopIdByOltSrlNo(
			@RequestParam(value = "oltSrlNo", required = false) String oltSrlNo,
			@RequestParam(value = "lmoCode", required = false) String lmoCode) {
		List<OLTPortDetails> oltPortIdsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getOLTPopIdByOltSrlNo() :: START");
			oltPortIdsList = aw.getCpeInformationService().getOLTPopIdByOltSrlNo(oltSrlNo, lmoCode);
			logger.info("ComsRestController :: getOLTPopIdByOltSrlNo() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getOLTPopIdByOltSrlNo :::" + e);
			e.printStackTrace();
		} finally {

		}
		return oltPortIdsList;
	}
	
	@RequestMapping(value = "/getVPNServicesByPopIdAndOltSrlNo")
	public List<VPNSrvcNames> getVPNServicesByPopIdAndOltSrlNo(@RequestParam(value = "oltSrlNo", required = false) String oltSrlNo, @RequestParam(value = "subStnId", required = false) String subStnId) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getVPNServicesByPopIdAndOltSrlNo() :: START");
			vpnSrvcNamesList = aw.getCpeInformationService().getVPNServicesByPopIdAndOltSrlNo(oltSrlNo, subStnId);
			logger.info("ComsRestController :: getVPNServicesByPopIdAndOltSrlNo() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getVPNServicesByPopIdAndOltSrlNo :::" + e);
			e.printStackTrace();
		} finally {

		}
		return vpnSrvcNamesList;
	}
	
	@RequestMapping(value = "/getOLTPortSplitterData")
	public SplitterVO getOLTPortSplitterData(
			@RequestParam(value = "oltSrlNo", required = false) String oltSrlNo,
			@RequestParam(value = "lmoCode", required = false) String lmoCode, 
			@RequestParam(value = "oltPort", required = false) Integer oltPort, 
			@RequestParam(value = "l1slot", required = false) String l1slot ) {
		SplitterVO splitterVO = new SplitterVO();
		try {
			logger.info("ComsRestController :: getOLTPortSplitterData() :: START");
			splitterVO = aw.getCpeInformationService().getOLTPortSplitterData(oltSrlNo, lmoCode, oltPort, l1slot);
			logger.info("ComsRestController :: getOLTPortSplitterData() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getOLTPortSplitterData :::" + e);
			e.printStackTrace();
		} finally {

		}
		return splitterVO;
	}
	
	@RequestMapping(value = "/checkSplitterValue")
	public String checkSplitterValue(
			@RequestParam(value = "oltSrlNo", required = false) String oltSrlNo,
			@RequestParam(value = "oltPort", required = false) Integer oltPort, 
			@RequestParam(value = "portSlotValue", required = false) String portSlotValue ) {
		String status = "false";
		int cardnum=1;
		cardnum=aw.getPaymentService().getCardDetails(oltSrlNo);
			
		try {
			logger.info("ComsRestController :: checkSplitterValue() :: START");
			if(!oltSrlNo.equals(""))aw.getPaymentService().getCardDetails(oltSrlNo);
			status = aw.getOltPortDetailsService().checkSplitterValue(oltSrlNo, cardnum,oltPort, portSlotValue);
			logger.info("ComsRestController :: checkSplitterValue() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: checkSplitterValue :::" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno")
	public List<OLT> getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(
			@RequestParam(value = "districtId", required = false) Integer districtId,
			@RequestParam(value = "mandalId", required = false) Integer mandalId,
			@RequestParam(value = "subStnSlno", required = false) Integer subStnSlno) {
		List<OLT> oltsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno() :: START");
			oltsList = aw.getCpeInformationService().getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(districtId, mandalId, subStnSlno);
			logger.info("ComsRestController :: getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno :::" + e);
			e.printStackTrace();
		} finally {

		}
		return oltsList;
	}
	
	@RequestMapping(value = "/getCpeEmiCountByCpeModel")
	public Cpecharges getCpeEmiCountByCpeModel(@RequestParam(value = "profileId", required = false) Long profileId) {
		Cpecharges cpecharge = new Cpecharges();
		try {
			logger.info("ComsRestController :: getCpeEmiCountByCpeModel() :: START");
			cpecharge = aw.getCpeInformationService().getCpeEmiCountByCpeModel(profileId);
			logger.info("ComsRestController :: getCpeEmiCountByCpeModel() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getCpeEmiCountByCpeModel :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cpecharge;
	}
	
	@RequestMapping(value = "/getCpeChargesByCpeModel")
	public Cpecharges getCpeChargesByCpeModel(@RequestParam(value = "serialNumber", required = false) String serialNumber) {
		Cpecharges cpecharges = new Cpecharges();
		try {
			logger.info("ComsRestController :: getCpeChargesByCpeModel() :: START");
			cpecharges = aw.getCpeInformationService().getAlCpeChargesByCpeModel(serialNumber);
			logger.info("ComsRestController :: getCpeChargesByCpeModel() :: START");

		} catch (Exception e) {
			logger.info("ComsRestController :: getCpeChargesByCpeModel :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cpecharges;
	}
	
	@RequestMapping(value = "/getCpeAndIptvbosSrlNoCheck")
	public CpeChargeDetailsBO getCpeAndIptvbosSrlNoCheck(@RequestParam(value = "cpeId", required = false) String cpeId,
			@RequestParam(value = "lmoCode", required = false) String lmoCode, 
			@RequestParam(value = "lmoAgrmntMspCodes", required = false) String lmoAgrmntMspCodes,
			@RequestParam(value = "cpetypelov", required = false) String cpetypelov) {
		CpeChargeDetailsBO cpeChargeDetailsBO = new CpeChargeDetailsBO();
		try {
			cpeChargeDetailsBO = aw.getCpeInformationService().getCpeAndIptvbosSrlNoCheck(cpeId, lmoCode, lmoAgrmntMspCodes,cpetypelov);
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getCpeAndIptvbosSrlNoCheck :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeChargeDetailsBO;
	}
	
	@RequestMapping(value = "/enterpriseCustomersPage", method = RequestMethod.POST)
	public ComsHelperDTO enterpriseCustomersPage(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: showcustomers() :: START");
			comsHelperDTO = aw.getEnterpriseCustomerService().findEnterpriseCustomers(comsHelperDTO);
			logger.info("ComsRestController :: showcustomers() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: showcustomers" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getEnterpriseSubTypes")
	public List<Lovs> getEnterpriseSubTypes(@RequestParam(value = "custType", required = false) String custType) {
		List<Lovs> lovsList = new ArrayList<>();
		try {
			lovsList = aw.getLovsService().getLovValuesByLovName(custType);
		} catch (Exception e) {
			logger.error("The Exception ComsRestController :: getEnterpriseSubTypes()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return lovsList;
	}
	
	@RequestMapping(value = "/createEntCustomer", method = RequestMethod.GET)
	public ComsHelperDTO getCreateEntCustomer() {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			logger.info("EnterpriseCustomerController :: getCreateEntCustomer()");
			comsHelperDTO.setState(state);
			comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.ENTERPRISE_TYPES.getCode()));
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setMandalList(aw.getCpeInformationService().getAllMandals());
			comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
		} catch (Exception e) {
			logger.error("ComsRestController :: getCreateEntCustomer()" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}

	@RequestMapping(value = "/nodeForm", method = RequestMethod.GET)
	public ComsHelperDTO showNodeForm(@RequestParam(value = "customerCode", required = false) String customerCode,
			@RequestParam(value = "custType", required = false) String custType,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "billCycle", required = false) String billCycle,
			@RequestParam(value = "officeTypeLov", required = false) String officeTypeLov) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setMandalList(aw.getCpeInformationService().getAllMandals());
			comsHelperDTO.setBillCycle(billCycle);
			comsHelperDTO.setCustomerCode(customerCode);
			comsHelperDTO.setCustType(custType);
			comsHelperDTO.setEmailId(emailId);
			comsHelperDTO.setOfficeTypeLov(officeTypeLov);
		} catch (Exception e) {
			logger.error("ComsRestController :: showNodeForm()" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/showentcustomer", method = RequestMethod.GET)
	public String showEntCustomer(@RequestParam(value = "pmntResFlag", required = false) String pmntResFlag, @RequestParam(value = "custId", required = false) Long custId) {
		String parentCustCode = "";
		try {
			if (pmntResFlag != null && pmntResFlag.equalsIgnoreCase("0")) {
				parentCustCode = aw.getStoredProcedureDAO().executePaymentCustomer(custId).toString();
			} else {
				parentCustCode = custId.toString();
			}
		} catch (Exception e) {
			logger.error("ComsRestController :: showEntCustomer()" + e);
			e.printStackTrace();
		} finally {
		}
		return parentCustCode;
	}
	
	@RequestMapping(value = "/bulkUploadCafs", method = RequestMethod.POST)
	public ComsHelperDTO bulkUploadCafs( @RequestBody EnterpriseCustomerVO enterpriseCustomerVO ) {
		String jsonObject = "";
		List<ProductsVO> productsList = new ArrayList<>();
		List<ProductsVO> baseProductList = new ArrayList<>();
		List<ProductsVO> addOnProductList = new ArrayList<>();
		List<ProductsVO> oneTimeProductList = new ArrayList<>();
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			jsonObject = getProducts(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode(), enterpriseCustomerVO.getTenantCode(), ComsEnumCodes.REGION_TYPE.getCode(), null, null);
			productsList = mapper.readValue(jsonObject, TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
			for (ProductsVO products : productsList) {
				if (products.getProdType().equalsIgnoreCase("B")) {
					baseProductList.add(products);
				} else if (products.getProdType().equalsIgnoreCase("A")) {
					addOnProductList.add(products);
				} else if (products.getProdType().equalsIgnoreCase("O")){
					oneTimeProductList.add(products);
				}
			}
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setTenantType(aw.getLovsService().getTenantTypeBySI(ComsEnumCodes.SI_Tenant_Type.getCode()));
			comsHelperDTO.setBaseProductList(baseProductList);
			comsHelperDTO.setAddOnProductList(addOnProductList);
			comsHelperDTO.setOneTimeProductList(oneTimeProductList);
			comsHelperDTO.setProductsList(productsList);
		} catch (Exception e) {
			logger.error("ComsRestController :: showEntCustomer()" + e);
			e.printStackTrace();
		} finally {
			productsList = null;
			jsonObject = null;
			baseProductList = null;
			oneTimeProductList = null;
			addOnProductList = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/saveBulkCafUpload", method = RequestMethod.POST)
	public ComsHelperDTO saveBulkCafUpload(@RequestBody CustomerCafVO customerCafVO) throws Exception {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		Caf caf = new Caf();
		UploadHistory uploadHistory = new UploadHistory();
		List<Object[]> enterpriseCustomerList = new ArrayList<>();
		String message = "";
		try {
			logger.info("ComsRestController :: saveBulkCafUpload() :: START");
			Map<String, Integer> packagesMap = FileUtil.getListSize(customerCafVO.getIptvBoxList(), customerCafVO.getProdCode());
			filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
			customerCafVO.setProducts(filterPackagesVO.getSelectedPackagesList());
			if(customerCafVO.getBulkUpload().equalsIgnoreCase("MultipleCafs")) {
				uploadHistory = aw.getUploadHistoryService().saveUploadHistory(customerCafVO);
				if(uploadHistory.getUploadId() != null) {
					caf = aw.getCafService().saveBulkUploadCafs(customerCafVO, customerCafVO.getLoginId(), uploadHistory.getUploadId());
					message = !caf.getFailedRecordCount().equalsIgnoreCase("0") ? caf.getSuccessRecordCount() +" CAF(s) Uploaded Successfully and "+caf.getFailedRecordCount()+ " CAF(s) could not be Uploaded" : caf.getSuccessRecordCount() +" CAF(s) Uploaded Successfully";
					comsHelperDTO.setMessage(message);
				}
			} else if(customerCafVO.getBulkUpload().equalsIgnoreCase("SingalCaf")) {
				caf = aw.getCafService().saveBulkUploadCafs(customerCafVO, customerCafVO.getLoginId(), 0l);
				message = "CAF Inserted Successfully";
				comsHelperDTO.setMessage(message);
			} else {
				message = "CAF could not be Uploaded";
				comsHelperDTO.setMessage(message);
			}
			enterpriseCustomerList = aw.getEnterpriseCustomerService().findAllEnterpriseCustomers(customerCafVO.getLmoCode(), customerCafVO.getTenantType());
			comsHelperDTO.setEnterpriseCustomerList(enterpriseCustomerList);
			logger.info("ComsRestController :: saveBulkCafUpload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::saveBulkCafUpload() " + e);
			e.printStackTrace();
		} finally {
			filterPackagesVO = null;
			caf = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/saveVPNServiceExcelUpload", method = RequestMethod.POST)
	public String saveVPNServiceExcelUpload(@RequestBody VPNServiceExcelUploadVO vpnServiceExcelUploadVO) throws Exception {
		VPNSrvcNames vpnSrvcNames = new VPNSrvcNames();
		UploadHistory uploadHistory = new UploadHistory();
		String message = "";
		try {
			logger.info("ComsRestController :: saveVPNServiceExcelUpload() :: START");
				uploadHistory = aw.getUploadHistoryService().saveUploadHistory(vpnServiceExcelUploadVO);
				if(uploadHistory.getUploadId() != null) {
					vpnSrvcNames = aw.getCafService().saveVPNServicesUpload(vpnServiceExcelUploadVO, uploadHistory.getUploadId());
					message = !vpnSrvcNames.getFailedRecordCount().equalsIgnoreCase("0") ? vpnSrvcNames.getSuccessRecordCount() +" VPN Service Record(s) Uploaded Successfully and "+vpnSrvcNames.getFailedRecordCount()+ " VPN Service Record(s) could not be Uploaded" : vpnSrvcNames.getSuccessRecordCount() +" VPN Service Record(s) Uploaded Successfully";
				}
			logger.info("ComsRestController :: saveVPNServiceExcelUpload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::saveVPNServiceExcelUpload() " + e);
			e.printStackTrace();
		} finally {
			vpnSrvcNames = null;
			uploadHistory = null;
		}
		return message;
	}
	
	@RequestMapping(value = "/updateEntCustomer", method = RequestMethod.GET)
	public ComsHelperDTO updateEntCustomer(@RequestParam(value = "customerCode", required = false) Long customerCode) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Customer enterpriseCustomer = new Customer();
		try {
			enterpriseCustomer = aw.getEnterpriseCustomerService().findByEnterpriseCustomerId(customerCode);
			comsHelperDTO.setState(state);
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
			comsHelperDTO.setEnterpriseCustomer(enterpriseCustomer);
		} catch (Exception e) {
			logger.error("ComsRestController :: updateEntCustomer()" + e);
			e.printStackTrace();
		} finally {
			enterpriseCustomer = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/viewEntCustomer", method = RequestMethod.GET)
	public ComsHelperDTO viewEntCustomer(@RequestParam(value = "customerCode", required = false) Long customerCode) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Customer enterpriseCustomer = new Customer();
		String custType = "";
		try {
			enterpriseCustomer = aw.getEnterpriseCustomerService().findByEnterpriseCustomerId(customerCode);
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(enterpriseCustomer.getDistrict())));
			comsHelperDTO.setBlMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(enterpriseCustomer.getBlDistrict())));
			comsHelperDTO.setVillages(aw.getCpeInformationService().getVillagesByVillageId(Integer.parseInt(enterpriseCustomer.getCityVillage()), Integer.parseInt(enterpriseCustomer.getMandal()), Integer.parseInt(enterpriseCustomer.getDistrict())));
			comsHelperDTO.setBlVillages(aw.getCpeInformationService().getVillagesByVillageId(Integer.parseInt(enterpriseCustomer.getBlCityVillage()), Integer.parseInt(enterpriseCustomer.getBlMandal()), Integer.parseInt(enterpriseCustomer.getBlDistrict())));
			comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.ENTERPRISE_TYPES.getCode()));
			if (enterpriseCustomer.getCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
				custType = ComsEnumCodes.GOVT_TYPES.getCode();
			} else {
				custType = ComsEnumCodes.PRIVATE_TYPES.getCode();
			}
			comsHelperDTO.setOfficeTypeLovList(aw.getLovsService().getLovValuesByLovName(custType));
			comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
			comsHelperDTO.setVillageList(aw.getCpeInformationService().getVillagesByDistrictAndMandal(Integer.parseInt(enterpriseCustomer.getDistrict()), Integer.parseInt(enterpriseCustomer.getMandal())));
			comsHelperDTO.setEnterpriseCustomer(enterpriseCustomer);
		} catch (Exception e) {
			logger.error("ComsRestController :: viewEntCustomer()" + e);
			e.printStackTrace();
		} finally {
			enterpriseCustomer = null;
			custType = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value ="/showEntNodes", method = RequestMethod.GET)
	public ComsHelperDTO showEntNodes(@RequestParam(value = "customerCode", required = false) Long customerCode ) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Customer enterpriseCustomerNode = new Customer();
		try {
			enterpriseCustomerNode = aw.getEnterpriseCustomerService().findByEnterpriseCustomerId(customerCode);
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			if(enterpriseCustomerNode.getDistrict() != null && !enterpriseCustomerNode.getDistrict().isEmpty()) {
				comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(enterpriseCustomerNode.getDistrict())));
			}
			if(enterpriseCustomerNode.getBlDistrict() != null && !enterpriseCustomerNode.getBlDistrict().isEmpty()) {
				comsHelperDTO.setBlMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(enterpriseCustomerNode.getBlDistrict())));
			}
			if((enterpriseCustomerNode.getDistrict() != null && enterpriseCustomerNode.getMandal() != null && enterpriseCustomerNode.getCityVillage() != null) && (!enterpriseCustomerNode.getDistrict().isEmpty() && !enterpriseCustomerNode.getMandal().isEmpty() && !enterpriseCustomerNode.getCityVillage().isEmpty())) {
				comsHelperDTO.setVillages(aw.getCpeInformationService().getVillagesByVillageId(Integer.parseInt(enterpriseCustomerNode.getCityVillage()), Integer.parseInt(enterpriseCustomerNode.getMandal()), Integer.parseInt(enterpriseCustomerNode.getDistrict())));
			}
			if((enterpriseCustomerNode.getBlDistrict() != null && enterpriseCustomerNode.getBlMandal() != null && enterpriseCustomerNode.getBlCityVillage() != null) && (!enterpriseCustomerNode.getBlDistrict().isEmpty() && !enterpriseCustomerNode.getBlMandal().isEmpty() && !enterpriseCustomerNode.getBlCityVillage().isEmpty())) {
				comsHelperDTO.setBlVillages(aw.getCpeInformationService().getVillagesByVillageId(Integer.parseInt(enterpriseCustomerNode.getBlCityVillage()), Integer.parseInt(enterpriseCustomerNode.getBlMandal()), Integer.parseInt(enterpriseCustomerNode.getBlDistrict())));
			}
			if((enterpriseCustomerNode.getDistrict() != null && enterpriseCustomerNode.getMandal() != null) && (!enterpriseCustomerNode.getDistrict().isEmpty() && !enterpriseCustomerNode.getMandal().isEmpty())) {
				comsHelperDTO.setVillageList(aw.getCpeInformationService().getVillagesByDistrictAndMandal(Integer.parseInt(enterpriseCustomerNode.getDistrict()), Integer.parseInt(enterpriseCustomerNode.getMandal())));
			}
			comsHelperDTO.setEnterpriseCustomer(enterpriseCustomerNode);
		} catch(Exception e) {
			logger.error("ComsRestController :: showEntNodes()" + e);
			e.printStackTrace();
		} finally {
			enterpriseCustomerNode = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/viewEntNodes", method = RequestMethod.GET)
	public List<Customer> viewEntNodes(@RequestParam(value = "custId", required = false) String custId,
			@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<Customer> entNodeList = new ArrayList<>();
		try {
			entNodeList = aw.getEnterpriseCustomerService().findByEntParentCustCodeAndLmocode(custId, tenantCode);
		} catch (Exception e) {
			logger.error("ComsRestController :: viewEntNodes()" + e);
			e.printStackTrace();
		} finally {

		}
		return entNodeList;
	}
	
	@RequestMapping(value = "/saveEntCustomer", method = RequestMethod.POST)
	public ComsHelperDTO saveEnterpriseCustomer(@RequestBody EnterpriseCustomerVO enterpriseCustomerVO) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		List<Object[]> enterpriseCustomerList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: saveEnterpriseCustomer()");
			aw.getEnterpriseCustomerService().saveEnterpriseCustomer(enterpriseCustomerVO, enterpriseCustomerVO.getLoginId(), enterpriseCustomerVO.getTenantCode());
			enterpriseCustomerList = aw.getEnterpriseCustomerService().findAllEnterpriseCustomers(enterpriseCustomerVO.getTenantCode(), enterpriseCustomerVO.getTenantType());
			comsHelperDTO.setEnterpriseCustomerList(enterpriseCustomerList);
			if (!enterpriseCustomerVO.getCustId().isEmpty()) {
				if (enterpriseCustomerVO.getCustCode() != null) {
					comsHelperDTO.setMessage("The Organization Node Updated Successfully");
				} else {
					comsHelperDTO.setMessage("The Organization Updated Successfully");
				}
			} else {
				if (enterpriseCustomerVO.getCustCode() != null) {
					comsHelperDTO.setMessage("The Organization Node Created Successfully");
				} else {
					comsHelperDTO.setMessage("The Organization Created Successfully");
				}
			}
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController::saveEnterpriseCustomer() " + e);
			e.printStackTrace();
		} finally {
			enterpriseCustomerList = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getCafNumber", method = RequestMethod.GET)
	public Long getCafNumber() {
		Long cafNo = (long) 0.00;
		try {
			logger.info("getting getCafNumber !!!!");
			cafNo = aw.getStoredProcedureDAO().executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getCafNumber() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cafNo;
	}
	
	@RequestMapping(value = "/getMonthlyPaymentBill", method = RequestMethod.GET)
	public List<MonthlyPaymentBO> getMonthlyPaymentBill(@RequestParam(value = "mobileNo") String mobileNo,
			@RequestParam(value = "tenantCode") String tenantCode) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		try {
			cafList = aw.getCafService().getMonthlyCafDetails(mobileNo, tenantCode, null);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getMonthlyPaymentDetails() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cafList;
	}
	
	@RequestMapping(value = "/getMonthlyPaymentDetails", method = RequestMethod.GET)
	public List<MonthlyPaymentBO> getMonthlyPaymentDetails(@RequestParam(value = "mobileNo") String mobileNo,
			@RequestParam(value = "tenantCode") String tenantCode) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		try {
			cafList = aw.getCafService().getMonthlyCafDetailsForMobile(mobileNo, tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getMonthlyPaymentDetails() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cafList;
	}
	
	@RequestMapping(value = "/cafPaymentDetails", method = RequestMethod.GET)
	public CpeInformationVO cafPaymentDetails(@RequestParam(value = "cafNo") Long cafNo, @RequestParam(value = "billCycle") String billCycle) {
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		try {
			cpeInformationVO = aw.getCafService().getPaymentDetails(cafNo, billCycle);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::cafPaymentDetails() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cpeInformationVO;
	}
	
	@RequestMapping(value = "/getCoreSrvcCodeByCafNo", method = RequestMethod.GET)
	public CorpusResponceStatus getCoreSrvcCodeByCafNo(@RequestParam(value = "cafNo") Long cafNo) {
		CorpusResponceStatus corpusResponceStatus = new CorpusResponceStatus();
		String coreSrvcCodes = "";
		try {
			coreSrvcCodes = aw.getCafService().getCoreSrvcCodeByCafNo(cafNo);
			corpusResponceStatus.setStatusCode("200");
			corpusResponceStatus.setStatusMessage(coreSrvcCodes);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::cafPaymentDetails() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return corpusResponceStatus;
	}
	
	@RequestMapping(value = "/allenterpriseCustomers", method = RequestMethod.GET)
	public List<Customer> enterpriseCustomers(@RequestParam(value = "tenantCode") String tenantCode) {
		List<Customer> enterpriseCustomersList = new ArrayList<>();
		try {
			enterpriseCustomersList = aw.getEnterpriseCustomerService().findAllEntCustomers(tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::enterpriseCustomers() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return enterpriseCustomersList;
	}
	
	@RequestMapping(value = "/alldistricts", method = RequestMethod.GET)
	public List<Districts> getAllDistricts() {
		List<Districts> districtsList = new ArrayList<>();
		try {
			districtsList = aw.getCpeInformationService().getAllDistricts();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllDistricts() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return districtsList;
	}
	
	@RequestMapping(value = "/allmandals", method = RequestMethod.GET)
	public List<Mandals> getAllMandals() {
		List<Mandals> mandalList = new ArrayList<>();
		try {
			mandalList = aw.getCpeInformationService().getAllMandals();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllMandals() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return mandalList;
	}
	
	@RequestMapping(value = "/getAllVillages", method = RequestMethod.GET)
	public List<VillagesBO> getAllVillages() {
		List<VillagesBO> villagesList = new ArrayList<>();
		try {
			villagesList = aw.getCpeInformationService().getAllVillages();;
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllMandals() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return villagesList;
	}
	
	@RequestMapping(value = "/getAllLMOByDistrictAndMandalAndVillage", method = RequestMethod.GET)
	public List<TenantsBO> getAllLMOByDistrictAndMandalAndVillage(@RequestParam(value = "district", required = false) String district, 
			@RequestParam(value = "mandal", required = false) String mandal, @RequestParam(value = "village", required = false) String village ) {
		List<TenantsBO> tenantsList = new ArrayList<>();
		try {
			tenantsList = aw.getCpeInformationService().getAllLMOByDistrictAndMandalAndVillage(district, mandal, village);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllLMOByDistrictAndMandalAndVillage() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return tenantsList;
	}
	
	@RequestMapping(value = "/getOntIdandCustTypeList", method = RequestMethod.GET)
	public List<OntIdCustTypeBO> getOntIdandCustTypeList(@RequestParam(value = "cafNo", required = false) String cafNo){
		List<OntIdCustTypeBO> ontIdCustTypeList = new ArrayList<>();
		ontIdCustTypeList = aw.getCafService().getOntIdandCustTypeList(cafNo);
		return ontIdCustTypeList;
	}
	
	@RequestMapping(value = "/allVillagesByDistrictId", method = RequestMethod.GET)
	public List<VillagesBO> getAllVillagesByDistrictId(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<VillagesBO> villagesList = new ArrayList<>();
		try {
			String districtId = aw.getLovsService().getDistrictsByTenantCode(tenantCode);
			villagesList = aw.getCpeInformationService().getAllVillagesByDistrictId(districtId);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllVillagesByDistrictId() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return villagesList;
	}
	
	@RequestMapping(value = "/allSubstationsByDistrictId", method = RequestMethod.GET)
	public List<SubstationsBO> getAllSubstationsByDistrictId(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<SubstationsBO> substationsList = new ArrayList<>();
		try {
			String districtId = aw.getLovsService().getDistrictsByTenantCode(tenantCode);
			substationsList = aw.getCpeInformationService().getAllSubstationsByDistrictId(districtId);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllSubstationsByDistrictId() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return substationsList;
	}
	
	@RequestMapping(value = "/allOLTPortDetailsByTenantCode", method = RequestMethod.GET)
	public List<OLTPortDetails> getAllOLTPortDetailsByTenantCode(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<OLTPortDetails> oltPortDetailsList = new ArrayList<>();
		try {
			oltPortDetailsList = aw.getCpeInformationService().getAllOLTPortDetailsByTenantCode(tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllOLTPortDetailsByTenantCode() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return oltPortDetailsList;
	}
	
	@RequestMapping(value = "/allVillages", method = RequestMethod.GET)
	public List<TenantBusinessAreas> getAllVillages(@RequestParam(value = "subStnCodes") String subStnCodes, @RequestParam(value = "tenantCode") String tenantCode) {
		List<TenantBusinessAreas> villagesList = new ArrayList<>();
		try {
			String subStCodes = FileUtil.mspCodeSplitMethod(subStnCodes);
			villagesList = aw.getCpeInformationService().getAllVillagesBySubstnId(subStCodes, tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllVillages() " + ex);
			ex.printStackTrace();
		} finally {
			
		}
		return villagesList;
	}
	
	@RequestMapping(value = "/getRegionByDistrictAndMandalAndVillage", method = RequestMethod.GET)
	public Region getRegionByDistrictAndMandalAndVillage(@RequestParam(value = "district") String district, @RequestParam(value = "mandal") String mandal, @RequestParam(value = "village") String village ) {
		String regionCode = "";
		Region region = new Region();
		try {
			regionCode = aw.getCafService().getRegionCodeByPinCode(district, mandal, village);
			region.setRegionCode(regionCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllVillages() " + ex);
			ex.printStackTrace();
		} finally {
			
		}
		return region;
	}
	
	@RequestMapping(value = "/allsubstations", method = RequestMethod.GET)
	public List<Substations> getAllSubstations(@RequestParam(value = "subStationCodes") String subStationCodes) {
		List<Substations> substationsList = new ArrayList<>();
		try {
			String subStCodes = FileUtil.mspCodeSplitMethod(subStationCodes);
			substationsList = aw.getCpeInformationService().getAllSubStations(subStCodes);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllSubstations() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return substationsList;
	}
	
	@RequestMapping(value = "/alloltdata", method = RequestMethod.GET)
	public List<OLT> getAllOLTs(@RequestParam(value = "subStnSrlNo") String subStnSrlNo) {
		List<OLT> OLTList = new ArrayList<>();
		try {
			OLTList = aw.getCpeInformationService().getOLTSLNOBySubstationsrlno(subStnSrlNo);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllOLTs() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return OLTList;
	}
	
	@RequestMapping(value = "/allcpemodals", method = RequestMethod.GET)
	public List<CpeModal> getAllCpeModels() {
		List<CpeModal> cpeModelList = new ArrayList<>();
		try {
			logger.info("getting allcpemodals !!!!");
			cpeModelList = aw.getCpeInformationService().getAllCpeModals();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllCpeModels() " + ex);
			ex.printStackTrace();
		} finally {
			
		}
		return cpeModelList;
	}
	
	@RequestMapping(value = "/allOLTPortDetails", method = RequestMethod.GET)
	public List<OLTPortDetails> allOLTPortDetailsByTenantCode(@RequestParam(value = "tenantCode") String tenantCode) {
		List<OLTPortDetails> oLTPortDetailsList = new ArrayList<>();
		try {
			logger.info("getting allOLTPortDetailsByTenantCode !!!!");
			oLTPortDetailsList = aw.getOltPortDetailsService().getAllOLTPortDetailsByTenantCode(tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::allOLTPortDetailsByTenantCode() " + ex);
			ex.printStackTrace();
		} finally {
			
		}
		return oLTPortDetailsList;
	}
	
	@RequestMapping(value = "/getSelectedPackageInformationByCafno", method = RequestMethod.GET)
	public CustomerCafVO getSelectedPackageInformationByCafno(@RequestParam(value = "cafNo", required = false) Long cafNo, @RequestParam(value = "tenantType", required = false) String tenantType) {
		CustomerCafVO customerCafVO = new CustomerCafVO();
		try {
			logger.info("getting getSelectedPackageInformationByCafno !!!!");
			customerCafVO = aw.getCafService().getPackageInformation(cafNo, tenantType);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getSelectedPackageInformationByCafno() " + ex);
			ex.printStackTrace();
		} finally {
			
		}
		return customerCafVO;
	}
	
	@RequestMapping(value = "/getTenantWalletBalence", method = RequestMethod.GET)
	public TenantWalletVO getTenantWalletBalence(@RequestParam(value = "tenantCode") String tenantCode) {
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		try {
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(tenantCode);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getTenantWalletBalence() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return tenantWalletVO;
	}
	
	@RequestMapping(value = "/getAllCpeCharges", method = RequestMethod.GET)
	public List<Cpecharges> getAllCpeCharges(@RequestParam(value = "region") String region) {
		List<Cpecharges> cpechargesList = new ArrayList<>();
		try {
			cpechargesList = aw.getCpeInformationService().getAlCpeCharges(region);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllCpeCharges() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cpechargesList;
	}
	
	@RequestMapping(value = "/searchMultiActionCafDetails", method = RequestMethod.POST)
	public List<CafAndCpeChargesVO> searchMultiActionCafDetails(@RequestBody MultiAction multiAction) {
		List<CafAndCpeChargesVO> cafProductsList = new ArrayList<>();
		PageObject pageObject = null;
		try {
			cafProductsList = aw.getCafService().searchCafDetails(multiAction, pageObject);
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::searchMultiActionCafDetails() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return cafProductsList;
	}
	
	@RequestMapping(value = "/getSISelectedPackagesList", method = RequestMethod.POST)
	public List<ProductsVO> getSISelectedPackagesList(@RequestBody CustomerCafVO customerCafVO) {
		List<ProductsVO> selectedProductsList = new ArrayList<>();
		List<ProductsVO> productsList = new ArrayList<>();
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		Map<String, Integer> packagesMap = new HashMap<>();
		try {
			String regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(),customerCafVO.getCity());
			regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
			String jsonObject = getProducts(customerCafVO.getCustType(), customerCafVO.getLmoCode(), regionCode, null, null);
			productsList = mapper.readValue(jsonObject, TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
			customerCafVO.setProducts(productsList);
			Object[] filterPkgDtls = aw.getCafProductsDao().getFilterPackageDetails(customerCafVO.getCafNo());
			customerCafVO.setProdCode(filterPkgDtls[0].toString());
			customerCafVO.setTenantCode(filterPkgDtls[1].toString());
			customerCafVO.setAgruniqueid(filterPkgDtls[2].toString());
			filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
			selectedProductsList = filterPackagesVO.getSelectedPackagesList();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getSISelectedPackagesList() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return selectedProductsList;
	}
	
	@RequestMapping(value = "/getLMOVPNSrvcNamesList")
	public List<VPNSrvcNames> getLMOVPNSrvcNamesList(@RequestParam(value = "subStnId", required = false) String subStnId) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: START");
			vpnSrvcNamesList = aw.getCpeInformationService().getLMOVPNSrvcNamesList(subStnId);
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList :::" + e);
			e.printStackTrace();
		} finally {

		}
		return vpnSrvcNamesList;
	}
	
	@RequestMapping(value = "/getSILMODistricts")
	public List<Districts> getSILMODistricts(@RequestParam(value = "tenantcode", required = false) String tenantcode) {
		List<Districts> dstrictList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: START");
			dstrictList = aw.getCpeInformationService().getSILMODistricts(tenantcode);
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList :::" + e);
			e.printStackTrace();
		} finally {

		}
		return dstrictList;
	}
	
	@RequestMapping(value = "/getSILMOMandals")
	public List<Mandals> getSILMOMandals(@RequestParam(value = "tenantcode", required = false) String tenantcode, @RequestParam(value = "district", required = false) String district) {
		List<Mandals> mandalList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: START");
			mandalList = aw.getCpeInformationService().getSILMOMandals(tenantcode, district);
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList() :: END");
		} catch (Exception e) {
			logger.info("ComsRestController :: getLMOVPNSrvcNamesList :::" + e);
			e.printStackTrace();
		} finally {

		}
		return mandalList;
	}
	
	@RequestMapping(value = "/savePayment", method = RequestMethod.POST)
	public CorpusResponceStatus savePayment(@RequestBody PaymentVO paymentVO) {
		
		// added by chaitanya_xyz
		int cardnum=1;
		
		CorpusResponceStatus corpusResponceStatus = new CorpusResponceStatus();
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		String status = "";
		String onuNumber = "";
		Gson gson = new Gson();
		String paymentJson = "";
		String regionCode = "";
		String jsonObject = "";
		List<ProductsVO> productsList = new ArrayList<>();
		List<IPTVBoxVO> iptvBoxVOsList = new ArrayList<>();
		CustomerCafVO customerCafVO = null;
		CpeInformationVO cpeInformationVO = null;
		Boolean oltstatus=false;
		try {
			logger.info("getting savePayment !!!!");
			if(paymentVO.getOltSrlNo()!=null && !paymentVO.getOltSrlNo().equals("")){
				cardnum=aw.getPaymentService().getCardDetails(paymentVO.getOltSrlNo());
				aw.getPaymentService().updateCafCard(paymentVO.getCafNo(), cardnum);
				if(cardnum==1)
					paymentVO.setOltPortNo(paymentVO.getOltPortNo());
				oltstatus=aw.getPaymentService().checkOltstatus(paymentVO.getOltSrlNo());
				if(!oltstatus)
					throw new Exception("OLT DOWN!!please Try After Some Time Please resubmit the CAF from Pending CAF forms");
			}else{
				cardnum=aw.getPaymentService().getCardDetailsByCaf(paymentVO.getCafNo().toString());
			}
			String mobileVesion = aw.getLovsService().getMobileVersion(ComsEnumCodes.MOBILE_VERSION.getCode());
			if(paymentVO.getCustomerCafVO().getVersion() != null && mobileVesion.equalsIgnoreCase(paymentVO.getCustomerCafVO().getVersion())) {
				if (paymentVO.getPayment() != null) {
					paymentJson = gson.toJson(paymentVO);
					logger.info("The Save Payment Json is :" + paymentJson);
					aw.getPaymentService().saveMonthlyPayment(paymentVO, paymentVO.getLoginId());
					
					SmsDTO smsDto = new SmsDTO();
					smsDto.setMobileNo(paymentVO.getMobileNo());
					String msg = "Dear Customer, We have received Cash Payment of Rs "+paymentVO.getPaidAmount()+" for your APSFL Customer ID: "+paymentVO.getCustId()+". Thank You";
					smsDto.setMsg(msg);
					smsApi.sendSMS(smsDto);
					logger.info("Message sent Successfully.");
					
					corpusResponceStatus.setStatusCode("200");
					corpusResponceStatus.setStatusMessage("The Monthly Payment Created Successfully.");
				} else {
					Caf caf = aw.getCafService().findByCafNo(paymentVO.getCustomerCafVO().getCafNo());
					if(caf.getStatus() == ComsEnumCodes.Caf_BulkUpload_status.getStatus() || caf.getStatus() == ComsEnumCodes.Caf_Edit_BulkUpload_status.getStatus() || caf.getStatus() == ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus()) {
						cpeInformationVO = aw.getCpeInformationService().getCpeChargesInformation(paymentVO.getCustomerCafVO());
						paymentVO.setCpeReleasedCharge(cpeInformationVO.getCpeCharge());
						if(paymentVO.getSource() != null && paymentVO.getSource().equalsIgnoreCase(ComsEnumCodes.SOURCE_CODE.getCode())) {
							customerCafVO = paymentVO.getCustomerCafVO();
							if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode())) {
								Long pmntCustId = aw.getStoredProcedureDAO().executePaymentCustomer(paymentVO.getCustomerCafVO().getCustId().longValue());
								customerCafVO.setCustCode(pmntCustId.toString());
							}
							regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(),customerCafVO.getCity());
							regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
							jsonObject = getProducts(customerCafVO.getCustType(), customerCafVO.getLmoCode(), regionCode, null, null);
							productsList = mapper.readValue(jsonObject,TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
							customerCafVO.setProducts(productsList);
							Map<String, Integer> packagesMap = new HashMap<>();
							filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
							customerCafVO.setProducts(filterPackagesVO.getSelectedPackagesList());
							paymentVO.setCustomerCafVO(customerCafVO);
						} else {
							if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode())) {
								customerCafVO = paymentVO.getCustomerCafVO();
								Long pmntCustId = aw.getStoredProcedureDAO().executePaymentCustomer(paymentVO.getCustomerCafVO().getCustId().longValue());
								customerCafVO.setCustCode(pmntCustId.toString());
								if(paymentVO.getCustomerCafVO().getTenantType().equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
									Object[] filterPkgDtls = aw.getCafProductsDao().getFilterPackageDetails(paymentVO.getCafNo());
									Cpecharges cpecharges = aw.getCpeInformationService().getAlCpeChargesByCpeModel(customerCafVO.getCpeId());
									Object[] object = aw.getCafService().getMacAddressBySerialNo(customerCafVO.getCpeId());
									customerCafVO.setInstCharge(cpecharges.getInstcharges().floatValue());
									customerCafVO.setProdCode(filterPkgDtls[0].toString());
									customerCafVO.setTenantCode(filterPkgDtls[1].toString());
									customerCafVO.setAgruniqueid(filterPkgDtls[2].toString());
									regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(),customerCafVO.getCity());
									regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
									jsonObject = getProducts(customerCafVO.getCustType(), customerCafVO.getLmoCode(), regionCode, null, null);
									productsList = mapper.readValue(jsonObject,TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
									customerCafVO.setProducts(productsList);
									customerCafVO.setOnuMacAddress(object[0] != null ? object[0].toString() : "");
									customerCafVO.setCpeModal(object[1] != null ? object[1].toString() : "0");
									if(!customerCafVO.getIptvBoxList().isEmpty()) {
										for(IPTVBoxVO iPTVBoxVO : customerCafVO.getIptvBoxList()) {
											if(iPTVBoxVO.getStbSerialNo() != null && iPTVBoxVO.getStbModel() != null ) {
												if(!iPTVBoxVO.getStbSerialNo().isEmpty() && !iPTVBoxVO.getStbModel().isEmpty() ) {
													IPTVBoxVO ipBoxVO1 = new IPTVBoxVO();
													Object[] IPTBObject = aw.getCafService().getMacAddressBySerialNo(iPTVBoxVO.getStbSerialNo());
													ipBoxVO1.setIptvSrvcCodes(iPTVBoxVO.getIptvSrvcCodes());
													ipBoxVO1.setMacAddress(IPTBObject[0] != null ? IPTBObject[0].toString() : "");
													ipBoxVO1.setStbModel(IPTBObject[1] != null ? IPTBObject[1].toString() : "0");
													ipBoxVO1.setStbEmiPrice(iPTVBoxVO.getStbEmiPrice());
													ipBoxVO1.setStbInstallmentCount(iPTVBoxVO.getStbInstallmentCount());
													ipBoxVO1.setStbLease(iPTVBoxVO.getStbLease());
													ipBoxVO1.setStbModel(iPTVBoxVO.getStbModel());
													ipBoxVO1.setStbPrice(iPTVBoxVO.getStbPrice());
													ipBoxVO1.setStbSerialNo(iPTVBoxVO.getStbSerialNo());
													iptvBoxVOsList.add(ipBoxVO1);
												}
											}
										}
										customerCafVO.setIptvBoxList(iptvBoxVOsList);
									}
									Map<String, Integer> packagesMap = FileUtil.getListSize(customerCafVO.getIptvBoxList(), customerCafVO.getProdCode());
									filterPackagesVO = aw.getCafService().getFilterPackagessList(customerCafVO, packagesMap);
									customerCafVO.setProducts(filterPackagesVO.getSelectedPackagesList());
									paymentVO.setCustomerCafVO(customerCafVO);
								} else {
									customerCafVO.setLmoCode(paymentVO.getTenantCode());
									paymentVO.setCustomerCafVO(customerCafVO);
								}
								paymentVO.setCustomerCafVO(customerCafVO);
							} else {
								customerCafVO = paymentVO.getCustomerCafVO();
								customerCafVO.setLmoCode(paymentVO.getTenantCode());
								paymentVO.setCustomerCafVO(customerCafVO);
							}
						}
						paymentJson = gson.toJson(paymentVO);
						logger.info("The Save Payment Json is :" + paymentJson);
						
						if (paymentVO.getFeasibility().equalsIgnoreCase("N")) {
							aw.getCustomerService().updateCustomer(paymentVO, paymentVO.getCustomerCafVO().getLoginId(),cardnum);
							corpusResponceStatus.setStatusCode("200");
							corpusResponceStatus.setStatusMessage("Feasibility Status has been Updated Successfully.");
						} else if (paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
							//modified by chaitanya_xyz
							//onuNumber = aw.getStoredProcedureDAO().executePullOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), 1, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()));
							onuNumber = aw.getStoredProcedureDAO().executePullOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), cardnum, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()));
							
							if (FileUtil.NumberCheck(onuNumber).equalsIgnoreCase("true")) {
								/*aw.getCafAccountService().saveCafAccount(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getLoginId());
								logger.info("The cafAccount Details Created Successfully");*/
								
								if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									status = aw.getPaymentService().saveEnterpriseCustomerPayment(paymentVO, paymentVO.getCustomerCafVO().getLoginId(), onuNumber,cardnum);
									status = !status.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + status : "Your Order Created Successfully.";
									
									aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(paymentVO.getCustomerCafVO().getCafNo().toString(), 1);
									logger.info("Provisioning Request Jsons Created Successfully");
									
									corpusResponceStatus.setStatusCode("200");
									corpusResponceStatus.setStatusMessage(status);
								} else {
									status = aw.getPaymentService().createPayment(paymentVO, paymentVO.getCustomerCafVO().getLoginId(), onuNumber,cardnum);
									status = !status.isEmpty() ? "The Payment Created Successfully. Your Allocated Telephone No(s) is " + status : "The Payment Created Successfully.";
									
									String result = aw.getPaymentService().processWOPayments(paymentVO);
									if(result.equalsIgnoreCase("0")) {
										logger.info("WorkOrder Process Payments and Provisioning Request Jsons Created Successfully");
									}
									
									corpusResponceStatus.setStatusCode("200");
									corpusResponceStatus.setStatusMessage(status);
								}
							} else {
								corpusResponceStatus.setStatusCode("500");
								corpusResponceStatus.setStatusMessage(onuNumber);
							}
						}
					} else {
						status = "CAF has been Successfully Submitted earlier. Please check CAF current Status through Search CAF";
						corpusResponceStatus.setStatusCode("200");
						corpusResponceStatus.setStatusMessage(status);
					}
				}
			} else {
				corpusResponceStatus.setStatusCode("500");
				corpusResponceStatus.setStatusMessage("Please update e-CAF Mobile Application to Latest Version by Downloading from BSS Portal.");
			}
		} catch (Exception ex) {
			if(!onuNumber.equals("")){
				String exception = aw.getStoredProcedureDAO().executePushOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), 1, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()), Integer.parseInt(onuNumber));
				if(exception.equalsIgnoreCase("0"))
					logger.info("ONU Id RoleBacked Successfully.");
			}			
			status = ex.getMessage() + " so we can't process Payment right now ";
			corpusResponceStatus.setStatusCode("500");
			corpusResponceStatus.setStatusMessage(status);
			logger.error("The Exception is ComsRestController::savePayment() " + ex);
			ex.printStackTrace();
		} finally {
			status = null;
			gson = null;
			paymentJson = null;
		}
		return corpusResponceStatus;
	}
	
	@RequestMapping(value = "/saveCaf", method = RequestMethod.POST)
	public CorpusResponceStatus saveCaf(@RequestBody CustomerCafVO customerCafVO) {
		CorpusResponceStatus corpusResponceStatus = new CorpusResponceStatus();
		String customerId = "";
		Customer customer = new Customer();
		Gson gson = new Gson();
		String cafJson = "";
		try {
			cafJson = gson.toJson(customerCafVO);
			logger.info("The Save Caf Json is :" + cafJson);
			String mobileVesion = aw.getLovsService().getMobileVersion(ComsEnumCodes.MOBILE_VERSION.getCode());
			if(customerCafVO.getVersion() != null && mobileVesion.equalsIgnoreCase(customerCafVO.getVersion())) {
				customer = aw.getCustomerService().createCustomers(customerCafVO, customerCafVO.getLoginId(), customerCafVO.getTenantCode());
				customerId = customer != null ? customer.getCustId().toString() : customerCafVO.getEntCustomerCode();
				corpusResponceStatus.setStatusCode("200");
				corpusResponceStatus.setStatusMessage(customerId.toString());
			} else {
				corpusResponceStatus.setStatusCode("500");
				corpusResponceStatus.setStatusMessage("Please update e-CAF Mobile Application to Latest Version by Downloading from BSS Portal.");
			}
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::saveCaf() " + ex);
			ex.printStackTrace();
		} finally {
			customer = null;
			customerId = null;
			gson = null;
			cafJson = null;
		}
		return corpusResponceStatus;
	}
	
	@RequestMapping(value = "/updateOSDFPDetails", method = RequestMethod.POST)
	public CorpusResponceStatus updateOSDFPDetails(@RequestBody CorpusResponseVO corpusResponseVO ) {
		CorpusResponceStatus corpusResponceStatus = new CorpusResponceStatus();
		String status = "false";
		try {
			status = aw.getOsdFingerPrintDetailsService().updateOSDFingerPrintDetails(corpusResponseVO);
			if (status.equalsIgnoreCase("true")) {
				corpusResponceStatus.setStatusCode("200");
				corpusResponceStatus.setStatusMessage("Success");
			} else {
				corpusResponceStatus.setStatusCode("500");
				corpusResponceStatus.setStatusMessage("Failed");
			}
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::updateOSDFPDetails() " + ex);
			ex.printStackTrace();
		} finally {
			status = null;
		}
		return corpusResponceStatus;
	}
	
	@RequestMapping(value = "/saveEnterpriseCustomer", method = RequestMethod.POST)
	public String saveMobileAppEnterpriseCustomer(@RequestBody EnterpriseCustomerVO enterpriseCustomerVO) {
		Customer enterpriseCustomer = new Customer();
		String response = "";
		try {
			logger.info("getting saveCaf !!!!");
			enterpriseCustomer = aw.getEnterpriseCustomerService().saveEnterpriseCustomer(enterpriseCustomerVO, enterpriseCustomerVO.getLoginId(), enterpriseCustomerVO.getTenantCode());
			if (enterpriseCustomer.getCustId() != null) {
				response = "Success";
			} else {
				response = "Failed";
			}
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::saveMobileAppEnterpriseCustomer() " + ex);
			ex.printStackTrace();
		} finally {
			enterpriseCustomer = null;
		}
		return response;
	}
	
	@RequestMapping(value = "/getAllTaxRegions", method = RequestMethod.GET)
	public List<TaxRegionMapping> getAllTaxRegions() {
		List<TaxRegionMapping> taxRegionMappingList = new ArrayList<TaxRegionMapping>();
		try {
			taxRegionMappingList = aw.getTaxRegionMappingService().getAllTaxRegionMappingList();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllTaxRegions() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return taxRegionMappingList;
	}
	
	@RequestMapping(value = "/getAllTaxes", method = RequestMethod.GET)
	public List<TaxMaster> getAllTaxes() {
		List<TaxMaster> taxMasterList = new ArrayList<TaxMaster>();
		try {
			taxMasterList = aw.getTaxRegionMappingService().getAllTaxMasterList();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllTaxes() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return taxMasterList;
	}
	
	@RequestMapping(value = "/getAllChargeCodes", method = RequestMethod.GET)
	public List<ChargeCodes> getAllChargeCodes() {
		List<ChargeCodes> chargeCodesList = new ArrayList<ChargeCodes>();
		try {
			chargeCodesList = aw.getTaxRegionMappingService().getAllChargeCodesList();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllChargeCodes() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return chargeCodesList;
	}
	
	@RequestMapping(value = "/getAllChargeTaxes", method = RequestMethod.GET)
	public List<ChargeTaxes> getAllChargeTaxes() {
		List<ChargeTaxes> chargeTaxesList = new ArrayList<ChargeTaxes>();
		try {
			logger.info("getting getAllChargeTaxes !!!!");
			chargeTaxesList = aw.getTaxRegionMappingService().getAllChargeTaxesList();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getTenantWalletBalence() " + ex);
			ex.printStackTrace();
		}
		return chargeTaxesList;
	}
	
	@RequestMapping(value = "/getAllFeatureParams", method = RequestMethod.GET)
	public List<FeatureParams> getAllFeatureParams() {
		List<FeatureParams> featureParamsList = new ArrayList<FeatureParams>();
		try {
			logger.info("getting getAllChargeTaxes !!!!");
			featureParamsList = aw.getCpeInformationService().getAllFeatureParams();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getTenantWalletBalence() " + ex);
			ex.printStackTrace();
		}
		return featureParamsList;
	}
	
	@RequestMapping(value = "/getAllSrvcFeatures", method = RequestMethod.GET)
	public List<SrvcFeatures> getAllSrvcFeatures() {
		List<SrvcFeatures> srvcFeaturesList = new ArrayList<SrvcFeatures>();
		try {
			logger.info("getting getAllChargeTaxes !!!!");
			srvcFeaturesList = aw.getCpeInformationService().getAllSrvcFeatures();
		} catch (Exception ex) {
			logger.error("The Exception is ComsRestController::getAllSrvcFeatures() " + ex);
			ex.printStackTrace();
		} finally {

		}
		return srvcFeaturesList;
	}
	
	@RequestMapping(value = "/getAllCpeStockByLmoCode", method = RequestMethod.GET)
	public List<CpeStockVO> getAllCpeStockByLmoCode(@RequestParam(value = "lmoCode") String lmoCode) {
		List<CpeStockVO> cpeStockList = new ArrayList<>();
		try {
			cpeStockList = aw.getCpeInformationService().getAllCpeStockByLmoCode(lmoCode);
		} catch (Exception ex) {
			logger.error("The Exception is :: ComsRestController :: getAllCpeStockByLmoCode" + ex);
			ex.printStackTrace();
		} finally {

		}
		return cpeStockList;
	}
	
	@RequestMapping(value = "/getCafCustDetailsToModify", method = RequestMethod.GET)
	public ComsHelperDTO getCafCustDetailsToModify(@RequestParam(value = "cafNo", required = false) Long cafNo,
			@RequestParam(value = "tenantCode", required = false) String tenantCode, 
			@RequestParam(value = "tenantType", required = false) String tenantType) {
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		Caf caf = new Caf();
		Customer customer = new Customer();
		Customer enterpriseCustomer = new Customer();
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		String subStCodes = "";
		String subStnCodes = "";
		List<Substations> substationsList = new ArrayList<>();
		try {
			logger.info("CustomerController :: getCafCustDetailsToModify()");
			caf = aw.getCafService().findByCafNo(cafNo);
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
				if(caf.getPopDistrict() != null && caf.getPopMandal() != null) {
					comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(caf.getPopDistrict()));
					comsHelperDTO.setInstVillageList(aw.getCpeInformationService().getVillagesByDistrictAndMandal(Integer.parseInt(caf.getInstDistrict()), Integer.parseInt(caf.getInstMandal())));
					substationsList = aw.getCpeInformationService().getSubstationsByDistrictIdAndMandalId(caf.getPopDistrict(), caf.getPopMandal());
				} else {
					comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(caf.getInstDistrict())));
					comsHelperDTO.setInstVillageList(aw.getCpeInformationService().getVillagesByDistrictAndMandal(Integer.parseInt(caf.getInstDistrict()), Integer.parseInt(caf.getInstMandal())));
					substationsList = aw.getCpeInformationService().getSubstationsByDistrictIdAndMandalId(Integer.parseInt(caf.getInstDistrict()), Integer.parseInt(caf.getInstMandal()));
				}
			} else {
				if(caf.getPopSubstnno() != null && caf.getInstDistrict() != null && caf.getInstMandal() != null) {
					subStCodes = FileUtil.mspCodeSplitMethod(caf.getPopSubstnno());
					substationsList = aw.getCpeInformationService().getAllSubStations(subStCodes);
					comsHelperDTO.setDistrict(aw.getCpeInformationService().getDistrictByDistrictId(Integer.parseInt(caf.getInstDistrict())));
					comsHelperDTO.setMandal(aw.getCpeInformationService().getMandalsByDistrictIdAndMandalSrlNo(Integer.parseInt(caf.getInstDistrict()), Integer.parseInt(caf.getInstMandal())));
					comsHelperDTO.setTenantBusinessAreasList(aw.getCpeInformationService().getAllVillagesBySubstnId(subStCodes, tenantCode));
				} else {
					subStnCodes = aw.getTenantWalletService().getLMOsubstations(tenantCode);
					subStCodes = FileUtil.mspCodeSplitMethod(subStnCodes);
					substationsList = aw.getCpeInformationService().getAllSubStations(subStCodes);
				}
			}
			comsHelperDTO.setSubStationsList(substationsList);
			if(caf.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				customer = aw.getCustomerService().findByCustomerId(caf.getCustId());
				comsHelperDTO.setCustomer(customer);
			} else {
				enterpriseCustomer = aw.getEnterpriseCustomerService().findByEnterpriseCustomerId(caf.getCustId());
				comsHelperDTO.setEnterpriseCustomer(enterpriseCustomer);
			}
			tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(tenantCode);
			comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
			comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.TITLES.getCode()));
			comsHelperDTO.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDTO.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDTO.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDTO.setAlert(tenantWalletVO.getAlert());
			comsHelperDTO.setFlag(tenantWalletVO.getFlag());
			comsHelperDTO.setCaf(caf);
		} catch (Exception e) {
			logger.info("ComsRestController :: getCafCustDetailsToModify()" + e);
			e.printStackTrace();
		} finally {
			caf = null;
			customer = null;
			tenantWalletVO = null;
			subStCodes = null;
			enterpriseCustomer = null;
			substationsList = null;
		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getCafDetailsToAPSFLCafModify", method = RequestMethod.GET)
	public ComsHelperDTO getCafDetailsToAPSFLCafModify(@RequestParam(value = "cafNo", required = false) Long cafNo,
			@RequestParam(value = "tenantCode", required = false) String tenantCode, 
			@RequestParam(value = "tenantType", required = false) String tenantType) {
		Caf caf = new Caf();
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			logger.info("CustomerController :: getCafCustDetailsToModify()");
			caf = aw.getCafService().findByCafNo(cafNo);
			String pocname = caf.getContactPerson();
			if(pocname.indexOf("(") != -1) {
				String[] splitString = pocname.split("\\(");
				caf.setContactPerson(splitString[0]);
				caf.setPocDesignation(splitString[1].replace(")", ""));
			} else {
				caf.setContactPerson(pocname);
			}
			String cafLocation = caf.getCpePlace();
			if(cafLocation.contains("^")) {
				String[] splitString2 = cafLocation.split("\\^");
				caf.setCpePlace(splitString2[0]);
				caf.setInstLocality(splitString2[1]);
			} else {
				String[] splitString2 = cafLocation.split(",");
				caf.setCpePlace(splitString2[0]);
				caf.setInstLocality(splitString2[1]);
			}
			comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
			comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(caf.getInstDistrict())));
			comsHelperDTO.setTenantType(aw.getLovsService().getTenantTypeBySI(ComsEnumCodes.SI_Tenant_Type.getCode()));
			comsHelperDTO.setCaf(caf);
		} catch (Exception e) {
			logger.info("ComsRestController :: getCafCustDetailsToModify()" + e);
			e.printStackTrace();
		} finally {
			caf = null;
		}
		return comsHelperDTO;
	}

	@RequestMapping(value = "/getCafDetailsForEdit", method = RequestMethod.GET)
	public CustomerCafModifyVo getCafDetailsForEdit(@RequestParam(value = "cafNo") Long cafNo, @RequestParam(value = "status", required = false) String status) {
		CustomerCafModifyVo custCafModifyObject = new CustomerCafModifyVo();
		try {
			custCafModifyObject = aw.getCpeInformationService().getCafDetailsForEdit(cafNo, status);
		} catch (Exception ex) {
			logger.error("The Exception is :: ComsRestController :: getCafDetailsForEdit" + ex);
			ex.printStackTrace();
		} finally {

		}
		return custCafModifyObject;
	}

	@RequestMapping(value = "/getAllCpeStockByMspCode", method = RequestMethod.GET)
	public List<CpeStockVO> getAllCpeStockByMspCode(@RequestParam(value = "mspCode") String mspCode) {
		List<CpeStockVO> cpeStockList = new ArrayList<>();
		try {
			String agrmntMspcodes = FileUtil.mspCodeSplitMethod(mspCode);
			cpeStockList = aw.getCpeInformationService().getAllCpeStockByMspCode(agrmntMspcodes);
		} catch (Exception ex) {
			logger.error("The Exception is :: ComsRestController :: getAllCpeStockByMspCode" + ex);
			ex.printStackTrace();
		} finally {

		}
		return cpeStockList;
	}
	
	@RequestMapping(value = "/cafBulkUploadErrorsPage", method = RequestMethod.POST)
	public ComsHelperDTO cafBulkUploadErrorsPage(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: cafBulkUploadErrorsPage() :: START");
			comsHelperDTO = aw.getCustomerService().getCafBulkUploadOrVPNUploadErrors(comsHelperDTO,ComsEnumCodes.ENT_Upload_caf_Type.getCode());
			logger.info("ComsRestController :: showcustomers() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: cafBulkUploadErrorsPage" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/cafBulkUploadErrorDownload", method = RequestMethod.GET)
	public List<EntCafStage> cafBulkUploadErrorDownload(@RequestParam(value = "uploadId", required=false) String uploadId) {
		List<EntCafStage> cafErrorList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: cafBulkUploadErrors() :: START");
			cafErrorList = aw.getCustomerService().stageCafBulkUploadErrors(uploadId);
			logger.info("ComsRestController :: cafBulkUploadErrors() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: cafBulkUploadErrors" + e);
			e.printStackTrace();
		} finally {

		}
		return cafErrorList;
	}
	
	@RequestMapping(value = "/vpnUploadErrPage", method = RequestMethod.POST)
	public ComsHelperDTO vpnUploadErrPage(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: vpnUploadErrPage() :: START");
			comsHelperDTO = aw.getCustomerService().getCafBulkUploadOrVPNUploadErrors(comsHelperDTO,ComsEnumCodes.VPN_Upload_Type.getCode());
			logger.info("ComsRestController :: vpnUploadErrPage() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: vpnUploadErrPage" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/vpnUploadErrorDownload", method = RequestMethod.GET)
	public List<VPNSrvcNamesStage> vpnUploadErrorDownload(@RequestParam(value = "uploadId", required=false) String uploadId) {
		List<VPNSrvcNamesStage> cafErrorList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: cafBulkUploadErrors() :: START");
			cafErrorList = aw.getCustomerService().stageVPNUploadErrors(uploadId);
			logger.info("ComsRestController :: cafBulkUploadErrors() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: cafBulkUploadErrors" + e);
			e.printStackTrace();
		} finally {

		}
		return cafErrorList;
	}
	
	@RequestMapping(value = "/editCustomer", method = RequestMethod.GET)
    public ComsHelperDTO editCustomer(@RequestParam(value = "custId", required = false) Long custId,
            @RequestParam(value = "tenantCode", required = false) String tenantCode) {
        ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
        String custType = "";
        try {
            logger.info("ComsRestController :: editCustomer()");
            Customer customer = aw.getCustomerService().findByCustomerId(custId);
            TenantWalletVO tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(tenantCode);
            comsHelperDTO.setBillCycleList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.BILLFREQUENCY.getCode()));
            comsHelperDTO.setCustomerTypeList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.TITLES.getCode()));
            comsHelperDTO.setDistrictList(aw.getCpeInformationService().getAllDistricts());
            comsHelperDTO.setMandalList(aw.getCpeInformationService().getMandalsByDistrictId(Integer.parseInt(customer.getDistrict())));
            comsHelperDTO.setVillages(aw.getCpeInformationService().getVillagesByVillageId(Integer.parseInt(customer.getCityVillage()), Integer.parseInt(customer.getMandal()), Integer.parseInt(customer.getDistrict())));
            comsHelperDTO.setOfficeTypeLovList(aw.getLovsService().getLovValuesByLovName(custType));
            comsHelperDTO.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
            comsHelperDTO.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
            comsHelperDTO.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
            comsHelperDTO.setAlert(tenantWalletVO.getAlert());
            comsHelperDTO.setFlag(tenantWalletVO.getFlag());
			comsHelperDTO.setCustomer(customer);
			comsHelperDTO.setVillageList(aw.getCpeInformationService().getVillagesByDistrictAndMandal(Integer.parseInt(customer.getDistrict()), Integer.parseInt(customer.getMandal())));
            
        } catch (Exception e) {
            logger.info("ComsRestController :: editCustomer()" + e);
            e.printStackTrace();
        }
        return comsHelperDTO;
    }
	
	@RequestMapping(value = "/updateCustomerDetails", method = RequestMethod.POST)
	public ComsHelperDTO updateCustomerDetails(@RequestBody CustomerCafVO customerCafVO,
			@RequestParam(value = "pocMob1Old", required = false) String pocMob1Old,
			@RequestParam(value = "pocMob2Old", required = false) String pocMob2Old,
			@RequestParam(value = "stdCodeOld", required = false) String stdCodeOld,
			@RequestParam(value = "landLine1Old", required = false) String landLine1Old,
			@RequestParam(value = "email1Old", required = false) String email1Old,
			@RequestParam(value = "tenantCode", required = false) String tenantCode,
			@RequestParam(value = "tenantName", required = false) String tenantName,
			@RequestParam(value = "address1Old", required = false) String address1Old,
			@RequestParam(value = "address2Old", required = false) String address2Old,
			@RequestParam(value = "localityOld", required = false) String localityOld,
			@RequestParam(value = "districtOld", required = false) String districtOld,
			@RequestParam(value = "mandalOld", required = false) String mandalOld,
			@RequestParam(value = "villageOld", required = false) String villageOld,
			@RequestParam(value = "pinOld", required = false) String pinOld) throws Exception {
		logger.info("ComsRestController :: updateCustomerDetails()");
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: updateCustomerDetails()");
			StringBuilder modifieddata = new StringBuilder();
			if (!customerCafVO.getMobileNo().equalsIgnoreCase(pocMob1Old)) {
				modifieddata.append("MobileNo").append("-").append(pocMob1Old).append(",");
			}
			if (!customerCafVO.getMobileNo1().equalsIgnoreCase(pocMob2Old)) {
				modifieddata.append("MobileNo1").append("-").append(pocMob2Old).append(",");
			}
			if (!customerCafVO.getStdCode().equalsIgnoreCase(stdCodeOld)) {
				modifieddata.append("StdCode").append("-").append(stdCodeOld).append(",");
			}
			if (!customerCafVO.getLandLineNo().equalsIgnoreCase(landLine1Old)) {
				modifieddata.append("LandLineNo").append("-").append(landLine1Old).append(",");
			}
			if (!customerCafVO.getEmailId().equalsIgnoreCase(email1Old)) {
				modifieddata.append("EmailId").append("-").append(email1Old).append(",");
			}
			if (!customerCafVO.getAddress1().equalsIgnoreCase(address1Old)) {
				modifieddata.append("Address1").append("-").append(address1Old).append(",");
			}
			if (!customerCafVO.getAddress2().equalsIgnoreCase(address2Old)) {
				modifieddata.append("Address2").append("-").append(address2Old).append(",");
			}
			if (!customerCafVO.getLocality().equalsIgnoreCase(localityOld)) {
				modifieddata.append("Locality").append("-").append(localityOld).append(",");
			}
			if (!customerCafVO.getDistrict().equalsIgnoreCase(districtOld)) {
				modifieddata.append("District").append("-").append(districtOld).append(",");
			}
			if (!customerCafVO.getMandal().equalsIgnoreCase(mandalOld)) {
				modifieddata.append("Mandal").append("-").append(mandalOld).append(",");
			}
			if (!customerCafVO.getCityVillage().equalsIgnoreCase(villageOld)) {
				modifieddata.append("Village").append("-").append(villageOld).append(",");
			}
			if (!customerCafVO.getPinCode().equalsIgnoreCase(pinOld)) {
				modifieddata.append("PinCode").append("-").append(pinOld).append(",");
			}
			StringBuilder modifieddatawithoutLastComma = modifieddata.deleteCharAt(modifieddata.lastIndexOf(","));
			String modificationData = modifieddatawithoutLastComma.toString();
			aw.getCustomerService().saveCustomerDetails(modificationData, customerCafVO);
			aw.getCustomerService().updateCustomerDetails(customerCafVO);
			customerList = aw.getCustomerService().findCustomers(customerCafVO.getLmoCode(), customerCafVO.getTenantType());
			comsHelperDto.setCustomerList(customerList);
			comsHelperDto.setMessage("The Customer Updated Successfully.");
		} catch (Exception e) {
			logger.error("ComsRestController::updateCustomerDetails() " + e);
			e.printStackTrace();
		}
		return comsHelperDto;
	}

	@RequestMapping(value = "/getStbModelList", method = RequestMethod.POST)
	public List<CpeModal> getStbModelList() {
		List<CpeModal> cpeModelList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getAadharDetails() :: START");
			cpeModelList = aw.getCpeInformationService().getAllCpeModalsByCpeType(ComsEnumCodes.IPTV_TYPE.getCode());
			logger.info("ComsRestController :: getAadharDetails() :: START");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getFingerPrintDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeModelList;
	}
	
	@RequestMapping(value = "/globallyMessageToFingerPrintDetailsJob", method = RequestMethod.GET)
	public String globallyMessageToFingerPrintDetailsJob() {
		FingerPrintJson fingerPrintJson = new FingerPrintJson();
		List<String> fPlist = new ArrayList<>();
		String status = null;
		try {
			logger.info("ComsRestController :: globallyMessageToFingerPrintDetailsJob() :: START");
				fPlist = aw.getCafService().getFingerPrintDetailsFromConfig();
				fingerPrintJson.setFpBgColor(fPlist.get(0));
				fingerPrintJson.setFpChannel(fPlist.get(1));
				fingerPrintJson.setFpDuration(fPlist.get(2));
				fingerPrintJson.setFpfingerPrintType(fPlist.get(3));
				fingerPrintJson.setFpFontColor(fPlist.get(4));
				fingerPrintJson.setFpFontSize(fPlist.get(5));
				fingerPrintJson.setFpFontType(fPlist.get(6));
				fingerPrintJson.setFpPosition(fPlist.get(7));
				fingerPrintJson.setFpStatus("F");
				fingerPrintJson.setGlchecked("gl");
				status = this.getFingerPrintDetails(fingerPrintJson);
			logger.info("ComsRestController :: globallyMessageToFingerPrintDetailsJob() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController :: globallyMessageToFingerPrintDetailsJob()" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}
	
	@RequestMapping(value = "/getTTPaymentDetails", method = RequestMethod.GET)
	public List<PaymentDetailsVO> getTTPaymentDetails(@RequestParam(value = "cafNo") Long cafNo) {
		List<PaymentDetailsVO> paymentDetailsList = new ArrayList<>();
		try {
			logger.info("ComsRestController :: getTTPaymentDetails() :: START");
			paymentDetailsList = aw.getPaymentService().getTTPayment(cafNo);
			logger.info("ComsRestController :: getTTPaymentDetails() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getTTPaymentDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return paymentDetailsList;
	}
	
	@RequestMapping(value = "/getCustomerDetailsByCafNo", method = RequestMethod.GET)
	public CustomerDetailsBO getCustomerDetailsByCafNo(@RequestParam(value="cafNo") Long cafNo){
		CustomerDetailsBO customerDetails = null;
		customerDetails = aw.getCafService().getCustomerDetailsByCafNo(cafNo);
		return customerDetails;
	}
	
	/**
	 * @param nwSubscriberCode
	 * @return
	 */
	@RequestMapping(value = "/unbilledInformation", method = RequestMethod.GET)
	public CorpusBillVO getUnbilledInformation(@RequestParam(value="nwSubscriberCode") String nwSubscriberCode){
		CorpusBillVO response = new CorpusBillVO();
		try {
			response = aw.getCustomerService().getCustomerDetailsByNWSubscriberCode(nwSubscriberCode);
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerController::getCustomerInvoiceDetailsByNWSubscriberCode() " + e);
		} finally {
			
		}
		return response;
	}
	
	/**
	 * @param nwSubscriberCode
	 * @return
	 */
	@RequestMapping(value = "/getCustomerPreviousInvoiceDetailsByNWSubscriberCode", method = RequestMethod.GET)
	public CorpusPreviousBillBO getCustomerPreviousInvoiceDetailsByNWSubscriberCode(@RequestParam(value="nwSubscriberCode") String nwSubscriberCode){
		CorpusPreviousBillBO response = new CorpusPreviousBillBO();
		try {
			response = aw.getCustomerService().getCustomerBillDetailsByNWSubscriberCode(nwSubscriberCode);
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerController::getCustomerInvoiceDetailsByNWSubscriberCode() " + e);
		} finally {
			
		}
		return response;
	}
	
	@RequestMapping(value = "/getBillSummaryByBillNo", method = RequestMethod.GET)
	public BillSummaryVO getBillSummaryByBillNo(@RequestParam(value="billNo") String billNo, @RequestParam(value="customerId") String customerId, 
			@RequestParam(value="accountNo") String accountNo){
		BillSummaryVO response = new BillSummaryVO();
		try {
			response = aw.getCustomerService().getBillSummary(billNo, customerId, accountNo );
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerController::getCustomerInvoiceDetailsByNWSubscriberCode() " + e);
		} finally {
			
		}
		return response; 
	}
	
	// SearchFor-AddPackage-Details
	@RequestMapping(value = "/SearchForAddPackageDetails", method = RequestMethod.GET)
	public ComsHelperDTO SearchForAddPackageDetails(@RequestParam(value = "cafNumber", required = false) Long cafNumber, 
			@RequestParam(value = "tenantType", required = false) String tenantType, @RequestParam(value = "tenantCode", required = false) String tenantCode,
			@RequestParam(value = "addOrChangePkgFlag", required = false) String addOrChangePkgFlag) throws Exception {
		logger.info("CustomerController :: createCustomerCafDetails()");
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		CustomerCafVO customerCafVO = new CustomerCafVO();
		String regionCode = "";
		String IPTVBox = null;
		List<ProductsVO> currentProductList = new ArrayList<>();
		List<ProductsVO> baseProductList = new ArrayList<>();
		List<ProductsVO> addOnProductList = new ArrayList<>();
		List<ProductsVO> oneTimeProductList = new ArrayList<>();
		Set<String> mspCodes = new HashSet<String>();
		String lmoAgrmntMspCodes = "";
		try {
			IPTVBox = aw.getCafService().hasIPTVBoxForCaf(cafNumber);
			logger.info("addorchangeflag is:"+addOrChangePkgFlag);
			logger.info("IPTV box for cafno"+cafNumber+" is there:"+IPTVBox);
			customerCafVO = aw.getCafService().getPackageInformation(cafNumber, tenantType);
			if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode())) {
				Long pmntCustId = aw.getStoredProcedureDAO().executePaymentCustomer(customerCafVO.getCustId().longValue());
				comsHelperDto.setCustomerCode(pmntCustId.toString());
			}
			TenantWalletVO tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(tenantCode);
			regionCode = aw.getCafService().getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
			String jsonObject = getProducts(customerCafVO.getCustType(), tenantCode, regionCode, null, null);
			logger.info("products json is:"+jsonObject);
			List<ProductsVO> productsList = mapper.readValue(jsonObject, TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
			// Call to get Selected Old Services
			List<CafOldPackagesBO> cafOldPackagesList = aw.getCafService().getOldPackageInformation(cafNumber);
			for (ProductsVO products : productsList) {
				int count = 0;
				for (CafOldPackagesBO oldVO : cafOldPackagesList) {
					if (products.getProdcode().equalsIgnoreCase(oldVO.getProdCode()) && products.getAgruniqueid().equals(Long.parseLong(oldVO.getAgruniqueid())) && products.getTenantcode().equalsIgnoreCase(oldVO.getTenantCode())) {
						count++;
						if (addOrChangePkgFlag.equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
							if (products.getProdType().equalsIgnoreCase("B")) {
								currentProductList.add(products);
							}
						}
					}
				}
				if (addOrChangePkgFlag.equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
					if (products.getProdType().equalsIgnoreCase("B")) {
						if(products.getCoreSrvcCodesWithComma().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) > 0) {
							if (count == 0) {
								if (IPTVBox.equalsIgnoreCase("YES")) {
									baseProductList.add(products);
								}
							}
						} else {
							if (count == 0) {
								baseProductList.add(products);
							}
						}
						
						mspCodes.add(products.getMspCode());
					}
				} else if (addOrChangePkgFlag.equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
					logger.info("products.getServicesList().get(0).getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.HSI_Add_Pkg.getCode()) : "+products.getServicesList().get(0).getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.HSI_Add_Pkg.getCode()));
					if ((products.getProdType().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode())) && (products.getServicesList().get(0).getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.HSI_Add_Pkg.getCode()))) {
						logger.info("core service codes:"+products.getCoreSrvcCodesWithComma()+" for product code:"+products.getProdname());
							if (count == 0) {
								addOnProductList.add(products);
							}
					}
					if (products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
						if(products.getCoreSrvcCodesWithComma().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) > 0) {
							if (IPTVBox.equalsIgnoreCase("YES")) {
								oneTimeProductList.add(products);
							}
						} else {
							oneTimeProductList.add(products);
						}
					}
				}
			}
				
			StringBuilder agrMspCodes = new StringBuilder();
			for (String mspcode : mspCodes) {
				agrMspCodes.append(mspcode).append(",");
			}
			lmoAgrmntMspCodes = FileUtil.mspCodeSplitMethod(agrMspCodes.toString());
			
			List<NoOfSTBsBO> noOfSTBsList = aw.getCafService().getNoOfSTBsForCaf(cafNumber);
			logger.info("addon products count:"+addOnProductList.size()+" one time products count:"+oneTimeProductList.size());
			comsHelperDto.setNoOfSTBsList(noOfSTBsList);
			comsHelperDto.setLmoAgrmntMspCodes(lmoAgrmntMspCodes);
			comsHelperDto.setProductsList(productsList);
			comsHelperDto.setCurrentProductList(currentProductList);
			comsHelperDto.setBaseProductList(baseProductList);
			comsHelperDto.setAddOnProductList(addOnProductList);
			comsHelperDto.setOneTimeProductList(oneTimeProductList);
			comsHelperDto.setLmoWalletBalence(tenantWalletVO.getWalletAmount().toString());
			comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
			comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
			comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
			comsHelperDto.setAlert(tenantWalletVO.getAlert());
			comsHelperDto.setFlag(tenantWalletVO.getFlag());
			comsHelperDto.setCustomerCafVO(customerCafVO);

		} catch (Exception e) {
			logger.error("CustomerController::createCustomerCafDetails() " + e);
			e.printStackTrace();
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/getProdInfoByCafNo", method = RequestMethod.GET)
	public List<TerminatePkgBO> getProdInfoByCafNo(@RequestParam(value = "cafNo") Long cafNo) {
		List<TerminatePkgBO> prodList = new ArrayList<>();
		try {
			prodList =  aw.getCafService().getProdInfoByCafNo(cafNo);
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getProdInfoByCafNo :::" + e);
			e.printStackTrace();
		} finally {

		}
		return prodList;
	}
	
	@RequestMapping(value = "/terminateAddonIPTVPackage", method = RequestMethod.POST)
	public String terminateAddonIPTVPackage(@RequestBody List<TerminatePkgBO> productsList) {
		String status = "false";
		try {
			logger.info("ComsController :: getPkgDtls() :: Start");
			for(TerminatePkgBO terminatePkg : productsList) {
				status = aw.getProvisioningBusinessServiceImpl().packageTermination(terminatePkg.getProdCafNo(), terminatePkg.getProdCode(), terminatePkg.getStbCafNo(), terminatePkg.getNwSubscriberCode());
				aw.getCafServicesService().updateCafServices(terminatePkg.getProdCafNo(), terminatePkg.getProdCode(), "", "");
				logger.info("The Cafsrvcs table updated successfully");
			}
			logger.info("ComsController :: getPkgDtls() :: End");
		} catch (Exception exception) {
			logger.error("ComsController :: getPkgDtls() :: " + exception);
			exception.printStackTrace();
		} finally {
		}
		return status;
	}
			
	private String getProducts( String custType, String tenantCode, String regionCode, String prodCode, String actDate) {
		String jsonObject = "";
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		String url = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			httpEntity = this.getHttpEntity(comUserName, comPwd);
			if(Objects.nonNull(prodCode) && prodCode.length() > 0 
					&& Objects.nonNull(actDate) && actDate.length() > 0) {
				url = catURL + "pcs/getAllProductsByLmoCode?lmoCode="+tenantCode+"&custType="+custType+"&charFlags=1,2,3"+"&region="+regionCode+"&prodCode="+prodCode+"&actDate="+actDate;
			} else {
				url = catURL + "pcs/getAllProductsByLmoCode?lmoCode="+tenantCode+"&custType="+custType+"&charFlags=1,2,3"+"&region="+regionCode;
			}
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			jsonObject = response.getBody();
			
		} catch(Exception e) {
			logger.info(":::ComsRestController -- getProducts :::" +e);
			e.printStackTrace();
		} finally {
			httpEntity = null;
			response = null;
		}
		return jsonObject;
		
	}
	
	@SuppressWarnings("static-access")
	private String saveAddonHsiParameters( AddOnPackageDTO addOnPackageDTO) {
		String jsonObject = "";
		HttpEntity<AddOnPackageDTO> httpEntity = null;
		ResponseEntity<String> response = null;
		String url = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			httpEntity = this.getHttpEntity(hsiUserName, hsiPwd, addOnPackageDTO);
			url = hsiAddonURL;
			response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
			jsonObject = response.getBody();
			
		} catch(Exception e) {
			logger.info(":::ComsRestController -- getProducts :::" +e);
			e.printStackTrace();
		} finally {
			httpEntity = null;
			response = null;
		}
		return jsonObject;
	}
	
	@SuppressWarnings("static-access")
	private String saveChangeBaseHsiParameters( ChangeBasePackage changeBasePackage) {
		String jsonObject = "";
		HttpEntity<ChangeBasePackage> httpEntity = null;
		ResponseEntity<String> response = null;
		String url = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			httpEntity = this.getHttpEntity(hsiUserName, hsiPwd, changeBasePackage);
			url = hsiBaseURL;
			response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
			jsonObject = response.getBody();
			
		} catch(Exception e) {
			logger.info(":::ComsRestController -- getProducts :::" +e);
			e.printStackTrace();
		} finally {
			httpEntity = null;
			response = null;
		}
		return jsonObject;
		
	}
	
	public  HttpEntity<String> getHttpEntity(String username, String pwd) {
		HttpHeaders headers = new HttpHeaders();
		String plainCreds = "";
		byte[] plainCredsBytes = null;
		byte[] base64CredsBytes = null;
		String base64Creds = "";
		try {
			plainCreds = username + ":" + pwd;
			plainCredsBytes = plainCreds.getBytes();
			base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			base64Creds = new String(base64CredsBytes);
			headers.add("Authorization", "Basic " + base64Creds);
		} catch(Exception e) {
			logger.info(":::ComsRestController -- getHttpEntity :::" +e);
			e.printStackTrace();
		} finally {
			plainCreds = null;
			plainCredsBytes = null;
			base64CredsBytes = null;
			base64Creds = null;
		}
		return new HttpEntity<String>(headers);
	}
	
	public static <DTO> HttpEntity<DTO> getHttpEntity(String username, String pwd, DTO dto) {

		String plainCreds = username + ":" + pwd;
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		return new HttpEntity<DTO>(dto, headers);
	}
	
	@RequestMapping(value = "/viewEntCafDetails", method = RequestMethod.POST)
	public ComsHelperDTO viewEntCafDetails(@RequestBody ComsHelperDTO comsHelperDTO) {
		try {
			logger.info("ComsRestController :: viewEntCafDetails() :: START");
			comsHelperDTO = aw.getCafService().viewEntCafDetails(comsHelperDTO);
			logger.info("ComsRestController :: viewEntCafDetails() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: viewEntCafDetails" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	@RequestMapping(value = "/getCafInfo", method = RequestMethod.GET)
	public List<CafsVO> getCafInfo(@RequestParam(value = "cafNo") Long cafNo) {
		List<CafsVO> cafsVO = new ArrayList<CafsVO>();
		try {
			logger.info("ComsRestController :: getCafInfo() :: START");
			cafsVO = aw.getCafService().getCafInfo(cafNo);
			logger.info("ComsRestController :: getCafInfo() :: END");
		} catch (Exception e) {
			logger.info(":::ComsRestController -- getCafInfo :::" + e);
			e.printStackTrace();
		} finally {

		}
		return cafsVO;
	}
	
	@RequestMapping(value = "/multiActionCafDownload", method = RequestMethod.POST)
	public List<CafAndCpeChargesVO> multiActionCafDownload(@RequestBody MultiAction multiaction) {
		List<CafAndCpeChargesVO> multiDataObject= new ArrayList<>();
		try {
			logger.info("ComsRestController :: multiActionCafDownload() :: START");
			multiDataObject=aw.getCafService().getMultiActionCafList(multiaction);
			logger.info("ComsRestController :: multiActionCafDownload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload() " + e);
			e.printStackTrace();
		} finally {
		}
		return multiDataObject;
	}
	
	@RequestMapping(value = "/searchBalanceAdjustment", method = RequestMethod.GET)
	public List<BalanceAdjustmentBO> searchBalanceAdjustment(@RequestParam(value="custId") String custId, @RequestParam(value="AadharNo") String AadharNo) {
		List<BalanceAdjustmentBO> custInvData= new ArrayList<>();
		try {
			logger.info("ComsRestController :: multiActionCafDownload() :: START");
			custInvData=aw.getCafService().searchBalanceAdjustment(custId, AadharNo);
			logger.info("ComsRestController :: multiActionCafDownload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload() " + e);
			e.printStackTrace();
		} finally {
		}
		return custInvData;
	}
	
	@RequestMapping(value = "/showCafinvDetailsByCustinvno", method = RequestMethod.GET)
	public List<BalanceAdjustmentBO> showCafinvDetailsByCustinvno(@RequestParam(value="custInvno") String custInvno) {
		List<BalanceAdjustmentBO> custInvData= new ArrayList<>();
		try {
			logger.info("ComsRestController :: multiActionCafDownload() :: START");
			custInvData=aw.getCafService().showCafinvDetailsByCustinvno(custInvno);
			logger.info("ComsRestController :: multiActionCafDownload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload() " + e);
			e.printStackTrace();
		} finally {
		}
		return custInvData;
	}
	
	@RequestMapping(value = "/checkedCafInvCafNos", method = RequestMethod.GET)
	public List<BalAdjCafinvDtslBO> checkedCafInvCafNos(@RequestParam(value="acctCafNo") String acctCafNo, @RequestParam(value="billNo") String billNo) {
		List<BalAdjCafinvDtslBO> custInvData= new ArrayList<>();
		try {
			logger.info("ComsRestController :: multiActionCafDownload() :: START");
			custInvData=aw.getCafService().checkedCafInvCafNos(acctCafNo, billNo);
			logger.info("ComsRestController :: multiActionCafDownload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload() " + e);
			e.printStackTrace();
		} finally {
		}
		return custInvData;
	}
	
	@RequestMapping(value = "/saveBalanceAdjustment", method = RequestMethod.POST)
	public String saveBalanceAdjustment(@RequestBody List<BalAdjCafinvDtslBO> balAdjCafinvDtslBO, @RequestParam(value = "tenantCode") String tenantCodeLMO) {
		String result = null;
		try {
			logger.info("ComsRestController :: multiActionCafDownload() :: START");
			result = aw.getCafService().saveBalanceAdjustment(balAdjCafinvDtslBO, tenantCodeLMO);
			logger.info("ComsRestController :: multiActionCafDownload() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload() " + e);
			e.printStackTrace();
		} finally {
		}
		return result;
	}
	
	@RequestMapping(value = "/changeProfileForCPE", method = RequestMethod.GET)
	public String changeProfileForCPE(@RequestParam("cpeSrlNo") String cpeSrlNo,
			@RequestParam("newProfileId") String newProfileId, @RequestParam("tenantCode") String tenantCode) {
		String result = null;
		try {
			logger.info("ComsRestController :: changeProfileForCPE() :: START");
			result = aw.getCafService().changeProfileForCPE(cpeSrlNo, newProfileId, tenantCode);
			logger.info("ComsRestController :: changeProfileForCPE() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::changeProfileForCPE() " + e);
			e.printStackTrace();
		} finally {
		}
		return result;
	}
	
	@RequestMapping(value = "/getTotalAAAUsagePerMonthPerCust", method = RequestMethod.GET)
    public List<HSICummSummaryMonthlyCustViewVO> getTotalAAAUsagePerMonthPerCust(Model uiModel,
            @RequestParam(value="month", required = false) String month,
            @RequestParam(value="year", required = false) String year ,
            @RequestParam(value="cafNo", required = false) String cafNo,
            @RequestParam(value="day", required = false) String day)
    {
        List<HSICummSummaryMonthlyCustViewVO> list = new ArrayList<>();
        try{
             logger.info("ReportsController :: gatTotalAAAUsagePerMonthPerCust() :: START");
             list = aw.getCafService().getTotalAAAUsagePerMonthCustDayWise(month,year,cafNo);
             logger.info("ReportsController :: gatTotalAAAUsagePerMonthPerCust() :: END");
        }catch(Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
        }
      
     return list;

    }
	
	@RequestMapping(value = "/SearchForTenantDetails", method = RequestMethod.GET)
	public ComsHelperDTO SearchForTenantDetails(@RequestParam(value = "cafNumber", required = false) Long cafNumber) throws Exception {
		logger.info("ComsRestController :: SearchForTenantDetails()");
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		TenantVO tenantVO=new TenantVO();
		try {
			tenantVO = aw.getTenantService().findTenantDtlsByCafno(cafNumber);
			comsHelperDto.setTenantVO(tenantVO);
		} catch (Exception e) {
			logger.error("ComsRestController::SearchForTenantDetails() " + e);
			e.printStackTrace();
		}
		return comsHelperDto;
	}
	
	@RequestMapping(value = "/getTotalAAAUsagePerMonth", method = RequestMethod.GET)
    public String getTotalAAAUsagePerMonth()
    {
        List<HSICummSummaryMonthly> list = new ArrayList<>();
        String summary1="";
        try{
        	Calendar cal = Calendar.getInstance();
    		int month = cal.get(Calendar.MONTH) + 1;
    		int year = cal.get(Calendar.YEAR);
    		String currmonth="";
    		if(month < 10)
    		{
    			currmonth=String.format("%02d", month);
    		}else{
    			currmonth=String.valueOf(month);
    		}
             logger.info("ReportsController :: gatTotalAAAUsagePerMonthPerCust() :: START");
             list = aw.getCafService().getTotalAAAUsagePerMonthDayWise(currmonth,String.valueOf(year));
             Gson json=new Gson();
             summary1=json.toJson(list);
			 logger.info("json.toJson(summary) :::: " + json.toJson(list));
             logger.info("ReportsController :: gatTotalAAAUsagePerMonthPerCust() :: END");
        }catch(Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
        }
     return summary1;

    }
	
	@RequestMapping(value = "/getPreviousMonthHSIData", method = RequestMethod.GET)
	public String getPreviousMonthHSIData(){
		HSICummSummaryPreviousMonthVO summary=new HSICummSummaryPreviousMonthVO();
		String summary1="";
		try {
			logger.info("fetching previous month HSI data");
			summary=aw.getCafService().getPreviousMonthHSIData();
			Gson json=new Gson();
			summary1=json.toJson(summary);
			logger.info("json.toJson(summary) :::: " + json.toJson(summary));
		} catch (Exception e) {
			logger.info("Exception:"+e.getMessage());
			e.printStackTrace();
		}
		return summary1;
	}
	
	@RequestMapping(value = "/getPreviousMonthHSISUMData", method = RequestMethod.GET)
	public String getPreviousMonthHSISUMData(){
		HSICummSumPMonthVO summary=new HSICummSumPMonthVO();
		String summary1="";
		try {
			logger.info("fetching previous month HSI  SUM data");
			summary=aw.getCafService().getPreviousMonthHSISUMData();
			Gson json=new Gson();
			summary1=json.toJson(summary);
			logger.info("json.toJson(summary)SUM :::: " + json.toJson(summary));
		} catch (Exception e) {
			logger.info("Exception:"+e.getMessage());
			e.printStackTrace();
		}
		return summary1;
	}
	
	
	@RequestMapping(value = "/getPreviousMonthInvAmountDtls", method = RequestMethod.GET)
	public String getPreviousInvAmountDtls(){
		InvoiceAmountDtls summary=new InvoiceAmountDtls();
		String summary1="";
		try {
			logger.info("fetching previous month Total invoice amount");
			summary=aw.getCafService().getPreviousMonthInvAmount();
			Gson json=new Gson();
			summary1=json.toJson(summary);
			logger.info("json.toJson(summary) :::: " + json.toJson(summary));
		} catch (Exception e) {
			logger.info("Exception:"+e.getMessage());
			e.printStackTrace();
		}
		return summary1;
	}
	
	@RequestMapping(value = "/createCssPaymentDetails", method = RequestMethod.POST)
	public ComsHelperDTO createCssPaymentDetails(@RequestBody PaymentVO paymentVO) {
		//added by chaitanya_xyz
		int cardnum=1;
		if(paymentVO.getOltSrlNo()!=null && !paymentVO.getOltSrlNo().equals("")){
			cardnum=aw.getPaymentService().getCardDetails(paymentVO.getOltSrlNo());
			aw.getPaymentService().updateCafCard(paymentVO.getCafNo(), cardnum);
			if(cardnum==1)paymentVO.setOltPortNo(paymentVO.getOltPortNo());
		}else{
			cardnum=aw.getPaymentService().getCardDetailsByCaf(paymentVO.getCafNo().toString());
		}
		ComsHelperDTO comsHelperDto = new ComsHelperDTO();
		List<ProductsVO> changePkgList = new ArrayList<>();
		List<HsiBO> hsiList = new ArrayList<>();
		String message = "";
		String jsonObject = "";
		String hsiStatus = "HSI";
		String srvcCode = "";
		String regionCode = "";
		Gson gson = new Gson();
		String paymentJson = "";
		String onuNumber = "";
		try {
			logger.info("ComsRestController :: createCssPaymentDetails() :: START");
			paymentJson = gson.toJson(paymentVO);
			logger.info("The Save Payment Json is :" + paymentJson);
			if (paymentVO.getPayment() != null) {
				aw.getPaymentService().saveMonthlyPayment(paymentVO, paymentVO.getLoginId());
				comsHelperDto.setFlag("The Monthly Payment Created Successfully.Your Payment Will Be Updated after 15 Minutes");
				SmsDTO smsDto = new SmsDTO();
				smsDto.setMobileNo(paymentVO.getMobileNo());
				String msg = "Dear Customer, We have received Cash Payment of Rs "+paymentVO.getPaidAmount()+" for your APSFL Customer ID: "+paymentVO.getCustId()+". Thank You";
				smsDto.setMsg(msg);
				smsApi.sendSMS(smsDto);
				logger.info("Message sent Successfully.");
				
			} else if(paymentVO.getCustomerCafVO().getChangePkgFlag() != null && (paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) 
						|| paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode()))) {
				if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
					ChangePkgBO changePkgBO = paymentVO.getCustomerCafVO().getChangePkgBO();
					regionCode = aw.getCafService().getRegionCodeByPinCode(paymentVO.getCustomerCafVO().getDistrict(), paymentVO.getCustomerCafVO().getMandal(), paymentVO.getCustomerCafVO().getCity());
					regionCode = regionCode.isEmpty() ? ComsEnumCodes.REGION_TYPE.getCode() : regionCode;
					jsonObject = getProducts(paymentVO.getCustomerCafVO().getCustType(), paymentVO.getCustomerCafVO().getLmoCode(), regionCode, changePkgBO.getProdCode(), changePkgBO.getActDate());
					changePkgList = mapper.readValue(jsonObject,TypeFactory.defaultInstance().constructCollectionType(List.class, ProductsVO.class));
				}
				if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
					if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
						AddOnPackageDTO addOnPackageDTO = new AddOnPackageDTO();
						for(ProductsVO products : paymentVO.getCustomerCafVO().getProducts()) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						hsiList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								addOnPackageDTO.setDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								addOnPackageDTO.setUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						addOnPackageDTO.setMonth(DateUtill.getCurrentMonth().toString());
						addOnPackageDTO.setYear(DateUtill.getYear().toString());
						addOnPackageDTO.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo().toString());
						hsiStatus = saveAddonHsiParameters(addOnPackageDTO);
					} else if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
						ChangeBasePackage changeBasePackage = new ChangeBasePackage();
						for(ProductsVO products : changePkgList) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						hsiList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								changeBasePackage.setOldDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								changeBasePackage.setOldUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						
						for(ProductsVO products : paymentVO.getCustomerCafVO().getProducts()) {
							for(AddtionalServicesVO services : products.getServicesList()) {
								if(services.getCoreServiceCode().indexOf(ComsEnumCodes.CORE_SERVICE_HSI.getCode()) != -1) {
									srvcCode = services.getServiceCode();
								}
							}
						}
						List<HsiBO> hsiNewList = aw.getCafService().getHSIParameters(srvcCode);
						for(HsiBO hsiBO : hsiNewList) {
							if(hsiBO.getPrmCode().equalsIgnoreCase("DOWNLOADLIMIT")) {
								changeBasePackage.setNewDownSize(new BigDecimal(hsiBO.getPrmValue()));
							} else if(hsiBO.getPrmCode().equalsIgnoreCase("UPLOADLIMIT")) {
								changeBasePackage.setNewUplSize(new BigDecimal(hsiBO.getPrmValue()));
							}
						}
						changeBasePackage.setMonth(DateUtill.getCurrentMonth().toString());
						changeBasePackage.setYear(DateUtill.getYear().toString());
						changeBasePackage.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo().toString());
						hsiStatus = saveChangeBaseHsiParameters(changeBasePackage);
					}
				} 
				if(hsiStatus.equalsIgnoreCase("Success") || hsiStatus.equalsIgnoreCase("HSI")) {
					message="Success";
					String phoneNo = aw.getPaymentService().saveAddpackage(paymentVO, changePkgList, message);
					message = !phoneNo.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "Your Order Created Successfully.";
					comsHelperDto.setFlag(message);
					
					if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
						message = aw.getProvisioningBusinessServiceImpl().basePackageChange(paymentVO.getCustomerCafVO().getProdCafNo(), paymentVO.getCustomerCafVO().getProdCode());
						if(message.equalsIgnoreCase("success")) {
							aw.getCafServicesService().updateCafServices(paymentVO.getCustomerCafVO().getCafNo().toString(), paymentVO.getCustomerCafVO().getChangePkgBO().getProdCode(), paymentVO.getCustomerCafVO().getLoginId(), paymentVO.getCustomerCafVO().getIpAddress());
							logger.info("The Cafsrvcs table updated successfully");
						}
					}
					
					if(paymentVO.getPaidAmount() != null && paymentVO.getPaidAmount() > 0) {
						String result = aw.getPaymentService().processCSSWOPayments(paymentVO);
						if(result.equalsIgnoreCase("0")) {
							logger.info("WorkOrder Process Payments and Provisioning Request Jsons Created Successfully");
						}
					}
					
					else {
						if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
							aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(paymentVO.getCustomerCafVO().getProdCafNo(), 1);
							logger.info("Provisioning Request Jsons Created Successfully");
						}
					}
				} else {
					comsHelperDto.setFlag(hsiStatus);
				}
			} else {
				if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) < 0 ) {
					List<IPTVBoxVO> iptvBoxVOList = new ArrayList<>();
					CustomerCafVO customerCafVO = paymentVO.getCustomerCafVO();
					customerCafVO.setIptvBoxList(iptvBoxVOList);
					paymentVO.setCustomerCafVO(customerCafVO);
				}
				paymentJson = gson.toJson(paymentVO);
				logger.info("The Save Payment Json is :" + paymentJson);
				if (paymentVO.getFeasibility().equalsIgnoreCase("N")) {
					aw.getCustomerService().updateCustomer(paymentVO, paymentVO.getLoginId(),cardnum);
					comsHelperDto.setFlag("Feasibility Status has been Updated Successfully.");
				} else if (paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
					onuNumber = aw.getStoredProcedureDAO().executePullOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), cardnum, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()));
					if (FileUtil.NumberCheck(onuNumber).equalsIgnoreCase("true")) {
						/*aw.getCafAccountService().saveCafAccount(paymentVO.getCustomerCafVO(), paymentVO.getLoginId());
						logger.info("The cafAccount Details Created Successfully");*/
						
						if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
							String phoneNo = aw.getPaymentService().saveEnterpriseCustomerPayment(paymentVO, paymentVO.getLoginId(), onuNumber,cardnum);
							message = !phoneNo.isEmpty() ? "Your Order Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "Your Order Created Successfully.";
							comsHelperDto.setFlag(message);
							
							aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(paymentVO.getCafNo().toString(), 1);
							logger.info("Provisioning Request Jsons Created Successfully");
						} else {
							String phoneNo = aw.getPaymentService().createPayment(paymentVO, paymentVO.getLoginId(), onuNumber,cardnum);
							message = !phoneNo.isEmpty() ? "The Payment Created Successfully. Your Allocated Telephone No(s) is " + phoneNo : "The Payment Created Successfully.";
							comsHelperDto.setFlag(message);
							
							String result = aw.getPaymentService().processWOPayments(paymentVO);
							if(result.equalsIgnoreCase("0")) {
								logger.info("WorkOrder Process Payments and Provisioning Request Jsons Created Successfully");
							}
						}
					} else {
						comsHelperDto.setFlag(onuNumber);
					}
				}
			}
			logger.info("ComsRestController :: createPaymentDetails() :: END");
		} catch (Exception e) {
			if(!onuNumber.isEmpty()) {
				String exception = aw.getStoredProcedureDAO().executePushOnuidStoredProcedure(paymentVO.getCustomerCafVO().getOltId(), 1, Integer.parseInt(paymentVO.getCustomerCafVO().getOltPortId()), Integer.parseInt(onuNumber));
				if(exception.equalsIgnoreCase("0"))
					logger.info("ONU Id RoleBacked Successfully.");
				logger.error("ComsRestController::createPaymentDetails() " + e);
			}
			e.printStackTrace();
			comsHelperDto.setFlag(e.getMessage() + " so we can't process Payment right now");
		} finally {
			
		}
		return comsHelperDto;
	}

// change code 10/1/2018
	
	
	@RequestMapping(value = "/multiActionCafDownload1", method = RequestMethod.POST)
	public List<CafAndCpeChargesVO> multiActionCafDownload1(@RequestBody MultiAction multiaction) {
		List<CafAndCpeChargesVO> multiDataObject= new ArrayList<>();
		try {
			logger.info("ComsRestController :: multiActionCafDownload1() :: START");
			multiDataObject=aw.getCafService().getMultiActionCafList1(multiaction);
			logger.info("ComsRestController :: multiActionCafDownload1() :: END");
		} catch (Exception e) {
			logger.error("ComsRestController::multiActionCafDownload1() " + e);
			e.printStackTrace();
		} finally {
		}
		return multiDataObject;
	}
	
	@RequestMapping(value = "/viewcustomersPage1", method = RequestMethod.POST)
	public ComsHelperDTO viewcustomersPage1(@RequestBody ComsHelperDTO comsHelperDTO) {

		try {
			logger.info("ComsRestController :: LMOTotalAmount() :: START");
			comsHelperDTO = aw.getCafService().findParticularCustomer1(comsHelperDTO);
			logger.info("ComsRestController :: LMOTotalAmount() :: END");

		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: LMOTotalAmount" + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
	
	// added by chaitanya_xyz
	@RequestMapping(value = "/getDayWiseCafCountDetails", method = RequestMethod.GET)
	public CafCountDetailsdaywiseDTO getCafCountDetailsdaywise() {
		return aw.getCafAccountService().getCafCountDetails("","");
	}
	
	// added by chaitanya_xyz
	@RequestMapping(value = "/getDistrictWiseCafCountDetails", method = RequestMethod.GET)
	public List<CafCountDetailsdistrictwiseDTO> getCafCountDetailsdistrictwise() {
			return aw.getCafAccountService().getCountdetailsdistrictwise();
	}
	
	// added by chaitanya_xyz
		@RequestMapping(value = "/getCustomerWiseCafCountDetails", method = RequestMethod.GET,produces = "application/json")
		public List<CountdetailsCustomerwiseDTO> getCafCountDetailsCustomerwise() {
			List<CountdetailsCustomerwiseDTO> list;
			list= aw.getCafAccountService().getCountdetailsCustomerwise();
			return list;
		}
		@RequestMapping(value = "/searchBulkMonthlyPaymentCafDetails1", method = RequestMethod.POST,produces = "application/json")
		public ComsmobilehelperDTO searchBulkMonthlyPaymentCafDetails1(@RequestBody MonthlyPayment monthlyPayment)
		{
			ComsmobilehelperDTO cmd=new ComsmobilehelperDTO();
			BulkCustomerDTO[] monthlyPaymentList;
			TenantWalletVO tenantWalletVO = new TenantWalletVO();
			try {
				logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: START");
				int offset=(monthlyPayment.getOffset()-1)*25;
				monthlyPaymentList = aw.getCafService().getMonthlyCafDetails2(monthlyPayment.getFrom(), monthlyPayment.getTenantCode(),null);
				
				
				//if 
				int maxsize=offset+25;
				if(offset>monthlyPaymentList.length) {
					return cmd;
					/*maxsize=monthlyPaymentList.length;
					offset=maxsize-25;
					if(offset<0) offset=0;*/
				}
				
				if(monthlyPaymentList.length<maxsize) maxsize=monthlyPaymentList.length;
				//monthlyPaymentList=monthlyPaymentList.subList(offset,maxsize);
				ArrayList<BulkCustomerDTO> temp=new ArrayList<BulkCustomerDTO>(Arrays.asList(monthlyPaymentList));
				ArrayList<BulkCustomerDTO> temp1=new ArrayList<BulkCustomerDTO>(temp.subList(offset, maxsize));
				/*monthlyPaymentList=Arrays.copyOfRange(monthlyPaymentList,offset,maxsize);*/
				monthlyPaymentList=temp1.toArray(new BulkCustomerDTO[temp1.size()]);
				cmd.setCustomerslist(monthlyPaymentList);
				tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(monthlyPayment.getTenantCode());
				cmd.setLmoWalletBalence(tenantWalletVO.getActualUserAmount().toString());
				cmd.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
				//cmd.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
				//cmd.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
				logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: END");
			} catch (Exception e) {
				logger.error("ComsRestController::searchMonthlyPaymentCafDetails() " + e);
				e.printStackTrace();
			} finally {
				monthlyPaymentList = null;
				tenantWalletVO = null;
			}
			return cmd;
			
		}
		//added by chaitanya_xyz
		@RequestMapping(value = "/searchBulkMonthlyPaymentCafDetails", method = RequestMethod.POST)
		public ComsHelperDTO searchBulkMonthlyPaymentCafDetails(@RequestBody MonthlyPayment monthlyPayment,HttpServletRequest req) {
			BulkCustomerDTO[] monthlyPaymentList;
			ComsHelperDTO comsHelperDto = new ComsHelperDTO();
			TenantWalletVO tenantWalletVO = new TenantWalletVO();
			try {
				logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: START");
				int offset=(monthlyPayment.getOffset()-1)*25;
				monthlyPaymentList = aw.getCafService().getMonthlyCafDetails2(monthlyPayment.getFrom(), monthlyPayment.getTenantCode(),null);
				int maxsize=offset+25;
				if(maxsize>monthlyPaymentList.length) {
					maxsize=monthlyPaymentList.length;
					offset=maxsize-25;
				}
				//monthlyPaymentList=monthlyPaymentList.subList(offset,monthlyPaymentList.length);
				monthlyPaymentList=Arrays.copyOfRange(monthlyPaymentList, 0,monthlyPaymentList.length);
				comsHelperDto.setMonthlyPayment(monthlyPayment);
				comsHelperDto.setClist(monthlyPaymentList);
				comsHelperDto.setLmoName(monthlyPayment.getTenantName());
				comsHelperDto.setLmoCode(monthlyPayment.getTenantCode());
				tenantWalletVO = aw.getTenantWalletService().getTenantWalletDetails(monthlyPayment.getTenantCode());
				comsHelperDto.setLmoWalletBalence(tenantWalletVO.getActualUserAmount().toString());
				comsHelperDto.setCreditLimit(tenantWalletVO.getCreditAmount().toString());
				comsHelperDto.setPaymentList(aw.getLovsService().getLovValuesByLovName(ComsEnumCodes.PAYMENT_MODES.getCode()));
				comsHelperDto.setActualUserAmount(tenantWalletVO.getActualUserAmount().toString());
				comsHelperDto.setAlert(tenantWalletVO.getAlert());
				comsHelperDto.setFlag(tenantWalletVO.getFlag());
				comsHelperDto.setUsedAmount(Float.toString(tenantWalletVO.getUsedAmount()));
				logger.info("ComsRestController :: searchMonthlyPaymentCafDetails() :: END");
			} catch (Exception e) {
				logger.error("ComsRestController::searchMonthlyPaymentCafDetails() " + e);
				e.printStackTrace();
			} finally {
				monthlyPaymentList = null;
				tenantWalletVO = null;
			}
			return comsHelperDto;
		}
		
		
		/*
	 * //added by chaitanya
	 * 
	 * @RequestMapping(value = "/createBulkPaymentDetails", method =
	 * RequestMethod.POST) public ComsHelperDTO
	 * createBulkPaymentDetails(@RequestBody PaymentVO paymentVO) {
	 * ComsHelperDTO comsHelperDto = new ComsHelperDTO(); List<ProductsVO>
	 * changePkgList = new ArrayList<>(); List<HsiBO> hsiList = new
	 * ArrayList<>(); String message = ""; String jsonObject = ""; String
	 * hsiStatus = "HSI"; String srvcCode = ""; String regionCode = ""; Gson
	 * gson = new Gson(); String paymentJson = ""; String onuNumber = ""; //
	 * added by chaitanya_xyz int cardnum=1; for(Object
	 * i:paymentVO.getCafList()) { try {
	 * aw.getPaymentService().saveMonthlyPayment(paymentVO,
	 * paymentVO.getLmoCode()); } catch (Exception e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } } comsHelperDto.setFlag("hello");
	 * return comsHelperDto; }
	 * 
	 */
		
		// added by sagar_xyz
		@RequestMapping(value = "/getIndividualCafCountDetails", method = RequestMethod.GET)
		public CafCountDetailsdaywiseDTO getIndividualCafCountDetails() {
				return aw.getCafAccountService().getCafCountDetails("INDIVIDUAL","INDIVIDUAL");
		}
		
		@RequestMapping(value = "/getEntGovtCafCountDetails", method = RequestMethod.GET)
		public CafCountDetailsdaywiseDTO getEntGovtCafCountDetails() {
				return aw.getCafAccountService().getCafCountDetails("ENTERPRISE","GOVT");
		}
		
		@RequestMapping(value = "/getEntPvtCafCountDetails", method = RequestMethod.GET)
		public CafCountDetailsdaywiseDTO getEntPvtCafCountDetails() {
				return aw.getCafAccountService().getCafCountDetails("ENTERPRISE","PRIVATE");
		}
		
		
		@RequestMapping(value = "/getINDDetails", method = RequestMethod.GET)
		public List<Details> getINDENTDetails() {
			   List<Details> details=aw.getCafAccountService().getDistrictINDENTDurationDetails("INDIVIDUAL","INDIVIDUAL");
				return details;
		}
		@RequestMapping(value = "/getENTGOVTDetails", method = RequestMethod.GET)
		public List<Details> getENTGOVTDetails() {
			   List<Details> details=aw.getCafAccountService().getDistrictINDENTDurationDetails("ENTERPRISE","GOVT");
				return details;
		}
		@RequestMapping(value = "/getENTPRIVATEDetails", method = RequestMethod.GET)
		public List<Details> getENTPRIVATEDetails() {
			   List<Details> details=aw.getCafAccountService().getDistrictINDENTDurationDetails("ENTERPRISE","PRIVATE");
				return details;
		}
		
		@RequestMapping(value = "/getLastTenDaysCountDetails", method = RequestMethod.GET)
		public String getLastTenDaysCountDetails() {
			    ObjectMapper mapperObj = new ObjectMapper();
			    String jsonResp="";
				Object[] count=aw.getCafAccountService().getLastTenDaysCountDetails();
				Map<String, Object> responseObj = new HashMap<String, Object>();
				final Calendar cal = Calendar.getInstance();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				responseObj.put(dateFormat.format(cal.getTime()), count[0].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[1].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[2].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[3].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[4].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[5].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[6].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[7].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[8].toString());
			    cal.add(Calendar.DATE, -1);
			    responseObj.put(dateFormat.format(cal.getTime()), count[9].toString());
			    
			    try {
		             jsonResp= mapperObj.writeValueAsString(responseObj);
		      
		        } catch (IOException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
				return jsonResp;
		}
		
	
	
}

