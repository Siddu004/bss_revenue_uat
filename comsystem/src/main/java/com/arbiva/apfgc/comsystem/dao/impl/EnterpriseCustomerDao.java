/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;

/**
 * @author Lakshman
 *
 */
@Repository
public class EnterpriseCustomerDao {
	
	private static final Logger LOGGER = Logger.getLogger(EnterpriseCustomerDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public void deleteEnterpriseCustomers(Customer enterpriseCustomer) {
		getEntityManager().remove(enterpriseCustomer);
		getEntityManager().flush();
	}

	public Customer findByEnterpriseCustomerId(Long custId) {
		Customer enterpriseCustomer = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE custid=:custId ");
			try {
				LOGGER.info("START::findByEnterpriseCustomerId()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("custId", custId);
				enterpriseCustomer = query.getSingleResult();
				LOGGER.info("END::findByEnterpriseCustomerId()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByEnterpriseCustomerId() " + e);
			} finally{
				builder = null;
			}
		  return enterpriseCustomer;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllEnterpriseCustomers(String tenantCode, String tenantType) {
		List<Object[]> enterpriseCustomerList = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String whereClause = "";
		if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode())) {
			whereClause = "IFNULL(parentcustcode, '') = '' and custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ";
		} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
			whereClause = " lmocode= '"+tenantCode+"' and IFNULL(parentcustcode, '') = '' and custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ";
		} 
		try {
			LOGGER.info("START::findAllEnterpriseCustomers()");
			builder.append("select custname, custcode, enttypelov, officetypelov, pocname, stdcode, landline1, billfreqlov, email1, dateofinc, custid, regncode, custtypelov from customermst where "+whereClause+" ");
			query = getEntityManager().createNativeQuery(builder.toString());
			enterpriseCustomerList = query.getResultList();
			LOGGER.info("END::findAllEnterpriseCustomers()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllEnterpriseCustomers() " + e);
		} finally{
			builder = null;
			query = null;
		}
	  return enterpriseCustomerList;
	}
	
	public List<Customer> findAllParentAndChildEnterpriseCustomers(String tenantCode) {
		List<Customer> enterpriseCustomerList = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE lmocode=:tenantCode ");
			try {
				LOGGER.info("START::findAllEnterpriseCustomers()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("tenantCode", tenantCode);
				enterpriseCustomerList = query.getResultList();
				LOGGER.info("END::findAllEnterpriseCustomers()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findAllEnterpriseCustomers() " + e);
			} finally{
				builder = null;
			}
		  return enterpriseCustomerList;
	}


	public Customer saveEnterpriseCustomer(Customer enterpriseCustomer) {
		return getEntityManager().merge(enterpriseCustomer);
	}
	
	public String findByCustomerCode(String custCode) {
		List<Customer> enterpriseCustomerList = new ArrayList<Customer>();
		String status = "false";
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE regncode = :custCode ");
			try {
				LOGGER.info("START::findByCustomerCode()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("custCode", custCode);
				enterpriseCustomerList = query.getResultList();
				status = enterpriseCustomerList.size() > 0 ? "true" : "false";
				LOGGER.info("END::findByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByCustomerCode() " + e);
			} finally{
				builder = null;
				enterpriseCustomerList = null;
			}
		  return status;
	}

	public List<Customer> findByEntParentCustCodeAndLmocode(String customerCode, String tenantCode) {
		List<Customer> enterpriseCustomerList = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE parentcustcode = :customerCode and lmocode=:tenantCode");
			try {
				LOGGER.info("START::findByEntParentCustCodeAndLmocode()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				query.setParameter("tenantCode", tenantCode);
				query.setParameter("customerCode", customerCode);
				enterpriseCustomerList = query.getResultList();
				LOGGER.info("END::findByEntParentCustCodeAndLmocode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByEntParentCustCodeAndLmocode() " + e);
			} finally{
				builder = null;
			}
		  return enterpriseCustomerList;
	}

	public List<Customer> findAllEntCustomers(String tenantCode) {
		List<Customer> enterpriseCustomerList = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Customer.class.getSimpleName()).append(" WHERE lmocode = '"+tenantCode+"' and custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ");
			try {
				LOGGER.info("START::findAllEntCustomers()");
				TypedQuery<Customer> query = getEntityManager().createQuery(builder.toString(), Customer.class);
				enterpriseCustomerList = query.getResultList();
				LOGGER.info("END::findAllEntCustomers()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findAllEntCustomers() " + e);
			} finally{
				builder = null;
			}
		  return enterpriseCustomerList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findEntCustomers(String tenantCode, String tenantType, PageObject pageObject) {
		List<Object[]> enterpriseCustomerList = null;
		StringBuilder builder = new StringBuilder();
		long count = 0l;
		Query countQuery = null;
		Query query = getEntityManager() .createNativeQuery("select tenanttypelov from tenants where tenantcode = '"+tenantCode+"'");
		String whereClause = "";
		if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode())) {
			whereClause = "IFNULL(parentcustcode, '') = '' and custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ";
		} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
			whereClause = " lmocode= '"+tenantCode+"' and IFNULL(parentcustcode, '') = '' and custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ";
		} 
		try {
			LOGGER.info("START::findAllEnterpriseCustomers()");
			builder.append("select custname, custcode, enttypelov, officetypelov, pocname, stdcode, landline1, billfreqlov, email1, dateofinc, custid, regncode, custtypelov,status from customermst where "+whereClause+" ");
			
			String like = "AND (custname LIKE '%" + pageObject.getSearchParameter() + "%' OR regncode LIKE '%" + pageObject.getSearchParameter() + "%'  OR enttypelov LIKE '%" + pageObject.getSearchParameter() + "%'  OR officetypelov LIKE '%" + pageObject.getSearchParameter() + "%' OR enttypelov LIKE '%" + pageObject.getSearchParameter() + "%'  OR officetypelov LIKE '%" + pageObject.getSearchParameter() + "%' OR pocname LIKE '%" + pageObject.getSearchParameter() + "%'  OR stdcode LIKE '%" + pageObject.getSearchParameter() + "%'  OR landline1 LIKE '%" + pageObject.getSearchParameter() + "%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString());
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));

			query = getEntityManager().createNativeQuery(builder.toString());
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			enterpriseCustomerList = query.getResultList();
			
			LOGGER.info("END::findEntCustomers()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findEntCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return enterpriseCustomerList;
	}

}
