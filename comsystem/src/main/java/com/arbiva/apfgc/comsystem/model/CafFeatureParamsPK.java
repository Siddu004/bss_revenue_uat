/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lakshman
 *
 */
public class CafFeatureParamsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long cafNo;
	
	private String srvcCode;
	
	private String featureCode;
	
	private String prmCode;
	
	private Date effectivefrom;

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getPrmCode() {
		return prmCode;
	}

	public void setPrmCode(String prmCode) {
		this.prmCode = prmCode;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cafNo == null) ? 0 : cafNo.hashCode());
		result = prime * result + ((effectivefrom == null) ? 0 : effectivefrom.hashCode());
		result = prime * result + ((featureCode == null) ? 0 : featureCode.hashCode());
		result = prime * result + ((prmCode == null) ? 0 : prmCode.hashCode());
		result = prime * result + ((srvcCode == null) ? 0 : srvcCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafFeatureParamsPK other = (CafFeatureParamsPK) obj;
		if (cafNo == null) {
			if (other.cafNo != null)
				return false;
		} else if (!cafNo.equals(other.cafNo))
			return false;
		if (effectivefrom == null) {
			if (other.effectivefrom != null)
				return false;
		} else if (!effectivefrom.equals(other.effectivefrom))
			return false;
		if (featureCode == null) {
			if (other.featureCode != null)
				return false;
		} else if (!featureCode.equals(other.featureCode))
			return false;
		if (prmCode == null) {
			if (other.prmCode != null)
				return false;
		} else if (!prmCode.equals(other.prmCode))
			return false;
		if (srvcCode == null) {
			if (other.srvcCode != null)
				return false;
		} else if (!srvcCode.equals(other.srvcCode))
			return false;
		return true;
	}
	
}
