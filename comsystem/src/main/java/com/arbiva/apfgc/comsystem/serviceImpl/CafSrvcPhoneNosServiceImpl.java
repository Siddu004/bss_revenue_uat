/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafSrvcPhoneNosDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafSrvcPhoneNosService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.FileUtil;

/**
 * @author Lakshman
 *
 */
@Service
public class CafSrvcPhoneNosServiceImpl implements CafSrvcPhoneNosService {
	
	private static final Logger logger = Logger.getLogger(CafSrvcPhoneNosServiceImpl.class);

	@Autowired
	CafSrvcPhoneNosDao cafSrvcPhoneNosDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CafService cafService;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Override
	@Transactional
	public CafSrvcPhoneNos saveCafSrvcPhoneNos(Long cafNo, String loginId, Integer district, Integer mandal, Integer village, String ipAddress, String custType, String noOfTPConn, Long addPkgCafNo, String flag ) {
		CafSrvcPhoneNos cafSrvcPhoneNos = new CafSrvcPhoneNos();
		String stdCode = "";
		String  phoneNo = "";
		String  telePhoneNos = "";
		String  telePhonePwds = "";
		String  status = "";
		try {
			int noOfConn = Integer.parseInt(noOfTPConn);
			for(int conn = 0; conn < noOfConn; conn++) {
				Random RANDOM = new SecureRandom();
				int PASSWORD_LENGTH = 8;
				String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789@";
				String pwd = "";
				for (int i = 0; i < PASSWORD_LENGTH; i++) {
					int index = (int) (RANDOM.nextDouble() * letters.length());
					pwd += letters.substring(index, index + 1);
				}
				stdCode = cafService.getStdCodeByVillage(district, mandal, village);
				phoneNo = storedProcedureDAO.getPhoneNoStoredProcedure(custType, stdCode);
				String mobileNo = phoneNo.replace("-", "");
				status = FileUtil.NumberCheck(mobileNo);
				if(!status.equalsIgnoreCase("true"))
					throw new RuntimeException(phoneNo);
				cafSrvcPhoneNos.setCafNo(flag != null && (flag.equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) || flag.equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) || flag.equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()))? addPkgCafNo : cafNo);
				cafSrvcPhoneNos.setParentCafno(cafNo);
				cafSrvcPhoneNos.setEffectiveFrom(Calendar.getInstance().getTime());
				cafSrvcPhoneNos.setPhoneNo(phoneNo);
				cafSrvcPhoneNos.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
				cafSrvcPhoneNos.setCreatedBy(loginId);
				cafSrvcPhoneNos.setCreatedOn(Calendar.getInstance());
				cafSrvcPhoneNos.setModifiedOn(Calendar.getInstance());
				cafSrvcPhoneNos.setCreatedIPAddr(ipAddress != null ? ipAddress : httpServletRequest.getRemoteAddr());
			    cafSrvcPhoneNos.setPassword(pwd);
			    if(telePhoneNos.isEmpty()) {
			    	telePhoneNos = phoneNo;
			    } else {
			    	telePhoneNos = telePhoneNos+","+phoneNo;
			    }
			    if(telePhonePwds.isEmpty()) {
			    	telePhonePwds = pwd;
			    } else {
			    	telePhonePwds = telePhonePwds+","+pwd;
			    }
			    cafSrvcPhoneNos.setTelePhoneNos(telePhoneNos);
			    cafSrvcPhoneNos.setTelePhonePwds(telePhonePwds);
				cafSrvcPhoneNosDao.saveCafSrvcPhoneNos(cafSrvcPhoneNos);
			}
			
		} catch(Exception e) {
			logger.error("CafSrvcPhoneNosServiceImpl::saveCafSrvcPhoneNos() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			stdCode = null;
			phoneNo = null;
			status = null;
		}
		return cafSrvcPhoneNos;
	}

}
