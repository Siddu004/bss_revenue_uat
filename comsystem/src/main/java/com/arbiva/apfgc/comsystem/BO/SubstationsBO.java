/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class SubstationsBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="substnuid")
	private String substnUid;
	
	@Column(name="substnname")
	private String substnName;
	
	@Column(name="districtuid")
	private String districtUid;
	
	@Column(name="mandalslno")
	private String mandalSlno;
	
	@Column(name="oltsrlno")
	private String oltSrlno; 
	
	@Column(name="vpnsrvcname")
	private String vpnSrvcName;
	
	public String getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(String substnUid) {
		this.substnUid = substnUid;
	}

	public String getSubstnName() {
		return substnName;
	}

	public void setSubstnName(String substnName) {
		this.substnName = substnName;
	}

	public String getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(String districtUid) {
		this.districtUid = districtUid;
	}

	public String getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(String mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public String getOltSrlno() {
		return oltSrlno;
	}

	public void setOltSrlno(String oltSrlno) {
		this.oltSrlno = oltSrlno;
	}

	public String getVpnSrvcName() {
		return vpnSrvcName;
	}

	public void setVpnSrvcName(String vpnSrvcName) {
		this.vpnSrvcName = vpnSrvcName;
	}
	
}
