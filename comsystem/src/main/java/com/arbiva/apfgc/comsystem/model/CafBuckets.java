/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cafbuckets")
public class CafBuckets extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CAFNO")
	private long cafNo;

	@Column(name = "bucketbalamt")
	private Float bucketbalamt;

	@Column(name = "lastcramt")
	private Float lastcramt;

	@Column(name = "lastcrdate")
	private Calendar lastcrdate; 

	@Column(name = "lastcrrefno")
	private String lastcrrefno;

	@Column(name = "lastdbamt")
	private Float lastdbamt;

	@Column(name = "lastdbdate")
	private Calendar lastdbdate;

	@Column(name = "lastdbrefno")
	private String lastdbrefno;

	@Column(name = "DEACTIVATEDBY")
	private Character deActivatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DEACTIVATEDON")
	private Date deActivatedOn;

	@Column(name = "DEACTIVATEDIPADDR")
	private String deActivatedIpAddr;
	
	@ManyToOne
    @JoinColumn(name = "cafno", referencedColumnName = "cafno", nullable = false, insertable = false, updatable = false)
    private Caf caf;
	
	public Caf getCaf() {
		return caf;
	}

	public void setCaf(Caf caf) {
		this.caf = caf;
	}

	public long getCafNo() {
		return cafNo;
	}

	public void setCafNo(long cafNo) {
		this.cafNo = cafNo;
	}

	public Float getBucketbalamt() {
		return bucketbalamt;
	}

	public void setBucketbalamt(Float bucketbalamt) {
		this.bucketbalamt = bucketbalamt;
	}

	public Float getLastcramt() {
		return lastcramt;
	}

	public void setLastcramt(Float lastcramt) {
		this.lastcramt = lastcramt;
	}

	public Calendar getLastcrdate() {
		return lastcrdate;
	}

	public void setLastcrdate(Calendar lastcrdate) {
		this.lastcrdate = lastcrdate;
	}

	public String getLastcrrefno() {
		return lastcrrefno;
	}

	public void setLastcrrefno(String lastcrrefno) {
		this.lastcrrefno = lastcrrefno;
	}

	public Float getLastdbamt() {
		return lastdbamt;
	}

	public void setLastdbamt(Float lastdbamt) {
		this.lastdbamt = lastdbamt;
	}

	public Calendar getLastdbdate() {
		return lastdbdate;
	}

	public void setLastdbdate(Calendar lastdbdate) {
		this.lastdbdate = lastdbdate;
	}

	public String getLastdbrefno() {
		return lastdbrefno;
	}

	public void setLastdbrefno(String lastdbrefno) {
		this.lastdbrefno = lastdbrefno;
	}

	public Character getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(Character deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public Date getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(Date deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public String getDeActivatedIpAddr() {
		return deActivatedIpAddr;
	}

	public void setDeActivatedIpAddr(String deActivatedIpAddr) {
		this.deActivatedIpAddr = deActivatedIpAddr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
