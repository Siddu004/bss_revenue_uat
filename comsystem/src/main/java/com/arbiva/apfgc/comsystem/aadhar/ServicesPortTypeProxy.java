package com.arbiva.apfgc.comsystem.aadhar;

public class ServicesPortTypeProxy implements ServicesPortType {
  private String _endpoint = null;
  private ServicesPortType servicesPortType = null;
  
  public ServicesPortTypeProxy() {
    _initServicesPortTypeProxy();
  }
  
  public ServicesPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicesPortTypeProxy();
  }
  
  private void _initServicesPortTypeProxy() {
    try {
      servicesPortType = (new ServicesLocator()).getServicesSOAP11port_http();
      if (servicesPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicesPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicesPortType != null)
      ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ServicesPortType getServicesPortType() {
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType;
  }
  
  public java.lang.String getAadhaarInfo(java.lang.String UID, java.lang.String EID) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.getAadhaarInfo(UID, EID);
  }
  
  
}