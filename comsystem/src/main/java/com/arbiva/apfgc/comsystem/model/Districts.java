/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="districts")
public class Districts implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="districtuid")
	private Integer districtUid;
	
	@Column(name="districtname")
	private String districtName;
	
	@Column(name="stateid")
	private Integer stateId;
	
	@ManyToOne
	@JoinColumn(name = "stateid", referencedColumnName = "stateid", nullable = false, insertable=false, updatable=false)
	private States state;

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public States getState() {
		return state;
	}

	public void setState(States state) {
		this.state = state;
	}
	
}
