package com.arbiva.apfgc.comsystem.util;

public enum ChargeTypes {
	
	RECURRING("1","Recurring"),
	ONETIME("2","Activation"),
	DEPOSIT("3","Deposit"),
	RESUME("4","Resume"),
	DISCONNECTION("5","Disconnection"),
	INSTALLATION("6","Installation"),
	CPE("7","CPE");
	
	private String chargeTypeId;
	private String name;
	
	ChargeTypes(String id, String name){
		this.chargeTypeId = id;
		this.name = name;
	}
	
	public String getchargeTypeId() {
		return chargeTypeId;
	}

	public String getName() {
		return name;
	}

}
