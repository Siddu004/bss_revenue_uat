/**
 * 
 */
package com.arbiva.apfgc.comsystem.util;

import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.comsystem.dto.UserMenuItemsRestDTO;

/**
 * @author Arbiva
 *
 */

public class ComsHelper {
	
	static final String APFGC_USER = "apfgc_user";
	private static final Logger logger = Logger.getLogger(ComsHelper.class);
	
	
	
	public static Model getMenuItemsDynamicallyByUserRole(String userName, Model uiModel, String umsURL, HttpServletRequest request){
		
		logger.info("-- ComsHelper -- getMenuItemsDynamicallyByUserRole --");
		RestTemplate restTemplate = new RestTemplate();
		String menuItemurl = umsURL+"rest/UMS/getHeader?userName="+userName;
		UserMenuItemsRestDTO userMenuObj = restTemplate.getForObject(menuItemurl, UserMenuItemsRestDTO.class);
		
		uiModel.addAttribute("domain",userMenuObj.getUsersDTOobj().getDomain());
		uiModel.addAttribute("tenantcode",userMenuObj.getUsersDTOobj().getTenantCode());
		uiModel.addAttribute("tenantname",userMenuObj.getUsersDTOobj().getTenantName());
		uiModel.addAttribute("moduleNameMap",userMenuObj.getMenuItemList());
		uiModel.addAttribute("userName",userMenuObj.getUsersDTOobj().getUserName());
		uiModel.addAttribute("loginID",userMenuObj.getUsersDTOobj().getLoginID());
		
		request.getSession().setAttribute("tenantcode", userMenuObj.getUsersDTOobj().getTenantCode());
		request.getSession().setAttribute("tenantname", userMenuObj.getUsersDTOobj().getTenantName());
		
		return uiModel;
	}
	
	public static  HttpEntity<String> getHeaderWithEncripted(){
		
		String string = "admin:admin";
        byte[] byteArray = Base64.encodeBase64(string.getBytes(Charset.forName("US-ASCII")));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", new String(byteArray));
        HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
        return httpEntity;
	}

}
