/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class ScrollTextDataMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String duration;
	
	private String message;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
