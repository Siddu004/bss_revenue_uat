/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="wipstages")
@IdClass(StatusCodesPK.class)
public class StatusCodes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "appcode")
	private String appCode;
	
	@Id
	@Column(name = "statuscode")
	private Integer statusCode;
	
	@Column(name = "statusdesc")
	private String statusDesc;
	
	@Column(name = "status")
	private Integer Status;

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public Integer getStatus() {
		return Status;
	}

	public void setStatus(Integer status) {
		Status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
