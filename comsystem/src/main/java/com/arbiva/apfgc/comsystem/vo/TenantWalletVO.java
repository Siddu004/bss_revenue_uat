/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;
/**
 * @author Lakshman
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TenantWalletVO {
	
		private static final long serialVersionUID = 1L;

		public TenantWalletVO() {
		
		}
		
		private String tenantcode;
		
		private String depositamt;
		
		private String depositrefno;
		
		private String loggeduser;
		
		private String ipaddr;
		
		private String status;
		
		private String errormsg;
		
		private Float walletAmount;
		
		private Float usedAmount;
		
		private Float creditAmount;
		
		private Float pecentage;
		
		private Float actualUserAmount;
		
		private String flag;
		
		private String alert;
		
		public String getAlert() {
			return alert;
		}

		public void setAlert(String alert) {
			this.alert = alert;
		}

		public String getFlag() {
			return flag;
		}

		public void setFlag(String flag) {
			this.flag = flag;
		}

		public Float getActualUserAmount() {
			return actualUserAmount;
		}

		public void setActualUserAmount(Float actualUserAmount) {
			this.actualUserAmount = actualUserAmount;
		}

		public Float getWalletAmount() {
			return walletAmount;
		}

		public void setWalletAmount(Float walletAmount) {
			this.walletAmount = walletAmount;
		}

		public Float getUsedAmount() {
			return usedAmount;
		}

		public void setUsedAmount(Float usedAmount) {
			this.usedAmount = usedAmount;
		}

		public Float getCreditAmount() {
			return creditAmount;
		}

		public void setCreditAmount(Float creditAmount) {
			this.creditAmount = creditAmount;
		}

		public Float getPecentage() {
			return pecentage;
		}

		public void setPecentage(Float pecentage) {
			this.pecentage = pecentage;
		}

		public String getTenantcode() {
			return tenantcode;
		}

		public void setTenantcode(String tenantcode) {
			this.tenantcode = tenantcode;
		}

		public String getDepositamt() {
			return depositamt;
		}

		public void setDepositamt(String depositamt) {
			this.depositamt = depositamt;
		}

		public String getDepositrefno() {
			return depositrefno;
		}

		public void setDepositrefno(String depositrefno) {
			this.depositrefno = depositrefno;
		}

		public String getLoggeduser() {
			return loggeduser;
		}

		public void setLoggeduser(String loggeduser) {
			this.loggeduser = loggeduser;
		}
		
		public String getIpaddr() {
			return ipaddr;
		}

		public void setIpaddr(String ipaddr) {
			this.ipaddr = ipaddr;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getErrormsg() {
			return errormsg;
		}

		public void setErrormsg(String errormsg) {
			this.errormsg = errormsg;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
}
