/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * @author Lakshman
 *
 */
@Entity
@IdClass(TerminatePkgBOPK.class)
public class TerminatePkgBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private String prodCafNo;
	
	@Id
	@Column(name = "prodcode")
	private String prodCode;
	
	@Id
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Id
	@Column(name = "stbslno")
	private String stbSrlNo;
	
	@Column(name = "parentcafno")
	private String cafNo;

	@Column(name = "proddate")
	private String prodDate;

	@Column(name = "prodname")
	private String prodName;

	@Column(name = "rsagruid")
	private String agrmntCode;

	@Column(name = "stbcafno")
	private String stbCafNo;

	@Column(name = "nwsubscode")
	private String nwSubscriberCode;

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getProdCafNo() {
		return prodCafNo;
	}

	public void setProdCafNo(String prodCafNo) {
		this.prodCafNo = prodCafNo;
	}

	public String getProdDate() {
		return prodDate;
	}

	public void setProdDate(String prodDate) {
		this.prodDate = prodDate;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAgrmntCode() {
		return agrmntCode;
	}

	public void setAgrmntCode(String agrmntCode) {
		this.agrmntCode = agrmntCode;
	}

	public String getStbCafNo() {
		return stbCafNo;
	}

	public void setStbCafNo(String stbCafNo) {
		this.stbCafNo = stbCafNo;
	}

	public String getStbSrlNo() {
		return stbSrlNo;
	}

	public void setStbSrlNo(String stbSrlNo) {
		this.stbSrlNo = stbSrlNo;
	}

	public String getNwSubscriberCode() {
		return nwSubscriberCode;
	}

	public void setNwSubscriberCode(String nwSubscriberCode) {
		this.nwSubscriberCode = nwSubscriberCode;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

}
