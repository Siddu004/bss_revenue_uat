/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;
import java.util.Map;

import com.arbiva.apfgc.comsystem.BO.CorpusPreviousBillBO;
import com.arbiva.apfgc.comsystem.dto.AadharHelperDTO;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerInfoDTO;
import com.arbiva.apfgc.comsystem.dto.LCODetailsDTO;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.vo.BillSummaryVO;
import com.arbiva.apfgc.comsystem.vo.CorpusBillVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
public interface CustomerService {
	
	public abstract Customer findByCustomerId(Long custId);

	public abstract List<Object[]> findAllCustomers(String tenantCode, String tenantType);
	
	public abstract ComsHelperDTO findAllCustomers(ComsHelperDTO comsHelperDTO);
	
	public abstract List<Object[]> findCustomers(String tenantCode, String tenantType);

	public abstract Customer createCustomers(CustomerCafVO customerCafVO, String loginID, String tenantCode);

	public abstract void updateCustomer(PaymentVO paymentVO, String loginID,int cardnum);
	
	public abstract String updateCustomerPaymentStatus(CustomerCafVO customerCafVO, PaymentVO paymentVO, String loginID, String onuNumber,int cardnum) throws Exception;
	
	public abstract void updateCustomerPackageStatus(CustomerCafVO customerCafVO);
	
	public Customer findByCustomerCode(String custCode);
	
	public Customer findByParentCustCode(String custCode);

	public abstract List<Object[]> findByParentCustomerCode(String customerCode, String tenantCode);
	
	public abstract Customer findParticularCustomer(Long custId, String custType);

	public abstract List<Customer> findByParentCustomerCode(String customerCode);

	public abstract Map<String, Map<String, Object>> buypackagefromcorpusserver(CustomerDetailsDTO customerDetailsDTO);

	public abstract String getBlackListCustomerByAadharNo(String aadharNumber);

	/*public abstract List<UploadHistory> findCafBulkUploadErrors();

	public abstract List<UploadHistory> findVPNUploadErrors();*/

	public abstract List<EntCafStage> stageCafBulkUploadErrors(String uploadId);

	public abstract List<VPNSrvcNamesStage> stageVPNUploadErrors(String uploadId);

	public abstract void updateCustomerDetails(CustomerCafVO customerCafVO);

	public abstract String saveCustomerDetails(String modificationData, CustomerCafVO customerCafVO);

	public abstract ComsHelperDTO getCafBulkUploadOrVPNUploadErrors(ComsHelperDTO comsHelperDTO, String uploadType);
	
	CustomerInfoDTO getCustDtlsByLoginid(String loginId);

	public abstract List<LCODetailsDTO> getNearestLCO(String districtId, String mandalId, String villageId);

	public abstract Customer findByCustomerCodeCSS(String aadharNo);

	AadharHelperDTO getLmoDetails(AadharHelperDTO helperDto);

	AadharHelperDTO getHsiUsageDtls(AadharHelperDTO helperDto);
	
	AadharHelperDTO getHsiUsageDtlsBasedOnCaf(AadharHelperDTO helperDto);

	AadharHelperDTO getTTDtls(AadharHelperDTO helperDto);

	AadharHelperDTO getPmntDtls(AadharHelperDTO helperDto);

	public abstract CorpusBillVO getCustomerDetailsByNWSubscriberCode(String nwSubscriberCode);

	public abstract CorpusPreviousBillBO getCustomerBillDetailsByNWSubscriberCode(String nwSubscriberCode);

	public abstract BillSummaryVO getBillSummary(String billNo, String customerId, String accountNo);

	AadharHelperDTO getEnterpriseHsiUsageDtls(String custid, Integer year, Integer month);

	AadharHelperDTO getEnterprisePhoneUsageDtls(String custid, Integer year, Integer month);
	
	AadharHelperDTO getPhoneUsageDtls(String acctcafno, Integer year, Integer month);

	AadharHelperDTO getEnterpriseCafDtls(String custid);

	AadharHelperDTO getEnterpriseCutomerDtls(String custid);
}
