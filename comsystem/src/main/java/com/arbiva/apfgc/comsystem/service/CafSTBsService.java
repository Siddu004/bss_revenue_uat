/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;

/**
 * @author Lakshman
 *
 */
public interface CafSTBsService {

	public abstract void saveCCafSTBs(CustomerCafVO customerCafVO, Float totalPaidAmount) throws Exception;
}
