package com.arbiva.apfgc.comsystem.serviceImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.BO.AlaCartePackagesBO;
import com.arbiva.apfgc.comsystem.controller.ProvisionRestController;
import com.arbiva.apfgc.comsystem.dao.impl.CafDao;
import com.arbiva.apfgc.comsystem.dao.impl.CorpusAPIDaoImpl;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dto.CorpusIptvDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.IptvPkeSales;
import com.arbiva.apfgc.comsystem.model.OlIptvPkeSales;
import com.arbiva.apfgc.comsystem.model.Products;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CorpusAPIService;
import com.arbiva.apfgc.comsystem.service.PaymentService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.util.Response;
import com.arbiva.apfgc.comsystem.vo.CustomerDetailsVo;
import com.arbiva.apfgc.comsystem.vo.GenarateChargesVO;
import com.arbiva.apfgc.comsystem.vo.TaxesVO;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;
import com.arbiva.apfgc.provision.dto.CafProvisioningDTO;
import com.xyzinnotech.bss.sms.dto.Sms;
import com.xyzinnotech.bss.sms.dto.SmsDTO;

@Service
@SuppressWarnings("unused")
public class CorpusAPIServiceImpl implements CorpusAPIService {

	private static final Logger logger = Logger.getLogger(CorpusAPIServiceImpl.class);

	@Autowired
	CorpusAPIDaoImpl corpusAPIDaoImpl;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Autowired
	ProvisionRestController provisionRestController;

	@Autowired
	CafDao cafDao;

	@Autowired
	CafService cafService;

	@Autowired
	PaymentService paymentService;

	@Autowired
	ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;

	@Override
	@Transactional
	public Map<String, Map<String, Object>> vodService(CustomerDetailsDTO customerDetailsDTO) {

		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		BigDecimal balance = null;
		BigDecimal chargedAmt;
		Object[] caf = null;
		String districId = null;
		String pmtCustId = null;
		int i;
		CafSTBs cafSTBs = null;
		GenarateChargesVO genarateChargesVO = null;
		String response = null;

		try {

			cafSTBs = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(customerDetailsDTO.getSubscribercode());

			if (cafSTBs.getParentCafno() != null) {
				balance = corpusAPIDaoImpl.findBalanceByParentCafNo(String.valueOf(cafSTBs.getParentCafno()));
				balance = balance == null ? new BigDecimal("0") : balance;

				chargedAmt = new BigDecimal(customerDetailsDTO.getPrice())
						.add(new BigDecimal(customerDetailsDTO.getSrvcTax()))
						.add(new BigDecimal(customerDetailsDTO.getSwatchTax()))
						.add(new BigDecimal(customerDetailsDTO.getKisanTax()));

				i = balance.add(chargedAmt).compareTo(new BigDecimal("0"));
				if (i == -1 || i == 0) {
					caf = corpusAPIDaoImpl.findDistrictCodeAndPmtCustIdByCaf(String.valueOf(cafSTBs.getParentCafno()));
					districId = caf[0].toString();
					pmtCustId = caf[1].toString();
					genarateChargesVO = new GenarateChargesVO(districId, cafSTBs, pmtCustId, customerDetailsDTO,chargedAmt);
					response = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);

					if (response.equalsIgnoreCase("Success")) {
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
						innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());

					} else {
						throw new RuntimeException();
					}
				} else {
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code401.getName());
					innerMap.put(Response.statusMessage.getName(), "Not Enough Balance");
					innerMap.put(Response.availableBal.getName(), balance.multiply(new BigDecimal("-1")));
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}

			returnMap.put(Response.responseStatus.getName(), innerMap);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			returnMap.put(Response.responseStatus.getName(), innerMap);
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
			balance = null;
			caf = null;
			districId = null;
			pmtCustId = null;
			genarateChargesVO = null;
			response = null;
		}
		return returnMap;
	}

	@Override
	public void validateRequest(CustomerDetailsDTO customerDetailsDTO) {

		if (customerDetailsDTO.getPackageCode() == null || customerDetailsDTO.getPackageCode().equalsIgnoreCase(""))
			throw new RuntimeException("Package Name Is Missing");
		if (customerDetailsDTO.getPrice() == null || customerDetailsDTO.getPrice().equalsIgnoreCase(""))
			throw new RuntimeException("Price Is Missing");
		if (customerDetailsDTO.getSrvcTax() == null || customerDetailsDTO.getSrvcTax().equalsIgnoreCase(""))
			throw new RuntimeException("Service Tax Amount Is Missing");
		if (customerDetailsDTO.getKisanTax() == null || customerDetailsDTO.getKisanTax().equalsIgnoreCase(""))
			throw new RuntimeException("Kisan Tax Amount Missing");
		if (customerDetailsDTO.getSwatchTax() == null || customerDetailsDTO.getSwatchTax().equalsIgnoreCase(""))
			throw new RuntimeException("Swacch Bharat Tax Amount Is Missing");
		if (customerDetailsDTO.getSubscribercode() == null
				|| customerDetailsDTO.getSubscribercode().equalsIgnoreCase(""))
			throw new RuntimeException("Subscriber Code Is Missing");
	}

	@Override
	@Transactional
	public Map<String, Map<String, Object>> getIptvPkges(String subscribercode) {
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		Object[] parentCafNoAndLmocode = null;
		List<AlaCartePackagesBO> list = null;
		List<Map<String, Object>> packList = new ArrayList<>();

		try {

			parentCafNoAndLmocode = corpusAPIDaoImpl.findParentCafeNoAndLmocodeBySubscriberCode(subscribercode);

			if (parentCafNoAndLmocode[1].toString() != null) {
				if(parentCafNoAndLmocode[3].toString().equalsIgnoreCase("SI") || parentCafNoAndLmocode[3].toString().equalsIgnoreCase("APSFL"))
					list = corpusAPIDaoImpl.getIptvPkgesByParentCafAndLmocodeForEnterPrise(parentCafNoAndLmocode);
				else
					list = corpusAPIDaoImpl.getIptvPkgesByParentCafAndLmocode(parentCafNoAndLmocode);
				
				for (AlaCartePackagesBO result : list) {
					if(result.getFeaturecodes() != null){
						Map<String, Object> pack = new HashMap<>();
						String[] priceAndTax = result.getPrice().split(",");
						String[] activation = result.getActivation().split(",");
						String[] recurring = result.getRecurring().toString().split(",");
						String[] deposit = result.getDepsoits().toString().split(",");
						pack.put(Response.PACKAGECODE.getName(), result.getProdcode().toString());
						pack.put(Response.PACKAGENAME.getName(), result.getProdname().toString());
						pack.put(Response.PRICE.getName(), priceAndTax[0]);
						pack.put(Response.TAX.getName(), priceAndTax[1]);
						pack.put(Response.TOTALPRICE.getName(),new BigDecimal(priceAndTax[0]).add(new BigDecimal(priceAndTax[1])));
						//pack.put(Response.ACTIVATION.getName(),new BigDecimal(activation[0]).add(new BigDecimal(activation[1])));
						//pack.put(Response.RECURRING.getName(),new BigDecimal(recurring[0]).add(new BigDecimal(recurring[1])));
						//pack.put(Response.DEPOSIT.getName(),new BigDecimal(deposit[0]).add(new BigDecimal(deposit[1])));
						pack.put(Response.CHANNELSLIST.getName(), result.getFeaturecodes());
						packList.add(pack);
					}
				}
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
				innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
				innerMap.put(Response.SUBSCRIBER_CODE.getName(), subscribercode);
				innerMap.put(Response.PACKAGELIST.getName(), packList);

			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
		}
		return returnMap;
	}

	@Override
	@Transactional
	public Map<String, Map<String, Object>> generateOTPForIptvPkges(CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CafSTBs cafSTBs = null;
		Customer customer = null;
		SmsDTO smsDTO = null;
		Long id = null;
		String randomNo = null;
		OlIptvPkeSales olIptvPkeSales = null;
		Random random = new Random();
		String status = "REQUESTED";
		BigDecimal prodPrice = new BigDecimal("0");
		BigDecimal balance = null;
		String[] priceAndTax = null;
		int i;

		try {
			cafSTBs = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(customerDetailsDTO.getSubscribercode());
			if (cafSTBs.getParentCafno() != null) {
				for (String packageCode : customerDetailsDTO.getPackageCodesList().split(",")) {
					priceAndTax = corpusAPIDaoImpl.findProdPriceByProdCode(packageCode).split(",");
					prodPrice.add(new BigDecimal(priceAndTax[0])).add(new BigDecimal(priceAndTax[1]));
				}
				balance = corpusAPIDaoImpl.findBalanceByParentCafNo(String.valueOf(cafSTBs.getParentCafno()));
				balance = balance == null ? new BigDecimal("0") : balance;
				i = balance.add(prodPrice).compareTo(new BigDecimal("0"));

				if (i == -1 || i == 0) {
					customer = corpusAPIDaoImpl.findCustomerCustId(customerDetailsDTO.getSubscribercode());
					randomNo = String.format("%04d", random.nextInt(10000));
					id = storedProcedureDAO.executeStoredProcedure("IPTVPURID");
					olIptvPkeSales = new OlIptvPkeSales(randomNo, String.valueOf(cafSTBs.getParentCafno()), id,customerDetailsDTO.getSubscribercode(), customerDetailsDTO.getPackageCodesList(), status);
					corpusAPIDaoImpl.save(olIptvPkeSales);

					smsDTO = new SmsDTO();
					smsDTO.setMobileNo(customer.getPocMob1());
					smsDTO.setMsg("OTP for subscribe channels is "+ randomNo +"don't share your OTP with anyone");
					provisionRestController.sendSMS(smsDTO);

					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
					innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
					innerMap.put(Response.SUBSCRIBER_CODE.getName(), customerDetailsDTO.getSubscribercode());
					innerMap.put(Response.PACKAGECODES.getName(), customerDetailsDTO.getPackageCodesList());
					innerMap.put(Response.OTP.getName(), randomNo);
					innerMap.put(Response.REFERENCENO.getName(), id);

				} else {
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code401.getName());
					innerMap.put(Response.statusMessage.getName(), "Not Enough Balance");
					innerMap.put(Response.availableBal.getName(), balance.multiply(new BigDecimal("-1")));
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			customer = null;
			smsDTO = null;
			id = null;
			randomNo = null;
			olIptvPkeSales = null;
			random = null;
			status = null;
		}
		return returnMap;
	}

	@Override
	@Transactional
	public CorpusIptvDTO activateAlaCartePackage(String referenceId) {
		CorpusIptvDTO corpusIptvDTO = null;
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CafProducts cafProducts = null;
		OlIptvPkeSales olIptvPkeSales = null;
		IptvPkeSales iptvPkeSales = null;
		String status = "SUCCESS";
		Caf caf = null;
		Long prodCafNo = null;
		CafServices cafSrvcs = null;
		String[] priceAndTax = null;
		BigDecimal prodPrice = new BigDecimal("0");
		BigDecimal balance = null;
		int i;
		String response = null;
		GenarateChargesVO genarateChargesVO = null;
		String regionCode = null;
		TaxesVO taxesVO = null;
		Long paymentId = null;
		String result = null;
		CafSTBs cafSTBs = null;

		try {
			olIptvPkeSales = corpusAPIDaoImpl.findById(referenceId);
			cafSTBs = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(olIptvPkeSales.getNwsubscode());
			if (olIptvPkeSales != null) {
				if(!olIptvPkeSales.getStatus().equalsIgnoreCase(status)){
					

					for (String packageCode : olIptvPkeSales.getPackages().split(",")) {
						priceAndTax = corpusAPIDaoImpl.findProdPriceByProdCode(packageCode).split(",");
						prodPrice = prodPrice.add(new BigDecimal(priceAndTax[0])).add(new BigDecimal(priceAndTax[1]));
					}
					
					if(!prodPrice.equals(new BigDecimal("0"))){
						balance = corpusAPIDaoImpl.findBalanceByParentCafNo(String.valueOf(olIptvPkeSales.getAcctcafno()));
						balance = balance == null ? new BigDecimal("0") : balance;
						i = balance.add(prodPrice).compareTo(new BigDecimal("0"));
					}else{
						i = 0;
					}
					

					if (i == -1 || i == 0) {
						
						prodCafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());

						/* Updating status in OlIptvPkeSales with Success */
						olIptvPkeSales.setStatus(status);
						olIptvPkeSales.setProdcafno(prodCafNo);
						olIptvPkeSales = corpusAPIDaoImpl.save(olIptvPkeSales);

						/* Saving IptvPkeSales data */
						iptvPkeSales = new IptvPkeSales(olIptvPkeSales, status);
						iptvPkeSales = corpusAPIDaoImpl.save(iptvPkeSales);

						/* Saving Data In CAF Products */
						String pkgs[] = olIptvPkeSales.getPackages().split(",");
						
						caf = cafDao.findByCafAccountNo(iptvPkeSales.getAcctcafno());
						regionCode = cafService.getRegionCodeByPinCode(String.valueOf(caf.getCustdistUid()), caf.getInstMandal(),caf.getInstCityVillage());
						for (String pkgCode : pkgs) {
							String rsAgrmtId = corpusAPIDaoImpl.findAgrmtIdPacakgeCodeAndLmoCode(pkgCode, caf.getLmoCode());
							rsAgrmtId = rsAgrmtId == null ? "0" : rsAgrmtId;
							Products product = corpusAPIDaoImpl.findProdByProdCode(pkgCode);
							cafProducts = new CafProducts(prodCafNo, caf, olIptvPkeSales, product,rsAgrmtId);
							corpusAPIDaoImpl.save(cafProducts);

							/* Saving Data In CAF Services */
							List<Object[]> srvcList = cafDao.findAllServicesByProdCode(pkgCode);
							for (Object[] srvcCode : srvcList) {
								cafSrvcs = new CafServices(prodCafNo, caf, olIptvPkeSales, pkgCode, srvcCode);
								cafSrvcs.setExpDate(product.getDurationDays() == 0 ? null : DateUtill.GetOneTimeServiceDate(product.getDurationDays()));
								cafSrvcs.setStbCafNo(cafSTBs.getStbcafno());
								corpusAPIDaoImpl.save(cafSrvcs);
								
								/* Calling Generate Charges SP To Insert data in CAFINVDETAILS table and update balance in CAFACCOUNT table */
								
								if(!prodPrice.equals(new BigDecimal("0"))){
								List<Object[]> prodCharges = corpusAPIDaoImpl.findProdChargesByProdCode(pkgCode,srvcCode[0].toString());
								for (Object[] obj : prodCharges) {
									taxesVO = paymentService.getTaxAmounts(obj[0].toString(), obj[1].toString(), regionCode,"1", obj[2].toString());
									genarateChargesVO = new GenarateChargesVO(caf.getInstDistrict(), caf.getCafNo(),
											caf.getPmntCustId(),  taxesVO, obj[2].toString(), prodCafNo, pkgCode,obj[0].toString(), obj[1].toString(),"INVOICE");
									genarateChargesVO.setRsAgrUid(rsAgrmtId);
									genarateChargesVO.setStbCafno(String.valueOf(cafSTBs.getStbcafno()));
									response = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
								}
							  }
							}
						}

						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
						innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());

					} else {
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.getName(), Response.Code401.getName());
						innerMap.put(Response.statusMessage.getName(), "Not Enough Balance");
						innerMap.put(Response.availableBal.getName(), balance.multiply(new BigDecimal("-1")));
					}
					
				}else{
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
					innerMap.put(Response.statusMessage.getName(), "Packages Already Activated");
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Reference ID");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			olIptvPkeSales = null;
			status = null;
		}
		corpusIptvDTO = new CorpusIptvDTO();
		corpusIptvDTO.setMap(returnMap);
		corpusIptvDTO.setProdCafNo(prodCafNo == null ? null : String.valueOf(prodCafNo));
		return corpusIptvDTO;
	}
	
	@Override
	@Transactional
	public CorpusIptvDTO activateAlaCartePackage(CustomerDetailsDTO customerDetailsDTO) {
		CorpusIptvDTO corpusIptvDTO = null;
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CafProducts cafProducts = null;
		OlIptvPkeSales olIptvPkeSales = null;
		IptvPkeSales iptvPkeSales = null;
		String status = "SUCCESS";
		Caf caf = null;
		Long id = null;
		Long prodCafNo = null;
		CafServices cafSrvcs = null;
		String[] priceAndTax = null;
		BigDecimal prodPrice = new BigDecimal("0");
		BigDecimal balance = null;
		int i;
		String response = null;
		GenarateChargesVO genarateChargesVO = null;
		String regionCode = null;
		TaxesVO taxesVO = null;
		Long paymentId = null;
		String result = null;
		CafSTBs cafSTBs = null;
		boolean flag=false;

		try {
			/*OlIptvPkeSales ol=corpusAPIDaoImpl.findSubscriberCode(customerDetailsDTO.getSubscribercode());
			if(ol != null && ol.getStatus().equalsIgnoreCase("SUCCESS")){
				logger.info("Package is already actvated for subscriber:"+customerDetailsDTO.getSubscribercode());
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Packages Already Activated");
			}else{*/
				cafSTBs = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(customerDetailsDTO.getSubscribercode());
				if (cafSTBs.getParentCafno() != null) {
					id = storedProcedureDAO.executeStoredProcedure("IPTVPURID");
					olIptvPkeSales = new OlIptvPkeSales("", String.valueOf(cafSTBs.getParentCafno()), id,customerDetailsDTO.getSubscribercode(), customerDetailsDTO.getPackageCodesList(), status);
					prodCafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
					logger.info("Prod caf no: "+prodCafNo+" for subscriber id:"+customerDetailsDTO.getSubscribercode());
					/* Updating status in OlIptvPkeSales with Success */
					olIptvPkeSales.setStatus(status);
					olIptvPkeSales.setProdcafno(prodCafNo);
					olIptvPkeSales = corpusAPIDaoImpl.save(olIptvPkeSales);
					/* Saving IptvPkeSales data */
					iptvPkeSales = new IptvPkeSales(olIptvPkeSales, status);
					iptvPkeSales = corpusAPIDaoImpl.save(iptvPkeSales);

					/* Saving Data In CAF Products */
					String pkgs[] = olIptvPkeSales.getPackages().split(",");
					
					caf = cafDao.findByCafAccountNo(iptvPkeSales.getAcctcafno());
					regionCode = cafService.getRegionCodeByPinCode(String.valueOf(caf.getCustdistUid()), caf.getInstMandal(),caf.getInstCityVillage());
					for (String pkgCode : pkgs) {
						logger.info("package code "+pkgCode+" for subscriber "+customerDetailsDTO.getSubscribercode()+" is activating");
						String rsAgrmtId = corpusAPIDaoImpl.findAgrmtIdPacakgeCodeAndLmoCode(pkgCode, caf.getLmoCode());
						rsAgrmtId = rsAgrmtId == null ? "0" : rsAgrmtId;
						Products product = corpusAPIDaoImpl.findProdByProdCode(pkgCode);
						cafProducts = new CafProducts(prodCafNo, caf, olIptvPkeSales, product,rsAgrmtId);
						corpusAPIDaoImpl.save(cafProducts);

						/* Saving Data In CAF Services */
						List<Object[]> srvcList = cafDao.findAllServicesByProdCode(pkgCode);
						for (Object[] srvcCode : srvcList) {
							cafSrvcs = new CafServices(prodCafNo, caf, olIptvPkeSales, pkgCode, srvcCode);
							cafSrvcs.setActDate(new Date());
							cafSrvcs.setExpDate(product.getDurationDays() == 0 ? null : DateUtill.GetOneTimeServiceDate(product.getDurationDays()));
							cafSrvcs.setStbCafNo(cafSTBs.getStbcafno());
							corpusAPIDaoImpl.save(cafSrvcs);
						}
					}
					flag=true;
					logger.info("alacart package is ready to activate for sub scriber id:"+customerDetailsDTO.getSubscribercode());
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
					innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
			} else {
				logger.info("**** Not a valid subscriber ****"+customerDetailsDTO.getSubscribercode());
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
	//	}
		} catch (Exception e) {
			logger.info(" Internal server error for ala cart channel activation for subscriber id:"+customerDetailsDTO.getSubscribercode());
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			olIptvPkeSales = null;
			status = null;
		}
		corpusIptvDTO = new CorpusIptvDTO();
		corpusIptvDTO.setMap(returnMap);
		if(flag)
			corpusIptvDTO.setProdCafNo(prodCafNo == null ? null : String.valueOf(prodCafNo));
		return corpusIptvDTO;
	}

	@Override
	@Transactional
	public Map<String, Map<String, Object>> reGenerateOTP(String referenceId) {
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		Customer customer = null;
		SmsDTO smsDTO = null;
		String randomNo = null;
		Random random = new Random();
		OlIptvPkeSales olIptvPkeSales = null;

		try {
			olIptvPkeSales = corpusAPIDaoImpl.findById(referenceId);
			if (olIptvPkeSales != null) {
				
				customer = corpusAPIDaoImpl.findCustomerCustId(olIptvPkeSales.getNwsubscode());
				randomNo = String.format("%04d", random.nextInt(10000));
				olIptvPkeSales.setOtp(randomNo);
				corpusAPIDaoImpl.save(olIptvPkeSales);

				smsDTO = new SmsDTO();
				smsDTO.setMobileNo(customer.getPocMob1());
				smsDTO.setMsg("OTP for subscribe channels is "+ randomNo +"don't share your OTP with anyone");
				provisionRestController.sendSMS(smsDTO);
				
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
				innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
				innerMap.put(Response.SUBSCRIBER_CODE.getName(), olIptvPkeSales.getNwsubscode());
				innerMap.put(Response.PACKAGECODES.getName(), olIptvPkeSales.getPackages());
				innerMap.put(Response.OTP.getName(), randomNo);
				innerMap.put(Response.REFERENCENO.getName(), referenceId);
				
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			customer = null;
			smsDTO = null;
			randomNo = null;
			random = null;
		}
		return returnMap;
	}
	
	
	@Transactional
	@Override
	public void saveDataInCafProdsAndCafSrvcs(CafProvisioningDTO caf) throws IOException, ParseException{
		String response;
		String regionCode = cafService.getRegionCodeByPinCode(caf.getDistId(), caf.getMandId(),caf.getVillId());
			Products product = corpusAPIDaoImpl.findProdByProdCode(caf.getPackageCode());
			CafProducts cafProducts = new CafProducts();
			cafProducts.setCafno(caf.getProdCafNo());
			cafProducts.setCreatedOn(Calendar.getInstance());
			cafProducts.setModifiedOn(Calendar.getInstance());
			cafProducts.setParentCafno(caf.getParentCafNo());
			cafProducts.setPmntCustId(caf.getPmtCustId());
			cafProducts.setProdCode(caf.getPackageCode());
			cafProducts.setTenantCode("APSFL");
			cafProducts.setStatus(2);
			cafProducts.setPriority(1);
			cafProducts.setMinlockDays(product.getLockInDays());
			cafProducts.setRsagruid(0L);
			cafProducts.setExpDate(product.getDurationDays() == 0 ? null : DateUtill.GetOneTimeServiceDate(product.getDurationDays()));
			corpusAPIDaoImpl.save(cafProducts);

			/* Saving Data In CAF Services */
			List<Object[]> srvcList = cafDao.findAllServicesByProdCode(caf.getPackageCode());
			for (Object[] srvcCode : srvcList) {
				CafServices cafSrvcs = new CafServices();
				cafSrvcs.setCafno(caf.getProdCafNo());
				cafSrvcs.setSrvcCode(srvcCode[0].toString());
				cafSrvcs.setTenantCode("APSFL");
				cafSrvcs.setProdCode(caf.getPackageCode());
				cafSrvcs.setFeatureCodes(srvcCode[1].toString());
				cafSrvcs.setMinlockDays(0);
				cafSrvcs.setPmntCustId(caf.getPmtCustId());
				//cafSrvcs.setNwsubsCode(caf.getNwSubscriberCode());
				cafSrvcs.setParentCafno(caf.getParentCafNo());
				cafSrvcs.setStatus(2);
				cafSrvcs.setCreatedOn(Calendar.getInstance());
				cafSrvcs.setModifiedOn(Calendar.getInstance());
				cafSrvcs.setExpDate(product.getDurationDays() == 0 ? null : DateUtill.GetOneTimeServiceDate(product.getDurationDays()));
				corpusAPIDaoImpl.save(cafSrvcs);
				
			}
		}

	@Override
	public Map<String, Map<String, Object>> sendCorpusSMS(Sms sms) {

		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CafSTBs cafSTBs = null;
		Customer customer = null;
		SmsDTO smsDTO = null;
		String status = "";
		try {
			logger.info("Ala-carte Sub code is:"+sms.getSubscribercode());
			cafSTBs = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(sms.getSubscribercode());
			if (cafSTBs.getParentCafno() != null) {
					//customer = corpusAPIDaoImpl.findCustomerCustId(sms.getSubscribercode());
				String mob=corpusAPIDaoImpl.findCustomerMobNo(sms.getSubscribercode());
				logger.info("Mobile Num for sending SMS:"+mob);
					smsDTO = new SmsDTO();
					smsDTO.setMobileNo(mob);
					smsDTO.setMsg(sms.getMsg());
					status=provisionRestController.sendSMS(smsDTO);
					if(!status.isEmpty() && !status.equalsIgnoreCase("FAIL")){
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
						innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
					}else{
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.getName(), Response.Code402.getName());
						innerMap.put(Response.statusMessage.getName(), "Message sending failed to subscriber:"+sms.getSubscribercode());
					}
					
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("Corpus SMS sending failed:" + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			customer = null;
			smsDTO = null;
			status = null;
		}
		return returnMap;
	
		
	}
	
	@Override
	@Transactional
	public CorpusIptvDTO deActivateAlaCartePackage(CustomerDetailsDTO customerDetailsDTO) {
		CorpusIptvDTO corpusIptvDTO = null;
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CafProducts cafProducts = null;
		OlIptvPkeSales olIptvPkeSales = null;
		IptvPkeSales iptvPkeSales = null;
		String status = "SUCCESS";
		Long id = null;
		Long prodCafNo = null;
		CafServices cafSrvcs = null;
		String[] priceAndTax = null;
		BigDecimal prodPrice = new BigDecimal("0");
		BigDecimal balance = null;
		int i;
		String response = null;
		IptvPkeSales iptvPkgSales = null;
		boolean flag=false;

		try {
			iptvPkgSales = corpusAPIDaoImpl.findParentCafeNoBySubscriberCode(customerDetailsDTO.getSubscribercode(),customerDetailsDTO.getPackageCodesList());
			/* Updating Status In IPTV PKG SALES */
			iptvPkgSales.setStatus("DEACT");
			corpusAPIDaoImpl.save(iptvPkgSales);
				if (iptvPkgSales.getRequestid() != null) {
					String pkgs[] = iptvPkgSales.getPackages().split(",");
					for (String pkgCode : pkgs) {
						/* Updating Status In CAF Services */
						logger.info("package code "+pkgCode+" for subscriber "+customerDetailsDTO.getSubscribercode()+" is deactivating");
						cafSrvcs = corpusAPIDaoImpl.findCafSrvcsByProdCodeCafNo(iptvPkgSales.getProdcafno(), pkgCode);

						if(cafSrvcs != null){
							cafSrvcs.setStatus(Integer.parseInt(Response.DEACTIVATIONSTATUS.getName()));
							cafSrvcs.setDeactDate(new Date());
							cafSrvcs.setModifiedOn  (Calendar.getInstance());
							corpusAPIDaoImpl.save(cafSrvcs);
						}
					}
					flag=true;
					logger.info("alacart package is ready to deactivate for sub scriber id:"+customerDetailsDTO.getSubscribercode());
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.getName(), Response.Code200.getName());
					innerMap.put(Response.statusMessage.getName(), ComsEnumCodes.ACCEPTED.getCode());
			} else {
				logger.info("**** Not a valid subscriber ****"+customerDetailsDTO.getSubscribercode());
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.getName(), Response.Code403.getName());
				innerMap.put(Response.statusMessage.getName(), "Not Valid Subscriber");
			}
	//	}
		} catch (Exception e) {
			logger.info(" Internal server error for ala cart channel deactivation for subscriber id:"+customerDetailsDTO.getSubscribercode());
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), "Internal Server Error");
			logger.error("CustomerServiceImpl :: deactivation() :: " + e);
			e.printStackTrace();
		} finally {
			returnMap.put(Response.responseStatus.getName(), innerMap);
			innerMap = null;
			olIptvPkeSales = null;
			status = null;
		}
		corpusIptvDTO = new CorpusIptvDTO();
		corpusIptvDTO.setMap(returnMap);
		if(flag)
			corpusIptvDTO.setProdCafNo(iptvPkgSales.getProdcafno() == null ? null : String.valueOf(iptvPkgSales.getProdcafno()));
			corpusIptvDTO.setCafNo(iptvPkgSales.getProdcafno()== null ? null : String.valueOf(iptvPkgSales.getProdcafno()));
			corpusIptvDTO.setProdCode(iptvPkgSales.getPackages()== null ? null : String.valueOf(iptvPkgSales.getPackages()));
			//corpusIptvDTO.setStbCafNo(cafSrvcs.getStbCafNo()== null ? null : String.valueOf(cafSrvcs.getStbCafNo()));
			corpusIptvDTO.setNwSubsCode(iptvPkgSales.getNwsubscode()== null ? null : String.valueOf(iptvPkgSales.getNwsubscode()));
		return corpusIptvDTO;
	}


}
