/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Arbiva
 *
 */
public class TaxesVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String srvcTax;

	private String entTax;

	private String swatchTax;

	private String kissanTax;

	public String getSrvcTax() {
		return srvcTax;
	}

	public void setSrvcTax(String srvcTax) {
		this.srvcTax = srvcTax;
	}

	public String getEntTax() {
		return entTax;
	}

	public void setEntTax(String entTax) {
		this.entTax = entTax;
	}

	public String getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(String swatchTax) {
		this.swatchTax = swatchTax;
	}

	public String getKissanTax() {
		return kissanTax;
	}

	public void setKissanTax(String kissanTax) {
		this.kissanTax = kissanTax;
	}
	public String getTotalTax(){
		BigDecimal b ;
		b =  new BigDecimal(this.kissanTax).add(new BigDecimal(this.swatchTax)).add(new BigDecimal(this.srvcTax)).add(new BigDecimal(this.entTax));
		return b.toString();
	}

}
