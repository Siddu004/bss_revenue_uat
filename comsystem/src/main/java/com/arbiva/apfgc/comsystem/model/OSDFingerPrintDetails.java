/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="osdfngprndtl")
@IdClass(OSDFingerPrintDetailsPK.class)
public class OSDFingerPrintDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "requestid")
	private Long requestId;
	
	@Id
	@Column(name = "jsonseqno")
	private Integer jsonSeqno;
	
	@Column(name = "subscriptionnos")
	private String subScriptionnos;
	
	@Column(name = "request_json")
	private String requestJson;
	
	@Column(name = "response_json")
	private String responseJson;
	
	@Column(name = "requesttype")
	private String requestType;
	
	@Column(name = "responseid")
	private String responseId;
	
	@Column(name = "displaymsg")
	private String displayMessage;
	
	@Column(name = "STATUS")
	private int status;
	
	@Column(name = "createdon")
	private Calendar createdOn;
	
	@Column(name = "requestdate")
	private Calendar requestDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String createdIpaddr;

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Integer getJsonSeqno() {
		return jsonSeqno;
	}

	public void setJsonSeqno(Integer jsonSeqno) {
		this.jsonSeqno = jsonSeqno;
	}

	public String getSubScriptionnos() {
		return subScriptionnos;
	}

	public void setSubScriptionnos(String subScriptionnos) {
		this.subScriptionnos = subScriptionnos;
	}
	
	public String getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedIpaddr() {
		return createdIpaddr;
	}

	public void setCreatedIpaddr(String createdIpaddr) {
		this.createdIpaddr = createdIpaddr;
	}

	public Calendar getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Calendar requestDate) {
		this.requestDate = requestDate;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	
}
