package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Details implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="districtname")
  private String districtname;


  public String getDistrictname() { return this.districtname; }

  public void setDistrictname(String districtname) { this.districtname = districtname; }

  @Column(name="districtid")
  private int districtid;

  public int getDistrictid() { return this.districtid; }

  public void setDistrictid(int districtid) { this.districtid = districtid; }

  
  @Column(name="mandalname")
  private String mandalname;

  public String getMandalname() { return this.mandalname; }

  public void setMandalname(String mandalname) { this.mandalname = mandalname; }

  @Column(name="mandalid")
  private int mandalid;

  public int getMandalid() { return this.mandalid; }

  public void setMandalid(int mandalid) { this.mandalid = mandalid; }

  @Column(name="village_Name")
  private String village_Name;

  public String getVillageName() { return this.village_Name; }

  public void setVillageName(String village_Name) { this.village_Name = village_Name; }
  
  
  @Column(name="villageid")
  private int villageid;

  public int getVillageid() { return this.villageid; }

  public void setVillageid(int villageid) { this.villageid = villageid; }

  @Column(name="regionname")
  private String regionname;

  public String getRegionname() { return this.regionname; }

  public void setRegionname(String regionname) { this.regionname = regionname; }

  @Column(name="Today")
  private int Today;

  public int getToday() { return this.Today; }

  public void setToday(int Today) { this.Today = Today; }

  @Column(name="last7days")
  private int last7days;

  public int getLast7days() { return this.last7days; }

  public void setLast7days(int last7days) { this.last7days = last7days; }

  
  @Column(name="last30days")
  private int last30days;

  public int getLast30days() { return this.last30days; }

  public void setLast30days(int last30days) { this.last30days = last30days; }

  @Column(name="last3months")
  private int last3months;

  public int getLast3months() { return this.last3months; }

  public void setLast3months(int last3months) { this.last3months = last3months; }

  
  @Column(name="last6months")
  private int last6months;

  public int getLast6months() { return this.last6months; }

  public void setLast6months(int last6months) { this.last6months = last6months; }
}





