/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "villages")
public class Villages implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="villageuid")
	private Integer villageUid;
	
	@Column(name="villagename")
	private String villageName;
	
	@Column(name="stateid")
	private Integer stateId;
	
	@Column(name="districtuid")
	private Integer districtUid;
	
	@Column(name="mandalslno")
	private Integer mandalSlno;
	
	@Column(name="villageslno")
	private Integer villageSlno;
	
	@Column(name="stdcode")
	private String stdCode;
	
	@Column(name="pincode")
	private String pinCode;
	
	/*@Column(name="substnuid")
	private Integer substnUid;*/
	
	@Column(name="enttax_zone")
	private String entTaxZone;
	
	@Column(name="enttax_circle")
	private String entTaxCircle;
	
	@Column(name="srvctax_circle")
	private String srvcTaxCircle;
	
	@Column(name="swachcess_circle")
	private String swachcessCircle;
	
	@Column(name="kisancess_circle")
	private String kisancessCircle;
	
	@ManyToOne
	@JoinColumns({@JoinColumn(name = "districtuid", referencedColumnName = "districtuid", nullable = false, insertable=false, updatable=false), @JoinColumn(name = "mandalslno", referencedColumnName = "mandalslno", nullable = false, insertable=false, updatable=false)})
	private Mandals mandal;
	
	@ManyToOne
	@JoinColumn(name = "stateid", referencedColumnName = "stateid", nullable = false, insertable=false, updatable=false)
	private States states;
	
	/*@ManyToOne
	@JoinColumn(name = "substnuid", referencedColumnName = "substnuid", nullable = false, insertable=false, updatable=false)
	private Substations substations;*/

	public Integer getVillageUid() {
		return villageUid;
	}

	public void setVillageUid(Integer villageUid) {
		this.villageUid = villageUid;
	}

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public Integer getVillageSlno() {
		return villageSlno;
	}

	public void setVillageSlno(Integer villageSlno) {
		this.villageSlno = villageSlno;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Mandals getMandal() {
		return mandal;
	}

	public void setMandal(Mandals mandal) {
		this.mandal = mandal;
	}

	public States getStates() {
		return states;
	}

	public void setStates(States states) {
		this.states = states;
	}

	public Integer getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(Integer mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	/*public Integer getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(Integer substnUid) {
		this.substnUid = substnUid;
	}*/

	public String getEntTaxZone() {
		return entTaxZone;
	}

	public void setEntTaxZone(String entTaxZone) {
		this.entTaxZone = entTaxZone;
	}

	public String getEntTaxCircle() {
		return entTaxCircle;
	}

	public void setEntTaxCircle(String entTaxCircle) {
		this.entTaxCircle = entTaxCircle;
	}

	public String getSrvcTaxCircle() {
		return srvcTaxCircle;
	}

	public void setSrvcTaxCircle(String srvcTaxCircle) {
		this.srvcTaxCircle = srvcTaxCircle;
	}

	public String getSwachcessCircle() {
		return swachcessCircle;
	}

	public void setSwachcessCircle(String swachcessCircle) {
		this.swachcessCircle = swachcessCircle;
	}

	public String getKisancessCircle() {
		return kisancessCircle;
	}

	public void setKisancessCircle(String kisancessCircle) {
		this.kisancessCircle = kisancessCircle;
	}

	/*public Substations getSubstations() {
		return substations;
	}

	public void setSubstations(Substations substations) {
		this.substations = substations;
	}*/
	
}
