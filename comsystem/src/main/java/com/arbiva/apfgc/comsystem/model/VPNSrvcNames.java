/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "vpnsrvcnames")
@IdClass(VPNSrvcNamesPK.class)
public class VPNSrvcNames implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "substnuid")
	private String substnUid;
	
	@Id
	@Column(name = "olt_serialno")
	private String oltSerialNo;
	
	@Id
	@Column(name = "vpnsrvcname")
	private String vpnsrvcName;
	
	@Column(name = "districtuid")
	private Integer districtUid;
	
	@Column(name = "mandalslno")
	private Integer mandalSlno;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdon")
	private Calendar createdOn;
	
	@Column(name = "status")
	private String status;
	
	@Transient
	private String successRecordCount;
	
	@Transient
	private String failedRecordCount;

	public String getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(String substnUid) {
		this.substnUid = substnUid;
	}

	public String getOltSerialNo() {
		return oltSerialNo;
	}

	public void setOltSerialNo(String oltSerialNo) {
		this.oltSerialNo = oltSerialNo;
	}

	public String getVpnsrvcName() {
		return vpnsrvcName;
	}

	public void setVpnsrvcName(String vpnsrvcName) {
		this.vpnsrvcName = vpnsrvcName;
	}

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public Integer getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(Integer mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSuccessRecordCount() {
		return successRecordCount;
	}

	public void setSuccessRecordCount(String successRecordCount) {
		this.successRecordCount = successRecordCount;
	}

	public String getFailedRecordCount() {
		return failedRecordCount;
	}

	public void setFailedRecordCount(String failedRecordCount) {
		this.failedRecordCount = failedRecordCount;
	}
	
}
