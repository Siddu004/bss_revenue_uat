/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdaywiseDTO;
import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdistrictwiseDTO;
import com.arbiva.apfgc.comsystem.BO.CountdetailsCustomerwiseDTO;
import com.arbiva.apfgc.comsystem.BO.Details;
import com.arbiva.apfgc.comsystem.model.CafAccount;



/**
 * @author Lakshman
 *
 */
@Repository
public class CafAccountDao {

	private static final Logger logger = Logger.getLogger(CafAccountDao.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<CafAccount> findAllCafAccounts() {
		List<CafAccount> cafAccountsList = new ArrayList<CafAccount>();
		TypedQuery<CafAccount> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(CafAccount.class.getSimpleName());
		try {
			query = getEntityManager().createQuery(builder.toString(), CafAccount.class);
			cafAccountsList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION CafAccountDao :: findAllCafAccounts() " + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return cafAccountsList;
	}

	public void saveCafAccount(CafAccount cafAccount) {
		try {
			getEntityManager().merge(cafAccount);
		} catch (Exception e) {
			logger.error("EXCEPTION CafAccountDao :: saveCafAccount() " + e);
			e.printStackTrace();
		} finally {

		}
	}

	public CafAccount findByCafNo(Long cafNo, Integer district) {
		CafAccount cafAccount = new CafAccount();
		TypedQuery<CafAccount> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(CafAccount.class.getSimpleName())
				.append(" WHERE acctcafno =:cafNo and districtuid =:district");
		try {
			query = getEntityManager().createQuery(builder.toString(), CafAccount.class);
			query.setParameter("cafNo", cafNo);
			query.setParameter("district", district);
			cafAccount = query.getSingleResult();
		} catch (Exception e) {
			logger.error("EXCEPTION CafAccountDao :: findByCafNo() " + e);
		} finally {
			builder = null;
			query = null;
		}
		return cafAccount;
	}

	//added by chaitanya
	@SuppressWarnings({ "unchecked" })
	public List<CafCountDetailsdistrictwiseDTO> getCountdetailsdistrictwise() {
		List<CafCountDetailsdistrictwiseDTO> lst = new ArrayList<CafCountDetailsdistrictwiseDTO>();
		StringBuilder builder = new StringBuilder("");
		Query query = null;
		try {
			builder.append("SELECT d.districtname District_Name,d.districtuid Districtid, m.mandalname Mandal_Name,m.mandalslno Mandalid, v.villagename Village_Name,v.villageslno Villageid,tax.regionname, c.custtypelov cust_type, cus.enttypelov Sub_type, COUNT(*) No_Connections FROM cafs c ");
			builder.append("LEFT JOIN customermst cus ON cus.custid=c.custid ");
			builder.append("LEFT JOIN districts d ON d.districtuid=C.inst_district ");
			builder.append("LEFT JOIN mandals m ON m.mandalslno=c.inst_mandal AND d.districtuid=m.districtuid ");
			builder.append("LEFT JOIN villages v ON v.villageslno=c.inst_city_village AND v.mandalslno=m.mandalslno AND v.districtuid=d.districtuid "); 
			builder.append("LEFT JOIN taxregions tax ON v.enttax_zone=tax.regioncode ");
			builder.append("WHERE c.status='6' GROUP BY C.INST_DISTRICT,C.INST_MANDAL,C.INST_CITY_VILLAGE, C.custtypelov,cus.enttypelov,tax.regionname ");
			query = getEntityManager() .createNativeQuery(builder.toString(),CafCountDetailsdistrictwiseDTO.class);
			lst = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCountdetailsdistrictwise() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return lst;
	}
	
	
	
		
	
	//added by chaitanya
			@SuppressWarnings({ "unchecked" })
			public List<CountdetailsCustomerwiseDTO> getCountdetailsCustomerwise() {
				List<CountdetailsCustomerwiseDTO> lst = null;
				StringBuilder builder = new StringBuilder("");
				Query query = null;
				try {
					builder.append("SELECT c.custtypelov cust_type, cus.enttypelov Sub_type, COUNT(*) No_Connections FROM cafs c  " + 
							"LEFT JOIN customermst cus ON cus.custid=c.custid " + 
							"WHERE c.status='6'  GROUP BY  C.custtypelov,cus.enttypelov");
					logger.info("query " +builder.toString());
					query = getEntityManager() .createNativeQuery(builder.toString(),CountdetailsCustomerwiseDTO.class);
					lst = query.getResultList();
				} catch(Exception ex){
					logger.error("EXCEPTION::getCountdetailsCustomerwise() " + ex);
				}finally{
					builder = null;
					query = null;
				}
				return lst;
			}
			
			@SuppressWarnings({ "unchecked" })
			public Object getCafCountDetails(String custtype,String subtype) {
				StringBuilder builder = new StringBuilder("");
				Query query = null;
				Object object = null;
				try {
					logger.info("query " +builder.toString());
					builder.append("SELECT SUM(CASE WHEN cf.actdate = CURDATE() THEN 1 ELSE 0 END) Today, ");
					builder.append("SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 1 DAY THEN 1 ELSE 0 END) yesterday, ");
					builder.append("SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 7 DAY THEN 1 ELSE 0 END) last7days, ");
					builder.append("SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 30 DAY THEN 1 ELSE 0 END) last30days, ");
					builder.append("SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 3 MONTH THEN 1 ELSE 0 END) last3months, ");
					builder.append("SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 6 MONTH THEN 1 ELSE 0 END) last6months ");
					builder.append("FROM cafs cf,customermst cst WHERE cf.STATUS =6 AND cf.custid=cst.custid ");
					if(custtype.equals("") && subtype.equals("")){
						query = getEntityManager() .createNativeQuery(builder.toString());
					}else{
						builder.append("AND cst.custtypelov=?");
						if(custtype.equalsIgnoreCase("ENTERPRISE")) {
							builder.append("AND cst.enttypelov=?");
						}
						query = getEntityManager() .createNativeQuery(builder.toString());
						query.setParameter(1, custtype);
						if(custtype.equalsIgnoreCase("ENTERPRISE")) {
							query.setParameter(2, subtype);
						}
						
					}
					object = query.getSingleResult();
				} catch(Exception ex){
					logger.error("EXCEPTION::getIndividualCafCountDetails() " + ex);
				}finally{
					builder = null;
					query = null;
				}
				return object;
			}

			@SuppressWarnings("unchecked")
			public Object getLastTenDaysCountDetails() {
				StringBuilder builder = new StringBuilder("");
				Query query = null;
				Object object = null;
				try {
					logger.info("query " +builder.toString());
					builder.append("SELECT SUM(CASE WHEN actdate = CURDATE() THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 2 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 3 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 4 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 5 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 6 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 7 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 8 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 9 DAY THEN 1 ELSE 0 END)," + 
							"SUM(CASE WHEN actdate >= DATE(NOW()) - INTERVAL 10 DAY THEN 1 ELSE 0 END)" + 
							"FROM cafs WHERE STATUS =6;");
					query = getEntityManager() .createNativeQuery(builder.toString());
					object = query.getSingleResult();
				} catch(Exception ex){
					logger.error("EXCEPTION::getLastTenDayCountDetails() " + ex);
				}finally{
					builder = null;
					query = null;
				}
				return object;
			}


			
			@SuppressWarnings("unchecked")
			public List<Details> getDistrictINDENTDurationDetails(String custtype,String enttypelov) {
				StringBuilder builder = new StringBuilder("");
				Query query = null;
				List<Details> object = new ArrayList<Details>();
				
				
				try {
					logger.info("query " +builder.toString());
					builder.append("SELECT d.districtname districtname,d.districtuid districtid, m.mandalname mandalname,m.mandalslno mandalid, v.villagename village_Name,v.villageslno villageid,tax.regionname regionname,SUM(CASE WHEN cf.actdate = CURDATE() THEN 1 ELSE 0 END) Today,\n" + 
							"SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 7 DAY THEN 1 ELSE 0 END) last7days,\n" + 
							"SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 30 DAY THEN 1 ELSE 0 END) last30days,\n" + 
							"SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 3 MONTH THEN 1 ELSE 0 END) last3months,\n" + 
							"SUM(CASE WHEN cf.actdate >= DATE(NOW()) - INTERVAL 6 MONTH THEN 1 ELSE 0 END) last6months\n" + 
							"FROM customermst cst \n" + 
							"LEFT JOIN cafs cf ON cf.custid=cst.custid\n" + 
							"LEFT JOIN districts d ON d.districtuid=cf.inst_district\n" + 
							"LEFT JOIN mandals m ON m.mandalslno=cf.inst_mandal AND d.districtuid=m.districtuid\n" + 
							"LEFT JOIN villages v ON v.villageslno=cf.inst_city_village AND v.mandalslno=m.mandalslno AND v.districtuid=d.districtuid \n" + 
							"LEFT JOIN taxregions tax ON v.enttax_zone=tax.regioncode\n" + 
							"WHERE cf.STATUS =6 AND cf.custid=cst.custid\n");
					builder.append("AND cst.custtypelov='"+custtype.toUpperCase()+"'");
					if(!custtype.toUpperCase().equalsIgnoreCase("INDIVIDUAL")) builder.append("AND cst.enttypelov='"+enttypelov.toUpperCase()+"'");
					builder.append("GROUP BY d.districtname ,d.districtuid , m.mandalname ,m.mandalslno , v.villagename ,v.villageslno ,tax.regionname;");
					query = getEntityManager() .createNativeQuery(builder.toString(),Details.class);
					object = query.getResultList();
				} catch(Exception ex){
					logger.error("EXCEPTION::getLastTenDayCountDetails() " + ex);
				}finally{
					builder = null;
					query = null;
				}
				return object;
			}

}

