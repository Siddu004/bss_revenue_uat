/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafFeatureParamsDao;
import com.arbiva.apfgc.comsystem.model.CafFeatureParams;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.FeatuireVO;
import com.arbiva.apfgc.comsystem.vo.ParamsVO1;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafFeatureParamsServiceImpl implements CafFeatureParamsService {

	private static final Logger logger = Logger.getLogger(CafFeatureParamsServiceImpl.class);

	@Autowired
	CafFeatureParamsDao cafFeatureParamsDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;

	@Override
	@Transactional
	public void saveCafFeatureParams(CustomerCafVO customerCafVO) {
		CafFeatureParams cafFeatureParams = null;
		try {
			for (FeatuireVO featuireVO : customerCafVO.getFeatureCodeList()) {
				String featureCode = "";
				featureCode = featuireVO.getFeatureCode();
				if (featureCode != null) {
					for (ParamsVO1 paramsVO : featuireVO.getParamsList()) {
						cafFeatureParams = new CafFeatureParams();
						String[] paramCode = paramsVO.getParamCode().split(",");
						cafFeatureParams.setCafNo(customerCafVO.getCafNo());
						cafFeatureParams.setParentCafno(customerCafVO.getCafNo());
						cafFeatureParams.setCreatedBy(customerCafVO.getLoginId());
						cafFeatureParams.setCreatedOn(Calendar.getInstance());
						cafFeatureParams.setEffectivefrom(Calendar.getInstance().getTime());
						cafFeatureParams.setModifiedOn(Calendar.getInstance());
						cafFeatureParams.setProdCode(customerCafVO.getTelProdCode());
						cafFeatureParams.setSrvcCode(customerCafVO.getTelSrvcCode());
						cafFeatureParams.setTenantCode(customerCafVO.getTelTenantCode());
						cafFeatureParams.setFeatureCode(featuireVO.getFeatureCode());
						cafFeatureParams.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
						cafFeatureParams.setPrmCode(paramCode[0]);
						cafFeatureParams.setPrmvalue(paramsVO.getParamValue());
						cafFeatureParams.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
						cafFeatureParams = cafFeatureParamsDao.saveCafFeatureParams(cafFeatureParams);
					}
				}
			}
		} catch (Exception e) {
			logger.error("CafFeatureParamsServiceImpl::saveCafFeatureParams() " + e);
			e.printStackTrace();
		} finally {
			cafFeatureParams = null;
		}
	}

	@Override
	@Transactional
	public void updateCafFeatureParams(PaymentVO paymentVO, String loginID) {
		try {
			cafFeatureParamsDao.updateCafFeatureParams(paymentVO, loginID);
		} catch (Exception e) {
			logger.error("CafFeatureParamsServiceImpl::updateCafFeatureParams() " + e);
			e.printStackTrace();
		} finally {

		}
	}

}
