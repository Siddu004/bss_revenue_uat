/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="chargecodes")
public class ChargeCodes extends Base  {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="chargecode")
	private String chargeCode;
	
	@Column(name="chargename")
	private String chargeName;
	
	@Column(name="chargetypeflag")
	private Character chargeTypeFlag;
	
	@Column(name="chargelevelflag")
	private Character chargeLevelFlag;
	
	@Column(name="taxlevelflag")
	private Character taxLevelFlag;
	
	@Column(name="refundflag")
	private Character refundFlag;
	
	@Column(name="glcode")
	private String glcode;

	@ManyToOne
	@JoinColumn(name = "glcode", referencedColumnName = "glcode", nullable = false, insertable=false, updatable=false)
	private GlCode glCode;
	
	public GlCode getGlCode() {
		return glCode;
	}

	public void setGlCode(GlCode glCode) {
		this.glCode = glCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeName() {
		return chargeName;
	}

	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}

	public Character getChargeTypeFlag() {
		return chargeTypeFlag;
	}

	public void setChargeTypeFlag(Character chargeTypeFlag) {
		this.chargeTypeFlag = chargeTypeFlag;
	}

	public Character getChargeLevelFlag() {
		return chargeLevelFlag;
	}

	public void setChargeLevelFlag(Character chargeLevelFlag) {
		this.chargeLevelFlag = chargeLevelFlag;
	}

	public Character getTaxLevelFlag() {
		return taxLevelFlag;
	}

	public void setTaxLevelFlag(Character taxLevelFlag) {
		this.taxLevelFlag = taxLevelFlag;
	}

	public Character getRefundFlag() {
		return refundFlag;
	}

	public void setRefundFlag(Character refundFlag) {
		this.refundFlag = refundFlag;
	}

	public String getGlcode() {
		return glcode;
	}

	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}
	
}
