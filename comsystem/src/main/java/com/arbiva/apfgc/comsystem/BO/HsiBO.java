/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class HsiBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="prmcode")
	private String prmCode;
	
	@Column(name="prmvalue")
	private String prmValue;

	public String getPrmCode() {
		return prmCode;
	}

	public void setPrmCode(String prmCode) {
		this.prmCode = prmCode;
	}

	public String getPrmValue() {
		return prmValue;
	}

	public void setPrmValue(String prmValue) {
		this.prmValue = prmValue;
	}
	
}
