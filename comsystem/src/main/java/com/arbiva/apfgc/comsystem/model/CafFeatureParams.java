/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="caffeatureprms")
@IdClass(CafFeatureParamsPK.class)
public class CafFeatureParams extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private Long cafNo;
	
	@Id
	@Column(name = "srvccode")
	private String srvcCode;
	
	@Id
	@Column(name = "featurecode")
	private String featureCode;
	
	@Id
	@Column(name = "prmcode")
	private String prmCode;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectivefrom;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Column(name = "prodcode")
	private String prodCode;
	
	@Column(name = "prmvalue")
	private String prmvalue;
	
	@ManyToOne
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable=false, updatable=false)
	private Caf cafNumber;
	
	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}

	public Caf getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(Caf cafNumber) {
		this.cafNumber = cafNumber;
	}

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getPrmCode() {
		return prmCode;
	}

	public void setPrmCode(String prmCode) {
		this.prmCode = prmCode;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	public String getPrmvalue() {
		return prmvalue;
	}

	public void setPrmvalue(String prmvalue) {
		this.prmvalue = prmvalue;
	}
	
}
