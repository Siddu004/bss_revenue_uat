/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lakshman
 *
 */
public class FeatureParamsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String coresrvccode;
	
	private Date effectiveFrom;
	
	private String featurecode;
	
	private String prmCode;

	public String getCoresrvccode() {
		return coresrvccode;
	}

	public void setCoresrvccode(String coresrvccode) {
		this.coresrvccode = coresrvccode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getFeaturecode() {
		return featurecode;
	}

	public void setFeaturecode(String featurecode) {
		this.featurecode = featurecode;
	}

	public String getPrmCode() {
		return prmCode;
	}

	public void setPrmCode(String prmCode) {
		this.prmCode = prmCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coresrvccode == null) ? 0 : coresrvccode.hashCode());
		result = prime * result + ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
		result = prime * result + ((featurecode == null) ? 0 : featurecode.hashCode());
		result = prime * result + ((prmCode == null) ? 0 : prmCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeatureParamsPK other = (FeatureParamsPK) obj;
		if (coresrvccode == null) {
			if (other.coresrvccode != null)
				return false;
		} else if (!coresrvccode.equals(other.coresrvccode))
			return false;
		if (effectiveFrom == null) {
			if (other.effectiveFrom != null)
				return false;
		} else if (!effectiveFrom.equals(other.effectiveFrom))
			return false;
		if (featurecode == null) {
			if (other.featurecode != null)
				return false;
		} else if (!featurecode.equals(other.featurecode))
			return false;
		if (prmCode == null) {
			if (other.prmCode != null)
				return false;
		} else if (!prmCode.equals(other.prmCode))
			return false;
		return true;
	}
	
}
