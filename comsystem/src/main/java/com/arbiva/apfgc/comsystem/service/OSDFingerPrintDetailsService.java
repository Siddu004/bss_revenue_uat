/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import com.arbiva.apfgc.comsystem.model.OSDFingerPrintDetails;
import com.arbiva.apfgc.comsystem.vo.CorpusResponseVO;

/**
 * @author Lakshman
 *
 */
public interface OSDFingerPrintDetailsService {
	
	public abstract OSDFingerPrintDetails saveOSDFingerPrintDetails(String subscribers, String requestJson, String responseJson, String loginID, Integer jsonSeqNo, String fpType, String trackingId, String message);

	public abstract String updateOSDFingerPrintDetails(CorpusResponseVO corpusResponseVO);

}
