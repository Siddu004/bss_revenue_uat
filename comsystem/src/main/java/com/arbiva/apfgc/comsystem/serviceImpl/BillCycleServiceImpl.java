/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.BillCycleDao;
import com.arbiva.apfgc.comsystem.model.BillCycle;

/**
 * @author Lakshman
 *
 */
@Service("billCycleService")
@Transactional
public class BillCycleServiceImpl {

	private static final Logger logger = Logger.getLogger(BillCycleServiceImpl.class);

	@Autowired
	BillCycleDao billCycleDao;

	public List<BillCycle> findAllBillCycle() {
		List<BillCycle> billCycle = new ArrayList<BillCycle>();
		try {
			billCycle = billCycleDao.findAllBillCycle();
		} catch (Exception e) {
			logger.error("BillCycleServiceImpl :: findAllBillCycle()" + e);
			e.printStackTrace();
		} finally {

		}
		return billCycle;
	}

}
