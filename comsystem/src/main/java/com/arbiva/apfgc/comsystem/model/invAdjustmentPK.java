package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

public class invAdjustmentPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long adjrefId;
	
	private int adjmm;

	public Long getAdjrefId() {
		return adjrefId;
	}

	public void setAdjrefId(Long adjrefId) {
		this.adjrefId = adjrefId;
	}

	public int getAdjmm() {
		return adjmm;
	}

	public void setAdjmm(int adjmm) {
		this.adjmm = adjmm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + adjmm;
		result = prime * result + ((adjrefId == null) ? 0 : adjrefId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		invAdjustmentPK other = (invAdjustmentPK) obj;
		if (adjmm != other.adjmm)
			return false;
		if (adjrefId == null) {
			if (other.adjrefId != null)
				return false;
		} else if (!adjrefId.equals(other.adjrefId))
			return false;
		return true;
	}
	
}
