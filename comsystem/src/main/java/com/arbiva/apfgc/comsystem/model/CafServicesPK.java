/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Arbiva
 *
 */
public class CafServicesPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long cafno;
	
	private String srvcCode;
	
	private Long stbCafNo;

	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}
	
	public Long getStbCafNo() {
		return stbCafNo;
	}

	public void setStbCafNo(Long stbCafNo) {
		this.stbCafNo = stbCafNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cafno == null) ? 0 : cafno.hashCode());
		result = prime * result + ((srvcCode == null) ? 0 : srvcCode.hashCode());
		result = prime * result + ((stbCafNo == null) ? 0 : stbCafNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafServicesPK other = (CafServicesPK) obj;
		if (cafno == null) {
			if (other.cafno != null)
				return false;
		} else if (!cafno.equals(other.cafno))
			return false;
		if (srvcCode == null) {
			if (other.srvcCode != null)
				return false;
		} else if (!srvcCode.equals(other.srvcCode))
			return false;
		if (stbCafNo == null) {
			if (other.stbCafNo != null)
				return false;
		} else if (!stbCafNo.equals(other.stbCafNo))
			return false;
		return true;
	}
	
}
