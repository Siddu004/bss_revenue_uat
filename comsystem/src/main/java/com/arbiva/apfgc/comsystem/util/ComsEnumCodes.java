package com.arbiva.apfgc.comsystem.util;

public enum ComsEnumCodes {
	
	CAF_DEACTIVE_STATUS(8),
	CAF_PROCESS_DEACTIVE_STATUS(81),
	CAF_SUSPEND_STATUS(7),
	PENDING_FOR_PACKAGE_STATUS(0),
	PENDING_FOR_PAYMENT_STATUS(1),
	PAYMENT_RECEIVED_STATUS(2),
	CAF_REJECTED_STAUTS(3),
	CAF_ALLOCATED_STATUS(4),
	CAF_BLOCKED_STATUS(41),
	LMO_ALLOCATED_STAUTS(3),
	MONTHLY_PAYMENT_STAUTS(22),
	WORKORDER_PAYMENT_STATUS(21),
	CUSTOMER_BLACKLIST_STATUS(99),
	Caf_BulkUpload_status(88),
	Caf_Edit_BulkUpload_status(89),
	Caf_Provision_status(6),
	
	Service_TaxLevelFlag("1"),
	CUSTOMER_PAYMENT_RECEIVED_STATUS("2"),
	Customer_Caf_BulkUpload_status("88"),
	Customer_Caf_Edit_BulkUpload_status("89"),
	CAF_PENDING_FOR_PACKAGE_STATUS("0"),
	Caf_TaxLevelFlag("2"),
	Recurring_ChargeTypeFlag("1"),
	Activation_ChargeTypeFlag("2"),
	SecurityDeposit_ChargeTypeFlag("3"),
	ENT_Caf_Pending("1"),
	ENT_Caf_Activation("2"),
	ENT_Caf_Rejected("3"),
	ENT_Caf_UploadId("UPLOADID"),
	CafInv_ChargeType("CHARGED"),
	ONU_COST_CODE("CPECOST"),
	ONU_UPFRONT_CODE("CPEUPFRONT"),
	ONU_INSTALLATION_CODE("CPEEMI"), 
	CPE_INSTALLATION_CODE("INSTALLATION"), 
	CPE_EXTRACABLE_CHARGE_CODE("EXTRACABLE"), 
	IPTV_COST_CODE("STBCOST"),
	IPTV_UPFRONT_CODE("STBUPFRONT"),
	IPTV_INSTALLATION_CODE("STBEMI"), 
	CPE_DEVICE_PURCHASE_CODE("P"),
	CPE_DEVICE_INSTALLATION_CODE("I"),
	CPE_ENTTAX_CODE("ENTTAX"),
	ONE_PACKAGE_TYPE_CODE("O"),
	BASE_PACKAGE_TYPE_CODE("B"),
	ADDON_PACKAGE_TYPE_CODE("A"),
	CUST_TYPE_CODE("INDIVIDUAL"),
	ENTCUST_TYPE_CODE("ENTERPRISE"),
	BILLCYCLE_MONTHLY("MONTHLY"),
	BILLCYCLE_QUARTERLY("QUARTERLY"),
	BILLCYCLE_HALFYEARLY("HALFYEARLY"),
	BILLCYCLE_YEARLY("YEARLY"),
	REGION_TYPE("GENERAL"), 
	SERVICE_FEATURE_PARAM_TYPE("LOV"),
	CHARGE_TYPE_RECURRING("Recurring"),
	CHARGE_TYPE_ACTIVATION("Activation"),
	CHARGE_TYPE_DEPOSIT("Deposit"), 
	CORE_SERVICE_VOIP("VOIP"), 
	CORE_SERVICE_IPTV("IPTV"),
	CORE_SERVICE_HSI("HSI"),
	CORE_SERVICE_VPN("VPN"),
	CUSTOMER_ID("CUSTID"), 
	CAF_NO("CAFNO"), 
	PAYMENT_ID("PAYMENTID"), 
	ACCEPTED("ACCEPTED"),
	ENTERPRISE_CUSTOMER_ID("ENTCUSTID"),
	PAYMENT_MODES("PAYMENT MODES"), 
	ONU_TYPE("ONU"), 
	IPTV_TYPE("IPTV/Android Box"), 
	BILLFREQUENCY("BILL FREQUENCY"), 
	TITLES("TITLES"), 
	ENTERPRISE_TYPES("ENTERPRISE TYPES"), 
	GOVT("GOVT"), 
	GOVT_TYPES("GOVT TYPES"), 
	FP_REQUEST_ID("FPREQUESTID"),
	PRIVATE_TYPES("PRIVATE TYPES"),
	Srvc_TaxCode("SERVICE"),
	Ent_TaxCode("ENTTAX"),
	Swatch_TaxCode("SWATCHBHARAT"),
	Kissan_TaxCode("KRISHIKALYAN"),
	VOD_CHARGE_CODE("VOD"),
	APSFL_Tenant_Type("APSFL"),
	LMO_Tenant_Type("LMO"),
	MSP_Tenant_Type("MSP"),
	ENT_Upload_caf_Type("ENTCAF"),
	VPN_Upload_Type("VPN"),
	SI_Tenant_Type("SI"),
	Call_Center_Tenant_Type("Call Center"),
	MOBILE_VERSION("VERSION"),
	TELEPHONE_CONNECTIONS("TELEPHONE_CONNECTIONS"),
	SOURCE_CODE("DTS"),
	Change_Pkg_Flag("chnagePkgPageFlag"),
	Add_Pkg_Flag("addPkgPageFlag"),
	Css_Pkg_Flag("cssPkgFlag"),
	CUSTOMER_TYPES("CUSTOMER TYPES"),
	HSI_Add_Pkg("HSI"),
	INVADJREF_ID("INVADJREFID");
	
	

	private  String code;
	private  int status;

	private ComsEnumCodes(String code) {
		this.code = code;
	}

	private ComsEnumCodes(int status) {
		this.status = status;
	}
	
	public String getCode() {
		return code ;
	}
	public int getStatus() {
		return status ;
	}
	
}
