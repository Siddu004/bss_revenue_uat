package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.exception.ErrorMessages;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Repository("errorMessagesDAO")
public class ErrorMessagesDAO {

	private static final Logger LOGGER = Logger.getLogger(ErrorMessagesDAO.class);
	
	private EntityManager em;
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	String errorMessages;
	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public void initErrorMessages(){
		List<ErrorMessages> list=new ArrayList<ErrorMessages>();
		
		StringBuilder builder = new StringBuilder("FROM ").append(ErrorMessages.class.getSimpleName()).append(" WHERE status='1' ");
		try {
			LOGGER.info("START::errorMessages()");
			TypedQuery<ErrorMessages> query = getEntityManager().createQuery(builder.toString(), ErrorMessages.class);
			
			list = query.getResultList();
			
			loadincache(list);
			
			LOGGER.info("END::errorMessages()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::errorMessages() " + e);
		}finally {
			builder=null;
		}
			LOGGER.info("Error messages list size :"+list.size());
		  return ;
	}
	
	private void loadincache(List<ErrorMessages> errMsgList) {
		//1. Create a cache manager
		CacheManager cm = CacheManager.getInstance();
		try{
			//2. Create a cache called "cache1"
			cm.addCache("errorMsgCache");
			
			//3. Get a cache called "cache1"
			Cache cache = cm.getCache("errorMsgCache");
			
			//4. Put few elements in cache
			LOGGER.info("ErrorMessagesDAO loadincache ");
			if(errMsgList.size() > 0){
				for (ErrorMessages errorMsg : errMsgList){
					cache.put(new Element(errorMsg.getErrorCode(),errorMsg.getErrorDesc()));					
				}				
			}
		
	 }catch (Exception ex) {
		 ex.printStackTrace();
	 }finally{
		
	 }
}

}
	
	
	
	
	


