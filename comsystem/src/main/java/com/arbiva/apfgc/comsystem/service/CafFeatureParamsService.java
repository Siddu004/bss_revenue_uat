/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
public interface CafFeatureParamsService {
	
	public abstract void saveCafFeatureParams(CustomerCafVO customerCafVO);
	
	public abstract void updateCafFeatureParams(PaymentVO paymentVO, String loginID);

}
