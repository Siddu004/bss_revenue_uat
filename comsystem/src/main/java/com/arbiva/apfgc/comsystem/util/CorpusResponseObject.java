/**
 * 
 */
package com.arbiva.apfgc.comsystem.util;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class CorpusResponseObject<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private T t;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}
	
}
