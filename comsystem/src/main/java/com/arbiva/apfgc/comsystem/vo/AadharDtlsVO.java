package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

public class AadharDtlsVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String fname;
	
	private String lname;
	
	private String cafno;
	
	private String balance;
	
	private String districtno;

	private String custid;
	
	private String custtypelov;
	
	private String mobileNo;
	
	private String email;
	
	private String address;
	
	private String landline;
	
	private String tenantCode;
	
	private String lmoName;
	
	private String aadharNo;
	
	private String usageLimit;
	
	private String totalLimit;
	
	private String status;
	
	private String count;
	
	private String date;
	
	private String phoneUsageCost;
	
	private String pocname;
	
	private String villagename;
	
	private String mandalname;
	
	private String districtname;
	
	private String addres2;
	
	private String cpeprofileid;
	private String calledParty;
	private String startTime;
	private String endTime;
	private String callDuration;
	private String callunits;
	private String callCharges;
	private String cafStatus;
	
	
	
	public String getCafStatus() {
		return cafStatus;
	}

	public void setCafStatus(String cafStatus) {
		this.cafStatus = cafStatus;
	}

	private String agorahsisubscode;
	
	public String getCpeprofileid() {
		return cpeprofileid;
	}

	public void setCpeprofileid(String cpeprofileid) {
		this.cpeprofileid = cpeprofileid;
	}

	public String getAgorahsisubscode() {
		return agorahsisubscode;
	}

	public void setAgorahsisubscode(String agorahsisubscode) {
		this.agorahsisubscode = agorahsisubscode;
	}

	public String getCalledParty() {
		return calledParty;
	}

	public void setCalledParty(String calledParty) {
		this.calledParty = calledParty;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getCallunits() {
		return callunits;
	}

	public void setCallunits(String callunits) {
		this.callunits = callunits;
	}

	public String getCallCharges() {
		return callCharges;
	}

	public void setCallCharges(String callCharges) {
		this.callCharges = callCharges;
	}

	public String getAddres2() {
		return addres2;
	}

	public void setAddres2(String addres2) {
		this.addres2 = addres2;
	}

	public String getPocname() {
		return pocname;
	}

	public void setPocname(String pocname) {
		this.pocname = pocname;
	}

	public String getVillagename() {
		return villagename;
	}

	public void setVillagename(String villagename) {
		this.villagename = villagename;
	}

	public String getMandalname() {
		return mandalname;
	}

	public void setMandalname(String mandalname) {
		this.mandalname = mandalname;
	}

	public String getDistrictname() {
		return districtname;
	}

	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}

	public String getPhoneUsageCost() {
		return phoneUsageCost;
	}

	public void setPhoneUsageCost(String phoneUsageCost) {
		this.phoneUsageCost = phoneUsageCost;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCafno() {
		return cafno;
	}

	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getDistrictno() {
		return districtno;
	}

	public void setDistrictno(String districtno) {
		this.districtno = districtno;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getCusttypelov() {
		return custtypelov;
	}

	public void setCusttypelov(String custtypelov) {
		this.custtypelov = custtypelov;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLmoName() {
		return lmoName;
	}

	public void setLmoName(String lmoName) {
		this.lmoName = lmoName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getUsageLimit() {
		return usageLimit;
	}

	public void setUsageLimit(String usageLimit) {
		this.usageLimit = usageLimit;
	}

	public String getTotalLimit() {
		return totalLimit;
	}

	public void setTotalLimit(String totalLimit) {
		this.totalLimit = totalLimit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
}
