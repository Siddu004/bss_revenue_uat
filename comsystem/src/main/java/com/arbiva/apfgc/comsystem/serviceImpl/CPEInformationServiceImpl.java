package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.comsystem.BO.CpeChargeDetailsBO;
import com.arbiva.apfgc.comsystem.BO.SubstationsBO;
import com.arbiva.apfgc.comsystem.BO.TenantsBO;
import com.arbiva.apfgc.comsystem.BO.VillagesBO;
import com.arbiva.apfgc.comsystem.controller.ComsRestController;
import com.arbiva.apfgc.comsystem.dao.impl.CPEInformationDao;
import com.arbiva.apfgc.comsystem.model.CpeModal;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.FeatureParams;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.model.OLT;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;
import com.arbiva.apfgc.comsystem.model.SrvcFeatures;
import com.arbiva.apfgc.comsystem.model.Substations;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.Villages;
import com.arbiva.apfgc.comsystem.service.CPEInformationService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.FileUtil;
import com.arbiva.apfgc.comsystem.vo.CpeInformationVO;
import com.arbiva.apfgc.comsystem.vo.CpeStockVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafModifyVo;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.STBChargesVO;
import com.arbiva.apfgc.comsystem.vo.SplitterVO;
import com.arbiva.apfgc.comsystem.vo.TenantBusinessAreas;

@Service
public class CPEInformationServiceImpl implements CPEInformationService {

	@Autowired
	CPEInformationDao cPEInformationDao;

	@Autowired
	CafService cafService;

	private static final Logger logger = Logger.getLogger(ComsRestController.class);

	@Override
	public List<Districts> getAllDistricts() {
		List<Districts> districtsList = new ArrayList<>();
		try {
			districtsList = cPEInformationDao.findAllDistricts();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return districtsList;
	}

	@Override
	public List<Mandals> getAllMandals() {
		List<Mandals> MandalsList = new ArrayList<>();
		try {
			MandalsList = cPEInformationDao.getAllMandals();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllMandals()" + e);
			e.printStackTrace();
		} finally {

		}
		return MandalsList;
	}

	@Override
	public Mandals getMandalsByDistrictIdAndMandalSrlNo(Integer districtId, Integer mandalSrlNo) {
		Mandals mandals = new Mandals();
		try {
			mandals = cPEInformationDao.getMandalsByDistrictIdAndMandalSrlNo(districtId, mandalSrlNo);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getMandalsByDistrictIdAndMandalSrlNo()" + e);
			e.printStackTrace();
		} finally {

		}
		return mandals;
	}

	@Override
	public List<OLT> getAllOLTs() {
		List<OLT> oltList = new ArrayList<>();
		try {
			oltList = cPEInformationDao.getAllOLTs();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllOLTs()" + e);
			e.printStackTrace();
		} finally {

		}
		return oltList;
	}

	@Override
	public List<Substations> getAllSubStations(String subStationCodes) {
		List<Substations> substationsList = new ArrayList<>();
		List<Object[]> substationList = new ArrayList<>();
		Substations substations = null;
		try {
			substationList = cPEInformationDao.getAllSubStations(subStationCodes);
			for (Object[] substation : substationList) {
				substations = new Substations();
				substations.setSubstnUid(substation[0].toString());
				substations.setSubstnName(substation[1].toString());
				substations.setDistrictUid(Integer.parseInt(substation[2].toString()));
				substations.setMandalSlno(Integer.parseInt(substation[3].toString()));
				substationsList.add(substations);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllSubStations()" + e);
			e.printStackTrace();
		} finally {
			substationList = null;
			substations = null;
		}
		return substationsList;
	}
	
	@Override
	public boolean check_SubstUid_OltSrNo_VPNSrNm(String subStationCodes, String oltSrNo, String VPNSrName) {
		boolean status = false;
		try {
			status = cPEInformationDao.check_SubstUid_OltSrNo_VPNSrNm(subStationCodes, oltSrNo, VPNSrName);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllSubStations()" + e);
			e.printStackTrace();
		} finally {
		}
		return status;
	}

	@Override
	public String getCpeModelByProfileId(Integer profileId) {
		String cpeModel = "";
		try {
			cpeModel = cPEInformationDao.getCpeModelByProfileId(profileId);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getCpeModelByProfileId()" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeModel;
	}

	@Override
	public List<CpeModal> getAllCpeModals() {
		List<CpeModal> cpeModelList = new ArrayList<>();
		try {
			cpeModelList = cPEInformationDao.getAllCpeModals();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllCpeModals()" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeModelList;
	}

	@Override
	public List<CpeModal> getAllCpeModalsByCpeType(String cpeType) {
		List<CpeModal> cpeModelList = new ArrayList<>();
		try {
			cpeModelList = cPEInformationDao.getAllCpeModalsByCpeType(cpeType);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllCpeModalsByCpeType()" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeModelList;
	}

	@Override
	public List<Mandals> getMandalsByDistrictId(Integer districtId) {
		List<Mandals> MandalsList = new ArrayList<>();
		try {
			MandalsList = cPEInformationDao.getMandalsByDistrictId(districtId);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getMandalsByDistrictId()" + e);
			e.printStackTrace();
		} finally {

		}
		return MandalsList;
	}

	@Override
	public List<Substations> getSubstationsByDistrictIdAndMandalId(Integer districtId, Integer mandalId) {
		List<Object[]> substationsList = new ArrayList<>();
		List<Substations> resultSubstationsList = new ArrayList<>();
		try {
			substationsList = cPEInformationDao.getSubstationsByDistrictIdAndMandalId(districtId, mandalId);
			for(Object[] substations : substationsList) {
				Substations substationsObject = new Substations();
				substationsObject.setSubstnUid(substations[0].toString());
				substationsObject.setSubstnName(substations[1].toString());
				resultSubstationsList.add(substationsObject);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getMandalsByDistrictId()" + e);
			e.printStackTrace();
		} finally {
			substationsList = null;
		}
		return resultSubstationsList;
	}

	@Override
	public List<OLT> getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(Integer districtId, Integer mandalId, Integer subStnSlno) {
		List<OLT> oltList = new ArrayList<>();
		try {
			oltList = cPEInformationDao.getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(districtId, mandalId, subStnSlno);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno()" + e);
			e.printStackTrace();
		} finally {

		}
		return oltList;
	}

	@Override
	public List<OLT> getOLTSLNOBySubstationsrlno(String subStnSlno) {
		List<OLT> oltList = new ArrayList<>();
		List<Object[]> oltObjectList = null;
		OLT olt = null;
		String subStnId = "";
		try {
			subStnId = FileUtil.mspCodeSplitMethod(subStnSlno);
			oltObjectList = cPEInformationDao.getOLTSLNOBySubstationsrlno(subStnId);
			for (Object[] oltObject : oltObjectList) {
				olt = new OLT();
				olt.setPop_oltlabelno(oltObject[0] != null ? oltObject[0].toString() : "");
				olt.setPopOltSerialno(oltObject[1] != null ? oltObject[1].toString() : "");
				olt.setPopId(oltObject[2] != null ? Long.parseLong((oltObject[2].toString())) : null);
				olt.setPopName(oltObject[3] != null ? oltObject[3].toString() : "");
				olt.setPopSubstnuid(oltObject[4] != null ? oltObject[4].toString() : "");
				olt.setPopOltIpaddress(oltObject[5] != null ? oltObject[5].toString() : "");
				olt.setOltAccessNode(oltObject[6] != null ? oltObject[6].toString() : "");
				oltList.add(olt);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getOLTSLNOBySubstationsrlno()" + e);
			e.printStackTrace();
		} finally {
			oltObjectList = null;
			olt = null;
			subStnId = null;
		}
		return oltList;
	}

	@Override
	public List<OLTPortDetails> getOLTPopIdByOltSrlNo(String oltSrlNo, String lmoCode) {
		List<OLTPortDetails> oltPortDetailsList = new ArrayList<>();
		List<Object[]> oltObjectList = new ArrayList<>();
		OLTPortDetails oltPortDetails = null;
		try {
			oltObjectList = cPEInformationDao.getOLTPopIdByOltSrlNo(oltSrlNo, lmoCode);
			for (Object[] oltObject : oltObjectList) {
				oltPortDetails = new OLTPortDetails();
				oltPortDetails.setPopOltSerialno(oltObject[0].toString());
				oltPortDetails.setPortNo(Integer.parseInt(oltObject[1].toString()));
				oltPortDetailsList.add(oltPortDetails);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getOLTPopIdByOltSrlNo()" + e);
			e.printStackTrace();
		} finally {
			oltObjectList = null;
			oltPortDetails = null;
		}
		return oltPortDetailsList;
	}

	@Override
	public Cpecharges getAlCpeChargesByCpeModel(String serialNumber) {
		Cpecharges cpecharges = new Cpecharges();
		try {
			cpecharges = cPEInformationDao.getAlCpeChargesByCpeModel(serialNumber);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAlCpeChargesByCpeModel()" + e);
			e.printStackTrace();
		} finally {

		}
		return cpecharges;
	}

	@Override
	public CpeInformationVO getCpeChargesInformation(CustomerCafVO customerCafVO) {
		List<STBChargesVO> stbChargesList = new ArrayList<>();
		BigDecimal onuPrice = new BigDecimal("0");
		BigDecimal onuEmiPrice = new BigDecimal("0");
		BigDecimal cpeInstallationPrice = new BigDecimal("0");
		BigDecimal cpeExtCablePrice = new BigDecimal("0");
		BigDecimal cpeTaxAmt = new BigDecimal("0");
		BigDecimal stbEmiPrice = new BigDecimal("0");
		BigDecimal totalCharge = new BigDecimal("0");
		BigDecimal cpeChrg = new BigDecimal("0");
		Cpecharges onucharges = new Cpecharges();
		Cpecharges stbcharges = new Cpecharges();
		Object[] instTax = null;
		Object[] extraCabelTax = null;
		Object[] onuPriceTax = null;
		Object[] stbPriceTax = null;
		
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		try {
			onucharges = this.getAlCpeChargesByCpeModel(customerCafVO.getCpeId());
			cpeChrg = cpeChrg.add(new BigDecimal(onucharges.getUpFrontCharges().toString()));
			cpeInstallationPrice = cpeInstallationPrice.add(onucharges.getInstcharges());
			instTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.CPE_INSTALLATION_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			cpeTaxAmt = cpeTaxAmt.add((cpeInstallationPrice.multiply(new BigDecimal(instTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(instTax[1].toString()));
			
			cpeExtCablePrice = cpeExtCablePrice.add(new BigDecimal(customerCafVO.getCableCharge()));
			extraCabelTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.CPE_EXTRACABLE_CHARGE_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			cpeTaxAmt = cpeTaxAmt.add((cpeExtCablePrice.multiply(new BigDecimal(extraCabelTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(extraCabelTax[1].toString()));
			
			if(customerCafVO.getInstallmentCount() != null ) {
				if(customerCafVO.getInstallmentCount().equalsIgnoreCase("0")) {
					onuPrice = onuPrice.add(onucharges.getUpFrontCharges());
					onuPriceTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.ONU_COST_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
					cpeTaxAmt = cpeTaxAmt.add((onuPrice.multiply(new BigDecimal(onuPriceTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(onuPriceTax[1].toString()));
				} else {
					onuPrice = onuPrice.add(onucharges.getUpFrontCharges());
					onuPriceTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.ONU_UPFRONT_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
					cpeTaxAmt = cpeTaxAmt.add((onuPrice.multiply(new BigDecimal(onuPriceTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(onuPriceTax[1].toString()));
				}
			}
			if (!customerCafVO.getIptvBoxList().isEmpty() && customerCafVO.getIptvBoxList().size() > 0) {
				for (IPTVBoxVO iPTVBoxVO : customerCafVO.getIptvBoxList()) {
					STBChargesVO stbChargesVO = new STBChargesVO();
					if (iPTVBoxVO.getStbSerialNo() != null && !iPTVBoxVO.getStbSerialNo().isEmpty()) {
						stbcharges = this.getAlCpeChargesByCpeModel(iPTVBoxVO.getStbSerialNo());
						cpeChrg = cpeChrg.add(new BigDecimal(stbcharges.getUpFrontCharges().toString()));
						if (iPTVBoxVO.getStbInstallmentCount() != null && iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
							BigDecimal stbPrice = stbcharges.getUpFrontCharges();
							stbPriceTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.IPTV_COST_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
							BigDecimal stbTaxAmt = (stbPrice.multiply(new BigDecimal(stbPriceTax[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(stbPriceTax[1].toString()));
							stbChargesVO.setStbInstallmentPrice(stbEmiPrice.toString());
							stbChargesVO.setStbMacAddress(iPTVBoxVO.getMacAddress());
							stbChargesVO.setStbModel(cPEInformationDao.getCpeModelByProfileId(Integer.parseInt(iPTVBoxVO.getStbModel())));
							stbChargesVO.setStbPrice(stbPrice.toString());
							stbChargesVO.setStbSrlNo(iPTVBoxVO.getStbSerialNo());
							stbChargesVO.setStbTax(stbTaxAmt.toString());
							stbChargesVO.setStbPackages(iPTVBoxVO.getIptvSrvcCodes());
							stbChargesList.add(stbChargesVO);
							totalCharge = totalCharge.add(new BigDecimal(stbPrice.toString())).add(new BigDecimal(stbTaxAmt.toString()));
						} else if (iPTVBoxVO.getStbInstallmentCount() != null && !iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
							BigDecimal stbPrice = stbcharges.getUpFrontCharges();
							stbPriceTax = cafService.getCafChargeTaxPecentage(ComsEnumCodes.IPTV_UPFRONT_CODE.getCode(), customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
							BigDecimal stbTaxAmt = (stbPrice.multiply(new BigDecimal(stbPriceTax[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(stbPriceTax[1].toString()));
							stbChargesVO.setStbInstallmentPrice(stbEmiPrice.toString());
							stbChargesVO.setStbMacAddress(iPTVBoxVO.getMacAddress());
							stbChargesVO.setStbModel(cPEInformationDao.getCpeModelByProfileId(Integer.parseInt(iPTVBoxVO.getStbModel())));
							stbChargesVO.setStbPrice(stbPrice.toString());
							stbChargesVO.setStbSrlNo(iPTVBoxVO.getStbSerialNo());
							stbChargesVO.setStbTax(stbTaxAmt.toString());
							stbChargesVO.setStbPackages(iPTVBoxVO.getIptvSrvcCodes());
							stbChargesList.add(stbChargesVO);
							totalCharge = totalCharge.add(new BigDecimal(stbPrice.toString())).add(new BigDecimal(stbTaxAmt.toString()));
						} else {
							stbChargesVO.setStbInstallmentPrice(stbEmiPrice.toString());
							stbChargesVO.setStbMacAddress(iPTVBoxVO.getMacAddress());
							stbChargesVO.setStbModel(cPEInformationDao.getCpeModelByProfileId(Integer.parseInt(iPTVBoxVO.getStbModel())));
							stbChargesVO.setStbPrice("0");
							stbChargesVO.setStbSrlNo(iPTVBoxVO.getStbSerialNo());
							stbChargesVO.setStbTax("0");
							stbChargesVO.setStbPackages(iPTVBoxVO.getIptvSrvcCodes());
							stbChargesList.add(stbChargesVO);
						}
					}
				}
			}
			cpeInformationVO.setCpeExtCableChrg(cpeExtCablePrice.toString());
			cpeInformationVO.setCpeInstChrg(cpeInstallationPrice.toString());
			cpeInformationVO.setCpePrice(onuPrice.toString());
			cpeInformationVO.setCpeTaxAmt(cpeTaxAmt.toString());
			cpeInformationVO.setCpeRentalPrice(onuEmiPrice.toString());
			cpeInformationVO.setCpeModel(cPEInformationDao.getCpeModelByProfileId(Integer.parseInt(customerCafVO.getCpeModal())));
			cpeInformationVO.setCpeMacAddress(customerCafVO.getOnuMacAddress());
			cpeInformationVO.setCpeSrlNo(customerCafVO.getCpeId());
			totalCharge = totalCharge.add(new BigDecimal(onuPrice.toString())).add(new BigDecimal(cpeTaxAmt.toString())).add(new BigDecimal(cpeInstallationPrice.toString())).add(new BigDecimal(cpeExtCablePrice.toString())).add(new BigDecimal(onuEmiPrice.toString()));
			cpeInformationVO.setTotalCharge(totalCharge.toString());
			cpeInformationVO.setStbChargesList(stbChargesList);
			cpeInformationVO.setCpeCharge(cpeChrg.toString());
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getCpeChargesInformation()" + e);
			e.printStackTrace();
		} finally {
			onuPrice = null;
			onuEmiPrice = null;
			cpeInstallationPrice = null;
			cpeExtCablePrice = null;
			cpeTaxAmt = null;
			stbEmiPrice = null;
			onucharges = null;
			stbcharges = null;
			instTax = null;
			extraCabelTax = null;
			onuPriceTax = null;
			stbPriceTax = null;
		}
		return cpeInformationVO;
	}

	@Override
	public List<Cpecharges> getAlCpeCharges(String regionCode) {
		List<Cpecharges> cpechargesList = new ArrayList<>();
		List<Cpecharges> cpechargesObjectList = new ArrayList<>();
		BigDecimal cpeCost = new BigDecimal("0");
		BigDecimal cpeTax = new BigDecimal("0");
		Object[] instTax = null;
		BigDecimal stbCost = new BigDecimal("0");
		BigDecimal stbTax = new BigDecimal("0");
		BigDecimal instCost = new BigDecimal("0");
		BigDecimal installationTax = new BigDecimal("0");
		Cpecharges cpechargesObject = null;
		try {
			cpechargesList = cPEInformationDao.getAlCpeCharges();
			for(Cpecharges cpecharges : cpechargesList) {
				cpechargesObject = new Cpecharges();
				cpeCost = new BigDecimal("0");
				cpeTax = new BigDecimal("0");
				instTax = null;
				stbCost = new BigDecimal("0");
				stbTax = new BigDecimal("0");
				instCost = new BigDecimal("0");
				installationTax = new BigDecimal("0");
				
				instCost = cpecharges.getInstcharges();
				instTax = cafService.getCpeChargeDetailsForMobileApp(ComsEnumCodes.CPE_INSTALLATION_CODE.getCode(), regionCode);
				installationTax = installationTax.add((instCost.multiply(new BigDecimal(instTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(instTax[1].toString()));
				
				cpeCost = cpecharges.getUpFrontCharges();
				instTax = cafService.getCpeChargeDetailsForMobileApp(ComsEnumCodes.ONU_COST_CODE.getCode(), regionCode);
				cpeTax = cpeTax.add((cpeCost.multiply(new BigDecimal(instTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(instTax[1].toString()));
				
				stbCost = cpecharges.getUpFrontCharges();
				instTax = cafService.getCpeChargeDetailsForMobileApp(ComsEnumCodes.IPTV_COST_CODE.getCode(), regionCode);
				stbTax = stbTax.add((stbCost.multiply(new BigDecimal(instTax[0].toString()))).divide(new BigDecimal("100"))).add(new BigDecimal(instTax[1].toString()));
				
				cpechargesObject.setCpeModal(cpecharges.getCpeModal());
				cpechargesObject.setCustCost(cpecharges.getCustCost());
				cpechargesObject.setUpFrontCharges(cpecharges.getUpFrontCharges());
				cpechargesObject.setCustRent(cpecharges.getCustRent());
				cpechargesObject.setEffectiveFrom(cpecharges.getEffectiveFrom());
				cpechargesObject.setEffectiveTo(cpecharges.getEffectiveTo());
				cpechargesObject.setEmiAmount(cpecharges.getEmiAmount());
				cpechargesObject.setEmiCount(cpecharges.getEmiCount());
				cpechargesObject.setInstallationTaxAmount(installationTax.toString());
				cpechargesObject.setInstcharges(cpecharges.getInstcharges());
				cpechargesObject.setIptvTaxAmount(stbTax.toString());
				cpechargesObject.setOnuTaxAmount(cpeTax.toString());
				cpechargesObject.setProfileId(cpecharges.getProfileId());
				cpechargesObject.setPurchaseCost(cpecharges.getPurchaseCost());
				cpechargesObjectList.add(cpechargesObject);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAlCpeCharges()" + e);
			e.printStackTrace();
		} finally {
			cpechargesList = null;
			cpeCost = null;
			cpeTax = null;
			instTax = null;
			stbCost = null;
			stbTax = null;
			instCost = null;
			installationTax = null;
			cpechargesObject = null;
		}
		return cpechargesObjectList;
	}

	@Override
	public List<Villages> getAllVillages(String subStationCodes) {
		List<Villages> villagesList = new ArrayList<>();
		try {
			villagesList = cPEInformationDao.getAllVillages(subStationCodes);
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllVillages()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return villagesList;
	}

	@Override
	public List<Villages> getVillagesByDistrictAndMandal(Integer districtId, Integer mandalId) {
		List<Villages> villagesList = new ArrayList<>();
		try {
			villagesList = cPEInformationDao.getVillagesByDistrictAndMandal(districtId, mandalId);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getVillagesByDistrictAndMandal()" + e);
			e.printStackTrace();
		} finally {

		}
		return villagesList;
	}

	@Override
	public Villages getVillagesByVillageId(Integer villageSlno, Integer mandalSlno, Integer districtUid) {
		Villages villages = new Villages();
		try {
			villages = cPEInformationDao.getVillagesByVillageId(villageSlno, mandalSlno, districtUid);
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getVillagesByVillageId()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return villages;
	}

	@Override
	public List<FeatureParams> getAllFeatureParams() {
		List<FeatureParams> featureParamsList = new ArrayList<>();
		try {
			featureParamsList = cPEInformationDao.getAllFeatureParams();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllFeatureParams()" + e);
			e.printStackTrace();
		} finally {

		}
		return featureParamsList;
	}

	@Override
	public List<SrvcFeatures> getAllSrvcFeatures() {
		List<SrvcFeatures> SrvcFeaturesList = new ArrayList<>();
		try {
			SrvcFeaturesList = cPEInformationDao.getAllSrvcFeatures();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllSrvcFeatures()" + e);
			e.printStackTrace();
		} finally {

		}
		return SrvcFeaturesList;
	}

	@Override
	public CpeChargeDetailsBO getCpeAndIptvbosSrlNoCheck(String cpeId, String lmoCode, String lmoAgrmntMspCodes,String cpetypelov) {
		CpeChargeDetailsBO cpeChargeDetailsBO = new CpeChargeDetailsBO();
		try {
			cpeChargeDetailsBO = cPEInformationDao.getCpeAndIptvbosSrlNoCheck(cpeId, lmoCode,lmoAgrmntMspCodes,cpetypelov);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllSrvcFeatures()" + e);
			e.printStackTrace();
		} finally {

		}
		return cpeChargeDetailsBO;
	}

	@Override
	public List<CpeStockVO> getAllCpeStockByLmoCode(String lmoCode) {
		List<CpeStockVO> cpeStockList = new ArrayList<>();
		List<Object[]> cpeStockLists = new ArrayList<>();
		CpeStockVO cpestockvo = null;
		try {
			cpeStockLists = cPEInformationDao.getAllCpeStockByLmoCode(lmoCode);
			for (Object[] cpestock : cpeStockLists) {
				cpestockvo = new CpeStockVO();
				cpestockvo.setCpeSrlno(cpestock[0].toString());
				cpestockvo.setProfileId(cpestock[1].toString());
				cpestockvo.setMspCode(cpestock[2] == null ? "" : cpestock[2].toString());
				cpestockvo.setLmoCode(cpestock[3].toString());
				cpestockvo.setMacAddress(cpestock[4].toString());
				cpeStockList.add(cpestockvo);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllCpeStockByLmoCode()" + e);
			e.printStackTrace();
		} finally {
			cpeStockLists = null;
			cpestockvo = null;
		}
		return cpeStockList;
	}

	@Override
	public CustomerCafModifyVo getCafDetailsForEdit(Long cafNo, String status) {
		CustomerCafModifyVo custCafResult = new CustomerCafModifyVo();
		Object[] custcafData = null;
		try {
			custcafData = cPEInformationDao.getCafDetailsForEdit(cafNo, status);
			custCafResult.setCafno(custcafData[0] == null ? null : custcafData[0].toString());
			custCafResult.setAadharno(custcafData[1] == null ? null : custcafData[1].toString());
			custCafResult.setTitlelov(custcafData[2] == null ? null : custcafData[2].toString());
			custCafResult.setFname(custcafData[3] == null ? null : custcafData[3].toString());
			custCafResult.setMname(custcafData[4] == null ? null : custcafData[4].toString());
			custCafResult.setLname(custcafData[5] == null ? null : custcafData[5].toString());
			custCafResult.setFhname(custcafData[6] == null ? null : custcafData[6].toString());
			custCafResult.setActualdob(custcafData[7] == null ? null : custcafData[7].toString());
			custCafResult.setGender(custcafData[8] == null ? null : custcafData[8].toString());
			custCafResult.setEmail1(custcafData[9] == null ? null : custcafData[9].toString());
			custCafResult.setPocName(custcafData[10] == null ? null : custcafData[10].toString());
			custCafResult.setBillfreqLov(custcafData[11] == null ? null : custcafData[11].toString());
			custCafResult.setInst_addr1(custcafData[12] == null ? null : custcafData[12].toString());
			custCafResult.setInst_addr2(custcafData[13] == null ? null : custcafData[13].toString());
			custCafResult.setInst_locality(custcafData[14] == null ? null : custcafData[14].toString());
			custCafResult.setInst_district(custcafData[15] == null ? null : custcafData[15].toString());
			custCafResult.setInst_mandal(custcafData[16] == null ? null : custcafData[16].toString());
			custCafResult.setInst_city_village(custcafData[17] == null ? null : custcafData[17].toString());
			custCafResult.setInst_pin(custcafData[18] == null ? null : custcafData[18].toString());
			custCafResult.setPocMob1(custcafData[19] == null ? null : custcafData[19].toString());
			custCafResult.setPocMob2(custcafData[20] == null ? null : custcafData[20].toString());
			custCafResult.setStdCode(custcafData[21] == null ? null : custcafData[21].toString());
			custCafResult.setLandLine1(custcafData[22] == null ? null : custcafData[22].toString());
			custCafResult.setLongitude(custcafData[23] == null ? null : custcafData[23].toString());
			custCafResult.setLattitude(custcafData[24] == null ? null : custcafData[24].toString());
			custCafResult.setCpeplace(custcafData[25] == null ? null : custcafData[25].toString());
			custCafResult.setStatus(custcafData[26] == null ? null : custcafData[26].toString());
			custCafResult.setCust_id(custcafData[27] == null ? null : custcafData[27].toString());
			custCafResult.setPopSubStnCode(custcafData[28] == null ? null : custcafData[28].toString());
			custCafResult.setCafStatus(custcafData[29] == null ? null : custcafData[29].toString());
			custCafResult.setPopDistrict(custcafData[30] == null ? null : custcafData[30].toString());
			custCafResult.setPopMandal(custcafData[31] == null ? null : custcafData[31].toString());
			custCafResult.setPopDistrictName(custcafData[32] == null ? null : custcafData[32].toString());
			custCafResult.setPopMandalName(custcafData[33] == null ? null : custcafData[33].toString());
			custCafResult.setPopName(custcafData[34] == null ? null : custcafData[34].toString());
			custCafResult.setInst_districtName(custcafData[35] == null ? null : custcafData[35].toString());
			custCafResult.setInst_mandalName(custcafData[36] == null ? null : custcafData[36].toString());
			custCafResult.setInst_city_villageName(custcafData[37] == null ? null : custcafData[37].toString());
			custCafResult.setApsflUniqueId(custcafData[38] == null ? null : custcafData[38].toString());
			custCafResult.setContactPersonName(custcafData[39] == null ? null : custcafData[39].toString());
			custCafResult.setContactPersonEmail(custcafData[40] == null ? null : custcafData[40].toString());
			custCafResult.setContactPersonMobileNo(custcafData[41] == null ? null : custcafData[41].toString());
			custCafResult.setCustType(custcafData[42] == null ? null : custcafData[42].toString());
		} catch (Exception ex) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getCafDetailsForEdit" + ex);
			ex.printStackTrace();
		} finally {
			custcafData = null;
		}
		return custCafResult;
	}

	@Override
	public List<CpeStockVO> getAllCpeStockByMspCode(String mspCode) {
		List<CpeStockVO> cpeStockList = new ArrayList<>();
		List<Object[]> cpeStockLists = new ArrayList<>();
		CpeStockVO cpestockvo = null;
		try {
			cpeStockLists = cPEInformationDao.getAllCpeStockByMspCode(mspCode);
			for (Object[] cpestock : cpeStockLists) {
				cpestockvo = new CpeStockVO();
				cpestockvo.setCpeSrlno(cpestock[0] == null ? null : cpestock[0].toString());
				cpestockvo.setProfileId(cpestock[1] == null ? null : cpestock[1].toString());
				cpestockvo.setMspCode(cpestock[2] == null ? null : cpestock[2].toString());
				cpestockvo.setLmoCode(cpestock[3] == null ? null : cpestock[3].toString());
				cpestockvo.setMacAddress(cpestock[4] == null ? null : cpestock[4].toString());
				cpeStockList.add(cpestockvo);
			}
		} catch (Exception ex) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getCafDetailsForEdit" + ex);
			ex.printStackTrace();
		} finally {
			cpeStockLists = null;
			cpestockvo = null;
		}
		return cpeStockList;
	}

	@Override
	public Districts getDistrictByDistrictId(Integer districtId) {
		Districts districts = new Districts();
		try {
			districts = cPEInformationDao.getDistrictByDistrictId(districtId);
		} catch(Exception e) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getDistrictByDistrictId" + e);
			e.printStackTrace();
		} finally {
			
		}
		return districts;
	}

	@Override
	public List<Villages> getAllVillagesByTenantCode(String tenantCode) {
		List<Villages> villagesList = new ArrayList<>();
		Villages villagesObject = new Villages();
		String villages = "";
		String[] splitVillages = null;
		String[] village = null;
		try {
			villages = cPEInformationDao.getAllVillagesByTenantCode(tenantCode);
			splitVillages = villages.split(",");
			for(String values : splitVillages) {
				village = values.split(java.util.regex.Pattern.quote("^"));
				villagesObject = cPEInformationDao.getVillagesByVillageId(Integer.parseInt(village[2].toString()), Integer.parseInt(village[1].toString()), Integer.parseInt(village[0].toString()));
				villagesList.add(villagesObject);
			}
		} catch(Exception e) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getAllVillagesByTenantCode" + e);
			e.printStackTrace();
		} finally {
			villagesObject = null;
			villages = null;
			splitVillages = null;
			village = null;
		}
		return villagesList;
	}

	@Override
	public List<TenantBusinessAreas> getAllVillagesBySubstnId(String subStnSlno, String tenantCode) {
		List<TenantBusinessAreas> tenantBusinessAreasList = new ArrayList<>();
		List<Object[]> villagesList = new ArrayList<>();
		TenantBusinessAreas tenantBusinessAreas = null;
		try {
			villagesList = cPEInformationDao.getAllVillagesBySubstnId(subStnSlno, tenantCode);
			for(Object[] object : villagesList) {
				tenantBusinessAreas = new TenantBusinessAreas();
				tenantBusinessAreas.setDistrictUid(object[0].toString());
				tenantBusinessAreas.setMandalSlno(object[1].toString());
				tenantBusinessAreas.setVillageSlno(object[2].toString());
				tenantBusinessAreas.setSubstnUid(object[3].toString());
				tenantBusinessAreas.setVillageName(object[4].toString());
				tenantBusinessAreas.setMandalName(object[5].toString());
				tenantBusinessAreas.setDistrictName(object[6].toString());
				tenantBusinessAreas.setRegion(object[7].toString());
				tenantBusinessAreas.setStdCode(object[8].toString());
				tenantBusinessAreasList.add(tenantBusinessAreas);
			}
		} catch(Exception e) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getAllVillagesBySubstnId" + e);
			e.printStackTrace();
		} finally {
			villagesList = null;
			tenantBusinessAreas = null;
		}
		return tenantBusinessAreasList;
	}

	@Override
	public SplitterVO getOLTPortSplitterData(String oltSrlNo, String lmoCode, Integer oltPort, String l1slot) {
		String splitterData = "";
		SplitterVO splitterVO = new SplitterVO();
		List<String> l1SpltterList = new ArrayList<>();
		List<Integer> l2SpltterList = new ArrayList<>();
		List<Integer> l3SpltterList = new ArrayList<>();
		String[] splitter1List = null;
		String[] splitter2List = null;
		try {
			splitterData = cPEInformationDao.getOLTPortSplitterData(oltSrlNo, lmoCode, oltPort);
			splitter1List = splitterData.split(",");
			if(!l1slot.equalsIgnoreCase("null")) {
				for (int i = 0; i < splitter1List.length; i++) {
					if(splitter1List[i].toString().contains("-")) {
						splitter2List = splitter1List[i].split("-");
						if(l1slot.equalsIgnoreCase(splitter2List[0])) {
							for (int j = 1; j <= Integer.parseInt(splitter2List[1]); j++) {
								l2SpltterList.add(j);
							}
							for (int k = 1; k <= Integer.parseInt(splitter2List[2]); k++) {
								l3SpltterList.add(k);
							}
						}
					}
				}
			} else {
				for (int i = 0; i < splitter1List.length; i++) {
					if(splitter1List[i].toString().contains("-")) {
						splitter2List = splitter1List[i].split("-");
						l1SpltterList.add(splitter2List[0]);
					}
				}
			}
			splitterVO.setLevel1SlotList(l1SpltterList);
			splitterVO.setLevel2SlotList(l2SpltterList);
			splitterVO.setLevel3SlotList(l3SpltterList);
		} catch(Exception e) {
			logger.error("The Exception is :: CPEInofrmationSerImpl :: getOLTPortSplitterData" + e);
			e.printStackTrace();
		} finally {
			splitterData = null;
			l1SpltterList = null;
			l2SpltterList = null;
			l3SpltterList = null;
			splitter1List = null;
			splitter2List = null;
		}
		return splitterVO;
	}

	@Override
	public List<VPNSrvcNames> getVPNServicesByPopIdAndOltSrlNo(String oltSrlNo, String subStnId) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>();
		try {
			vpnSrvcNamesList = cPEInformationDao.getVPNServicesByPopIdAndOltSrlNo(oltSrlNo, subStnId);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getVPNServicesByPopIdAndOltSrlNo()" + e);
			e.printStackTrace();
		} finally {
		}
		return vpnSrvcNamesList;
	}

	@Override
	public List<OLT> getOLTSLNOAndSubstationsrlno(String substnUid, String oltSerialNo) {
		List<OLT> oltList = new ArrayList<>();
		try {
			oltList = cPEInformationDao.getOLTSLNOAndSubstationsrlno(substnUid, oltSerialNo);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getOLTSLNOAndSubstationsrlno()" + e);
			e.printStackTrace();
		} finally {
		}
		return oltList;
	}

	@Override
	public List<VPNSrvcNames> getLMOVPNSrvcNamesList(String subStnId) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>();
		try {
			String substnCode = FileUtil.mspCodeSplitMethod(subStnId);
			vpnSrvcNamesList = cPEInformationDao.getLMOVPNSrvcNamesList(substnCode);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getVPNServicesByPopIdAndOltSrlNo()" + e);
			e.printStackTrace();
		} finally {
		}
		return vpnSrvcNamesList;
	}

	@Override
	public List<Districts> getSILMODistricts(String tenantcode) {
		List<Districts> districtsList = new ArrayList<>();
		try {
			List<String> districtsObjectList = cPEInformationDao.getSILMODistricts(tenantcode);
			for(String districtObj : districtsObjectList) {
				Districts district = new Districts();
				district.setDistrictName(districtObj);
				districtsList.add(district);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return districtsList;
	}

	@Override
	public List<Mandals> getSILMOMandals(String tenantcode, String district) {
		List<Mandals> mandalsList = new ArrayList<>();
		try {
			List<String> mandalsObjectList = cPEInformationDao.getSILMOMandals(tenantcode, district);
			for(String mandalObj : mandalsObjectList) {
				Mandals mandal = new Mandals();
				mandal.setMandalName(mandalObj);
				mandalsList.add(mandal);
			}
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return mandalsList;
	}

	@Override
	public Cpecharges getCpeEmiCountByCpeModel(Long profileId) {
		Cpecharges cpecharges = new Cpecharges();
		try {
			cpecharges = cPEInformationDao.getCpeEmiCountByCpeModel(profileId);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getCpeEmiCountByCpeModel()" + e);
			e.printStackTrace();
		} finally {
		}
		return cpecharges;
	}
	
	@Override
	public List<VillagesBO> getAllVillagesByDistrictId(String districtId) {
		List<VillagesBO> villagesList = new ArrayList<>();
		try {
			villagesList = cPEInformationDao.getAllVillagesByDistrictId(districtId);
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllVillagesByDistrictId()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return villagesList;
	}

	@Override
	public List<SubstationsBO> getAllSubstationsByDistrictId(String districtId) {
		List<SubstationsBO> substationsList = new ArrayList<>();
		try {
			substationsList = cPEInformationDao.getAllSubstationsByDistrictId(districtId);
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllSubstationsByDistrictId()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return substationsList;
	}

	@Override
	public List<OLTPortDetails> getAllOLTPortDetailsByTenantCode(String tenantCode) {
		List<OLTPortDetails> oltPortDetailsList = new ArrayList<>();
		try {
			List<Object[]> oltPortDetailsObjList = cPEInformationDao.getAllOLTPortDetailsByTenantCode(tenantCode);
			for(Object[] object : oltPortDetailsObjList) {
				OLTPortDetails oltPortDetails = new OLTPortDetails();
				oltPortDetails.setPopOltSerialno(object[0].toString());
				oltPortDetails.setPortNo(Integer.parseInt(object[1].toString()));
				oltPortDetails.setLmocode(object[2].toString());
				oltPortDetailsList.add(oltPortDetails);
			}
		} catch(Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllOLTPortDetailsByTenantCode()" + e);
			e.printStackTrace();
		} finally {
			
		}
		return oltPortDetailsList;
	}

	@Override
	public List<VillagesBO> getAllVillages() {
		List<VillagesBO> villagesList = new ArrayList<>();
		try {
			villagesList = cPEInformationDao.getAllVillages();
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllVillages()" + e);
			e.printStackTrace();
		} finally {

		}
		return villagesList;
	}

	@Override
	public List<TenantsBO> getAllLMOByDistrictAndMandalAndVillage(String district, String mandal, String village) {
		List<TenantsBO> tenantsList = new ArrayList<>();
		try {
			tenantsList = cPEInformationDao.getAllLMOByDistrictAndMandalAndVillage(district, mandal, village);
		} catch (Exception e) {
			logger.error("CPEInformationServiceImpl :: getAllLMOByDistrictAndMandalAndVillage()" + e);
			e.printStackTrace();
		} finally {

		}
		return tenantsList;
	}
}
