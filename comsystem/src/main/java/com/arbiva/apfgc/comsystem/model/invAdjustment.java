package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "invadj")
@IdClass(invAdjustmentPK.class)
public class invAdjustment implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "adjrefid")
	private Long adjrefId;
	
	@Id
	@Column(name = "adjmm")
	private int adjmm;
	
	@Column(name = "adjyyyy")
	private Integer adjyyyy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "adjdate")
	private Date adjdate;
	
	@Column(name = "adjdesc")
	private String adjdesc;
	
	@Column(name = "custdistuid")
	private Integer custdistuid;
	
	@Column(name = "pmntcustid")
	private Long pmntcustid;
	
	@Column(name = "acctcafno")
	private Long acctcafno;
	
	@Column(name = "invdtlid")
	private Long invdtlid;
	
	@Column(name = "cafinvno")
	private Long cafinvno;
	
	@Column(name = "custinvno")
	private Long custinvno;
	
	@Column(name = "tenantcode")
	private String tenantcode;
	
	@Column(name = "prodcode")
	private String prodcode;
	
	@Column(name = "srvccode")
	private String srvccode;
	
	@Column(name = "featurecode")
	private String featurecode;
	
	@Column(name = "prodcafno")
	private Long prodcafno;
	
	@Column(name = "stbcafno")
	private Long stbcafno;
	
	@Column(name = "chargecode")
	private String chargecode;
	
	@Column(name = "applytaxflag")
	private int applytaxflag;
	
	@Column(name = "chargeamt")
	private float chargeamt;
	
	@Column(name = "srvctax")
	private float srvctax;
	
	@Column(name = "swatchtax")
	private float swatchtax;
	
	@Column(name = "kisantax")
	private float kisantax;
	
	@Column(name = "enttax")
	private float enttax;
	
	@Column(name = "status")
	private Integer status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdon")
	private Calendar createdon;
	
	@Column(name = "createdby")
	private String createdby;
	
	@Column(name = "createdipaddr")
	private String createdipaddr;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approvedon")
	private Calendar approvedon;
	
	@Column(name = "approvededby")
	private String approvededby;
	
	@Column(name = "approvedipaddr")
	private String approvedipaddr;

	public Long getAdjrefId() {
		return adjrefId;
	}

	public void setAdjrefId(Long adjrefId) {
		this.adjrefId = adjrefId;
	}

	public int getAdjmm() {
		return adjmm;
	}

	public void setAdjmm(int adjmm) {
		this.adjmm = adjmm;
	}

	public Integer getAdjyyyy() {
		return adjyyyy;
	}

	public void setAdjyyyy(Integer adjyyyy) {
		this.adjyyyy = adjyyyy;
	}

	public Date getAdjdate() {
		return adjdate;
	}

	public void setAdjdate(Date adjdate) {
		this.adjdate = adjdate;
	}

	public String getAdjdesc() {
		return adjdesc;
	}

	public void setAdjdesc(String adjdesc) {
		this.adjdesc = adjdesc;
	}

	public Integer getCustdistuid() {
		return custdistuid;
	}

	public void setCustdistuid(Integer custdistuid) {
		this.custdistuid = custdistuid;
	}

	public Long getPmntcustid() {
		return pmntcustid;
	}

	public void setPmntcustid(Long pmntcustid) {
		this.pmntcustid = pmntcustid;
	}

	public Long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(Long acctcafno) {
		this.acctcafno = acctcafno;
	}

	public Long getInvdtlid() {
		return invdtlid;
	}

	public void setInvdtlid(Long invdtlid) {
		this.invdtlid = invdtlid;
	}

	public Long getCafinvno() {
		return cafinvno;
	}

	public void setCafinvno(Long cafinvno) {
		this.cafinvno = cafinvno;
	}

	public Long getCustinvno() {
		return custinvno;
	}

	public void setCustinvno(Long custinvno) {
		this.custinvno = custinvno;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getSrvccode() {
		return srvccode;
	}

	public void setSrvccode(String srvccode) {
		this.srvccode = srvccode;
	}

	public String getFeaturecode() {
		return featurecode;
	}

	public void setFeaturecode(String featurecode) {
		this.featurecode = featurecode;
	}

	public Long getProdcafno() {
		return prodcafno;
	}

	public void setProdcafno(Long prodcafno) {
		this.prodcafno = prodcafno;
	}

	public Long getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(Long stbcafno) {
		this.stbcafno = stbcafno;
	}

	public String getChargecode() {
		return chargecode;
	}

	public void setChargecode(String chargecode) {
		this.chargecode = chargecode;
	}

	public int getApplytaxflag() {
		return applytaxflag;
	}

	public void setApplytaxflag(int applytaxflag) {
		this.applytaxflag = applytaxflag;
	}

	public float getChargeamt() {
		return chargeamt;
	}

	public void setChargeamt(float chargeamt) {
		this.chargeamt = chargeamt;
	}

	public float getSrvctax() {
		return srvctax;
	}

	public void setSrvctax(float srvctax) {
		this.srvctax = srvctax;
	}

	public float getSwatchtax() {
		return swatchtax;
	}

	public void setSwatchtax(float swatchtax) {
		this.swatchtax = swatchtax;
	}

	public float getKisantax() {
		return kisantax;
	}

	public void setKisantax(float kisantax) {
		this.kisantax = kisantax;
	}

	public float getEnttax() {
		return enttax;
	}

	public void setEnttax(float enttax) {
		this.enttax = enttax;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Calendar getApprovedon() {
		return approvedon;
	}

	public void setApprovedon(Calendar approvedon) {
		this.approvedon = approvedon;
	}

	public String getApprovededby() {
		return approvededby;
	}

	public void setApprovededby(String approvededby) {
		this.approvededby = approvededby;
	}

	public String getApprovedipaddr() {
		return approvedipaddr;
	}

	public void setApprovedipaddr(String approvedipaddr) {
		this.approvedipaddr = approvedipaddr;
	}
	
	

}
