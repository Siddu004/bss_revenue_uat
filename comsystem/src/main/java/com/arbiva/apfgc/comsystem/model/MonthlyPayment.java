/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class MonthlyPayment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mobileNo;
	
	private String loginId;
	
	private String tenantCode;
	
	private String tenantName;
	
	private int offset;
	
	private String from;
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String effectivefrom) {
		this.from = effectivefrom;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String effectiveto) {
		this.to = effectiveto;
	}

	private String to;

// new code

public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

private String cafNo;

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}


	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	
}
