package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "custcafhist")
public class CustCafHist {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "CHANGEID")
	private Long changeid;
	
	@Column(name = "CUSTID")
	private String custid;
	
	@Column(name = "ENTUNITID")
	private String entunitid;
	
	@Column(name = "ACCTCAFNO")
	private Long acctcafno;

	@Column(name = "TENANTCODE")
	private String tenantcode;
	
	@Column(name = "CHANGEDTLS")
	private String changedtls;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIEDON")
	protected Calendar modifiedOn;

	@Column(name = "MODIFIEDBY")
	protected String modifiedBy;

	@Column(name = "MODIFIEDIPADDR")
	protected String modifiedIPAddr;

	public Long getChangeid() {
		return changeid;
	}

	public void setChangeid(Long changeid) {
		this.changeid = changeid;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getEntunitid() {
		return entunitid;
	}

	public void setEntunitid(String entunitid) {
		this.entunitid = entunitid;
	}

	public Long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(Long acctcafno) {
		this.acctcafno = acctcafno;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getChangedtls() {
		return changedtls;
	}

	public void setChangedtls(String changedtls) {
		this.changedtls = changedtls;
	}

	public Calendar getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Calendar modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddr() {
		return modifiedIPAddr;
	}

	public void setModifiedIPAddr(String modifiedIPAddr) {
		this.modifiedIPAddr = modifiedIPAddr;
	}
	
	
	
}

