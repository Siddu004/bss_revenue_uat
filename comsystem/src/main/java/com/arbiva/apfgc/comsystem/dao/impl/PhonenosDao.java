/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.Phonenos;

/**
 * @author Lakshman
 *
 */
@Repository
public class PhonenosDao {

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public Phonenos savePhonenos(Phonenos phonenos) {
		return getEntityManager().merge(phonenos);
	}

	public Phonenos findByPhoneno(String phoneNo) {
		return getEntityManager().find(Phonenos.class, phoneNo);
	}
}
