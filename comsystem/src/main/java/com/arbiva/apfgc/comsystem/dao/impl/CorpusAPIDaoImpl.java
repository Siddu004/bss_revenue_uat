package com.arbiva.apfgc.comsystem.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.BO.AlaCartePackagesBO;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.IptvPkeSales;
import com.arbiva.apfgc.comsystem.model.OlIptvPkeSales;
import com.arbiva.apfgc.comsystem.model.Products;

@Repository
@SuppressWarnings("rawtypes")
public class CorpusAPIDaoImpl {

	private static final Logger logger = Logger.getLogger(CorpusAPIDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	public CafSTBs findParentCafeNoBySubscriberCode(String subscribercode) {

		CafSTBs response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder("select * from cafstbs where nwsubscode = :subsCode");
			query = em.createNativeQuery(builder.toString(),CafSTBs.class);
			query.setParameter("subsCode", subscribercode);
			list = query.getResultList();

			if (list.size() > 0)
				response = (CafSTBs) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}
	
	public OlIptvPkeSales findSubscriberCode(String subscribercode) {

		OlIptvPkeSales response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder("select * from oliptvpkgsales where nwsubscode = :subsCode");
			query = em.createNativeQuery(builder.toString(),OlIptvPkeSales.class);
			query.setParameter("subsCode", subscribercode);
			list = query.getResultList();

			if (list.size() > 0)
				response = (OlIptvPkeSales) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

	@SuppressWarnings({ "unchecked" })
	public Object[] findParentCafeNoAndLmocodeBySubscriberCode(String subscribercode) {

		Object[] response = null;
		Query query = null;
		List<Object[]> list = null;

		try {
			StringBuilder builder = new StringBuilder("select distinct c.lmocode,cs.parentcafno,c.custtypelov,t.tenanttypelov from cafs c, cafstbs cs, tenants t ");
			builder.append(" where cs.nwsubscode = :subsCode and c.cafno = cs.parentcafno AND t.tenantcode = c.lmocode");
			query = em.createNativeQuery(builder.toString());
			query.setParameter("subsCode", subscribercode);
			list = query.getResultList();

			if (list.size() > 0)
				response = list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public BigDecimal findBalanceByParentCafNo(String parentCafNo) {
		BigDecimal response = null;
		Query query = null;
		List<BigDecimal> list = null;

		try {
			StringBuilder builder = new StringBuilder("SELECT cu.regbal+cu.chargedbal AS balance  FROM customermst cu, cafs c "); 
				builder.append(" WHERE c.cafno = '"+parentCafNo+"' AND c.custid = cu.custid");
			query = em.createNativeQuery(builder.toString());
			list = query.getResultList();

			if (list.size() > 0)
				response = list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public Object[] findDistrictCodeAndPmtCustIdByCaf(String parentCafNo) {
		Object[] response = null;
		Query query = null;
		List<Object[]> list = null;

		try {
			StringBuilder builder = new StringBuilder("select custdistuid,pmntcustid from cafs where cafno = :cafNo");
			query = em.createNativeQuery(builder.toString());
			query.setParameter("cafNo", parentCafNo);
			list = query.getResultList();

			if (list.size() > 0)
				response = list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getIptvPkgesBySbscriberCode(String subscribercode) {
		Query query = null;
		List<Object[]> list = null;
		StringBuilder builder = new StringBuilder("");

		builder.append(" select a.prodcode,a.prodname, a.price, a.featurecodes from");
		builder.append(" (select x.prodcode,x.prodname, x.price, s.featurecodes from ");
		builder.append(
				" (select p.prodcode,p.prodname,get_prodcharge(p.tenantcode,p.prodcode,null,null,'1,2,3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') price from products p,");
		builder.append(
				" (select prodcode, GROUP_CONCAT(coresrvccode ORDER BY coresrvccode ASC SEPARATOR ',') as coresrvc from prodcomponents group by prodcode)a");
		builder.append(" where p.prodcode = a.prodcode ");
		builder.append("	and a.coresrvc IN ('IPTV') and current_date() between p.effectivefrom and p.effectiveto");
		builder.append("	and p.custtypelov = 'INDIVIDUAL'");
		builder.append(" AND p.prodcode NOT IN (");
		builder.append(" select distinct prodcode from cafsrvcs ");
		builder.append(
				"	where  parentcafno = (select distinct parentcafno from cafsrvcs where nwsubscode = :subscribercode) ");
		builder.append("	and status in(6)");
		builder.append("	)) x,");
		builder.append(" srvcs s, prodcomponents pc where pc.prodcode = x.prodcode");
		builder.append(" and s.srvccode = pc.componentcode");
		builder.append(" and pc.coresrvccode = s.coresrvccode");
		builder.append(" and current_date() between s.effectivefrom and s.effectiveto )a,");

		builder.append(
				" (select rsp.agruniqueid, rsp.partnercode, rs.prodcode from rsagrpartners rsp, rsagrmnts rs,products p,");
		builder.append(
				" (select prodcode, GROUP_CONCAT(coresrvccode ORDER BY coresrvccode ASC SEPARATOR ',') as coresrvc from prodcomponents group by prodcode)a");
		builder.append("	where partnercode = ");
		builder.append(
				" (select lmocode from cafs where cafno = (select distinct parentcafno from cafsrvcs where nwsubscode = :subscribercode))");
		builder.append(" and rsp.partnertype = 'LMO'");
		builder.append(" and rsp.agruniqueid = rs.agruniqueid ");
		builder.append(" and rs.prodcode = a.prodcode ");
		builder.append(" and a.prodcode = p.prodcode ");
		builder.append(" and a.coresrvc IN ('IPTV') and current_date() between p.effectivefrom and p.effectiveto");
		builder.append(" and p.custtypelov = 'INDIVIDUAL')b");
		builder.append(" where a.prodcode = b.prodcode");

		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("subscribercode", subscribercode);
			list = query.getResultList();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return list;
	}

	public Customer findCustomerCustId(String subscribercode) {
		Customer response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder(
					"select * from customers where custid = (select custid from cafs where cafno  = (select distinct parentcafno from cafstbs where nwsubscode = :subsCode ))");
			query = em.createNativeQuery(builder.toString(), Customer.class);
			query.setParameter("subsCode", subscribercode);
			list = query.getResultList();
			logger.error(query);
			if (list.size() > 0)
				response = (Customer) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}
	
	
	public String findCustomerMobNo(String subscribercode) {
		String response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder(
					"select pocmob1 from customers where custid = (select custid from cafs where cafno  = (select distinct parentcafno from cafstbs where nwsubscode = :subsCode ))");
			query = em.createNativeQuery(builder.toString());
			query.setParameter("subsCode", subscribercode);
			list = query.getResultList();
			logger.error(query);
			if (list.size() > 0)
				response = list.get(0).toString();

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

	public <T> T save(T t) {
		return em.merge(t);
	}

	public OlIptvPkeSales findById(String referenceId) {
		OlIptvPkeSales response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder("select * from oliptvpkgsales where requestid = :referenceId ");
			query = em.createNativeQuery(builder.toString(), OlIptvPkeSales.class);
			query.setParameter("referenceId", referenceId);
			list = query.getResultList();

			if (list.size() > 0)
				response = (OlIptvPkeSales) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

	public String findProdPriceByProdCode(String packageCode) {
		String response = null;
		Query query = null;

		try {
			StringBuilder builder = new StringBuilder(
					"select get_prodcharge(:tenantCode, :prodCode,null,null,'2,3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','')");
			query = em.createNativeQuery(builder.toString());
			query.setParameter("tenantCode", "APSFL");
			query.setParameter("prodCode", packageCode);
			response = query.getSingleResult().toString();

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			query = null;
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findProdChargesByProdCode(String pkgCode, String srvcCode) {
		List<Object[]> list = null;
		Query query = null;
		try {
			StringBuilder builder = new StringBuilder(
					"select pc.componentcode,pc.chargecode,pc.chargeamt from prodcharges pc, chargecodes cc ");
			builder.append(
					"where pc.prodcode = :pkgCode and pc.componentcode = :srvcCode and pc.chargecode = cc.chargecode and cc.chargetypeflag in (2,3)");
			query = em.createNativeQuery(builder.toString());
			query.setParameter("pkgCode", pkgCode.trim());
			query.setParameter("srvcCode", srvcCode.trim());
			list = query.getResultList();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return list;
	}

	public String findAgrmtIdPacakgeCodeAndLmoCode(String pkgCode, String lmoCode) {
		String result = null;
		Query query = null;
		List list = null;
		StringBuilder builder = new StringBuilder("select  max(rs.agruniqueid) from rsagrmnts rs, rsagrpartners rp ");
		builder.append("where rs.agruniqueid = rp.agruniqueid");
		builder.append(" and rp.partnercode = :lmoCode ");
		builder.append(" and rs.prodcode = :pkgCode ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("pkgCode", pkgCode.trim());
			query.setParameter("lmoCode", lmoCode.trim());
			list = query.getResultList();
			if (list.size() > 0)
				result = list.get(0).toString();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return result;
	}

	
	public Products findProdByProdCode(String pkgCode) {
		Products response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder(
					"select * from products where prodcode = :pkgCode and current_date() between effectivefrom and effectiveto ");
			query = em.createNativeQuery(builder.toString(), Products.class);
			query.setParameter("pkgCode", pkgCode);
			list = query.getResultList();

			if (list.size() > 0)
				response = (Products) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public List<AlaCartePackagesBO> getIptvPkgesByParentCafAndLmocode(Object[] parentCafNoAndLmocode) {

		StringBuilder builder = new StringBuilder("");
		builder.append(" SELECT p1.prodcode,p1.prodname, ");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'1,2,3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') price");
		builder.append(" ,(SELECT GROUP_CONCAT(s.featurecodes SEPARATOR ',') FROM  srvcs s, prodcomponents pcom");
		builder.append(" WHERE p1.tenantcode = pcom.tenantcode");
		builder.append(" AND p1.prodcode = pcom.prodcode");
		builder.append(" AND pcom.componenttype = 'S'");
		builder.append(" AND pcom.componentcode = s.srvccode");
		builder.append(" AND current_date() between s.effectivefrom and s.effectiveto ");
		builder.append(" AND pcom.effectivefrom = (SELECT MAX(pc0.effectivefrom) FROM prodcomponents pc0 ");
		builder.append(" WHERE pc0.tenantcode=pcom.tenantcode AND pc0.prodcode=pcom.prodcode ");
		builder.append(" AND pc0.componenttype=pcom.componenttype AND pc0.componentcode=pcom.componentcode ");
		builder.append(" AND pc0.effectivefrom <= CURRENT_DATE()");
		builder.append(" )");
		builder.append(" ) featurecodes,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'1',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Recurring,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'2',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Activation,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Depsoits");
		builder.append(" FROM products p1 ,rsagrpartners prt1,rsagrmnts agr1 ");
		builder.append(" WHERE prt1.agruniqueid=agr1.agruniqueid AND agr1.tenantcode=p1.tenantcode AND agr1.prodcode=p1.prodcode  ");
		builder.append(" AND prt1.partnertype='LMO' AND prt1.partnercode= :lmoCode");
		//Query Change Fastlane
		builder.append(" AND agr1.agrfdate in (SELECT MAX(agr2.agrfdate) FROM rsagrmnts agr2 WHERE agr2.agruniqueid=agr1.agruniqueid AND agr2.agrfdate <= CURRENT_DATE() )");
		builder.append(" AND NOT EXISTS (SELECT 1 FROM cafsrvcs cs2 WHERE cs2.parentcafno= :parentCafNo AND cs2.tenantcode=p1.tenantcode AND cs2.prodcode=p1.prodcode AND IFNULL(cs2.expdate,ADDDATE(CURRENT_DATE(),365)) >= CURRENT_DATE())");
		builder.append(" AND NOT EXISTS (SELECT 1 FROM prodcomponents pc1, srvcs s1 ");
		builder.append(" WHERE pc1.tenantcode=p1.tenantcode AND pc1.prodcode=p1.prodcode  ");
		builder.append("  AND pc1.effectivefrom = (SELECT MAX(pc2.effectivefrom) FROM prodcomponents pc2 ");
		builder.append(" WHERE pc2.tenantcode=pc1.tenantcode AND pc2.prodcode=pc1.prodcode ");
		builder.append("  AND pc2.componenttype=pc1.componenttype AND pc2.componentcode=pc1.componentcode ");
		builder.append("  AND pc2.effectivefrom <= CURRENT_DATE()");
		builder.append(" )");
		builder.append(" AND pc1.componenttype='S' AND pc1.componentcode=s1.srvccode");
		builder.append(" AND CURRENT_DATE() BETWEEN s1.effectivefrom AND s1.effectiveto");
		builder.append(" AND CURRENT_DATE() BETWEEN p1.effectivefrom AND p1.effectiveto");
		builder.append(" AND s1.coresrvccode NOT IN ('IPTV')");
		builder.append(" )");
		builder.append(" AND p1.custtypelov = :custType ");

		List<AlaCartePackagesBO> list = null;
		Query query = null;
		try {
			query = em.createNativeQuery(builder.toString(),AlaCartePackagesBO.class);
			query.setParameter("parentCafNo", parentCafNoAndLmocode[1].toString());
			query.setParameter("lmoCode", parentCafNoAndLmocode[0].toString());
			query.setParameter("custType", parentCafNoAndLmocode[2].toString());
			list = query.getResultList();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<AlaCartePackagesBO> getIptvPkgesByParentCafAndLmocodeForEnterPrise(Object[] parentCafNoAndLmocode) {
		StringBuilder builder = new StringBuilder("");
		builder.append(" SELECT p1.prodcode,p1.prodname, ");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'1,2,3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') price");
		builder.append(" ,(SELECT GROUP_CONCAT(s.featurecodes SEPARATOR ',') FROM  srvcs s, prodcomponents pcom");
		builder.append(" WHERE p1.tenantcode = pcom.tenantcode");
		builder.append(" AND p1.prodcode = pcom.prodcode");
		builder.append(" AND pcom.componenttype = 'S'");
		builder.append(" AND pcom.componentcode = s.srvccode");
		builder.append(" AND current_date() between s.effectivefrom and s.effectiveto ");
		builder.append(" AND pcom.effectivefrom = (SELECT MAX(pc0.effectivefrom) FROM prodcomponents pc0 ");
		builder.append(" WHERE pc0.tenantcode=pcom.tenantcode AND pc0.prodcode=pcom.prodcode ");
		builder.append(" AND pc0.componenttype=pcom.componenttype AND pc0.componentcode=pcom.componentcode ");
		builder.append(" AND pc0.effectivefrom <= CURRENT_DATE()");
		builder.append(" )");
		builder.append(" ) featurecodes,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'1',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Recurring,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'2',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Activation,");
		builder.append(" get_prodcharge(p1.tenantcode, p1.prodcode,null,null,'3',DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') Depsoits");
		builder.append(" FROM products p1 ");
		builder.append(" WHERE NOT EXISTS (SELECT 1 FROM cafsrvcs cs2 WHERE cs2.parentcafno= :parentCafNo AND cs2.tenantcode=p1.tenantcode AND cs2.prodcode=p1.prodcode AND IFNULL(cs2.expdate,ADDDATE(CURRENT_DATE(),365)) >= CURRENT_DATE())");
		builder.append(" AND NOT EXISTS (SELECT 1 FROM prodcomponents pc1, srvcs s1 ");
		builder.append(" WHERE pc1.tenantcode=p1.tenantcode AND pc1.prodcode=p1.prodcode  ");
		builder.append("  AND pc1.effectivefrom = (SELECT MAX(pc2.effectivefrom) FROM prodcomponents pc2 ");
		builder.append(" WHERE pc2.tenantcode=pc1.tenantcode AND pc2.prodcode=pc1.prodcode ");
		builder.append("  AND pc2.componenttype=pc1.componenttype AND pc2.componentcode=pc1.componentcode ");
		builder.append("  AND pc2.effectivefrom <= CURRENT_DATE()");
		builder.append(" )");
		builder.append(" AND pc1.componenttype='S' AND pc1.componentcode=s1.srvccode");
		builder.append(" AND CURRENT_DATE() BETWEEN s1.effectivefrom AND s1.effectiveto");
		builder.append(" AND CURRENT_DATE() BETWEEN p1.effectivefrom AND p1.effectiveto");
		builder.append(" AND s1.coresrvccode NOT IN ('IPTV')");
		builder.append(" )");
		builder.append(" AND p1.custtypelov = :custType ");

		List<AlaCartePackagesBO> list = null;
		Query query = null;
		try {
			query = em.createNativeQuery(builder.toString(),AlaCartePackagesBO.class);
			query.setParameter("parentCafNo", parentCafNoAndLmocode[1].toString());
			query.setParameter("custType", parentCafNoAndLmocode[2].toString());
			list = query.getResultList();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return list;
	}
	
	public IptvPkeSales findParentCafeNoBySubscriberCode(String nwsubscode , String pkgList) {
		IptvPkeSales response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder("select * from iptvpkgsales where nwsubscode = :subsCode and packages LIKE '"+pkgList+"'");
			query = em.createNativeQuery(builder.toString(),IptvPkeSales.class);
			query.setParameter("subsCode", nwsubscode);
			list = query.getResultList();
			if (list.size() > 0)
				response = (IptvPkeSales) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}
	
	public CafServices findCafSrvcsByProdCodeCafNo(Long cafno , String prodcode) {
		CafServices response = null;
		Query query = null;
		List list = null;

		try {
			StringBuilder builder = new StringBuilder("select * from cafsrvcs where status = 2 and cafno = :cafno and prodcode = :prodcode");
			query = em.createNativeQuery(builder.toString(),CafServices.class);
			query.setParameter("cafno", cafno);
			query.setParameter("prodcode", prodcode);
			list = query.getResultList();
			if (list.size() > 0)
				response = (CafServices) list.get(0);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			list = null;
			query = null;
		}
		return response;
	}

}
