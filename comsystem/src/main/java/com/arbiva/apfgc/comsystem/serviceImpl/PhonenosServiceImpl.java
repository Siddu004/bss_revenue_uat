/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.PhonenosDao;
import com.arbiva.apfgc.comsystem.model.Phonenos;
import com.arbiva.apfgc.comsystem.service.PhonenosService;

/**
 * @author Lakshman
 *
 */
@Service
public class PhonenosServiceImpl implements PhonenosService {
	
	private static final Logger logger = Logger.getLogger(PhonenosServiceImpl.class);
	
	@Autowired
	PhonenosDao phonenosDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;

	@Override
	@Transactional
	public Phonenos updatePhonenos(String phoneNo, String loginId, String ipAddress) {
		Phonenos phonenos = new Phonenos();
		try {
			phonenos = this.findByPhoneno(phoneNo);
			phonenos.setAllocatedBy(loginId);
			phonenos.setAllocatedIpaddr(ipAddress != null ? ipAddress : httpServletRequest.getRemoteAddr());
			phonenos.setAllocatedOn(Calendar.getInstance());
			phonenosDao.savePhonenos(phonenos);
		} catch(Exception e) {
			logger.error("The Exception is PhonenosServiceImpl :: updatePhonenos " + e);
			e.printStackTrace();
		} finally {
			
		}
		return phonenos;
	}

	@Override
	public Phonenos findByPhoneno(String phoneNo) {
		Phonenos phonenos = new Phonenos();
		try {
			phonenos = phonenosDao.findByPhoneno(phoneNo);
		} catch (Exception e) {
			logger.error("PhonenosServiceImpl :: findByPhoneno()" + e);
			e.printStackTrace();
		} finally {

		}
		return phonenos;
	}

}
