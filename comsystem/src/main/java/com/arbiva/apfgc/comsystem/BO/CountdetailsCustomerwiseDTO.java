package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CountdetailsCustomerwiseDTO implements Serializable{
	
	private static final long serialVersionUID = 805653405175096337L;
	
	@Id
	@Column(name="cust_type")
	private String cust_type;
	@Id
	@Column(name="Sub_type")
	private String Sub_type;
	@Column(name="No_Connections")
	private String Number_Connections;
	
	
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
	public String getSub_type() {
		return Sub_type;
	}
	public void setSub_type(String sub_type) {
		Sub_type = sub_type;
	}
	public String getNumber_Connections() {
		return Number_Connections;
	}
	public void setNumber_Connections(String number_Connections) {
		Number_Connections = number_Connections;
	}
	
	

}
