package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

public class CafAccountPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long acctcafno;
	
	private Integer districtuid;

	public Long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(Long acctcafno) {
		this.acctcafno = acctcafno;
	}

	public Integer getDistrictuid() {
		return districtuid;
	}

	public void setDistrictuid(Integer districtuid) {
		this.districtuid = districtuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acctcafno == null) ? 0 : acctcafno.hashCode());
		result = prime * result + ((districtuid == null) ? 0 : districtuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafAccountPK other = (CafAccountPK) obj;
		if (acctcafno == null) {
			if (other.acctcafno != null)
				return false;
		} else if (!acctcafno.equals(other.acctcafno))
			return false;
		if (districtuid == null) {
			if (other.districtuid != null)
				return false;
		} else if (!districtuid.equals(other.districtuid))
			return false;
		return true;
	}
	
}
