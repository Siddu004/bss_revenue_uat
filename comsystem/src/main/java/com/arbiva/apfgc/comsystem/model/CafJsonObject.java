/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.List;

/**
 * @author Lakshman
 *
 */
public class CafJsonObject {
	
	private long iTotalRecords;

	private long iTotalDisplayRecords;

	private String sEcho;

	private String sColumns;

	private List<CafJsonData> aaData;
	
	public long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public long getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(long iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public String getsColumns() {
		return sColumns;
	}

	public void setsColumns(String sColumns) {
		this.sColumns = sColumns;
	}

	public List<CafJsonData> getAaData() {
		return aaData;
	}

	public void setAaData(List<CafJsonData> aaData) {
		this.aaData = aaData;
	}

}
