/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;

/**
 * @author Lakshman
 *
 */
public interface CafSrvcPhoneNosService {
	
	public abstract CafSrvcPhoneNos saveCafSrvcPhoneNos(Long cafNo, String loginId, Integer district, Integer mandal, Integer village, String ipAddress, String custType, String noOfTPConn, Long addPkgCafNo, String flag );
}
