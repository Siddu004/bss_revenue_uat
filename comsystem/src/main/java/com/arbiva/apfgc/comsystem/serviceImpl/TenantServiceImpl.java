package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dao.impl.TenantDaoImpl;
import com.arbiva.apfgc.comsystem.model.Tenant;
import com.arbiva.apfgc.comsystem.vo.TenantVO;

@Service
public class TenantServiceImpl {
	
	@Autowired
	TenantDaoImpl tenantDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	private static final Logger LOGGER = Logger.getLogger(TenantDaoImpl.class);
	
	public List<Tenant> findAllTenants() {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		try {
			tenantsList = tenantDao.findAllTenants();
		} catch(Exception e) {
			LOGGER.error("The Exception TenantDaoImpl:: findAllTenants" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenantsList;
	}
	
	public Tenant findByTenantCode(String tenantCode) {
		Tenant tenant = new Tenant();
		try {
			tenant = tenantDao.findByTenantCode(tenantCode);
		} catch(Exception e) {
			LOGGER.error("The Exception TenantDaoImpl:: findByTenantCode" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenant;
	}
	
	public List<Tenant> findMspCodeByTenantType() {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		try {
			tenantsList = tenantDao.findMspCodeByTenantType();
		} catch(Exception e) {
			LOGGER.error("The Exception TenantDaoImpl:: findMspCodeByTenantType" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenantsList;
	}
	
	public Tenant findByTenantId(Integer tenantId) {
		Tenant tenant = new Tenant();
		try {
			tenant = tenantDao.findByTenantId(tenantId);
		} catch(Exception e) {
			LOGGER.error("The Exception TenantDaoImpl:: findByTenantId" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenant;
	}
	
	public TenantVO findTenantDtlsByCafno(Long cafNumber) {
		TenantVO tenantVO=new TenantVO();
		try {
			String lmocode = tenantDao.findTenantDtlsByCafno(cafNumber);
			tenantVO = tenantDao.findTenatInfoByTenantID(lmocode);
		} catch(Exception e) {
			LOGGER.error("The Exception TenantDaoImpl:: findByTenantId" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenantVO;
	}
}
