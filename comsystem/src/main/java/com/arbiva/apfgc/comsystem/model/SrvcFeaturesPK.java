package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

public class SrvcFeaturesPK implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private String coresrvccode;
	
	private String featurecode;

	public String getCoresrvccode() {
		return coresrvccode;
	}

	public void setCoresrvccode(String coresrvccode) {
		this.coresrvccode = coresrvccode;
	}

	public String getFeaturecode() {
		return featurecode;
	}

	public void setFeaturecode(String featurecode) {
		this.featurecode = featurecode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coresrvccode == null) ? 0 : coresrvccode.hashCode());
		result = prime * result + ((featurecode == null) ? 0 : featurecode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SrvcFeaturesPK other = (SrvcFeaturesPK) obj;
		if (coresrvccode == null) {
			if (other.coresrvccode != null)
				return false;
		} else if (!coresrvccode.equals(other.coresrvccode))
			return false;
		if (featurecode == null) {
			if (other.featurecode != null)
				return false;
		} else if (!featurecode.equals(other.featurecode))
			return false;
		return true;
	}

	
}
