/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafSTBs;

/**
 * @author Lakshman
 *
 */
@Repository
public class CafSTBsDao {

	//private static final Logger logger = Logger.getLogger(CafSTBsDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public CafSTBs saveCafSTBs(CafSTBs cafSTBs) {
		return getEntityManager().merge(cafSTBs);
	}
}
