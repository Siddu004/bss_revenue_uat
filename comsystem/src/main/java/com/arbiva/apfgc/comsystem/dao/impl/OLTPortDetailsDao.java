/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.controller.ComsRestController;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;

/**
 * @author Arbiva
 *
 */
@Repository
public class OLTPortDetailsDao {
	
	private static final Logger logger = Logger.getLogger(ComsRestController.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public OLTPortDetails saveOLTPortDetails(OLTPortDetails oLTPortDetails) {
		return getEntityManager().merge(oLTPortDetails);
	}

	public List<OLTPortDetails> getAllOLTPortDetailsByTenantCode(String tenantCode) {
		List<OLTPortDetails> oLTPortDetailsList = new ArrayList<OLTPortDetails>();
		StringBuilder builder = new StringBuilder(" FROM ").append(OLTPortDetails.class.getSimpleName()).append(" WHERE lmocode=:tenantCode and slots <> REPEAT('1', 128) ");
		TypedQuery<OLTPortDetails> query =  null;
			try {
				logger.info("START::getAllOLTPortDetailsByTenantCode()");
				query = getEntityManager().createQuery(builder.toString(), OLTPortDetails.class);
				query.setParameter("tenantCode", tenantCode);
				oLTPortDetailsList = query.getResultList();
				logger.info("END::getAllOLTPortDetailsByTenantCode()");
			} catch (Exception e) {
				logger.error("EXCEPTION is OLTPortDetailsDao :: getAllOLTPortDetailsByTenantCode() " + e);
			} finally{
				query = null;
				builder = null;
			}
		  return oLTPortDetailsList;
	}

	public OLTPortDetails getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId(String oltId, int cardId, int portNo, String splitterValue) {
		OLTPortDetails oLTPortDetails = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(OLTPortDetails.class.getSimpleName()).append(" WHERE pop_olt_serialno =:oltId ");
		builder.append(" and cardid =:cardId and portno =:portNo and instr(ifnull(l3slotsused,''), '"+splitterValue+"') = 0 ");
		TypedQuery<OLTPortDetails> query =  null;
			try {
				logger.info("START::getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId()");
				query = getEntityManager().createQuery(builder.toString(), OLTPortDetails.class);
				query.setParameter("oltId", oltId);
				query.setParameter("cardId", cardId);
				query.setParameter("portNo", portNo);
				oLTPortDetails = query.getSingleResult();
				logger.info("END::getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId()");
			} catch (Exception e) {
				logger.error("EXCEPTION is OLTPortDetailsDao :: getoLTPortDetailsByoltPortIdAndOLTSrlNoAndCardId() " + e);
			} finally{
				query = null;
				builder = null;
			}
		  return oLTPortDetails;
	}

}
