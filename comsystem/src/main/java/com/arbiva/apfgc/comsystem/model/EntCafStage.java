/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "entcafstg")
@IdClass(EntCafStagePK.class)
public class EntCafStage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "uploadid")
	private Long uploadId;
	
	@Id
	@Column(name = "uploadrecno")
	private Long uploadRecNo;
	
	@Column(name = "district")
	private String district;
	
	@Column(name = "mandal")
	private String mandal;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "contactname")
	private String contactName;
	
	@Column(name = "contactmob")
	private String contactMobileNo;
	
	@Column(name = "contactemail")
	private String contactEmail;
	
	@Column(name = "pmntrespflag")
	private String pmntrespFlag;
	
	@Column(name = "lmocode")
	private String lmocode;
	
	@Column(name = "lattitude")
	private String lattitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "apsfluniqueid")
	private String apsflUniqueId;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "remarks")
	private String remarks;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uploadid", referencedColumnName = "uploadid", nullable = false, insertable=false, updatable=false)
	@JsonIgnore
	private UploadHistory uploadHistory;

	public Long getUploadId() {
		return uploadId;
	}

	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}
	
	public Long getUploadRecNo() {
		return uploadRecNo;
	}

	public void setUploadRecNo(Long uploadRecNo) {
		this.uploadRecNo = uploadRecNo;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMandal() {
		return mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactMobileNo() {
		return contactMobileNo;
	}

	public void setContactMobileNo(String contactMobileNo) {
		this.contactMobileNo = contactMobileNo;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPmntrespFlag() {
		return pmntrespFlag;
	}

	public void setPmntrespFlag(String pmntrespFlag) {
		this.pmntrespFlag = pmntrespFlag;
	}

	public String getLmocode() {
		return lmocode;
	}

	public void setLmocode(String lmocode) {
		this.lmocode = lmocode;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getApsflUniqueId() {
		return apsflUniqueId;
	}

	public void setApsflUniqueId(String apsflUniqueId) {
		this.apsflUniqueId = apsflUniqueId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public UploadHistory getUploadHistory() {
		return uploadHistory;
	}

	public void setUploadHistory(UploadHistory uploadHistory) {
		this.uploadHistory = uploadHistory;
	}
	
}
