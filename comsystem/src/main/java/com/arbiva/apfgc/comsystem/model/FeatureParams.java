/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="featureprms")
@IdClass(FeatureParamsPK.class)
public class FeatureParams extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "coresrvccode")
	private String coresrvccode;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectiveFrom;
	
	@Id
	@Column(name = "featurecode")
	private String featurecode;
	
	@Id
	@Column(name = "prmcode")
	private String prmCode;
	
	@Column(name = "prmname")
	private String prmName;
	
	@Column(name = "prmdfltlbl")
	private String prmdfltlbl;
	
	@Column(name = "prmdatatypelov")
	private String prmdataTypelov;
	
	@Column(name = "prmvaltype")
	private String prmvalType;
	
	@Column(name = "prmlovname")
	private String prmlovName;
	
	@Column(name = "prmvalue")
	private String prmValue;
	
	@Column(name = "deactivatedon")
	private Calendar deactivatedon;
	
	@Column(name = "deactivatedby")
	private String deactivatedby;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedipaddr;
	
	@ManyToOne
	@JoinColumns({@JoinColumn(name = "coresrvccode", referencedColumnName = "coresrvccode", nullable = false, insertable=false, updatable=false), @JoinColumn(name = "featurecode", referencedColumnName = "featurecode", nullable = false, insertable=false, updatable=false)})
	private SrvcFeatures srvcFeatures;

	public String getCoresrvccode() {
		return coresrvccode;
	}

	public void setCoresrvccode(String coresrvccode) {
		this.coresrvccode = coresrvccode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getFeaturecode() {
		return featurecode;
	}

	public void setFeaturecode(String featurecode) {
		this.featurecode = featurecode;
	}

	public String getPrmCode() {
		return prmCode;
	}

	public void setPrmCode(String prmCode) {
		this.prmCode = prmCode;
	}

	public String getPrmName() {
		return prmName;
	}

	public void setPrmName(String prmName) {
		this.prmName = prmName;
	}

	public String getPrmdfltlbl() {
		return prmdfltlbl;
	}

	public void setPrmdfltlbl(String prmdfltlbl) {
		this.prmdfltlbl = prmdfltlbl;
	}

	public String getPrmdataTypelov() {
		return prmdataTypelov;
	}

	public void setPrmdataTypelov(String prmdataTypelov) {
		this.prmdataTypelov = prmdataTypelov;
	}

	public String getPrmvalType() {
		return prmvalType;
	}

	public void setPrmvalType(String prmvalType) {
		this.prmvalType = prmvalType;
	}

	public String getPrmlovName() {
		return prmlovName;
	}

	public void setPrmlovName(String prmlovName) {
		this.prmlovName = prmlovName;
	}

	public String getPrmValue() {
		return prmValue;
	}

	public void setPrmValue(String prmValue) {
		this.prmValue = prmValue;
	}

	public Calendar getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(Calendar deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}

	public String getDeactivatedipaddr() {
		return deactivatedipaddr;
	}

	public void setDeactivatedipaddr(String deactivatedipaddr) {
		this.deactivatedipaddr = deactivatedipaddr;
	}

	public SrvcFeatures getSrvcFeatures() {
		return srvcFeatures;
	}

	public void setSrvcFeatures(SrvcFeatures srvcFeatures) {
		this.srvcFeatures = srvcFeatures;
	}
	
}
