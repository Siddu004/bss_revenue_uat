/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */

@Entity
@Table(name="chargetaxes")
@IdClass(ChargeTaxesPK.class)
public class ChargeTaxes extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="chargecode")
	private String chargeCode;
	
	@Id
	@Column(name="effectivefrom")
	private Date effectiveFrom;
	
	@Id
	@Column(name="taxcode")
	private String taxCode;
	
	@Column(name="deactivatedon")
	private Calendar deactivatedon;
	
	@Column(name="deactivatedby")
	private String deactivatedby;
	
	@Column(name="deactivatedipaddr")
	private String deactivatedipaddr;
	
	@ManyToOne
	@JoinColumn(name = "chargecode", referencedColumnName = "chargecode", nullable = false, insertable=false, updatable=false)
	private ChargeCodes chargeCodes;
	
	@ManyToOne
	@JoinColumn(name = "taxcode", referencedColumnName = "taxcode", nullable = false, insertable=false, updatable=false)
	private TaxCode taxCodes;

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public Calendar getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(Calendar deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}

	public String getDeactivatedipaddr() {
		return deactivatedipaddr;
	}

	public void setDeactivatedipaddr(String deactivatedipaddr) {
		this.deactivatedipaddr = deactivatedipaddr;
	}

	public ChargeCodes getChargeCodes() {
		return chargeCodes;
	}

	public void setChargeCodes(ChargeCodes chargeCodes) {
		this.chargeCodes = chargeCodes;
	}

	public TaxCode getTaxCodes() {
		return taxCodes;
	}

	public void setTaxCodes(TaxCode taxCodes) {
		this.taxCodes = taxCodes;
	}
	
}
