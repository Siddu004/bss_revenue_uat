/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafSTBsDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafSTBsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.PaymentService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.ChargeVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.GenarateChargesVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;
import com.arbiva.apfgc.comsystem.vo.TaxesVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafSTBsServiceImpl implements CafSTBsService {

	private static final Logger logger = Logger.getLogger(CafSTBsServiceImpl.class);
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CafService cafService;
	
	@Autowired
	CafSTBsDao cafSTBsDao;
	
	@Autowired
	CafProductsService cafProductsService;
	
	@Autowired
	CafChargesService cafChargesService;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	CafFeatureParamsService cafFeatureParamsService;
	
	@Autowired
	CafServicesService cafServicesService;
	
	@Override
	@Transactional
	public void saveCCafSTBs(CustomerCafVO customerCafVO, Float totalPaidAmount) throws Exception {
		String exception = "";
		String stbPrice = "0";
		GenarateChargesVO genarateChargesVO = new GenarateChargesVO();
		List<AddtionalServicesVO> serviceList = new ArrayList<>();
		List<ProductsVO> selectedProductsList = new ArrayList<>();
		List<ChargeVO> chargeTypesList = new ArrayList<>();
		try {
			String regionCode = cafService.getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			selectedProductsList = customerCafVO.getProducts();
			if(!customerCafVO.getIptvBoxList().isEmpty()) {
				for(IPTVBoxVO iPTVBoxVO : customerCafVO.getIptvBoxList()) {
					if(iPTVBoxVO.getStbSerialNo() != null && iPTVBoxVO.getMacAddress() != null && !iPTVBoxVO.getStbSerialNo().isEmpty() && !iPTVBoxVO.getMacAddress().isEmpty() ) {
						Long cafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
						CafSTBs cafSTBs = new CafSTBs();
						cafSTBs.setParentCafno(customerCafVO.getCafNo());
						cafSTBs.setCreatedBy(customerCafVO.getLoginId());
						cafSTBs.setCreatedIPAddr(customerCafVO.getIpAddress());
						cafSTBs.setCreatedOn(Calendar.getInstance());
						cafSTBs.setModifiedOn(Calendar.getInstance());
						cafSTBs.setStbcafno(cafNo);
						cafSTBs.setStbLeaseyn('N');
						String stbStatus = cafService.getCpeStackStatusBySrlNo(iPTVBoxVO.getStbSerialNo(), iPTVBoxVO.getStbModel());
						if(stbStatus.equalsIgnoreCase("3") || stbStatus.equalsIgnoreCase("2")) {
							cafSTBs.setStbSrlNo(iPTVBoxVO.getStbSerialNo());
							cafSTBs.setStbmacAddr(iPTVBoxVO.getMacAddress());
						} else {
							exception = "This IPTV Box Serial Number is already blocked";
							throw new RuntimeException(exception);
						}
						cafSTBs.setStbProfileId(Long.parseLong(iPTVBoxVO.getStbModel()));
						cafSTBs.setStbStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
						cafSTBsDao.saveCafSTBs(cafSTBs);
						logger.info("Caf cafSTBs inserted Successfully");
						
						if(customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
							cafProductsService.createCafProducts(customerCafVO, customerCafVO.getCafNo(), customerCafVO.getLoginId(), customerCafVO.getCustCode(), cafNo, iPTVBoxVO.getIptvSrvcCodes(), null, 0l);
							logger.info("The cafProducts Created Successfully");
						} else if(customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
							/*List<CafProducts> cafProductsList = cafProductsService.getCafProducts(customerCafVO.getCafNo());
							String pkgCodes = "";
							for(CafProducts cafProducts : cafProductsList) {
								if(pkgCodes.isEmpty()) {
									pkgCodes = cafProducts.getProdCode();
								} else {
									pkgCodes = pkgCodes+","+cafProducts.getProdCode();
								}
							}
							List<ProductsVO> selectedProductsList =  cafProductsService.getCafServicesListByProdcode(FileUtil.mspCodeSplitMethod(pkgCodes));
							customerCafVO.setProducts(selectedProductsList); */
							
							cafServicesService.createCafServices(customerCafVO, customerCafVO.getCafNo(), customerCafVO.getLoginId(), customerCafVO.getCustCode(), cafNo, iPTVBoxVO.getIptvSrvcCodes(), 0l);
							logger.info("The CafServices Details Created Successfully");

							/*if (customerCafVO.getFlag() == null && customerCafVO.getFeatureCodeList() != null) {
								cafFeatureParamsService.saveCafFeatureParams(customerCafVO);
								logger.info("The cafFeatureParams Details Created Successfully");
							}*/
						}
						
						if(customerCafVO.getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) != -1 && customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
							cafProductsService.createMSOPackages(customerCafVO, cafNo);
							logger.info("The MSO cafProducts Created Successfully");
						}
						
						cafService.updateCpeStockPaymentStatus(customerCafVO.getCafNo(), iPTVBoxVO.getStbSerialNo(), customerCafVO.getLoginId(), customerCafVO.getLmoCode());
						logger.info("Customer and Caf Status Updated Successfully");
						
						cafChargesService.saveEntTaxCafCharges(customerCafVO, cafNo);
						logger.info("The cafCharges Details Created Successfully");
						
						if (!iPTVBoxVO.getStbPrice().isEmpty() && new BigDecimal(iPTVBoxVO.getStbPrice()).compareTo(BigDecimal.ZERO) > 0 && !iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
							cafChargesService.saveUpfrontStbCafCharges(customerCafVO, cafNo, iPTVBoxVO);
							logger.info("The cafCharges Details Created Successfully");
							
							cafChargesService.saveStbCafCharges(customerCafVO, cafNo, iPTVBoxVO);
							logger.info("The cafCharges Details Created Successfully");
						} else if(!iPTVBoxVO.getStbPrice().isEmpty() && new BigDecimal(iPTVBoxVO.getStbPrice()).compareTo(BigDecimal.ZERO) > 0 && iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0")) {
							cafChargesService.saveStbCafCharges(customerCafVO, cafNo, iPTVBoxVO);
							logger.info("The cafCharges Details Created Successfully");
						}
						
						if(iPTVBoxVO.getIptvSrvcCodes() != null) {
							String[] iptvPkgCodes = iPTVBoxVO.getIptvSrvcCodes().split(",");
							for (ProductsVO products : selectedProductsList) {
								for(String prodcode : iptvPkgCodes) {
									if(prodcode.equalsIgnoreCase(products.getProdcode()) && !products.getProdType().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode())) {
										serviceList = products.getServicesList();
										for (AddtionalServicesVO services : serviceList) {
											chargeTypesList = services.getChargeList();
											for (ChargeVO charges : chargeTypesList) {
												if (charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.Activation_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
													TaxesVO taxesVO = paymentService.getTaxAmounts(services.getServiceCode(), charges.getChargeCode(), regionCode, ComsEnumCodes.Service_TaxLevelFlag.getCode(), charges.getChargeAmt());
													genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
													genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
													if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
														genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
													} else {
														genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
													}
													genarateChargesVO.setChargeAmt(charges.getChargeAmt());
													genarateChargesVO.setPaidAmount(totalPaidAmount != null ? String.valueOf(totalPaidAmount) : "0");
													genarateChargesVO.setChargeCode(charges.getChargeCode());
													genarateChargesVO.setFeatureCode(charges.getFeatureCode());
													genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
													genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
													genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
													genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
													genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
													genarateChargesVO.setProdCafno(customerCafVO.getCafNo().toString());
													genarateChargesVO.setStbCafno(cafNo.toString());
													genarateChargesVO.setProdCode(products.getProdcode());
													genarateChargesVO.setSrvcCode(services.getServiceCode());
													genarateChargesVO.setTenantCode(products.getTenantcode());
													genarateChargesVO.setChargeDescription(null);
													genarateChargesVO.setAdjrefId(null);
													if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
														genarateChargesVO.setIgnoreBalenceFlag("Y");
													} else {
														genarateChargesVO.setIgnoreBalenceFlag("N");
													}
													genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
													String status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
													if(!status.equalsIgnoreCase("Success")) {
														throw new RuntimeException("Insufficient Payment.");
													}
												} else if(charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.SecurityDeposit_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
													genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
													genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
													if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
														genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
													} else {
														genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
													}
													genarateChargesVO.setChargeAmt(charges.getChargeAmt());
													genarateChargesVO.setPaidAmount(totalPaidAmount != null ? String.valueOf(totalPaidAmount) : "0");
													genarateChargesVO.setChargeCode(charges.getChargeCode());
													genarateChargesVO.setFeatureCode(charges.getFeatureCode());
													genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
													genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
													genarateChargesVO.setEntTaxamt("0");
													genarateChargesVO.setKisanTaxamt("0");
													genarateChargesVO.setProdCafno(customerCafVO.getCafNo().toString());
													genarateChargesVO.setStbCafno(cafNo.toString());
													genarateChargesVO.setProdCode(products.getProdcode());
													genarateChargesVO.setSrvcCode(services.getServiceCode());
													genarateChargesVO.setSrvcTaxamt("0");
													genarateChargesVO.setSwatchTaxamt("0");
													genarateChargesVO.setTenantCode(products.getTenantcode());
													genarateChargesVO.setChargeDescription(null);
													genarateChargesVO.setAdjrefId(null);
													if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
														genarateChargesVO.setIgnoreBalenceFlag("Y");
													} else {
														genarateChargesVO.setIgnoreBalenceFlag("N");
													}
													genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
													String status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
													if(!status.equalsIgnoreCase("Success")) {
														throw new RuntimeException("Insufficient Payment.");
													}
												}
											}
										}
									}
								}
							}
						}
						
						if(iPTVBoxVO.getStbInstallmentCount() != null) {
							TaxesVO taxesVO = null;
							stbPrice = !iPTVBoxVO.getStbPrice().isEmpty() ? iPTVBoxVO.getStbPrice() : stbPrice;
							if (!iPTVBoxVO.getStbInstallmentCount().isEmpty() && new BigDecimal(stbPrice).compareTo(BigDecimal.ZERO) > 0 ) {
								if (iPTVBoxVO.getStbInstallmentCount().equalsIgnoreCase("0") ) {
									taxesVO = paymentService.getTaxAmounts("", ComsEnumCodes.IPTV_COST_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), stbPrice);
									genarateChargesVO.setChargeCode(ComsEnumCodes.IPTV_COST_CODE.getCode());
								} else {
									taxesVO = paymentService.getTaxAmounts("", ComsEnumCodes.IPTV_UPFRONT_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), stbPrice);
									genarateChargesVO.setChargeCode(ComsEnumCodes.IPTV_UPFRONT_CODE.getCode());
								}
								genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
								genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
								if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
								} else {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
								}
								genarateChargesVO.setChargeAmt(iPTVBoxVO.getStbPrice());
								genarateChargesVO.setPaidAmount(totalPaidAmount != null ? String.valueOf(totalPaidAmount) : "0");
								genarateChargesVO.setFeatureCode("0");
								genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
								genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
								genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
								genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
								genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
								genarateChargesVO.setProdCafno(customerCafVO.getCafNo().toString());
								genarateChargesVO.setStbCafno(cafNo.toString());
								genarateChargesVO.setProdCode("");
								genarateChargesVO.setSrvcCode("");
								genarateChargesVO.setTenantCode("");
								genarateChargesVO.setChargeDescription(null);
								genarateChargesVO.setAdjrefId(null);
								if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									genarateChargesVO.setIgnoreBalenceFlag("Y");
								} else {
									genarateChargesVO.setIgnoreBalenceFlag("N");
								}
								genarateChargesVO.setRsAgrUid("0");
								String status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
								if(!status.equalsIgnoreCase("Success")) {
									throw new RuntimeException("Insufficient Payment.");
								} 
							}
						}
					}
				}
			} else {
				if(customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
					cafProductsService.createCafProducts(customerCafVO, customerCafVO.getCafNo(), customerCafVO.getLoginId(), customerCafVO.getCustCode(), 0l, null, null, 0l);
					logger.info("The cafProducts Created Successfully");
				}
				cafServicesService.createCafServices(customerCafVO, customerCafVO.getCafNo(), customerCafVO.getLoginId(), customerCafVO.getCustCode(), 0l, null, 0l);
				logger.info("The CafServices Details Created Successfully");
			}
		} catch(Exception e) {
			logger.error("The Exception is CafSTBsServiceImpl::saveCCafSTBs " +e);
			e.printStackTrace();
			throw e;
		}
	}

}
