package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author kiran
 *
 */
@Entity
public class OntIdCustTypeBO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="agorahsisubscode")
	private String ontId;
	
	@Column(name="custtypelov")
	private String CustType;
	
	@Column(name="cafno")
	private String cafNo;
	
	//new code ..7/02/2018
	@Column(name="custid")
	private String customerID;
	
	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	
	
	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getOntId() {
		return ontId;
	}

	public void setOntId(String ontId) {
		this.ontId = ontId;
	}

	public String getCustType() {
		return CustType;
	}

	public void setCustType(String custType) {
		CustType = custType;
	}
	
	
	
}
