package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.util.List;

public class CorpusResponce implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CorpusResponceStatus responseStatus;
	
	private String trackingId;
	
	private List<String> invalidSubsriberCodes;
	

	public List<String> getInvalidSubsriberCodes() {
		return invalidSubsriberCodes;
	}

	public void setInvalidSubsriberCodes(List<String> invalidSubsriberCodes) {
		this.invalidSubsriberCodes = invalidSubsriberCodes;
	}

	public CorpusResponceStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(CorpusResponceStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
}
