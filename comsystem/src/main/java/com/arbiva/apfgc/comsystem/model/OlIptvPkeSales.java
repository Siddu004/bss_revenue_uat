package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="oliptvpkgsales")
public class OlIptvPkeSales {

	@Id
	@Column(name="requestid")
	private Long requestid;
	
	@Column(name="nwsubscode")
	private String nwsubscode;
	
	@Column(name="acctcafno")
	private Long acctcafno;
	
	@Column(name="prodcafno")
	private Long prodcafno;
	
	@Column(name="packages")
	private String packages;
	
	@Column(name="otp")
	private String otp;
	
	@Column(name="status")
	private String status;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="created_date")
	private Calendar created_date;
	
	@Column(name="confirm_date")
	private Calendar confirm_date;
	
	@Column(name="executed_date")
	private Calendar executed_date;
	

	public OlIptvPkeSales(){
		
	}
	public OlIptvPkeSales(String randumNo, String parentCafNo, Long id, String subscribercode, String packageCode, String status) {
		this.requestid = id;
		this.otp = randumNo;
		this.acctcafno = Long.parseLong(parentCafNo);
		this.nwsubscode = subscribercode;
		this.packages = packageCode;
		this.created_date = Calendar.getInstance();
		this.status = status;
	}

	public Long getRequestid() {
		return requestid;
	}

	public void setRequestid(Long requestid) {
		this.requestid = requestid;
	}

	public String getNwsubscode() {
		return nwsubscode;
	}

	public void setNwsubscode(String nwsubscode) {
		this.nwsubscode = nwsubscode;
	}

	public Long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(Long acctcafno) {
		this.acctcafno = acctcafno;
	}

	public Long getProdcafno() {
		return prodcafno;
	}

	public void setProdcafno(Long prodcafno) {
		this.prodcafno = prodcafno;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Calendar getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Calendar created_date) {
		this.created_date = created_date;
	}

	public Calendar getConfirm_date() {
		return confirm_date;
	}

	public void setConfirm_date(Calendar confirm_date) {
		this.confirm_date = confirm_date;
	}

	public Calendar getExecuted_date() {
		return executed_date;
	}

	public void setExecuted_date(Calendar executed_date) {
		this.executed_date = executed_date;
	}
	

}
