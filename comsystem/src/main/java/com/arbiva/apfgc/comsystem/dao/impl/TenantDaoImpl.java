/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.Tenant;
import com.arbiva.apfgc.comsystem.vo.TenantVO;

/**
 * @author Arbiva
 *
 */
@Repository
public class TenantDaoImpl {
	
	private static final Logger LOGGER = Logger.getLogger(TenantDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public Tenant findByTenantCode(String tenantCode) {
		Tenant tenant = new Tenant();
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName()).append(" WHERE tenantcode=:tenantCode");
		TypedQuery<Tenant> query = null;
		try {
			LOGGER.info("START::findByTenantCode()");
			query = getEntityManager().createQuery(builder.toString(),
					Tenant.class);
			query.setParameter("tenantCode", tenantCode);
			tenant = query.getSingleResult();
			LOGGER.info("END::findByTenantCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantCode() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return tenant;
	}
	
	public List<Tenant> findMspCodeByTenantType() {
		List<Tenant> tenantList = new ArrayList<Tenant>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName()).append(" WHERE tenanttypelov = 'MSP'");
		TypedQuery<Tenant> query = null;
		try {
			LOGGER.info("START::findMspCodeByTenantType()");
			query = getEntityManager().createQuery(builder.toString(),Tenant.class);
			tenantList = query.getResultList();
			LOGGER.info("END::findMspCodeByTenantType()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findMspCodeByTenantType() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return tenantList;
	}
	
	public List<Tenant> findAllTenants() {
		List<Tenant> Tenants = new ArrayList<Tenant>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName());
		TypedQuery<Tenant> query = null;
		try {
			LOGGER.info("START::findAllTenants()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			Tenants = query.getResultList();
			LOGGER.info("END::findAllTenants()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenants() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return Tenants;
	}
	
	public Tenant findByTenantId(Integer tenantId) {
		Tenant tenant = new Tenant();
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName()).append(" WHERE tenantid=:tenantId");
		TypedQuery<Tenant> query = null;
		try {
			LOGGER.info("START::findByTenantId()");
			query = getEntityManager().createQuery(builder.toString(),Tenant.class);
			query.setParameter("tenantId", tenantId);
			tenant = query.getSingleResult();
			LOGGER.info("END::findByTenantId()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantId() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return tenant;
	}
	
	public void saveTenant(Tenant tenant) {
		getEntityManager().merge(tenant);
	}

	public String findTenantDtlsByCafno(Long cafNumber) {
		String lmoCode = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(" SELECT lmocode from cafs where cafno ='" + cafNumber + "' ");
		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			lmoCode = (String) query.getSingleResult();
		}catch(Exception e){
			LOGGER.error("The Exception is updateTenatWallet()::  " +e);
			e.printStackTrace();
		}
		return lmoCode;
	}
	
	@SuppressWarnings("unchecked")
	public TenantVO findTenatInfoByTenantID(String lmocode) {
		List<Object[]> tenantList = new ArrayList<>();
		Object[] objects = null;
		TenantVO tenantVO = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("END::findTenatInfoByTenantID()");
			builder = new StringBuilder("SELECT tenantid, tenantcode, tenantname, tenanttypelov FROM tenants WHERE tenantcode='" + lmocode + "' ");
			query = getEntityManager().createNativeQuery(builder.toString());
			tenantList = (List<Object[]>) query.getResultList();
			if (!tenantList.isEmpty()) {
				tenantVO = new TenantVO();
				objects = tenantList.get(0);
				tenantVO.setTenantId(objects[0] == null ? "" : objects[0].toString());
				tenantVO.setTenantCode(objects[1] == null ? "" : objects[1].toString());
				tenantVO.setName(objects[2] == null ? "" : objects[2].toString());
				tenantVO.setTenantTypeLov(objects[3] == null ? "" : objects[3].toString());
			}
			LOGGER.info("END::findTenatInfoByTenantID()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findTenatInfoByTenantID() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenantVO;
	}
}
