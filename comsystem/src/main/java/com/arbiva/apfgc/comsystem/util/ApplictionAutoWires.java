package com.arbiva.apfgc.comsystem.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.comsystem.dao.impl.CafProductsDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.service.CPEInformationService;
import com.arbiva.apfgc.comsystem.service.CafAccountService;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.CorpusAPIService;
import com.arbiva.apfgc.comsystem.service.CustomerService;
import com.arbiva.apfgc.comsystem.service.EnterpriseCustomerService;
import com.arbiva.apfgc.comsystem.service.OSDFingerPrintDetailsService;
import com.arbiva.apfgc.comsystem.service.PaymentService;
import com.arbiva.apfgc.comsystem.service.TaxRegionMappingService;
import com.arbiva.apfgc.comsystem.service.UploadHistoryService;
import com.arbiva.apfgc.comsystem.serviceImpl.BillCycleServiceImpl;
import com.arbiva.apfgc.comsystem.serviceImpl.LovsServiceImpl;
import com.arbiva.apfgc.comsystem.serviceImpl.OLTPortDetailsServiceImpl;
import com.arbiva.apfgc.comsystem.serviceImpl.StatusCodesServiceImpl;
import com.arbiva.apfgc.comsystem.serviceImpl.TenantServiceImpl;
import com.arbiva.apfgc.comsystem.serviceImpl.TenantWalletServiceImpl;
import com.arbiva.apfgc.provision.businessService.ProvisionErrorsBusinessServiceImpl;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;
import com.xyzinnotech.bss.sms.service.SmsApi;

@Component
public class ApplictionAutoWires {
	
	@Autowired
	private CafProductsDao cafProductsDao;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CafService cafService;

	@Autowired
	private CafProductsService cafProductsService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private LovsServiceImpl lovsService;

	@Autowired
	private BillCycleServiceImpl billCycleService;

	@Autowired
	private StoredProcedureDAO storedProcedureDAO;

	@Autowired
	private TenantWalletServiceImpl tenantWalletService;

	@Autowired
	private TenantServiceImpl tenantService;

	@Autowired
	private StatusCodesServiceImpl statusCodesService;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private EnterpriseCustomerService enterpriseCustomerService;

	@Autowired
	private CPEInformationService cpeInformationService;

	@Autowired
	private TaxRegionMappingService taxRegionMappingService;

	@Autowired
	private CafChargesService cafChargesService;
	
	@Autowired
	private ProvisionErrorsBusinessServiceImpl provisionErrorsBusinessServiceImpl;
	
	@Autowired
	private ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;
	
	@Autowired
	private SmsApi smsApi;
	
	@Autowired
	private CafAccountService cafAccountService;
	
	@Autowired
	private OSDFingerPrintDetailsService osdFingerPrintDetailsService;
	
	@Autowired
	private OLTPortDetailsServiceImpl oltPortDetailsService;
	
	@Autowired
	CorpusAPIService corpusAPIService;
	
	@Autowired
	UploadHistoryService uploadHistoryService;
	
	@Autowired
	CafServicesService cafServicesService;
	
	public UploadHistoryService getUploadHistoryService() {
		return uploadHistoryService;
	}

	public void setUploadHistoryService(UploadHistoryService uploadHistoryService) {
		this.uploadHistoryService = uploadHistoryService;
	}

	public CorpusAPIService getCorpusAPIService() {
		return corpusAPIService;
	}

	public void setCorpusAPIService(CorpusAPIService corpusAPIService) {
		this.corpusAPIService = corpusAPIService;
	}

	public OLTPortDetailsServiceImpl getOltPortDetailsService() {
		return oltPortDetailsService;
	}

	public void setOltPortDetailsService(OLTPortDetailsServiceImpl oltPortDetailsService) {
		this.oltPortDetailsService = oltPortDetailsService;
	}

	public OSDFingerPrintDetailsService getOsdFingerPrintDetailsService() {
		return osdFingerPrintDetailsService;
	}

	public void setOsdFingerPrintDetailsService(OSDFingerPrintDetailsService osdFingerPrintDetailsService) {
		this.osdFingerPrintDetailsService = osdFingerPrintDetailsService;
	}

	public CafAccountService getCafAccountService() {
		return cafAccountService;
	}

	public void setCafAccountService(CafAccountService cafAccountService) {
		this.cafAccountService = cafAccountService;
	}

	public ProvisionErrorsBusinessServiceImpl getProvisionErrorsBusinessServiceImpl() {
		return provisionErrorsBusinessServiceImpl;
	}

	public void setProvisionErrorsBusinessServiceImpl(
			ProvisionErrorsBusinessServiceImpl provisionErrorsBusinessServiceImpl) {
		this.provisionErrorsBusinessServiceImpl = provisionErrorsBusinessServiceImpl;
	}

	public ProvisioningBusinessServiceImpl getProvisioningBusinessServiceImpl() {
		return provisioningBusinessServiceImpl;
	}

	public void setProvisioningBusinessServiceImpl(ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl) {
		this.provisioningBusinessServiceImpl = provisioningBusinessServiceImpl;
	}

	public SmsApi getSmsApi() {
		return smsApi;
	}

	public void setSmsApi(SmsApi smsApi) {
		this.smsApi = smsApi;
	}

	public CafProductsDao getCafProductsDao() {
		return cafProductsDao;
	}

	public void setCafProductsDao(CafProductsDao cafProductsDao) {
		this.cafProductsDao = cafProductsDao;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public CafService getCafService() {
		return cafService;
	}

	public void setCafService(CafService cafService) {
		this.cafService = cafService;
	}

	public CafProductsService getCafProductsService() {
		return cafProductsService;
	}

	public void setCafProductsService(CafProductsService cafProductsService) {
		this.cafProductsService = cafProductsService;
	}

	public PaymentService getPaymentService() {
		return paymentService;
	}

	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	public LovsServiceImpl getLovsService() {
		return lovsService;
	}

	public void setLovsService(LovsServiceImpl lovsService) {
		this.lovsService = lovsService;
	}

	public BillCycleServiceImpl getBillCycleService() {
		return billCycleService;
	}

	public void setBillCycleService(BillCycleServiceImpl billCycleService) {
		this.billCycleService = billCycleService;
	}

	public StoredProcedureDAO getStoredProcedureDAO() {
		return storedProcedureDAO;
	}

	public void setStoredProcedureDAO(StoredProcedureDAO storedProcedureDAO) {
		this.storedProcedureDAO = storedProcedureDAO;
	}

	public TenantWalletServiceImpl getTenantWalletService() {
		return tenantWalletService;
	}

	public void setTenantWalletService(TenantWalletServiceImpl tenantWalletService) {
		this.tenantWalletService = tenantWalletService;
	}

	public TenantServiceImpl getTenantService() {
		return tenantService;
	}

	public void setTenantService(TenantServiceImpl tenantService) {
		this.tenantService = tenantService;
	}

	public StatusCodesServiceImpl getStatusCodesService() {
		return statusCodesService;
	}

	public void setStatusCodesService(StatusCodesServiceImpl statusCodesService) {
		this.statusCodesService = statusCodesService;
	}

	public HttpServletRequest getHttpServletRequest() {
		return httpServletRequest;
	}

	public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	public EnterpriseCustomerService getEnterpriseCustomerService() {
		return enterpriseCustomerService;
	}

	public void setEnterpriseCustomerService(EnterpriseCustomerService enterpriseCustomerService) {
		this.enterpriseCustomerService = enterpriseCustomerService;
	}

	public CPEInformationService getCpeInformationService() {
		return cpeInformationService;
	}

	public void setCpeInformationService(CPEInformationService cpeInformationService) {
		this.cpeInformationService = cpeInformationService;
	}

	public TaxRegionMappingService getTaxRegionMappingService() {
		return taxRegionMappingService;
	}

	public void setTaxRegionMappingService(TaxRegionMappingService taxRegionMappingService) {
		this.taxRegionMappingService = taxRegionMappingService;
	}

	public CafChargesService getCafChargesService() {
		return cafChargesService;
	}

	public void setCafChargesService(CafChargesService cafChargesService) {
		this.cafChargesService = cafChargesService;
	}

	public CafServicesService getCafServicesService() {
		return cafServicesService;
	}

	public void setCafServicesService(CafServicesService cafServicesService) {
		this.cafServicesService = cafServicesService;
	}
}
