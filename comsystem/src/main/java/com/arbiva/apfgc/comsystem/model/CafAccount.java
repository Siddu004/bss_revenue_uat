/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cafacct")
@IdClass(CafAccountPK.class)
public class CafAccount extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "acctcafno")
	private Long acctcafno;
	
	@Id
	@Column(name = "districtuid")
	private Integer districtuid;
	
	@Column(name = "pmntcustid")
	private Long custId;
	
	@Column(name = "regbal")
	private BigDecimal regBalence;
	
	@Column(name = "depbal")
	private BigDecimal depBalence;
	
	@Column(name = "unbbal")
	private BigDecimal unbBalence;
	
	@Column(name = "chargedbal")
	private BigDecimal chargedBalence;
	
	@Column(name = "billfreqlov")
	private String billfreqlov;
	
	@Column(name = "billrunday")
	private int billrunday;
	
	@Column(name = "billedfdate")
	private Date billedfDate;
	
	@Column(name = "billedtdate")
	private Date billedtDate;
	
	@Column(name = "nextbillfdate")
	private Date nextbillfDate;
	
	@Column(name = "nextbilltdate")
	private Date nextbilltDate;
	
	@Column(name = "actdate")
	private Date actDate;
	
	@Column(name = "deactdate")
	private Date deactDate;
	
	@Column(name = "suspdate")
	private Date suspDate;
	
	@Column(name = "suspdates")
	private String suspDates;
	
	@Column(name = "resumedate")
	private Date resumeDate;
	
	@Column(name = "resumedates")
	private String resumeDates;
	
	@Column(name = "lastinvmon")
	private Integer lastInvmon;
	
	@Column(name = "lastlatepaymon")
	private Integer lastLatePaymon;
	
	@Column(name = "lastpmntid")
	private Long lastPmntId;
	
	@Column(name = "lastinvid")
	private Long lastInvid;
	
	@Column(name = "lastadjid")
	private Long lastAdjid;
	
	@Column(name = "nextdtlrecid")
	private Long nextDtlrecid;
	
	@Column(name = "openinvmon")
	private Integer openInvmon;
	
	@Column(name = "openinvno")
	private Long openInvno;
	
	@Column(name = "openduedate")
	private Date opendueDate;
	
	@Column(name = "openinvamt")
	private BigDecimal openinvAmt;

	public BigDecimal getChargedBalence() {
		return chargedBalence;
	}

	public void setChargedBalence(BigDecimal chargedBalence) {
		this.chargedBalence = chargedBalence;
	}

	public Long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(Long acctcafno) {
		this.acctcafno = acctcafno;
	}
	
	public Integer getDistrictuid() {
		return districtuid;
	}

	public void setDistrictuid(Integer districtuid) {
		this.districtuid = districtuid;
	}

	public BigDecimal getRegBalence() {
		return regBalence;
	}

	public void setRegBalence(BigDecimal regBalence) {
		this.regBalence = regBalence;
	}

	public BigDecimal getDepBalence() {
		return depBalence;
	}

	public void setDepBalence(BigDecimal depBalence) {
		this.depBalence = depBalence;
	}

	public BigDecimal getUnbBalence() {
		return unbBalence;
	}

	public void setUnbBalence(BigDecimal unbBalence) {
		this.unbBalence = unbBalence;
	}

	public Date getActDate() {
		return actDate;
	}

	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	public Date getDeactDate() {
		return deactDate;
	}

	public void setDeactDate(Date deactDate) {
		this.deactDate = deactDate;
	}

	public Date getSuspDate() {
		return suspDate;
	}

	public void setSuspDate(Date suspDate) {
		this.suspDate = suspDate;
	}
	
	public Date getResumeDate() {
		return resumeDate;
	}

	public void setResumeDate(Date resumeDate) {
		this.resumeDate = resumeDate;
	}
	
	public Integer getLastInvmon() {
		return lastInvmon;
	}

	public void setLastInvmon(Integer lastInvmon) {
		this.lastInvmon = lastInvmon;
	}

	public Integer getLastLatePaymon() {
		return lastLatePaymon;
	}

	public void setLastLatePaymon(Integer lastLatePaymon) {
		this.lastLatePaymon = lastLatePaymon;
	}

	public Integer getOpenInvmon() {
		return openInvmon;
	}

	public void setOpenInvmon(Integer openInvmon) {
		this.openInvmon = openInvmon;
	}

	public Long getLastPmntId() {
		return lastPmntId;
	}

	public void setLastPmntId(Long lastPmntId) {
		this.lastPmntId = lastPmntId;
	}

	public Long getLastInvid() {
		return lastInvid;
	}

	public void setLastInvid(Long lastInvid) {
		this.lastInvid = lastInvid;
	}

	public Long getLastAdjid() {
		return lastAdjid;
	}

	public void setLastAdjid(Long lastAdjid) {
		this.lastAdjid = lastAdjid;
	}

	public Long getNextDtlrecid() {
		return nextDtlrecid;
	}

	public void setNextDtlrecid(Long nextDtlrecid) {
		this.nextDtlrecid = nextDtlrecid;
	}
	
	public String getBillfreqlov() {
		return billfreqlov;
	}

	public void setBillfreqlov(String billfreqlov) {
		this.billfreqlov = billfreqlov;
	}

	public Date getBilledfDate() {
		return billedfDate;
	}

	public void setBilledfDate(Date billedfDate) {
		this.billedfDate = billedfDate;
	}

	public Date getBilledtDate() {
		return billedtDate;
	}

	public void setBilledtDate(Date billedtDate) {
		this.billedtDate = billedtDate;
	}

	public Date getNextbillfDate() {
		return nextbillfDate;
	}

	public void setNextbillfDate(Date nextbillfDate) {
		this.nextbillfDate = nextbillfDate;
	}

	public Date getNextbilltDate() {
		return nextbilltDate;
	}

	public void setNextbilltDate(Date nextbilltDate) {
		this.nextbilltDate = nextbilltDate;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public int getBillrunday() {
		return billrunday;
	}

	public void setBillrunday(int billrunday) {
		this.billrunday = billrunday;
	}

	public String getSuspDates() {
		return suspDates;
	}

	public void setSuspDates(String suspDates) {
		this.suspDates = suspDates;
	}

	public String getResumeDates() {
		return resumeDates;
	}

	public void setResumeDates(String resumeDates) {
		this.resumeDates = resumeDates;
	}

	public Long getOpenInvno() {
		return openInvno;
	}

	public void setOpenInvno(Long openInvno) {
		this.openInvno = openInvno;
	}

	public Date getOpendueDate() {
		return opendueDate;
	}

	public void setOpendueDate(Date opendueDate) {
		this.opendueDate = opendueDate;
	}

	public BigDecimal getOpeninvAmt() {
		return openinvAmt;
	}

	public void setOpeninvAmt(BigDecimal openinvAmt) {
		this.openinvAmt = openinvAmt;
	}
	
}
