package com.arbiva.apfgc.comsystem.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.vo.EventVO;
import com.arbiva.apfgc.comsystem.vo.GenarateChargesVO;
import com.arbiva.apfgc.comsystem.vo.TenantWalletVO;

/**
 * 
 * @author Lakshman
 *
 */
@Repository("storedProcedureDAO")
public class StoredProcedureDAO {
    
    private static final Logger LOGGER = Logger.getLogger(StoredProcedureDAO.class);

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public DataSource getDataSource() {
        return dataSource;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * 
     * @return
     */
    @Transactional
    public Long executePaymentCustomer(Long custId) {
        Long sequenceNo = 0L;
        SimpleJdbcCall generateSequenceSP = null;
        Map<String,Object> in = null;
        Map<String, Object> out = null;
        try{
            generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("get_pmntcustid").withoutProcedureColumnMetaDataAccess().useInParameterNames(
                    "p_custid").declareParameters(new SqlParameter("p_custid",Types.VARCHAR), new SqlOutParameter("p_pmntcustid", Types.BIGINT), 
                            new SqlOutParameter("p_result", Types.VARCHAR));
            
            in = new HashMap<String, Object>();
            in.put("p_custid", custId);
            out = generateSequenceSP.execute(in); 
            if(out.get("p_result").toString().equalsIgnoreCase("0")) {
                sequenceNo = Long.valueOf(out.get("p_pmntcustid").toString());
            }
        }
        catch(Exception ex) {
            LOGGER.error("Exception occurred during executePaymentCustomer(): " + ex); 
        }
        finally {
            generateSequenceSP = null;
            in = null;
            out = null;
        }
        return sequenceNo;
    }
    
    @Transactional
    public Long executeStoredProcedure(String type) {
        Long sequenceNo = 0L;
        SimpleJdbcCall generateSequenceSP = null;
        Map<String,Object> in = null;
        Map<String, Object> out = null;
        try{
            generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("get_nextid").withoutProcedureColumnMetaDataAccess().useInParameterNames(
                    "p_idname").declareParameters(new SqlParameter("p_idname",Types.VARCHAR), new SqlOutParameter("p_nextid", Types.BIGINT), 
                            new SqlOutParameter("p_result", Types.VARCHAR));
            
            in = new HashMap<String, Object>();
            in.put("p_idname", type);
            out = generateSequenceSP.execute(in);            
            if(out.get("p_nextid") != null)
                sequenceNo = Long.valueOf(out.get("p_nextid").toString());
        }
        catch(Exception ex) {
            LOGGER.error("Exception occurred during getCafNo(): " + ex); 
        }
        finally {
            generateSequenceSP = null;
            in = null;
            out = null;
        }
        return sequenceNo;
    }
    
    @Transactional
    public String getPhoneNoStoredProcedure(String custType, String stdCode) {
        String phoneNo = "";
        SimpleJdbcCall generateSequenceSP = null;
        Map<String,Object> in = null;
        Map<String, Object> out = null;
        try{
            generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("get_nextphone").withoutProcedureColumnMetaDataAccess().useInParameterNames(
                    "p_custtype", "p_stdcode").declareParameters(new SqlParameter("p_custtype",Types.VARCHAR), new SqlParameter("p_stdcode",Types.VARCHAR), new SqlOutParameter("p_phoneno", Types.VARCHAR), 
                            new SqlOutParameter("p_result", Types.VARCHAR));
            
            in = new HashMap<String, Object>();
            in.put("p_stdcode", stdCode);
            in.put("p_custtype", custType);
            out = generateSequenceSP.execute(in);            
            if(out.get("p_result").toString().equalsIgnoreCase("0")) {
            	phoneNo = out.get("p_phoneno").toString();
            } else {
            	phoneNo = out.get("p_result").toString();
            }
        }
        catch(Exception ex) {
        	LOGGER.error("Phone Number is not Avaliable for that std code"); 
            LOGGER.error("Exception occurred during getPhoneNo(): " + ex); 
        }
        finally {
            generateSequenceSP = null;
            in = null;
            out = null;
        }
        return phoneNo;
    }
    
    @Transactional
    public String executePullOnuidStoredProcedure(String srlNo, int cardNo, Integer portNo) {
        String sequenceNo = "";
        SimpleJdbcCall generateSequenceSP = null;
        Map<String,Object> in = null;
        Map<String, Object> out = null;
        try{
            generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("pull_onuid").withoutProcedureColumnMetaDataAccess().useInParameterNames(
                    "p_oltslno", "p_cardid", "p_portno").declareParameters(new SqlParameter("p_oltslno",Types.VARCHAR), 
                    		new SqlParameter("p_cardid", Types.TINYINT), new SqlParameter("p_portno", Types.INTEGER),
                    		new SqlOutParameter("p_onuid", Types.BIGINT), 
                            new SqlOutParameter("p_result", Types.VARCHAR));
            
            in = new HashMap<String, Object>();
            in.put("p_oltslno", srlNo);
            in.put("p_cardid", cardNo);
            in.put("p_portno", portNo);
            out = generateSequenceSP.execute(in);
            if(out.get("p_result").toString().equalsIgnoreCase("0")) {
            	sequenceNo = out.get("p_onuid").toString();
            } else {
            	sequenceNo = out.get("p_result").toString();
            }
        }
        catch(Exception ex) {
            LOGGER.error("Exception occurred during getOnuNo(): " + ex); 
        }
        finally {
            generateSequenceSP = null;
            in = null;
            out = null;
        }
        return sequenceNo;
    }
    
	@Transactional
	public String executePushOnuidStoredProcedure(String srlNo, int cardNo, Integer portNo, Integer onuId) {
		String sequenceNo = "";
		SimpleJdbcCall generateSequenceSP = null;
		Map<String, Object> in = null;
		Map<String, Object> out = null;
		try {
			generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("push_onuid").withoutProcedureColumnMetaDataAccess()
					.useInParameterNames("p_oltslno", "p_cardid", "p_portno", "p_onuid").declareParameters(new SqlParameter("p_oltslno", Types.VARCHAR),
							new SqlParameter("p_cardid", Types.TINYINT), new SqlParameter("p_portno", Types.INTEGER),
							new SqlParameter("p_onuid", Types.INTEGER), new SqlOutParameter("p_result", Types.VARCHAR));

			in = new HashMap<String, Object>();
			in.put("p_oltslno", srlNo);
			in.put("p_cardid", cardNo);
			in.put("p_portno", portNo);
			in.put("p_onuid", onuId);
			out = generateSequenceSP.execute(in);
			sequenceNo = out.get("p_result").toString();
		} catch (Exception ex) {
			LOGGER.error("Exception occurred during getOnuNo(): " + ex);
		} finally {
			generateSequenceSP = null;
			in = null;
			out = null;
		}
		return sequenceNo;
	}
    
    @Transactional
	public boolean processSuccessfulPayments() {
		SimpleJdbcCall processSuccessfulPaymentsSP = null;
		boolean isSuccess = false;
		try{
			processSuccessfulPaymentsSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("processSucessPayments").withoutProcedureColumnMetaDataAccess();			
			processSuccessfulPaymentsSP.execute();
			isSuccess = true;
		}
		catch(Exception ex) {
			isSuccess = false;
			LOGGER.error("Exception occurred during processSuccessfulPayments(): " + ex); 
		}
		finally {
			processSuccessfulPaymentsSP = null;
		}
		return isSuccess;
	}
    
    @Transactional
    public boolean processRegPayments() {
        SimpleJdbcCall generateSequenceSP = null;
        boolean isSuccess = false;
        Map<String, Object> out = null;
        try{
            generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("processregpayments").withoutProcedureColumnMetaDataAccess().
            		declareParameters(new SqlOutParameter("p_result", Types.VARCHAR));
            
            out = generateSequenceSP.execute(); 
            if(out.get("p_result").toString().equalsIgnoreCase("0")) {
            	isSuccess = true;
            }
        }
        catch(Exception ex) {
            LOGGER.error("Exception occurred during processRegPayments(): " + ex); 
        }
        finally {
            generateSequenceSP = null;
            out = null;
        }
        return isSuccess;
    }
    
    @Transactional
	public String processWOPayments(Integer districtUid, long acctCafNo, long pmntId, float pmntAmt) {
		SimpleJdbcCall processWOPaymentsSP = null;
		String result = "";
        Map<String,Object> in = new HashMap<String, Object>();
        Map<String, Object> out = null;
		try{
			processWOPaymentsSP = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("processwopayments")
					.withoutProcedureColumnMetaDataAccess()
					.useInParameterNames("p_districtuid","p_pmntcustid", "p_pmntid","p_pmntamt")
					.declareParameters(new SqlParameter("p_districtuid",Types.BIGINT)
							,new SqlParameter("p_pmntcustid",Types.BIGINT)
							,new SqlParameter("p_pmntid",Types.BIGINT)
							,new SqlParameter("p_pmntamt",Types.DECIMAL)
							,new SqlOutParameter("p_result", Types.VARCHAR));
			in.put("p_districtuid", districtUid);
			in.put("p_pmntcustid", acctCafNo);
			in.put("p_pmntid", pmntId);
			in.put("p_pmntamt", pmntAmt);
			out = processWOPaymentsSP.execute(in);
			if(out.get("p_result") != null)
				result = out.get("p_result").toString();
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during processWOPayments(): " + ex);
			ex.printStackTrace();
		}
		finally {
			processWOPaymentsSP = null;
		}
		return result;
	}
    
    @Transactional
	public boolean dailybatchProcedure() {
		SimpleJdbcCall dailybatchProcedure = null;
		boolean isSuccess = false;
		try{
			dailybatchProcedure = new SimpleJdbcCall(jdbcTemplate).withProcedureName("dailybatch").withoutProcedureColumnMetaDataAccess();			
			dailybatchProcedure.execute();
			isSuccess = true;
		}
		catch(Exception ex) {
			isSuccess = false;
			LOGGER.error("Exception occurred during dailybatchProcedure(): " + ex); 
		}
		finally {
			dailybatchProcedure = null;
		}
		return isSuccess;
	}
	
	/**
	 * 
	 * @param eventDTO
	 * @return
	 */
	@Transactional
	public EventVO processEvents(EventVO eventDTO) {
		SimpleJdbcCall processEventsSP = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			processEventsSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("processEvents").withoutProcedureColumnMetaDataAccess()
					.useInParameterNames("p_custcode", "p_servicecode", "p_amttobereduced", "p_additionalinfo")
					.declareParameters(
							new SqlParameter("p_custcode",Types.BIGINT),
							new SqlParameter("p_servicecode",Types.VARCHAR),
							new SqlParameter("p_amttobereduced",Types.FLOAT),
							new SqlParameter("p_additionalinfo",Types.VARCHAR),
							new SqlOutParameter("o_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_custcode", Long.valueOf(eventDTO.getCustid()));
			in.put("p_servicecode", eventDTO.getServicecode());
			in.put("p_amttobereduced", Float.valueOf(eventDTO.getAmounttobereduced()));
			in.put("p_additionalinfo", eventDTO.getAdditionalinfo());
			out = processEventsSP.execute(in);			
			if(out.get("o_result") != null) {
				if(out.get("o_result").equals("0"))
					eventDTO.setStatus("success");
				else {
					eventDTO.setStatus("failure");
					eventDTO.setErrormsg((String) out.get("o_result"));
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during processEvents(): " + ex); 
		}
		finally {
			processEventsSP = null;
			in = null;
			out = null;
		}
		return eventDTO;
	}
	
	/**
	 * 
	 * @param dto
	 * @return
	 */
	@Transactional
	public TenantWalletVO walletPayment(TenantWalletVO dto) {
		SimpleJdbcCall walletPaymentSP = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			walletPaymentSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("tenantWalletPayment").withoutProcedureColumnMetaDataAccess()
					.useInParameterNames("p_tenantcode", "p_depositrefno", "p_depositamt", "p_loggedinuser", "p_ipaddr")
					.declareParameters(
							new SqlParameter("p_tenantcode",Types.VARCHAR),
							new SqlParameter("p_depositrefno",Types.VARCHAR),
							new SqlParameter("p_depositamt",Types.FLOAT),
							new SqlParameter("p_loggedinuser",Types.VARCHAR),
							new SqlParameter("p_ipaddr",Types.VARCHAR),
							new SqlOutParameter("o_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_tenantcode", dto.getTenantcode());
			in.put("p_depositrefno", dto.getDepositrefno());
			in.put("p_depositamt", Float.valueOf(dto.getDepositamt()));
			in.put("p_loggedinuser", dto.getLoggeduser());
			in.put("p_ipaddr", dto.getIpaddr());
			out = walletPaymentSP.execute(in);			
			if(out.get("o_result") != null) {
				if(out.get("o_result").equals("0"))
					dto.setStatus("success");
				else {
					dto.setStatus("failure");
					dto.setErrormsg("Failed");
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during walletPayment(): " + ex); 
		}
		finally {
			walletPaymentSP = null;
			in = null;
			out = null;
		}
		return dto;
	}
	
	@Transactional
	public String generateChargeProcedure(GenarateChargesVO genarateChargesVO) {
		String status = "Failed";
		SimpleJdbcCall generateChargeProcedure = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			generateChargeProcedure = new SimpleJdbcCall(jdbcTemplate).withProcedureName("generatecharge").withoutProcedureColumnMetaDataAccess()
					.useInParameterNames("p_districtuid", "p_acctcafno", "p_pmntcustid", "p_chargeddate", "p_tenantcode", "p_prodcode", 
							"p_srvccode", "p_featurecode", "p_prodcafno", "p_stbcafno", "p_chargecode", "p_chargedesc", "p_chargefdate", "p_chargetdate", "p_chargeamt", "p_srvctaxamt",
							"p_swatchtaxamt", "p_kisantaxamt", "p_enttaxamt", "p_chargetype", "p_paidamt", "p_ignorebalflag", "p_rsagruid", "p_adjrefid")
					.declareParameters(
							new SqlParameter("p_districtuid",Types.INTEGER),
							new SqlParameter("p_acctcafno",Types.BIGINT),
							new SqlParameter("p_pmntcustid",Types.BIGINT),
							new SqlParameter("p_chargeddate",Types.VARCHAR),
							new SqlParameter("p_tenantcode",Types.VARCHAR),
							new SqlParameter("p_prodcode", Types.VARCHAR),
							new SqlParameter("p_srvccode",Types.VARCHAR),
							new SqlParameter("p_featurecode",Types.VARCHAR),
							new SqlParameter("p_prodcafno",Types.BIGINT),
							new SqlParameter("p_stbcafno",Types.BIGINT),
							new SqlParameter("p_chargecode",Types.VARCHAR),
							new SqlParameter("p_chargedesc",Types.VARCHAR),
							new SqlParameter("p_chargefdate",Types.VARCHAR),
							new SqlParameter("p_chargetdate", Types.VARCHAR),
							new SqlParameter("p_chargeamt",Types.DECIMAL),
							new SqlParameter("p_srvctaxamt",Types.DECIMAL),
							new SqlParameter("p_swatchtaxamt",Types.DECIMAL),
							new SqlParameter("p_kisantaxamt",Types.DECIMAL),
							new SqlParameter("p_enttaxamt",Types.DECIMAL),
							new SqlParameter("p_chargetype", Types.VARCHAR),
							new SqlParameter("p_paidamt", Types.DECIMAL),
							new SqlParameter("p_ignorebalflag", Types.VARCHAR),
							new SqlParameter("p_rsagruid", Types.BIGINT),
							new SqlParameter("p_adjrefid", Types.VARCHAR),
							new SqlOutParameter("p_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_districtuid", Integer.parseInt(genarateChargesVO.getDistrict()));
			in.put("p_acctcafno", Long.parseLong(genarateChargesVO.getAcctCafno()));
			in.put("p_pmntcustid", Long.parseLong(genarateChargesVO.getPmntCustId()));
			in.put("p_chargeddate", genarateChargesVO.getChargedDate());
			in.put("p_tenantcode", genarateChargesVO.getTenantCode());
			in.put("p_prodcode", genarateChargesVO.getProdCode());
			in.put("p_srvccode", genarateChargesVO.getSrvcCode());
			in.put("p_featurecode", genarateChargesVO.getFeatureCode());
			in.put("p_prodcafno", Long.parseLong(genarateChargesVO.getProdCafno()));
			in.put("p_stbcafno", Long.parseLong(genarateChargesVO.getStbCafno()));
			in.put("p_chargecode", genarateChargesVO.getChargeCode());
			in.put("p_chargedesc", genarateChargesVO.getChargeDescription());
			in.put("p_chargefdate", genarateChargesVO.getChargeFdate());
			in.put("p_chargetdate", genarateChargesVO.getChargeTdate());
			in.put("p_chargeamt", new BigDecimal(genarateChargesVO.getChargeAmt()));
			in.put("p_srvctaxamt", new BigDecimal(genarateChargesVO.getSrvcTaxamt()));
			in.put("p_swatchtaxamt", new BigDecimal(genarateChargesVO.getSwatchTaxamt()));
			in.put("p_kisantaxamt", new BigDecimal(genarateChargesVO.getKisanTaxamt()));
			in.put("p_enttaxamt", new BigDecimal(genarateChargesVO.getEntTaxamt()));
			in.put("p_chargetype", genarateChargesVO.getChargeType());
			in.put("p_paidamt",  new BigDecimal(genarateChargesVO.getPaidAmount()));
			in.put("p_ignorebalflag", genarateChargesVO.getIgnoreBalenceFlag());
			in.put("p_rsagruid", new BigDecimal(genarateChargesVO.getRsAgrUid()));
			in.put("p_adjrefid", genarateChargesVO.getAdjrefId());
			out = generateChargeProcedure.execute(in);
			String result = out.get("p_result").toString();
			if(result != null) {
				if(result.equalsIgnoreCase("0")) {
					status = "Success";
				} else {
					status = "Failed";
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during generateChargeProcedure(): " + ex); 
		}
		finally {
			generateChargeProcedure = null;
			in = null;
			out = null;
		}
		return status;
	}
	
}