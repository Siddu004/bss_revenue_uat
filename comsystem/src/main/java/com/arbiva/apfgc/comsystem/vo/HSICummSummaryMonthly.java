package com.arbiva.apfgc.comsystem.vo;

import java.math.BigDecimal;

import javax.persistence.Column;

public class HSICummSummaryMonthly {
	

	private String usageYYYY;
	
	private String usageMM;
	
	
	private String day;
	
	
	private String daydnldusage;
	
	private String dayupldusage;
	
	private String dayTotalUsage;
	
	private BigDecimal planUsage;
	
	
	private String ulh01;
	private String ulh02;
	private String ulh03;
	private String ulh04;
	private String ulh05;
	private String ulh06;
	private String ulh07;
	private String ulh08;
	private String ulh09;
	private String ulh10;
	private String ulh11;
	private String ulh12;
	private String ulh13;
	private String ulh14;
	private String ulh15;
	private String ulh16;
	private String ulh17;
	private String ulh18;
	private String ulh19;
	private String ulh20;
	private String ulh21;
	private String ulh22;
	private String ulh23;
	private String ulh24;

	private String dlh01;
	private String dlh02;
	private String dlh03;
	private String dlh04;
	private String dlh05;
	private String dlh06;
	private String dlh07;
	private String dlh08;
	private String dlh09;
	private String dlh10;
	private String dlh11;
	private String dlh12;
	private String dlh13;
	private String dlh14;
	private String dlh15;
	private String dlh16;
	private String dlh17;
	private String dlh18;
	private String dlh19;
	private String dlh20;
	private String dlh21;
	private String dlh22;
	private String dlh23;
	private String dlh24;
	
	private String sch01;
	private String sch02;
	private String sch03;
	private String sch04;
	private String sch05;
	private String sch06;
	private String sch07;
	private String sch08;
	private String sch09;
	private String sch10;
	private String sch11;
	private String sch12;
	private String sch13;
	private String sch14;
	private String sch15;
	private String sch16;
	private String sch17;
	private String sch18;
	private String sch19;
	private String sch20;
	private String sch21;
	private String sch22;
	private String sch23;
	private String sch24;
	
	public String getSch01() {
		return sch01;
	}

	public void setSch01(String sch01) {
		this.sch01 = sch01;
	}

	public String getSch02() {
		return sch02;
	}

	public void setSch02(String sch02) {
		this.sch02 = sch02;
	}

	public String getSch03() {
		return sch03;
	}

	public void setSch03(String sch03) {
		this.sch03 = sch03;
	}

	public String getSch04() {
		return sch04;
	}

	public void setSch04(String sch04) {
		this.sch04 = sch04;
	}

	public String getSch05() {
		return sch05;
	}

	public void setSch05(String sch05) {
		this.sch05 = sch05;
	}

	public String getSch06() {
		return sch06;
	}

	public void setSch06(String sch06) {
		this.sch06 = sch06;
	}

	public String getSch07() {
		return sch07;
	}

	public void setSch07(String sch07) {
		this.sch07 = sch07;
	}

	public String getSch08() {
		return sch08;
	}

	public void setSch08(String sch08) {
		this.sch08 = sch08;
	}

	public String getSch09() {
		return sch09;
	}

	public void setSch09(String sch09) {
		this.sch09 = sch09;
	}

	public String getSch10() {
		return sch10;
	}

	public void setSch10(String sch10) {
		this.sch10 = sch10;
	}

	public String getSch11() {
		return sch11;
	}

	public void setSch11(String sch11) {
		this.sch11 = sch11;
	}

	public String getSch12() {
		return sch12;
	}

	public void setSch12(String sch12) {
		this.sch12 = sch12;
	}

	public String getSch13() {
		return sch13;
	}

	public void setSch13(String sch13) {
		this.sch13 = sch13;
	}

	public String getSch14() {
		return sch14;
	}

	public void setSch14(String sch14) {
		this.sch14 = sch14;
	}

	public String getSch15() {
		return sch15;
	}

	public void setSch15(String sch15) {
		this.sch15 = sch15;
	}

	public String getSch16() {
		return sch16;
	}

	public void setSch16(String sch16) {
		this.sch16 = sch16;
	}

	public String getSch17() {
		return sch17;
	}

	public void setSch17(String sch17) {
		this.sch17 = sch17;
	}

	public String getSch18() {
		return sch18;
	}

	public void setSch18(String sch18) {
		this.sch18 = sch18;
	}

	public String getSch19() {
		return sch19;
	}

	public void setSch19(String sch19) {
		this.sch19 = sch19;
	}

	public String getSch20() {
		return sch20;
	}

	public void setSch20(String sch20) {
		this.sch20 = sch20;
	}

	public String getSch21() {
		return sch21;
	}

	public void setSch21(String sch21) {
		this.sch21 = sch21;
	}

	public String getSch22() {
		return sch22;
	}

	public void setSch22(String sch22) {
		this.sch22 = sch22;
	}

	public String getSch23() {
		return sch23;
	}

	public void setSch23(String sch23) {
		this.sch23 = sch23;
	}

	public String getSch24() {
		return sch24;
	}

	public void setSch24(String sch24) {
		this.sch24 = sch24;
	}

	private String monthDay;
	
	private String totalDwlSize;
	
	private String totalUplSize;
	
	private String totalUsedSize;
	
	private String subsCount;

	public String getUsageYYYY() {
		return usageYYYY;
	}

	public void setUsageYYYY(String usageYYYY) {
		this.usageYYYY = usageYYYY;
	}

	public String getUsageMM() {
		return usageMM;
	}

	public void setUsageMM(String usageMM) {
		this.usageMM = usageMM;
	}


	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}


	public String getDaydnldusage() {
		return daydnldusage;
	}

	public void setDaydnldusage(String daydnldusage) {
		this.daydnldusage = daydnldusage;
	}

	public String getDayupldusage() {
		return dayupldusage;
	}

	public void setDayupldusage(String dayupldusage) {
		this.dayupldusage = dayupldusage;
	}

	public String getDayTotalUsage() {
		return dayTotalUsage;
	}

	public void setDayTotalUsage(String dayTotalUsage) {
		this.dayTotalUsage = dayTotalUsage;
	}

	public BigDecimal getPlanUsage() {
		return planUsage;
	}

	public void setPlanUsage(BigDecimal planUsage) {
		this.planUsage = planUsage;
	}

	
	public String getUlh01() {
		return ulh01;
	}

	public void setUlh01(String ulh01) {
		this.ulh01 = ulh01;
	}

	public String getUlh02() {
		return ulh02;
	}

	public void setUlh02(String ulh02) {
		this.ulh02 = ulh02;
	}

	public String getUlh03() {
		return ulh03;
	}

	public void setUlh03(String ulh03) {
		this.ulh03 = ulh03;
	}

	public String getUlh04() {
		return ulh04;
	}

	public void setUlh04(String ulh04) {
		this.ulh04 = ulh04;
	}

	public String getUlh05() {
		return ulh05;
	}

	public void setUlh05(String ulh05) {
		this.ulh05 = ulh05;
	}

	public String getUlh06() {
		return ulh06;
	}

	public void setUlh06(String ulh06) {
		this.ulh06 = ulh06;
	}

	public String getUlh07() {
		return ulh07;
	}

	public void setUlh07(String ulh07) {
		this.ulh07 = ulh07;
	}

	public String getUlh08() {
		return ulh08;
	}

	public void setUlh08(String ulh08) {
		this.ulh08 = ulh08;
	}

	public String getUlh09() {
		return ulh09;
	}

	public void setUlh09(String ulh09) {
		this.ulh09 = ulh09;
	}

	public String getUlh10() {
		return ulh10;
	}

	public void setUlh10(String ulh10) {
		this.ulh10 = ulh10;
	}

	public String getUlh11() {
		return ulh11;
	}

	public void setUlh11(String ulh11) {
		this.ulh11 = ulh11;
	}

	public String getUlh12() {
		return ulh12;
	}

	public void setUlh12(String ulh12) {
		this.ulh12 = ulh12;
	}

	public String getUlh13() {
		return ulh13;
	}

	public void setUlh13(String ulh13) {
		this.ulh13 = ulh13;
	}

	public String getUlh14() {
		return ulh14;
	}

	public void setUlh14(String ulh14) {
		this.ulh14 = ulh14;
	}

	public String getUlh15() {
		return ulh15;
	}

	public void setUlh15(String ulh15) {
		this.ulh15 = ulh15;
	}

	public String getUlh16() {
		return ulh16;
	}

	public void setUlh16(String ulh16) {
		this.ulh16 = ulh16;
	}

	public String getUlh17() {
		return ulh17;
	}

	public void setUlh17(String ulh17) {
		this.ulh17 = ulh17;
	}

	public String getUlh18() {
		return ulh18;
	}

	public void setUlh18(String ulh18) {
		this.ulh18 = ulh18;
	}

	public String getUlh19() {
		return ulh19;
	}

	public void setUlh19(String ulh19) {
		this.ulh19 = ulh19;
	}

	public String getUlh20() {
		return ulh20;
	}

	public void setUlh20(String ulh20) {
		this.ulh20 = ulh20;
	}

	public String getUlh21() {
		return ulh21;
	}

	public void setUlh21(String ulh21) {
		this.ulh21 = ulh21;
	}

	public String getUlh22() {
		return ulh22;
	}

	public void setUlh22(String ulh22) {
		this.ulh22 = ulh22;
	}

	public String getUlh23() {
		return ulh23;
	}

	public void setUlh23(String ulh23) {
		this.ulh23 = ulh23;
	}

	public String getUlh24() {
		return ulh24;
	}

	public void setUlh24(String ulh24) {
		this.ulh24 = ulh24;
	}

	public String getDlh01() {
		return dlh01;
	}

	public void setDlh01(String dlh01) {
		this.dlh01 = dlh01;
	}

	public String getDlh02() {
		return dlh02;
	}

	public void setDlh02(String dlh02) {
		this.dlh02 = dlh02;
	}

	public String getDlh03() {
		return dlh03;
	}

	public void setDlh03(String dlh03) {
		this.dlh03 = dlh03;
	}

	public String getDlh04() {
		return dlh04;
	}

	public void setDlh04(String dlh04) {
		this.dlh04 = dlh04;
	}

	public String getDlh05() {
		return dlh05;
	}

	public void setDlh05(String dlh05) {
		this.dlh05 = dlh05;
	}

	public String getDlh06() {
		return dlh06;
	}

	public void setDlh06(String dlh06) {
		this.dlh06 = dlh06;
	}

	public String getDlh07() {
		return dlh07;
	}

	public void setDlh07(String dlh07) {
		this.dlh07 = dlh07;
	}

	public String getDlh08() {
		return dlh08;
	}

	public void setDlh08(String dlh08) {
		this.dlh08 = dlh08;
	}

	public String getDlh09() {
		return dlh09;
	}

	public void setDlh09(String dlh09) {
		this.dlh09 = dlh09;
	}

	public String getDlh10() {
		return dlh10;
	}

	public void setDlh10(String dlh10) {
		this.dlh10 = dlh10;
	}

	public String getDlh11() {
		return dlh11;
	}

	public void setDlh11(String dlh11) {
		this.dlh11 = dlh11;
	}

	public String getDlh12() {
		return dlh12;
	}

	public void setDlh12(String dlh12) {
		this.dlh12 = dlh12;
	}

	public String getDlh13() {
		return dlh13;
	}

	public void setDlh13(String dlh13) {
		this.dlh13 = dlh13;
	}

	public String getDlh14() {
		return dlh14;
	}

	public void setDlh14(String dlh14) {
		this.dlh14 = dlh14;
	}

	public String getDlh15() {
		return dlh15;
	}

	public void setDlh15(String dlh15) {
		this.dlh15 = dlh15;
	}

	public String getDlh16() {
		return dlh16;
	}

	public void setDlh16(String dlh16) {
		this.dlh16 = dlh16;
	}

	public String getDlh17() {
		return dlh17;
	}

	public void setDlh17(String dlh17) {
		this.dlh17 = dlh17;
	}

	public String getDlh18() {
		return dlh18;
	}

	public void setDlh18(String dlh18) {
		this.dlh18 = dlh18;
	}

	public String getDlh19() {
		return dlh19;
	}

	public void setDlh19(String dlh19) {
		this.dlh19 = dlh19;
	}

	public String getDlh20() {
		return dlh20;
	}

	public void setDlh20(String dlh20) {
		this.dlh20 = dlh20;
	}

	public String getDlh21() {
		return dlh21;
	}

	public void setDlh21(String dlh21) {
		this.dlh21 = dlh21;
	}

	public String getDlh22() {
		return dlh22;
	}

	public void setDlh22(String dlh22) {
		this.dlh22 = dlh22;
	}

	public String getDlh23() {
		return dlh23;
	}

	public void setDlh23(String dlh23) {
		this.dlh23 = dlh23;
	}

	public String getDlh24() {
		return dlh24;
	}

	public void setDlh24(String dlh24) {
		this.dlh24 = dlh24;
	}

	public String getMonthDay() {
		return monthDay;
	}

	public void setMonthDay(String monthDay) {
		this.monthDay = monthDay;
	}

	public String getTotalDwlSize() {
		return totalDwlSize;
	}

	public void setTotalDwlSize(String totalDwlSize) {
		this.totalDwlSize = totalDwlSize;
	}

	public String getTotalUplSize() {
		return totalUplSize;
	}

	public void setTotalUplSize(String totalUplSize) {
		this.totalUplSize = totalUplSize;
	}

	public String getTotalUsedSize() {
		return totalUsedSize;
	}

	public void setTotalUsedSize(String totalUsedSize) {
		this.totalUsedSize = totalUsedSize;
	}

	public String getSubsCount() {
		return subsCount;
	}

	public void setSubsCount(String subsCount) {
		this.subsCount = subsCount;
	}
	
}
