/**
 * 
 */
package com.arbiva.apfgc.comsystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.util.ApplictionAutoWires;
import com.arbiva.apfgc.provision.dto.ErrorJsonsDTO;
import com.arbiva.apfgc.provision.dto.ErrorsDTO;
import com.arbiva.apfgc.provision.dto.PendingProvisonErrorsDTO;
import com.xyzinnotech.bss.sms.dto.SmsDTO;

/**
 * @author Arbiva
 *
 */
@RestController
public class ProvisionRestController {
	
	@Autowired
	ApplictionAutoWires aw;
	
	private static final Logger logger = Logger.getLogger(ProvisionRestController.class);
	
	@RequestMapping(value = "/provisonErrors", method = RequestMethod.GET)
	public List<ErrorsDTO> provisonErrors() {
		List<ErrorsDTO> provisionErrorList = new ArrayList<>();
		try {
			provisionErrorList = aw.getProvisionErrorsBusinessServiceImpl().getProvisionErrorList();
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: provisonErrors" +e);
			e.printStackTrace();
		}
		return provisionErrorList;
	}
	
	@RequestMapping(value = "/getErrJsons", method = RequestMethod.GET)
	public List<ErrorJsonsDTO> getErrJsons(@RequestParam("reqid") String reqid) {
		List<ErrorJsonsDTO> ErrorJsonsList = new ArrayList<>();
		try{
			ErrorJsonsList = aw.getProvisionErrorsBusinessServiceImpl().getErrJsons(reqid);
		}
		catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: getErrJsons" +e);
			e.printStackTrace();
		}
		return ErrorJsonsList;
	}
	
	@RequestMapping(value = "/saveProvErrRecycle", method = RequestMethod.GET)
	public ComsHelperDTO saveProvErrRecycle(@RequestParam(value="arr", required=false) String arr) {
		List<ErrorsDTO> provisionErrorList = new ArrayList<>();
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			aw.getProvisionErrorsBusinessServiceImpl().updateProvTables(arr);
			comsHelperDTO.setMessage("Provision Errors Corrected Successfully");
		} catch (Exception e) {
			logger.info("The Exception is CustomerController :: saveProvErrRecycle" +e);
			comsHelperDTO.setMessage("Failed to Correct Provision Errors");
			e.printStackTrace();
		}
		finally{
			provisionErrorList = aw.getProvisionErrorsBusinessServiceImpl().getProvisionErrorList();
			comsHelperDTO.setProvisionErrorList(provisionErrorList);
		}
		return comsHelperDTO;
	}
	
	
	@RequestMapping(value = "/updateProvisionErrors", method = RequestMethod.GET)
	public List<PendingProvisonErrorsDTO> updateProvisionErrors(@RequestParam(value="arr", required=false) String arr) {
		List<PendingProvisonErrorsDTO> provisionErrorList = new ArrayList<>();
		ComsHelperDTO comsHelperDTO = new ComsHelperDTO();
		try {
			if (arr!=null ||arr!=""){
			aw.getProvisionErrorsBusinessServiceImpl().updateProvisionErrors(arr);
			comsHelperDTO.setMessage("Provision Errors Corrected Successfully");
			}
		} catch (Exception e) {
			logger.info("The Exception is CustomerController :: updateProvisionErrors" +e);
			comsHelperDTO.setMessage("Failed to Correct Provision Errors");
			e.printStackTrace();
		}
		finally{
			provisionErrorList = aw.getProvisionErrorsBusinessServiceImpl().getPendingProvisonErrors();
			
		}
		return provisionErrorList;
	}
	
	@RequestMapping(value = "/blacklistSubscribers", method = RequestMethod.GET)
	public String blacklistSubscribers(@RequestParam(value="nwsubscode") String nwsubscode) {
		String status = null;
		try {
				status = aw.getProvisioningBusinessServiceImpl().blacklistSubscribers(nwsubscode);
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: suspendIPTVServices" +e);
			e.printStackTrace();
		}
		finally{

		}
		return status;
	}

	@RequestMapping(value = "/sendSMS", method = RequestMethod.POST)
	public @ResponseBody String sendSMS(@RequestBody SmsDTO smsDto) {
		String status = "";
		try {
			status = aw.getSmsApi().sendSMS(smsDto);
			if(status.isEmpty()) {
				logger.info("The OTP is not send to your mobileno");
			}
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: sendSMS" +e);
			e.printStackTrace();
			status = "FAIL";
		}
		finally{

		}
		return status;
	}
	
	@RequestMapping(value = "/sendSMS", method = RequestMethod.GET, headers = "Accept=*/*")
	public String sendSMS(@RequestParam(value = "mobileNo") String mobileNo, @RequestParam(value = "msg") String msg) {
		String status = "";
		SmsDTO smsDto = new SmsDTO();
		smsDto.setMobileNo(mobileNo);
		smsDto.setMsg(msg);
		try {
			status = aw.getSmsApi().sendSMS(smsDto);
			if(status.isEmpty()) {
				logger.info("The OTP is not send to your mobileno");
			}
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: sendSMS" + e);
			e.printStackTrace();
			status = "FAIL";
		}
		return status;
	}
	
	@RequestMapping(value = "/sendTTSMS", method = RequestMethod.GET,headers="Accept=*/*")
	public String sendTTSMS(@RequestParam(value="mobileNo") String mobileNo,@RequestParam(value="msg") String msg) {
		String status = null;
		SmsDTO smsDto=new SmsDTO();
		smsDto.setMobileNo(mobileNo);
		smsDto.setMsg(msg);
		try {
				status = aw.getSmsApi().sendSMS(smsDto);
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: sendSMS" +e);
			e.printStackTrace();
			status = "FAIL";
		}
		finally{

		}
		return status;
	}
	
	@RequestMapping(value = "/addMSOChannels", method = RequestMethod.GET)
	public String addMSOChannels(@RequestParam(value="distId") String distId, @RequestParam(value="mandId") String mandId,
								 @RequestParam(value="villId") String villId, @RequestParam(value="corpusPackageCode") String corpusPackageCode) {
		String status = null;
		try {
			status = aw.getProvisioningBusinessServiceImpl().addMSOChannels(distId,mandId,villId,corpusPackageCode);
				
				
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: addMSOChannels" +e);
			e.printStackTrace();
		}
		finally{

		}
		return status;
	}
	
	@RequestMapping(value = "/sendPgSMS", method = RequestMethod.GET,headers="Accept=*/*")
	public String sendPgSMS(@RequestParam(value="mobileNo") String mobileNo,
			                @RequestParam(value="msg") String msg) {
		
		String status = null;
		SmsDTO smsDto=new SmsDTO();
		smsDto.setMobileNo(mobileNo);
		smsDto.setMsg(msg);
		try {
			logger.info("ProvisionRestController :: sendPgSMS() :: Start");
			status = aw.getSmsApi().sendSMS(smsDto);
			logger.info("ProvisionRestController :: sendPgSMS() :: End");
		} catch (Exception e) {
			logger.error("ProvisionRestController :: sendPgSMS() ::" +e);
			e.printStackTrace();
			status = "FAIL";
		}
		return status;
	}
	
	@RequestMapping(value = "/pendingProvison", method = RequestMethod.GET)
	public List<PendingProvisonErrorsDTO> pendingProvison() {
		List<PendingProvisonErrorsDTO> PendingProvisonErrorsList = new ArrayList<>();
		try {
			PendingProvisonErrorsList = aw.getProvisionErrorsBusinessServiceImpl().getPendingProvisonErrors();
		} catch (Exception e) {
			logger.info("The Exception is ProvisionRestController :: provisonErrors" +e);
			e.printStackTrace();
		}
		return PendingProvisonErrorsList;
	}
}
