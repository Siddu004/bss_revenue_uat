package com.arbiva.apfgc.comsystem.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "products")
public class Products extends Base {
	

	private static final long serialVersionUID = 1L;

	/**
	 * Default 
	 */
	public Products() {

	}

	/**
	 * @param productsPK
	 * @param tenants
	 * @param request
	 * @param productInfoVo
	 * @throws ParseException 
	 */
	
	
	@EmbeddedId
    private ProductsPK id;

	public ProductsPK getId() {
        return this.id;
    }

	public void setId(ProductsPK id) {
        this.id = id;
    }


	@Column(name = "prodname", length = 100, unique = true)
    @NotNull
    private String prodname;
	
	@Column(name = "custtypelov", length = 100)
    private String custTypeLov;

	@Column(name = "validfrom")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date validfrom;

	@Column(name = "validto")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date validto;
	
	@Column(name = "effectiveto")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date effectiveTo;
	
	@Column(name = "prodtype")
    private Character prodType;
    

	@Column(name = "lockindays")
    private Integer lockInDays;
	

	@Column(name = "durationdays")
    private Integer durationDays;
/*    
	@Column(name = "glcode", length = 12)
    private String glcode;*/
	
	

	public Integer getDurationDays() {
		return durationDays;
	}

	public void setDurationDays(Integer durationDays) {
		this.durationDays = durationDays;
	}

/*	public String getGlcode() {
		return glcode;
	}

	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}*/

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public Character getProdType() {
		return prodType;
	}

	public void setProdType(Character prodType) {
		this.prodType = prodType;
	}

	public String getCustTypeLov() {
		return custTypeLov;
	}

	public void setCustTypeLov(String custTypeLov) {
		this.custTypeLov = custTypeLov;
	}


	public String getProdname() {
		return prodname;
	}

	public void setProdname(String prodname) {
		this.prodname = prodname;
	}

	public Date getValidfrom() {
		return validfrom;
	}

	public void setValidfrom(Date validfrom) {
		this.validfrom = validfrom;
	}

	public Date getValidto() {
		return validto;
	}

	public void setValidto(Date validto) {
		this.validto = validto;
	}

	public Integer getLockInDays() {
		return lockInDays;
	}

	public void setLockInDays(Integer lockInDays) {
		this.lockInDays = lockInDays;
	}

	
}
