package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.arbiva.apfgc.comsystem.vo.PaymentVO;


/**
 * 
 * @author Lakshman
 * 
 */
@Entity
@Table(name = "payments")
public class Payments extends Base {

	public Payments() {

	}

	public Payments(OLPayment olp ) {
		this.pmntId = olp.getPmntId();
		this.acctCafNo = olp.getAcctCafNo();
		this.custId = olp.getCustId();
		this.aadharNo = olp.getAadharNo();
		this.pmntMode = olp.getPmntMode();
		this.pmntDate = olp.getPmntDate();
		this.pmntRefNo = olp.getPmntRefNo();
		this.pmntAmt = olp.getPmntAmt();
		this.status = olp.getStatus();
		this.createdOn = olp.getCreatedOn();
		this.createdBy = olp.getCreatedBy();
		this.createdIPAddr = olp.getCreatedIPAddr();
		this.modifiedOn = olp.getModifiedOn();
		this.modifiedBy = olp.getModifiedBy();
		this.modifiedIPAddr = olp.getModifiedIPAddr();
		this.deActivatedOn = olp.getDeActivatedOn();
		this.deActivatedBy = olp.getDeActivatedBy();
		this.deActivatedIpAddr = olp.getDeActivatedIpAddr();
	}
	
	public Payments(PaymentVO paymentVO) {
		this.acctCafNo = paymentVO.getCafNo();
		this.aadharNo = paymentVO.getAadharNumber();
		this.pmntAmt = paymentVO.getPaidAmount();
		this.pmntDate = Calendar.getInstance();
		this.pmntMode = paymentVO.getPaymentMode();
	}

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pmntid")
	private Long pmntId;

	@Column(name = "acctcafno")
	private long acctCafNo;

	@Column(name = "aadharno")
	private String aadharNo;

	@Column(name = "pmntcustid")
	private Long custId;

	@Column(name = "pmntmodelov")
	private String pmntMode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pmntdate", nullable = false)
	private Calendar pmntDate;

	@Column(name = "pmntrefno")
	private String pmntRefNo;

	@Column(name = "pmntamt")
	private float pmntAmt;

	@Column(name = "deactivatedby")
	private String deActivatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deactivatedon")
	private Date deActivatedOn;

	@Column(name = "deactivatedipaddr")
	private String deActivatedIpAddr;

	@ManyToOne
	@JoinColumn(name = "acctcafno", referencedColumnName = "cafno", nullable = false, insertable = false, updatable = false)
	private Caf cafNo;

	@ManyToOne
	@JoinColumn(name = "pmntcustid", referencedColumnName = "custid", nullable = false, insertable = false, updatable = false)
	private Customer customerId;

	public Caf getCafNo() {
		return cafNo;
	}

	public void setCafNo(Caf cafNo) {
		this.cafNo = cafNo;
	}

	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	public Long getPmntId() {
		return pmntId;
	}

	public void setPmntId(Long pmntId) {
		this.pmntId = pmntId;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public long getAcctCafNo() {
		return acctCafNo;
	}

	public void setAcctCafNo(long acctCafNo) {
		this.acctCafNo = acctCafNo;
	}

	public String getPmntMode() {
		return pmntMode;
	}

	public void setPmntMode(String pmntMode) {
		this.pmntMode = pmntMode;
	}

	public String getPmntRefNo() {
		return pmntRefNo;
	}

	public void setPmntRefNo(String pmntRefNo) {
		this.pmntRefNo = pmntRefNo;
	}

	public Calendar getPmntDate() {
		return pmntDate;
	}

	public void setPmntDate(Calendar pmntDate) {
		this.pmntDate = pmntDate;
	}

	public float getPmntAmt() {
		return pmntAmt;
	}

	public void setPmntAmt(float pmntAmt) {
		this.pmntAmt = pmntAmt;
	}

	public String getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(String deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public Date getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(Date deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public String getDeActivatedIpAddr() {
		return deActivatedIpAddr;
	}

	public void setDeActivatedIpAddr(String deActivatedIpAddr) {
		this.deActivatedIpAddr = deActivatedIpAddr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
