/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class VillagesBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="villageuid")
	private String villageUid;
	
	@Column(name="villageslno")
	private String villageSlno;
	
	@Column(name="villagename")
	private String villageName;
	
	@Column(name="districtuid")
	private String districtUid;
	
	@Column(name="districtname")
	private String districtName;
	
	@Column(name="mandalslno")
	private String mandalSlno;
	
	@Column(name="mandalname")
	private String mandalName;
	
	@Column(name="enttax_zone")
	private String enttaxZone;

	public String getVillageUid() {
		return villageUid;
	}

	public void setVillageUid(String villageUid) {
		this.villageUid = villageUid;
	}

	public String getVillageSlno() {
		return villageSlno;
	}

	public void setVillageSlno(String villageSlno) {
		this.villageSlno = villageSlno;
	}

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	public String getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(String districtUid) {
		this.districtUid = districtUid;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(String mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public String getMandalName() {
		return mandalName;
	}

	public void setMandalName(String mandalName) {
		this.mandalName = mandalName;
	}

	public String getEnttaxZone() {
		return enttaxZone;
	}

	public void setEnttaxZone(String enttaxZone) {
		this.enttaxZone = enttaxZone;
	}
	
}
