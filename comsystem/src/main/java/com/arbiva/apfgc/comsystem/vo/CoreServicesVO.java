/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

/**
 * @author Arbiva
 *
 */
public class CoreServicesVO {
	
	private String name;
	
	private String code;
	
	private String effFrom;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEffFrom() {
		return effFrom;
	}

	public void setEffFrom(String effFrom) {
		this.effFrom = effFrom;
	}

}
