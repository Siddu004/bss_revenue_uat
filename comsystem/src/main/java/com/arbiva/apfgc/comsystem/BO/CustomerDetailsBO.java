package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author kiran
 *
 */
@Entity
public class CustomerDetailsBO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="agorahsisubscode")
	private String ontId;
	
	@Column(name="custtypelov")
	private String custType;
	
	@Column(name="custname")
	private String custName;
	
	@Column(name="address")
	private String custAddr;
	
	@Column(name="contactmob")
	private String contactMob;
	
	@Column(name="apsfluniqueid")
	private String apsflUniqueId;
	
	@Column(name="cafno")
	private String cafNo;

	public String getOntId() {
		return ontId;
	}

	public void setOntId(String ontId) {
		this.ontId = ontId;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAddr() {
		return custAddr;
	}

	public void setCustAddr(String custAddr) {
		this.custAddr = custAddr;
	}

	public String getContactMob() {
		return contactMob;
	}

	public void setContactMob(String contactMob) {
		this.contactMob = contactMob;
	}

	public String getApsflUniqueId() {
		return apsflUniqueId;
	}

	public void setApsflUniqueId(String apsflUniqueId) {
		this.apsflUniqueId = apsflUniqueId;
	}

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

}
