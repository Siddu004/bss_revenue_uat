/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.BO.BalAdjCafinvDtslBO;
import com.arbiva.apfgc.comsystem.BO.BalanceAdjustmentBO;
import com.arbiva.apfgc.comsystem.BO.CafOldPackagesBO;
import com.arbiva.apfgc.comsystem.BO.ChangePkgBO;
import com.arbiva.apfgc.comsystem.BO.CustomerCafBO;
import com.arbiva.apfgc.comsystem.BO.CustomerDetailsBO;
import com.arbiva.apfgc.comsystem.BO.FingerPrintBO;
import com.arbiva.apfgc.comsystem.BO.HsiBO;
import com.arbiva.apfgc.comsystem.BO.LmoDetailsBO;
import com.arbiva.apfgc.comsystem.BO.MonthlyPaymentBO;
import com.arbiva.apfgc.comsystem.BO.NoOfSTBsBO;
import com.arbiva.apfgc.comsystem.BO.OntIdCustTypeBO;
import com.arbiva.apfgc.comsystem.BO.TerminatePkgBO;
import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.dto.ComsmobilehelperDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CpeStock;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.HSICummSummaryMonthlyCustViewDTO;
import com.arbiva.apfgc.comsystem.model.HSICummSummaryMonthlyViewDTO;
import com.arbiva.apfgc.comsystem.model.MultiAction;
import com.arbiva.apfgc.comsystem.model.Payments;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.invAdjustment;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryPreviousMonthVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneNoVO;

/**
 * @author Lakshman
 *
 */
@Repository
public class CafDao {
	
	private static final Logger logger = Logger.getLogger(CafDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCafs(Caf caf) {
		getEntityManager().remove(caf);
		getEntityManager().flush();
	}

	public Caf findByCafNo(Long cafNo) {
		Caf caf = new Caf();
		StringBuilder builder = null;
		TypedQuery<Caf> query = null;
		try {
			logger.info("START::findByCafNo()");
			builder = new StringBuilder(" FROM ").append(Caf.class.getSimpleName()).append(" WHERE cafno =:cafNo ");
			query = getEntityManager().createQuery(builder.toString(), Caf.class);
			query.setParameter("cafNo", cafNo);
			caf = query.getSingleResult();
		} catch(Exception e) {
			logger.error("The Exception is CafDao::findByCafNo " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return caf;
	}
	
	@SuppressWarnings("unchecked")
	public List<Caf> findAllCafs() {
		return (List<Caf>) getEntityManager().createQuery("Select caf from " + Caf.class.getSimpleName() + " caf").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Caf> findListedCafs(List<Long> cafLists) {

		StringBuilder builder= new StringBuilder();
		//builder.append("SELECT * FROM cafs cf where cf.cafno in(");
		builder.append("SELECT cf.cafno,cf.cafdate,cf.cpeplace,cf.status FROM cafs cf where cf.cafno in(");
		//Query query = null;

		List<Object[]> cafList1=new ArrayList<Object[]>(); 
		List<Caf> cafList=new ArrayList<Caf>(); 
		try{
			for(Long cafno : cafLists){
				builder.append(cafno);
				builder.append(",");
			}
			builder=builder.replace(builder.lastIndexOf(","),builder.lastIndexOf(",")+1 , "");
			builder.append(")");
			logger.info(builder);
			System.out.println("caf==>"+builder);
			cafList1 =  getEntityManager().createNativeQuery(builder.toString()).getResultList();
			cafList=convertTOCAF(cafList1);
		}catch(Exception e){
			logger.error("EXCEPTION::searchCafDetails() " + e);
		}
		return cafList;
	}
	
	private List<Caf> convertTOCAF(List<Object[]> cafObject){
		
		List<Caf> cafList=new ArrayList<Caf>();
		
		for(Object[] obj : cafObject){
			Caf caf=new Caf();
			BigInteger cagInt =  (BigInteger)obj[0];
			java.sql.Date date = (java.sql.Date)obj[1];
			int stat=(byte)obj[3];
			Calendar cal = new GregorianCalendar();
			cal.setTime(date);
			caf.setCafNo(cagInt.longValue());
			caf.setCafDate(cal);
			caf.setCpePlace((String)obj[2]);
			caf.setStatus(stat);
			cafList.add(caf);
		}
		
		
		return cafList;
	}

	public Caf saveCaf(Caf caf) {
		return getEntityManager().merge(caf);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> searchCafDetails(MultiAction multiAction, PageObject pageObject) {
		List<Object[]> cafList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		String orderByClause = "";
		Query query = null;
		Query querycount = null;
		String searchParameter = "";
		
		try {
			if(!multiAction.getTenantType().equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode())) {
				whereClause = "and cf.lmocode = '" +multiAction.getTenantCode()+"'";
			}
			if(multiAction.getOrganizationName() != null && !multiAction.getOrganizationName().isEmpty()) {
				whereClause = " and c.custname like '%"+multiAction.getOrganizationName()+"%' ";
			}
			if(multiAction.getCafSubType() != null && !multiAction.getCafSubType().isEmpty()) {
				whereClause = " and c.enttypelov = '"+multiAction.getCafSubType()+"' ";
			}
			if(multiAction.getCafType() != null && !multiAction.getCafType().isEmpty()) {
				whereClause = whereClause + " and cf.custtypelov = '"+multiAction.getCafType()+"' ";
			}
			
			if((multiAction.getEffectiveFrom() != null && multiAction.getEffectiveTo() != null) && (!multiAction.getEffectiveFrom().isEmpty() && !multiAction.getEffectiveTo().isEmpty()) ) {
				Date fromDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveFrom());
				Date toDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveTo());
				whereClause = whereClause + " and cf.cafdate between '"+ new SimpleDateFormat("yyyy-MM-dd").format(fromDate)+"' and '" + new SimpleDateFormat("yyyy-MM-dd").format(toDate)+"'";
			} 
			if(multiAction.getStatus() != null && !multiAction.getStatus().isEmpty()) {
				if(multiAction.getStatus().equalsIgnoreCase("0")) {
					whereClause = whereClause + " and (cf.status = "+multiAction.getStatus()+" OR cf.status = "+ComsEnumCodes.Caf_Edit_BulkUpload_status.getStatus()+") ";
				} else {
					whereClause = whereClause + " and cf.status = "+multiAction.getStatus()+" ";
				}
			}
			if(multiAction.getCafNo() != null ) {
				whereClause = whereClause + " and cf.cafno = " + multiAction.getCafNo();
			} 
			if(multiAction.getAadharNo() != null) {
				if(!multiAction.getAadharNo().isEmpty()) {
					whereClause = whereClause + " and cf.aadharno = '" + multiAction.getAadharNo() + "'";
				}
			}
			if(multiAction.getApsflTrackId() != null && !multiAction.getApsflTrackId().isEmpty() ) {
				whereClause = whereClause + " and cf.apsfluniqueid like '%"+multiAction.getApsflTrackId()+"%'";
			}
			if(multiAction.getDistrict() != null && !multiAction.getDistrict().isEmpty()) {
				whereClause = whereClause + " and cf.inst_district = "+multiAction.getDistrict()+" ";
			}
			if(multiAction.getMandal() != null && !multiAction.getMandal().isEmpty()) {
				whereClause = whereClause + " and cf.inst_mandal = "+multiAction.getMandal()+" ";
			}
			if(multiAction.getPopName() != null && !multiAction.getPopName().isEmpty()) {
				whereClause = whereClause + " and cf.pop_substnno = '"+multiAction.getPopName()+"' ";
			}
			if (pageObject != null) {
				if (pageObject.getSearchParameter().indexOf("/") > 0) {
					try {
						searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
					} catch (Exception e) {
						searchParameter = pageObject.getSearchParameter();
					}
				} else {
					searchParameter = pageObject.getSearchParameter();
				}
				whereClause = whereClause + " and (c.custname like '%" + searchParameter + "%' or c.custtypelov like '%"+ searchParameter + "%' or cf.apsfluniqueid like '%" + searchParameter + "%' ";
				whereClause = whereClause + " or cf.cpeplace like '%" + searchParameter + "%'  or cf.lmocode like '%"+ searchParameter + "%' or cf.cafno like '%" + searchParameter + "%' ";
				whereClause = whereClause + " or cf.aadharno like '%" + searchParameter + "%' or cf.cafdate like '%"+ searchParameter + "%' ) ";
				orderByClause = " ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder();
			}
			try {
				//Query Modified on 15_March_2018 by Nagarjuna
				//merged code given by nagarjuna_4_5_2018
				builder.append(" SELECT cf.cafno, cf.aadharno, DATE_FORMAT(cf.cafdate,'%d/%m/%Y'), w.statusdesc, c.custname, ");
				builder.append(" (SELECT GROUP_CONCAT(DISTINCT p1.prodname) products FROM cafprods cp1,products p1 WHERE cp1.parentcafno = cf.cafno AND cp1.tenantcode = p1.tenantcode AND cp1.prodcode = p1.prodcode AND cf.cafdate BETWEEN p1.effectivefrom AND p1.effectiveto ) products, ");
				builder.append(" c.lname, c.enttypelov, cf.pop_substnno, cf.custid, cf.inst_city_village, cf.inst_district, cf.inst_mandal, c.billfreqlov, cf.status, cf.lmocode, ");
				builder.append(" (select substnname from substations where substnuid = cf.pop_substnno) as popname, cf.apsfluniqueid, cf.inst_addr1, cf.inst_addr2, cf.contactmob, cf.cpeplace, cf.contactperson, ");
				builder.append(" (select group_concat(phoneno) from cafsrvcphonenos where parentcafno = cf.cafno) phoneNo, cf.custtypelov caftype, ");
				builder.append(" (SELECT cafno FROM cafprods cp WHERE cp.parentcafno = cf.cafno ");
				builder.append(" AND EXISTS (SELECT 1 FROM cafsrvcs cs WHERE cs.cafno=cp.cafno AND cs.tenantcode=cp.tenantcode AND cs.prodcode=cp.prodcode AND cs.parentcafno=cp.parentcafno AND cs.status in("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+")) ");
				builder.append(" AND EXISTS (SELECT 1 FROM products p  WHERE p.tenantcode=cp.tenantcode AND p.prodcode=cp.prodcode AND p.prodtype='"+ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()+"' )) prodcafno ");
				builder.append(" FROM cafs cf, customermst c ,wipstages w WHERE cf.custid = c.custid AND w.appcode = 'COMS' AND w.statuscode = cf.status "+whereClause + " GROUP BY cf.cafno "+orderByClause+" ");
				logger.info(builder);
				query = getEntityManager() .createNativeQuery(builder.toString());
				if(pageObject != null) {
					querycount = getEntityManager() .createNativeQuery(builder.toString());
					long totalDisplayCount = querycount.getResultList().size();
					pageObject.setTotalDisplayCount(String.valueOf(totalDisplayCount));
					
					query.setFirstResult(pageObject.getMinSize());
					query.setMaxResults(pageObject.getMaxSize());
				}
				cafList = query.getResultList();
			} catch(Exception ex){
				logger.error("EXCEPTION::searchCafDetails() " + ex);
			}finally{
				builder = null;
				query = null;
				whereClause = null;
			}
		} catch(Exception e) {
			logger.error("EXCEPTION::searchCafDetails() " + e);
			e.printStackTrace();
		}
		return cafList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getPaymentDetails(Long cafNo) {
		List<Object[]> cafList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("SELECT prodname, GROUP_CONCAT(srvcname SEPARATOR ' , ') as srvcname, rentalChrg1, rentaltax, prodtype, DATE_FORMAT(actdate,'%d/%m/%Y') actdate, prodcode, tenantcode, rsagruid ");
		builder.append(" FROM(select prodname,srvcname, substr(rentalChrg,1,instr(rentalChrg,',')-1) rentalChrg1, ");
		builder.append(" substr(rentalChrg,instr(rentalChrg,',')+1) rentaltax, prodtype, actdate, prodcode, tenantcode, rsagruid from (select p.prodname, s.srvcname, ");
		builder.append(" get_prodcharge(csrv.tenantcode,csrv.prodcode,'',NULL,'1',DATE_FORMAT(date(now()),'%Y%m%d'), ");
		builder.append(" get_taxregion(cf.inst_district, cf.inst_mandal, cf.inst_city_village, DATE_FORMAT(CURRENT_DATE(),'%Y%m%d')),'') rentalChrg, p.prodtype, csrv.actdate, ");
		builder.append(" p.prodcode, cp.tenantcode, cp.rsagruid ");
		builder.append(" from cafs cf, cafsrvcs csrv, products p, srvcs s, cafprods cp where cf.cafno = "+cafNo+" ");
		builder.append(" and cf.cafno = csrv.parentcafno and csrv.tenantcode = p.tenantcode and csrv.prodcode = p.prodcode and CURRENT_DATE() BETWEEN p.effectivefrom AND p.effectiveto ");
		builder.append(" AND CURRENT_DATE() BETWEEN s.effectivefrom AND s.effectiveto and cf.cafno = cp.parentcafno and cp.tenantcode = p.tenantcode and cp.prodcode = p.prodcode ");
		builder.append(" and csrv.srvccode = s.srvccode ) x) x1 group by prodname, rentalChrg1, rentaltax, prodtype, actdate, prodcode, tenantcode, rsagruid ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cafList = query.getResultList();
		}catch(Exception ex){
			logger.error("EXCEPTION::searchCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return cafList;
	}

	@SuppressWarnings("unchecked")
	public List<MonthlyPaymentBO> getMonthlyCafDetails(String mobileNo, String tenantCode, Integer custId) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("");
		Query query = null;
		try {
			if(custId != null) {
				builder.append( "SELECT c.custname, c.custcode, c.regbal + c.chargedbal AS regbal, ");
				builder.append(" c.custid, c.custtypelov, c.lname, c.custdistuid,c.pocmob1 FROM customermst c where c.custid='"+custId+"' and c.lmocode = '"+tenantCode+"' and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ");
			} else if(mobileNo != null && !mobileNo.isEmpty()) {
				builder.append(" SELECT c.custname, c.custcode, c.regbal + c.chargedbal AS regbal, ");
				builder.append(" c.custid, c.custtypelov, c.lname, c.custdistuid,c.pocmob1 FROM customermst c where c.pocmob1='"+mobileNo+"' and c.lmocode = '"+tenantCode+"' and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ");
			}
			query = getEntityManager() .createNativeQuery(builder.toString(), MonthlyPaymentBO.class);
			cafList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return cafList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCustomerInformationByTenantCode(String tenantCode) {
		List<Object[]> customerList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("SELECT cf.cafno, cf.custtypelov, cf.custcode, c.fname, c.pocmob1, c.addr1, c.lname, c.addr2, ");
		builder.append("c.locality, v.villagename FROM cafs cf, customers c, villages v where cf.lmocode = '"+tenantCode+"' and cf.custid = c.custid and v.villageuid = c.city_village ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			customerList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return customerList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getApproveCafDetailsByTenantCode(String tenantCode) {
		List<Object[]> cafList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("SELECT cafno, GROUP_CONCAT(prodname) products, GROUP_CONCAT(srvcname) srvcs , SUM(CAST(substr(chrg,1,instr(chrg,',')-1) AS DECIMAL(14,2) )) prodcharge, ");
		builder.append("SUM(CAST(substr(chrg,instr(chrg,',')+1) AS DECIMAL(14,2))) prodtax, GROUP_CONCAT(prodcode) cafprods, GROUP_CONCAT(srvccode) cafsrvcs, tenantcode from (SELECT cf.cafno, p.prodname, s.srvcname, ");
		builder.append("get_prodcharge(cp.tenantcode,cp.prodcode,'',NULL,DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'GENERAL','') chrg, cp.prodcode, cs.srvccode, cp.tenantcode, cf.createdon ");
		builder.append("from cafs cf, cafsrvcs cs, cafprods cp, products p, srvcs s where cf.cafno=cp.cafno AND cf.cafno=cs.cafno AND cp.tenantcode=cs.tenantcode AND cp.prodcode=cs.prodcode and cp.tenantcode=p.tenantcode ");
		builder.append("and cp.prodcode = p.prodcode and cs.srvccode = s.srvccode AND cp.tenantcode = '"+tenantCode+"' and cf.status = 1 and cp.status=1 and cs.status=1 ) x group by cafno order by createdon desc ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cafList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getApproveCafDetailsByTenantCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return cafList;
	}
	
	public Integer getMaxOltOnuNumber(String olt_portid, String oltId) {
		Integer oltOnuNumber = 0;
		StringBuilder builder = new StringBuilder("SELECT max(olt_onuid) from cafs where olt_portid = '"+olt_portid+"' and olt_id = '"+oltId+"' ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			Integer result = Integer.parseInt(query.getSingleResult().toString());
			oltOnuNumber = result == null ? -1 : result;
		} catch(Exception ex){
			logger.error("EXCEPTION::getApproveCafDetailsByTenantCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return oltOnuNumber;
	}
	
	public Float getProdChargeByRegionCode(String tenantCode, String prodCode, String regionCode) {
		Float prodTax = 0.00f;
		StringBuilder builder = new StringBuilder("SELECT SUBSTR(chrg,instr(chrg,',')+1) prodtax "
				+ "From(select get_prodcharge('"+tenantCode+"', '"+prodCode+"','', NULL, '1,2,3', DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'),'"+regionCode+"','') chrg) x ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			prodTax = Float.parseFloat(query.getSingleResult().toString());
		} catch(Exception ex){
			logger.error("EXCEPTION::getProdChargeByRegionCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return prodTax;
	}
	
	public String getRegionCodeByPinCode(String district, String mandal, String village) {
		String regionCode = "";
		StringBuilder builder = new StringBuilder("SELECT enttax_zone FROM villages where districtuid = '"+district+"' and mandalslno = '"+mandal+"' and villageslno = '"+village+"' ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			regionCode = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getRegionCodeByPinCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return regionCode;
		
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCafCharges(Long cafNo) {
		List<Object[]> cafChargesList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("select cc.chargecode, cc.chargeamt, cp.cpe_model, ccs.chargetypeflag, c.cafdate, c.inst_district, ");
		builder.append("c.inst_mandal, c.inst_city_village from cafcharges cc, cafs c, chargecodes ccs, cpe_profilemaster cp ");
		builder.append("where cc.cafno="+cafNo+" and cc.cafno = c.cafno and cc.chargecode = ccs.chargecode and c.cpeprofileid = cp.profile_id ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cafChargesList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCafCharges() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return cafChargesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafFeatureService(Long cafNo) {
		List<Object[]> cafFeatureServiceList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("select cc.chargecode, cc.chargeamt, cp.cpe_model, from cafcharges cc, cafs c, chargecodes ccs, ");
		builder.append("cpe_profilemaster cp where cc.cafno="+cafNo+" and cc.cafno = c.cafno and cc.chargecode = ccs.chargecode and c.cpeprofileid = cp.profile_id ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cafFeatureServiceList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCafCharges() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return cafFeatureServiceList;
	}

	public Object[] getCafChargeTaxPecentage(String chargeCode, String district, String mandal, String village) {
		Object[] tax = null;
		StringBuilder builder = new StringBuilder("SELECT SUBSTR(tax,1,instr(tax,',')-1) tax, SUBSTR(tax,instr(tax,',')+1) taxabs FROM( select get_taxpercharge('', '"+chargeCode+"', ");
		builder.append("get_taxregion('"+district+"', '"+mandal+"', '"+village+"', DATE_FORMAT(CURRENT_DATE(),'%Y%m%d')), DATE_FORMAT(CURRENT_DATE(),'%Y%m%d')) tax) x1 ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			tax = (Object[]) query.getSingleResult();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCafChargeTaxPecentage() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return tax;
	}
	
	public Object[] getCpeChargeDetailsForMobileApp(String chargeCode, String region) {
		Object[] tax = null;
		StringBuilder builder = new StringBuilder("SELECT SUBSTR(tax,1,instr(tax,',')-1) tax, SUBSTR(tax,instr(tax,',')+1) taxabs FROM( select get_taxpercharge('', '"+chargeCode+"', '"+region+"', DATE_FORMAT(CURRENT_DATE(),'%Y%m%d')) tax) x1 ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			tax = (Object[]) query.getSingleResult();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCpeChargeDetailsForMobileApp() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return tax;
	}
	//merged in code given by nagarjuna
	/*@SuppressWarnings("unchecked")
    public List<Object[]> findCafDaoByCafNo(Long custId) {
        List<Object[]> cafDaoList = new ArrayList<Object[]>();
        StringBuilder builder = new StringBuilder("SELECT cf.cafno, ca.depbal, ca.regbal, CASE WHEN cf.status = 6 THEN 'Active' ");
        builder.append(" WHEN cf.status = 2 THEN 'Pending For Provisioning' WHEN cf.status = 7 THEN 'Suspended' WHEN cf.status = 8 THEN 'De-activated' END status, IFNULL(DATE_FORMAT(cf.actdate,'%d/%m/%Y'), 'NA') ");
        builder.append(" from cafs cf, cafacct ca WHERE cf.custid = "+custId+" and cf.cafno = ca.acctcafno and (cf.status = 6 OR cf.status = 2 OR cf.status = 7 OR cf.status = 8) ");
        Query query = null;
        try {
            query = getEntityManager() .createNativeQuery(builder.toString());
            cafDaoList = query.getResultList();
        }catch (Throwable e) {
          logger.error("EXCEPTION::getMonthlyCafDetails() "+e.getMessage());
        }finally{
        	query = null;
        	builder = null;
        }
        return cafDaoList;
    }*/
	
	@SuppressWarnings("unchecked")
	public List<CustomerCafBO> findCafDaoByCafNo(String tenantCode, String tenantType, PageObject pageObject ) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		long count = 0l;
		Query countQuery = null;
		String searchParameter = "";
		try {
			logger.info("START::findCustomers()");
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode()) && tenantType.equalsIgnoreCase(ComsEnumCodes.Call_Center_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode = '"+tenantCode+"' ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.MSP_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.MSP_Tenant_Type.getCode()+"' AND partnercode = '"+tenantCode+"')) ";
			}
			builder.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, cf.cpeslno, CASE WHEN cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" THEN 'Active' ");
			builder.append(" WHEN cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" THEN 'Pending For Provisioning' WHEN cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" THEN 'Suspended' WHEN cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+" THEN 'De-activated' END STATUS, ");
			builder.append(" IFNULL(DATE_FORMAT(cf.actdate,'%d/%m/%Y'), 'NA') actdate, cust.aadharno,cf.apsfluniqueid FROM cafs cf, customermst cust ");
			builder.append(" WHERE cf.custid = cust.custid "+whereClause+" ");
			
			if(pageObject.getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
				} catch(Exception e) {
					searchParameter = pageObject.getSearchParameter();
				}
			} else {
				searchParameter = pageObject.getSearchParameter();
			}
			String like="AND (cafno LIKE '%"+searchParameter+"%' OR cust.depbal LIKE '%"+searchParameter+"%'  OR cust.regbal LIKE '%"+searchParameter+"%'  OR cf.lmocode LIKE '%"+searchParameter+"%' OR custname LIKE '%"+searchParameter+"%' OR mname LIKE '%"+ searchParameter+"%' OR lname LIKE '%"+searchParameter+"%'  OR cpemacaddr LIKE '%"+searchParameter+"%' OR cf.status LIKE '%"+searchParameter+"%'  OR cust.aadharno LIKE '%"+searchParameter+"%' OR cf.actdate LIKE '%"+searchParameter+"%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			cafsList = query.getResultList();

			logger.info("END::findCustomers()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cafsList;
	}
		
	public Object[] getAllParticularCPE(Long cafNo) {
		Object[] cafDao = null;
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			builder.append("SELECT cfs.longitude,cfs.lattitude,(select om.pop_oltlabelno from oltmaster om where cfs.olt_id = om.pop_olt_serialno and cfs.pop_substnno = om.pop_substnuid) oltid, ");
			builder.append(" CONCAT(cfs.olt_portid,'(', IFNULL(cfs.portsplit,'NA'),')') portNo, ");
			builder.append(" ss.substnname, cfs.inst_addr1,v.villagename, m.mandalname , d.districtname, cp.cpe_model , cfs.cpeslno, ");
			builder.append(" (SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='"+ComsEnumCodes.ONU_INSTALLATION_CODE.getCode()+"') AS cpeInstChrg,");
			builder.append(" (SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='"+ComsEnumCodes.ONU_COST_CODE.getCode()+"') AS cpecost ,");
			builder.append(" cfs.inst_pin, cfs.inst_addr2, ");
			builder.append(" (SELECT totalemicnt FROM cafcharges WHERE cafno ="+cafNo+" AND chargecode='"+ComsEnumCodes.ONU_INSTALLATION_CODE.getCode()+"') AS cpeInstCount, ");
			builder.append(" cfs.cpemacaddr,cfs.cpeleaseyn ");
			builder.append(" FROM cafs cfs ,districts d ,mandals m , substations ss, cpe_profilemaster cp, villages v ");
			builder.append(" WHERE cfs.cpeprofileid = cp.profile_id AND d.districtuid = cfs.inst_district ");
			builder.append(" AND m.mandalslno = cfs.inst_mandal AND m.districtuid = cfs.inst_district ");
			builder.append(" AND v.villageslno = cfs.inst_city_village AND v.mandalslno = cfs.inst_mandal ");
			builder.append(" AND v.districtuid = cfs.inst_district AND cfs.pop_substnno = ss.substnuid ");
			builder.append(" AND cfs.cafno = "+cafNo+" ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			cafDao = (Object[]) query.getSingleResult();
		} catch (Exception e) {
			logger.error("EXCEPTION::getAllParticularCPE() " + e);
		}finally{
			query = null;
			builder = null;
		}
		 return cafDao;
	}

	public Object[] getPackageInformation(Long cafNumber) {
		Object[] packages = null;
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(" select cf.custid, cf.custtypelov, cf.cafno, cf.custcode, c.custname, c.lname, cf.inst_pin, c.billfreqlov, cf.inst_district, ");
			builder.append(" cf.inst_city_village, cf.inst_mandal, c.status, cf.lmocode, cf.pop_substnno, cf.pmntcustid, c.enttypelov, cf.status as cafstatus, cf.pop_district, cf.pop_mandal, cf.custdistuid from cafs cf, customermst c where cafno = "+cafNumber+" and cf.custid = c.custid ");
			
			query = getEntityManager() .createNativeQuery(builder.toString());
			packages = (Object[]) query.getSingleResult();
		} catch (Exception e) {
			logger.error("EXCEPTION::getPackageInformation() " + e);
		}finally{
			query = null;
			builder = null;
		}
	  return packages;	
	}

	public String getSubCode(Long cafNo) {
		String subCode = "";
		StringBuilder builder = new StringBuilder("SELECT DISTINCT nwsubscode FROM cafsrvcs WHERE nwsubscode IS NOT NULL AND cafno = "+cafNo+" ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			subCode = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getSubCode() " + ex);
			ex.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return subCode;
	}

	public String getVATAmountByRegion(String regionCode) {
		String VATAmount = "0";
		StringBuilder builder = new StringBuilder("SELECT taxperc FROM taxmast where regioncode = '"+regionCode+"' and taxcode = 'ENTTAX' ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			VATAmount = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getSubCode() " + ex);
			ex.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return VATAmount;
	}

	public String deActivateCaf(Long cafNo) {
		String status = "";
		StringBuilder builder = null;
		Query query = null;
		try {
			String dueDate = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			/*StringBuilder builder1 = new StringBuilder("delete from olpayments where acctcafno = "+cafNo+" and status = "+ComsEnumCodes.MONTHLY_PAYMENT_STAUTS.getStatus()+" ");
			Query query1 = getEntityManager() .createNativeQuery(builder1.toString());
			query1.executeUpdate();*/
			
			builder = new StringBuilder("update dunningcafs set duedate = '"+dueDate+"', status = "+ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus()+" where acctcafno = "+cafNo+" ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			int result = query.executeUpdate();
			if(result == 1) {
				status = "Deactivation Successfully Done.";
			} else {
				status = "Issue in downstream system. Please try again.";
			}
		} catch(Exception e) {
			logger.error("EXCEPTION::updateCafCharges() " + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}

	public String getCpeModel(String cpeModal) {
		String cpeModel = "";
		StringBuilder builder = new StringBuilder("SELECT cp.cpe_model FROM cafs cf, cpe_profilemaster cp WHERE cf.cpeprofileid = '"+cpeModal+"' and cf.cpeprofileid = cp.profile_id limit 1 ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cpeModel = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getSubCode() " + ex);
			ex.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return cpeModel;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getFeatureParamList(String featureCodes) {
		List<Object[]> featureCodesList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("select fp.prmdfltlbl, fp.prmvaltype, fp.prmlovname, sf.maxprmvalues, fp.featurecode, fp.prmcode from featureprms fp, srvcfeatures sf ");
		builder.append("where fp.featurecode = '"+featureCodes+"' and fp.featurecode = sf.featurecode and fp.coresrvccode = sf.coresrvccode ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			featureCodesList = query.getResultList();
		}catch (Throwable e) {
			logger.error("EXCEPTION::getMonthlyCafDetails() " + e.getMessage());
		}finally{
			query = null;
			builder = null;
		}
		return featureCodesList;
	}

	public String getStdCodeByVillage(Integer district, Integer mandal, Integer village) {
		String stdCode = "";
		StringBuilder builder = new StringBuilder("SELECT stdcode FROM villages where districtuid = "+district+" and mandalslno = "+mandal+" and villageslno = "+village+" ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			stdCode = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getRegionCodeByPinCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return stdCode;
	}	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getTelephoneServc(Long cafNo) {
		List<Object[]> telephonesrvcList = null;
		StringBuilder builder = new StringBuilder("SELECT  srf.featurename,cfp.prmcode,cfp.prmvalue,sv.coresrvccode,cfp.cafno FROM ");
		builder.append("caffeatureprms cfp,srvcs sv,cafsrvcs cs, srvcfeatures srf " + "WHERE cfp.cafno = "+cafNo+" ");
		builder.append("AND sv.coresrvccode = '"+ComsEnumCodes.CORE_SERVICE_VOIP.getCode()+"' AND cs.srvccode = sv.srvccode AND cfp.cafno=cs.cafno AND srf.featurecode = cfp.featurecode ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			telephonesrvcList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getTelephoneServ() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return telephonesrvcList;
	}
	
	// For featureCodes
	@SuppressWarnings("unchecked")
	public List<String> getFeatureCodes(Long cafNo) {
		List<String> featurecode = null;
		String[] tokens;
		List<String> ftrCodes = new ArrayList<String>();
		StringBuilder builder1 = new StringBuilder(" SELECT s.featurecodes FROM cafsrvcs cs, srvcs s WHERE cs.cafno = "+cafNo+" ");
		builder1.append("AND cs.srvccode = s.srvccode AND s.coresrvccode = 'VOIP'  AND CURRENT_DATE BETWEEN s.effectivefrom AND s.effectiveto ");
		Query query1 = null;
		try {
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			featurecode = query1.getResultList();
			for (String fc : featurecode) {
				if (!fc.isEmpty()) {
					tokens = fc.split(",");
					for (String fcode : tokens) {
						StringBuilder builder2 = new StringBuilder("SELECT featurename FROM srvcfeatures WHERE featurecode='" + fcode + "'");
						Query query2 = getEntityManager().createNativeQuery(builder2.toString());
						String featureCodeName = (String) query2.getSingleResult();
						ftrCodes.add(featureCodeName);

					}
				}
			}
		} catch (Exception e) {
			logger.error("EXCEPTION::getTelephoneServ() " + e);
		}finally{
			query1 = null;
			builder1 = null;
		}
		return ftrCodes;
	}
	
	@SuppressWarnings("unchecked")
	public List<TelephoneNoVO> getTelephoneNo(Long cafNo) {
		List<TelephoneNoVO> telephonesrvcList = new ArrayList<>();
		List<String> list =new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT phoneno FROM cafsrvcphonenos WHERE  cafno='"+cafNo+"'");
			try {
				query = getEntityManager() .createNativeQuery(builder.toString());
				list = query.getResultList();
				for (String str : list) {
					TelephoneNoVO telephoneNoVO = new TelephoneNoVO();
					telephoneNoVO.setTelephoneNo(str == null || str == "" ? "NA" : str);
					telephonesrvcList.add(telephoneNoVO);
				}
			} catch (Exception e) {
				logger.error("EXCEPTION::getTelephoneServ() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return telephonesrvcList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getBillingInfo(Long cafNo) {
		List<Object[]> billinfo = null;
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(" SELECT SUBSTRING('JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC ', (ci.invmn * 4) - 3, 3) AS invmn, invfilepath,");
			builder.append(" cui.invamt + cui.srvctax + cui.swatchtax + cui.kisantax + cui.enttaX invamt, date_format(cui.invdate, '%d/%m/%Y') invDate, date_format(cui.invduedate, '%d/%m/%Y') invDueDate ");
			builder.append(" FROM cafinv  ci , cafs cf, custinv cui WHERE ");
			builder.append(" cf.pmntcustid = ci.pmntcustid AND ci.acctcafno = cf.cafno  AND acctcafno = "+cafNo+" AND cui.invmn IN (MONTH(NOW())-1,MONTH(NOW())-3,MONTH(NOW())-2) ");
			builder.append(" AND cui.pmntcustid = ci.pmntcustid AND ci.invmn = cui.invmn AND ci.invyr = cui.invyr AND cf.pmntcustid = ci.pmntcustid ");
			query = getEntityManager().createNativeQuery(builder.toString());
			billinfo = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getTelephoneServ() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return billinfo;
	}
	
	public Caf findByCafAccountNo(Long acctcafno) {
		Caf caf = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(" select * from cafs where cafno = :acctcafno");
		try {
			query = getEntityManager().createNativeQuery(builder.toString(),Caf.class);
			query.setParameter("acctcafno", acctcafno);
			caf =  (Caf) query.getSingleResult();
		} catch (Exception e) {
			logger.error("EXCEPTION::findByCafAccountNo() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return caf;
	}

	public Payments findPaymentByAcctCno(Long acctcafno) {
		Payments payments = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(" select * from payments where  createdon = (select max(createdon) from payments where acctcafno = :acctcafno)");
		try {
			query = getEntityManager().createNativeQuery(builder.toString(),Payments.class);
			query.setParameter("acctcafno", acctcafno);
			payments =  (Payments) query.getSingleResult();
		} catch (Exception e) {
			logger.error("EXCEPTION::findPaymentByAcctCno() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return payments;
	}

	public <T> T saveOrUpdateAnyObject(T t) {
		
		return getEntityManager().merge(t);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getProdSrvesList(String prodCode) {
		List<Object[]> list = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(" ");
		builder.append(" select prodcode,componentcode, sum(chargeamt)");
		builder.append(" from prodcharges where prodcode = :prodCode ");
		builder.append(" and current_date()  between effectivefrom and effectiveto ");
		builder.append(" group by prodcode");
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("prodCode", prodCode);
			list = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getProdSrvesList() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return list;
	}

	public Object[] getOnuStbChargeDetails(Long cafNo) {
		Object[] getOnuStbCharges = null;
		StringBuilder builder = new StringBuilder("SELECT (SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='CPEINST') AS cpeInstChrg, ");
		builder.append("(SELECT totalemicnt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='CPEINST') AS cpeInstCount, "); 
		builder.append("(SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='CPECOST') AS cpeCost, ");
		builder.append("(SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='INSTALLATION') AS instChrg, ");
		builder.append("(SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='EXTRACABLE') AS extraCableChrg, ");
		builder.append("(SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='STBCOST') AS stbCost, ");
		builder.append("(SELECT chargeamt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='STBINST') AS stbInstChrg, ");
		builder.append("(SELECT totalemicnt FROM cafcharges WHERE cafno = "+cafNo+" AND chargecode='STBINST') AS stbInstCount ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			getOnuStbCharges = (Object[]) query.getSingleResult();
		} catch (Throwable e) {
			logger.error("EXCEPTION::getMonthlyCafDetails() " + e.getMessage());
		}
		finally{
			query = null;
			builder = null;
		}
		return getOnuStbCharges;
	}
	
	// getOldPackageInformation
	@SuppressWarnings("unchecked")
	public List<CafOldPackagesBO> getOldPackageInformation(Long cafNumber) {
		List<CafOldPackagesBO> cafOldPackagesList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(" select cp.tenantcode,cp.prodcode,cp.rsagruid FROM cafprods cp, cafsrvcs cs, products p WHERE cp.parentcafno = "+cafNumber+" and cp.cafno = cs.cafno ");
			builder.append(" and cp.tenantcode = cs.tenantcode and p.tenantcode = cp.tenantcode and p.tenantcode = cs.tenantcode  and p.globalflag = 'Y' and current_date() between p.effectivefrom and p.effectiveto and  ");
			builder.append(" cp.parentcafno = cs.parentcafno and p.prodcode = cp.prodcode and p.prodcode = cs.prodcode and cs.status not in("+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") group by cp.tenantcode,cp.prodcode,cp.rsagruid ");
			query = getEntityManager().createNativeQuery(builder.toString(), CafOldPackagesBO.class);
			cafOldPackagesList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getOldPackageInformation() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return cafOldPackagesList;
	}

	public void updateStbStockStatus(String stbSerialNo, Long cafNo, String loginID) {
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::updateStbStockStatus()");
			String modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			builder = new StringBuilder(" update cpestock set cafno = "+cafNo+", status = "+ComsEnumCodes.CAF_BLOCKED_STATUS.getStatus()+", modifiedby = '"+loginID+"', modifiedon = '"+modifiedOn+"' where cpeslno = '"+stbSerialNo+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();
			logger.info("END::updateStbStockStatus()");
		} catch(Exception e) {
			logger.error("The Exception is CafDao :: updateStbStockStatus()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
	}

	public void updateCpeStockPaymentStatus(Long cafNo, String srlNo, String loginID, String lmoCode) {
		Query query = null;
		StringBuilder builder = null;
		String modifiedOn = null;
		try {
			logger.info("START::updateCpeStockPaymentStatus()");
			builder = new StringBuilder();
			modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			builder = new StringBuilder(" update cpestock set lmocode = '"+lmoCode+"', cafno = "+cafNo+", status = "+ComsEnumCodes.CAF_ALLOCATED_STATUS.getStatus()+", modifiedby = '"+loginID+"', modifiedon = '"+modifiedOn+"' where cpeslno = '"+srlNo+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();
			logger.info("END::updateCpeStockPaymentStatus()");
		} catch(Exception e) {
			logger.error("The Exception is CafDao :: updateCpeStockPaymentStatus()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
			modifiedOn = null;
		}
	}

	@SuppressWarnings("unchecked")
	public String getCpeStackStatusBySrlNo(String serialNo, String cpeModel) {
		String status = "false";
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT status FROM cpestock where cpeslno = '"+serialNo+"' and profile_id = '"+cpeModel+"' ");
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			List<Object> objectList = query.getResultList();
			if(objectList.size() > 0) {
				for(Object object : objectList) {
					status = object.toString();
				}
			}
		} catch(Exception ex){
			logger.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<FingerPrintBO> getIndividualCafFPActivationInformation(PageObject pageObject) {
		List<FingerPrintBO> customerList = new ArrayList<>();
		Query query = null;
		Query querycount = null;
		StringBuilder builder = new StringBuilder();
		String orderByClause = "";
		String likeClause = "";
		String searchParameter = "";
		try {
			if(pageObject != null) {
				if (pageObject.getSearchParameter().indexOf("/") > 0) {
					try {
						searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
						} catch (Exception e) {
						searchParameter = pageObject.getSearchParameter();
						}
					} else {
						searchParameter = pageObject.getSearchParameter();
					}
				
				likeClause = likeClause + " and (cf.cafno like '%" + searchParameter + "%' or c.aadharno like '%"+ searchParameter + "%' or c.custname like '%" + searchParameter + "%' ";
				likeClause = likeClause + " or cfs.stbcafno like '%" + searchParameter + "%'  or cf.cafdate like '%"+ searchParameter + "%' or cf.cpeplace like '%" + searchParameter + "%' ";
				likeClause = likeClause + " or cfs.stbslno like '%" + searchParameter + "%' or cfs.stbmacaddr like '%"+ searchParameter + "%' or cfs.nwsubscode like '%"+ searchParameter + "%' ) ";
				
				orderByClause = " ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder();
			}
			builder.append(" select cf.cafno, c.aadharno, c.custname, c.lname, cfs.stbcafno, DATE_FORMAT(cf.cafdate,'%d/%m/%Y') cafdate, cf.cpeplace, cfs.stbslno, cfs.stbmacaddr, cfs.nwsubscode ");
			builder.append(" from cafsrvcs cs, cafstbs cfs, cafs cf, customermst c where cs.srvccode in(select srvccode from srvcs where coresrvccode = 'IPTV') ");
			builder.append(" and cs.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" and cs.parentcafno = cfs .parentcafno and c.custid = cf.custid and cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" ");
			builder.append(" and cfs.parentcafno = cf.cafno and cs.parentcafno = cf.cafno "+likeClause+" "+orderByClause+" ");
			query = getEntityManager().createNativeQuery(builder.toString(), FingerPrintBO.class);
			
			if(pageObject != null) {
				querycount = getEntityManager() .createNativeQuery(builder.toString(), FingerPrintBO.class);
				long totalDisplayCount = querycount.getResultList().size();
				pageObject.setTotalDisplayCount(String.valueOf(totalDisplayCount));
				
				query.setFirstResult(pageObject.getMinSize());
				query.setMaxResults(pageObject.getMaxSize());
			}
			customerList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getIndividualCafFPActivationInformation() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return customerList;
	}

	@SuppressWarnings("unchecked")
	public List<FingerPrintBO> getGroupCafFPActivationInformation(FingerPrintBO fingerPrintBO) {
		List<FingerPrintBO> customerList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		String whereClause = "";
		Query querycount = null;
		String orderByClause = "";
		String likeClause = "";
		String searchParameter = "";
		try {
			if(fingerPrintBO.getDistrict() != null && !fingerPrintBO.getDistrict().isEmpty() && fingerPrintBO.getMandal() != null && !fingerPrintBO.getMandal().isEmpty() && fingerPrintBO.getVillage() != null && !fingerPrintBO.getVillage().isEmpty()) {
				whereClause = " and cf.inst_district = "+fingerPrintBO.getDistrict()+" and cf.inst_mandal = "+fingerPrintBO.getMandal()+" and cf.inst_city_village = "+fingerPrintBO.getVillage()+" ";
			} else if(fingerPrintBO.getDistrict() != null && !fingerPrintBO.getDistrict().isEmpty() && fingerPrintBO.getMandal() != null && !fingerPrintBO.getMandal().isEmpty()) {
				whereClause = " and cf.inst_district = "+fingerPrintBO.getDistrict()+" and cf.inst_mandal = "+fingerPrintBO.getMandal()+" ";
			} else if(fingerPrintBO.getDistrict() != null && !fingerPrintBO.getDistrict().isEmpty()) {
				whereClause = " and cf.inst_district = "+fingerPrintBO.getDistrict()+" ";
			}
			
			if (fingerPrintBO.getPageObject().getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(fingerPrintBO.getPageObject().getSearchParameter()));
					} catch (Exception e) {
					searchParameter = fingerPrintBO.getPageObject().getSearchParameter();
					}
				} else {
					searchParameter = fingerPrintBO.getPageObject().getSearchParameter();
				}
			
			likeClause = likeClause + " and (cf.cafno like '%" + searchParameter + "%' or c.aadharno like '%"+ searchParameter + "%' or c.custname like '%" + searchParameter + "%' ";
			likeClause = likeClause + " or cfs.stbcafno like '%" + searchParameter + "%'  or cf.cafdate like '%"+ searchParameter + "%' or cf.cpeplace like '%" + searchParameter + "%' ";
			likeClause = likeClause + " or cfs.stbslno like '%" + searchParameter + "%' or cfs.stbmacaddr like '%"+ searchParameter + "%' or cfs.nwsubscode like '%"+ searchParameter + "%' ) ";
			
			orderByClause = " ORDER BY "+fingerPrintBO.getPageObject().getSortColumn()+" "+fingerPrintBO.getPageObject().getSortOrder();
			
			builder.append("select cf.cafno, c.aadharno, c.custname, c.lname, cfs.stbcafno, cf.cafdate, cf.cpeplace, cfs.stbslno, cfs.stbmacaddr, cfs.nwsubscode ");  
			builder.append(" from cafsrvcs cs, cafstbs cfs, cafs cf, customermst c where cs.srvccode in(select srvccode from srvcs where coresrvccode = 'IPTV') ");
			builder.append(" and cs.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" and cs.parentcafno = cfs.parentcafno and c.custid = cf.custid and cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" ");
			builder.append(" and cfs.parentcafno = cf.cafno and cs.parentcafno = cf.cafno "+whereClause+" "+likeClause+" "+orderByClause+" ");
			
			query = getEntityManager().createNativeQuery(builder.toString(), FingerPrintBO.class);
			
			querycount = getEntityManager() .createNativeQuery(builder.toString(), FingerPrintBO.class);
			long totalDisplayCount = querycount.getResultList().size();
			fingerPrintBO.getPageObject().setTotalDisplayCount(String.valueOf(totalDisplayCount));
			
			query.setFirstResult(fingerPrintBO.getPageObject().getMinSize());
			query.setMaxResults(fingerPrintBO.getPageObject().getMaxSize());
			
			customerList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getGroupCafFPActivationInformation() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return customerList;
	}

	public String getCoreSrvcCodeByCafNo(Long cafNumber) {
		String coreSrvcCode = "";
		StringBuilder builder = new StringBuilder("SELECT GROUP_CONCAT(coresrvccode) from (select coresrvccode from prodcomponents where ");
		builder.append(" prodcode in (select prodcode from cafprods where parentcafno = "+cafNumber+" )) x ");
		Query query = null;
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			coreSrvcCode = query.getSingleResult().toString();
		} catch(Exception ex){
			logger.error("EXCEPTION::getApproveCafDetailsByTenantCode() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return coreSrvcCode;
	}

	public VPNSrvcNames saveVPNSrvcNames(VPNSrvcNames vpnSrvcNames) {
		VPNSrvcNames vpnSrvcNamesObj = new VPNSrvcNames(); 
		try {
			vpnSrvcNamesObj = getEntityManager().merge(vpnSrvcNames);
		} catch (Exception e) {
			logger.error("EXCEPTION CafDao :: saveVPNSrvcNames() " + e);
			e.printStackTrace();
		} finally {

		}
		return vpnSrvcNamesObj;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllServicesByProdCode(String pkgCode) {
		List<Object[]> list = new ArrayList<>();
		Query query = null;
		StringBuilder builder = null;
		try {
			builder = new StringBuilder("select componentcode, s.featurecodes from prodcomponents pc, products p, srvcs s ");
			builder.append(" where p.prodcode = pc.prodcode and current_date() between p.effectivefrom and p.effectiveto and p.prodcode = :pkgCode");
			builder.append(" and s.srvccode = pc.componentcode and current_date() between s.effectivefrom and s.effectiveto");
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("pkgCode", pkgCode);
			list = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::findAllServicesByProdCode() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getSISelectedPackages(String prodCodes, String billCycle, String regionCode) {
		List<Object[]> packagesList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = null;
		try {
			builder = new StringBuilder("select p.prodcode,p.prodname,p.prodtype,group_concat(s.srvcname), ");
			builder.append(" SUBSTRING_INDEX(get_prodcharge(p.tenantcode,p.prodcode,'',null,'2',date_format(date(now()) , '%Y%m%d'),'"+regionCode+"',''),',',1) activationCharge, ");
			builder.append(" SUBSTRING_INDEX(get_prodcharge(p.tenantcode,p.prodcode,'',null,'2',date_format(date(now()) , '%Y%m%d'),'"+regionCode+"',''),',',-1) activationTax, ");
			builder.append(" SUBSTRING_INDEX(get_prodcharge(p.tenantcode,p.prodcode,'',null,'3',date_format(date(now()) , '%Y%m%d'),'"+regionCode+"',''),',',1) securityCharge, "); 
			builder.append(" SUBSTRING_INDEX(get_prodcharge(p.tenantcode,p.prodcode,'',null,'3',date_format(date(now()) , '%Y%m%d'),'"+regionCode+"',''),',',-1) securityTax ");
			builder.append(" from products p, prodcomponents pc, srvcs s  where p.prodcode = pc.prodcode AND p.tenantcode = pc.tenantcode AND s.srvccode = pc.componentcode ");
			builder.append(" AND current_date() between p.effectivefrom and p.effectiveto AND current_date() between s.effectivefrom and s.effectiveto AND p.prodcode in("+prodCodes+") ");
			builder.append(" group by prodcode, prodname, prodtype, activationCharge, activationTax,securityCharge, securityTax ");
			query = getEntityManager().createNativeQuery(builder.toString());
			packagesList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::findAllServicesByProdCode() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return packagesList;
	}

	@SuppressWarnings("unchecked")
	public List<String> getFingerPrintDetailsFromConfig() {
		List<String> fPlist = new ArrayList<>();
		Query query = null;
		StringBuilder builder = null;
		try {
			builder = new StringBuilder("SELECT configval FROM configitems WHERE configname IN ('FpBgColor','FpChannel','FpDuration','FpfingerPrintType','FpFontColor','FpFontSize','FpFontType','FpPosition') ");
			query = getEntityManager().createNativeQuery(builder.toString());
			fPlist = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getFingerPrintDetailsFromConfig() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return fPlist;
	}
	
	public Object[] getMacAddressBySerialNo(String srlNo) {
		Object[] object = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			builder.append(" select cpemacaddr, profile_id from cpestock where cpeslno = '"+srlNo+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			object = (Object[]) query.getSingleResult();
		} catch(Exception ex){
			logger.error("EXCEPTION::getMacAddressBySerialNo() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return object;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getTicketInfo(Long cafNo) {
		List<Object[]> ticketList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder(" SELECT tt.ticketno,DATE_FORMAT(tt.createdon,'%d/%m/%Y'),tt.ticketdesc, tt.status, ");
		builder.append(" DATE_FORMAT(tt.modifiedon,'%d/%m/%Y'), tc.name ");
		builder.append(" FROM tt_tickets tt,ticket_category tc ");
		builder.append(" WHERE tt.cafno='"+cafNo+"' AND tt.status=tc.id ORDER BY  tt.createdon DESC LIMIT 5 ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			ticketList = query.getResultList();
		} catch (Exception ex) {
			logger.error("EXCEPTION::searchCafDetails() " + ex);
		} finally {
			builder = null;
			query = null;
		}
		return ticketList;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCafUsage(String yyyy, String mm, String cafNo) {
		List<Object[]> list = new ArrayList<Object[]>();
		Map<String, String> map = new HashMap<>();
		
		StringBuilder builder = new StringBuilder(" select cast((dnldsize+upldsize)/1024/1024/1024 as decimal(10,3)) used , ");
		builder.append(" cast((basednldsize+baseupldsize+bstrdnldsize+bstrupldsize)/1024/1024/1024 as decimal(10,3))   plan ");
		builder.append(" from hsicumusage where acctcafno = '"+cafNo+"' and usageyyyy = '"+yyyy+"' and usagemm = '"+mm+"' ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			list = query.getResultList();
			for (Object[] obj : list) {
				map.put("Used Data", obj[0].toString());
				map.put("Total Data", obj[1].toString());
			}
		} catch (Exception ex) {
			logger.error("EXCEPTION::searchCafDetails() " + ex);
		} finally {
			builder = null;
			query = null;
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findCustomerInfoByCafNo(Long cafNo) {
		List<Object[]> customerList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			logger.info("START::findCustomers()");
			builder = new StringBuilder("SELECT cus.custname,cus.mname,cus.lname ,cus.email1,IFNULL(DATE_FORMAT(cus.dateofinc,'%d/%m/%Y'), 'NA'), ");
			builder.append(" cus.gender,cus.pocmob1,cf.billfreqlov,cus.custtypelov,cus.lmocode FROM cafs cf,customermst cus ");
			builder.append(" WHERE cf.custid=cus.custid AND cafno='" + cafNo + "' ");
			query = getEntityManager().createNativeQuery(builder.toString());
			customerList = query.getResultList();
			logger.info("END::findCustomers()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return customerList;
	}

	@SuppressWarnings("unchecked")
    public List<Object[]> findCafByCafNo(Long custId) {
        List<Object[]> cafDaoList = new ArrayList<Object[]>();
        StringBuilder builder = new StringBuilder("SELECT cf.cafno, c.depbal, c.regbal, CASE WHEN cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" THEN 'Active' ");
        builder.append(" WHEN cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" THEN 'Pending For Provisioning' WHEN cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" THEN 'Suspended' WHEN cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+" THEN 'De-activated' END status, IFNULL(cf.actdate, 'NA') ");
        builder.append(" from cafs cf, customermst c WHERE cf.custid = "+custId+"  AND cf.custid = c.custid  and (cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" OR cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" OR cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" OR cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") ");
        Query query = null;
        try {
            query = getEntityManager() .createNativeQuery(builder.toString());
            cafDaoList = query.getResultList();
        }catch (Throwable e) {
          logger.error("EXCEPTION::getMonthlyCafDetails() "+e.getMessage());
        }finally{
        	query = null;
        	builder = null;
        }
        return cafDaoList;
    }

	@SuppressWarnings("unchecked")
	public List<MonthlyPaymentBO> getMonthlyCafDetailsForMobile(String mobileNo, String tenantCode) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT c.custname, c.aadharno custcode, 0 cafno, c.regbal+c.chargedbal regbal, c.custid, c.custtypelov, c.lname, ");
		builder.append(" c.custdistuid, c.pocmob1 FROM customermst c where c.pocmob1='"+mobileNo+"' and c.lmocode = '"+tenantCode+"' ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(), MonthlyPaymentBO.class);
			cafList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetailsForMobile() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return cafList;
	}

//	@SuppressWarnings("unchecked")
//	public List<OntIdCustTypeBO> getOntIdandCustTypeList() {
//		List<OntIdCustTypeBO> ontIdCustTypeList = new ArrayList<>();
//		StringBuilder builder = new StringBuilder("SELECT IFNULL(REPLACE(agorahsisubscode,'-HSI',''),'null') as agorahsisubscode, custtypelov, custid, cafno  FROM cafs WHERE STATUS='6' "); 
//		Query query = null;
//		try {
//			query = getEntityManager().createNativeQuery(builder.toString(),OntIdCustTypeBO.class);
//			ontIdCustTypeList = query.getResultList();
//		} catch(Exception ex) {
//			logger.error("EXCEPTION::getOntIdandCustTypeList() " + ex);
//		}
//		return ontIdCustTypeList;
//	}
	
	@SuppressWarnings("unchecked")
	public List<OntIdCustTypeBO> getOntIdandCustTypeList(String cafNo) {
		List<OntIdCustTypeBO> ontIdCustTypeList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT IFNULL(REPLACE(agorahsisubscode,'-HSI',''),'null') as agorahsisubscode, custtypelov, custid, cafno  FROM cafs WHERE STATUS='6' AND cafno>"+cafNo); 
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(),OntIdCustTypeBO.class);
			ontIdCustTypeList = query.getResultList();
		} catch(Exception ex) {
			logger.error("EXCEPTION::getOntIdandCustTypeList() " + ex);
		}
		return ontIdCustTypeList;
	}
	
	public CustomerDetailsBO getCustomerDetailsByCafNo(Long cafNo) {
		CustomerDetailsBO customerDetails = null;
		StringBuilder builder = new StringBuilder("SELECT cf.cafno, CASE WHEN cf.agorahsisubscode IS NOT NULL THEN REPLACE(cf.agorahsisubscode,'-HSI','') ELSE 'NULL' END AS agorahsisubscode, cf.custtypelov , ");
		builder.append(" CONCAT(cm.custname,cm.lname) AS custname, CONCAT(cm.addr1,cm.addr2) AS address, cf.contactmob, cf.apsfluniqueid AS apsfluniqueid ");
		builder.append(" FROM cafs cf, customermst cm WHERE cf.STATUS='6'");
		builder.append(" AND cf.custcode = cm.custcode AND cf.cafno='"+cafNo+"'");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(),CustomerDetailsBO.class);
			customerDetails = (CustomerDetailsBO) query.getSingleResult();
		} catch(Exception ex) {
			logger.error("EXCEPTION::getOntIdandCustTypeList() " + ex);
		}
		return customerDetails;
	}

	public String hasIPTVBoxForCaf(Long cafNumber) {
		int count = 0;
		String status = "NO";
		StringBuilder builder = new StringBuilder("SELECT * FROM cafstbs WHERE parentcafno="+cafNumber );
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			count = query.getResultList().size();
			if(count > 0) {
				status = "YES";
			}
		}catch(Exception ex) {
			logger.error("Exception :: hasIPTVBoxForCaf() " +ex);
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<NoOfSTBsBO> getNoOfSTBsForCaf(Long cafNumber) {
		List<NoOfSTBsBO> noOfSTBsList= new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT stbcafno,stbslno,stbmacaddr FROM cafstbs WHERE parentcafno = '"+cafNumber+"' ORDER BY stbslno ASC"); 
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(),NoOfSTBsBO.class);
			noOfSTBsList = query.getResultList();
		} catch(Exception ex) {
			logger.error("EXCEPTION::getOntIdandCustTypeList() " + ex);
		}
		return noOfSTBsList;
	}
	
	public ChangePkgBO getProdCodeForCaf(Long cafNumber) {
		ChangePkgBO changePkgBO = new ChangePkgBO();
		StringBuilder builder = new StringBuilder("SELECT cp.tenantcode, cp.prodcode, DATE_FORMAT(cp.createdon,'%Y-%m-%d') actdate, ");
		builder.append(" cp.rsagruid  FROM  products p , cafprods cp WHERE  p.prodtype = '"+ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()+"' "); 
		builder.append(" AND cp.cafno ='"+cafNumber+"' AND  cp.tenantcode = p.tenantcode AND cp.prodcode = p.prodcode ");

		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(), ChangePkgBO.class);
			@SuppressWarnings("unchecked")
			List<ChangePkgBO> changePkgBOList = query.getResultList();
			if(changePkgBOList.size() > 0) {
				changePkgBO = changePkgBOList.get(0);
			}
		} catch(Exception ex) {
			logger.error("EXCEPTION::getProdCodeForCaf() " + ex);
		}
		return changePkgBO;
	}

	public String getSecurityDepositByProdcode(String prodCafNo, String prodCode, String regionCode) {
		String secDeposit = "0";
		StringBuilder builder = new StringBuilder("");
		Query query = null;
		try {
			builder.append(" select substr(secdeposit,1,instr(secdeposit,',')-1) secdep ");
			builder.append(" from (select get_prodcharge(tenantcode, prodcode,'', NULL, '3', DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'), '"+regionCode+"' , '') secdeposit ");
			builder.append(" from cafprods where prodcode = '"+prodCode+"' and cafno = "+prodCafNo+" ) x ");
			query = getEntityManager().createNativeQuery(builder.toString());
			secDeposit = query.getSingleResult().toString();
		} catch(Exception ex) {
			logger.error("EXCEPTION::getProdCodeForCaf() " + ex);
		}
		return secDeposit;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getGolbalOrGroupSubCodes(String district, String mandal, String village) {
		List<String> subsCodeList= new ArrayList<>();
		StringBuilder builder = new StringBuilder(""); 
		Query query = null;
		String whereClause = "";
		if(district != null && !district.isEmpty() && mandal != null && !mandal.isEmpty() && village != null && !village.isEmpty()) {
			whereClause = " and cf.inst_district = "+district+" and cf.inst_mandal = "+mandal+" and cf.inst_city_village = "+village+" ";
		} else if(district != null && !district.isEmpty() && mandal != null && !mandal.isEmpty()) {
			whereClause = " and cf.inst_district = "+district+" and cf.inst_mandal = "+mandal+" ";
		} else if(district != null && !district.isEmpty()) {
			whereClause = " and cf.inst_district = "+district+" ";
		}
		try {
			builder.append("SELECT cfs.nwsubscode  FROM cafsrvcs cs, cafstbs cfs, cafs cf ");
			builder.append(" WHERE cs.srvccode IN(SELECT srvccode FROM srvcs WHERE coresrvccode = 'IPTV')  AND cs.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" ");
			builder.append(" AND cs.parentcafno = cfs.parentcafno AND cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+"  AND cfs.parentcafno = cf.cafno ");
			builder.append(" AND cs.parentcafno = cf.cafno " +whereClause);
			query = getEntityManager().createNativeQuery(builder.toString());
			subsCodeList = query.getResultList();
		} catch(Exception ex) {
			logger.error("EXCEPTION::getOntIdandCustTypeList() " + ex);
		}
		return subsCodeList;
	}

    @SuppressWarnings("unchecked")
	public List<TerminatePkgBO> getProdInfoByCafNo(Long cafNo) {
        List<TerminatePkgBO> prodList = new ArrayList<>();
        Query query = null;
        StringBuilder builder = new StringBuilder(" SELECT cp.cafno, cp.parentcafno, cp.tenantcode, p.prodname,cp.rsagruid, cp.prodcode, ");
        builder.append(" DATE_FORMAT(cs.actdate, '%d/%m/%Y') proddate, cfs.stbcafno, cfs.stbslno, cfs.nwsubscode ");
        builder.append(" FROM cafprods cp, cafsrvcs cs, products p, cafstbs cfs ");
        builder.append(" WHERE cp.parentcafno = "+cafNo+" AND cp.parentcafno = cs.parentcafno ");
        builder.append(" AND cp.cafno = cs.cafno AND cs.status IN("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" ) AND p.prodcode = cp.prodcode ");
        builder.append(" AND p.prodcode = cs.prodcode AND p.globalflag = 'Y' AND p.prodtype = '"+ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()+"' ");
        builder.append(" AND cp.createdon BETWEEN p.effectivefrom AND p.effectiveto AND cp.parentcafno = cfs.parentcafno ");
        builder.append(" AND cs.parentcafno = cfs.parentcafno and cfs.stbcafno = cs.stbcafno ");
        builder.append(" and cs.srvccode in(select srvccode from srvcs where coresrvccode = '"+ComsEnumCodes.CORE_SERVICE_IPTV.getCode()+"') ");
		try {
			query = getEntityManager().createNativeQuery(builder.toString(), TerminatePkgBO.class);
			prodList = query.getResultList();
		} catch (Exception e) {
			logger.error("EXCEPTION::getProdInfoByCafNo() " + e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return prodList;
	}
    
    @SuppressWarnings("unchecked")
	public List<HsiBO> getProdInfoByCafNo(String srvcCode) {
		 List<HsiBO> hsiList = new ArrayList<>();
	        Query query = null;
	        StringBuilder builder = new StringBuilder(" SELECT prmcode, prmvalue FROM srvcprms WHERE srvccode = '"+srvcCode+"' AND prmcode IN ('DOWNLOADLIMIT','UPLOADLIMIT') ");
			try {
				query = getEntityManager().createNativeQuery(builder.toString(), HsiBO.class);
				hsiList = query.getResultList();
			} catch (Exception e) {
				logger.error("EXCEPTION::getProdInfoByCafNo() " + e.getMessage());
			} finally {
				query = null;
				builder = null;
			}
			return hsiList;
	}
    
    @SuppressWarnings("unchecked")
	public List<Object[]> findEntCafDtls(String custId,PageObject pageObject ) {
		List<Object[]> cafsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		long count = 0l;
		Query countQuery = null;
		String searchParameter = "";
		try {
			logger.info("START::findEntCafDtls()");
			builder.append(" SELECT cf.cafno, cf.cpeslno, cf.cpeplace, cust.custname, cust.lname, cf.cpemacaddr, ");
			builder.append(" (SELECT GROUP_CONCAT(cfs.stbslno )stbslno FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbslno, ");
			builder.append(" (SELECT GROUP_CONCAT(cfs.stbmacaddr )stbmacaddr FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbmacaddr, ");
			builder.append(" (SELECT GROUP_CONCAT(cp.phoneno) FROM cafsrvcphonenos cp WHERE cp.parentcafno = cf.cafno) cafphoneno, ws.statusdesc ");
			builder.append(" FROM cafs cf,customermst cust, wipstages ws WHERE cf.custid = cust.custid and cf.custid = "+custId +" and cf.status = ws.statuscode and ws.appcode = 'COMS' ");
			
			if(pageObject.getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
				} catch(Exception e) {
					searchParameter = pageObject.getSearchParameter();
				}
			} else {
				searchParameter = pageObject.getSearchParameter();
			}
			String like="AND (cafno LIKE '%"+searchParameter+"%' OR custname LIKE '%"+searchParameter+"%' OR cpeslno LIKE '%"+searchParameter+"%' OR cpemacaddr LIKE '%"+searchParameter+"%' OR cpeplace LIKE '%"+searchParameter+"%' )";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			String groupBy=(" GROUP BY cafno ");
			builder.append(like +" "+groupBy+" "+ orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString());
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			cafsList = query.getResultList();

			logger.info("END::findEntCafDtls()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findEntCafDtls() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cafsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafInfo(String cafNo) {
		List<Object[]> cafsList = new ArrayList<>();
	        Query query = null;
	        StringBuilder builder = new StringBuilder();
			try {
				builder.append(" SELECT cf.cafno,cf.cpeslno,cf.cpeplace,cf.cpemacaddr,  (SELECT GROUP_CONCAT(cfs.stbslno ) FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbslno,  (SELECT GROUP_CONCAT(cfs.stbmacaddr) FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbmacaddr,  ");
				builder.append(" (SELECT GROUP_CONCAT(cp.phoneno) FROM cafsrvcphonenos cp WHERE cp.parentcafno = cf.cafno) cafphoneno FROM cafs cf  WHERE cf.cafno = "+cafNo+" GROUP BY cafno ");
				query = getEntityManager().createNativeQuery(builder.toString());
				cafsList = query.getResultList();
			} catch (Exception e) {
				logger.error("EXCEPTION::getProdInfoByCafNo() " + e.getMessage());
			} finally {
				query = null;
				builder = null;
			}
			return cafsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getMultiActionCafList(MultiAction multiAction) {
		List<Object[]> cafsList = new ArrayList<>();
	        Query query = null;
	        StringBuilder builder = new StringBuilder();
	        String whereClause="";
	        try{
		        if(!multiAction.getTenantType().equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode())) {
					whereClause = "and cf.lmocode = '" +multiAction.getTenantCode()+"'";
				}
				if(multiAction.getOrganizationName() != null && !multiAction.getOrganizationName().isEmpty()) {
					whereClause = " and c.custname like '%"+multiAction.getOrganizationName()+"%' ";
				}
				if(multiAction.getCafSubType() != null && !multiAction.getCafSubType().isEmpty()) {
					whereClause = " and c.enttypelov = '"+multiAction.getCafSubType()+"' ";
				}
				if(multiAction.getCafType() != null && !multiAction.getCafType().isEmpty()) {
					whereClause = whereClause + " and cf.custtypelov = '"+multiAction.getCafType()+"' ";
				}
				
				if((multiAction.getEffectiveFrom() != null && multiAction.getEffectiveTo() != null) && (!multiAction.getEffectiveFrom().isEmpty() && !multiAction.getEffectiveTo().isEmpty()) ) {
					Date fromDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveFrom());
					Date toDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveTo());
					whereClause = whereClause + " and cf.cafdate between '"+ new SimpleDateFormat("yyyy-MM-dd").format(fromDate)+"' and '" + new SimpleDateFormat("yyyy-MM-dd").format(toDate)+"'";
				} 
				if(multiAction.getStatus() != null && !multiAction.getStatus().isEmpty()) {
					if(multiAction.getStatus().equalsIgnoreCase("0")) {
						whereClause = whereClause + " and (cf.status = "+multiAction.getStatus()+" OR cf.status = "+ComsEnumCodes.Caf_Edit_BulkUpload_status.getStatus()+") ";
					} else {
						whereClause = whereClause + " and cf.status = "+multiAction.getStatus()+" ";
					}
				}
				if(multiAction.getCafNo() != null ) {
					whereClause = whereClause + " and cf.cafno = " + multiAction.getCafNo();
				} 
				if(multiAction.getAadharNo() != null) {
					if(!multiAction.getAadharNo().isEmpty()) {
						whereClause = whereClause + " and cf.aadharno = '" + multiAction.getAadharNo() + "'";
					}
				}
				if(multiAction.getApsflTrackId() != null && !multiAction.getApsflTrackId().isEmpty() ) {
					whereClause = whereClause + " and cf.apsfluniqueid like '%"+multiAction.getApsflTrackId()+"%'";
				}
				if(multiAction.getDistrict() != null && !multiAction.getDistrict().isEmpty()) {
					whereClause = whereClause + " and cf.inst_district = "+multiAction.getDistrict()+" ";
				}
				if(multiAction.getMandal() != null && !multiAction.getMandal().isEmpty()) {
					whereClause = whereClause + " and cf.inst_mandal = "+multiAction.getMandal()+" ";
				}
				if(multiAction.getPopName() != null && !multiAction.getPopName().isEmpty()) {
					whereClause = whereClause + " and cf.pop_substnno = '"+multiAction.getPopName()+"' ";
				}
				try {
					builder.append(" SELECT c.custname,c.lname, cf.custtypelov caftype,cf.apsfluniqueid, ");
					builder.append(" cf.cpeplace,cf.lmocode,cf.cafno,cf.aadharno, ");
					builder.append(" (SELECT substnname FROM substations WHERE substnuid = cf.pop_substnno) AS popname, ");
					builder.append(" DATE_FORMAT(cf.cafdate,'%d/%m/%Y') cafDate,w.statusdesc,cf.cpemacaddr, ");
					builder.append(" (SELECT GROUP_CONCAT(cfs.stbslno )stbslno FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbslno, ");
					builder.append(" (SELECT GROUP_CONCAT(cfs.stbmacaddr )stbmacaddr FROM cafstbs cfs WHERE cfs.parentcafno=cf.cafno) stbmacaddr ,cf.cpeslno, ");
					builder.append(" (SELECT GROUP_CONCAT(cp.phoneno) FROM cafsrvcphonenos cp WHERE cp.parentcafno = cf.cafno) cafphoneno ");
					builder.append(" FROM cafs cf, customermst c ,wipstages w WHERE cf.custid = c.custid AND w.appcode = 'COMS' AND w.statuscode = cf.status "+whereClause + " GROUP BY cf.cafno ");
					query = getEntityManager().createNativeQuery(builder.toString());
					cafsList = query.getResultList();
				} catch (Exception e) {
					logger.error("EXCEPTION::getMultiActionCafList() " + e.getMessage());
				} finally {
					query = null;
					builder = null;
				}
		} catch(Exception e) {
			logger.error("EXCEPTION::getMultiActionCafList() " + e);
			e.printStackTrace();
		}
			return cafsList;
	}

	@SuppressWarnings("unchecked")
	public List<BalanceAdjustmentBO> searchBalanceAdjustment(String custId, String aadharNo) {
		List<BalanceAdjustmentBO> custInvData= new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		if(custId != null && !custId.isEmpty() && aadharNo != null && !aadharNo.isEmpty()) {
			whereClause = whereClause + " AND cm.custid = '"+custId+"' AND cm.aadharno = '"+aadharNo+"' ";
		} else if(custId != null && !custId.isEmpty()) {
			whereClause = whereClause + " AND cm.custid = '"+custId+"' ";
		} else if(aadharNo != null && !aadharNo.isEmpty()) {
			whereClause = whereClause + " AND cm.aadharno = '"+aadharNo+"' ";
		}
		try{
			builder.append(" SELECT cm.custid, cm.aadharno, cm.custname, ci.invmn, ci.invyr, ci.invdate as invfdate, ");
			builder.append(" (ci.invamt+ci.srvctax+ci.swatchtax+ci.kisantax+ci.enttax+ci.prevdueamt+ci.cumpaidamt) AS totalamount, ci.custinvno, ci.custinvno AS acctcafno ");
			builder.append(" FROM custinv ci, customermst cm  ");
			builder.append(" WHERE cm.custid = ci.pmntcustid "+whereClause);
			query = getEntityManager().createNativeQuery(builder.toString(), BalanceAdjustmentBO.class);
			custInvData = query.getResultList();
		}catch(Exception e){
			logger.error("EXCEPTION::searchBalanceAdjustment() " + e.getMessage());
		}
		return custInvData;
	}

	@SuppressWarnings("unchecked")
	public List<BalanceAdjustmentBO> showCafinvDetailsByCustinvno(String custInvno) {
		List<BalanceAdjustmentBO> cafInvData= new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		
		try{
			builder.append("SELECT ci.pmntcustid as custid, ci.acctcafno, ci.invmn, ci.invyr, date_format(ci.invdate, '%d-%m-%Y') as invfdate, ci.custinvno,");
			builder.append(" NULL AS aadharno , NULL AS custname, NULL AS totalAmount");
			builder.append( " FROM cafinv ci WHERE ci.custinvno='"+custInvno+"'" );
			query = getEntityManager().createNativeQuery(builder.toString(), BalanceAdjustmentBO.class);
			cafInvData = query.getResultList();
		}catch(Exception e){
			logger.error("EXCEPTION::showCafinvDetailsByCustinvno() " + e.getMessage());
		}
		
		return cafInvData;
	}

	@SuppressWarnings("unchecked")
	public List<BalAdjCafinvDtslBO> checkedCafInvCafNos(String acctCafNo, String billNo) {
		List<BalAdjCafinvDtslBO> cafinvdtlsData = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try{
			builder.append("SELECT cid.chargeddate,cid.invdtlid, cid.acctcafno, cid.chargecode, cid.prodcode, cid.chargeamt ");
			builder.append(" , (cid.srvctax+cid.swatchtax+cid.kisantax+cid.enttax) AS taxamt ,cid.custdistuid, cid.pmntcustid ");
			builder.append(" ,NULL AS paidAmount, cid.chargetdate, cid.chargefdate, cid.rectype, cid.prodcafno,cid.stbcafno, cid.srvccode,  cid.tenantcode,cid.rsagruid ");
			builder.append(" ,cid.custinvno, cid.enttax, cid.kisantax, cid.srvctax, cid.swatchtax, cid.featurecode,cid.status, ci.invno AS cafinvno ");
			builder.append(" FROM cafinvdtls cid,cafinv ci ");
			builder.append(" WHERE cid.acctcafno IN ('"+acctCafNo+"') and cid.custinvno = '"+billNo+"' and cid.custinvno = ci.custinvno ");
			builder.append(" AND ci.acctcafno IN ('"+acctCafNo+"') ");
			builder.append(" AND cid.custinvno IS NOT NULL ");
			builder.append(" AND MONTH(chargeddate) IN (MONTH(NOW()), MONTH(NOW())-1,MONTH(NOW())-3,MONTH(NOW())-2) ");
			query = getEntityManager().createNativeQuery(builder.toString(), BalAdjCafinvDtslBO.class);
			cafinvdtlsData = query.getResultList();
		}catch(Exception e){
			logger.error("EXCEPTION::showCafinvDetailsByCustinvno() " + e.getMessage());
		}
		return cafinvdtlsData;
	}

	public invAdjustment saveBalAdj(invAdjustment invAdjList) {
		invAdjustment ind = null;
		try{
			ind = getEntityManager().merge(invAdjList);
		}catch(Exception e){
			logger.error("EXCEPTION::showCafinvDetailsByCustinvno() " + e.getMessage());
		}
		
		return ind;
		
	}

	public CpeStock findCpeStockBySrlNo(String cpeSrlNo) {
		CpeStock cpeStock = null;
		StringBuilder builder = null;
		TypedQuery<CpeStock> query = null;
		try {
			logger.info("START::findCpeStockBySrlNo()");
			builder = new StringBuilder(" FROM ").append(CpeStock.class.getSimpleName()).append(" WHERE cpeslno =:cpeslno ");
			query = getEntityManager().createQuery(builder.toString(), CpeStock.class);
			query.setParameter("cpeslno", cpeSrlNo);
			cpeStock = query.getSingleResult();
		} catch(Exception e) {
			logger.error("The Exception is findCpeStockBySrlNo()::  " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return cpeStock;
	}

	public Cpecharges findCpeCharge(Long profileId, String cpeSrlNo) {
		
		Cpecharges cpeCharges = null;
		StringBuilder builder = null;
		Query  query = null;
		try {
			logger.info("START::findCpeStockBySrlNo()");
			builder = new StringBuilder("SELECT * FROM cpecharges ch, cpestock cpe WHERE ch.profile_id = :profileId ");
			builder.append(" AND cpe.batchdate BETWEEN ch.effectivefrom AND ch.effectiveto  AND cpe.cpeslno = :cpeSrlNo");
			query = getEntityManager().createNativeQuery(builder.toString(), Cpecharges.class);
			query.setParameter("profileId", profileId);
			query.setParameter("cpeSrlNo", cpeSrlNo);
			cpeCharges = (Cpecharges) query.getSingleResult();
		} catch(Exception e) {
			logger.error("The Exception is findCpeStockBySrlNo()::  " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return cpeCharges;
	}

	public void updateTenatWallet(BigDecimal oldUpFrontCharges, BigDecimal newUpFrontCharges, String tenantCode) {
		
		StringBuilder builder = new StringBuilder(
				"UPDATE tenantswallet SET cpeblkamt = cpeblkamt + " + newUpFrontCharges + ", cperelamt = cperelamt + "
						+ oldUpFrontCharges + " WHERE tenantcode = '" + tenantCode + "'");
		
		Query query = null;

		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
		}catch(Exception e){
			logger.error("The Exception is updateTenatWallet()::  " +e);
			e.printStackTrace();
		}
	}

	public String findSameCpeType(long oldProfileId, long newProfileId) {
		String returnVal = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(
				" SELECT "
				+ " CASE WHEN (SELECT cpetypelov FROM cpe_profilemaster WHERE profile_id = "+oldProfileId+") = "
				+ " (SELECT cpetypelov FROM cpe_profilemaster WHERE profile_id = "+newProfileId+") "
				+ " THEN 'true' ELSE 'false' END AS opt");
		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			returnVal = (String) query.getSingleResult();
		}catch(Exception e){
			logger.error("The Exception is updateTenatWallet()::  " +e);
			e.printStackTrace();
		}
		return returnVal;
	}
	
	@SuppressWarnings("unchecked")
	public List<HSICummSummaryMonthlyCustViewDTO> getTotalAAAUsagePerMonthCustomerDayWise(String month, String year,
			String cafNo) {
		List<HSICummSummaryMonthlyCustViewDTO> returnList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT custdistuid,customerid,acctcafno,usageyyyy,usagemm, dayn, ");
						builder.append("daydnldusage,dayupldusage,dlh01,dlh02,dlh03,dlh04,dlh05,dlh06,dlh07,dlh08,dlh09,dlh10");
						builder.append(",dlh11,dlh12,dlh13,dlh14,dlh15,dlh16,dlh17,dlh18,dlh19,dlh20,dlh21,dlh22,dlh23,dlh24");
						builder.append(",ulh01,ulh02,ulh03,ulh04,ulh05,ulh06,ulh07,ulh08,ulh09,ulh10,ulh11,ulh12,ulh13,ulh14");
						builder.append(",ulh15,ulh16,ulh17,ulh18,ulh19,ulh20,ulh21,ulh22,ulh23,ulh24 FROM (");
		String changeMonth = month;
		if(Integer.parseInt(month) < 10)
			changeMonth = month.substring(1);
		
		LocalDate date = LocalDate.of(Integer.parseInt(year), Integer.parseInt(changeMonth), 1);
		int days = date.lengthOfMonth();
		for (int i=1; i <= days ; i++ ){
			String day = i < 10 ? String.valueOf("0"+i) : String.valueOf(i);
			builder.append(this.getQuery(year,changeMonth,cafNo,day));
			if(i != days)
				builder.append(" UNION");
		}
		builder.append(" )a  ORDER BY dayn");
		logger.info("Hsi View Query =====>  "+builder.toString());
		try{
			
			returnList = getEntityManager().createNativeQuery(builder.toString(), HSICummSummaryMonthlyCustViewDTO.class).getResultList();
			
		}catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return returnList;
	}
	
	private String getQuery(String year, String changeMonth, String cafNo, String day) {
		StringBuilder builder = new StringBuilder();
		String dayOfMonth = Integer.parseInt(day) < 10 ? day.substring(1) : day ;
		builder.append(" SELECT custdistuid,customerid,acctcafno,usageyyyy,usagemm, "+dayOfMonth+" dayn");
		builder.append("  ,SPLIT_STRING(day"+day+"usage,',', 1)+SPLIT_STRING(day"+day+"usage,',', 2)+SPLIT_STRING(day"+day+"usage,',', 3)+SPLIT_STRING(day"+day+"usage,',' ,4)+SPLIT_STRING(day"+day+"usage,',' ,5)+SPLIT_STRING(day"+day+"usage,',' ,6)+SPLIT_STRING(day"+day+"usage,',' ,7)+SPLIT_STRING(day"+day+"usage,',' ,8)+SPLIT_STRING(day"+day+"usage,',' ,9)+SPLIT_STRING(day"+day+"usage,',',10)+SPLIT_STRING(day"+day+"usage,',',11)+SPLIT_STRING(day"+day+"usage,',',12)");
		builder.append(" +SPLIT_STRING(day"+day+"usage,',',13)+SPLIT_STRING(day"+day+"usage,',',14)+SPLIT_STRING(day"+day+"usage,',',15)+SPLIT_STRING(day"+day+"usage,',',16)+SPLIT_STRING(day"+day+"usage,',',17)+SPLIT_STRING(day"+day+"usage,',',18)+SPLIT_STRING(day"+day+"usage,',',19)+SPLIT_STRING(day"+day+"usage,',',20)+SPLIT_STRING(day"+day+"usage,',',21)+SPLIT_STRING(day"+day+"usage,',',22)+SPLIT_STRING(day"+day+"usage,',',23)+SPLIT_STRING(day"+day+"usage,',',24) daydnldusage");
		builder.append("  ,SPLIT_STRING(day"+day+"usage,',',25)+SPLIT_STRING(day"+day+"usage,',',26)+SPLIT_STRING(day"+day+"usage,',',27)+SPLIT_STRING(day"+day+"usage,',',28)+SPLIT_STRING(day"+day+"usage,',',29)+SPLIT_STRING(day"+day+"usage,',',30)+SPLIT_STRING(day"+day+"usage,',',31)+SPLIT_STRING(day"+day+"usage,',',32)+SPLIT_STRING(day"+day+"usage,',',33)+SPLIT_STRING(day"+day+"usage,',',34)+SPLIT_STRING(day"+day+"usage,',',35)+SPLIT_STRING(day"+day+"usage,',',36)");
		builder.append(" +SPLIT_STRING(day"+day+"usage,',',37)+SPLIT_STRING(day"+day+"usage,',',38)+SPLIT_STRING(day"+day+"usage,',',39)+SPLIT_STRING(day"+day+"usage,',',40)+SPLIT_STRING(day"+day+"usage,',',41)+SPLIT_STRING(day"+day+"usage,',',42)+SPLIT_STRING(day"+day+"usage,',',43)+SPLIT_STRING(day"+day+"usage,',',44)+SPLIT_STRING(day"+day+"usage,',',45)+SPLIT_STRING(day"+day+"usage,',',46)+SPLIT_STRING(day"+day+"usage,',',47)+SPLIT_STRING(day"+day+"usage,',',48) dayupldusage");
		builder.append("  ,SPLIT_STRING(day"+day+"usage,',', 1) dlh"+day+",SPLIT_STRING(day"+day+"usage,',', 2) dlh02,SPLIT_STRING(day"+day+"usage,',', 3) dlh03,SPLIT_STRING(day"+day+"usage,',' ,4) dlh04,SPLIT_STRING(day"+day+"usage,',' ,5) dlh05,SPLIT_STRING(day"+day+"usage,',' ,6) dlh06,SPLIT_STRING(day"+day+"usage,',' ,7) dlh07,SPLIT_STRING(day"+day+"usage,',' ,8) dlh08,SPLIT_STRING(day"+day+"usage,',' ,9) dlh09,SPLIT_STRING(day"+day+"usage,',',10) dlh10,SPLIT_STRING(day"+day+"usage,',',11) dlh11,SPLIT_STRING(day"+day+"usage,',',12) dlh12");
		builder.append("  ,SPLIT_STRING(day"+day+"usage,',',13) dlh13,SPLIT_STRING(day"+day+"usage,',',14) dlh14,SPLIT_STRING(day"+day+"usage,',',15) dlh15,SPLIT_STRING(day"+day+"usage,',',16) dlh16,SPLIT_STRING(day"+day+"usage,',',17) dlh17,SPLIT_STRING(day"+day+"usage,',',18) dlh18,SPLIT_STRING(day"+day+"usage,',',19) dlh19,SPLIT_STRING(day"+day+"usage,',',20) dlh20,SPLIT_STRING(day"+day+"usage,',',21) dlh21,SPLIT_STRING(day"+day+"usage,',',22) dlh22,SPLIT_STRING(day"+day+"usage,',',23) dlh23,SPLIT_STRING(day"+day+"usage,',',24) dlh24");
		builder.append(" ,SPLIT_STRING(day"+day+"usage,',',25) ulh"+day+",SPLIT_STRING(day"+day+"usage,',',26) ulh02,SPLIT_STRING(day"+day+"usage,',',27) ulh03,SPLIT_STRING(day"+day+"usage,',',28) ulh04,SPLIT_STRING(day"+day+"usage,',',29) ulh05,SPLIT_STRING(day"+day+"usage,',',30) ulh06,SPLIT_STRING(day"+day+"usage,',',31) ulh07,SPLIT_STRING(day"+day+"usage,',',32) ulh08,SPLIT_STRING(day"+day+"usage,',',33) ulh09,SPLIT_STRING(day"+day+"usage,',',34) ulh10,SPLIT_STRING(day"+day+"usage,',',35) ulh11,SPLIT_STRING(day"+day+"usage,',',36) ulh12");
		builder.append(" ,SPLIT_STRING(day"+day+"usage,',',37) ulh13,SPLIT_STRING(day"+day+"usage,',',38) ulh14,SPLIT_STRING(day"+day+"usage,',',39) ulh15,SPLIT_STRING(day"+day+"usage,',',40) ulh16,SPLIT_STRING(day"+day+"usage,',',41) ulh17,SPLIT_STRING(day"+day+"usage,',',42) ulh18,SPLIT_STRING(day"+day+"usage,',',43) ulh19,SPLIT_STRING(day"+day+"usage,',',44) ulh20,SPLIT_STRING(day"+day+"usage,',',45) ulh21,SPLIT_STRING(day"+day+"usage,',',46) ulh22,SPLIT_STRING(day"+day+"usage,',',47) ulh23,SPLIT_STRING(day"+day+"usage,',',48) ulh24");
		builder.append(" FROM hsicumusage WHERE usagemm = '"+changeMonth+"' AND usageyyyy = '"+year+"' AND acctcafno = '"+cafNo+"' ");
		builder.append(" AND CURRENT_DATE >= STR_TO_DATE((usageyyyy * 10000)+(usagemm * 100) + "+dayOfMonth+", '%Y%m%d')  ");
		return builder.toString();
	}
	

	@SuppressWarnings("unchecked")
	public List<HSICummSummaryMonthlyViewDTO> getTotalAAAUsagePerMonthDaywise(String month, String year) {
		List<HSICummSummaryMonthlyViewDTO> returnList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("SELECT * FROM v_hsicumsmrydtl WHERE usageyyyymm ="+year+""+month+" AND CURRENT_DATE >= STR_TO_DATE((usageyyyymm * 100) + dayn, '%Y%m%d') ORDER BY dayn desc");
		logger.info("Hsi View Query =====>  "+builder.toString());
		try{
			
			returnList = getEntityManager().createNativeQuery(builder.toString(), HSICummSummaryMonthlyViewDTO.class).getResultList();
			
		}catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return returnList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getPrevHSIData(String flag,String yy,String mm) {
		List<Object[]> obj = null;
		
		StringBuilder builder;
		if(flag.equalsIgnoreCase("I")){
		builder = new StringBuilder("select SUM(dnldsize) as donload ,SUM(upldsize) as upload from hsicumusage where acctcafno in(select cafs.cafno from cafs where cafs.custtypelov='INDIVIDUAL' ) and usagemm='"+mm+"' and usageyyyy='"+yy+"'");
		
		
		}else{
			builder = new StringBuilder("select SUM(dnldsize) as donload ,SUM(upldsize) as upload from hsicumusage where acctcafno in(select cafs.cafno from cafs where cafs.custtypelov='ENTERPRISE' ) and usagemm='"+mm+"' and usageyyyy='"+yy+"'");
		}
		logger.info("Query:"+builder.toString());
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			obj = query.getResultList();
		} catch (Exception e) {
			logger.error("Exception while fetching hsi data:" + e);
		}finally{
			query = null;
			builder = null;
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getPrevHSISUMData(String yyyymmval) {
		
		List<Object[]> obj = null;
		
		StringBuilder builder;
		
		builder = new StringBuilder("select SUM(dnldsize) as donload ,SUM(upldsize) as upload from hsicumsmry where  usageyyyymm='"+yyyymmval+"'");
		
		
		
		logger.info("QueryHSISUM:"+builder.toString());
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			obj = query.getResultList();
		} catch (Exception e) {
			logger.error("Exception while fetching hsi data:" + e);
		}finally{
			query = null;
			builder = null;
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getPrevInvoiceDtls(String flag,String yy,String mm) {
		List<Object> obj = null;
		
		StringBuilder builder;
		if(flag.equalsIgnoreCase("I")){
			builder = new StringBuilder("select SUM(invamt+srvctax+swatchtax+kisantax+enttax) as amount from custinv where custdistuid!=0 and status=1 and invmn='"+mm+"' and invyr='"+yy+"'");
		}else{
			builder = new StringBuilder("select SUM(invamt+srvctax+swatchtax+kisantax+enttax) as amount from custinv where custdistuid=0 and status=1 and invmn='"+mm+"' and invyr='"+yy+"'");
		}
		logger.info("Query:"+builder.toString());
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			obj = query.getResultList();
		} catch (Exception e) {
			logger.error("Exception while fetching hsi data:" + e);
		}finally{
			query = null;
			builder = null;
		}
		return obj;
	}
	
	/*@SuppressWarnings("unchecked")
	 public List<HSICummSummaryMonthlyCustViewDTO> getTotalAAAUsagePerMonthCustomerDayWis(String month, String year,String cafNo) {
	     List<HSICummSummaryMonthlyCustViewDTO> returnList = new ArrayList<>();
	     String changeMonth = month;
	     if(Integer.parseInt(month) < 10){
	     	month = month.substring(1);
    	 }
	     StringBuilder builder = new StringBuilder("SELECT * FROM v_hsicustsmrydtl WHERE usageyyyy ="+year+" AND usagemm ="+month+" acctcafno = "+cafNo+" ");
	     builder.append(" ORDER BY dayn desc");
	
	     logger.info("Hsi View Query =====> "+builder.toString());
	     try{
	
	         returnList = getEntityManager().createNativeQuery(builder.toString(), HSICummSummaryMonthlyCustViewDTO.class).getResultList();
	
	     }catch(Exception e){
	    	 logger.error(e.getMessage());
	         e.printStackTrace();
	     }
	     return returnList;
	 }*/
// new code
	@SuppressWarnings("unchecked")
	public LmoDetailsBO findParticularCustomer1(String tenantCode, String tenantType) {
		
		LmoDetailsBO lmosList = new LmoDetailsBO();
		StringBuilder builder = new StringBuilder();
			
				logger.info("START::LMOTotalAmount()");
				if(tenantType.trim().equalsIgnoreCase("LMO")) {					
					 builder.append("SELECT SUM(p.pmntamt) pmntamt,(SELECT DISTINCT c.lmocode FROM customermst c  WHERE c.lmocode ='" + tenantCode + "') lmocode, ");
				     builder.append(" (SELECT SUM(c.regbal) FROM customermst c WHERE c.lmocode ='" + tenantCode + "') regbal FROM payments p,customermst c  ");
				     builder.append(" WHERE p.pmntcustid=c.custid AND p.acctcafno='0' AND p.status=1  AND c.lmocode=p.createdby AND p.createdby='" + tenantCode + "'");
				}
				try {
				Query query = getEntityManager().createNativeQuery(builder.toString(),LmoDetailsBO.class);
				lmosList = (LmoDetailsBO) query.getSingleResult();
				logger.info("END::LMOTotalAmount()");
			} catch (Exception e) {
				logger.error("EXCEPTION::LMOTotalAmount() " + e);
			}finally{
				builder = null;
			}
		  return lmosList;
		  
	}

//new code


 @SuppressWarnings("unchecked")
	public List<Object[]> getMultiActionCafList1(MultiAction multiAction) {
		List<Object[]> cafsList = new ArrayList<>();
	        Query query = null;
	        StringBuilder builder = new StringBuilder();
	        String whereClause="";
	        try{
		        if(!multiAction.getTenantType().equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode())) {
					whereClause = "and cf.lmocode = '" +multiAction.getTenantCode()+"'";
				}
				
//				if(multiAction.getOrganizationName() != null && !multiAction.getOrganizationName().isEmpty()) {
//					whereClause = " and c.custname like '%"+multiAction.getOrganizationName()+"%' ";
//				}
//				if(multiAction.getCafSubType() != null && !multiAction.getCafSubType().isEmpty()) {
//					whereClause = " and c.enttypelov = '"+multiAction.getCafSubType()+"' ";
//				}
//				if(multiAction.getCafType() != null && !multiAction.getCafType().isEmpty()) {
//					whereClause = whereClause + " and cf.custtypelov = '"+multiAction.getCafType()+"' ";
//				}
//				
//				if((multiAction.getEffectiveFrom() != null && multiAction.getEffectiveTo() != null) && (!multiAction.getEffectiveFrom().isEmpty() && !multiAction.getEffectiveTo().isEmpty()) ) {
//					Date fromDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveFrom());
//					Date toDate = new SimpleDateFormat("MM/dd/yyyy").parse(multiAction.getEffectiveTo());
//					whereClause = whereClause + " and cf.cafdate between '"+ new SimpleDateFormat("yyyy-MM-dd").format(fromDate)+"' and '" + new SimpleDateFormat("yyyy-MM-dd").format(toDate)+"'";
//				} 
//				if(multiAction.getStatus() != null && !multiAction.getStatus().isEmpty()) {
//					if(multiAction.getStatus().equalsIgnoreCase("0")) {
//						whereClause = whereClause + " and (cf.status = "+multiAction.getStatus()+" OR cf.status = "+ComsEnumCodes.Caf_Edit_BulkUpload_status.getStatus()+") ";
//					} else {
//						whereClause = whereClause + " and cf.status = "+multiAction.getStatus()+" ";
//					}
//				}
//				if(multiAction.getCafNo() != null ) {
//					whereClause = whereClause + " and cf.cafno = " + multiAction.getCafNo();
//				} 
//				if(multiAction.getAadharNo() != null) {
//					if(!multiAction.getAadharNo().isEmpty()) {
//						whereClause = whereClause + " and cf.aadharno = '" + multiAction.getAadharNo() + "'";
//					}
//				}
//				if(multiAction.getApsflTrackId() != null && !multiAction.getApsflTrackId().isEmpty() ) {
//					whereClause = whereClause + " and cf.apsfluniqueid like '%"+multiAction.getApsflTrackId()+"%'";
//				}
				if(multiAction.getDistrict() != null && !multiAction.getDistrict().isEmpty()) {
					whereClause = whereClause + " and cf.inst_district = "+multiAction.getDistrict()+" ";
				}
				if(multiAction.getLmoCode() != null && !multiAction.getLmoCode().isEmpty()) {
				whereClause = whereClause + " and cf.lmocode = '"+multiAction.getLmoCode()+"'";
				}
				
				if(multiAction.getMsoCode() != null && !multiAction.getMsoCode().isEmpty()) {
					whereClause = whereClause + " and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+multiAction.getMsoCode()+"%') AND partnertype ='LMO') ";
			}

//				if(multiAction.getMandal() != null && !multiAction.getMandal().isEmpty()) {
//					whereClause = whereClause + " and cf.inst_mandal = "+multiAction.getMandal()+" ";
//				}
//				if(multiAction.getPopName() != null && !multiAction.getPopName().isEmpty()) {
//					whereClause = whereClause + " and cf.pop_substnno = '"+multiAction.getPopName()+"' ";
//				}
//				Calendar now = Calendar.getInstance();   // Gets the current date and time
				String year = multiAction.getUsageYYYY(); 
				String month=multiAction.getUsageMM();
				if(Integer.parseInt(month)<10)
					
				{
					month="0"+month;
				}
				else
				{
					
					month=month;
				}
			try {
					//Query changed on 15_March_2018 by Nagarjuna
					//meged_nag_4_5_2018
					builder.append(" SELECT DISTINCT cf.cafno,c.custname,c.lname,cf.aadharno, ");
					builder.append(" cf.apsfluniqueid,cf.cpeslno, ");
					builder.append(" DATE_FORMAT(cf.cafdate,'%d/%m/%Y') cafDate,cf.contactmob,c.depbal, w.statusdesc, ");
					builder.append(" IFNULL((SELECT SUM(apsflshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='"+year+"' AND invmn='"+month+"' GROUP BY invs.cafno),0.00) AS ApsflShare, ");
					builder.append(" IFNULL((SELECT SUM(msoshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='"+year+"' AND invmn='"+month+"' GROUP BY invs.cafno),0.00) AS MsoShare, ");
					builder.append(" IFNULL((SELECT SUM(lmoshareamt) FROM invamtsharedtls invs WHERE cf.cafno=invs.cafno AND invyr='"+year+"' AND invmn='"+month+"' GROUP BY invs.cafno),0.00) AS LmoShare, ");
//					builder.append(" IFNULL((SELECT SUM(cafinvdtls.chargeamt) FROM cafinvdtls cafinvdtls, chargecodes chargecodes WHERE chargecodes.chargecode=cafinvdtls.chargecode AND chargecodes.chargecode IN ('SRVCRENT') AND cafinvdtls.pmntcustid=cf.pmntcustid AND DATE_FORMAT(chargeddate,'%Y-%m')='"+year+"-"+month+"' AND cafinvdtls.acctcafno=cf.cafno GROUP BY cafno),0.00) AS Monthlycharges, ");
//					builder.append(" IFNULL((SELECT SUM(cafinvdtls.chargeamt) FROM cafinvdtls cafinvdtls, chargecodes chargecodes WHERE chargecodes.chargecode=cafinvdtls.chargecode AND chargecodes.chargecode IN ('CPEEMI') AND cafinvdtls.pmntcustid=cf.pmntcustid AND DATE_FORMAT(chargeddate,'%Y-%m')='"+year+"-"+month+"'AND cafinvdtls.acctcafno=cf.cafno GROUP BY cafno),0.00) AS OneTimeCharges, ");
//					builder.append(" IFNULL((SELECT SUM(chargeamt) FROM cafinvdtls WHERE pmntcustid=cf.pmntcustid AND acctcafno=cf.cafno AND DATE_FORMAT(chargeddate,'%Y-%m')='"+year+"-"+month+"'AND chargecode IN ('LOCALUSAGE', 'STDUSAGE', 'ISDUSAGE')),0.00) AS TelephoneUsageCharges,cfv.srvctax AS Tax,");
					builder.append(" IFNULL((SELECT SUM(invamt+srvctax) FROM cafinv WHERE pmntcustid=cf.pmntcustid AND acctcafno=cf.cafno AND invyr='"+year+"' AND invmn='"+month+"' ),0.00) AS Total,");
					builder.append(" IFNULL((SELECT prevbal FROM custinv cus WHERE cus.prevpaidamt='0' AND cus.pmntcustid=cf.pmntcustid AND invyr='"+year+"' AND invmn='"+month+"' GROUP BY cus.pmntcustid),0.00) AS PreviousBalance");
//					builder.append(" IFNULL((SELECT SUM(invamt+srvctax) FROM cafinv WHERE pmntcustid=cf.pmntcustid AND acctcafno=cf.cafno ),0.00) AS TotalPayable,p.pmntamt ");							
					builder.append(" FROM  cafs cf, cafinv cfv,customermst c ,wipstages w WHERE cf.custid = c.custid AND cf.cafno=cfv.acctcafno AND cfv.invyr='"+year+"' AND cfv.invmn='"+month+"'  AND w.appcode = 'COMS' AND w.statuscode = cf.status "+whereClause);
					query = getEntityManager().createNativeQuery(builder.toString());
					cafsList = query.getResultList();

				} catch (Exception e) {
					logger.error("EXCEPTION::getMultiActionCafList1() " + e.getMessage());
				} finally {
					query = null;
					builder = null;
				}
		} catch(Exception e) {
			logger.error("EXCEPTION::getMultiActionCafList1() " + e);
			e.printStackTrace();
		}
			return cafsList;
	}


// change code  09-jan-2018
	
	@SuppressWarnings("unchecked")
	public List<MonthlyPaymentBO> getMonthlyCafDetails1(String cafNo, String tenantCode, Integer custId) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		StringBuilder builder = new StringBuilder("");
		Query query = null;
		try {
			if(custId != null) {
				builder.append( "SELECT c.custname, c.custcode, c.regbal + c.chargedbal AS regbal, ");
				builder.append(" c.custid, c.custtypelov, c.lname, c.custdistuid,c.pocmob1 FROM customermst c where c.custid='"+custId+"' and c.lmocode = '"+tenantCode+"' and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ");
			} else if(cafNo != null && !cafNo.isEmpty()) {
				builder.append(" SELECT c.custname, c.custcode, c.regbal + c.chargedbal AS regbal, ");
				builder.append(" c.custid, c.custtypelov, c.lname, c.custdistuid,c.pocmob1 FROM customermst c,cafs cf where cf.cafno='"+cafNo+"' and c.lmocode = '"+tenantCode+"' and cf.custid=c.custid and c.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" ");
			}
			query = getEntityManager() .createNativeQuery(builder.toString(), MonthlyPaymentBO.class);
			cafList = query.getResultList();
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return cafList;
	}
	/*@SuppressWarnings("unchecked")
	public List<BulkCustomerDTO> getMonthlyCafDetails2(String from, String tenantCode, String to) {
		List<BulkCustomerDTO> cafList = new ArrayList<BulkCustomerDTO>();
		List<Object[]> cafList1 = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("");
		String yr= from+"-31";
		Query query = null;
		try {
			
				builder.append( "select c.cafno,mst.custname,mst.pocmob1,sum(invamt) AS regbal from custinv inv,cafs c,customermst mst where inv.prevpaidamt=0 and mst.custid=c.custid and c.lmocode='"+tenantCode+"' and invtdate<='"+yr+"'  and curdate()>='"+yr+"' and c.custid=inv.pmntcustid  group by c.cafno");
						
				query = getEntityManager() .createNativeQuery(builder.toString());
			cafList1 = query.getResultList();
			for(Object[] i:cafList1) {
				BulkCustomerDTO temp=new BulkCustomerDTO();
				temp.setCafno(i[0].toString());
				temp.setCustomername(i[1].toString());
				temp.setMobile(i[2].toString());
				temp.setDueamount(i[3].toString());
				if(temp!=null && Float.valueOf(temp.getDueamount())>0)cafList.add(temp);
				
				
			}
			
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return cafList;
	}*///siddharth_7_5_2018

	
	@SuppressWarnings("unchecked")
	public List<BulkCustomerDTO> getMonthlyCafDetails2(String from, String tenantCode, String to) {
		
		List<BulkCustomerDTO> cafList = new ArrayList<BulkCustomerDTO>();
		List<BulkCustomerDTO> cafList2 = new ArrayList<BulkCustomerDTO>();
		List<Object[]> cafList1 = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("");
		// String yr= from+"-31";
		Query query = null;
		
		String string = from;
		String[] parts = string.split("-");
		String yr = parts[0]; // 004
		String mn = parts[1]; // 034556
		String mnprev=String.valueOf(Integer.parseInt(mn)-1);
		String prevyr=yr;
		String PrevPaid=null;
		
		/*if(Integer.parseInt(mn)==1)
		{	mnprev="12";
		prevyr=String.valueOf(Integer.parseInt(yr)-1);
		}*/
		
		//if prev =t then 
		try {
			
			//PrevPaid=getPrevPaid(from,tenantCode,to);
			
		
				builder.append( "select Distinct inv.pmntcustid,c.cafno,mst.custname,mst.pocmob1,sum(invamt+prevbal+srvctax-prevpymtreceived) AS regbal,prevbal from custinv inv,cafs c,customermst mst where inv.prevpaidamt=0 and mst.custid=c.custid and c.lmocode='"+tenantCode+"' and inv.invyr='"+yr+"'  and inv.invmn='"+mn+"' and c.custid=inv.pmntcustid  group by c.cafno");

	//builder.append( "select c.cafno,mst.custname,mst.pocmob1,sum(invamt+srvctax) AS regbal,inv.pmntcustid from custinv inv,cafs c,customermst mst where inv.prevpaidamt=0 and mst.custid=c.custid and c.lmocode='"+tenantCode+"' and inv.invyr='"+yr+"'  and inv.invmn='"+mn+"' and c.custid=inv.pmntcustid  group by c.cafno");
						
				query = getEntityManager() .createNativeQuery(builder.toString());
			cafList1 = query.getResultList();
			
			
			for(Object[] i:cafList1) {
				BulkCustomerDTO temp=new BulkCustomerDTO();
				temp.setCafno(i[1].toString());
				temp.setCustomername(i[2].toString());
				temp.setMobile(i[3].toString());
				
			temp.setDueamount(i[4].toString());
			temp.setPmntcustid(i[0].toString());
			temp.setPrevbal(i[5].toString());
				
				if(temp!=null && Float.valueOf(temp.getDueamount())>0)cafList.add(temp);
				
				
			}
			
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		
		//cafList2=getPrevPaid(from,tenantCode,to,cafList);
		return cafList;
	}
	
	
	
	
/*public List<BulkCustomerDTO> getPrevPaid(String from, String tenantCode, String to,List<BulkCustomerDTO> cafListPrevpaid) {
		
		List<BulkCustomerDTO> cafList = new ArrayList<BulkCustomerDTO>();
		Integer cafList1 ;
		StringBuilder builder = new StringBuilder("");
		// String yr= from+"-31";
	//	List<BulkCustomerDTO> cafListQuery query = null;
		Query query = null;
		
		String string = from;
		String[] parts = string.split("-");
		String yr = parts[0]; // 004
		String mn = parts[1]; // 034556
		String mnprev=String.valueOf(Integer.parseInt(mn)-1);
		String prevyr=yr;
		String prevPaidFlag = null;
		
		if(Integer.parseInt(mn)==1)
		{	mnprev="12";
		prevyr=String.valueOf(Integer.parseInt(yr)-1);
		}
		
		//if prev =t then 
		try {
						
				
				for( BulkCustomerDTO custDto:cafListPrevpaid) {
					builder.setLength(0);
					
					builder.append( "select inv.prevpaidamt from custinv inv,cafs c,customermst mst where mst.custid=c.custid and c.custid='" +custDto.getPmntcustid()+"' and  c.lmocode='"+tenantCode+"' and inv.invyr='"+prevyr+"'  and inv.invmn='"+mnprev+"' and c.custid=inv.pmntcustid  group by c.cafno");
					
					query = getEntityManager() .createNativeQuery(builder.toString());
				cafList1 =  (Integer) query.getResultList().get(0);
				prevPaidFlag=cafList1.toString();
					
					if (prevPaidFlag.equals("1"))
						custDto.setDueamount(String.valueOf(round((Float.parseFloat(custDto.getDueamount())-Float.parseFloat((custDto.getPrevbal()))),2)));

				}
			
				
			
		} catch(Exception ex){
			logger.error("EXCEPTION::getMonthlyCafDetails() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		
		return cafListPrevpaid ;
	}
	
	
public static float round(float d, int decimalPlace) {
    BigDecimal bd = new BigDecimal(Float.toString(d));
    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
    return bd.floatValue();
}
	
	
	*/
	
	
	
	
	
	
	
	
	
	
	//new code vijaya 
	
	@SuppressWarnings("unchecked")
	public List<CustomerCafBO> findCafDaoByCafNo(String tenantCode, String tenantType, String DistrictId, String lmocode1, PageObject pageObject ) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		String whereClause1 = "";
		long count = 0l;
		Query countQuery = null;
		String searchParameter = "";
		try {
			logger.info("START::findCustomers()");

			if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode()) && tenantType.equalsIgnoreCase(ComsEnumCodes.Call_Center_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode = '"+tenantCode+"' ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.MSP_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.MSP_Tenant_Type.getCode()+"' AND partnercode = '"+tenantCode+"')) ";
			}
			if(DistrictId != null && !DistrictId.isEmpty()) {

				whereClause1 = whereClause1 + " and cf.inst_district = "+DistrictId+" ";
			}

			if(lmocode1 != null && !lmocode1.isEmpty()) {

				whereClause1 = whereClause1 + " and cf.lmocode = '"+lmocode1+"' ";
			}
			
//			if((DistrictId == null) && (lmocode1 == null) && (msocode != null && msocode.isEmpty())) {
//				logger.info("amsmamsmas"+DistrictId+"dsvsdvdsvds"+lmocode1);
//
//				whereClause1 = whereClause1 + " and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+msocode+"%') AND partnertype ='LMO')";
//			}
//			if((DistrictId != null && !DistrictId.isEmpty()) && (lmocode1 != null && !lmocode1.isEmpty())) {
//				logger.info("amsmamsmas"+DistrictId+"dsvsdvdsvds"+lmocode1);
//				whereClause1 = whereClause1 + " and cf.inst_district = "+DistrictId+" and cf.lmocode = '"+lmocode1+"' ";
//			}
//			
//			if((DistrictId != null && !DistrictId.isEmpty()) && (msocode != null && !msocode.isEmpty())) {
//				logger.info("amsmamgsdgdssmas"+DistrictId+"dsvstttdvdsvds"+lmocode1);
//				whereClause1 = whereClause1 + " and cf.inst_district = "+DistrictId+" and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+msocode+"%') AND partnertype ='LMO')";
//			}
			builder.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode,cf.contactmob, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, cf.cpeslno, CASE WHEN cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" THEN 'Active' ");
			builder.append(" WHEN cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" THEN 'Pending For Provisioning' WHEN cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" THEN 'Suspended' WHEN cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+" THEN 'De-activated' END STATUS, ");
			builder.append(" IFNULL(DATE_FORMAT(cf.actdate,'%d/%m/%Y'), 'NA') actdate, cust.aadharno,cf.apsfluniqueid FROM cafs cf, customermst cust ");
			builder.append(" WHERE cf.custid = cust.custid "+whereClause+" "+whereClause1);
			
			if(pageObject.getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
				} catch(Exception e) {
					searchParameter = pageObject.getSearchParameter();
				}
			} else {
				searchParameter = pageObject.getSearchParameter();
			}
			String like="AND (cafno LIKE '%"+searchParameter+"%' OR cust.depbal LIKE '%"+searchParameter+"%'  OR cust.regbal LIKE '%"+searchParameter+"%'  OR cf.lmocode LIKE '%"+searchParameter+"%' OR custname LIKE '%"+searchParameter+"%' OR mname LIKE '%"+ searchParameter+"%' OR lname LIKE '%"+searchParameter+"%'  OR cpemacaddr LIKE '%"+searchParameter+"%' OR cf.status LIKE '%"+searchParameter+"%'  OR cust.aadharno LIKE '%"+searchParameter+"%' OR cf.actdate LIKE '%"+searchParameter+"%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			cafsList = query.getResultList();
			logger.info("END::findCustomers()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cafsList;
	}
		
	@SuppressWarnings("unchecked")
	public List<CustomerCafBO> findCafDaoByCafNo(String tenantCode, String tenantType, String msocode, PageObject pageObject ) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		String whereClause1 = "";
		long count = 0l;
		Query countQuery = null;
		String searchParameter = "";
		try {
			logger.info("START::findCustomers()");
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode()) && tenantType.equalsIgnoreCase(ComsEnumCodes.Call_Center_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode = '"+tenantCode+"' ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.MSP_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.MSP_Tenant_Type.getCode()+"' AND partnercode = '"+tenantCode+"')) ";
			}

			if(msocode != null && !msocode.isEmpty()) {
				whereClause1 = whereClause1 + " and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+msocode+"%') AND partnertype ='LMO')";
			}

			builder.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode,cf.contactmob, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, cf.cpeslno, CASE WHEN cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" THEN 'Active' ");
			builder.append(" WHEN cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" THEN 'Pending For Provisioning' WHEN cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" THEN 'Suspended' WHEN cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+" THEN 'De-activated' END STATUS, ");
			builder.append(" IFNULL(DATE_FORMAT(cf.actdate,'%d/%m/%Y'), 'NA') actdate, cust.aadharno,cf.apsfluniqueid FROM cafs cf, customermst cust ");
			builder.append(" WHERE cf.custid = cust.custid "+whereClause+" "+whereClause1);
			
			if(pageObject.getSearchParameter().indexOf("/") > 0) {
				try {
					searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
				} catch(Exception e) {
					searchParameter = pageObject.getSearchParameter();
				}
			} else {
				searchParameter = pageObject.getSearchParameter();
			}
			String like="AND (cafno LIKE '%"+searchParameter+"%' OR cust.depbal LIKE '%"+searchParameter+"%'  OR cust.regbal LIKE '%"+searchParameter+"%'  OR cf.lmocode LIKE '%"+searchParameter+"%' OR custname LIKE '%"+searchParameter+"%' OR mname LIKE '%"+ searchParameter+"%' OR lname LIKE '%"+searchParameter+"%'  OR cpemacaddr LIKE '%"+searchParameter+"%' OR cf.status LIKE '%"+searchParameter+"%'  OR cust.aadharno LIKE '%"+searchParameter+"%' OR cf.actdate LIKE '%"+searchParameter+"%')";
			String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
			builder.append(like + " " + orderBy);
			
			countQuery = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			count = countQuery.getResultList().size();
			pageObject.setTotalDisplayCount(String.valueOf(count));
			
			query = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
			query.setFirstResult(pageObject.getMinSize());
			query.setMaxResults(pageObject.getMaxSize());
			cafsList = query.getResultList();
			logger.info("END::findCustomers()");
		} catch (Exception e) {
			logger.error("EXCEPTION::findCustomers() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cafsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerCafBO> findCafDaoByCafNo1(String tenantCode, String tenantType, String MM, String yyyy, PageObject pageObject ) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		String whereClause = "";
		String whereClause1 = "";
	        String whereClause2 = "";
		long count = 0l;
		Query countQuery = null;
		String searchParameter = "";
		try {
			logger.info("START::findCustomers()");
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.APSFL_Tenant_Type.getCode()) && tenantType.equalsIgnoreCase(ComsEnumCodes.Call_Center_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN("+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+") ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode = '"+tenantCode+"' ";
			} else if(tenantType.equalsIgnoreCase(ComsEnumCodes.MSP_Tenant_Type.getCode())) {
				whereClause = "AND cf.status IN( "+ComsEnumCodes.Caf_Provision_status.getStatus()+", "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+", "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+ ") and cf.lmocode IN (SELECT DISTINCT partnercode FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' AND agruniqueid IN (SELECT agruniqueid FROM rsagrpartners WHERE partnertype = '"+ComsEnumCodes.MSP_Tenant_Type.getCode()+"' AND partnercode = '"+tenantCode+"')) ";
			}
			if(MM != null && !MM.isEmpty()) {

				whereClause1 = whereClause1 + " AND ci.invmn = "+MM+" ";
			}

			if(yyyy != null && !yyyy.isEmpty()) {

					whereClause2 = whereClause2 + " AND ci.invyr = '"+yyyy+"' ";
				}
				
//				if((DistrictId == null) && (lmocode1 == null) && (msocode != null && msocode.isEmpty())) {
//					logger.info("amsmamsmas"+DistrictId+"dsvsdvdsvds"+lmocode1);
	//
//					whereClause1 = whereClause1 + " and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+msocode+"%') AND partnertype ='LMO')";
//				}
//				if((DistrictId != null && !DistrictId.isEmpty()) && (lmocode1 != null && !lmocode1.isEmpty())) {
//					logger.info("amsmamsmas"+DistrictId+"dsvsdvdsvds"+lmocode1);
//					whereClause1 = whereClause1 + " and cf.inst_district = "+DistrictId+" and cf.lmocode = '"+lmocode1+"' ";
//				}
//				
//				if((DistrictId != null && !DistrictId.isEmpty()) && (msocode != null && !msocode.isEmpty())) {
//					logger.info("amsmamgsdgdssmas"+DistrictId+"dsvstttdvdsvds"+lmocode1);
//					whereClause1 = whereClause1 + " and cf.inst_district = "+DistrictId+" and cf.lmocode IN (SELECT DISTINCT partnercode as lmocode FROM rsagrpartners WHERE agruniqueid IN(SELECT DISTINCT agruniqueid FROM RSAGRMNTS WHERE PARTNERLIST LIKE '%"+msocode+"%') AND partnertype ='LMO')";
//				}
				builder.append("SELECT cf.cafno, cust.depbal, cust.regbal, cf.lmocode,cf.contactmob, cust.custname, cust.mname, IFNULL(cust.lname, '') lname, cust.custid, cf.cpeslno, CASE WHEN cf.status = "+ComsEnumCodes.Caf_Provision_status.getStatus()+" THEN 'Active' ");
				builder.append(" WHEN cf.status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+" THEN 'Pending For Provisioning' WHEN cf.status = "+ComsEnumCodes.CAF_SUSPEND_STATUS.getStatus()+" THEN 'Suspended' WHEN cf.status = "+ComsEnumCodes.CAF_DEACTIVE_STATUS.getStatus()+" THEN 'De-activated' END STATUS, ");
				builder.append(" IFNULL(DATE_FORMAT(cf.actdate,'%d/%m/%Y'), 'NA') actdate, cust.aadharno,cf.apsfluniqueid FROM cafs cf, customermst cust,cafinv ci ");
				builder.append(" WHERE cf.custid = cust.custid AND cf.cafno=ci.acctcafno "+whereClause+" "+whereClause1+" "+whereClause2);
				
				if(pageObject.getSearchParameter().indexOf("/") > 0) {
					try {
						searchParameter = DateUtill.dateToStringFormat(DateUtill.stringtoDateFormat(pageObject.getSearchParameter()));
					} catch(Exception e) {
						searchParameter = pageObject.getSearchParameter();
					}
				} else {
					searchParameter = pageObject.getSearchParameter();
				}
				String like="AND (cafno LIKE '%"+searchParameter+"%' OR cust.depbal LIKE '%"+searchParameter+"%'  OR cust.regbal LIKE '%"+searchParameter+"%'  OR cf.lmocode LIKE '%"+searchParameter+"%' OR custname LIKE '%"+searchParameter+"%' OR mname LIKE '%"+ searchParameter+"%' OR lname LIKE '%"+searchParameter+"%'  OR cpemacaddr LIKE '%"+searchParameter+"%' OR cf.status LIKE '%"+searchParameter+"%'  OR cust.aadharno LIKE '%"+searchParameter+"%' OR cf.actdate LIKE '%"+searchParameter+"%')";
				String orderBy = " ORDER BY " + pageObject.getSortColumn() + " " + pageObject.getSortOrder();
				builder.append(like + " " + orderBy);
				
				countQuery = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
				count = countQuery.getResultList().size();
				pageObject.setTotalDisplayCount(String.valueOf(count));
				
				query = getEntityManager().createNativeQuery(builder.toString(), CustomerCafBO.class);
				query.setFirstResult(pageObject.getMinSize());
				query.setMaxResults(pageObject.getMaxSize());
				cafsList = query.getResultList();
				logger.info("END::findCustomers()");
			} catch (Exception e) {
				logger.error("EXCEPTION::findCustomers() " + e);
			} finally {
				query = null;
				builder = null;
			}
			return cafsList;
		}
			

}
