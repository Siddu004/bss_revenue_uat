package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author kiran
 *
 */
@Entity
public class CustomerInvDtlsByNetSubCodeBO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="customerId")
	private String customerId;
	
	@Column(name="customerName")
	private String customerName;
	
	@Column(name="mobileNo")
	private String mobileNo;
	
	@Column(name="dueAmount")
	private String dueAmount;
	
	@Column(name="lastInvNo")
	private String lastInvNo;
	
	@Column(name="lastInvDate")
	private String lastInvDate;
	
	@Column(name="lastInvAmount")
	private String lastInvAmount;
	
	@Column(name="usageLimit")
	private String usageLimit;
	
	@Column(name="hsiUsage")
	private String hsiUsage;
	
	@Column(name="telephoneUsage")
	private String telephoneUsage;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(String dueAmount) {
		this.dueAmount = dueAmount;
	}

	public String getLastInvNo() {
		return lastInvNo;
	}

	public void setLastInvNo(String lastInvNo) {
		this.lastInvNo = lastInvNo;
	}

	public String getLastInvDate() {
		return lastInvDate;
	}

	public void setLastInvDate(String lastInvDate) {
		this.lastInvDate = lastInvDate;
	}

	public String getLastInvAmount() {
		return lastInvAmount;
	}

	public void setLastInvAmount(String lastInvAmount) {
		this.lastInvAmount = lastInvAmount;
	}

	public String getUsageLimit() {
		return usageLimit;
	}

	public void setUsageLimit(String usageLimit) {
		this.usageLimit = usageLimit;
	}

	public String getHsiUsage() {
		return hsiUsage;
	}

	public void setHsiUsage(String hsiUsage) {
		this.hsiUsage = hsiUsage;
	}

	public String getTelephoneUsage() {
		return telephoneUsage;
	}

	public void setTelephoneUsage(String telephoneUsage) {
		this.telephoneUsage = telephoneUsage;
	}
	
	
	
}
