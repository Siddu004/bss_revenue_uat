/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdaywiseDTO;
import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdistrictwiseDTO;
import com.arbiva.apfgc.comsystem.BO.CountdetailsCustomerwiseDTO;
import com.arbiva.apfgc.comsystem.BO.Details;
import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.model.CafAccount;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
public interface CafAccountService {

	public abstract List<CafAccount> findAllCafAccounts();
	
	public abstract CafAccount findByCafNo (Long cafNo, Integer district);

	public abstract void saveCafAccount(CustomerCafVO customerCafVO, String loginID);

	public abstract void updateCafAccount(PaymentVO paymentVO, String loginID);

	public abstract List<CafCountDetailsdistrictwiseDTO> getCountdetailsdistrictwise();

	public abstract List<CountdetailsCustomerwiseDTO> getCountdetailsCustomerwise();

	public abstract CafCountDetailsdaywiseDTO getCafCountDetails(String custtype,String subtype);

	public abstract Object[] getLastTenDaysCountDetails();

	public abstract List<Details> getDistrictINDENTDurationDetails(String custtypelov, String enttypelov);
}
