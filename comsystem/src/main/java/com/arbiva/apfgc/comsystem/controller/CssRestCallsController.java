package com.arbiva.apfgc.comsystem.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.comsystem.dto.AadharHelperDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerInfoDTO;
import com.arbiva.apfgc.comsystem.dto.LCODetailsDTO;
import com.arbiva.apfgc.comsystem.dto.PackAndSrvcsDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.util.ApplictionAutoWires;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AadharDtlsVO;
import com.arbiva.apfgc.comsystem.vo.BillingInfoVO;
import com.arbiva.apfgc.comsystem.vo.CafDetailsVO;
import com.arbiva.apfgc.comsystem.vo.CafStbsVO;
import com.arbiva.apfgc.comsystem.vo.CpeDetailsVO;
import com.arbiva.apfgc.comsystem.vo.PackWiseSrvcsVO;
import com.arbiva.apfgc.comsystem.vo.PaymentDetailsVO;
import com.arbiva.apfgc.comsystem.vo.RecentPaymentVO;

@RestController
public class CssRestCallsController {

	private static final Logger logger = Logger.getLogger(CssRestCallsController.class);

	@Autowired
	ApplictionAutoWires aw;

	@RequestMapping(value = "/getCustomerDetails", method = RequestMethod.POST)
	@ResponseBody
	public List<CpeDetailsVO> getCustomerDetails(@RequestParam(value = "cafNo", required = false) Long cafNo) {

		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<CpeDetailsVO> cpeDtlsVO = new ArrayList<>();
		Object[] list = null;
		List<CafStbsVO> stbInfoLists = new ArrayList<CafStbsVO>();
		try {
			logger.info("CssRestCallsController :: getCustomerDetails() :: Start");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(cafNo);
			list = cafDetailsVO.getCafDao();
			stbInfoLists = cafDetailsVO.getStbInfoList();

			if (list != null) {
				CpeDetailsVO cpeVO = new CpeDetailsVO();
				cpeVO.setLongtitude(list[0] != null ? list[0].toString() : "NA");
				cpeVO.setLattitude(list[1] != null ? list[1].toString() : "NA");
				cpeVO.setOltId(list[2] != null ? list[2].toString() : "NA");
				cpeVO.setPortNo(list[3] != null ? list[3].toString() : "NA");
				cpeVO.setSubStationName(list[4] != null ? list[4].toString() : "NA");
				cpeVO.setAddress(list[5] != null ? list[5].toString() : "NA");
				cpeVO.setVillage(list[6] != null ? list[6].toString() : "NA");
				cpeVO.setMandal(list[7] != null ? list[7].toString() : "NA");
				cpeVO.setDistrict(list[8] != null ? list[8].toString() : "NA");
				cpeVO.setCpeModel(list[9] != null ? list[9].toString() : "NA");
				cpeVO.setCpeSrlNo(list[10] != null ? list[10].toString() : "NA");
				cpeVO.setCpeInstChrge(list[11] != null ? list[11].toString() : "NA");
				cpeVO.setCpeCost(list[12] != null ? list[12].toString() : "NA");
				cpeVO.setPin(list[13] != null ? list[13].toString() : "NA");
				cpeVO.setAddress2(list[14] != null ? list[14].toString() : "NA");
				cpeVO.setCpeInstCount(list[15] != null ? list[15].toString() : "NA");
				cpeVO.setCpeMacAddress(list[16] != null ? list[16].toString() : "NA");
				cpeVO.setCpeLease(list[17] != null ? list[17].toString() : "NA");

				if (stbInfoLists != null) {
					for (CafStbsVO cafVo : stbInfoLists) {
						cpeVO.setIptvBoxCafNo(cafVo.getStbcafno() != null ? cafVo.getStbcafno() : "NA");
						cpeVO.setIptvBoxModel(cafVo.getStbprofileid() != null ? cafVo.getStbprofileid() : "NA");
						cpeVO.setIptvBoxOwnerShip(cafVo.getStbleaseyn() != null ? cafVo.getStbleaseyn() : "NA");
						cpeVO.setIptvBoxCharge(cafVo.getStbboxcharge() != null ? cafVo.getStbboxcharge() : "NA");
						cpeVO.setIptvBoxInstCharge(cafVo.getStbinstcharge() != null ? cafVo.getStbinstcharge() : "NA");
						cpeVO.setIptvBoxEmiCount(cafVo.getStbemicount() != null ? cafVo.getStbemicount() : "NA");
						cpeVO.setIptvBoxSrlNo(cafVo.getStbslno() != null ? cafVo.getStbslno() : "NA");
						cpeVO.setIptvBoxMacId(cafVo.getStbmacaddr() != null ? cafVo.getStbmacaddr() : "NA");
					}
				}
				cpeDtlsVO.add(cpeVO);
			}

			logger.info("CssRestCallsController :: getCustomerDetails() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getCustomerDetails() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return cpeDtlsVO;
	}

	@RequestMapping(value = "/getPackgeDetails", method = RequestMethod.POST)
	@ResponseBody
	public List<PaymentDetailsVO> getPackageDetails(
			@RequestParam(value = "cafNo", required = false, defaultValue = "") String cafNo) {

		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<PaymentDetailsVO> list = new ArrayList<>();
		try {
			logger.info("CssRestCallsController :: getPackageDetails() :: Start");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(Long.parseLong(cafNo));
			list = cafDetailsVO.getNewCafList();
			logger.info("CssRestCallsController :: getPackageDetails() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getPackageDetails() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	@RequestMapping(value = "/getBillingDetails", method = RequestMethod.POST)
	@ResponseBody
	public List<BillingInfoVO> getBillingDetails(@RequestParam(value = "cafNo", required = false) Long cafNo) {

		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<BillingInfoVO> list = new ArrayList<>();
		try {
			logger.info("CssRestCallsController :: getBillingDetails() :: Start");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(cafNo);
			list = cafDetailsVO.getBillInfo();
			logger.info("CssRestCallsController :: getBillingDetails() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getBillingDetails() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	@RequestMapping(value = "/getPaymentDetails", method = RequestMethod.POST)
	@ResponseBody
	public List<RecentPaymentVO> getPaymentDetails(@RequestParam(value = "cafNo", required = false) Long cafNo) {

		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<RecentPaymentVO> list = new ArrayList<>();
		try {
			logger.info("CssRestCallsController :: getPaymentDetails() :: Start");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(cafNo);
			list = cafDetailsVO.getPaymentList();
			logger.info("CssRestCallsController :: getPaymentDetails() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getPaymentDetails() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	@RequestMapping(value = "/getCafUsage", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String> getCafUsage(@RequestParam(value = "yyyy") String yyyy,
			@RequestParam(value = "mm") String mm, @RequestParam(value = "cafNo") String cafNo) {

		Map<String, String> list = null;
		try {
			logger.info("CssRestCallsController :: getCafUsage() :: Start");
			list = aw.getPaymentService().getCafUsage(yyyy.trim(), mm.trim(), cafNo.trim());
			logger.info("CssRestCallsController :: getCafUsage() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getCafUsage() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	@RequestMapping(value = "/getPackagesAndServices", method = RequestMethod.GET)
	@ResponseBody
	@Produces("application/json")
	public PackAndSrvcsDTO getPackagesAndServices(@RequestParam(value = "cafNo", required = false) Long cafNo) {

		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<PaymentDetailsVO> list = new ArrayList<>();
		List<PaymentDetailsVO> packslist = new ArrayList<>();
		PackAndSrvcsDTO dto = new PackAndSrvcsDTO();
		try {
			logger.info("CssRestCallsController :: getPackagesAndServices() :: Start");
			cafDetailsVO = aw.getPaymentService().getRecentPayment(cafNo);
			list = cafDetailsVO.getNewCafList();
			for (PaymentDetailsVO payment : list) {
				PackWiseSrvcsVO srvcsVO = new PackWiseSrvcsVO();
				PaymentDetailsVO pmntVO = new PaymentDetailsVO();
				pmntVO.setCreatedon(payment.getCreatedon());
				Float amount = Float.parseFloat(payment.getProdcharge()) + Float.parseFloat(payment.getProdtax());
				pmntVO.setProdcharge(Float.toString(amount));
				pmntVO.setProdname(payment.getProdname());
				pmntVO.setProdtype(payment.getProdtype());
				if ((payment.getCoresrvccode()).equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_HSI.getCode())) {
					srvcsVO.setHsi(payment.getSrvcname());
					pmntVO.setServices(srvcsVO);
				} else if ((payment.getCoresrvccode()).equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode())) {
					srvcsVO.setIptv(payment.getSrvcname());
					pmntVO.setServices(srvcsVO);
				} else if ((payment.getCoresrvccode()).equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_VOIP.getCode())) {
					srvcsVO.setVoip(payment.getSrvcname());
					pmntVO.setServices(srvcsVO);
				}
				packslist.add(pmntVO);
			}
			dto.setPaymentDtlsList(packslist);
			dto.setSuccess("Success");
			dto.setSuccessMessage("Success Message");
			logger.info("CssRestCallsController :: getPackagesAndServices() :: End");
		} catch (Exception e) {
			logger.info("CssRestCallsController :: getPackagesAndServices() :: " + e);
			e.printStackTrace();
		} finally {
		}
		return dto;

	}

	@RequestMapping(value = "/getCustomerDetailsById", method = RequestMethod.GET)
	@ResponseBody
	public CustomerInfoDTO getCustomerDetailsByLoginId(
			@RequestParam(value = "loginId", required = false) String loginId) {

		CustomerInfoDTO custInfo = new CustomerInfoDTO();
		try {
			logger.info("CssRestCallsController :: getCustomerDetailsByLoginId() :: Start");
			custInfo = aw.getCustomerService().getCustDtlsByLoginid(loginId);
			logger.info("CssRestCallsController :: getCustomerDetailsByLoginId() :: End");
		} catch (Exception e) {
			logger.error("CssRestCallsController :: getCustomerDetailsByLoginId() ::" + e);
			e.printStackTrace();
		}
		return custInfo;
	}

	@RequestMapping(value = "/getNearestLCO", method = RequestMethod.GET)
	@ResponseBody
	public CustomerInfoDTO getNearestLCO(@RequestParam(value = "districtUid", required = false) String districtId,
			@RequestParam(value = "mandalSlno", required = false) String mandalId,
			@RequestParam(value = "villageSlno", required = false) String villageId) {

		List<LCODetailsDTO> lcoInfo = new ArrayList<>();
		CustomerInfoDTO lcoList = new CustomerInfoDTO();
		try {
			logger.info("CssRestCallsController :: getCustomerDetailsByLoginId() :: Start");
			lcoInfo = aw.getCustomerService().getNearestLCO(districtId, mandalId, villageId);
			lcoList.setLcoList(lcoInfo);
			logger.info("CssRestCallsController :: getCustomerDetailsByLoginId() :: End");
		} catch (Exception e) {
			logger.error("CssRestCallsController :: getCustomerDetailsByLoginId() ::" + e);
			e.printStackTrace();
		}
		return lcoList;
	}

	@RequestMapping(value = "/checkExitCustomerCSS", method = RequestMethod.GET)
	@ResponseBody
	public Customer getExitCustomer(@RequestParam(value = "aadharNo", required = false) String aadharNo) {

		Customer customer = null;
		try {
			logger.info("ComsRestController :: checkExistCustomer() :: START");
			customer = aw.getCustomerService().findByCustomerCodeCSS(aadharNo);
			if (customer != null) {
				customer.setDob(DateUtill.dateToString(customer.getDateofinc()));
				logger.info("ComsRestController :: checkExistCustomer() :: END");
			}
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: checkExistCustomer" + e);
			e.printStackTrace();
		} finally {

		}
		return customer;

	}

	@RequestMapping(value = "/getLmoDtls", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO getLmoDtls(@RequestBody AadharHelperDTO aadharDto) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getLmoDtls() :: START");

			dto = aw.getCustomerService().getLmoDetails(aadharDto);

		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getLmoDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	@RequestMapping(value = "/findListedCafs", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO findListedCafs(@RequestBody AadharHelperDTO aadharDto) {
		
		//AadharHelperDTO dto = new AadharHelperDTO();
		List<Long> cafLists = new ArrayList<Long>();
		
		
		try{
			List<AadharDtlsVO> adharDtlsVO=aadharDto.getAadharDtlsVO();
			HashMap<Long,AadharDtlsVO> adharDtlsMap =new HashMap<Long,AadharDtlsVO>();
			
			for(AadharDtlsVO adharDtl : adharDtlsVO){
				
				Long cafNo=new Long(adharDtl.getCafno());
				cafLists.add(cafNo);
				adharDtlsMap.put(cafNo, adharDtl);
			}
			int cafActiveCount=0;
			int totalCAFActive=0;
			
			List<Caf> cafsList=aw.getCafService().findListedCafs(cafLists);
			for(Object cafObj : cafsList){
				Caf caf=(Caf)cafObj;
				AadharDtlsVO AadharDtlsVO1=adharDtlsMap.get(caf.getCafNo());
				boolean cafActive=reArrargeAadharDtlsVO(AadharDtlsVO1,caf);
				if(cafActive){
					cafActiveCount=cafActiveCount+1;
				}
				totalCAFActive=totalCAFActive+1;
			}
			aadharDto.setCafActiveCount(cafActiveCount);
			aadharDto.setCafTotalCount(totalCAFActive);
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		return aadharDto;
	}
	
	private boolean reArrargeAadharDtlsVO(AadharDtlsVO adharVO,Caf caf){
		
		boolean cafActive=false;
		adharVO.setAddress(caf.getCpePlace());
		Calendar calendar=caf.getCafDate();
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);
		mMonth=mMonth+1;
		
		
		adharVO.setDate(mDay+"/"+mMonth+"/"+mYear);
		String status="";
		if(caf.getStatus() == 6){
			status="Active";
			cafActive=true;
		}else if(caf.getStatus() == 8){
			status="DeActive";
		}else if(caf.getStatus() == 99){
			status="Black Listed";
		}else if(caf.getStatus() == 7){
			status="Suspend";
		}
		
		adharVO.setStatus(status);
		return cafActive;
	}

	
	@RequestMapping(value = "/getHsiUsageDtls", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO getHsiUsageDtls(@RequestBody AadharHelperDTO aadharDto) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getHsiUsageDtls() :: START");

			dto = aw.getCustomerService().getHsiUsageDtls(aadharDto);

		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getHsiUsageDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}
	
	
	@RequestMapping(value = "/getHsiUsageDtlsBasedOnCaf", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO getHsiUsageDtlsBasedonCaf(@RequestBody AadharHelperDTO aadharDto) {

		AadharHelperDTO dto = null;
		try {
			logger.info("ComsRestController :: getHsiUsageDtls() :: START");

			dto = aw.getCustomerService().getHsiUsageDtlsBasedOnCaf(aadharDto);

		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getHsiUsageDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	@RequestMapping(value = "/getPaymentDtls", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO getPaymentDtls(@RequestBody AadharHelperDTO aadharDto) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getPaymentDtls() :: START");

			dto = aw.getCustomerService().getPmntDtls(aadharDto);

		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getPaymentDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	@RequestMapping(value = "/getTicketDtls", method = RequestMethod.POST)
	@ResponseBody
	public AadharHelperDTO getTicketDtls(@RequestBody AadharHelperDTO aadharDto) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getTicketDtls() :: START");

			dto = aw.getCustomerService().getTTDtls(aadharDto);

		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getTicketDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	/**
	 * 
	 * @author vinay
	 * @param cutomerid,year,month
	 */
	@RequestMapping(value = "/getEnterpriseHsiUsageDtls", method = RequestMethod.GET)
	@ResponseBody
	public AadharHelperDTO getEnterpriseHsiUsageDtls(@RequestParam("custid") String custid,
			@RequestParam("year") Integer year, @RequestParam("month") Integer month) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getEnterpriseHsiUsageDtls() :: START");

			dto = aw.getCustomerService().getEnterpriseHsiUsageDtls(custid, year, month);
			logger.info("ComsRestController :: getEnterpriseHsiUsageDtls() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getEnterpriseHsiUsageDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	@RequestMapping(value = "/getEnterprisePhoneUsageDtls", method = RequestMethod.GET)
	@ResponseBody
	public AadharHelperDTO getEnterprisePhoneUsageDtls(@RequestParam("custid") String custid,
			@RequestParam("year") Integer year, @RequestParam("month") Integer month) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getEnterprisePhoneUsageDtls() :: START");

			dto = aw.getCustomerService().getEnterprisePhoneUsageDtls(custid, year, month);
			logger.info("ComsRestController :: getEnterprisePhoneUsageDtls() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getEnterprisePhoneUsageDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}
	
	@RequestMapping(value = "/getPhoneUsageDtls", method = RequestMethod.GET)
	@ResponseBody
	public AadharHelperDTO getPhoneUsageDtls(@RequestParam("cafno") String cafno,
			@RequestParam("year") Integer year, @RequestParam("month") Integer month) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("fetching phone usage for cafno:"+cafno);

			dto = aw.getCustomerService().getPhoneUsageDtls(cafno, year, month);
			logger.info("fetching phone usage for cafno "+cafno+" completed");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getEnterprisePhoneUsageDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

	@RequestMapping(value = "/getEnterpriseCafDtls", method = RequestMethod.GET)
	@ResponseBody
	public AadharHelperDTO getEnterpriseCafDtls(@RequestParam("custid") String custid) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getEnterpriseCafDtls() :: START");

			dto = aw.getCustomerService().getEnterpriseCafDtls(custid);
			logger.info("ComsRestController :: getEnterpriseCafDtls() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getEnterpriseCafDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}
	
	
	@RequestMapping(value = "/getEnterpriseCutomerDtls", method = RequestMethod.GET)
	@ResponseBody
	public AadharHelperDTO getEnterpriseCutomerDtls(@RequestParam("custid") String custid) {

		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			logger.info("ComsRestController :: getEnterpriseCutomerDtls() :: START");

			dto = aw.getCustomerService().getEnterpriseCutomerDtls(custid);
			logger.info("ComsRestController :: getEnterpriseCutomerDtls() :: END");
		} catch (Exception e) {
			logger.error("The Exception is ComsRestController :: getEnterpriseCutomerDtls" + e);
			e.printStackTrace();
		} finally {

		}
		return dto;

	}

}
