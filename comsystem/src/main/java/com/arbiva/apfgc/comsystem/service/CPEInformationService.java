package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.BO.CpeChargeDetailsBO;
import com.arbiva.apfgc.comsystem.BO.SubstationsBO;
import com.arbiva.apfgc.comsystem.BO.TenantsBO;
import com.arbiva.apfgc.comsystem.BO.VillagesBO;
import com.arbiva.apfgc.comsystem.model.CpeModal;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.FeatureParams;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.model.OLT;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;
import com.arbiva.apfgc.comsystem.model.SrvcFeatures;
import com.arbiva.apfgc.comsystem.model.Substations;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.Villages;
import com.arbiva.apfgc.comsystem.vo.CpeInformationVO;
import com.arbiva.apfgc.comsystem.vo.CpeStockVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafModifyVo;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.SplitterVO;
import com.arbiva.apfgc.comsystem.vo.TenantBusinessAreas;

public interface CPEInformationService {

	public List<Districts> getAllDistricts();
	
	public List<Mandals> getAllMandals();
	
	public List<Villages> getAllVillages(String subStationCodes);
	
	public List<OLT> getAllOLTs();
	
	public List<Substations> getAllSubStations(String subStationCodes);
	
	public String getCpeModelByProfileId(Integer profileId);
	
	public List<CpeModal> getAllCpeModals();
	
	public List<CpeModal> getAllCpeModalsByCpeType(String cpeType);
	
	public List<Mandals> getMandalsByDistrictId(Integer districtId);
	
	public List<Villages> getVillagesByDistrictAndMandal(Integer districtId, Integer mandalId);
	
	public List<Substations> getSubstationsByDistrictIdAndMandalId(Integer districtId, Integer mandalId);
	
	public List<OLT> getOLTSLNOBySubstationsrlno(String subStnSrlNo);
	
	public List<OLTPortDetails> getOLTPopIdByOltSrlNo(String oltSrlNo, String lmoCode);
	
	public List<OLT> getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(Integer districtId, Integer mandalId, Integer subStnSlno);

	public abstract Cpecharges getAlCpeChargesByCpeModel(String serialNumber);
	
	public abstract CpeInformationVO getCpeChargesInformation(CustomerCafVO customerCafVO);

	public abstract List<Cpecharges> getAlCpeCharges(String region);

	public Villages getVillagesByVillageId(Integer villageSlno, Integer mandalSlno, Integer districtUid);
	
	public abstract List<FeatureParams> getAllFeatureParams();
	
	public abstract List<SrvcFeatures> getAllSrvcFeatures();

	public Mandals getMandalsByDistrictIdAndMandalSrlNo(Integer districtId, Integer mandalSrlNo);

	public CpeChargeDetailsBO getCpeAndIptvbosSrlNoCheck(String cpeId, String lmoCode, String lmoAgrmntMspCodes, String cpetypelov);
	
	public abstract List<CpeStockVO> getAllCpeStockByLmoCode(String lmoCode);
	
	public abstract CustomerCafModifyVo getCafDetailsForEdit(Long cafNo, String status);

	public List<CpeStockVO> getAllCpeStockByMspCode(String mspCode);

	public Districts getDistrictByDistrictId(Integer districtId);

	public List<Villages> getAllVillagesByTenantCode(String tenantCode);

	public List<TenantBusinessAreas> getAllVillagesBySubstnId(String subStnSlno, String tenantCode);

	public SplitterVO getOLTPortSplitterData(String oltSrlNo, String lmoCode, Integer oltPort, String l1slot);

	public boolean check_SubstUid_OltSrNo_VPNSrNm(String substnUid, String oltSerialNo, String vpnsrvcName);

	public List<VPNSrvcNames> getVPNServicesByPopIdAndOltSrlNo(String oltSrlNo, String subStnId);

	public List<OLT> getOLTSLNOAndSubstationsrlno(String substnUid, String oltSerialNo);

	public List<VPNSrvcNames> getLMOVPNSrvcNamesList(String subStnId);

	public List<Districts> getSILMODistricts(String tenantcode);

	public List<Mandals> getSILMOMandals(String tenantcode, String district);

	public Cpecharges getCpeEmiCountByCpeModel(Long profileId);
		
	public List<VillagesBO> getAllVillagesByDistrictId(String districtId);

	public List<SubstationsBO> getAllSubstationsByDistrictId(String districtId);

	public List<OLTPortDetails> getAllOLTPortDetailsByTenantCode(String tenantCode);

	public List<VillagesBO> getAllVillages();

	public List<TenantsBO> getAllLMOByDistrictAndMandalAndVillage(String district, String mandal, String village);

}
