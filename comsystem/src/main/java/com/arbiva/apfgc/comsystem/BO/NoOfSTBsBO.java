package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author kiran
 *
 */
@Entity
public class NoOfSTBsBO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="stbcafno")
	private String stbCafNo;
	
	@Column(name="stbslno")
	private String stbSlNo;

	@Column(name="stbmacaddr")
	private String stbMacAddr;

	public String getStbCafNo() {
		return stbCafNo;
	}

	public void setStbCafNo(String stbCafNo) {
		this.stbCafNo = stbCafNo;
	}

	public String getStbSlNo() {
		return stbSlNo;
	}

	public void setStbSlNo(String stbSlNo) {
		this.stbSlNo = stbSlNo;
	}

	public String getStbMacAddr() {
		return stbMacAddr;
	}

	public void setStbMacAddr(String stbMacAddr) {
		this.stbMacAddr = stbMacAddr;
	}
	
	
	
}
