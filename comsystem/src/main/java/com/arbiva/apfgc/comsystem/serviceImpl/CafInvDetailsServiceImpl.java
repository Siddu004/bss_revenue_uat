/**
 * 
 *//*
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafInvDetailsDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.CafInvDetails;
import com.arbiva.apfgc.comsystem.service.CafInvDetailsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.ChargeVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;

*//**
 * @author Lakshman
 *
 *//*
@Service
public class CafInvDetailsServiceImpl implements CafInvDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(CafInvDetailsServiceImpl.class);
	
	@Autowired
	CafInvDetailsDao cafInvDetailsDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CafService cafService;
	
	@Override
	public List<CafInvDetails> findAllCafInvDetails() {
		return cafInvDetailsDao.findAllCafInvDetails();
	}

	@Override
	@Transactional
	public void saveCafInvDetails(CustomerCafVO customerCafVO, String loginID, String tenantCode) {
		try {
			CafInvDetails cafInvDetails = new CafInvDetails();
			List<ProductsVO> selectedProdList = new ArrayList<ProductsVO>();
			if(customerCafVO.getIpAddress() != null) {
				selectedProdList = customerCafVO.getProducts();
			} else {
				String [] agruniqueids = customerCafVO.getAgruniqueid().split(",");
				String [] prodCodes = customerCafVO.getProdCode().split(",");
				String [] tenantCodes = customerCafVO.getTenantCode().split(",");
				HttpSession session = httpServletRequest.getSession(false);
				@SuppressWarnings("unchecked")
				List<ProductsVO> productsList = (List<ProductsVO>) session.getAttribute("productList");
				for(ProductsVO product : productsList) {
					for(int i=0; i<agruniqueids.length; i++)
						if(product.getAgruniqueid().equals(Long.parseLong(agruniqueids[i])) && product.getProdcode().equalsIgnoreCase(prodCodes[i]) && product.getTenantcode().equalsIgnoreCase(tenantCodes[i])) {
						selectedProdList.add(product);
					}
				}
			}
			
			for(ProductsVO products : selectedProdList) {
				List<AddtionalServicesVO> serviceList = products.getServicesList();
				for(AddtionalServicesVO services : serviceList) {
					List<ChargeVO> chargeList = services.getChargeList();
					for(ChargeVO charges : chargeList) {
						cafInvDetails.setAcctCafno(customerCafVO.getCafNo());
						cafInvDetails.setInvDtlId(storedProcedureDAO.executeStoredProcedure("INVDTLID"));
						cafInvDetails.setChargedDate(Calendar.getInstance().getTime());
						cafInvDetails.setInvNo(Long.valueOf(0));
						cafInvDetails.setTenantCode(tenantCode);
						cafInvDetails.setProdCode(products.getProdcode());
						cafInvDetails.setSrvcCode(services.getServiceCode());
						cafInvDetails.setProdCafNo(customerCafVO.getCafNo());
						cafInvDetails.setChargeCode(charges.getChargeCode());
						cafInvDetails.setChargeAmount(new BigDecimal(charges.getChargeAmt()));
						//Object[] tax = cafService.getCafChargeTaxPecentage(charges.getChargeCode(), customerCafVO.getPinCode());
						//Double cpeTaxAmt = ((Double.parseDouble(charges.getChargeAmt().toString())*Double.parseDouble(tax[0].toString()))/100) + Double.parseDouble(tax[1].toString());
						//cafInvDetails.setTaxAmount(BigDecimal.valueOf(cpeTaxAmt));
						cafInvDetails.setTaxDtl("");
						cafInvDetails.setStatus(ComsEnumCodes.PENDING_FOR_PAYMENT_STATUS.getStatus());
						cafInvDetails.setCreatedBy(loginID);
						cafInvDetails.setCreatedOn(Calendar.getInstance());
						if(customerCafVO.getIpAddress() != null) {
							cafInvDetails.setCreatedIPAddr(customerCafVO.getIpAddress());
						} else {
							cafInvDetails.setCreatedIPAddr(httpServletRequest.getRemoteAddr());
						}
						cafInvDetails.setModifiedOn(Calendar.getInstance());
						cafInvDetailsDao.saveCafInvDetails(cafInvDetails);
					}
				}
			}
		} catch(Exception e) {
			logger.error("CafInvDetailsServiceImpl::saveCafInvDetails() " + e);
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public void updateCafInvDetails(PaymentVO paymentVO, String loginID) {
		try {
			cafInvDetailsDao.updateCafInvDetails(paymentVO, loginID);
		} catch (Exception e) {
			logger.error("CafInvDetailsServiceImpl::updateCafInvDetails() " + e);
			e.printStackTrace();
		}
	}

}
*/