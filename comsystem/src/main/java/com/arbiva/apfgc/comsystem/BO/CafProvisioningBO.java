package com.arbiva.apfgc.comsystem.BO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CafProvisioningBO {
	
	@Id
	@Column(name= "cafno")
	private Long parentCafNo;
	
	@Column(name= "lmocode")
	private String lmoCode;
	
	@Column(name= "pmntcustid")
	private Long pmtCustId;
	
	@Column(name= "nwsubscode")
	private String nwSubscriberCode;
	
/*	private Long prodCafNo;  
	
	private String distId;
	
	private String mandId;
	
	private String villId;
	
	private String packageCode;*/
	
	

	public Long getParentCafNo() {
		return parentCafNo;
	}

	public void setParentCafNo(Long parentCafNo) {
		this.parentCafNo = parentCafNo;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Long getPmtCustId() {
		return pmtCustId;
	}

	public void setPmtCustId(Long pmtCustId) {
		this.pmtCustId = pmtCustId;
	}

	public String getNwSubscriberCode() {
		return nwSubscriberCode;
	}

	public void setNwSubscriberCode(String nwSubscriberCode) {
		this.nwSubscriberCode = nwSubscriberCode;
	}

	
}
