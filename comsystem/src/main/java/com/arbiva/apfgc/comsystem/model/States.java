/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Arbiva
 *
 */
@Entity
@Table(name="states")
public class States {
	
	@Id
	@Column(name="stateid")
	private Integer stateId;
	
	@Column(name="statename")
	private String stateName;

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
