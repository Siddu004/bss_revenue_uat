/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdaywiseDTO;
import com.arbiva.apfgc.comsystem.BO.CafCountDetailsdistrictwiseDTO;
import com.arbiva.apfgc.comsystem.BO.CountdetailsCustomerwiseDTO;
import com.arbiva.apfgc.comsystem.BO.Details;
import com.arbiva.apfgc.comsystem.dao.impl.CafAccountDao;
import com.arbiva.apfgc.comsystem.model.CafAccount;
import com.arbiva.apfgc.comsystem.service.CafAccountService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafAccountServiceImpl implements CafAccountService {

	private static final Logger logger = Logger.getLogger(CafAccountServiceImpl.class);

	@Autowired
	CafAccountDao cafAccountDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Override
	public List<CafAccount> findAllCafAccounts() {
		List<CafAccount> cafAccountList = new ArrayList<>();
		try {
			cafAccountList = cafAccountDao.findAllCafAccounts();
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl :: findAllCafAccounts()" + e);
			e.printStackTrace();
		} finally {

		}
		return cafAccountList;
	}

	@Override
	@Transactional
	public void saveCafAccount(CustomerCafVO customerCafVO, String loginID) {
		CafAccount cafAccount = new CafAccount();
		try {
			cafAccount.setAcctcafno(customerCafVO.getCafNo());
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				cafAccount.setCustId(customerCafVO.getCustId().longValue());
			} else {
				cafAccount.setCustId(Long.parseLong(customerCafVO.getCustCode()));
			}
			cafAccount.setRegBalence(BigDecimal.valueOf(0.00));
			cafAccount.setDepBalence(BigDecimal.valueOf(0.00));
			cafAccount.setUnbBalence(BigDecimal.valueOf(0.00));
			cafAccount.setOpeninvAmt(BigDecimal.valueOf(0.00));
			cafAccount.setChargedBalence(BigDecimal.valueOf(0.00));
			cafAccount.setDistrictuid(Integer.parseInt(customerCafVO.getDistrict()));
			cafAccount.setCreatedBy(loginID);
			cafAccount.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			cafAccount.setCreatedOn(Calendar.getInstance());
			cafAccount.setModifiedOn(Calendar.getInstance());
			cafAccount.setNextDtlrecid(Long.valueOf(1));
			cafAccount.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafAccount.setBillfreqlov(customerCafVO.getBillCycle());
			cafAccountDao.saveCafAccount(cafAccount);
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl::saveCafAccount() " + e);
			e.printStackTrace();
		} finally {
			cafAccount = null;
		}
	}

	@Override
	@Transactional
	public void updateCafAccount(PaymentVO paymentVO, String loginID) {
		CafAccount cafAccount = new CafAccount();
		try {
			cafAccount = findByCafNo(paymentVO.getCafNo(), Integer.parseInt(paymentVO.getDistrict()));
			cafAccount.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			cafAccount.setModifiedBy(loginID);
			cafAccount.setModifiedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			cafAccount.setModifiedOn(Calendar.getInstance());
			cafAccountDao.saveCafAccount(cafAccount);
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl::updateCafAccount() " + e);
			e.printStackTrace();
		} finally {
			cafAccount = null;
		}
	}

	@Override
	public CafAccount findByCafNo(Long cafNo, Integer district) {
		CafAccount cafAccount = new CafAccount();
		try {
			cafAccount = cafAccountDao.findByCafNo(cafNo, district);
		} catch (Exception e) {
			logger.error("CafAccountServiceImpl::findByCafNo() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafAccount;
	}

	@Override
	public List<CafCountDetailsdistrictwiseDTO> getCountdetailsdistrictwise() {
		// TODO Auto-generated method stub
		return cafAccountDao.getCountdetailsdistrictwise();
	}

	@Override
	public List<CountdetailsCustomerwiseDTO> getCountdetailsCustomerwise() {
		List<CountdetailsCustomerwiseDTO> list;
		list =cafAccountDao.getCountdetailsCustomerwise();
		return list;
	}
	
	@Override
	public CafCountDetailsdaywiseDTO getCafCountDetails(String custtype,String subtype) {
		// TODO Auto-generated method stub
		Object[] countObject = (Object[]) cafAccountDao.getCafCountDetails(custtype,subtype);

		CafCountDetailsdaywiseDTO cafCount = new CafCountDetailsdaywiseDTO();
		if(countObject!=null && countObject.length == 6) {
			cafCount.setToday(((BigDecimal)countObject[0]).longValue());
			cafCount.setYesterday(((BigDecimal)countObject[1]).longValue());
			cafCount.setLast_seven_days(((BigDecimal)countObject[2]).longValue());
			cafCount.setLast_one_month(((BigDecimal)countObject[3]).longValue());
			cafCount.setLast_three_months(((BigDecimal)countObject[4]).longValue());
			cafCount.setLast_six_months(((BigDecimal)countObject[5]).longValue());
		}
		return cafCount;
	}

	@Override
	public Object[] getLastTenDaysCountDetails() {
		// TODO Auto-generated method stub
		Object[] countObject = (Object[]) cafAccountDao.getLastTenDaysCountDetails();
		return countObject;
	}
	
	@Override
	public List<Details> getDistrictINDENTDurationDetails(String custtypelov,String enttypelov) {
		// TODO Auto-generated method stub
		List<Details> countObject = cafAccountDao.getDistrictINDENTDurationDetails(custtypelov,enttypelov);
		return countObject;
	}

}
