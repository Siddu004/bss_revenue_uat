/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;

/**
 * @author Lakshman
 *
 */
@Repository
public class CafProductsDao {
	
	private static final Logger LOGGER = Logger.getLogger(CustomerDao.class);
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCafProducts(CafProducts cafProducts) {
		getEntityManager().remove(cafProducts);
		getEntityManager().flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<CafProducts> findAllCafProducts() {
		return (List<CafProducts>)getEntityManager().createQuery("Select cafProducts from " + CafProducts.class.getSimpleName() + " cafProducts").getResultList();
	}
	
	public void saveCafProducts(CafProducts cafProducts) {
		try{
		getEntityManager().merge(cafProducts);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::saveCafProducts() " + e);
		}
	}
	
	public void createCafProducts(CafProducts cafProducts) {
		getEntityManager().merge(cafProducts);
	}

	public CafProducts findCafProductByCafNoAndMspCodeAndProdCode(Long cafNo, String mspCode, String prodcode) {
		CafProducts cafProducts = null;
		TypedQuery<CafProducts> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(CafProducts.class.getSimpleName()).append(" WHERE cafno=:cafNo and mspcode=:mspCode and prodcode=:prodcode");
			try {
				LOGGER.info("START::findCafProductByCafNoAndMspCodeAndProdCode()");
				query = getEntityManager().createQuery(builder.toString(), CafProducts.class);
				query.setParameter("cafNo", cafNo);
				query.setParameter("mspCode", mspCode);
				query.setParameter("prodcode", prodcode);
				cafProducts = query.getSingleResult();
				LOGGER.info("END::findCafProductByCafNoAndMspCodeAndProdCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findCafProductByCafNoAndMspCodeAndProdCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		return cafProducts;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafProducts(Long cafNo) {
		List<Object[]> CafProductsList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder(" select p.prodcode, p.prodname, p.prodtype from cafprods cp, products p where p.prodcode = cp.prodcode and cp.cafno = "+cafNo+" "); 
				builder.append(" and current_date() between p.effectivefrom and p.effectiveto ");
			try {
				LOGGER.info("START::findCafDaoByCustomerCode()");
				query = getEntityManager().createNativeQuery(builder.toString());
				CafProductsList = query.getResultList();
				LOGGER.info("END::findCafDaoByCustomerCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findCafDaoByCustomerId() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return CafProductsList;
	}

	public void updateCafProducts(PaymentVO paymentVO, String loginID) {
		Query query = null;
		String ipAddress = null;
		StringBuilder builder = null;
		String modifiedOn = null;
		try {
			modifiedOn = DateUtill.dateToStringdateFormat(Calendar.getInstance().getTime());
			ipAddress = null;
			builder = null;
			if(paymentVO.getIpAddress() != null) {
				ipAddress = paymentVO.getIpAddress();
			} else {
				ipAddress = httpServletRequest.getRemoteAddr();
			}
			if(paymentVO.getFeasibility().equalsIgnoreCase("Y")) {
				builder = new StringBuilder("update cafprods set status = "+ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			} else {
				builder = new StringBuilder("update cafprods set status = "+ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus()+", modifiedby = '"+loginID+"', ");
				builder.append("modifiedon = '"+modifiedOn+"', modifiedipaddr = '"+ipAddress+"' where cafno = "+paymentVO.getCafNo()+" ");
			}

			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();

		} catch(Exception e) {
			LOGGER.error("EXCEPTION::updateCafProducts() " + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
			ipAddress = null;
			modifiedOn = null;
		}
	}
	
	public int checkcaf(Long cafNo) {
		int checkcaf = 0;
		StringBuilder builder = new StringBuilder("SELECT cafNo FROM cafprods where  cafNo =  " + cafNo + "");
		Query query = null;
		try {
			LOGGER.info("START::findCafDaoByCustomerCode()");
			query = getEntityManager().createNativeQuery(builder.toString());
			if (!query.getResultList().isEmpty()) {
				checkcaf++;
			}
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCafDaoByCustomerId() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return checkcaf;
	}
		
	public int deleteAllProdsSrvcsFeature(Long cafNo) {
		int checkservices = 0;
		StringBuilder builder1 = new StringBuilder("delete from cafprods  where  cafNo = "+cafNo+" ");
		StringBuilder builder2 = new StringBuilder("delete from cafsrvcs   where  cafNo = "+cafNo+" ");
		StringBuilder builder3 = new StringBuilder("delete from caffeatureprms  where  cafNo = "+cafNo+" ");
		Query query1 = null;
		Query query2= null;
		Query query3 = null;
		try {
			LOGGER.info("START::checkServices()");
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			query2 = getEntityManager().createNativeQuery(builder2.toString());
			query3 = getEntityManager().createNativeQuery(builder3.toString());
			query3.executeUpdate();
			query2.executeUpdate();
			query1.executeUpdate();
			checkservices++;
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::checkServices() " + e);
		}finally{
			query1 = null;
			query2 = null;
			query3 = null;
			builder1 = null;
			builder2 = null;
			builder3 = null;
		}
		return checkservices;
	}

	@SuppressWarnings("unchecked")
	public List<ProductsVO> getMSOProducts(String mspCode) {
		List<ProductsVO> msoProductList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("START::findCafDaoByCustomerCode()");
			builder.append(" select tenantcode, prodcode, prodtype, durationdays, lockindays, effectivefrom from products where  tenantcode = '"+mspCode+"' and globalflag = 'N' and CURRENT_DATE() BETWEEN effectivefrom AND effectiveto ");
			query = getEntityManager().createNativeQuery(builder.toString());
			List<Object[]> msoObjectProductList = query.getResultList();
			if (!msoObjectProductList.isEmpty()) {
				for (Object[] object : msoObjectProductList) {
					ProductsVO productsVO = new ProductsVO();
					productsVO.setTenantcode(object[0] != null ? object[0].toString() : "");
					productsVO.setMspCode(object[0] != null ? object[0].toString() : "");
					productsVO.setProdcode(object[1] != null ? object[1].toString() : "");
					productsVO.setProdType(object[2] != null ? object[2].toString() : "");
					productsVO.setOneTimeProdDuration(object[3] != null ? Integer.parseInt(object[3].toString()) : 0);
					productsVO.setLockInPeriod(object[4] != null ? Integer.parseInt(object[4].toString()) : 0);
					productsVO.setEffectivefrom(object[5] != null ? object[5].toString() : "");
					productsVO.setAgruniqueid(0l);
					msoProductList.add(productsVO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCafDaoByCustomerId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return msoProductList;
	}

	@SuppressWarnings("unchecked")
	public List<AddtionalServicesVO> getMSOServicesList(String mspCode, String prodcode, String effectivefrom) {
		List<AddtionalServicesVO> msoServicesList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("START::findCafDaoByCustomerCode()");
			builder.append(" select componentcode, lockindays from prodcomponents where tenantcode = '"+mspCode+"' and prodcode = '"+prodcode+"' and '"+effectivefrom+"' = effectivefrom ");
			query = getEntityManager().createNativeQuery(builder.toString());
			List<Object[]> msoObjectServicesList = query.getResultList();
			if (!msoObjectServicesList.isEmpty()) {
				for (Object[] object : msoObjectServicesList) {
					AddtionalServicesVO addtionalServicesVO = new AddtionalServicesVO();
					addtionalServicesVO.setServiceCode(object[0] != null ? object[0].toString() : "");
					addtionalServicesVO.setLockInPeriod(object[1] != null ? Integer.parseInt(object[1].toString()) : 0);
					msoServicesList.add(addtionalServicesVO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCafDaoByCustomerId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return msoServicesList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCafServicesListByProdcode(String pkgCodes) {
		List<Object[]> siProductList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("START::findCafDaoByCustomerCode()");
			builder.append(" select tenantcode, prodcode, prodtype, durationdays from products where CURRENT_DATE() BETWEEN effectivefrom AND effectiveto "); 
			builder.append(" and prodcode in("+pkgCodes+") ");
			query = getEntityManager().createNativeQuery(builder.toString());
			siProductList = query.getResultList();
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findCafDaoByCustomerId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return siProductList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSIServicesListByProdcode(String prodcode) {
		List<Object[]> siServicesList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("START::getSIServicesListByProdcode()");
			builder.append(" select pc.componentcode, pc.lockindays, pc.coresrvccode, s.featurecodes from prodcomponents pc, srvcs s where pc.componentcode = s.srvccode "); 
			builder.append(" and CURRENT_DATE() BETWEEN s.effectivefrom AND s.effectiveto and pc.prodcode = '"+prodcode+"' ");
			query = getEntityManager().createNativeQuery(builder.toString());
			siServicesList = query.getResultList();
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getSIServicesListByProdcode() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return siServicesList;
	}

	public Object[] getFilterPackageDetails(Long cafNumber) {
		Object[] filterPkgDtls = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			LOGGER.info("START::getSIServicesListByProdcode()");
			builder.append(" select group_concat(prodcode) prodcode, group_concat(tenantcode) tenantcode, group_concat(rsagruid) agrmntId from cafprods where parentcafno = "+cafNumber+" "); 
			query = getEntityManager().createNativeQuery(builder.toString());
			filterPkgDtls = (Object[]) query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getSIServicesListByProdcode() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return filterPkgDtls;
	}
}
