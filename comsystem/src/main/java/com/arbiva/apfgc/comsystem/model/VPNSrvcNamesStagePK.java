/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class VPNSrvcNamesStagePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long uploadId;
	
	private Long uploadRecNo;

	public Long getUploadId() {
		return uploadId;
	}

	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}

	public Long getUploadRecNo() {
		return uploadRecNo;
	}

	public void setUploadRecNo(Long uploadRecNo) {
		this.uploadRecNo = uploadRecNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uploadId == null) ? 0 : uploadId.hashCode());
		result = prime * result + ((uploadRecNo == null) ? 0 : uploadRecNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VPNSrvcNamesStagePK other = (VPNSrvcNamesStagePK) obj;
		if (uploadId == null) {
			if (other.uploadId != null)
				return false;
		} else if (!uploadId.equals(other.uploadId))
			return false;
		if (uploadRecNo == null) {
			if (other.uploadRecNo != null)
				return false;
		} else if (!uploadRecNo.equals(other.uploadRecNo))
			return false;
		return true;
	}
	
}
