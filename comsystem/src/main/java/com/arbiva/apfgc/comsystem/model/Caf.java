package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;

/**
 * 
 * @author Laxman
 * 
 */
@Entity
@Table(name = "cafs")
public class Caf extends Base {

	public Caf() {

	}

	public Caf(CustomerCafVO customerCafVO) {
		if (customerCafVO.getBillCycle() != null) {
			this.billfreqLov = customerCafVO.getBillCycle();
		}
		this.cpePlace = customerCafVO.getLocation();
		this.aadharNo = customerCafVO.getAadharNumber();
		this.custType = customerCafVO.getCustType();
		if (!customerCafVO.getLatitude().isEmpty()) {
			this.lattitude = Float.parseFloat(customerCafVO.getLatitude());
		}
		if (!customerCafVO.getLongitude().isEmpty()) {
			this.longitude = Float.parseFloat(customerCafVO.getLongitude());
		}
		this.lmoCode = customerCafVO.getLmoCode();
		this.popSubstnno = customerCafVO.getPopId();
		if (customerCafVO.getPopDistrict() != null) {
			if (!customerCafVO.getPopDistrict().isEmpty())
				this.popDistrict = Integer.parseInt(customerCafVO.getPopDistrict());
		}
		if (customerCafVO.getPopMandal() != null) {
			if (!customerCafVO.getPopMandal().isEmpty())
				this.popMandal = Integer.parseInt(customerCafVO.getPopMandal());
		}
		this.instAddress1 = customerCafVO.getAddress1();
		this.instAddress2 = customerCafVO.getAddress2();
		this.instDistrict = customerCafVO.getDistrict();
		this.instMandal = customerCafVO.getMandal();
		this.instCityVillage = customerCafVO.getCity().split(",")[0];
		this.instLocality = customerCafVO.getLocality();
		this.instPin = customerCafVO.getPinCode();
		this.contactEmail = customerCafVO.getEmailId();
		this.contactmobileNo = customerCafVO.getMobileNo();
		this.contactPerson = customerCafVO.getPocName();
	}

	public Caf(CustomerCafVO customerCafVO, EntCafStage entCafStage) {
		this.aadharNo = customerCafVO.getAadharNumber();
		this.custType = ComsEnumCodes.ENTCUST_TYPE_CODE.getCode();
		this.billfreqLov = customerCafVO.getBillCycle();
		this.cafDate = Calendar.getInstance();
		this.contactEmail = entCafStage.getContactEmail();
		this.contactmobileNo = entCafStage.getContactMobileNo();
		this.contactPerson = entCafStage.getContactName();
		this.createdBy = customerCafVO.getLoginId();
		this.createdOn = Calendar.getInstance();
		this.createdIPAddr = customerCafVO.getIpAddress();
		this.instAddress1 = entCafStage.getDistrict() + "," + entCafStage.getMandal();
		this.cpePlace = entCafStage.getLocation();
		this.instLocality = entCafStage.getMandal();
		this.status = ComsEnumCodes.Caf_BulkUpload_status.getStatus();
		this.modifiedOn = Calendar.getInstance();
		this.lmoCode = entCafStage.getLmocode();
		this.oltCardno = 1;
		this.cpeLeaseyn = 'N';
		this.lmoDeclaration = 'N';
		this.customerDeclaration = 'N';
		this.instState = "Andhra Pradesh";
		this.instPin = "";
		this.apsflUniqueId = entCafStage.getApsflUniqueId();
		if (entCafStage.getLongitude() != null) {
			if (!entCafStage.getLongitude().isEmpty())
				this.longitude = Float.parseFloat(entCafStage.getLongitude());
		}
		if (entCafStage.getLattitude() != null) {
			if (!entCafStage.getLattitude().isEmpty())
				this.lattitude = Float.parseFloat(entCafStage.getLattitude());
		}
	}

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CAFNO")
	private Long cafNo;

	@Column(name = "LMOCODE")
	private String lmoCode;

	@Column(name = "custtypelov")
	private String custType;

	@Column(name = "CUSTIDNO")
	private String custIdNo; // Aadhar no for individual customers and PAN+TAN
								// for enterprise customers

	@Column(name = "CUSTID")
	private Long custId;

	@Column(name = "CUSTCODE")
	private String custCode;

	@Column(name = "PMNTCUSTCODE")
	private String pmntCustCode;

	@Column(name = "PMNTCUSTID")
	private Long pmntCustId;

	@Column(name = "AADHARNO")
	private String aadharNo;

	@Column(name = "PANNO")
	private String panNo;

	@Column(name = "TANNO")
	private String tanNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CAFDATE", nullable = false)
	private Calendar cafDate;

	@Column(name = "LONGITUDE")
	private float longitude;

	@Column(name = "LATTITUDE")
	private float lattitude;

	@Column(name = "altitude")
	private float altitude;

	@Column(name = "cpeplace")
	private String cpePlace;

	@Column(name = "cpeslno")
	private String cpeslNo;

	@Column(name = "cpemacaddr")
	private String cpeMacAddr;

	@Column(name = "cpeleaseyn")
	private Character cpeLeaseyn;

	@Column(name = "cpetenantcode")
	private String cpeTenantcode;

	@Column(name = "cpelockedmons")
	private int cpeLockedmons;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cpelockeddate")
	private Date cpeLockeddate;

	@Column(name = "billfreqlov")
	private String billfreqLov;

	@Column(name = "cust_decl")
	private Character customerDeclaration;

	@Column(name = "lmo_decl")
	private Character lmoDeclaration;

	@Column(name = "DEACTIVATEDBY")
	private String deActivatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DEACTIVATEDON")
	private Date deActivatedOn;

	@Column(name = "DEACTIVATEDIPADDR")
	private String deActivatedIpAddr;

	@Column(name = "pop_district")
	private Integer popDistrict;

	@Column(name = "pop_mandal")
	private Integer popMandal;

	@Column(name = "olt_id")
	private String oltId;

	@Column(name = "oltipaddr")
	private String oltIpaddr;

	@Column(name = "olt_portid")
	private Integer oltPortid;

	@Column(name = "cpeprofileid")
	private Long cpeModel;

	@Column(name = "olt_onuid")
	private Integer oltOnuid;

	@Column(name = "aaacode")
	private String aaaCode;

	@Column(name = "portsplit")
	private String portSplit;

	@Column(name = "onuregnno")
	private String onuRegnNo;

	@Column(name = "agorahsisubscode")
	private String agoraHsiSubsCode;

	@Column(name = "pop_substnno")
	private String popSubstnno;

	@Column(name = "olt_cardno")
	private Integer oltCardno;

	@Column(name = "inst_addr1")
	private String instAddress1;

	@Column(name = "inst_addr2")
	private String instAddress2;

	@Column(name = "inst_locality")
	private String instLocality;

	@Column(name = "inst_area")
	private String instArea;

	@Column(name = "inst_district")
	private String instDistrict;
	
	@Column(name = "custdistuid")
	private Integer custdistUid;

	@Column(name = "inst_mandal")
	private String instMandal;

	@Column(name = "inst_city_village")
	private String instCityVillage;

	@Column(name = "inst_state", nullable = false)
	private String instState;

	@Column(name = "inst_pin", nullable = false)
	private String instPin;

	@Column(name = "inst_taxgradelov")
	private String instTaxgradeLov;

	@Column(name = "contactperson")
	private String contactPerson;

	@Column(name = "contactemail")
	private String contactEmail;

	@Column(name = "contactmob")
	private String contactmobileNo;

	@Column(name = "apsfluniqueid")
	private String apsflUniqueId;

	@Transient
	private String paymentResponsible;

	@Column(name = "actdate")
	private Date actDate;

	@Transient
	private String pocDesignation;

	@ManyToOne
	@JoinColumn(name = "custid", referencedColumnName = "custid", nullable = false, insertable = false, updatable = false)
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "lmocode", referencedColumnName = "tenantcode", nullable = false, insertable = false, updatable = false)
	private Tenant tenantcode;

	@ManyToOne
	@JoinColumn(name = "cpeprofileid", referencedColumnName = "profile_id", nullable = false, insertable = false, updatable = false)
	private CpeModal cpeModal;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "olt_id", referencedColumnName = "pop_olt_serialno", insertable = false, updatable = false),
			@JoinColumn(name = "olt_cardno", referencedColumnName = "cardid", insertable = false, updatable = false),
			@JoinColumn(name = "olt_portid", referencedColumnName = "portno", insertable = false, updatable = false) })
	private OLTPortDetails olPortDetails;

	@Transient
	private String successRecordCount;

	@Transient
	private String failedRecordCount;

	public String getInstDistrict() {
		return instDistrict;
	}

	public void setInstDistrict(String instDistrict) {
		this.instDistrict = instDistrict;
	}
	
	public Integer getCustdistUid() {
		return custdistUid;
	}

	public void setCustdistUid(Integer custdistUid) {
		this.custdistUid = custdistUid;
	}

	public String getInstMandal() {
		return instMandal;
	}

	public void setInstMandal(String instMandal) {
		this.instMandal = instMandal;
	}

	public String getInstAddress1() {
		return instAddress1;
	}

	public void setInstAddress1(String instAddress1) {
		this.instAddress1 = instAddress1;
	}

	public String getInstAddress2() {
		return instAddress2;
	}

	public void setInstAddress2(String instAddress2) {
		this.instAddress2 = instAddress2;
	}

	public String getInstLocality() {
		return instLocality;
	}

	public void setInstLocality(String instLocality) {
		this.instLocality = instLocality;
	}

	public String getInstArea() {
		return instArea;
	}

	public void setInstArea(String instArea) {
		this.instArea = instArea;
	}

	public String getInstCityVillage() {
		return instCityVillage;
	}

	public void setInstCityVillage(String instCityVillage) {
		this.instCityVillage = instCityVillage;
	}

	public String getInstState() {
		return instState;
	}

	public void setInstState(String instState) {
		this.instState = instState;
	}

	public String getInstPin() {
		return instPin;
	}

	public void setInstPin(String instPin) {
		this.instPin = instPin;
	}

	public CpeModal getCpeModal() {
		return cpeModal;
	}

	public void setCpeModal(CpeModal cpeModal) {
		this.cpeModal = cpeModal;
	}

	public String getPopSubstnno() {
		return popSubstnno;
	}

	public void setPopSubstnno(String popSubstnno) {
		this.popSubstnno = popSubstnno;
	}

	public Integer getOltCardno() {
		return oltCardno;
	}

	public void setOltCardno(Integer oltCardno) {
		this.oltCardno = oltCardno;
	}

	public Integer getPopDistrict() {
		return popDistrict;
	}

	public void setPopDistrict(Integer popDistrict) {
		this.popDistrict = popDistrict;
	}

	public Integer getPopMandal() {
		return popMandal;
	}

	public void setPopMandal(Integer popMandal) {
		this.popMandal = popMandal;
	}

	public String getOltId() {
		return oltId;
	}

	public void setOltId(String oltId) {
		this.oltId = oltId;
	}

	public Integer getOltPortid() {
		return oltPortid;
	}

	public void setOltPortid(Integer oltPortid) {
		this.oltPortid = oltPortid;
	}

	public Long getCpeModel() {
		return cpeModel;
	}

	public void setCpeModel(Long cpeModel) {
		this.cpeModel = cpeModel;
	}

	public Integer getOltOnuid() {
		return oltOnuid;
	}

	public void setOltOnuid(Integer oltOnuid) {
		this.oltOnuid = oltOnuid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Tenant getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(Tenant tenantcode) {
		this.tenantcode = tenantcode;
	}

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getCpePlace() {
		return cpePlace;
	}

	public void setCpePlace(String cpePlace) {
		this.cpePlace = cpePlace;
	}

	public String getCpeslNo() {
		return cpeslNo;
	}

	public void setCpeslNo(String cpeslNo) {
		this.cpeslNo = cpeslNo;
	}

	public String getCpeMacAddr() {
		return cpeMacAddr;
	}

	public void setCpeMacAddr(String cpeMacAddr) {
		this.cpeMacAddr = cpeMacAddr;
	}

	public Character getCpeLeaseyn() {
		return cpeLeaseyn;
	}

	public void setCpeLeaseyn(Character cpeLeaseyn) {
		this.cpeLeaseyn = cpeLeaseyn;
	}

	public String getCpeTenantcode() {
		return cpeTenantcode;
	}

	public void setCpeTenantcode(String cpeTenantcode) {
		this.cpeTenantcode = cpeTenantcode;
	}

	public int getCpeLockedmons() {
		return cpeLockedmons;
	}

	public void setCpeLockedmons(int cpeLockedmons) {
		this.cpeLockedmons = cpeLockedmons;
	}

	public Date getCpeLockeddate() {
		return cpeLockeddate;
	}

	public void setCpeLockeddate(Date cpeLockeddate) {
		this.cpeLockeddate = cpeLockeddate;
	}

	public Character getCustomerDeclaration() {
		return customerDeclaration;
	}

	public void setCustomerDeclaration(Character customerDeclaration) {
		this.customerDeclaration = customerDeclaration;
	}

	public Character getLmoDeclaration() {
		return lmoDeclaration;
	}

	public void setLmoDeclaration(Character lmoDeclaration) {
		this.lmoDeclaration = lmoDeclaration;
	}

	public String getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(String deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public Date getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(Date deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public String getDeActivatedIpAddr() {
		return deActivatedIpAddr;
	}

	public void setDeActivatedIpAddr(String deActivatedIpAddr) {
		this.deActivatedIpAddr = deActivatedIpAddr;
	}

	public String getCustIdNo() {
		return custIdNo;
	}

	public void setCustIdNo(String custIdNo) {
		this.custIdNo = custIdNo;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getPmntCustCode() {
		return pmntCustCode;
	}

	public void setPmntCustCode(String pmntCustCode) {
		this.pmntCustCode = pmntCustCode;
	}

	public Long getPmntCustId() {
		return pmntCustId;
	}

	public void setPmntCustId(Long pmntCustId) {
		this.pmntCustId = pmntCustId;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getTanNo() {
		return tanNo;
	}

	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}

	public Calendar getCafDate() {
		return cafDate;
	}

	public void setCafDate(Calendar cafDate) {
		this.cafDate = cafDate;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLattitude() {
		return lattitude;
	}

	public void setLattitude(float lattitude) {
		this.lattitude = lattitude;
	}

	public float getAltitude() {
		return altitude;
	}

	public void setAltitude(float altitude) {
		this.altitude = altitude;
	}

	public String getBillfreqLov() {
		return billfreqLov;
	}

	public void setBillfreqLov(String billfreqLov) {
		this.billfreqLov = billfreqLov;
	}

	public String getOnuRegnNo() {
		return onuRegnNo;
	}

	public void setOnuRegnNo(String onuRegnNo) {
		this.onuRegnNo = onuRegnNo;
	}

	public String getAgoraHsiSubsCode() {
		return agoraHsiSubsCode;
	}

	public void setAgoraHsiSubsCode(String agoraHsiSubsCode) {
		this.agoraHsiSubsCode = agoraHsiSubsCode;
	}

	public String getInstTaxgradeLov() {
		return instTaxgradeLov;
	}

	public void setInstTaxgradeLov(String instTaxgradeLov) {
		this.instTaxgradeLov = instTaxgradeLov;
	}

	public Date getActDate() {
		return actDate;
	}

	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	public OLTPortDetails getOlPortDetails() {
		return olPortDetails;
	}

	public void setOlPortDetails(OLTPortDetails olPortDetails) {
		this.olPortDetails = olPortDetails;
	}

	public String getPortSplit() {
		return portSplit;
	}

	public void setPortSplit(String portSplit) {
		this.portSplit = portSplit;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactmobileNo() {
		return contactmobileNo;
	}

	public void setContactmobileNo(String contactmobileNo) {
		this.contactmobileNo = contactmobileNo;
	}

	public String getPaymentResponsible() {
		return paymentResponsible;
	}

	public void setPaymentResponsible(String paymentResponsible) {
		this.paymentResponsible = paymentResponsible;
	}

	public String getPocDesignation() {
		return pocDesignation;
	}

	public void setPocDesignation(String pocDesignation) {
		this.pocDesignation = pocDesignation;
	}

	public void updateCafDetails(CustomerCafVO customerCafVO) {

	}

	public String getSuccessRecordCount() {
		return successRecordCount;
	}

	public void setSuccessRecordCount(String successRecordCount) {
		this.successRecordCount = successRecordCount;
	}

	public String getFailedRecordCount() {
		return failedRecordCount;
	}

	public void setFailedRecordCount(String failedRecordCount) {
		this.failedRecordCount = failedRecordCount;
	}

	public String getApsflUniqueId() {
		return apsflUniqueId;
	}

	public void setApsflUniqueId(String apsflUniqueId) {
		this.apsflUniqueId = apsflUniqueId;
	}

	public String getOltIpaddr() {
		return oltIpaddr;
	}

	public void setOltIpaddr(String oltIpaddr) {
		this.oltIpaddr = oltIpaddr;
	}

	public String getAaaCode() {
		return aaaCode;
	}

	public void setAaaCode(String aaaCode) {
		this.aaaCode = aaaCode;
	}
}
