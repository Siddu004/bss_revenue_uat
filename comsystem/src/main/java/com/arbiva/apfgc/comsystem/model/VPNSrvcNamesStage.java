/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name = "vpnsrvcnamesstg")
@IdClass(VPNSrvcNamesStagePK.class)
public class VPNSrvcNamesStage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "uploadid")
	private Long uploadId;
	
	@Id
	@Column(name = "uploadrecno")
	private Long uploadRecNo;
	
	@Column(name = "districtuid")
	private Integer districtUid;
	
	@Column(name = "mandalslno")
	private Integer mandalSlno;
	
	@Column(name = "substnuid")
	private String substnUid;
	
	@Column(name = "olt_serialno")
	private String oltSerialNo;
	
	@Column(name = "vpnsrvcname")
	private String vpnsrvcName;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdon")
	private Calendar createdOn;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "remarks")
	private String remarks;
	
	@ManyToOne
	@JoinColumn(name = "uploadid", referencedColumnName = "uploadid", nullable = false, insertable=false, updatable=false)
	@JsonIgnore
	private UploadHistory uploadHistory;

	public Long getUploadId() {
		return uploadId;
	}

	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}

	public Long getUploadRecNo() {
		return uploadRecNo;
	}

	public void setUploadRecNo(Long uploadRecNo) {
		this.uploadRecNo = uploadRecNo;
	}

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public Integer getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(Integer mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public String getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(String substnUid) {
		this.substnUid = substnUid;
	}

	public String getOltSerialNo() {
		return oltSerialNo;
	}

	public void setOltSerialNo(String oltSerialNo) {
		this.oltSerialNo = oltSerialNo;
	}

	public String getVpnsrvcName() {
		return vpnsrvcName;
	}

	public void setVpnsrvcName(String vpnsrvcName) {
		this.vpnsrvcName = vpnsrvcName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public UploadHistory getUploadHistory() {
		return uploadHistory;
	}

	public void setUploadHistory(UploadHistory uploadHistory) {
		this.uploadHistory = uploadHistory;
	}
	
}
