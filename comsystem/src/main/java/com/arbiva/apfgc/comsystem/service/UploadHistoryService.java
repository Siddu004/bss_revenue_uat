/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.UploadHistory;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.VPNServiceExcelUploadVO;

/**
 * @author Lakshman
 *
 */
public interface UploadHistoryService {
	
	public List<EntCafStage> getAllEntCafStageByStatus(Long uploadId);

	public UploadHistory saveUploadHistory(CustomerCafVO customerCafVO);

	public void updateUploadHistory(Long uploadId, Long successRecordCount);

	public void updateEntCafStage(EntCafStage entCafStage, String flag);

	public UploadHistory saveUploadHistory(VPNServiceExcelUploadVO vpnServiceExcelUploadVO);

	public List<VPNSrvcNamesStage> getAllVPNSrvcNamesStageStatus(Long uploadId);

	public void updateVPNSrvcNamesStage(VPNSrvcNamesStage vpnSrvcNamesStage, String status);

}
