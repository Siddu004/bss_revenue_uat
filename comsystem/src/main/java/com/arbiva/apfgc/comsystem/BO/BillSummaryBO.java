/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class BillSummaryBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "onetimecharges")
	private String oneTimeCharges;

	@Column(name = "onetimesrvctax")
	private String onetimesrvcTax;
	
	@Column(name = "onetimeswtax")
	private String onetimeswtax;
	
	@Column(name = "onetimekktax")
	private String onetimekktax;
	
	@Column(name = "onetimeenttax")
	private String onetimeenttax;
	
	@Column(name = "vodcharges")
	private String vodcharges;
	
	@Column(name = "vodsrvctax")
	private String vodsrvctax;
	
	@Column(name = "vodkktax")
	private String vodkktax;
	
	@Column(name = "vodswtax")
	private String vodswtax;
	
	@Column(name = "vodenttax")
	private String vodenttax;
	
	@Column(name = "rentalcharges")
	private String rentalcharges;
	
	@Column(name = "rentalsrvctax")
	private String rentalsrvctax;
	
	@Column(name = "rentalswtax")
	private String rentalswtax;
	
	@Column(name = "rentalskktax")
	private String rentalskktax;
	
	@Column(name = "rentalenttax")
	private String rentalenttax;
	
	@Column(name = "tpusagecharges")
	private String tpusagecharges;
	
	@Column(name = "tpsrvctax")
	private String tpsrvctax;
	
	@Column(name = "tpswtax")
	private String tpswtax;
	
	@Column(name = "tpkktax")
	private String tpkktax;
	
	@Column(name = "tpenttax")
	private String tpenttax;

	public String getOneTimeCharges() {
		return oneTimeCharges;
	}

	public void setOneTimeCharges(String oneTimeCharges) {
		this.oneTimeCharges = oneTimeCharges;
	}

	public String getOnetimesrvcTax() {
		return onetimesrvcTax;
	}

	public void setOnetimesrvcTax(String onetimesrvcTax) {
		this.onetimesrvcTax = onetimesrvcTax;
	}

	public String getOnetimeswtax() {
		return onetimeswtax;
	}

	public void setOnetimeswtax(String onetimeswtax) {
		this.onetimeswtax = onetimeswtax;
	}

	public String getOnetimekktax() {
		return onetimekktax;
	}

	public void setOnetimekktax(String onetimekktax) {
		this.onetimekktax = onetimekktax;
	}

	public String getOnetimeenttax() {
		return onetimeenttax;
	}

	public void setOnetimeenttax(String onetimeenttax) {
		this.onetimeenttax = onetimeenttax;
	}

	public String getVodcharges() {
		return vodcharges;
	}

	public void setVodcharges(String vodcharges) {
		this.vodcharges = vodcharges;
	}
	
	public String getVodkktax() {
		return vodkktax;
	}

	public void setVodkktax(String vodkktax) {
		this.vodkktax = vodkktax;
	}

	public String getVodsrvctax() {
		return vodsrvctax;
	}

	public void setVodsrvctax(String vodsrvctax) {
		this.vodsrvctax = vodsrvctax;
	}

	public String getVodswtax() {
		return vodswtax;
	}

	public void setVodswtax(String vodswtax) {
		this.vodswtax = vodswtax;
	}

	public String getVodenttax() {
		return vodenttax;
	}

	public void setVodenttax(String vodenttax) {
		this.vodenttax = vodenttax;
	}

	public String getRentalcharges() {
		return rentalcharges;
	}

	public void setRentalcharges(String rentalcharges) {
		this.rentalcharges = rentalcharges;
	}

	public String getRentalsrvctax() {
		return rentalsrvctax;
	}

	public void setRentalsrvctax(String rentalsrvctax) {
		this.rentalsrvctax = rentalsrvctax;
	}

	public String getRentalswtax() {
		return rentalswtax;
	}

	public void setRentalswtax(String rentalswtax) {
		this.rentalswtax = rentalswtax;
	}

	public String getRentalskktax() {
		return rentalskktax;
	}

	public void setRentalskktax(String rentalskktax) {
		this.rentalskktax = rentalskktax;
	}

	public String getRentalenttax() {
		return rentalenttax;
	}

	public void setRentalenttax(String rentalenttax) {
		this.rentalenttax = rentalenttax;
	}

	public String getTpusagecharges() {
		return tpusagecharges;
	}

	public void setTpusagecharges(String tpusagecharges) {
		this.tpusagecharges = tpusagecharges;
	}

	public String getTpsrvctax() {
		return tpsrvctax;
	}

	public void setTpsrvctax(String tpsrvctax) {
		this.tpsrvctax = tpsrvctax;
	}

	public String getTpswtax() {
		return tpswtax;
	}

	public void setTpswtax(String tpswtax) {
		this.tpswtax = tpswtax;
	}

	public String getTpkktax() {
		return tpkktax;
	}

	public void setTpkktax(String tpkktax) {
		this.tpkktax = tpkktax;
	}

	public String getTpenttax() {
		return tpenttax;
	}

	public void setTpenttax(String tpenttax) {
		this.tpenttax = tpenttax;
	}

}
