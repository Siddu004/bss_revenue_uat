package com.arbiva.apfgc.comsystem.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.comsystem.dto.CorpusIptvDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.util.ApplictionAutoWires;
import com.arbiva.apfgc.comsystem.util.Response;
import com.arbiva.apfgc.comsystem.vo.CustomerDetailsVo;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;
import com.xyzinnotech.bss.sms.dto.Sms;

@RestController
public class CorpusAPIRestController {

	private static final Logger logger = Logger.getLogger(CorpusAPIRestController.class);

	@Autowired
	ApplictionAutoWires aw;

	@Autowired
	ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;

/*	@RequestMapping(value = "/buypackagefromcorpusserver", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> buypackagefromcorpusserver(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		try {
			map = aw.getCustomerService().buypackagefromcorpusserver(customerDetailsDTO);
		} catch (Exception e) {
			logger.error("CorpusAPIRestController::buypackagefromcorpusserver() " + e);
			e.printStackTrace();
		} finally {

		}
		return map;
	}*/

	@RequestMapping(value = "/vodService", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> vodService(@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		try {
			aw.getCorpusAPIService().validateRequest(customerDetailsDTO);
			map = aw.getCorpusAPIService().vodService(customerDetailsDTO);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::vodService() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}

	@RequestMapping(value = "/getIptvPackages", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> getIptvPkges(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		try {
			if (customerDetailsDTO.getSubscribercode() == null || customerDetailsDTO.getSubscribercode().equalsIgnoreCase(""))
				throw new RuntimeException("Subscriber Code Is Empty or Null");
			map = aw.getCorpusAPIService().getIptvPkges(customerDetailsDTO.getSubscribercode());
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::getIptvPkges() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}

	@RequestMapping(value = "/generateOTPForIptvPkges", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> generateOTPForIptvPkges(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		try {
			if (customerDetailsDTO.getSubscribercode() == null
					|| customerDetailsDTO.getSubscribercode().equalsIgnoreCase(""))
				throw new RuntimeException("Subscriber Code Is Empty or Null");
			if (customerDetailsDTO.getPackageCodesList() == null
					|| customerDetailsDTO.getPackageCodesList().equalsIgnoreCase(""))
				throw new RuntimeException("Package Codes Are Empty");
			map = aw.getCorpusAPIService().generateOTPForIptvPkges(customerDetailsDTO);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::getIptvPkges() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}
	
	@RequestMapping(value = "/sendCorpusSMS", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> generateCorpusSMS(
			@RequestBody Sms sms) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		try {
			if (sms.getSubscribercode() == null
					|| sms.getSubscribercode().equalsIgnoreCase(""))
				throw new RuntimeException("Subscriber Code Is Empty or Null");
			if (sms.getMsg() == null
					|| sms.getMsg().equalsIgnoreCase(""))
				throw new RuntimeException("Message is Empty or Null");
			map = aw.getCorpusAPIService().sendCorpusSMS(sms);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::Sending Corpus SMS Failed " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}

	
	
	/*@RequestMapping(value = "/activateAlaCartePackage", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> activateAlaCartePackage(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CorpusIptvDTO corpusIptvDTO = null;
		try {
			if (customerDetailsDTO.getReferenceId() == null || customerDetailsDTO.getReferenceId().equalsIgnoreCase(""))
				throw new RuntimeException("Reference Id Is Empty or Null");
			corpusIptvDTO = aw.getCorpusAPIService().activateAlaCartePackage(customerDetailsDTO.getReferenceId());
			map = corpusIptvDTO.getMap();

			 Calling Provisioning Request 
			if (corpusIptvDTO.getProdCafNo() != null && !corpusIptvDTO.getProdCafNo().equalsIgnoreCase(""))
				provisioningBusinessServiceImpl.processProvisioningRequests(corpusIptvDTO.getProdCafNo(), 0);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::getIptvPkges() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}*/
	
	@RequestMapping(value = "/activateAlaCartePackage", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> activateAlaCartePackage(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CorpusIptvDTO corpusIptvDTO = null;
		try {
			logger.info("Alacarte Subscriber code is:"+customerDetailsDTO.getSubscribercode());
			logger.info("Alacarte Package codes:"+customerDetailsDTO.getPackageCodesList());
			if (customerDetailsDTO.getSubscribercode() == null
					|| customerDetailsDTO.getSubscribercode().equalsIgnoreCase(""))
				throw new RuntimeException("Alacarte Subscriber Code Is Empty or Null");
			if (customerDetailsDTO.getPackageCodesList() == null
					|| customerDetailsDTO.getPackageCodesList().equalsIgnoreCase(""))
				throw new RuntimeException("Alacarte Package Codes Are Empty");
			corpusIptvDTO = aw.getCorpusAPIService().activateAlaCartePackage(customerDetailsDTO);
			map = corpusIptvDTO.getMap();

			/* Calling Provisioning Request */
			if (corpusIptvDTO.getProdCafNo() != null && !corpusIptvDTO.getProdCafNo().equalsIgnoreCase(""))
				provisioningBusinessServiceImpl.processProvisioningRequests(corpusIptvDTO.getProdCafNo(), 0);
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::getIptvPkges() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}

	@RequestMapping(value = "/reGenerateOTP", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> reGenerateOTP(@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		try {
			if (customerDetailsDTO.getReferenceId() == null || customerDetailsDTO.getReferenceId().equalsIgnoreCase(""))
				throw new RuntimeException("Reference Id Is Empty or Null");
			map = aw.getCorpusAPIService().reGenerateOTP(customerDetailsDTO.getReferenceId());
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::getIptvPkges() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}
	
	@RequestMapping(value = "/deActivateAlaCartePackage", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> deActivateAlaCartePackage(
			@RequestBody CustomerDetailsDTO customerDetailsDTO) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		CorpusIptvDTO corpusIptvDTO = null;
		try {
			logger.info("Alacarte Subscriber code is:"+customerDetailsDTO.getSubscribercode());
			logger.info("Alacarte Package codes:"+customerDetailsDTO.getPackageCodesList());
			if (customerDetailsDTO.getSubscribercode() == null
					|| customerDetailsDTO.getSubscribercode().equalsIgnoreCase(""))
				throw new RuntimeException("Alacarte Subscriber Code Is Empty or Null");
			if (customerDetailsDTO.getPackageCodesList() == null
					|| customerDetailsDTO.getPackageCodesList().equalsIgnoreCase(""))
				throw new RuntimeException("Alacarte Package Codes Are Empty");
			corpusIptvDTO = aw.getCorpusAPIService().deActivateAlaCartePackage(customerDetailsDTO);
			map = corpusIptvDTO.getMap();

			/* Calling Provisioning Request */
			if (corpusIptvDTO.getProdCafNo() != null && !corpusIptvDTO.getProdCafNo().equalsIgnoreCase(""))
				provisioningBusinessServiceImpl.alaCartepackageTermination(corpusIptvDTO.getCafNo(), corpusIptvDTO.getProdCode(), corpusIptvDTO.getStbCafNo(), corpusIptvDTO.getNwSubsCode());
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.getName(), Response.Code500.getName());
			innerMap.put(Response.statusMessage.getName(), e.getMessage());
			map.put(Response.responseStatus.getName(), innerMap);
			logger.error("CorpusAPIRestController::deActivateAlaCartePackage() " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
		}
		return map;
	}
	

}
