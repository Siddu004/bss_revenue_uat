package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.dto.PublicIpAddressDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;
import com.arbiva.apfgc.comsystem.model.CpeStock;
import com.arbiva.apfgc.comsystem.model.Phonenos;

/**
 * @author Srinivas V
 * @since Feb 08 2017
 */
@Repository
public class ChangeDeviceDaoImpl {
	
	private static final Logger LOGGER = Logger.getLogger(ChangeDeviceDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	/**
	 * @param aadhar
	 * @param tenantCode
	 * @param tenatType 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafList(String aadhar, String tenantCode, String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append(" CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid ");
		if(!tenatType.equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+tenantCode+"'  ");
		queryString.append(" AND cu.aadharno = '"+aadhar+"' ");
		queryString.append(" AND c.status IN (6,7) ");
		
		/*if(tenatType!= null && tenatType.equalsIgnoreCase("LMO")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.fname, cu.lname, cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from customers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and cu.aadharno = '"+aadhar+"'");
			queryString.append(" and c.status = 6 ");
		}else if(tenatType!= null && tenatType.equalsIgnoreCase("SI")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.custname, '', cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from entcustomers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and cu.regncode = '"+aadhar+"'");
			queryString.append(" and c.status = 6 ");
		}*/
		try {
			
			
			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * @param mobileNo
	 * @param tenantCode
	 * @param tenatType 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafListWithMobileNO(String mobileNo, String tenantCode, String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append("  CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid  ");
		if(!tenatType.equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+tenantCode+"'  ");
		queryString.append(" AND cu.pocmob1 = '"+mobileNo+"' ");
		queryString.append(" AND c.status IN (6,7) ");
		
		/*if(tenatType!= null && tenatType.equalsIgnoreCase("LMO")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.fname,cu.lname, cu.addr1,cu.locality,");
			queryString.append("v.villagename, m.mandalname,d.districtname  from customers cu,cafs c,");
			queryString.append("districts d, mandals m, villages v where cu.custid = c.custid and cu.district = d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid ");
			queryString.append(" and c.lmocode = '"+tenantCode+"' and cu.pocmob1 = '"+mobileNo+"'");
			queryString.append(" and c.status = 6 ");
		}else if(tenatType!= null && tenatType.equalsIgnoreCase("SI")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.custname, '', cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from entcustomers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and cu.pocmob1 = '"+mobileNo+"'");
			queryString.append(" and c.status = 6 ");
		}*/
		try {
			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * @param cafNumber
	 * @param tenantCode
	 * @param tenatType 
	 * @return Customer detail based on CAF Number
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafsByCafNo(String cafNumber, String tenantCode, String tenatType) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> caflist = new ArrayList<>();
		
		queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append(" CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid ");
		if(!tenatType.equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+tenantCode+"'  ");
		queryString.append(" AND c.cafno = '"+cafNumber+"' ");
		queryString.append(" AND c.status IN (6,7) ");
		
	/*	if(tenatType!= null && tenatType.equalsIgnoreCase("LMO")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.fname,cu.lname, cu.addr1, ");
			queryString.append(" cu.locality,v.villagename, m.mandalname,d.districtname  from customers cu, ");
			queryString.append("cafs c, districts d, mandals m, villages v ");
			queryString.append(" where cu.custid = c.custid and cu.district = d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and ");
			queryString.append(" c.lmocode = '"+tenantCode+"' and c.cafno = '"+cafNumber+"'");
			queryString.append(" and c.status = 6 ");
		}else if(tenatType!= null && tenatType.equalsIgnoreCase("SI")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.custname, '', cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from entcustomers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and c.cafno = '"+cafNumber+"'");
			queryString.append(" and c.status = 6 ");
		}*/
		
		try {

			
			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * @param tenantCode
	 * @param tenatType 
	 * @return Customers List based on Login
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafsByTenantCode(String tenantCode, String tenatType) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> caflist = new ArrayList<>();
		
		queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append(" CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid ");
		if(!tenatType.equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+tenantCode+"'  ");
		queryString.append(" AND c.status IN (6,7) ");
		
		/*if(tenatType!= null && tenatType.equalsIgnoreCase("LMO")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.fname, cu.lname, cu.addr1, cu.locality, ");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from customers cu, cafs c, ");
			queryString.append(" districts d, mandals m, villages v  ");
			queryString.append(" where cu.custid = c.custid and cu.district = d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"'");
			queryString.append(" and c.status = 6 ");
		}else if(tenatType!= null && tenatType.equalsIgnoreCase("SI")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.custname, '', cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from entcustomers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and c.status = 6 ");
		}*/
		try {

			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getCafListByTrackId(String trackId, String tenantCode, String tenatType) {
		List<Object[]> caflist = new ArrayList<>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append(" CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid  ");
		if(!tenatType.equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+tenantCode+"'  ");
		queryString.append(" and c.apsfluniqueid = '"+trackId+"'");
		queryString.append(" AND c.status IN (6,7) ");
		
		/* if(tenatType!= null && tenatType.equalsIgnoreCase("SI")){
			queryString.append("select c.lmocode, c.aadharno, c.cafno, cu.custname, '', cu.addr1, cu.locality,");
			queryString.append(" v.villagename, m.mandalname, d.districtname  from entcustomers cu, cafs c, districts d, ");
			queryString.append(" mandals m, villages v where cu.custid = c.custid and cu.district=d.districtuid and ");
			queryString.append(" cu.mandal=m.mandalslno and cu.district = m.districtuid and cu.city_village= v.villageslno and cu.mandal = v.mandalslno and cu.district = v.districtuid and c.lmocode= '"+tenantCode+"' ");
			queryString.append(" and c.apsfluniqueid = '"+trackId+"'");
			queryString.append(" and c.status = 6 ");
		}*/
		try {
			
			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caflist;
	}

	/**
	 * @param cafNum
	 * @return Stb details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getStbDetailsByCafNo(String cafNum) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> stbDetails = new ArrayList<>();

		try {
			queryString.append("select c.cafno, cs.stbcafno , cs.stbmacaddr, cs.nwsubscode, c.custid, cs.stbslno, cs.stbprofileid from ");
			queryString.append(" cafstbs cs, cafs c where c.cafno = cs.parentcafno  and c.cafno = '"+cafNum+"'");
			queryString.append(" and c.status = 6 ");
			stbDetails = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stbDetails;
	}

	/**
	 * Update STB details and change the status of the new And Old STb's
	 * @param oldstbmac
	 * @param newStbMac
	 * @param stbCafNo
	 * @param changePurpose
	 * @param tenantCode
	 * @return Update Status 
	 */
	@SuppressWarnings("unchecked")
	public String[] updateStbMac(String oldstbmac, String newStbMac, String stbCafNo, String changePurpose,
			String tenantCode,String paretnCafNo) {
		StringBuilder queryString = new StringBuilder();
		/*StringBuilder queryStringCheck = new StringBuilder();*/
		StringBuilder statuschangeQuery = new StringBuilder();
		StringBuilder oldStbStatusChangeQuery = new StringBuilder();
		/*List<CpeStock> checkList = new ArrayList<>();*/
		String[] statusMessage = new String[3];
		List<Object[]> newStbserialList = new ArrayList<>();
		/*CpeStock cpeStock = new CpeStock();*/

		try {

			queryString.append("select cpeslno, profile_id from cpestock where cpemacaddr = '"+newStbMac+"'");
			newStbserialList = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

			if (newStbserialList.size() > 0)
			{
				  Object[] s= newStbserialList.get(0);
				  statusMessage[1] = s[0].toString();
				  statusMessage[2] = s[1].toString();
			}
			queryString = new StringBuilder();
			queryString.append("update cafstbs set stbslno = '"+statusMessage[1]+"', stbmacaddr = '"+newStbMac+"',");
			queryString.append(" stbchangehist = CONCAT('"+oldstbmac+"',' , ',IFNULL(stbchangehist,''))");
			queryString.append(" where stbcafno = '"+stbCafNo+"' and stbmacaddr = '"+oldstbmac+"'");
			getEntityManager().createNativeQuery(queryString.toString()).executeUpdate();

			statuschangeQuery.append(" update cpestock set lmocode='"+tenantCode+"' , status= '4' , cafno = '"+paretnCafNo+"' where  cpemacaddr = '"+newStbMac+"'");
			getEntityManager().createNativeQuery(statuschangeQuery.toString()).executeUpdate();

			oldStbStatusChangeQuery.append(" update cpestock set  cafno = null , status = '"+changePurpose+"' ");
			oldStbStatusChangeQuery.append(" where  cpemacaddr = '"+oldstbmac+"' and status='4' ");
			getEntityManager().createNativeQuery(oldStbStatusChangeQuery.toString()).executeUpdate();

			statusMessage[0] = "STB Mac Address Updated Successfully!!!";

		} catch (Exception e) {
			e.printStackTrace();
			statusMessage[0] = "<span style='color: red;'> STB Mac Address Updation Failed !!! </span>";
		}
		return statusMessage;
	}

	/**
	 * Get Valid STB based on LMO or LMO Partners MSO
	 * @param newStbMac
	 * @param tenantCode
	 * @return STB Status
	 */
	@SuppressWarnings("unchecked")
	public String getNewStdStatus(String newStbserial, String tenantCode) {
		 
		StringBuilder queryString = new StringBuilder();
		String macaddress = null;
		List<String> checkList = new ArrayList<>();

		try {
			queryString.append("select cpemacaddr from cpestock where lmocode  = '"+tenantCode+"'");
			queryString.append(" and  status = 3 and  cpeslno = '"+newStbserial+"'");
			checkList = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

			if (checkList.isEmpty()) {
				 
				queryString = new StringBuilder();
				queryString.append("select cpemacaddr from cpestock where mspcode IN (select distinct  ");
				queryString.append(" rsagrpartners.partnercode from rsagrpartners where  agruniqueid  IN ");
				queryString.append(" (select distinct agruniqueid from rsagrpartners where partnercode = '"+tenantCode+"') ");
				queryString.append("  and rsagrpartners.partnertype = 'msp') ");
				queryString.append(" and cpestock.status = 2  and cpestock.cpeslno = '"+newStbserial+"'");
				checkList = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
				if (!checkList.isEmpty())
				macaddress= checkList.get(0);
				 
			} else
				macaddress= checkList.get(0);
				 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return macaddress;
	}

	/**
	 *  Get ONU details based on CAF number
	 * @param cafNum
	 * @return ONU details
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getOnuDetailsByCafNo(String cafNum) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> onuDetails = new ArrayList<>();
		try {

			queryString.append("select  c.cafno, c.custid,  c.cpeslno, c.cpemacaddr, cp.cpe_model, ");
			queryString.append("cp.cpe_profilename,c.agorahsisubscode, c.cpeprofileid from cafs c, cpe_profilemaster cp ");
			queryString.append("where cp.profile_id = c.cpeprofileid and c.cafno = '"+cafNum+"'");
			queryString.append(" and c.status = 6 ");
			onuDetails = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return onuDetails;
	}

	/**
	 * Check for Valid  Serial Number Based on LMO or LMO's Partners MSO
	 * @param newOnuMacNumber
	 * @param tenantCode
	 * @param oldOnuSerial
	 * @return status
	 */
	@SuppressWarnings("unchecked")
	public String getNewOnuStatus(String newOnuMacNumber, String tenantCode, String oldOnuSerial) {
		 
		String macaddress = null;
		StringBuilder queryString = new StringBuilder();
		List<String> checkList = new ArrayList<>();
		try {
			queryString.append("select cpemacaddr from cpestock where cpestock.profile_id = ");
			queryString.append("(select distinct cafs.cpeprofileid from cafs cafs, cpe_profilemaster cp where ");
			queryString.append(" cp.profile_id = cafs.cpeprofileid and cafs.cpeslno = '"+oldOnuSerial+"')");
			queryString.append(" and cpestock.cpeslno = '"+newOnuMacNumber+"'");
			queryString.append(" and cpestock.status = '3' and cpestock.lmocode = '"+tenantCode+"'");
			checkList = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

			if (checkList.isEmpty()) {
				 
				queryString = new StringBuilder();
				queryString.append("select cpemacaddr from cpestock where cpestock.profile_id = ");
				queryString.append("(select cafs.cpeprofileid from cafs cafs, cpe_profilemaster cp where ");
				queryString.append(" cp.profile_id = cafs.cpeprofileid and cafs.cpeslno = '"+oldOnuSerial+"') ");
				queryString.append(" and cpestock.cpeslno = '"+newOnuMacNumber+"'");
				queryString.append(" and cpestock.status = '2' and cpestock.mspcode in (select distinct ");
				queryString.append(" rsagrpartners.partnercode from rsagrpartners where  agruniqueid in ");
				queryString.append(" (select distinct agruniqueid from rsagrpartners where partnercode = '"+tenantCode+"')");
				queryString.append(" and partnertype = 'MSP')");
				checkList = getEntityManager().createNativeQuery(queryString.toString()).getResultList();
			
				if (!checkList.isEmpty())
					macaddress= checkList.get(0);
				 
			} else
				macaddress= checkList.get(0);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return macaddress;
	}

	/**
	 * Update ONU and change the Status of old and new ONU
	 * Defective to 99
	 * change Device IF LMO Status=3 If MSO Status=2
	 * status 4= assign to Caf
	 * old ONU Device Caf Number= NULL
	 * 
	 * @param oldOnuserail
	 * @param newOnuSerial
	 * @param onuCafNo
	 * @param changePurpose
	 * @return Status message
	 */
	@SuppressWarnings("unchecked")
	public String[] updateOnuMac(String oldOnuserail, String newOnuSerial,  String changePurpose,String onuCafNo, String tenantCode ) {

		StringBuilder queryString = new StringBuilder();
		StringBuilder queryStringCheck = new StringBuilder();
		StringBuilder statuschangeQuery = new StringBuilder();
		StringBuilder oldOnuStatusChangeQuery = new StringBuilder();
		/*List<CpeStock> checkList = new ArrayList<>();*/
		String[] statusMessage = new String[2];
		List<String>  onumacAddress = new ArrayList<>();
		/*CpeStock cpeStock = new CpeStock();*/

		try {
			queryStringCheck.append("select cpemacaddr from cpestock where cpeslno = '"+newOnuSerial+"'");
			 onumacAddress = getEntityManager().createNativeQuery(queryStringCheck.toString()).getResultList();

			if (onumacAddress.size() > 0)
				statusMessage[1] = onumacAddress.get(0);

			queryString.append("update cafs set cpechangehist = CONCAT('"+oldOnuserail+"',' , ',IFNULL(cpechangehist,'')) , cpeslno = '"+newOnuSerial+"' , cpemacaddr = '"+statusMessage[1]+"'");
			queryString.append(" where cafno = '"+onuCafNo+"' and cpeslno = '"+oldOnuserail+"'");

			getEntityManager().createNativeQuery(queryString.toString()).executeUpdate();

			statuschangeQuery.append("update cpestock set lmocode='"+tenantCode+"' , status= '4' , cafno = '"+onuCafNo+"' where  cpeslno = '"+newOnuSerial+"'");
			getEntityManager().createNativeQuery(statuschangeQuery.toString()).executeUpdate();

			oldOnuStatusChangeQuery.append("update cpestock set cafno = null , status= '"+changePurpose+"'");
			oldOnuStatusChangeQuery.append(" where  cpeslno = '"+oldOnuserail+"' and status = '4'");
			getEntityManager().createNativeQuery(oldOnuStatusChangeQuery.toString()).executeUpdate();

			statusMessage[0] = "ONU Mac Address Updated Successfully!!!";
		
		} catch (Exception e) {
			e.printStackTrace();
			statusMessage[0] = "<span style='color: red;'>ONU Mac Address Updation Failed!!!</span>";
		}
		return statusMessage;
	}

	@SuppressWarnings("unchecked")
	public List<Caf> findAllTerminatedCafs() {
		
		StringBuilder builder = new StringBuilder("");
		List<Caf> list = new ArrayList<>();
		
		builder.append(" select * from cafs where status = 8");
		
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(),Caf.class).getResultList();
			
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return list;
	}
	
	public <T> T  save (T t){
		return getEntityManager().merge(t);
	}

	@SuppressWarnings("unchecked")
	public CpeStock findOnuByOnuSerialNo(String cpeslNo) {
		CpeStock cpeStock= null;
		List<CpeStock> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT * FROM cpestock WHERE cpeslno = '"+cpeslNo+"'");
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(),CpeStock.class).getResultList();
			if(list.size() > 0)
				cpeStock = list.get(0);
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return cpeStock;
	}

	
	@SuppressWarnings("unchecked")
	public List<CafSTBs> findCafStbsByCafId(Long cafNo) {
		List<CafSTBs> list = new ArrayList<>();
		StringBuilder builder = new  StringBuilder();
		builder.append(" select * from cafstbs where parentcafno = '"+cafNo+"' ");
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(), CafSTBs.class).getResultList();
			
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public CpeStock findStbByStbMacAddress(String cpeMacAddr) {
		CpeStock cpeStock= null;
		List<CpeStock> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT * FROM cpestock WHERE cpemacaddr = '"+cpeMacAddr+"'");
		//builder.append("SELECT * FROM cpestock WHERE cpemacaddr = '"+cpeMacAddr+"'");
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(),CpeStock.class).getResultList();
			if(list.size() > 0)
				cpeStock = list.get(0);
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return cpeStock;
	}

	@SuppressWarnings("unchecked")
	public List<CafSrvcPhoneNos> finsCafPhoneNosByCafNo(Long cafNo) {
		List<CafSrvcPhoneNos> list = new ArrayList<>();
		StringBuilder builder = new  StringBuilder();
		builder.append(" select * from cafsrvcphonenos where parentcafno = '"+cafNo+"' ");
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(), CafSrvcPhoneNos.class).getResultList();
			
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public Phonenos findPhoneNosByCafSrvcPhNo(String phoneNo) {
		Phonenos phonenos = null;
		List<Phonenos> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT * FROM phonenos WHERE phoneno = '"+phoneNo+"'");
		
		try{
			list = getEntityManager().createNativeQuery(builder.toString(),Phonenos.class).getResultList();
			if(list.size() > 0)
				phonenos = list.get(0);
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return phonenos;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getCafsListPuIP(PublicIpAddressDTO publicIpAddressDTO) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> caflist = new ArrayList<>();
		if(!publicIpAddressDTO.getCafNum().equalsIgnoreCase("") || !publicIpAddressDTO.getTrackId().equalsIgnoreCase("") || !publicIpAddressDTO.getCustId().equalsIgnoreCase(""))
		{
		/*queryString.append(" SELECT c.lmocode, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.regncode ELSE cu.aadharno END aadharno, c.cafno, "); 
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  cu.custname ELSE cu.custname END fname,  ");
		queryString.append(" CASE WHEN cu.custtypelov = 'ENTERPRISE' THEN  '' ELSE cu.lname END lname,  ");
		queryString.append(" CONCAT(c.inst_addr1,',',c.inst_addr2) addr, c.inst_locality ,v.villagename, m.mandalname, d.districtname  ");
		queryString.append(" FROM customermst cu, cafs c, districts d, ");
		queryString.append(" mandals m, villages v WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND  ");
		queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno ");
		queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid ");*/
		
			queryString.append(" SELECT  c.cafno as 'Account Number',  CONCAT( cu.custname,' ',IFNULL(cu.lname,'')) as CustomerName,");
			queryString.append(" om.pop_name As 'POP Name',  om.pop_olt_ipaddress as 'OLT ID',");
			queryString.append(" c.olt_portid, c.olt_onuid, CONCAT(c.inst_addr1,',',c.inst_addr2) Address, v.villagename, m.mandalname,");
			queryString.append(" d.districtname  FROM customermst cu, cafs c, districts d,  mandals m,");
			queryString.append(" villages v, oltmaster om WHERE cu.custid = c.custid AND c.inst_district=d.districtuid AND ");  
			queryString.append(" c.inst_mandal=m.mandalslno AND c.inst_district = m.districtuid AND c.inst_city_village= v.villageslno "); 
			queryString.append(" AND c.inst_mandal = v.mandalslno AND c.inst_district = v.districtuid AND c.olt_id = om.pop_olt_serialno ");
			
		if(!publicIpAddressDTO.getTenantType().equalsIgnoreCase("APSFL"))
			queryString.append(" AND c.lmocode= '"+publicIpAddressDTO.getTenantCode()+"'  ");
		
		if(publicIpAddressDTO.getCafNum()!= null && !publicIpAddressDTO.getCafNum().isEmpty() )
		queryString.append(" AND c.cafno = '"+publicIpAddressDTO.getCafNum()+"' ");
		
		if(publicIpAddressDTO.getCustId() != null && !publicIpAddressDTO.getCustId().isEmpty() )
		queryString.append(" AND c.custid = '"+publicIpAddressDTO.getCustId() +"' ");
		
		if(publicIpAddressDTO.getTrackId()!= null && !publicIpAddressDTO.getTrackId().isEmpty() )
		queryString.append(" AND c.apsfluniqueid = '"+publicIpAddressDTO.getTrackId()+"' ");
		
		queryString.append(" AND c.status IN (6,7) ");
			 
		try {
			
			caflist = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());	
		}
		}
		return caflist;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getIpAddress(PublicIpAddressDTO publicIpAddressDTO) {
		StringBuilder queryString = new StringBuilder();
		List<Object[]> ipAddress = new ArrayList<>();
		queryString.append(" SELECT publicipno FROM cafs where cafno='"+publicIpAddressDTO.getCafNum()+"';"); 
try {
			
				ipAddress = getEntityManager().createNativeQuery(queryString.toString()).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return ipAddress;
	}

	
	public String updateIpAddress(PublicIpAddressDTO publicIpAddressDTo) {
		StringBuilder queryString = new StringBuilder();
		StringBuilder queryCafs = new StringBuilder();
		StringBuilder queryPublicIpsnos = new StringBuilder();
		StringBuilder queryPIpsnosReset = new StringBuilder();
		 
		String statusMsg="";
		String ipAddress= "";
		queryString.append("  SELECT ipno FROM publicipnos where ipno= '"+publicIpAddressDTo.getpIpAddress()+"' AND  status = 1 ;"); 
try {
			
	ipAddress = (String) getEntityManager().createNativeQuery(queryString.toString()).getSingleResult();
	
	
	     if(ipAddress != null && !ipAddress.isEmpty() )
	     {    
	    	 queryCafs.append(" update cafs set publicipno = '"+ipAddress+"' , publicipnohist = CONCAT(DATE_FORMAT(NOW(), '%Y%m%d'),'-','"+ipAddress+" ',' , ',IFNULL(publicipnohist,''))");
	    	 queryCafs.append(" where cafno = '"+publicIpAddressDTo.getCafNum()+"'; ");
	    	 getEntityManager().createNativeQuery(queryCafs.toString()).executeUpdate();
	    	
	    	 
	    	 queryPIpsnosReset.append(" update publicipnos set cafno = null , status = 1 , statushist = CONCAT(2,'-',DATE_FORMAT(NOW(), '%Y%m%d'),'-','"+publicIpAddressDTo.getCafNum()+" ',' , ',IFNULL(statushist,'')) where ");
	    	 queryPIpsnosReset.append(" cafno= '"+publicIpAddressDTo.getCafNum()+"'; ");
	    	 getEntityManager().createNativeQuery(queryPIpsnosReset.toString()).executeUpdate();
	    	 
	    	 queryPublicIpsnos.append(" update publicipnos set allocatedon = NOW(), cafno = '"+publicIpAddressDTo.getCafNum()+"' , status = 2 , ");
	    	 queryPublicIpsnos.append(" statushist = CONCAT(1,'-',DATE_FORMAT(NOW(), '%Y%m%d'),'-','"+publicIpAddressDTo.getCafNum()+"',' , ',IFNULL(statushist,'')), modifiedby='"+publicIpAddressDTo.getTenantCode()+"' ");
	    	 queryPublicIpsnos.append(" where ipno= '"+ipAddress+"';");
	    	 getEntityManager().createNativeQuery(queryPublicIpsnos.toString()).executeUpdate();
	    	 
	    	 statusMsg="Public IP Address Successfully Updated...!!!";
	     }
	     else
	    	 statusMsg = "Update Failed... Please Eneter Valid IP Address...!!!";
	     
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return statusMsg;
	}

	public void releaseIpAddress(String cafNo) {
		StringBuilder queryString = new StringBuilder();
		 
		try{
		queryString.append(" update publicipnos set cafno = null , status = 1 , statushist = CONCAT(2,'-',DATE_FORMAT(NOW(), '%Y%m%d'),'-','"+cafNo+" ',' , ',IFNULL(statushist,'')) where ");
		queryString.append(" cafno= '"+cafNo+"'; ");
   	 getEntityManager().createNativeQuery(queryString.toString()).executeUpdate();
		}catch(Exception e)
		{
			LOGGER.error(e.getMessage());
		}
	}
}
