/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author Arbiva
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EventVO {

	private static final long serialVersionUID = 1L;

	public EventVO() {
	
	}
	
	private String custid;
	
	private String servicecode;
	
	private String amounttobereduced;
	
	private String additionalinfo;
	
	private String status;
	
	private String errormsg;

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getServicecode() {
		return servicecode;
	}

	public void setServicecode(String servicecode) {
		this.servicecode = servicecode;
	}

	public String getAmounttobereduced() {
		return amounttobereduced;
	}

	public void setAmounttobereduced(String amounttobereduced) {
		this.amounttobereduced = amounttobereduced;
	}

	public String getAdditionalinfo() {
		return additionalinfo;
	}

	public void setAdditionalinfo(String additionalinfo) {
		this.additionalinfo = additionalinfo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrormsg() {
		return errormsg;
	}

	public void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
