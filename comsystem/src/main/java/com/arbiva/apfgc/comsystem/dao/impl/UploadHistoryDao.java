/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.UploadHistory;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;

/**
 * @author Lakshman
 *
 */
@Repository
public class UploadHistoryDao {

private static final Logger logger = Logger.getLogger(UploadHistoryDao.class);
	
	private EntityManager em;
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	private EntityManager getEntityManager() {
		return em;
	}

	public List<EntCafStage> findAllEntCafStageByStatus(Long uploadId) {
		List<EntCafStage> entCafStagesList = new ArrayList<>();
		TypedQuery<EntCafStage> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(EntCafStage.class.getSimpleName()).append(" WHERE status = "+ComsEnumCodes.ENT_Caf_Pending.getCode()+" and uploadid = "+uploadId+" ");
		try {
			query = getEntityManager().createQuery(builder.toString(), EntCafStage.class);
			entCafStagesList = query.getResultList();
		} catch(Exception e) {
			logger.error("The Exception is UploadHistoryDao :: findAllEntCafStage" +e);
			e.printStackTrace();
		} finally {
			
		}
		return entCafStagesList;
	}

	public UploadHistory saveUploadHistory(UploadHistory uploadHistory) {
		UploadHistory uploadHistoryObj = new UploadHistory(); 
		try {
			uploadHistoryObj = getEntityManager().merge(uploadHistory);
		} catch (Exception e) {
			logger.error("EXCEPTION UploadHistoryDao :: saveUploadHistory() " + e);
			e.printStackTrace();
		} finally {

		}
		return uploadHistoryObj;
	}

	public EntCafStage saveEntCafStage(EntCafStage entCafStage) {
		EntCafStage entCafStageObj = new EntCafStage(); 
		try {
			entCafStageObj = getEntityManager().merge(entCafStage);
		} catch (Exception e) {
			logger.error("EXCEPTION UploadHistoryDao :: saveEntCafStage() " + e);
			e.printStackTrace();
		} finally {

		}
		return entCafStageObj;
	}

	public void updateUploadHistory(Long uploadId, Long successRecordCount) {
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::updateUploadHistory()");
			builder = new StringBuilder(" update uploadhist set successrecs = "+successRecordCount+" where uploadid = "+uploadId+" ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();
			logger.info("END::updateUploadHistory()");
		} catch(Exception e) {
			logger.error("The Exception is CafDao :: updateUploadHistory()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
	}

	public void updateEntCafStage(Long uploadId, Long uploadRecNo, String flag, String status) {
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::updateEntCafStage()");
			builder = new StringBuilder(" update entcafstg set remarks = '"+flag+"', status = '"+status+"' where uploadid = "+uploadId+" and uploadrecno = '"+uploadRecNo+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			query.executeUpdate();
			logger.info("END::updateEntCafStage()");
		} catch(Exception e) {
			logger.error("The Exception is CafDao :: updateEntCafStage()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
	}

	public VPNSrvcNamesStage saveVPNSrvcNamesStage(VPNSrvcNamesStage vpnSrvcNamesStage) {
		VPNSrvcNamesStage vpSrvcNamesStageObj = new VPNSrvcNamesStage(); 
		try {
			vpSrvcNamesStageObj = getEntityManager().merge(vpnSrvcNamesStage);
		} catch (Exception e) {
			logger.error("EXCEPTION UploadHistoryDao :: saveEntCafStage() " + e);
			e.printStackTrace();
		} finally {

		}
		return vpSrvcNamesStageObj;
	}

	public List<VPNSrvcNamesStage> getAllVPNSrvcNamesStageStatus(Long uploadId) {
		List<VPNSrvcNamesStage> vpnSrvcNamesStageList = new ArrayList<>();
		TypedQuery<VPNSrvcNamesStage> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(VPNSrvcNamesStage.class.getSimpleName()).append(" WHERE status = "+ComsEnumCodes.ENT_Caf_Pending.getCode()+" and uploadid = "+uploadId+" ");
		try {
			query = getEntityManager().createQuery(builder.toString(), VPNSrvcNamesStage.class);
			vpnSrvcNamesStageList = query.getResultList();
		} catch(Exception e) {
			logger.error("The Exception is UploadHistoryDao :: getAllVPNSrvcNamesStageStatus" +e);
			e.printStackTrace();
		} finally {
			
		}
		return vpnSrvcNamesStageList;
	}

	public VPNSrvcNamesStage updateVPNSrvcNamesStage(Long uploadId, Long uploadRecNo) {
		VPNSrvcNamesStage vpnSrvcNamesStage = new VPNSrvcNamesStage();
		StringBuilder builder = null;
		TypedQuery<VPNSrvcNamesStage> query = null;
		try {
			logger.info("START::updateVPNSrvcNamesStage()");
			builder = new StringBuilder(" FROM ").append(VPNSrvcNamesStage.class.getSimpleName()).append(" WHERE uploadid =:uploadId and uploadrecno =:uploadRecNo ");
			query = getEntityManager().createQuery(builder.toString(), VPNSrvcNamesStage.class);
			query.setParameter("uploadId", uploadId);
			query.setParameter("uploadRecNo", uploadRecNo);
			vpnSrvcNamesStage = query.getSingleResult();
		} catch (Exception e) {
			logger.error("The Exception is UploadHistoryDao::updateVPNSrvcNamesStage " + e);
			e.printStackTrace();
		} finally {
			builder = null;
			query = null;
		}
		return vpnSrvcNamesStage;
	}
}
