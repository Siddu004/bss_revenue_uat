/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.StatusCodesDaoImpl;
import com.arbiva.apfgc.comsystem.model.StatusCodes;

/**
 * @author Arbiva
 *
 */
@Service
@Transactional
public class StatusCodesServiceImpl {
	
	private static final Logger logger = Logger.getLogger(CafAccountServiceImpl.class);

	@Autowired
	StatusCodesDaoImpl statusCodesDao;

	public List<StatusCodes> findStatusCodesName() {
		List<StatusCodes> statusCodesList = new ArrayList<StatusCodes>();
		try {
			statusCodesList = statusCodesDao.findStatusCodesName();
		} catch(Exception e) {
			logger.error("StatusCodesServiceImpl :: findStatusCodesName()" + e);
			e.printStackTrace();
		}
		return statusCodesList;
	}
}
