/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.BO.BillPaymentBO;
import com.arbiva.apfgc.comsystem.BO.BillSummaryBO;
import com.arbiva.apfgc.comsystem.BO.CorpusPreviousBillBO;
import com.arbiva.apfgc.comsystem.BO.PackageBO;
import com.arbiva.apfgc.comsystem.BO.PreviousBillBO;
import com.arbiva.apfgc.comsystem.dao.impl.CPEInformationDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafAccountDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafBucketsDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafDao;
import com.arbiva.apfgc.comsystem.dao.impl.CustomerDao;
import com.arbiva.apfgc.comsystem.dao.impl.EnterpriseCustomerDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dto.AadharHelperDTO;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerInfoDTO;
import com.arbiva.apfgc.comsystem.dto.LCODetailsDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.dto.UploadHistDTO;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafAccount;
import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.model.CustCafHist;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.OLT;
import com.arbiva.apfgc.comsystem.model.Payments;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.service.CafAccountService;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafSTBsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.CustomerService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.util.FileUtil;
import com.arbiva.apfgc.comsystem.util.Response;
import com.arbiva.apfgc.comsystem.vo.AadharDtlsVO;
import com.arbiva.apfgc.comsystem.vo.BillSummaryVO;
import com.arbiva.apfgc.comsystem.vo.CorpusBillVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = Logger.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerDao customerDao;

	@Autowired
	CafDao cafDao;

	@Autowired
	CafService cafService;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Autowired
	CafProductsService cafProductsService;

	@Autowired
	CafServicesService cafServicesService;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	CafBucketsDao cafBucketsDao;

	@Autowired
	CafChargesService cafChargesService;

	@Autowired
	CafAccountService cafAccountService;

	@Autowired
	CafAccountDao cafAccountDao;

	@Autowired
	CafFeatureParamsService cafFeatureParamsService;

	@Autowired
	EnterpriseCustomerDao enterpriseCustomerDao;

	@Autowired
	CafSTBsService cafSTBsService;

	@Autowired
	CPEInformationDao cpeInformationDao;

	@Override
	@Transactional
	public Customer createCustomers(CustomerCafVO customerCafVO, String loginID, String tenantCode) {

		Customer customer = null;
		Long customerSeqNo = null;

		try {
			logger.info("CustomerServiceImpl :: createCustomers() :: Start");
			customerCafVO.setStatus(customerCafVO.getCustId() != null ? customerCafVO.getStatus()
					: ComsEnumCodes.CAF_PENDING_FOR_PACKAGE_STATUS.getCode());
			if (!customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode())) {
				if (!customerCafVO.getStatus()
						.equalsIgnoreCase(ComsEnumCodes.CUSTOMER_PAYMENT_RECEIVED_STATUS.getCode())) {
					if (customerCafVO.getCustId() == null) {
						customer = customerDao.findByCustomerCode(customerCafVO.getAadharNumber());
					}
					if (customer != null) {
						customer = customerDao.saveCustomer(customer);
					} else {
						customer = new Customer(customerCafVO);
						if (customerCafVO.getCustId() == null) {
							Long customerId = storedProcedureDAO
									.executeStoredProcedure(ComsEnumCodes.CUSTOMER_ID.getCode());
							customer.setCustId(customerId);
						} else {
							customer.setModifiedBy(loginID);
							customer.setModifiedIPAddr(customerCafVO.getIpAddress() != null
									? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
							customer.setCustId(customerCafVO.getCustId().longValue());
						}
						customer.setCreatedBy(loginID);
						customer.setCreatedOn(Calendar.getInstance());
						customer.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress()
								: httpServletRequest.getRemoteAddr());
						customer.setModifiedOn(Calendar.getInstance());
						customer.setStatus(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
						customer.setState("Andhra Pradesh");
						customer = customerDao.saveCustomer(customer);
						logger.info("The Customer Created Successfully");
					}
				} else {
					customer = customerDao.findByCustomerCode(customerCafVO.getAadharNumber());
				}
			}
			customerSeqNo = customer != null ? customer.getCustId()
					: Long.parseLong(customerCafVO.getEntCustomerCode());
			Integer custDistrictId = customer != null ? customer.getCustdistUid()
					: ComsEnumCodes.CAF_PENDING_FOR_PACKAGE_STATUS.getStatus();

			cafService.saveCaf(customerCafVO, customerCafVO.getCafNo(), customerSeqNo, loginID, custDistrictId);
			logger.info("The Caf Details Created Successfully");

			logger.info("CustomerServiceImpl :: createCustomers() :: End");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: createCustomers() :: " + e);
			e.printStackTrace();
		} finally {
			customerCafVO = null;
		}
		return customer;
	}

	@Override
	@Transactional
	public void updateCustomer(PaymentVO paymentVO, String loginID,int cardnum) {
		Customer customer = new Customer();
		Caf caf;
		try {
			logger.info("CustomerServiceImpl :: updateCustomer() :: Start");
			customer = customerDao.findByCustomerId(paymentVO.getCustomerCafVO().getCustId().longValue());
			customer.setStatus(ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus());
			customer.setModifiedBy(loginID);
			customer.setModifiedIPAddr(
					paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			customer.setModifiedOn(Calendar.getInstance());
			customerDao.saveCustomer(customer);
			logger.info("Customer Status Updated Successfully");

			caf = cafDao.findByCafNo(paymentVO.getCafNo());
			caf.setOltCardno(cardnum);
			caf.setStatus(ComsEnumCodes.CAF_REJECTED_STAUTS.getStatus());
			caf.setModifiedBy(loginID);
			caf.setModifiedIPAddr(
					paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			caf.setModifiedOn(Calendar.getInstance());
			cafDao.saveCaf(caf);
			logger.info("Caf Status Updated Successfully");
			logger.info("CustomerServiceImpl :: updateCustomer() :: End");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: updateCustomer() " + e);
			e.printStackTrace();
		} finally {
			paymentVO = null;
			caf = null;
			customer = null;
		}
	}

	@Override
	@Transactional
	//added cardnum parameter by chaitanya_xyz
	public String updateCustomerPaymentStatus(CustomerCafVO customerCafVO, PaymentVO paymentVO, String loginID,
			String onuNumber,int cardnum) throws Exception {
		String exception = "";
		Customer customer = new Customer();
		Caf caf;
		try {
			logger.info("CustomerServiceImpl :: updateCustomerPaymentStatus() :: Start");
			customer = customerDao.findByCustomerId(paymentVO.getCustomerCafVO().getCustId().longValue());
			customer.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			customer.setModifiedBy(loginID);
			customer.setModifiedIPAddr(paymentVO.getCustomerCafVO().getIpAddress() != null ? paymentVO.getCustomerCafVO().getIpAddress() : httpServletRequest.getRemoteAddr());
			customer.setModifiedOn(Calendar.getInstance());
			customerDao.saveCustomer(customer);
			logger.info("Customer Status Updated Successfully");

			caf = cafDao.findByCafNo(paymentVO.getCustomerCafVO().getCafNo());
			customerCafVO.setCustDistrictId(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? caf.getCustdistUid().toString() : "0");
			if (paymentVO.getPayment() == null) {
				exception = onuNumber;
				String status = FileUtil.NumberCheck(onuNumber);
				if (!status.equalsIgnoreCase("true"))
					throw new RuntimeException(exception);
				caf.setOltOnuid(Integer.parseInt(onuNumber));
			}
			caf.setOltCardno(cardnum);
			caf.setCafDate(Calendar.getInstance());
			caf.setCpeModel(Long.parseLong(customerCafVO.getCpeModal()));
			caf.setOltId(customerCafVO.getOltId());
			OLT olt = cpeInformationDao.getOLTDataByOLTSrlNo(customerCafVO.getOltId());
			caf.setOltIpaddr(olt.getPopOltIpaddress());
			String AAACode = "lag:" + olt.getOltAccessNode() + ":"+cardnum+":" + customerCafVO.getOltPortId() + ":" + onuNumber;
			caf.setAaaCode(AAACode);
			caf.setOltPortid((Integer.parseInt(customerCafVO.getOltPortId())));
			caf.setPopSubstnno(customerCafVO.getPopId());
			if (!ComsEnumCodes.SI_Tenant_Type.getCode().equalsIgnoreCase(customerCafVO.getTenantType())) {
				caf.setPortSplit(customerCafVO.getL1Slot() + "-" + customerCafVO.getL2Slot() + "-" + customerCafVO.getL3Slot());
			}
			String onuStatus = cafService.getCpeStackStatusBySrlNo(customerCafVO.getCpeId(), customerCafVO.getCpeModal());
			if (onuStatus.equalsIgnoreCase("3") || onuStatus.equalsIgnoreCase("2")) {
				customerCafVO.setFlag(onuStatus);
				caf.setCpeslNo(customerCafVO.getCpeId());
				caf.setCpeMacAddr(customerCafVO.getOnuMacAddress());
			} /*
				 * else if(onuStatus.equalsIgnoreCase("No Record")) {
				 * caf.setCpeslNo(customerCafVO.getCpeId()); }
				 */else {
				exception = "This ONU Serial Number is already blocked";
				throw new RuntimeException(exception);
			}
			caf.setCpeLeaseyn('N');
			caf.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			caf.setInstCityVillage(customerCafVO.getCity());
			if (customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
				caf.setPopDistrict(Integer.parseInt(customerCafVO.getPopDistrict()));
				caf.setPopMandal(Integer.parseInt(customerCafVO.getPopMandal()));
			}
			caf.setModifiedBy(loginID);
			caf.setModifiedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress()
					: httpServletRequest.getRemoteAddr());
			caf.setModifiedOn(Calendar.getInstance());
			cafDao.saveCaf(caf);
			logger.info("Caf Status Updated Successfully");

			cafSTBsService.saveCCafSTBs(customerCafVO, paymentVO.getPaidAmount());
			logger.info("Caf cafSTBs inserted Successfully");

			logger.info("CustomerServiceImpl :: updateCustomerPaymentStatus() :: End");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: updateCustomerPaymentStatus() :: " + e);
			e.printStackTrace();
			throw e;
		} finally {
			customerCafVO = null;
			paymentVO = null;
			customer = null;
			caf = null;
		}
		return exception;
	}

	@Override
	public Customer findByCustomerCode(String custCode) {
		Customer customer = null;
		try {
			logger.info("CustomerServiceImpl :: findByCustomerCode() :: Start");
			customer = customerDao.findByCustomerCode(custCode);
			String pocname = customer.getPocName();
			if (pocname.indexOf("(") != -1) {
				String[] splitString = pocname.split("\\(");
				customer.setPocName(splitString[0]);
				customer.setPocDesignation(splitString[1].replace(")", ""));
			}
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findByCustomerCode() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customer;
	}

	@Override
	public Customer findParticularCustomer(Long custId, String custType) {
		Customer customer = new Customer();
		Object[] object = null;
		String gender = "";
		try {
			logger.info("CustomerServiceImpl :: findParticularCustomer() :: Start");
			object = customerDao.findParticularCustomer(custId, custType);
			gender = object[4].toString();
			customer.setCustName(object[0].toString());
			customer.setlName(object[1] != null ? object[1].toString() : "NA");
			customer.setFhName(object[2] != null ? object[2].toString() : "NA");
			if (object[3] != null) {
				customer.setDateofinc(DateUtill.stringToDBDateString_Format(object[3].toString()));
			}
			customer.setGender(gender.charAt(0));
			customer.setEmail1(object[5] != null ? object[5].toString() : "NA");
			customer.setPocMob1(object[6] != null ? object[6].toString() : "NA");
			customer.setBillfreqLov(object[7] != null ? object[7].toString() : "NA");
			customer.setCustType(object[8] != null ? object[8].toString() : "NA");
			customer.setLmoCode(object[9] != null ? object[9].toString() : "NA");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findParticularCustomer() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customer;
	}

	@Override
	public Customer findByCustomerId(Long custId) {
		Customer customer = null;
		try {
			logger.info("CustomerServiceImpl :: findByCustomerId() :: Start");
			customer = customerDao.findByCustomerId(custId);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findByCustomerId() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customer;
	}

	@Override
	public List<Object[]> findAllCustomers(String tenantCode, String tenantType) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findAllCustomers() :: Start");
			customerList = customerDao.findAllCustomers(tenantCode, tenantType);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findAllCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public List<Object[]> findByParentCustomerCode(String customerCode, String tenantCode) {
		List<Object[]> customerList = new ArrayList<Object[]>();
		try {
			logger.info("CustomerServiceImpl :: findByParentCustomerCode() :: Start");
			customerList = customerDao.findByParentCustomerCode(customerCode, tenantCode);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findByParentCustomerCode() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public List<Customer> findByParentCustomerCode(String customerCode) {
		List<Customer> customerList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findByParentCustomerCode() :: Start");
			customerList = customerDao.findByParentCustomerCode(customerCode);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findByParentCustomerCode() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public List<Object[]> findCustomers(String tenantCode, String tenantType) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			customerList = customerDao.findCustomers(tenantCode, tenantType);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	@Transactional
	public void updateCustomerPackageStatus(CustomerCafVO customerCafVO) {
		Customer customer;
		Caf caf;
		try {
			if (!customerCafVO.getStatus().equalsIgnoreCase("2")) {
				customer = customerDao.findByCustomerId(customerCafVO.getCustId().longValue());
				customer.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
				customer.setModifiedBy(customerCafVO.getLoginId());
				customer.setModifiedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress()
						: httpServletRequest.getRemoteAddr());
				customer.setModifiedOn(Calendar.getInstance());
				customerDao.saveCustomer(customer);
				logger.info("Customer Status Updated Successfully");
			}

			caf = cafDao.findByCafNo(customerCafVO.getCafNo());
			caf.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
			caf.setModifiedBy(customerCafVO.getLoginId());
			caf.setModifiedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress()
					: httpServletRequest.getRemoteAddr());
			caf.setModifiedOn(Calendar.getInstance());
			cafDao.saveCaf(caf);
			logger.info("Caf Status Updated Successfully");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl::updateCustomer() " + e);
			e.printStackTrace();
		} finally {
			customer = null;
			caf = null;
			customerCafVO = null;
		}

	}

	@Override
	public Customer findByParentCustCode(String custCode) {
		Customer customer = null;
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			customer = customerDao.findByParentCustCode(custCode);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customer;
	}

	@Override
	@Transactional
	public Map<String, Map<String, Object>> buypackagefromcorpusserver(CustomerDetailsDTO customerDetailsDTO) {

		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		List<CafServices> cafSrvicesList;
		CafAccount cafAccount;
		Caf caf;
		Payments payments;
		CafProducts cafProd;
		List<Object[]> list;

		Object[] capAcc = customerDao.findCustomerBalance(customerDetailsDTO.getSubscribercode());
		try {
			logger.info("CustomerServiceImpl :: buypackagefromcorpusserver() :: Start");
			if (capAcc[0] != null && capAcc[0] != "") {

				Object[] product = customerDao.findPackageByPackageCode(customerDetailsDTO.getPackageCode());
				if (product[0] != null && product[0] != "") {
					BigDecimal custBal = capAcc[1] == null ? new BigDecimal("0") : new BigDecimal(capAcc[1].toString());
					BigDecimal prodCharge = product[1] == null ? new BigDecimal("0")
							: new BigDecimal(product[1].toString());
					int i = custBal.compareTo(prodCharge);
					if (i >= 0) {
						cafSrvicesList = customerDao.findDuplicateProduct(customerDetailsDTO.getPackageCode(),
								capAcc[0].toString());

						if (cafSrvicesList.size() == 0) {

							cafAccount = cafAccountDao.findByCafNo(Long.parseLong(capAcc[0].toString()),
									Integer.parseInt(capAcc[2].toString()));
							cafAccount.setRegBalence(custBal.subtract(prodCharge));
							cafAccountDao.saveCafAccount(cafAccount);

							caf = cafDao.findByCafAccountNo(cafAccount.getAcctcafno());
							caf.setCafNo(storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode()));
							caf.setCreatedOn(Calendar.getInstance());
							caf.setModifiedOn(Calendar.getInstance());
							caf.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
							cafDao.saveCaf(caf);

							payments = cafDao.findPaymentByAcctCno(cafAccount.getAcctcafno());
							payments.setPmntId(
									storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode()));
							payments.setCreatedOn(Calendar.getInstance());
							payments.setModifiedOn(Calendar.getInstance());
							cafDao.saveOrUpdateAnyObject(payments);

							cafProd = new CafProducts(caf, product);
							cafDao.saveOrUpdateAnyObject(cafProd);

							list = cafDao.getProdSrvesList(product[0].toString());
							for (Object[] obj : list) {
								CafServices cafSrvcs = new CafServices(caf, product, obj,
										customerDetailsDTO.getSubscribercode());
								cafDao.saveOrUpdateAnyObject(cafSrvcs);
							}

							innerMap = new LinkedHashMap<>();
							innerMap.put(Response.statusCode.name(), 200);
							innerMap.put(Response.statusMessage.name(), ComsEnumCodes.ACCEPTED.getCode());
						} else {
							innerMap = new LinkedHashMap<>();
							innerMap.put(Response.statusCode.name(), 404);
							innerMap.put(Response.statusMessage.name(), "Duplicate Product");
						}
					} else {
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.name(), 401);
						innerMap.put(Response.statusMessage.name(), "No Sufficient Balance");
					}
				} else {
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.name(), 402);
					innerMap.put(Response.statusMessage.name(), "Not valid package");
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.name(), 403);
				innerMap.put(Response.statusMessage.name(), "Not Valid Subscriber");

			}

			returnMap.put(Response.responseStatus.name(), innerMap);
			logger.info("CustomerServiceImpl :: buypackagefromcorpusserver() :: End");
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: buypackagefromcorpusserver() :: " + e);
			e.printStackTrace();
		} finally {
			innerMap = null;
			cafSrvicesList = null;
			cafAccount = null;
			caf = null;
			payments = null;
			cafProd = null;
			list = null;
			customerDetailsDTO = null;
		}
		return returnMap;
	}

	@Override
	public String getBlackListCustomerByAadharNo(String aadharNumber) {
		String status = "false";
		try {
			status = customerDao.getBlackListCustomerByAadharNo(aadharNumber);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMaxOltOnuNumber" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	/*
	 * @Override public List<UploadHistory> findCafBulkUploadErrors() {
	 * List<UploadHistory> errorList = new ArrayList<>(); try { logger.info(
	 * "CustomerServiceImpl :: findCafBulkUploadErrors() :: Start"); errorList =
	 * customerDao.findCafBulkUploadErrors(); for(UploadHistory vpnObject :
	 * errorList) { SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
	 * fmt.setCalendar(vpnObject.getUploadDate()); String dateFormatted =
	 * fmt.format(vpnObject.getUploadDate().getTime());
	 * vpnObject.setFileSize(dateFormatted); //Inserting Date into filesize }
	 * }catch (Exception e) { logger.error(
	 * "CustomerServiceImpl :: findCafBulkUploadErrors() :: "+e);
	 * e.printStackTrace(); } finally {
	 * 
	 * } return errorList; }
	 */

	@Override
	public List<EntCafStage> stageCafBulkUploadErrors(String uploadId) {
		List<EntCafStage> cafErrorList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCafBulkUploadErrors() :: Start");
			cafErrorList = customerDao.stageCafBulkUploadErrors(uploadId);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCafBulkUploadErrors() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return cafErrorList;
	}

	/*
	 * @Override public List<UploadHistory> findVPNUploadErrors() {
	 * List<UploadHistory> errorList = new ArrayList<>(); try { logger.info(
	 * "CustomerServiceImpl :: findCafBulkUploadErrors() :: Start"); errorList =
	 * customerDao.findVPNUploadErrors(); for(UploadHistory vpnObject :
	 * errorList) { SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
	 * fmt.setCalendar(vpnObject.getUploadDate()); String dateFormatted =
	 * fmt.format(vpnObject.getUploadDate().getTime());
	 * vpnObject.setFileSize(dateFormatted); //Inserting Date into filesize }
	 * }catch (Exception e) { logger.error(
	 * "CustomerServiceImpl :: findCafBulkUploadErrors() :: "+e);
	 * e.printStackTrace(); } finally {
	 * 
	 * } return errorList; }
	 */

	@Override
	public List<VPNSrvcNamesStage> stageVPNUploadErrors(String uploadId) {
		List<VPNSrvcNamesStage> errorList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCafBulkUploadErrors() :: Start");
			errorList = customerDao.stageVPNUploadErrors(uploadId);
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCafBulkUploadErrors() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return errorList;
	}

	@Override
	@Transactional
	public void updateCustomerDetails(CustomerCafVO customerCafVO) {
		Customer customer = new Customer();
		try {
			customer = customerDao.findByCustomerId(Long.parseLong(customerCafVO.getCustId().toString()));
			//customer.setEmail1(customerCafVO.getEmailId());
			customer.setPocMob1(customerCafVO.getMobileNo());
			customer.setPocMob2(customerCafVO.getMobileNo1());
			customer.setLandLine1(customerCafVO.getLandLineNo());
			customer.setBillfreqLov(customerCafVO.getBillCycle());
			customer.setStdCode(customerCafVO.getStdCode());
			customer.setAddress1(customerCafVO.getAddress1());
			customer.setAddress2(customerCafVO.getAddress2());
			customer.setLocality(customerCafVO.getLocality());
			customer.setDistrict(customerCafVO.getDistrict());
			customer.setMandal(customerCafVO.getMandal());
			customer.setCityVillage(customerCafVO.getCityVillage());
			customer.setPin(customerCafVO.getPinCode());
			customerDao.saveCustomer(customer);
			logger.info("The Customer Details Updated Successfully");

		} catch (Exception e) {
			logger.error("CustomerServiceImpl::updateCustomers() " + e);
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public String saveCustomerDetails(String modificationData, CustomerCafVO customerCafVO) {
		CustCafHist custCafHist = new CustCafHist();
		try {
			custCafHist.setChangedtls(modificationData);
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				custCafHist.setCustid(customerCafVO.getCustId().toString());
			} else {
				custCafHist.setEntunitid(customerCafVO.getCustId().toString());
			}
			custCafHist.setTenantcode(customerCafVO.getLmoCode());
			custCafHist.setModifiedBy(customerCafVO.getLoginId());
			custCafHist.setModifiedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress()
					: httpServletRequest.getRemoteAddr());
			custCafHist.setModifiedOn(Calendar.getInstance());
			custCafHist = customerDao.saveCustCafHist(custCafHist);
			logger.info("The Customer History Saved Successfully");

		} catch (Exception e) {
			logger.error("CustomerServiceImpl::updateCustomers() " + e);
			e.printStackTrace();
		}
		return "Saved successfully";
	}

	public ComsHelperDTO findAllCustomers(ComsHelperDTO comsHelperDTO) {
		List<Customer> customerList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			List<Object[]> customerObjectList = customerDao.findCustomers(comsHelperDTO.getTenantCode(),
					comsHelperDTO.getTenanttype(), comsHelperDTO.getPageObject(), comsHelperDTO.getCustType(),
					comsHelperDTO.getCustId());
			PageObject pageObject = comsHelperDTO.getPageObject();
			for (Object[] obj : customerObjectList) {
				Customer customer = new Customer();
				customer.setCustName(obj[0] != null ? obj[0].toString() : "NA");
				customer.setlName(obj[1] != null ? obj[1].toString() : "");
				customer.setCustCode(obj[2] != null ? obj[2].toString() : "NA");
				customer.setAddress1(obj[3] != null ? obj[3].toString() : "NA");
				customer.setAddress2(obj[4] != null ? obj[4].toString() : "NA");
				customer.setLocality(obj[5] != null ? obj[5].toString() : "NA");
				customer.setCityVillage(obj[6] != null ? obj[6].toString() : "NA");
				customer.setPocMob1(obj[7] != null ? obj[7].toString() : "NA");
				customer.setCustId(obj[8] != null ? Long.parseLong(obj[8].toString()) : 0);
				customer.setCustTypeLov(obj[9] != null ? obj[9].toString() : "NA");
				customer.setAadharNo(obj[10] != null ? obj[10].toString() : "NA");
				customer.setPocMob2(obj[11] != null ? obj[11].toString() : "NA");
				customerList.add(customer);
			}
			comsHelperDTO.setCustList(customerList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}

	public ComsHelperDTO getCafBulkUploadOrVPNUploadErrors(ComsHelperDTO comsHelperDTO, String uploadType) {
		List<UploadHistDTO> vpnErrorList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			List<Object[]> errorList = customerDao.getCafBulkUploadOrVPNUploadErrors(comsHelperDTO.getPageObject(),
					uploadType);
			PageObject pageObject = comsHelperDTO.getPageObject();
			for (Object[] obj : errorList) {
				UploadHistDTO uploadHistDTO = new UploadHistDTO();
				uploadHistDTO.setUploadid(obj[0] != null ? obj[0].toString() : "NA");
				uploadHistDTO.setFileName(obj[1] != null ? obj[1].toString() : "NA");
				uploadHistDTO.setUploaddate(obj[2] != null ? obj[2].toString() : "NA");
				uploadHistDTO.setTotalRec(obj[3] != null ? obj[3].toString() : "NA");
				uploadHistDTO.setSuccessRec(obj[4] != null ? obj[4].toString() : "NA");
				int failrec = ((Integer.parseInt(uploadHistDTO.getTotalRec()))
						- (Integer.parseInt(uploadHistDTO.getSuccessRec())));
				uploadHistDTO.setFailureRec(String.valueOf(failrec));
				vpnErrorList.add(uploadHistDTO);
			}
			comsHelperDTO.setVpnErrorList(vpnErrorList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}

	@Override
	public CustomerInfoDTO getCustDtlsByLoginid(String loginId) {

		CustomerInfoDTO custInfo = new CustomerInfoDTO();
		List<Object[]> list = new ArrayList<>();
		try {
			list = customerDao.getCustDtlsByLoginid(loginId);
			for (Object[] obj : list) {
				custInfo = new CustomerInfoDTO();
				custInfo.setSuccess("Success");
				custInfo.setSuccessMessage("Success Messge");
				custInfo.setCustId(obj[0] != null ? obj[0].toString() : "NA");
				custInfo.setCustType(obj[1] != null ? obj[1].toString() : "NA");
				custInfo.setAadharNo(obj[2] != null ? obj[2].toString() : "NA");
				custInfo.setFname(obj[3] != null ? obj[3].toString() : "NA");
				custInfo.setLname(obj[4] != null ? obj[4].toString() : "NA");
				custInfo.setEmail(obj[5] != null ? obj[5].toString() : "NA");
				custInfo.setMobile(obj[5] != null ? obj[6].toString() : "NA");
				custInfo.setLmo(obj[7] != null ? obj[7].toString() : "NA");
			}
		} catch (Exception e) {
			custInfo.setSuccess("Failure");
			custInfo.setSuccessMessage("Failed due to Exception");
			e.printStackTrace();
		}
		return custInfo;
	}

	@Override
	public List<LCODetailsDTO> getNearestLCO(String districtId, String mandalId, String villageId) {

		LCODetailsDTO custInfo = new LCODetailsDTO();
		List<Object[]> list = new ArrayList<>();
		List<LCODetailsDTO> lcoList = new ArrayList<>();
		try {
			list = customerDao.getNearestLCO(districtId, mandalId, villageId);
			for (Object[] obj : list) {

				custInfo = new LCODetailsDTO();
				custInfo.setSuccess("Success");
				custInfo.setSuccessMessage("Success Messge");
				custInfo.setTenantCode(obj[0] != null ? obj[0].toString() : "NA");
				custInfo.setTenantName(obj[1] != null ? obj[1].toString() : "NA");
				custInfo.setAddr1(obj[2] != null ? obj[2].toString() : "NA");
				custInfo.setAddr2(obj[3] != null ? obj[3].toString() : "NA");
				custInfo.setLocality(obj[4] != null ? obj[4].toString() : "NA");
				custInfo.setArea(obj[5] != null ? obj[5].toString() : "NA");
				custInfo.setCity(obj[6] != null ? obj[6].toString() : "NA");
				custInfo.setState(obj[7] != null ? obj[7].toString() : "NA");
				custInfo.setPin(obj[8] != null ? obj[8].toString() : "NA");
				custInfo.setEmail(obj[9] != null ? obj[9].toString() : "NA");
				custInfo.setPocName(obj[10] != null ? obj[10].toString() : "NA");
				custInfo.setPocMobile(obj[11] != null ? obj[11].toString() : "NA");

				lcoList.add(custInfo);
			}
		} catch (Exception e) {
			custInfo.setSuccess("Failure");
			custInfo.setSuccessMessage("No records found");
			e.printStackTrace();
		}
		return lcoList;
	}

	@Override
	public Customer findByCustomerCodeCSS(String aadharNo) {
		Customer customer = null;
		try {
			customer = customerDao.findByCustomerCodeCSS(aadharNo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return customer;
	}

	@Override
	public AadharHelperDTO getLmoDetails(AadharHelperDTO helperDto) {

		List<Object[]> list = new ArrayList<>();
		List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();
		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();
		try {
			for (AadharDtlsVO adharVo : adharList) {

				String aadharNo = adharVo.getAadharNo();
				list = customerDao.getLmoDetails(aadharNo);
				for (Object[] obj : list) {
					AadharDtlsVO lmoInfo = new AadharDtlsVO();
					lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
					lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
					lmoInfo.setLname(obj[2] != null ? obj[2].toString() : "NA");
					lmoInfo.setLmoName(obj[3] != null ? obj[3].toString() : "NA");
					lmoInfo.setMobileNo(obj[4] != null ? obj[4].toString() : "NA");
					lmoInfo.setEmail(obj[5] != null ? obj[5].toString() : "NA");
					lmoList.add(lmoInfo);
				}
				dto.setAadharDtlsVO(lmoList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}

	@Override
	public AadharHelperDTO getHsiUsageDtls(AadharHelperDTO helperDto) {

		List<Object[]> list = new ArrayList<>();
		List<BigInteger> cafList;
		List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();
		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		try {
			for (AadharDtlsVO adharVo : adharList) {

				String aadharNo = adharVo.getAadharNo();
				cafList = customerDao.getCafNos(aadharNo);
				for (BigInteger cafNo : cafList) {

					list = customerDao.getHsiUsageDtls(cafNo, year, month);
					for (Object[] obj : list) {
						AadharDtlsVO lmoInfo = new AadharDtlsVO();
						lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
						lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
						lmoInfo.setLname(obj[2] != null ? obj[2].toString() : "NA");
						lmoInfo.setTotalLimit(obj[4] != null ? obj[4].toString() : "NA");
						lmoInfo.setUsageLimit(obj[3] != null ? obj[3].toString() : "NA");
						lmoList.add(lmoInfo);
					}
					dto.setAadharDtlsVO(lmoList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
	
	@Override
	public AadharHelperDTO getHsiUsageDtlsBasedOnCaf(AadharHelperDTO helperDto) {
		List<Object[]> list = new ArrayList<>();
		List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();
		//AadharHelperDTO dto = new AadharHelperDTO();
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		try {
			for (AadharDtlsVO adharVo : adharList) {

				BigInteger cafNo =  new BigInteger(adharVo.getCafno());
					list = customerDao.getHsiUsageDtls(cafNo, year, month);
					for (Object[] obj : list) {
						//AadharDtlsVO lmoInfo = new AadharDtlsVO();
						//lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
						//lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
						//lmoInfo.setLname(obj[2] != null ? obj[2].toString() : "NA");
						adharVo.setTotalLimit(obj[4] != null ? obj[4].toString() : "NA");
						adharVo.setUsageLimit(obj[3] != null ? obj[3].toString() : "NA");
						//lmoList.add(lmoInfo);
					}
					//dto.setAadharDtlsVO(lmoList);
				//}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return helperDto;
	}
	
	

	@Override
	public AadharHelperDTO getTTDtls(AadharHelperDTO helperDto) {

		List<Object[]> list = new ArrayList<>();
		List<BigInteger> cafList;
		List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();
		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();

		try {
			for (AadharDtlsVO adharVo : adharList) {

				String aadharNo = adharVo.getAadharNo();
				cafList = customerDao.getCafNos(aadharNo);
				for (BigInteger cafNo : cafList) {

					list = customerDao.getTTDtls(cafNo);
					for (Object[] obj : list) {
						AadharDtlsVO lmoInfo = new AadharDtlsVO();
						lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
						lmoInfo.setStatus(obj[1] != null ? obj[1].toString() : "NA");
						lmoInfo.setCount(obj[2] != null ? obj[2].toString() : "NA");
						lmoList.add(lmoInfo);
					}
					dto.setAadharDtlsVO(lmoList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}

	@Override
	public AadharHelperDTO getPmntDtls(AadharHelperDTO helperDto) {

		List<Object[]> list = new ArrayList<>();
		List<BigInteger> cafList;
		List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();
		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();

		try {
			for (AadharDtlsVO adharVo : adharList) {

				String aadharNo = adharVo.getAadharNo();
				cafList = customerDao.getCafNos(aadharNo);
				for (BigInteger cafNo : cafList) {

					list = customerDao.getPmntDtls(cafNo);
					for (Object[] obj : list) {
						AadharDtlsVO lmoInfo = new AadharDtlsVO();
						lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
						lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
						lmoInfo.setLname(obj[2] != null ? obj[2].toString() : "NA");
						lmoInfo.setBalance(obj[3] != null ? obj[3].toString() : "NA");
						lmoInfo.setDate(obj[4] != null ? obj[4].toString() : "NA");
						lmoList.add(lmoInfo);
					}
					dto.setAadharDtlsVO(lmoList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}

	@Override
	public CorpusBillVO getCustomerDetailsByNWSubscriberCode(String nwSubscriberCode) {
		List<Object[]> customerHSIObject = new ArrayList<>();
		List<Object[]> customerPhoneObject = new ArrayList<>();
		CorpusBillVO corpusBillVO = new CorpusBillVO();
		;
		try {
			customerHSIObject = customerDao.getCustomerHSIDetails(nwSubscriberCode);
			customerPhoneObject = customerDao.getCustomerPhoneDetails(nwSubscriberCode);
			if (customerHSIObject.size() > 0) {
				corpusBillVO.setHsiUsage(customerHSIObject.get(0)[0] != null
						? customerHSIObject.get(0)[0].toString() + "(GB)" : "0(GB)");
				corpusBillVO.setUsageLimit(customerHSIObject.get(0)[1] != null
						? customerHSIObject.get(0)[1].toString() + "(GB)" : "0(GB)");
				corpusBillVO.setThroughtlingStatus(
						customerHSIObject.get(0)[2] != null ? customerHSIObject.get(0)[2].toString() : "N");
				corpusBillVO.setStatusCode("200");
				corpusBillVO.setDescription("Success");
			} else {
				corpusBillVO.setHsiUsage("0(GB)");
				corpusBillVO.setUsageLimit("0(GB)");
				corpusBillVO.setThroughtlingStatus("N");
			}

			if (customerPhoneObject.size() > 0) {
				corpusBillVO.setTelephoneUsage(customerPhoneObject.get(0) != null
						? String.valueOf(customerPhoneObject.get(0)) + "(Units)" : "0(Units)");
			} else {
				corpusBillVO.setTelephoneUsage("0(Units)");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CustomerServiceImpl :: getCustomerDetailsByNWSubscriberCode() :: " + e);
		}
		return corpusBillVO;
	}

	@Override
	public CorpusPreviousBillBO getCustomerBillDetailsByNWSubscriberCode(String nwSubscriberCode) {
		CorpusPreviousBillBO response = new CorpusPreviousBillBO();
		List<PreviousBillBO> billList = new ArrayList<>();
		try {
			response = customerDao.getCustomerBillDetailsByNWSubscriberCode(nwSubscriberCode);
			if (response.getCustomerId() != null && !response.getCustomerId().isEmpty()) {
				billList = customerDao.getPreviousBillDetails(response.getCustomerId());
				response.setPreviousBillList(billList);
				response.setStatusCode("200");
				response.setDescription("Success");

			} else {
				response.setStatusCode("500");
				response.setDescription("Invalid N/W Subscriber Code");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CustomerServiceImpl :: getCustomerDetailsByNWSubscriberCode() :: " + e);
		}
		return response;
	}

	@Override
	public BillSummaryVO getBillSummary(String billNo, String customerId, String accountNo) {
		BillSummaryBO response = new BillSummaryBO();
		BillSummaryVO billSummaryVO = new BillSummaryVO();
		BillPaymentBO billPaymentBO = new BillPaymentBO();
		List<PackageBO> packageList = new ArrayList<>();
		BigDecimal serviceTax = new BigDecimal("0");
		BigDecimal kisanTax = new BigDecimal("0");
		BigDecimal swatchTax = new BigDecimal("0");
		BigDecimal entTax = new BigDecimal("0");
		BigDecimal totalTax = new BigDecimal("0");
		BigDecimal totalChrg = new BigDecimal("0");
		try {
			response = customerDao.getBillSummary(billNo);
			if (response.getOneTimeCharges() != null && !response.getOneTimeCharges().isEmpty()) {
				serviceTax = serviceTax.add(new BigDecimal(response.getRentalsrvctax()))
						.add(new BigDecimal(response.getOnetimesrvcTax()))
						.add(new BigDecimal(response.getTpsrvctax()).add(new BigDecimal(response.getVodsrvctax())));
				kisanTax = kisanTax.add(new BigDecimal(response.getRentalskktax()))
						.add(new BigDecimal(response.getOnetimekktax()))
						.add(new BigDecimal(response.getTpkktax()).add(new BigDecimal(response.getVodkktax())));
				swatchTax = swatchTax.add(new BigDecimal(response.getRentalswtax()))
						.add(new BigDecimal(response.getOnetimeswtax()))
						.add(new BigDecimal(response.getTpswtax()).add(new BigDecimal(response.getVodswtax())));
				entTax = entTax.add(new BigDecimal(response.getRentalenttax()))
						.add(new BigDecimal(response.getOnetimeenttax()))
						.add(new BigDecimal(response.getTpenttax()).add(new BigDecimal(response.getVodenttax())));
				totalTax = totalTax.add(serviceTax).add(kisanTax).add(swatchTax).add(entTax);
				totalChrg = totalChrg.add(new BigDecimal(response.getOneTimeCharges()))
						.add(new BigDecimal(response.getRentalcharges()))
						.add(new BigDecimal(response.getTpusagecharges())).add(new BigDecimal(response.getVodcharges()))
						.add(totalTax);
				packageList = customerDao.getPackageList(accountNo);
				billPaymentBO = customerDao.getLastPaymentDetails(customerId);

				billSummaryVO.setLastPaymentAmount(billPaymentBO.getPmntamt());
				billSummaryVO.setLastPaymentDate(billPaymentBO.getPmntdate());
				billSummaryVO.setLastPaymentMode(billPaymentBO.getPmntmode());
				billSummaryVO.setPackageList(packageList);
				billSummaryVO.setAdjustmentCharges("0");
				billSummaryVO.setInternetUsageCharges("0");
				billSummaryVO.setEntTax(entTax.toString());
				billSummaryVO.setKisanTax(kisanTax.toString());
				billSummaryVO.setLateFee("0");
				billSummaryVO.setMonthlyCharges(response.getRentalcharges());
				billSummaryVO.setOneTimeCharges(response.getOneTimeCharges());
				billSummaryVO.setServiceTax(serviceTax.toString());
				billSummaryVO.setSwatchTax(swatchTax.toString());
				billSummaryVO.setTelephoneUsageCharges(response.getTpusagecharges());
				billSummaryVO.setTotalCharge(totalChrg.toString());
				billSummaryVO.setTotalTax(totalTax.toString());
				billSummaryVO.setValueAddedCharges(response.getVodcharges());
				billSummaryVO.setStatusCode("200");
				billSummaryVO.setDescription("Success");
			} else {
				billSummaryVO.setStatusCode("500");
				billSummaryVO.setDescription("Invalid Bill Number ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CustomerServiceImpl :: getCustomerDetailsByNWSubscriberCode() :: " + e);
		}
		return billSummaryVO;
	}

	/**
	 * 
	 * @author vinay
	 * @param cutomerid,year,month
	 */
	@Override
	public AadharHelperDTO getEnterpriseHsiUsageDtls(String custid, Integer year, Integer month) {

		List<Object[]> list = new ArrayList<>();
		// List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();

		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();
		// Calendar cal = Calendar.getInstance();
		// month = cal.get(Calendar.MONTH) + 1;
		// year = cal.get(Calendar.YEAR);

		try {

			list = customerDao.getEnterpriseHsiUsageDtls(custid, year, month);
			for (Object[] obj : list) {
				AadharDtlsVO lmoInfo = new AadharDtlsVO();
				lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
				lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
				lmoInfo.setLname(obj[2] != null ? obj[2].toString() : "NA");
				lmoInfo.setUsageLimit(obj[3] != null ? obj[3].toString() : "NA");
				lmoInfo.setTotalLimit(obj[4] != null ? obj[4].toString() : "NA");
				
				lmoList.add(lmoInfo);
			}
			dto.setAadharDtlsVO(lmoList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
	
	@Override
	public AadharHelperDTO getEnterprisePhoneUsageDtls(String custid, Integer year, Integer month) {

		List<Object[]> list = new ArrayList<>();
		// List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();

		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();
		// Calendar cal = Calendar.getInstance();
		// month = cal.get(Calendar.MONTH) + 1;
		// year = cal.get(Calendar.YEAR);

		try {

			list = customerDao.getEnterprisePhoneUsageDtls(custid, year, month);
			for (Object[] obj : list) {
				AadharDtlsVO lmoInfo = new AadharDtlsVO();
				lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
				lmoInfo.setUsageLimit(obj[1] != null ? obj[1].toString() : "NA");
				lmoInfo.setPhoneUsageCost(obj[2] != null ? obj[2].toString() : "NA");
				 
				lmoList.add(lmoInfo);
			}
			dto.setAadharDtlsVO(lmoList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
	
	
	@Override
	public AadharHelperDTO getPhoneUsageDtls(String acctcafno, Integer year, Integer month) {

		List<Object[]> list = new ArrayList<>();
		// List<AadharDtlsVO> adharList = helperDto.getAadharDtlsVO();

		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();
		// Calendar cal = Calendar.getInstance();
		// month = cal.get(Calendar.MONTH) + 1;
		// year = cal.get(Calendar.YEAR);

		try {
			list = customerDao.getPhoneUsageDtls(acctcafno, year, month);
			for (Object[] obj : list) {
				AadharDtlsVO lmoInfo = new AadharDtlsVO();
				lmoInfo.setCalledParty(obj[0] != null ? obj[0].toString() : "NA");
				lmoInfo.setStartTime(obj[1] != null ? obj[1].toString() : "NA");
				lmoInfo.setEndTime(obj[2] != null ? obj[2].toString() : "NA");
				lmoInfo.setCallDuration(obj[3] != null ? obj[3].toString() : "NA");
				lmoInfo.setCallunits(obj[4] != null ? obj[4].toString() : "NA");
				lmoInfo.setCallCharges(obj[5] != null ? obj[5].toString() : "NA");
				lmoList.add(lmoInfo);
			}
			dto.setAadharDtlsVO(lmoList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
	
	@Override
	public AadharHelperDTO getEnterpriseCafDtls(String custid) {

		List<Object[]> list = new ArrayList<>();

		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();

		try {

			list = customerDao.getEnterpriseCafDtls(custid);
			for (Object[] obj : list) {
				AadharDtlsVO lmoInfo = new AadharDtlsVO();
				lmoInfo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
				lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
				lmoInfo.setEmail(obj[2] != null ? obj[2].toString() : "NA");
				lmoInfo.setMobileNo(obj[3] != null ? obj[3].toString() : "NA");
				lmoInfo.setCpeprofileid(obj[4] != null ? obj[4].toString() : "NA");
				lmoInfo.setAgorahsisubscode(obj[5] != null ? obj[5].toString() : "NA");
				lmoList.add(lmoInfo);
			}
			dto.setAadharDtlsVO(lmoList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
	
	
	@Override
	public AadharHelperDTO getEnterpriseCutomerDtls(String custid) {

		List<Object[]> list = new ArrayList<>();

		List<AadharDtlsVO> lmoList = new ArrayList<>();
		AadharHelperDTO dto = new AadharHelperDTO();

		try {

			list = customerDao.getEnterpriseCutomerDtls(custid);
			for (Object[] obj : list) {
				AadharDtlsVO lmoInfo = new AadharDtlsVO();
				lmoInfo.setCustid(obj[0] != null ? obj[0].toString() : "NA");
				lmoInfo.setFname(obj[1] != null ? obj[1].toString() : "NA");
				lmoInfo.setPocname(obj[2] != null ? obj[2].toString() : "NA");
				lmoInfo.setMobileNo(obj[3] != null ? obj[3].toString() : "NA");
				//lmoList.add(lmoInfo);
				
				//AadharDtlsVO lmoInfo1 = new AadharDtlsVO();
				lmoInfo.setAddress(obj[4] != null ? obj[4].toString() : "NA");
				lmoInfo.setAddres2(obj[5] != null ? obj[5].toString() : "NA");
				lmoInfo.setVillagename(obj[6] != null ? obj[6].toString() : "NA");
				lmoInfo.setMandalname(obj[7] != null ? obj[7].toString() : "NA");
				lmoInfo.setDistrictname(obj[8] != null ? obj[8].toString() : "NA");
				lmoList.add(lmoInfo);
				
			}
			dto.setAadharDtlsVO(lmoList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}

	

}
