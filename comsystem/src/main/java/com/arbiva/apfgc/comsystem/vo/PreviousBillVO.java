/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class PreviousBillVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String billNumber;

	private String billDate;

	private String billPeriod;

	private String paymentDueDate;

	private String previousBalence;

	private String lastPaymentAmount;
	
	private String balenceAmount;

	private String adjustmentsAmount;
	
	private String currentBillAmount;

	private String payableAmount;

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillPeriod() {
		return billPeriod;
	}

	public void setBillPeriod(String billPeriod) {
		this.billPeriod = billPeriod;
	}

	public String getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getPreviousBalence() {
		return previousBalence;
	}

	public void setPreviousBalence(String previousBalence) {
		this.previousBalence = previousBalence;
	}

	public String getLastPaymentAmount() {
		return lastPaymentAmount;
	}

	public void setLastPaymentAmount(String lastPaymentAmount) {
		this.lastPaymentAmount = lastPaymentAmount;
	}

	public String getBalenceAmount() {
		return balenceAmount;
	}

	public void setBalenceAmount(String balenceAmount) {
		this.balenceAmount = balenceAmount;
	}

	public String getAdjustmentsAmount() {
		return adjustmentsAmount;
	}

	public void setAdjustmentsAmount(String adjustmentsAmount) {
		this.adjustmentsAmount = adjustmentsAmount;
	}

	public String getCurrentBillAmount() {
		return currentBillAmount;
	}

	public void setCurrentBillAmount(String currentBillAmount) {
		this.currentBillAmount = currentBillAmount;
	}

	public String getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}
	
}
