/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.MultiAction;

/**
 * @author kiran
 *
 */
public class SearchDataVO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private PageObject pageObject;
	
	private MultiAction multiAction;

	public PageObject getPageObject() {
		return pageObject;
	}

	public void setPageObject(PageObject pageObject) {
		this.pageObject = pageObject;
	}

	public MultiAction getMultiAction() {
		return multiAction;
	}

	public void setMultiAction(MultiAction multiAction) {
		this.multiAction = multiAction;
	}
	
	
}
