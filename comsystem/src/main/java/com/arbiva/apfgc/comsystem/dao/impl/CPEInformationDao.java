package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.BO.CpeChargeDetailsBO;
import com.arbiva.apfgc.comsystem.BO.SubstationsBO;
import com.arbiva.apfgc.comsystem.BO.TenantsBO;
import com.arbiva.apfgc.comsystem.BO.VillagesBO;
import com.arbiva.apfgc.comsystem.model.CpeModal;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.FeatureParams;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.model.OLT;
import com.arbiva.apfgc.comsystem.model.SrvcFeatures;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.Villages;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;

@Repository
public class CPEInformationDao {

private static final Logger logger = Logger.getLogger(CPEInformationDao.class);
	
	private EntityManager em;
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	private EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	public List<Districts> findAllDistricts() {
		return (List<Districts>)getEntityManager().createQuery("Select districts from " + Districts.class.getSimpleName() + " districts order by districtname").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Mandals> getAllMandals() {
		return (List<Mandals>)getEntityManager().createQuery("Select mandals from " + Mandals.class.getSimpleName() + " mandals order by mandalname").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<OLT> getAllOLTs() {
		return (List<OLT>)getEntityManager().createQuery("Select olt from " + OLT.class.getSimpleName() + " olt").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getAllSubStations(String subStationCodes) {
		List<Object[]> SubstationsList = new ArrayList<>();
		StringBuilder builder = null;
		Query query = null;
		try {
			logger.info("START::getAllSubStations()");
			builder = new StringBuilder("SELECT substnuid, substnname, districtuid, mandalslno FROM substations where substnuid in("+subStationCodes+")");
			query = getEntityManager().createNativeQuery(builder.toString());
			SubstationsList = query.getResultList();
			logger.info("END::getAllSubStations()");
		} catch (Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllSubStations()" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return SubstationsList;
	}
	
	public boolean check_SubstUid_OltSrNo_VPNSrNm(String subStationCodes, String oltSrNo, String VPNSrName) {
		StringBuilder builder = null;
		Query query = null;
		boolean status = false;
		try {
			logger.info("START::getAllSubStations()");
			builder = new StringBuilder("SELECT * FROM vpnsrvcnames where substnuid='"+subStationCodes+"' and olt_serialno='"+oltSrNo+"' and vpnsrvcname='"+VPNSrName+"'");
			query = getEntityManager() .createNativeQuery(builder.toString());
			if(query.getResultList().isEmpty()) {
				status = true; 
			}
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: check_SubstUid_OltSrNo_VPNSrNm()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return status;
	}
	
	public String getCpeModelByProfileId(Integer profileId) {
		String cpeModel = ""; 
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT cpe_model FROM cpe_profilemaster where profile_id = '"+profileId+"' ");
		try {
			query = getEntityManager() .createNativeQuery(builder.toString());
			cpeModel = (String) query.getSingleResult();
		} catch(Exception ex){
			logger.error("EXCEPTION::getCpeModelByProfileId() " + ex);
		}finally{
			query = null;
			builder = null;
		}
		return cpeModel;
	}
	
	@SuppressWarnings("unchecked")
	public List<CpeModal> getAllCpeModals() {
		List<CpeModal> cpeModalsList = null; 
		try {
			cpeModalsList =  (List<CpeModal>)getEntityManager().createQuery("Select cpeModal from " + CpeModal.class.getSimpleName() + " cpeModal").getResultList();
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllCpeModals()" +e);
			e.printStackTrace();
		}
		return cpeModalsList;
	}
	
	public List<CpeModal> getAllCpeModalsByCpeType(String cpeType) {
		List<CpeModal> cpeModalList = new ArrayList<CpeModal>();
		StringBuilder builder = new StringBuilder(" FROM ").append(CpeModal.class.getSimpleName()).append(" WHERE cpetypelov=:cpeType order by cpe_model ");
		TypedQuery<CpeModal> query = null;
			try {
				logger.info("START::getAllCpeModalsByCpeType()");
				query = getEntityManager().createQuery(builder.toString(), CpeModal.class);
				query.setParameter("cpeType", cpeType);
				cpeModalList = query.getResultList();
				logger.info("END::getAllCpeModalsByCpeType()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllCpeModalsByCpeType() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return cpeModalList;
	}

	public List<Mandals> getMandalsByDistrictId(Integer districtId) {
		List<Mandals> mandalsList = new ArrayList<Mandals>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Mandals.class.getSimpleName()).append(" WHERE districtuid=:districtId order by mandalname ");
		TypedQuery<Mandals> query =  null;
			try {
				logger.info("START::getMandalsByDistrictId()");
				query = getEntityManager().createQuery(builder.toString(), Mandals.class);
				query.setParameter("districtId", districtId);
				mandalsList = query.getResultList();
				logger.info("END::getMandalsByDistrictId()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getMandalsByDistrictId() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return mandalsList;
	}
	
	public Mandals getMandalsByDistrictIdAndMandalSrlNo(Integer districtId, Integer mandalSrlNo) {
		Mandals mandals = new Mandals();
		StringBuilder builder = new StringBuilder(" FROM ").append(Mandals.class.getSimpleName()).append(" WHERE districtuid =:districtId and mandalslno =:mandalSrlNo ");
		TypedQuery<Mandals> query = null;
			try {
				logger.info("START::getMandalsByDistrictIdAndMandalSrlNo()");
				query = getEntityManager().createQuery(builder.toString(), Mandals.class);
				query.setParameter("districtId", districtId);
				query.setParameter("mandalSrlNo", mandalSrlNo);
				mandals = query.getSingleResult();
				logger.info("END::getMandalsByDistrictIdAndMandalSrlNo()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getMandalsByDistrictIdAndMandalSrlNo() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return mandals;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getSubstationsByDistrictIdAndMandalId(Integer districtId, Integer mandalId) {
		List<Object[]> substationsList = null; 
		StringBuilder builder = new StringBuilder(" select substnuid, substnname from substations where districtUid = "+districtId+" and mandalslno = "+mandalId+" order by substnname");
		Query query = null;
		try {
			logger.info("START::getSubstationsByDistrictIdAndMandalId()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			substationsList = query.getResultList();
			logger.info("END::getSubstationsByDistrictIdAndMandalId()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getSubstationsByDistrictIdAndMandalId()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return substationsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<OLT> getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno(Integer districtId, Integer mandalId, Integer subStnSlno) {
		List<OLT> oltsList = null; 
		StringBuilder builder = new StringBuilder(" select pop_id, pop_olt_serialno from oltmaster "
				+ "where pop_district = "+districtId+" and pop_mandal = "+mandalId+" and pop_substnslno = "+subStnSlno+" order by pop_olt_serialno");
		try {
			logger.info("START::getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno()");
			Query query = getEntityManager() .createNativeQuery(builder.toString());
			oltsList = query.getResultList();
			logger.info("END::getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTSLNOByDistrictIdAndMandalIdAndSubstationsrlno()" +e);
			e.printStackTrace();
		}finally{
			builder = null;
		}
		return oltsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getOLTSLNOBySubstationsrlno(String subStnSlno) {
		List<Object[]> oltsList = new ArrayList<>(); 
		StringBuilder builder = new StringBuilder(" select pop_oltlabelno, pop_olt_serialno, pop_id, pop_name, pop_substnuid, pop_olt_ipaddress, oltaccessnode from oltmaster "
				+ " where pop_substnuid in("+subStnSlno+") order by pop_olt_serialno");
		try {
			logger.info("START::getOLTSLNOBySubstationsrlno()");
			Query query = getEntityManager() .createNativeQuery(builder.toString());
			oltsList = query.getResultList();
			logger.info("END::getOLTSLNOBySubstationsrlno()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTSLNOBySubstationsrlno()" +e);
			e.printStackTrace();
		}finally{
			builder = null;
		}
		return oltsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getOLTPopIdByOltSrlNo(String oltSrlNo, String lmoCode) {
		List<Object[]> oltPortIdsList = new ArrayList<>(); 
		StringBuilder builder = new StringBuilder(" select pop_olt_serialno, portno from oltportdtls  where pop_olt_serialno = '"+oltSrlNo+"' and lmocode='"+lmoCode+"' and slots <> REPEAT('1', 128) order by portno");
		try {
			logger.info("START::getOLTPopIdByOltSrlNo()");
			Query query = getEntityManager() .createNativeQuery(builder.toString());
			oltPortIdsList = query.getResultList();
			logger.info("END::getOLTPopIdByOltSrlNo()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTPopIdByOltSrlNo()" +e);
			e.printStackTrace();
		}finally{
			builder = null;
		}
		return oltPortIdsList;
	}
	
	public String getCpeModelNameByProfileId(Integer profileId) {
		String cpeModelName = null; 
		StringBuilder builder = new StringBuilder(" select cpe_model from cpe_profilemaster where profile_id = "+profileId+" ");
		Query query = null;
		try {
			logger.info("START::getCpeModelNameByProfileId()");
			query = getEntityManager() .createNativeQuery(builder.toString());
			cpeModelName = query.getSingleResult().toString();
			logger.info("END::getCpeModelNameByProfileId()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getCpeModelNameByProfileId()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return cpeModelName;
	}

	public Cpecharges getAlCpeChargesByCpeModel(String serialNumber) {
		Cpecharges cpecharges = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			logger.info("START::getAlCpeChargesByCpeModel()");
			builder.append(" select cp.profile_id, cp.effectivefrom, cp.effectiveto, cp.custcost, cp.custrent, cp.purchasecost, cp.instcharges, cp.emicount, cp.emiamt, ");
			builder.append(" cp.upfrontcharges from cpecharges cp, cpestock cs where cp.profile_id = cs.profile_id and ifnull(cs.batchdate, current_date()) between ");
			builder.append(" cp.effectivefrom and cp.effectiveto and cs.cpeslno = '"+serialNumber+"' ");
			query = getEntityManager().createNativeQuery(builder.toString(), Cpecharges.class);
			cpecharges = (Cpecharges) query.getSingleResult();
			logger.info("END::getAlCpeChargesByCpeModel()");
		} catch (Exception e) {
			logger.error("EXCEPTION::getAlCpeChargesByCpeModel() " + e);
		}finally{
			query = null;
			builder = null;
		}
	  return cpecharges;
	}

	@SuppressWarnings("unchecked")
	public List<Cpecharges> getAlCpeCharges() {
		List<Cpecharges> cpechargesList = null; 
		try {
			cpechargesList =  (List<Cpecharges>)getEntityManager().createQuery("Select cpecharges from " + Cpecharges.class.getSimpleName() + " cpecharges").getResultList();
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAlCpeCharges()" +e);
			e.printStackTrace();
		}finally{
			
		}
		return cpechargesList;
	}

	public List<Villages> getAllVillages(String subStationCodes) {
		List<Villages> villagesList = new ArrayList<>(); 
		StringBuilder builder = null;
		TypedQuery<Villages> query = null;
		try {
			logger.info("START::getAllVillages()");
			builder = new StringBuilder(" FROM ").append(Villages.class.getSimpleName()).append(" WHERE substnuid in("+subStationCodes+") order by villagename ");
			query = getEntityManager().createQuery(builder.toString(), Villages.class);
			//query.setParameter("subStationCodes", subStationCodes);
			//StringBuilder builder = new StringBuilder("SELECT * FROM villages where substnuid in("+subStationCodes+")");
			//Query query = getEntityManager() .createNativeQuery(builder.toString());
			villagesList = query.getResultList();
			logger.info("END::getAllVillages()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllVillages()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return villagesList;
	}

	public List<Villages> getVillagesByDistrictAndMandal(Integer districtId, Integer mandalId) {
		List<Villages> VillagesList = new ArrayList<Villages>();
		TypedQuery<Villages> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Villages.class.getSimpleName()).append(" WHERE districtuid =:districtId and mandalslno =:mandalId order by villagename ");
			try {
				logger.info("START::getVillagesByDistrictAndMandal()");
				query = getEntityManager().createQuery(builder.toString(), Villages.class);
				query.setParameter("districtId", districtId);
				query.setParameter("mandalId", mandalId);
				VillagesList = query.getResultList();
				logger.info("END::getVillagesByDistrictAndMandal()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getVillagesByDistrictAndMandal() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return VillagesList;
	}

	public Villages getVillagesByVillageId(Integer villageSlno, Integer mandalSlno, Integer districtUid) {
		Villages Villages = new Villages();
		StringBuilder builder = new StringBuilder(" FROM ").append(Villages.class.getSimpleName()).append(" WHERE villageslno =:villageSlno and mandalslno =:mandalSlno and districtuid =:districtUid ");
		TypedQuery<Villages> query = null;
			try {
				logger.info("START::getVillagesByDistrictAndMandal()");
				query = getEntityManager().createQuery(builder.toString(), Villages.class);
				query.setParameter("villageSlno", villageSlno);
				query.setParameter("mandalSlno", mandalSlno);
				query.setParameter("districtUid", districtUid);
				Villages = query.getSingleResult();
				logger.info("END::getVillagesByDistrictAndMandal()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getVillagesByDistrictAndMandal() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return Villages;
	}
	
	@SuppressWarnings("unchecked")
	public List<FeatureParams> getAllFeatureParams() {
		List<FeatureParams> featureParamsList = new ArrayList<>(); 
		try {
			featureParamsList =  (List<FeatureParams>)getEntityManager().createQuery("Select featureParams from " + FeatureParams.class.getSimpleName() + " featureParams").getResultList();
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAlCpeCharges()" +e);
			e.printStackTrace();
		}finally{
			
		}
		return featureParamsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SrvcFeatures> getAllSrvcFeatures() {
		List<SrvcFeatures> srvcFeaturesList = new ArrayList<>(); 
		try {
			srvcFeaturesList =  (List<SrvcFeatures>)getEntityManager().createQuery("Select srvcFeatures from " + SrvcFeatures.class.getSimpleName() + " srvcFeatures").getResultList();
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAlCpeCharges()" +e);
			e.printStackTrace();
		}finally{
			
		}
		return srvcFeaturesList;
	}

	public CpeChargeDetailsBO getCpeAndIptvbosSrlNoCheck(String cpeId, String lmoCode, String lmoAgrmntMspCodes,String cpetypelov) {
		CpeChargeDetailsBO cpeChargeDetailsBO = new CpeChargeDetailsBO();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			logger.info("START::getCpeAndIptvbosSrlNoCheck()");
			builder.append(" select cpe.cpe_model, cp.profile_id, cp.instcharges, cp.emicount, cp.emiamt, cp.upfrontcharges, cs.cpemacaddr ");
			builder.append(" from cpecharges cp, cpestock cs, cpe_profilemaster cpe where cp.profile_id = cs.profile_id  and cpe.profile_id = cs.profile_id  ");
			builder.append(" and cp.profile_id = cpe.profile_id and ifnull(cs.batchdate, current_date()) between  ");
			builder.append(" cp.effectivefrom and cp.effectiveto and cs.cpeslno = '"+cpeId+"' ");
			builder.append(" and ((cs.lmocode = '"+lmoCode+"' and cs.status = 3) OR (cs.mspcode in("+lmoAgrmntMspCodes+") and cs.status = 2)) and cpe.cpetypelov = '"+cpetypelov+"' ");
			query = getEntityManager() .createNativeQuery(builder.toString(), CpeChargeDetailsBO.class);
			cpeChargeDetailsBO = (CpeChargeDetailsBO) query.getSingleResult();
			logger.info("END::getCpeAndIptvbosSrlNoCheck()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getCpeAndIptvbosSrlNoCheck()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return cpeChargeDetailsBO;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllCpeStockByLmoCode(String lmoCode) {
		List<Object[]> cpeStockList = new ArrayList<>(); 
		StringBuilder builder = null;
		Query query = null;
		try {
			logger.info("START::getAllCpeStockByLmoCode()");
			builder = new StringBuilder("SELECT cpeslno, profile_id, mspcode, lmocode, cpemacaddr FROM cpestock where lmocode = '"+lmoCode+"' and status = 3 ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			cpeStockList = query.getResultList();
			logger.info("END::getAllCpeStockByLmoCode()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllCpeStockByLmoCode()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return cpeStockList;
	}
	
	public Object[] getCafDetailsForEdit(Long cafNo, String status) {
		Object[] custcafData = null;
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::getCafDetailsForEdit()");
			builder = new StringBuilder("SELECT cf.cafno, ct.aadharno, ct.titlelov, ct.fname, ct.mname, ct.lname, ");
			builder.append(" ct.fhname, ct.actualdob, ct.gender,ct.email1,ct.pocName, ct.billfreqLov, ");
			builder.append("cf.inst_addr1, cf.inst_addr2, cf.inst_locality, cf.inst_district, cf.inst_mandal, cf.inst_city_village, cf.inst_pin, ");
			builder.append("ct.pocMob1, ct.pocMob2, ct.stdCode, ct.landLine1, cf.longitude, cf.lattitude, cf.cpeplace, ct.status, ct.custid, cf.pop_substnno, cf.status as cafstatus, ");
			builder.append(" cf.pop_district, cf.pop_mandal, (select d.districtname from districts d where d.districtuid = cf.pop_district) as popdistrict, (select m.mandalname from mandals m where m.districtuid = cf.pop_district and m.mandalslno = cf.pop_mandal) as popmandal, ");
			builder.append(" (select s.substnname from substations s where cf.pop_substnno = s.substnuid) substnname, (select d.districtname from districts d where d.districtuid = cf.inst_district) as instdistrict, ");
			builder.append(" (select m.mandalname from mandals m where m.districtuid = cf.inst_district and m.mandalslno = cf.inst_mandal) as instmandal, ");
			builder.append(" (select v.villagename from villages v where v.districtuid = cf.inst_district and cf.inst_mandal = v.mandalslno and v.villageslno = cf.inst_city_village) villagename, cf.apsfluniqueid, cf.contactperson, cf.contactemail, cf.contactmob, ct.custtypelov ");
			builder.append(" FROM cafs cf , customers ct WHERE cf.cafno = "+cafNo+" AND cf.custid = ct.custid and cf.custtypelov = '"+ComsEnumCodes.CUST_TYPE_CODE.getCode()+"' ");
			builder.append(" UNION SELECT cf.cafno, ct.regncode, 'Ms.' as titlelov, ct.custname, '' as mname, '' as lname,  ");
			builder.append(" '' as fhname, ct.dateofinc, '' as gender, ct.email1, ct.pocName, ct.billfreqLov, cf.inst_addr1, cf.inst_addr2, cf.inst_locality, ");
			builder.append(" cf.inst_district, cf.inst_mandal, cf.inst_city_village, cf.inst_pin, ct.pocMob1, ct.pocMob2, ct.stdCode, ct.landLine1, cf.longitude, ");
			builder.append(" cf.lattitude, cf.cpeplace, ct.status, ct.custid, cf.pop_substnno, cf.status as cafstatus, ");
			builder.append(" cf.pop_district, cf.pop_mandal, (select d.districtname from districts d where d.districtuid = cf.pop_district) as popdistrict, (select m.mandalname from mandals m where m.districtuid = cf.pop_district  and m.mandalslno = cf.pop_mandal) as popmandal, ");
			builder.append(" (select s.substnname from substations s where cf.pop_substnno = s.substnuid) substnname, (select d.districtname from districts d where d.districtuid = cf.inst_district) as instdistrict, ");
			builder.append(" (select m.mandalname from mandals m where m.districtuid = cf.inst_district and m.mandalslno = cf.inst_mandal) as instmandal, ");
			builder.append(" (select v.villagename from villages v where v.districtuid = cf.inst_district and cf.inst_mandal = v.mandalslno and v.villageslno = cf.inst_city_village) villagename, cf.apsfluniqueid, cf.contactperson, cf.contactemail, cf.contactmob, ct.enttypelov ");
			builder.append(" FROM cafs cf , entcustomers ct WHERE cf.cafno = "+cafNo+" AND cf.custid = ct.custid and cf.custtypelov = '"+ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()+"' ");

			query = getEntityManager().createNativeQuery(builder.toString());
			custcafData = (Object[]) query.getSingleResult();
			logger.info("END::getAllCpeStockByLmoCode()");
		} catch (Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllCpeStockByLmoCode()" + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return custcafData;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllCpeStockByMspCode(String mspCode) {
		List<Object[]> cpeStockList = new ArrayList<>();
		StringBuilder builder = null;
		Query query = null;
		try {
			logger.info("START::getAllCpeStockByMspCode()");
			builder = new StringBuilder("SELECT cpeslno, profile_id, mspcode, lmocode, cpemacaddr FROM cpestock where mspcode in("+mspCode+") and status = 2 ");
			query = getEntityManager().createNativeQuery(builder.toString());
			cpeStockList = query.getResultList();
			logger.info("END::getAllCpeStockByMspCode()");
		} catch (Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllCpeStockByMspCode()" + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return cpeStockList;
	}

	public Districts getDistrictByDistrictId(Integer districtId) {
		Districts districts = new Districts();
		StringBuilder builder = new StringBuilder(" FROM ").append(Districts.class.getSimpleName()).append(" WHERE districtuid =:districtId ");
		TypedQuery<Districts> query = null;
			try {
				logger.info("START::getDistrictByDistrictId()");
				query = getEntityManager().createQuery(builder.toString(), Districts.class);
				query.setParameter("districtId", districtId);
				districts = query.getSingleResult();
				logger.info("END::getDistrictByDistrictId()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getDistrictByDistrictId() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return districts;
	}

	public String getAllVillagesByTenantCode(String tenantCode) {
		String villages = ""; 
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::getAllSubStations()");
			builder = new StringBuilder("SELECT villageids FROM tenants where tenantcode = '"+tenantCode+"'");
			query = getEntityManager() .createNativeQuery(builder.toString());
			villages = query.getSingleResult().toString();
			logger.info("END::getAllSubStations()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllVillagesByTenantCode()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return villages;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllVillagesBySubstnId(String subStnSlno, String tenantCode) {
		List<Object[]> villagesList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::getAllVillagesBySubstnId()");
			builder = new StringBuilder("SELECT t.districtuid, t.mandalslno, t.villageslno, t.substnuid, v.villagename, m.mandalname, d.districtname, v.enttax_zone, v.stdcode FROM tenantbusareas t, ");  
			builder.append("villages v,  mandals m, districts d where t.substnuid in("+subStnSlno+") and t.tenantcode = '"+tenantCode+"' and t.districtuid = v.districtuid and t.mandalslno = v.mandalslno and t.villageslno = v.villageslno ");
			builder.append("and t.villageslno = v.villageslno and t.districtuid = m.districtuid and t.mandalslno = m.mandalslno and t.districtuid = d.districtuid ");
			query = getEntityManager().createNativeQuery(builder.toString());
			villagesList = query.getResultList();
			logger.info("END::getAllVillagesBySubstnId()");
		} catch (Exception e) {
			logger.error("The Exception is CPEInformationDao :: getAllVillagesBySubstnId()" + e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return villagesList;
	}

	public String getOLTPortSplitterData(String oltSrlNo, String lmoCode, Integer oltPort) {
		String splitterData = ""; 
		Query query = null;
		StringBuilder builder = null;
		try {
			logger.info("START::getOLTPortSplitterData()");
			builder = new StringBuilder("SELECT l1slots FROM oltportdtls where pop_olt_serialno = '"+oltSrlNo+"'");
			if(lmoCode!=null && !"".equalsIgnoreCase(lmoCode.trim()))
			{
				builder.append(" and lmocode = '"+lmoCode+"' ");
			}
			builder.append(" and portno ='"+oltPort+"'");
			query = getEntityManager() .createNativeQuery(builder.toString());
			splitterData = query.getSingleResult().toString();
			logger.info("END::getOLTPortSplitterData()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTPortSplitterData()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return splitterData;
	}

	public List<VPNSrvcNames> getVPNServicesByPopIdAndOltSrlNo(String oltSrlNo, String subStnId) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>(); 
		TypedQuery<VPNSrvcNames> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(VPNSrvcNames.class.getSimpleName()).append(" WHERE substnuid =:subStnId and olt_serialno =:oltSrlNo order by vpnsrvcname ");
			try {
				logger.info("START :: getVPNServicesByPopIdAndOltSrlNo()");
				query = getEntityManager().createQuery(builder.toString(), VPNSrvcNames.class);
				query.setParameter("oltSrlNo", oltSrlNo);
				query.setParameter("subStnId", subStnId);
				vpnSrvcNamesList = query.getResultList();
				logger.info("END :: getVPNServicesByPopIdAndOltSrlNo()");
			} catch (Exception e) {
				logger.error("EXCEPTION :: getVPNServicesByPopIdAndOltSrlNo() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return vpnSrvcNamesList;
	}

	public List<OLT> getOLTSLNOAndSubstationsrlno(String substnUid, String oltSerialNo) {
		List<OLT> oltList = new ArrayList<>(); 
		TypedQuery<OLT> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(OLT.class.getSimpleName()).append(" WHERE pop_substnuid =:subStnId and pop_olt_serialno =:oltSrlNo ");
			try {
				logger.info("START :: getOLTSLNOAndSubstationsrlno()");
				query = getEntityManager().createQuery(builder.toString(), OLT.class);
				query.setParameter("oltSrlNo", oltSerialNo);
				query.setParameter("subStnId", substnUid);
				oltList = query.getResultList();
				logger.info("END :: getOLTSLNOAndSubstationsrlno()");
			} catch (Exception e) {
				logger.error("EXCEPTION :: getOLTSLNOAndSubstationsrlno() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return oltList;
	}

	public List<VPNSrvcNames> getLMOVPNSrvcNamesList(String substnCode) {
		List<VPNSrvcNames> vpnSrvcNamesList = new ArrayList<>(); 
		TypedQuery<VPNSrvcNames> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(VPNSrvcNames.class.getSimpleName()).append(" WHERE substnuid in("+substnCode+") order by vpnsrvcname ");
			try {
				logger.info("START :: getLMOVPNSrvcNamesList()");
				query = getEntityManager().createQuery(builder.toString(), VPNSrvcNames.class);
				vpnSrvcNamesList = query.getResultList();
				logger.info("END :: getLMOVPNSrvcNamesList()");
			} catch (Exception e) {
				logger.error("EXCEPTION :: getLMOVPNSrvcNamesList() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return vpnSrvcNamesList;
	}

	@SuppressWarnings("unchecked")
	public List<String> getSILMODistricts(String tenantcode) {
		Query query = null;
		StringBuilder builder = null;
		List<String> districtsList = new ArrayList<>();
		try {
			logger.info("START::getOLTPortSplitterData()");
			builder = new StringBuilder("SELECT distinct SUBSTRING_INDEX(inst_addr1,',',1) district  FROM cafs WHERE status = "+ComsEnumCodes.Caf_BulkUpload_status.getStatus()+" and lmocode = '"+tenantcode+"' order by district ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			districtsList = query.getResultList();
			logger.info("END::getOLTPortSplitterData()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTPortSplitterData()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return districtsList;
	}

	@SuppressWarnings("unchecked")
	public List<String> getSILMOMandals(String tenantcode, String district) {
		Query query = null;
		StringBuilder builder = null;
		List<String> mandalList = new ArrayList<>();
		try {
			logger.info("START::getOLTPortSplitterData()");
			builder = new StringBuilder("SELECT distinct SUBSTRING_INDEX(inst_addr1,',',-1) mandal  FROM cafs WHERE status = "+ComsEnumCodes.Caf_BulkUpload_status.getStatus()+" and lmocode = '"+tenantcode+"' AND SUBSTRING_INDEX(inst_addr1,',',1) = '"+district+"' order by mandal ");
			query = getEntityManager() .createNativeQuery(builder.toString());
			mandalList = query.getResultList();
			logger.info("END::getOLTPortSplitterData()");
		} catch(Exception e) {
			logger.error("The Exception is CPEInformationDao :: getOLTPortSplitterData()" +e);
			e.printStackTrace();
		}finally{
			query = null;
			builder = null;
		}
		return mandalList;
	}

	public Cpecharges getCpeEmiCountByCpeModel(Long profileId) {
		List<Cpecharges> cpechargesList = new ArrayList<>();
		Cpecharges cpecharges = new Cpecharges();
		TypedQuery<Cpecharges> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Cpecharges.class.getSimpleName()).append(" WHERE profile_id = "+profileId+" and current_date() between effectivefrom and effectiveto order by emicount ");
			try {
				logger.info("START :: getCpeEmiCountByCpeModel()");
				query = getEntityManager().createQuery(builder.toString(), Cpecharges.class);
				cpechargesList = query.getResultList();
				if(cpechargesList.size() > 0) {
					cpecharges = cpechargesList.get(0);
				}
				logger.info("END :: getCpeEmiCountByCpeModel()");
			} catch (Exception e) {
				logger.error("EXCEPTION :: getCpeEmiCountByCpeModel() " + e);
			} finally{
				query = null;
				builder = null;
				cpechargesList = null;
			}
		  return cpecharges;
	}
	
	@SuppressWarnings("unchecked")
	public List<VillagesBO> getAllVillagesByDistrictId(String districtId) {
		List<VillagesBO> villagesList = new ArrayList<VillagesBO>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;
			try {
				logger.info("START::getAllVillagesByDistrictId()");
				builder.append("select v.villageuid, v.villageslno, v.villagename, v.districtuid, d.districtname, v.mandalslno, m.mandalname, v.enttax_zone ");
				builder.append(" From villages v, mandals m, districts d where  m.districtuid = v.districtuid and m.mandalslno = v.mandalslno ");
				builder.append(" and d.districtuid = v.districtuid and v.districtuid in("+districtId+") order by v.villageuid ");
				query = getEntityManager().createNativeQuery(builder.toString(), VillagesBO.class);
				villagesList = query.getResultList();
				logger.info("END::getAllVillagesByDistrictId()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllVillagesByDistrictId() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return villagesList;
	}

	@SuppressWarnings("unchecked")
	public List<SubstationsBO> getAllSubstationsByDistrictId(String districtId) {
		List<SubstationsBO> substationsList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;
			try {
				logger.info("START::getAllSubstationsByDistrictId()");
				builder.append(" select a.substnuid, a.substnname, a.districtuid, a.mandalslno,a.oltSrlno,b.vpnsrvcname ");
				builder.append(" from ( select s.substnuid, s.substnname, s.districtuid, s.mandalslno, GROUP_CONCAT(o.pop_olt_serialno,'-',o.pop_oltlabelno) oltSrlno ");
				builder.append(" from substations s,oltmaster o  WHERE o.pop_substnuid = s.substnuid ");
				builder.append(" and exists (select 1 from oltportdtls p where p.pop_olt_serialno=o.pop_olt_serialno AND p.lmocode='APSFL' AND p.slots <> REPEAT('1',128)) ");
				builder.append(" group by  s.substnuid, s.substnname, s.districtuid, s.mandalslno) a LEFT JOIN " );
				builder.append(" (select s1.substnuid, s1.substnname, s1.districtuid, s1.mandalslno, GROUP_CONCAT(o1.pop_substnuid,'^',o1.pop_olt_serialno,'^',IFNULL(v1.vpnsrvcname,'')) vpnsrvcname ");
				builder.append(" from substations s1,oltmaster o1,vpnsrvcnames v1 ");
				builder.append("  WHERE o1.pop_substnuid = s1.substnuid and v1.substnuid = s1.substnuid and o1.pop_olt_serialno=v1.olt_serialno ");
				builder.append("  and exists (select 1 from oltportdtls p1 where p1.pop_olt_serialno=o1.pop_olt_serialno AND p1.lmocode='APSFL' AND p1.slots <> REPEAT('1',128)) ");
				builder.append("  group by  s1.substnuid, s1.substnname, s1.districtuid, s1.mandalslno) b ON a.substnuid=b.substnuid ");
				builder.append(" where a.districtuid in ("+districtId+")  ");
				query = getEntityManager().createNativeQuery(builder.toString(), SubstationsBO.class);
				substationsList = query.getResultList();
				logger.info("END::getAllSubstationsByDistrictId()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllSubstationsByDistrictId() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return substationsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllOLTPortDetailsByTenantCode(String tenantCode) {
		List<Object[]> oltPortDetailsList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;
			try {
				logger.info("START::getAllOLTPortDetailsByTenantCode()");
				builder.append(" select pop_olt_serialno, portno, lmocode from oltportdtls where lmocode = '"+tenantCode+"' and slots <> REPEAT('1',128) ");
				query = getEntityManager().createNativeQuery(builder.toString());
				oltPortDetailsList = query.getResultList();
				logger.info("END::getAllOLTPortDetailsByTenantCode()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllOLTPortDetailsByTenantCode() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return oltPortDetailsList;
	}

	@SuppressWarnings("unchecked")
	public OLT getOLTDataByOLTSrlNo(String oltId) {
		OLT olt = null;
		StringBuilder builder = new StringBuilder();
		Query query =  null;
			try {
				logger.info("START::getOLTDataByOLTSrlNo()");
				builder.append(" select pop_olt_ipaddress, oltaccessnode from oltmaster where pop_olt_serialno = '"+oltId+"' ");
				query = getEntityManager().createNativeQuery(builder.toString());
				List<Object[]> objectList = query.getResultList();
				if(objectList.size() > 0) {
					for(Object[] object : objectList) {
						olt = new OLT();
						olt.setPopOltIpaddress(object[0].toString());
						olt.setOltAccessNode(object[1].toString());
					}
				}
				logger.info("END::getOLTDataByOLTSrlNo()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getOLTDataByOLTSrlNo() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return olt;
	}

	@SuppressWarnings("unchecked")
	public List<VillagesBO> getAllVillages() {
		List<VillagesBO> villagesList = new ArrayList<VillagesBO>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;
			try {
				logger.info("START::getAllVillagesByDistrictId()");
				builder.append("select v.villageuid, v.villageslno, v.villagename, v.districtuid, d.districtname, v.mandalslno, m.mandalname, v.enttax_zone ");
				builder.append(" From villages v, mandals m, districts d where  m.districtuid = v.districtuid and m.mandalslno = v.mandalslno ");
				builder.append(" and d.districtuid = v.districtuid order by v.villageuid ");
				query = getEntityManager().createNativeQuery(builder.toString(), VillagesBO.class);
				villagesList = query.getResultList();
				logger.info("END::getAllVillages()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllVillages() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return villagesList;
	}

	@SuppressWarnings("unchecked")
	public List<TenantsBO> getAllLMOByDistrictAndMandalAndVillage(String district, String mandal, String village) {
		List<TenantsBO> tenantsList = new ArrayList<TenantsBO>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;
		String whereClause = "";
			try {
				logger.info("START::getAllLMOByDistrictAndMandalAndVillage()");
				if(district != null && !district.isEmpty() && mandal != null && !mandal.isEmpty() && village != null && !village.isEmpty()) {
					whereClause = " and tenanttypelov = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' and t.portal_districtid = "+district+" and t.portal_mandalid = "+mandal+" and t.portal_villageid = "+village+" ";
				} else if(district != null && !district.isEmpty() && mandal != null && !mandal.isEmpty()) {
					whereClause = " and tenanttypelov = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' and t.portal_districtid = "+district+" and t.portal_mandalid = "+mandal+" ";
				} else if(district != null && !district.isEmpty()) {
					whereClause = " and tenanttypelov = '"+ComsEnumCodes.LMO_Tenant_Type.getCode()+"' and t.portal_districtid = "+district+" ";
				}
				builder.append("SELECT tenantcode, tenantname, concat(regoff_addr1, ' ', regoff_addr2) address, regoff_email1, regoff_pocmob1, ");
				builder.append(" (SELECT GROUP_CONCAT(substnuid) from tenantbusareas tbs where tbs.tenantcode = t.tenantcode) substations, enttax_zone,  portal_villageid ");
				builder.append(" FROM tenants t, districts d, mandals m, villages v WHERE t.portal_mandalid = m.mandalslno  AND t.portal_districtid = m.districtuid ");
				builder.append(" AND t.portal_districtid = d.districtuid AND t.portal_districtid = v.districtuid AND t.portal_mandalid = v.mandalslno AND t.portal_villageid = v.villageslno "+whereClause+" ");
				query = getEntityManager().createNativeQuery(builder.toString(), TenantsBO.class);
				tenantsList = query.getResultList();
				logger.info("END::getAllLMOByDistrictAndMandalAndVillage()");
			} catch (Exception e) {
				logger.error("EXCEPTION::getAllLMOByDistrictAndMandalAndVillage() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return tenantsList;
	}
}
