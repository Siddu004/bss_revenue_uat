package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

public class PackWiseSrvcsVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String hsi;
	
	private String voip;
	
	private String iptv;

	public String getHsi() {
		return hsi;
	}

	public void setHsi(String hsi) {
		this.hsi = hsi;
	}

	public String getVoip() {
		return voip;
	}

	public void setVoip(String voip) {
		this.voip = voip;
	}

	public String getIptv() {
		return iptv;
	}

	public void setIptv(String iptv) {
		this.iptv = iptv;
	}


}
