/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.comsystem.dao.impl.TenantWalletDaoImpl;
import com.arbiva.apfgc.comsystem.model.TenantWallet;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.TenantWalletVO;

/**
 * @author Lakshman
 *
 */
@Component("tenantWalletService")
public class TenantWalletServiceImpl {
	
	private static final Logger logger = Logger.getLogger(TenantWalletServiceImpl.class);
	
	@Autowired
	TenantWalletDaoImpl tenantWalletDao;
	
	@Autowired
	HttpServletRequest httpServletRequest;

	public TenantWallet findByLmoCode(String lmoCode) {
		TenantWallet tenantWallet = new TenantWallet();
		try {
			tenantWallet = tenantWalletDao.findByLmoCode(lmoCode);
		} catch(Exception e) {
			logger.error("The Exception TenantWalletServiceImpl:: findByLmoCode" + e);
			e.printStackTrace();
		} finally {
			
		}
		return tenantWallet;
	}
	
	public void updateTenantWallet(PaymentVO paymentVO, String loginID) {
		TenantWallet tenantWallet = new TenantWallet();
		Float usedAmount = null;
		Float cpeReleasedAmount = null;
		try {
			tenantWallet = tenantWalletDao.findByLmoCode(paymentVO.getCustomerCafVO().getLmoCode());
			if(!ComsEnumCodes.SI_Tenant_Type.getCode().equalsIgnoreCase(paymentVO.getCustomerCafVO().getTenantType())) {
				usedAmount = tenantWallet.getUsedAmount();
				tenantWallet.setUsedAmount(usedAmount + paymentVO.getPaidAmount());
			}
			if(paymentVO.getCustomerCafVO().getFlag() != null && paymentVO.getCustomerCafVO().getFlag().equalsIgnoreCase("3")) {
				cpeReleasedAmount = tenantWallet.getCperelamt();
				tenantWallet.setCperelamt(cpeReleasedAmount + Float.parseFloat(paymentVO.getCpeReleasedCharge()));
			}
			tenantWallet.setModifiedBy(loginID);
			tenantWallet.setModifiedDate(Calendar.getInstance());
			tenantWallet.setModifiedIPAddress(paymentVO.getCustomerCafVO().getIpAddress() != null ? paymentVO.getCustomerCafVO().getIpAddress() : httpServletRequest.getRemoteAddr());
			tenantWalletDao.updateTenantWallet(tenantWallet);
		} catch(Exception e) {
			logger.error("TenantWalletServiceImpl::updateTenantWallet() " + e);
			e.printStackTrace();
		} finally {
			tenantWallet = null;
			usedAmount = null;
		}
	}

	public TenantWalletVO getTenantWalletDetails(String tenantCode) {
		TenantWalletVO tenantWalletVO = new TenantWalletVO();
		TenantWallet tenantWallet = new TenantWallet();
		Float walletAmount = null;
		Float usedAmount = null;
		Float creditAmount = null;
		Float pecentage = 0.0f;
		Float actualUserAmount = 0.0f;
		Float wallet = null;
		Float cpeblkamt = null;
		Float cperelamt = null;
		try {
			tenantWallet = tenantWalletDao.findByLmoCode(tenantCode);
			walletAmount = tenantWallet.getWalletAmount();
			usedAmount = tenantWallet.getUsedAmount();
			creditAmount = tenantWallet.getCrlimitAmount();
			cpeblkamt = tenantWallet.getCpeblkamt();
			cperelamt = tenantWallet.getCperelamt();
			if(walletAmount != null && usedAmount != null && creditAmount != null) {
				//pecentage = ((usedAmount - walletAmount)/creditAmount)*100;
				//actualUserAmount = (walletAmount + (creditAmount*0.9f)) - usedAmount;
				//actualUserAmount=(walletAmount + creditAmount + cperelamt - usedAmount - cpeblkamt);
				actualUserAmount=(walletAmount + creditAmount);
			}
			wallet = walletAmount;
			
			tenantWalletVO.setCreditAmount(creditAmount);
			tenantWalletVO.setPecentage(pecentage);
			tenantWalletVO.setWalletAmount(wallet);
			tenantWalletVO.setActualUserAmount(actualUserAmount);
			tenantWalletVO.setUsedAmount(usedAmount);
		} catch(Exception e) {
			logger.error("TenantWalletServiceImpl::getTenantWalletDetails() " + e);
			e.printStackTrace();
		} finally {
			tenantWallet = null;
			walletAmount = null;
			usedAmount = null;
			creditAmount = null;
			pecentage = null;
			actualUserAmount = null;
			wallet = null;
		}
		return tenantWalletVO;
	}

	public String getLMOsubstations(String tenantCode) {
		Set<String> subStnCodeList = new HashSet<>();
		StringBuilder subStnCodes = new StringBuilder();
		List<String> substationList = new ArrayList<>();
		try {
			substationList = tenantWalletDao.getLMOsubstations(tenantCode);
			for(String subCode : substationList) {
				subStnCodeList.add(subCode);
			}
			for(String subCode : subStnCodeList) {
				subStnCodes.append(subCode).append(",");
			}
		} catch(Exception e) {
			logger.error("TenantWalletServiceImpl::getLMOsubstations() " + e);
			e.printStackTrace();
		} finally {
			subStnCodeList = null;
			substationList = null;
		}
		return subStnCodes.toString();
	}

	public void updateCustInv(PaymentVO paymentVO) {
		// TODO Auto-generated method stub
		try {
			tenantWalletDao.updateCustinv(paymentVO);
			// tenantWalletDao.updateCustinvPrepaidamnt(paymentVO);
			
		}catch(Exception e) {
			
		}
		
	}
}
