/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.StatusCodes;

/**
 * @author Lakshman
 *
 */
@Repository
public class StatusCodesDaoImpl {
	
	private static final Logger LOGGER = Logger.getLogger(BillCycleDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<StatusCodes> findStatusCodesName() {
		List<StatusCodes> statusCodes = new ArrayList<StatusCodes>();
		StringBuilder builder = new StringBuilder(" FROM ").append(StatusCodes.class.getSimpleName()).append(" WHERE appcode ='COMS'");
		TypedQuery<StatusCodes> query = null;
		try {
			LOGGER.info("START::findStatusCodesName()");
			query = getEntityManager().createQuery(builder.toString(), StatusCodes.class);
			statusCodes = query.getResultList();
			LOGGER.info("END::findStatusCodesName()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findStatusCodesName() " + e);
		}finally{
			query = null;
			builder = null;
		}
		return statusCodes;
	}
}
