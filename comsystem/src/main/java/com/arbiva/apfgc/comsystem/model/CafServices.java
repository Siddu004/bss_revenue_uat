/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="cafsrvcs")
@IdClass(CafServicesPK.class)
public class CafServices extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cafno")
	private Long cafno;
	
	@Id
	@Column(name = "srvccode")
	private String srvcCode;
	
	@Id
	@Column(name = "stbcafno")
	private Long stbCafNo;
	
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Column(name = "parentcafno")
	private Long parentCafno;
	
	@Column(name = "featurecodes")
	private String featureCodes;
	
	@Column(name = "prodcode")
	private String prodCode;
	
	@Column(name = "minlockdays")
	private Integer minlockDays;
	
	@Column(name = "expdate")
	private Date expDate;
	
	@Column(name = "actdate")
	private Date actDate;
	
	@Column(name = "deactdate")
	private Date deactDate;
	
	@Column(name = "suspdate")
	private Date suspDate;
	
	@Column(name = "resumedate")
	private Date resumeDate;
	
	@Column(name = "activatedon")
	private Date activatedon;
	
	@Column(name = "suspdates")
	private String suspDates;
	
	@Column(name = "resumedates")
	private String resumedates;

	@Column(name = "lastinvmon")
	private Integer lastinvmon;

	@Column(name = "pmntcustid")
	private Long pmntCustId;
	
	@Column(name = "vpnsrvcname")
	private String vpnSrvcName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentcafno", referencedColumnName = "cafno", nullable = false, insertable=false, updatable=false)
	private Caf cafNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({@JoinColumn(name="cafno", referencedColumnName="cafno", insertable=false,updatable=false), @JoinColumn(name="tenantcode", referencedColumnName="tenantcode", insertable=false,updatable=false), @JoinColumn(name="prodcode", referencedColumnName="prodcode", insertable=false,updatable=false)})
	private CafProducts cafProducts;
	
	public CafServices() {
		// TODO Auto-generated constructor stub
	}
	
	public CafServices(Caf caf, Object[] product, Object[] obj, String nwsubsCode) {
		this.setCafno(caf.getCafNo());
		this.setSrvcCode(obj[1].toString());
		this.setTenantCode(product[3].toString());
		this.setProdCode(product[0].toString());
		this.setFeatureCodes("");
		this.setMinlockDays(Integer.parseInt(product[2].toString()));
		this.setPmntCustId(caf.getPmntCustId());
		//this.setNwsubsCode(nwsubsCode);
		this.setStatus(1);
		this.setCreatedOn(Calendar.getInstance());
		this.setModifiedOn(Calendar.getInstance());
	}
	
	public CafServices(Long cafNo, Caf caf, OlIptvPkeSales olIptvPkeSales, String pkgCode, Object[] srvcCode) {
		this.setCafno(cafNo);
		this.setSrvcCode(srvcCode[0].toString());
		this.setTenantCode("APSFL");
		this.setProdCode(pkgCode);
		this.setFeatureCodes(srvcCode[1].toString());
		this.setMinlockDays(0);
		this.setPmntCustId(caf.getPmntCustId());
		//this.setNwsubsCode(olIptvPkeSales.getNwsubscode());
		this.setParentCafno(olIptvPkeSales.getAcctcafno());
		this.setStatus(2);
		this.setCreatedOn(Calendar.getInstance());
		this.setModifiedOn(Calendar.getInstance());
	}

	public Caf getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(Caf cafNumber) {
		this.cafNumber = cafNumber;
	}

	public CafProducts getCafProducts() {
		return cafProducts;
	}

	public void setCafProducts(CafProducts cafProducts) {
		this.cafProducts = cafProducts;
	}
	
	public String getFeatureCodes() {
		return featureCodes;
	}

	public void setFeatureCodes(String featureCodes) {
		this.featureCodes = featureCodes;
	}

	public Date getActDate() {
		return actDate;
	}

	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	public Date getDeactDate() {
		return deactDate;
	}

	public void setDeactDate(Date deactDate) {
		this.deactDate = deactDate;
	}

	public Date getSuspDate() {
		return suspDate;
	}

	public void setSuspDate(Date suspDate) {
		this.suspDate = suspDate;
	}

	public Date getResumeDate() {
		return resumeDate;
	}

	public void setResumeDate(Date resumeDate) {
		this.resumeDate = resumeDate;
	}

	public Date getActivatedon() {
		return activatedon;
	}

	public void setActivatedon(Date activatedon) {
		this.activatedon = activatedon;
	}
	
	public Long getStbCafNo() {
		return stbCafNo;
	}

	public void setStbCafNo(Long stbCafNo) {
		this.stbCafNo = stbCafNo;
	}

	public String getSuspDates() {
		return suspDates;
	}

	public void setSuspDates(String suspDates) {
		this.suspDates = suspDates;
	}

	public String getResumedates() {
		return resumedates;
	}

	public void setResumedates(String resumedates) {
		this.resumedates = resumedates;
	}
	
	public Integer getLastinvmon() {
		return lastinvmon;
	}

	public void setLastinvmon(Integer lastinvmon) {
		this.lastinvmon = lastinvmon;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}
	
	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public Integer getMinlockDays() {
		return minlockDays;
	}

	public void setMinlockDays(Integer minlockDays) {
		this.minlockDays = minlockDays;
	}

	public Long getPmntCustId() {
		return pmntCustId;
	}

	public void setPmntCustId(Long pmntCustId) {
		this.pmntCustId = pmntCustId;
	}

	public Long getParentCafno() {
		return parentCafno;
	}

	public void setParentCafno(Long parentCafno) {
		this.parentCafno = parentCafno;
	}

	public String getVpnSrvcName() {
		return vpnSrvcName;
	}

	public void setVpnSrvcName(String vpnSrvcName) {
		this.vpnSrvcName = vpnSrvcName;
	}
	
}
