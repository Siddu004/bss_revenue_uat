/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class CafChargesPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long cafno;
	
	private String chargeCode;
	
	private Long stbcafno;
	
	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	
	public Long getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(Long stbcafno) {
		this.stbcafno = stbcafno;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cafno == null) ? 0 : cafno.hashCode());
		result = prime * result + ((chargeCode == null) ? 0 : chargeCode.hashCode());
		result = prime * result + ((stbcafno == null) ? 0 : stbcafno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafChargesPK other = (CafChargesPK) obj;
		if (cafno == null) {
			if (other.cafno != null)
				return false;
		} else if (!cafno.equals(other.cafno))
			return false;
		if (chargeCode == null) {
			if (other.chargeCode != null)
				return false;
		} else if (!chargeCode.equals(other.chargeCode))
			return false;
		if (stbcafno == null) {
			if (other.stbcafno != null)
				return false;
		} else if (!stbcafno.equals(other.stbcafno))
			return false;
		return true;
	}
	
}
