/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
public class CorpusPreviousBillBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "custid")
	private String customerId;

	@Column(name = "customerName")
	private String customerName;
	
	@Column(name = "address")
	private String customerAddress;

	@Column(name = "phoneno")
	private String phoneNumber;

	@Column(name = "cafno")
	private String accountNumber;
	
	@Transient
	private List<PreviousBillBO> PreviousBillList;

	@Transient
	private String statusCode;
	
	@Transient
	private String description;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<PreviousBillBO> getPreviousBillList() {
		return PreviousBillList;
	}

	public void setPreviousBillList(List<PreviousBillBO> previousBillList) {
		PreviousBillList = previousBillList;
	}
	
}
