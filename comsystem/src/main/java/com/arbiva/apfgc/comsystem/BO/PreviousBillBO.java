/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class PreviousBillBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "custinvno")
	private String billNumber;

	@Column(name = "invDate")
	private String billDate;

	@Column(name = "billperiod")
	private String billPeriod;

	@Column(name = "invDueDate")
	private String paymentDueDate;

	@Column(name = "prevbal")
	private String previousBalence;

	@Column(name = "prevpaid")
	private String lastPaymentAmount;
	
	@Column(name = "balamnt")
	private String balenceAmount;

	@Column(name = "adjustments")
	private String adjustmentsAmount;
	
	@Column(name = "invamt")
	private String currentBillAmount;

	@Column(name = "payableamt")
	private String payableAmount;

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillPeriod() {
		return billPeriod;
	}

	public void setBillPeriod(String billPeriod) {
		this.billPeriod = billPeriod;
	}

	public String getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getPreviousBalence() {
		return previousBalence;
	}

	public void setPreviousBalence(String previousBalence) {
		this.previousBalence = previousBalence;
	}

	public String getLastPaymentAmount() {
		return lastPaymentAmount;
	}

	public void setLastPaymentAmount(String lastPaymentAmount) {
		this.lastPaymentAmount = lastPaymentAmount;
	}

	public String getBalenceAmount() {
		return balenceAmount;
	}

	public void setBalenceAmount(String balenceAmount) {
		this.balenceAmount = balenceAmount;
	}

	public String getAdjustmentsAmount() {
		return adjustmentsAmount;
	}

	public void setAdjustmentsAmount(String adjustmentsAmount) {
		this.adjustmentsAmount = adjustmentsAmount;
	}

	public String getCurrentBillAmount() {
		return currentBillAmount;
	}

	public void setCurrentBillAmount(String currentBillAmount) {
		this.currentBillAmount = currentBillAmount;
	}

	public String getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}
}
