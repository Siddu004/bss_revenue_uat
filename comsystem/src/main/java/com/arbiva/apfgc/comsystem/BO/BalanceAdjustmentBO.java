/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * @author kiran
 *
 */
@Entity
@IdClass(BalanceAdjustmentBOIDClass.class)
public class BalanceAdjustmentBO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="custid")
	private String custId;
	
	@Id
	@Column(name="custinvno")
	private String custinvno;
	
	@Id
	@Column(name="acctcafno")
	private String acctcafno;
	
	@Id
	@Column(name="invmn")
	private String invMonth;
	
	@Id
	@Column(name="invyr")
	private String invYear;
	
	@Column(name="aadharno")
	private String aadharNo;
	
	@Column(name="custname")
	private String custName;
	
	@Column(name="invfdate")
	private String invDate;
	
	@Column(name="totalAmount")
	private String totalAmount;
	
	public String getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}

	public String getCustinvno() {
		return custinvno;
	}

	public void setCustinvno(String custinvno) {
		this.custinvno = custinvno;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getInvMonth() {
		return invMonth;
	}

	public void setInvMonth(String invMonth) {
		this.invMonth = invMonth;
	}

	public String getInvYear() {
		return invYear;
	}

	public void setInvYear(String invYear) {
		this.invYear = invYear;
	}

	public String getInvDate() {
		return invDate;
	}

	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

}
