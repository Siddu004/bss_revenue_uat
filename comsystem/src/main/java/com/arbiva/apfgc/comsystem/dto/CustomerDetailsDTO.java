package com.arbiva.apfgc.comsystem.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDetailsDTO {

	@JsonProperty("Subscriber Code")
	private String subscribercode;

	@JsonProperty("Package Name")
	private String packageCode;
	
	@JsonProperty("Package Codes")
	private String packageCodesList;

	@JsonProperty("Corpus Activated Date")
	private String coprusActivatedDate;
	
	@JsonProperty("Price")
	private String price;
	
	@JsonProperty("Service Tax Amount")
	private String srvcTax;
	
	@JsonProperty("Swatcha Bharat Tax Amount")
	private String swatchTax;
	
	@JsonProperty("kisan Tax Amount")
	private String kisanTax;
	
	@JsonProperty("ReferenceId")
	private String referenceId;
	
	


	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getPackageCodesList() {
		return packageCodesList;
	}

	public void setPackageCodesList(String packageCodesList) {
		this.packageCodesList = packageCodesList;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSrvcTax() {
		return srvcTax;
	}

	public void setSrvcTax(String srvcTax) {
		this.srvcTax = srvcTax;
	}

	public String getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(String swatchTax) {
		this.swatchTax = swatchTax;
	}

	public String getKisanTax() {
		return kisanTax;
	}

	public void setKisanTax(String kisanTax) {
		this.kisanTax = kisanTax;
	}

	public String getSubscribercode() {
		return subscribercode;
	}

	public void setSubscribercode(String subscribercode) {
		this.subscribercode = subscribercode;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public String getCoprusActivatedDate() {
		return coprusActivatedDate;
	}

	public void setCoprusActivatedDate(String coprusActivatedDate) {
		this.coprusActivatedDate = coprusActivatedDate;
	}
	
	

}
