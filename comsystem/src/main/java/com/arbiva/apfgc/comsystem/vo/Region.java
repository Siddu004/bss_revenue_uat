/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class Region implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String regionCode;

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

}
