/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.ChargeCodes;
import com.arbiva.apfgc.comsystem.model.ChargeTaxes;
import com.arbiva.apfgc.comsystem.model.TaxMaster;
import com.arbiva.apfgc.comsystem.model.TaxRegionMapping;

/**
 * @author Lakshman
 *
 */
@Repository
public class TaxRegionMappingDao {
	
	private static final Logger LOGGER = Logger.getLogger(TaxRegionMappingDao.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public List<TaxRegionMapping> getAllTaxRegionMappingList() {
		 List<TaxRegionMapping> taxRegionMappingList = new ArrayList<TaxRegionMapping>();
		  StringBuilder builder = new StringBuilder(" FROM ").append(TaxRegionMapping.class.getSimpleName());
		  TypedQuery<TaxRegionMapping> query = null;
			try {
				LOGGER.info("START::getAllTaxRegionMappingList()");
				query = getEntityManager().createQuery(builder.toString(), TaxRegionMapping.class);
				taxRegionMappingList = query.getResultList();
				LOGGER.info("END::getAllTaxRegionMappingList()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getAllTaxRegionMappingList() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return taxRegionMappingList;
	}
	
	public List<TaxMaster> getAllTaxMasterList() {
		 List<TaxMaster> taxMasterList = new ArrayList<TaxMaster>();
		  StringBuilder builder = new StringBuilder(" FROM ").append(TaxMaster.class.getSimpleName());
		  TypedQuery<TaxMaster> query = null;
			try {
				LOGGER.info("START::getAllTaxMasterList()");
				query = getEntityManager().createQuery(builder.toString(), TaxMaster.class);
				taxMasterList = query.getResultList();
				LOGGER.info("END::getAllTaxMasterList()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getAllTaxMasterList() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return taxMasterList;
	}
	
	public List<ChargeCodes> getAllChargeCodesList() {
		 List<ChargeCodes> chargeCodesList = new ArrayList<ChargeCodes>();
		  StringBuilder builder = new StringBuilder(" FROM ").append(ChargeCodes.class.getSimpleName());
			try {
				LOGGER.info("START::getAllChargeCodesList()");
				TypedQuery<ChargeCodes> query = getEntityManager().createQuery(builder.toString(), ChargeCodes.class);
				chargeCodesList = query.getResultList();
				LOGGER.info("END::getAllChargeCodesList()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getAllChargeCodesList() " + e);
			}
		  return chargeCodesList;
	}
	
	public List<ChargeTaxes> getAllChargeTaxesList() {
		 List<ChargeTaxes> chargeTaxesList = new ArrayList<ChargeTaxes>();
		  StringBuilder builder = new StringBuilder(" FROM ").append(ChargeTaxes.class.getSimpleName());
		  TypedQuery<ChargeTaxes> query = null;
			try {
				LOGGER.info("START::getAllChargeTaxesList()");
				query = getEntityManager().createQuery(builder.toString(), ChargeTaxes.class);
				chargeTaxesList = query.getResultList();
				LOGGER.info("END::getAllChargeTaxesList()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getAllChargeTaxesList() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return chargeTaxesList;
	}

}
