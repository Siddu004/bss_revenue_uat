/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lakshman
 *
 */
public class RestProductVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mspcode;
	
	private String prodcode;
	
	private Float prodcharge;
	
	private Integer lockinperiod;
	
	private List<AddtionalServicesVO> services;

	public String getMspcode() {
		return mspcode;
	}

	public void setMspcode(String mspcode) {
		this.mspcode = mspcode;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public Float getProdcharge() {
		return prodcharge;
	}

	public void setProdcharge(Float prodcharge) {
		this.prodcharge = prodcharge;
	}

	public Integer getLockinperiod() {
		return lockinperiod;
	}

	public void setLockinperiod(Integer lockinperiod) {
		this.lockinperiod = lockinperiod;
	}

	public List<AddtionalServicesVO> getServices() {
		return services;
	}

	public void setServices(List<AddtionalServicesVO> services) {
		this.services = services;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
