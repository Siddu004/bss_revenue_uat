/**
 * 
 */
package com.arbiva.apfgc.comsystem.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Lakshman
 *
 */
public class DateUtill {
	
	private static final SimpleDateFormat Date_Format = new SimpleDateFormat("MM/dd/yyyy");
	
	private static final SimpleDateFormat Date_Format1 = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final SimpleDateFormat STRING_Format = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final SimpleDateFormat DATE_Format = new SimpleDateFormat("MM/dd/yyyy");
	
	private static final SimpleDateFormat String_Format = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final SimpleDateFormat cafInv_DATE_Format = new SimpleDateFormat("yyyyMMdd");
	
	private static final SimpleDateFormat aadhar_Date_Format1 = new SimpleDateFormat("MM/dd/yyyy");
	
	private static final SimpleDateFormat aadhar_Date_Format2 = new SimpleDateFormat("dd-MM-yyyy");
	
	public static Date aadharStringtoDateFormat1(String datestr) throws ParseException {
		return aadhar_Date_Format1.parse(datestr);
	}
	
	public static Date aadharStringtoDateFormat2(String datestr) throws ParseException {
		return aadhar_Date_Format2.parse(datestr);
	}
	
	public static Date stringtoDate(String datestr) throws ParseException {
		return Date_Format.parse(datestr);
	}
	
	public static Date stringtoDateFormat(String datestr) throws ParseException {
		return STRING_Format.parse(datestr);
	}
	
	public static String dateToString(Date date) throws ParseException {
		return DATE_Format.format(date);
	}
	
	public static String dateToStringFormat(Date date) throws ParseException {
		return String_Format.format(date);
	}
	
	public static Integer getEmiDate(Date date) throws ParseException {
		String emiDate = Date_Format1.format(date);
		String splitEmiDate[] = emiDate.split("-");
		String emiCountDate = splitEmiDate[0]+splitEmiDate[1];
		return Integer.parseInt(emiCountDate);
	}
	
	public static Date stringToDateString_Format(String date) throws ParseException {
		return String_Format.parse(date);
	}
	
	public static Date GetOneTimeServiceDate(Integer duration) throws IOException, ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, duration);
		Date oneTimeDate = calendar.getTime();
		return oneTimeDate;
	}
	
	public static Integer getEmiMonthAndYear(Integer duration) throws IOException, ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, duration);
		Date oneTimeDate = calendar.getTime();
		String emiEndDate = Date_Format1.format(oneTimeDate);
		String splitEmiDate[] = emiEndDate.split("-");
		String emiCountDate = splitEmiDate[0]+splitEmiDate[1];
		return Integer.parseInt(emiCountDate);
	}
	
	public static String dateToStringdateFormat(Date date) throws ParseException {
		return dateFormat.format(date);
	}
	
	public static String calenderToStringdateFormat(Calendar date) throws ParseException {
		return DATE_Format.format(date);
	}
	
	public static String cafInvDate(Date date) throws ParseException {
		return cafInv_DATE_Format.format(date);
	}
	
	public static Date stringToDBDateString_Format(String date) throws ParseException {
		return Date_Format1.parse(date);
	}
	
	public static Integer getMonth() throws IOException, ParseException {
		java.util.Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer month = cal.get(Calendar.MONTH);
		return month;
	}
	
	public static Integer getYear() throws IOException, ParseException {
		java.util.Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer year = cal.get(Calendar.YEAR);
		return year;
	}
	
	public static Integer getCurrentMonth() throws IOException, ParseException {
		java.util.Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer month = cal.get(Calendar.MONTH);
		return month+1;
	}
	
}
