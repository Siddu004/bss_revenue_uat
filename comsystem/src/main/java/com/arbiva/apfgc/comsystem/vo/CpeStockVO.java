package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

public class CpeStockVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cpeSrlno;
	
	private String profileId;
	
	private String mspCode;
	
	private String lmoCode;
	
	private String macAddress;
	
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getCpeSrlno() {
		return cpeSrlno;
	}

	public void setCpeSrlno(String cpeSrlno) {
		this.cpeSrlno = cpeSrlno;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

}
