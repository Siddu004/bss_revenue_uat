/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

/**
 * @author Arbiva
 *
 */
public class CafJsonData {
	
	private String cafno;
	
	private String custtypelov;
	
	private String custcode;
	
	private String pocname;
	
	private String pocmob1;
	
	private String addr1;

	public String getCafno() {
		return cafno;
	}

	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	public String getCusttypelov() {
		return custtypelov;
	}

	public void setCusttypelov(String custtypelov) {
		this.custtypelov = custtypelov;
	}

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public String getPocname() {
		return pocname;
	}

	public void setPocname(String pocname) {
		this.pocname = pocname;
	}

	public String getPocmob1() {
		return pocmob1;
	}

	public void setPocmob1(String pocmob1) {
		this.pocmob1 = pocmob1;
	}

	public String getAddr1() {
		return addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

}
