package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="srvcfeatures")
@IdClass(SrvcFeaturesPK.class)
public class SrvcFeatures implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "coresrvccode")
	private String coresrvccode;
	
	@Column(name = "featureid")
	private String featureid;
	
	@Id
	@Column(name = "featurecode")
	private String featurecode;
	
	@Column(name = "featurename")
	private String featurename;
	
	@Column(name = "genre")
	private String genre;
	
	@Column(name = "broadcaster")
	private String broadcaster;
	
	@Column(name = "lang")
	private String lang;
	
	@Column(name = "maxprmvalues")
	private Integer maxPrmValues;
	
	public Integer getMaxPrmValues() {
		return maxPrmValues;
	}

	public void setMaxPrmValues(Integer maxPrmValues) {
		this.maxPrmValues = maxPrmValues;
	}

	public String getCoresrvccode() {
		return coresrvccode;
	}

	public void setCoresrvccode(String coresrvccode) {
		this.coresrvccode = coresrvccode;
	}

	public String getFeatureid() {
		return featureid;
	}

	public void setFeatureid(String featureid) {
		this.featureid = featureid;
	}

	public String getFeaturecode() {
		return featurecode;
	}

	public void setFeaturecode(String featurecode) {
		this.featurecode = featurecode;
	}

	public String getFeaturename() {
		return featurename;
	}

	public void setFeaturename(String featurename) {
		this.featurename = featurename;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getBroadcaster() {
		return broadcaster;
	}

	public void setBroadcaster(String broadcaster) {
		this.broadcaster = broadcaster;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
