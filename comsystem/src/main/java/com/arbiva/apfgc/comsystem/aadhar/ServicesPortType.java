/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.arbiva.apfgc.comsystem.aadhar;

public interface ServicesPortType extends java.rmi.Remote {
    public java.lang.String getAadhaarInfo(java.lang.String UID, java.lang.String EID) throws java.rmi.RemoteException;
}
