/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CafBuckets;

/**
 * @author Arbiva
 *
 */
@Repository
public class CafBucketsDao {

private static final Logger LOGGER = Logger.getLogger(CafBucketsDao.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public void deleteCafs(Caf caf) {
		getEntityManager().remove(caf);
		getEntityManager().flush();
	}
	
	public List<CafBuckets> findAllCafBuckets() {
		List<CafBuckets> cafBuckets = new ArrayList<CafBuckets>();
		TypedQuery<CafBuckets> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(CafBuckets.class.getSimpleName());
		try {
			LOGGER.info("START::findAllCafBuckets()");
			query = getEntityManager().createQuery(builder.toString(), CafBuckets.class);
			cafBuckets = query.getResultList();
			LOGGER.info("END::findAllCafBuckets()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllCafBuckets() " + e);
		}
		finally{
			builder = null;
			query = null;
		}
		return cafBuckets;
	}

	public CafBuckets saveCafBuckets(CafBuckets cafBuckets) {
		return getEntityManager().merge(cafBuckets);
	}
}
