/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

/**
 * @author Arbiva
 *
 */
public class CafProductsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long cafno;
	
	private String tenantCode;
	
	private String prodCode;

	public Long getCafno() {
		return cafno;
	}

	public void setCafno(Long cafno) {
		this.cafno = cafno;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cafno == null) ? 0 : cafno.hashCode());
		result = prime * result + ((prodCode == null) ? 0 : prodCode.hashCode());
		result = prime * result + ((tenantCode == null) ? 0 : tenantCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CafProductsPK other = (CafProductsPK) obj;
		if (cafno == null) {
			if (other.cafno != null)
				return false;
		} else if (!cafno.equals(other.cafno))
			return false;
		if (prodCode == null) {
			if (other.prodCode != null)
				return false;
		} else if (!prodCode.equals(other.prodCode))
			return false;
		if (tenantCode == null) {
			if (other.tenantCode != null)
				return false;
		} else if (!tenantCode.equals(other.tenantCode))
			return false;
		return true;
	}
	
}
