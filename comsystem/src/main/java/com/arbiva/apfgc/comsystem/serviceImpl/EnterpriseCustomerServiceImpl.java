/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.EnterpriseCustomerDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.service.EnterpriseCustomerService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.EnterpriseCustomerVO;

/**
 * @author Lakshman
 *
 */
@Service
public class EnterpriseCustomerServiceImpl implements EnterpriseCustomerService {
	
	private static final Logger logger = Logger.getLogger(EnterpriseCustomerServiceImpl.class);

	@Autowired
	EnterpriseCustomerDao enterpriseCustomerDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Override
	public void deleteEnterpriseCustomers(Customer enterpriseCustomer) {
		try {
			logger.info("EnterpriseCustomerServiceImpl :: deleteEnterpriseCustomers() :: Start");
			enterpriseCustomerDao.deleteEnterpriseCustomers(enterpriseCustomer);
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: deleteEnterpriseCustomers() :: " + e);
			e.printStackTrace();
		} finally {
			enterpriseCustomer = null;
		}
	}

	@Override
	public Customer findByEnterpriseCustomerId(Long custId) {
		Customer enterpriseCustomer = new Customer();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findByEnterpriseCustomerId() :: Start");
			enterpriseCustomer = enterpriseCustomerDao.findByEnterpriseCustomerId(custId);
			
			String pocname = enterpriseCustomer.getPocName();
			if(pocname.indexOf("(") != -1) {
				String[] splitString = pocname.split("\\(");
				enterpriseCustomer.setPocName(splitString[0]);
				enterpriseCustomer.setPocDesignation(splitString[1].replace(")", ""));
			}
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findByEnterpriseCustomerId() :: " + e);
			e.printStackTrace();
		} finally {
			
		}
		return enterpriseCustomer;
	}

	@Override
	public List<Object[]> findAllEnterpriseCustomers(String tenantCode, String tenantType) {
		List<Object[]> customerList = new ArrayList<Object[]>();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findAllEnterpriseCustomers() :: Start");
			customerList = enterpriseCustomerDao.findAllEnterpriseCustomers(tenantCode, tenantType);
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findAllEnterpriseCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public Customer saveEnterpriseCustomer(Customer enterpriseCustomer) {
		try {
			logger.info("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: Start");
			enterpriseCustomer = enterpriseCustomerDao.saveEnterpriseCustomer(enterpriseCustomer);
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return enterpriseCustomer;
	}

	@Override
	@Transactional
	public Customer saveEnterpriseCustomer(EnterpriseCustomerVO enterpriseCustomerVO, String loginID, String tenantCode) {
		Customer enterpriseCustomer = new Customer();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: Start");
			enterpriseCustomer = new Customer(enterpriseCustomerVO);
			if(enterpriseCustomerVO.getCustId().isEmpty()) {
				Long customerId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.ENTERPRISE_CUSTOMER_ID.getCode());
				enterpriseCustomer.setCustUid(customerId.toString());
				enterpriseCustomer.setCustCode(customerId.toString());
				enterpriseCustomer.setCustId(customerId);
			} else {
				enterpriseCustomer.setCustUid(enterpriseCustomerVO.getCustId());
				enterpriseCustomer.setCustCode(enterpriseCustomerVO.getCustId());
				enterpriseCustomer.setCustId(Long.parseLong(enterpriseCustomerVO.getCustId()));
				enterpriseCustomer.setModifiedBy(loginID);
				enterpriseCustomer.setModifiedIPAddr(enterpriseCustomerVO.getIpAddress() != null ? enterpriseCustomerVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			}
			if(enterpriseCustomerVO.getCustCode() != null) {
				if(!enterpriseCustomerVO.getCustCode().isEmpty()) {
					enterpriseCustomer.setParentcustcode(enterpriseCustomerVO.getCustCode());
				} 
			}
			enterpriseCustomer.setCreatedBy(loginID);
			enterpriseCustomer.setCreatedIPAddr(enterpriseCustomerVO.getIpAddress() != null ? enterpriseCustomerVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			enterpriseCustomer.setCreatedOn(Calendar.getInstance());
			enterpriseCustomer.setModifiedOn(Calendar.getInstance());
			enterpriseCustomer.setStatus(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
			enterpriseCustomer.setState("Andhra Pradesh");
			enterpriseCustomer.setBlState("Andhra Pradesh");
			enterpriseCustomer.setLmoCode(tenantCode);
			enterpriseCustomer = enterpriseCustomerDao.saveEnterpriseCustomer(enterpriseCustomer);
			logger.info("The Enterprise Customer Created Successfully");
			
			logger.info("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: End");
		} catch(Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: " +e);
			e.printStackTrace();
		}finally {
			enterpriseCustomerVO = null;
		}
		return enterpriseCustomer;
	}

	@Override
	public String findByCustomerCode(String custCode) {
		String status = "false";
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findByCustomerCode() :: Start");
			status = enterpriseCustomerDao.findByCustomerCode(custCode);
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findByCustomerCode() :: " + e);
			e.printStackTrace();
		} finally {
			
		}
		return status;
	}

	@Override
	public List<Customer> findByEntParentCustCodeAndLmocode(String customerCode, String tenantCode) {
		List<Customer> entCustomerList = new ArrayList<>();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findByEntParentCustCodeAndLmocode() :: Start");
			entCustomerList = enterpriseCustomerDao.findByEntParentCustCodeAndLmocode(customerCode, tenantCode);
		}catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findByEntParentCustCodeAndLmocode() :: "+e);
			e.printStackTrace();
		} finally {
			
		}
		return entCustomerList;
	}

	@Override
	public List<Customer> findAllParentAndChildEnterpriseCustomers(String tenantCode) {
		List<Customer> entCustomerList = new ArrayList<>();
		List<Customer> enterpriseCustomerList = new ArrayList<>();
		Long pmntId;
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findAllParentAndChildEnterpriseCustomers() :: Start");
			entCustomerList = enterpriseCustomerDao.findAllParentAndChildEnterpriseCustomers(tenantCode);
			for(Customer enterpriseCustomerObj : entCustomerList) {
				pmntId = storedProcedureDAO.executePaymentCustomer(enterpriseCustomerObj.getCustId());
				enterpriseCustomerObj.setCustUid(pmntId.toString());
				enterpriseCustomerList.add(enterpriseCustomerObj);
			}
		}catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findAllParentAndChildEnterpriseCustomers() :: "+e);
			e.printStackTrace();
		} finally {
			pmntId = null;
			entCustomerList = null;
		}
		return enterpriseCustomerList;
	}

	@Override
	@Transactional
	public Customer saveEnterpriseCustomer(CustomerCafVO customerCafVO, EntCafStage entCafStage, String district, String mandal) {
		Customer enterpriseCustomer = new Customer();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: Start");
			Long customerId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.ENTERPRISE_CUSTOMER_ID.getCode());
			enterpriseCustomer.setCustId(customerId);
			enterpriseCustomer.setCustTypeLov(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode());
			enterpriseCustomer.setTitleLov("Ms.");
			enterpriseCustomer.setGender('O');
			enterpriseCustomer.setBlacklistFlag('N');
			enterpriseCustomer.setCustCode(customerId.toString());
			enterpriseCustomer.setBillfreqLov(customerCafVO.getBillCycle());
			enterpriseCustomer.setDateofinc(DateUtill.stringtoDate("06/02/2014"));
			enterpriseCustomer.setCustUid(customerId.toString());
			enterpriseCustomer.setCustType(customerCafVO.getCustType());
			enterpriseCustomer.setOfficeTypeLov(customerCafVO.getEntCustType());
			enterpriseCustomer.setParentcustcode(customerCafVO.getCustCode());
			enterpriseCustomer.setRegnCode(customerCafVO.getAadharNumber());
			if(entCafStage.getLocation().contains("^")) {
				enterpriseCustomer.setCustName(entCafStage.getLocation().split("\\^")[0]);
				enterpriseCustomer.setOfficeLocation(entCafStage.getLocation().split("\\^")[1]);
			} else {
				enterpriseCustomer.setCustName("");
				enterpriseCustomer.setOfficeLocation(entCafStage.getLocation());
			}
			enterpriseCustomer.setPmntliabilityflag(1);
			enterpriseCustomer.setAddress1(entCafStage.getDistrict());
			enterpriseCustomer.setBlAddress1(entCafStage.getDistrict());
			enterpriseCustomer.setState("Andhra Pradesh");
			if(entCafStage.getContactEmail() != null) {
				enterpriseCustomer.setEmail1(entCafStage.getContactEmail());
				enterpriseCustomer.setBlEmail1(entCafStage.getContactEmail());
			} else {
				enterpriseCustomer.setEmail1("");
				enterpriseCustomer.setBlEmail1("");
			}
			enterpriseCustomer.setPocName(entCafStage.getContactName());
			enterpriseCustomer.setPocMob1(entCafStage.getContactMobileNo());
			enterpriseCustomer.setLmoCode(customerCafVO.getLmoCode());
			enterpriseCustomer.setStatus(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
			enterpriseCustomer.setCreatedBy(customerCafVO.getLoginId());
			enterpriseCustomer.setCreatedOn(Calendar.getInstance());
			enterpriseCustomer.setModifiedOn(Calendar.getInstance());
			enterpriseCustomer.setCreatedIPAddr(customerCafVO.getIpAddress());
			enterpriseCustomer.setDistrict(district);
			enterpriseCustomer.setBlDistrict(district);
			enterpriseCustomer.setMandal(mandal);
			enterpriseCustomer.setBlMandal(mandal);
			//enterpriseCustomer.setCustdistUid(Integer.parseInt((district)));
			enterpriseCustomer.setCustdistUid(0);
			enterpriseCustomer = enterpriseCustomerDao.saveEnterpriseCustomer(enterpriseCustomer);
			logger.info("The Enterprise Customer Created Successfully");
			
			logger.info("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: End");
		} catch(Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: saveEnterpriseCustomer() :: " +e);
			e.printStackTrace();
		}finally {
		}
		return enterpriseCustomer;
	}

	@Override
	public List<Customer> findAllEntCustomers(String tenantCode) {
		List<Customer> enterpriseCustomerList = new ArrayList<>();
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findAllEntCustomers() :: Start");
			enterpriseCustomerList = enterpriseCustomerDao.findAllEntCustomers(tenantCode);
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findAllEntCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return enterpriseCustomerList;
	}
	
	public ComsHelperDTO findEnterpriseCustomers(ComsHelperDTO comsHelperDTO) {
		List<EnterpriseCustomerVO> entCustomerList = new ArrayList<>();
		long count = 0;
		try {
			logger.info("EnterpriseCustomerServiceImpl :: findEnterpriseCustomers() :: Start");
			List<Object[]> customerObjectList = enterpriseCustomerDao.findEntCustomers(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(), comsHelperDTO.getPageObject());
			PageObject pageObject = comsHelperDTO.getPageObject();
			for (Object[] obj : customerObjectList) {
				count = count + 1;
				EnterpriseCustomerVO entCustomer = new EnterpriseCustomerVO();
				entCustomer.setRowCount(String.valueOf(count));
				entCustomer.setName(obj[0] != null ? obj[0].toString() : "NA");
				entCustomer.setCustCode(obj[1] != null ? obj[1].toString() : "NA");
				entCustomer.setEntTypeLov(obj[2] != null ? obj[2].toString() : "NA");
				entCustomer.setOffcTypeLov(obj[3] != null ? obj[3].toString() : "NA");
				entCustomer.setPocName(obj[4] != null ? obj[4].toString() : "NA");
				entCustomer.setStdCode(obj[5] != null ? obj[5].toString() : "NA");
				entCustomer.setLandlineNo(obj[6] != null ? obj[6].toString() : "NA");
				entCustomer.setBillCycle(obj[7] != null ? obj[7].toString() : "NA");
				entCustomer.setEmailId(obj[8] != null ? obj[8].toString() : "NA");
				entCustomer.setDateOfIncorporation(obj[9] != null ? obj[9].toString() : "NA");
				entCustomer.setCustId(obj[10] != null ? obj[10].toString() : "NA");
				entCustomer.setRegnCode(obj[11] != null ? obj[11].toString() : "NA");
				entCustomer.setCustTypeLov(obj[12] != null ? obj[12].toString() : "NA");
				entCustomer.setStatus(obj[13] != null ? obj[13].toString() : "NA");
				entCustomer.setTenantCode(comsHelperDTO.getTenantCode() != null ? comsHelperDTO.getTenantCode() : "NA");
				entCustomer.setTenantType(comsHelperDTO.getTenanttype() != null ? comsHelperDTO.getTenanttype() : "NA");
				entCustomerList.add(entCustomer);
			}
			comsHelperDTO.setEntCustList(entCustomerList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("EnterpriseCustomerServiceImpl :: findEnterpriseCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}
}
