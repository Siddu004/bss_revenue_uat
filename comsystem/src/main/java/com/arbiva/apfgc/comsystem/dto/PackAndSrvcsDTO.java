package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;
import java.util.List;

import com.arbiva.apfgc.comsystem.vo.PaymentDetailsVO;

public class PackAndSrvcsDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String success;
	
	private String successMessage;
	
	private List<PaymentDetailsVO> paymentDtlsList;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public List<PaymentDetailsVO> getPaymentDtlsList() {
		return paymentDtlsList;
	}

	public void setPaymentDtlsList(List<PaymentDetailsVO> paymentDtlsList) {
		this.paymentDtlsList = paymentDtlsList;
	}
	

}
