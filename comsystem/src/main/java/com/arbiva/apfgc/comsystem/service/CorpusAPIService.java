package com.arbiva.apfgc.comsystem.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import com.arbiva.apfgc.comsystem.dto.CorpusIptvDTO;
import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.vo.CustomerDetailsVo;
import com.arbiva.apfgc.provision.dto.CafProvisioningDTO;
import com.xyzinnotech.bss.sms.dto.Sms;

public interface CorpusAPIService {

	public Map<String, Map<String, Object>> vodService(CustomerDetailsDTO customerDetailsDTO);

	public void validateRequest(CustomerDetailsDTO customerDetailsDTO);

	public Map<String, Map<String, Object>> getIptvPkges(String subscribercode);

	public Map<String, Map<String, Object>> generateOTPForIptvPkges(CustomerDetailsDTO customerDetailsDTO);

	public CorpusIptvDTO activateAlaCartePackage(String referenceId);
	
	public CorpusIptvDTO activateAlaCartePackage(CustomerDetailsDTO customerDetailsDTO);

	public Map<String, Map<String, Object>> reGenerateOTP(String referenceId);

	public void saveDataInCafProdsAndCafSrvcs(CafProvisioningDTO caf) throws IOException, ParseException;
	
	public Map<String, Map<String, Object>> sendCorpusSMS(Sms sms);
	
	public CorpusIptvDTO deActivateAlaCartePackage(CustomerDetailsDTO customerDetailsDTO);


}
