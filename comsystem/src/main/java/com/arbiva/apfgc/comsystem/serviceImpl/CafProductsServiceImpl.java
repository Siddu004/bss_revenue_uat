/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafProductsDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafServicesDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.CafProducts;
import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.service.CafAccountService;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.CustomerService;
import com.arbiva.apfgc.comsystem.service.PaymentService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;

/**
 * @author Lakshman
 *
 */
@Service
public class CafProductsServiceImpl implements CafProductsService {

	private static final Logger logger = Logger.getLogger(CafProductsServiceImpl.class);

	@Autowired
	CafProductsDao cafProductsDao;
	
	@Autowired
	CafServicesDao cafServicesDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	CafServicesService cafServicesService;

	@Autowired
	CafChargesService cafChargesService;

	@Autowired
	CafAccountService cafAccountService;

	@Autowired
	CustomerService customerService;

	@Autowired
	CafFeatureParamsService cafFeatureParamsService;

	@Autowired
	CafService cafService;

	@Autowired
	CafDao cafDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	PaymentService paymentService;
	
	@Override
	@Transactional
	public void DeletePreviewsPkg(Long cafNo) {
		try {
			int checkcaf = cafProductsDao.checkcaf(cafNo);
			if (checkcaf > 0) {
				int del = cafProductsDao.deleteAllProdsSrvcsFeature(cafNo);
				logger.info("The CafServices Details Created Successfully" + del);
			}
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::DeletePreviewsPkg() " + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	@Transactional
	public CafProducts createCafProducts(CustomerCafVO customerCafVO,  Long cafNo, String loginID, String pmntCustId, Long stbCafNo, String stbPkgCode, Float paidAmount,  Long addPkgCafNo) throws Exception {
		CafProducts cafProducts = null;
		String status = "";
		List<ProductsVO> selectedProdList = new ArrayList<ProductsVO>();
		try {
			selectedProdList = customerCafVO.getProducts();
			for (ProductsVO products : selectedProdList) {
				cafProducts = new CafProducts();
				if (products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
					Date oneTimeExpDate = DateUtill.GetOneTimeServiceDate(products.getOneTimeProdDuration());
					cafProducts.setExpDate(oneTimeExpDate);
				}
				cafProducts.setCafno(customerCafVO.getFlag() != null && (customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()) || customerCafVO.getFlag().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()))? addPkgCafNo : cafNo);
				cafProducts.setParentCafno(cafNo);
				cafProducts.setCreatedBy(loginID);
				cafProducts.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
				cafProducts.setCreatedOn(Calendar.getInstance());
				cafProducts.setModifiedOn(Calendar.getInstance());
				cafProducts.setTenantCode(products.getTenantcode());
				cafProducts.setRsagruid(products.getAgruniqueid());
				cafProducts.setProdCode(products.getProdcode());
				cafProducts.setMinlockDays(products.getLockInPeriod() == null ? 0 : products.getLockInPeriod());
				cafProducts.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
				cafProducts.setPriority(1);
				if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					cafProducts.setPmntCustId(customerCafVO.getCustId().longValue());
				} else {
					cafProducts.setPmntCustId(Long.parseLong(pmntCustId));
				}
				cafProducts.setMspCode(products.getMspCode());
				cafProductsDao.saveCafProducts(cafProducts);
			}
			
			if(customerCafVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode()) || customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) {
				cafServicesService.createCafServices(customerCafVO, customerCafVO.getCafNo(), loginID, pmntCustId, stbCafNo, stbPkgCode, addPkgCafNo);
				logger.info("The CafServices Details Created Successfully");
			}
			if (customerCafVO.getFeatureCodeList() != null && !customerCafVO.getFeatureCodeList().isEmpty()) {
				cafFeatureParamsService.saveCafFeatureParams(customerCafVO);
				logger.info("The cafFeatureParams Details Created Successfully");
			}
			
			if(customerCafVO.getChangePkgFlag() != null && customerCafVO.getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) ) {
				status = paymentService.generateChargesAndAddPackagePayments(customerCafVO, addPkgCafNo, paidAmount, stbCafNo);
				if(status.equalsIgnoreCase("Success")) {
					logger.info("The cafInvDetails Details Created Successfully");
				} else {
					throw new RuntimeException("The cafInvDetails Details Insertion Failed");
				}
			}
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::createCafProducts() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			selectedProdList = null;
		}
		return cafProducts;
	}
	
	@Override
	@Transactional
	public void createMSOPackages(CustomerCafVO customerCafVO, Long stbCafNo) {
		CafProducts cafProducts = null;
		List<ProductsVO> selectedProdList = new ArrayList<ProductsVO>();
		try {
			selectedProdList = customerCafVO.getProducts();
			for (ProductsVO products : selectedProdList) {
				if(products.getCoreSrvcCodesWithComma().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) != -1 ) {
					List<ProductsVO> msoProductList = cafProductsDao.getMSOProducts(products.getMspCode());
					for (ProductsVO msoProducts : msoProductList) {
						cafProducts = new CafProducts();
						if (msoProducts.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
							Date oneTimeExpDate = DateUtill.GetOneTimeServiceDate(msoProducts.getOneTimeProdDuration());
							cafProducts.setExpDate(oneTimeExpDate);
						}
						cafProducts.setCafno(customerCafVO.getCafNo());
						cafProducts.setParentCafno(customerCafVO.getCafNo());
						cafProducts.setCreatedBy(customerCafVO.getLoginId());
						cafProducts.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
						cafProducts.setCreatedOn(Calendar.getInstance());
						cafProducts.setModifiedOn(Calendar.getInstance());
						cafProducts.setTenantCode(msoProducts.getTenantcode());
						cafProducts.setRsagruid(msoProducts.getAgruniqueid());
						cafProducts.setProdCode(msoProducts.getProdcode());
						cafProducts.setMinlockDays(msoProducts.getLockInPeriod() == null ? 0 : msoProducts.getLockInPeriod());
						cafProducts.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
						cafProducts.setPriority(1);
						if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
							cafProducts.setPmntCustId(customerCafVO.getCustId().longValue());
						} else {
							cafProducts.setPmntCustId(Long.parseLong(customerCafVO.getCustCode()));
						}
						cafProducts.setMspCode(msoProducts.getMspCode());
						cafProductsDao.saveCafProducts(cafProducts);
						logger.info("The MSO Package Details Created Successfully");
						
						List<AddtionalServicesVO> msoServicesList = cafProductsDao.getMSOServicesList(msoProducts.getMspCode(), msoProducts.getProdcode(), msoProducts.getEffectivefrom());
						for(AddtionalServicesVO services : msoServicesList) {
						CafServices	cafServices = new CafServices();
							if(msoProducts.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode())) {
								Date oneTimeExpDate = DateUtill.GetOneTimeServiceDate(msoProducts.getOneTimeProdDuration());
								cafServices.setExpDate(oneTimeExpDate);
							}
							cafServices.setCafno(customerCafVO.getCafNo());
							cafServices.setParentCafno(customerCafVO.getCafNo());
							cafServices.setCreatedBy(customerCafVO.getLoginId());
							cafServices.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress() : httpServletRequest.getRemoteAddr());
							cafServices.setCreatedOn(Calendar.getInstance());
							cafServices.setMinlockDays(services.getLockInPeriod());
							cafServices.setModifiedOn(Calendar.getInstance());
							cafServices.setTenantCode(msoProducts.getTenantcode());
							cafServices.setProdCode(msoProducts.getProdcode());
							cafServices.setSrvcCode(services.getServiceCode());
							cafServices.setStatus(ComsEnumCodes.PAYMENT_RECEIVED_STATUS.getStatus());
							cafServices.setFeatureCodes("0");
							cafServices.setStbCafNo(stbCafNo);
							cafServices.setPmntCustId(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? customerCafVO.getCustId().longValue() : Long.parseLong(customerCafVO.getCustCode()));
							cafServicesDao.saveCafServices(cafServices);
							logger.info("The MSO Services Details Created Successfully");
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::createMSOPackages() " + e);
			e.printStackTrace();
		} finally {
			selectedProdList = null;
		}
	}

	@Override
	@Transactional
	public CafProducts findCafProductByCafNoAndMspCodeAndProdCode(Long cafNo, String mspCode, String prodcode) {
		CafProducts cafProducts = new CafProducts();
		try {
			cafProducts = cafProductsDao.findCafProductByCafNoAndMspCodeAndProdCode(cafNo, mspCode, prodcode);
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::findCafProductByCafNoAndMspCodeAndProdCode() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafProducts;
	}

	@Override
	public List<CafProducts> getCafProducts(Long cafNo) {
		List<CafProducts> cafProductsList = new ArrayList<>();
		try {
			List<Object[]> objectCafProductsList = cafProductsDao.getCafProducts(cafNo);
			for(Object[] object : objectCafProductsList) {
				CafProducts cafProducts = new CafProducts();
				cafProducts.setProdCode(object[0] != null ? object[0].toString() : null);
				cafProducts.setProdName(object[1] != null ? object[1].toString() : null);
				cafProductsList.add(cafProducts);
			}
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::getCafProducts() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafProductsList;
	}

	@Override
	@Transactional
	public void updateCafProducts(PaymentVO paymentVO, String loginID) {
		try {
			cafProductsDao.updateCafProducts(paymentVO, loginID);
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::updateCafProducts() " + e);
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	public List<ProductsVO> getCafServicesListByProdcode(String pkgCodes) {
		List<ProductsVO> cafProductsList = new ArrayList<>();
		try {
			List<Object[]> siProductsList = cafProductsDao.getCafServicesListByProdcode(pkgCodes);
			for(Object[] object : siProductsList) {
				List<AddtionalServicesVO> addtionalServicesVOList = new ArrayList<>();
				ProductsVO productsVO = new ProductsVO();
				productsVO.setTenantcode(object[0] != null ? object[0].toString() : "");
				productsVO.setMspCode(object[0] != null ? object[0].toString() : "");
				productsVO.setProdcode(object[1] != null ? object[1].toString() : "");
				productsVO.setProdType(object[2] != null ? object[2].toString() : "");
				productsVO.setOneTimeProdDuration(object[3] != null ? Integer.parseInt(object[3].toString()) : 0);
				List<Object[]> siServicesList = cafProductsDao.getSIServicesListByProdcode(productsVO.getProdcode());
				for(Object[] serviceObject : siServicesList) {
					AddtionalServicesVO addtionalServicesVO = new AddtionalServicesVO();
					addtionalServicesVO.setServiceCode(serviceObject[0] != null ? serviceObject[0].toString() : "");
					addtionalServicesVO.setLockInPeriod(serviceObject[1] != null ? Integer.parseInt(serviceObject[1].toString()) : 0);
					addtionalServicesVO.setCoreServiceCode(serviceObject[2] != null ? serviceObject[2].toString() : "");
					addtionalServicesVO.setFeatureCodes(serviceObject[3] != null ? serviceObject[3].toString() : "");
					addtionalServicesVOList.add(addtionalServicesVO);
					productsVO.setServicesList(addtionalServicesVOList);
				}
				cafProductsList.add(productsVO);
			}
		} catch (Exception e) {
			logger.error("CafProductsServiceImpl::getCafProducts() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafProductsList;
	}

}
