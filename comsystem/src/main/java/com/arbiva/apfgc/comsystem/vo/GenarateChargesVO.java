/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

import com.arbiva.apfgc.comsystem.dto.CustomerDetailsDTO;
import com.arbiva.apfgc.comsystem.model.CafSTBs;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;

/**
 * @author Lakshman
 *
 */
public class GenarateChargesVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String district;

	private String acctCafno;

	private String pmntCustId;

	private String chargedDate;

	private String tenantCode;

	private String prodCode;

	private String srvcCode;

	private String featureCode;

	private String prodCafno;
	
	private String stbCafno;

	private String chargeCode;

	private String chargeFdate;

	private String chargeTdate;

	private String chargeAmt;

	private String srvcTaxamt;

	private String swatchTaxamt;

	private String kisanTaxamt;

	private String entTaxamt;

	private String chargeType;

	private String paidAmount;

	private String chargeDescription;

	private String ignoreBalenceFlag;
	
	private String rsAgrUid;
	
	private String adjrefId;

	public GenarateChargesVO(String districId, CafSTBs cafSTBs, String pmtCustId,
			CustomerDetailsDTO customerDetailsDTO, BigDecimal paidAmt) throws ParseException {
		this.setDistrict(districId);
		this.setAcctCafno(String.valueOf(cafSTBs.getParentCafno()));
		this.setPmntCustId(pmtCustId);
		this.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setTenantCode("");
		this.setProdCode("");
		this.setSrvcCode("");
		this.setFeatureCode("0");
		this.setProdCafno(String.valueOf(cafSTBs.getParentCafno()));
		this.setChargeCode(ComsEnumCodes.VOD_CHARGE_CODE.getCode());
		this.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeAmt(customerDetailsDTO.getPrice());
		this.setSrvcTaxamt(customerDetailsDTO.getSrvcTax());
		this.setSwatchTaxamt(customerDetailsDTO.getSwatchTax());
		this.setKisanTaxamt(customerDetailsDTO.getKisanTax());
		this.setEntTaxamt("0");
		this.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
		this.setPaidAmount(String.valueOf(paidAmt));
		this.setChargeDescription(customerDetailsDTO.getPackageCode());
		this.setIgnoreBalenceFlag("N");
		this.setRsAgrUid("0");
		this.setAdjrefId(null);
		this.setStbCafno(String.valueOf(cafSTBs.getStbcafno()));
	}

	public GenarateChargesVO() {
	}

	public GenarateChargesVO(String instDistrict, Long parentCafNo, Long pmntCustId,
			TaxesVO taxesVO, String srvcAmount, Long prodCafNo, String pkgCode, String srvcCode, String chargeCode) throws ParseException {
		this.setDistrict(instDistrict);
		this.setAcctCafno(String.valueOf(parentCafNo));
		this.setPmntCustId(String.valueOf(pmntCustId));
		this.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setTenantCode("APSFL");
		this.setProdCode(pkgCode);
		this.setSrvcCode(srvcCode);
		this.setFeatureCode("0");
		this.setProdCafno(String.valueOf(prodCafNo));
		this.setChargeCode(chargeCode);
		this.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeAmt(srvcAmount);
		this.setSrvcTaxamt(taxesVO.getSrvcTax());
		this.setSwatchTaxamt(taxesVO.getSwatchTax());
		this.setKisanTaxamt(taxesVO.getKissanTax());
		this.setEntTaxamt(taxesVO.getEntTax());
		this.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
		this.setPaidAmount("0");
		this.setAdjrefId(null);
		this.setChargeDescription("");
		this.setIgnoreBalenceFlag("N");
	}
	
	public GenarateChargesVO(String instDistrict, Long parentCafNo, Long pmntCustId,
			TaxesVO taxesVO, String srvcAmount, Long prodCafNo, String pkgCode, String srvcCode, String chargeCode,String chargeType) throws ParseException {
		this.setDistrict(instDistrict);
		this.setAcctCafno(String.valueOf(parentCafNo));
		this.setPmntCustId(String.valueOf(pmntCustId));
		this.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setTenantCode("APSFL");
		this.setProdCode(pkgCode);
		this.setSrvcCode(srvcCode);
		this.setFeatureCode("0");
		this.setProdCafno(String.valueOf(prodCafNo));
		this.setChargeCode(chargeCode);
		this.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
		this.setChargeAmt(srvcAmount);
		this.setSrvcTaxamt(taxesVO.getSrvcTax());
		this.setSwatchTaxamt(taxesVO.getSwatchTax());
		this.setKisanTaxamt(taxesVO.getKissanTax());
		this.setEntTaxamt(taxesVO.getEntTax());
		this.setChargeType(chargeType);
		this.setPaidAmount("0");
		this.setAdjrefId(null);
		this.setChargeDescription("");
		this.setIgnoreBalenceFlag("N");
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAcctCafno() {
		return acctCafno;
	}

	public void setAcctCafno(String acctCafno) {
		this.acctCafno = acctCafno;
	}

	public String getPmntCustId() {
		return pmntCustId;
	}

	public void setPmntCustId(String pmntCustId) {
		this.pmntCustId = pmntCustId;
	}

	public String getChargedDate() {
		return chargedDate;
	}

	public void setChargedDate(String chargedDate) {
		this.chargedDate = chargedDate;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getSrvcCode() {
		return srvcCode;
	}

	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getProdCafno() {
		return prodCafno;
	}

	public void setProdCafno(String prodCafno) {
		this.prodCafno = prodCafno;
	}
	
	public String getStbCafno() {
		return stbCafno;
	}

	public void setStbCafno(String stbCafno) {
		this.stbCafno = stbCafno;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeFdate() {
		return chargeFdate;
	}

	public void setChargeFdate(String chargeFdate) {
		this.chargeFdate = chargeFdate;
	}

	public String getChargeTdate() {
		return chargeTdate;
	}

	public void setChargeTdate(String chargeTdate) {
		this.chargeTdate = chargeTdate;
	}

	public String getChargeAmt() {
		return chargeAmt;
	}

	public void setChargeAmt(String chargeAmt) {
		this.chargeAmt = chargeAmt;
	}

	public String getSrvcTaxamt() {
		return srvcTaxamt;
	}

	public void setSrvcTaxamt(String srvcTaxamt) {
		this.srvcTaxamt = srvcTaxamt;
	}

	public String getSwatchTaxamt() {
		return swatchTaxamt;
	}

	public void setSwatchTaxamt(String swatchTaxamt) {
		this.swatchTaxamt = swatchTaxamt;
	}

	public String getKisanTaxamt() {
		return kisanTaxamt;
	}

	public void setKisanTaxamt(String kisanTaxamt) {
		this.kisanTaxamt = kisanTaxamt;
	}

	public String getEntTaxamt() {
		return entTaxamt;
	}

	public void setEntTaxamt(String entTaxamt) {
		this.entTaxamt = entTaxamt;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getChargeDescription() {
		return chargeDescription;
	}

	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	public String getIgnoreBalenceFlag() {
		return ignoreBalenceFlag;
	}

	public void setIgnoreBalenceFlag(String ignoreBalenceFlag) {
		this.ignoreBalenceFlag = ignoreBalenceFlag;
	}

	public String getRsAgrUid() {
		return rsAgrUid;
	}

	public void setRsAgrUid(String rsAgrUid) {
		this.rsAgrUid = rsAgrUid;
	}

	public String getAdjrefId() {
		return adjrefId;
	}

	public void setAdjrefId(String adjrefId) {
		this.adjrefId = adjrefId;
	}
	
}
