/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

/**
 * @author Lakshman
 *
 */
public class CorpusBillVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String usageLimit;

	private String hsiUsage;
	
	private String throughtlingStatus;

	private String telephoneUsage;
	
	private String statusCode;
	
	private String description;

	public String getUsageLimit() {
		return usageLimit;
	}

	public void setUsageLimit(String usageLimit) {
		this.usageLimit = usageLimit;
	}

	public String getHsiUsage() {
		return hsiUsage;
	}

	public void setHsiUsage(String hsiUsage) {
		this.hsiUsage = hsiUsage;
	}
	
	public String getThroughtlingStatus() {
		return throughtlingStatus;
	}

	public void setThroughtlingStatus(String throughtlingStatus) {
		this.throughtlingStatus = throughtlingStatus;
	}

	public String getTelephoneUsage() {
		return telephoneUsage;
	}

	public void setTelephoneUsage(String telephoneUsage) {
		this.telephoneUsage = telephoneUsage;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
