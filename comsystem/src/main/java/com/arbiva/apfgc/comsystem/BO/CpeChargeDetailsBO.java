/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Lakshman
 *
 */
@Entity
public class CpeChargeDetailsBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "profile_id")
	private Long profileId;
	
	@Column(name = "cpe_model")
	private String cpeModel;
	
	@Column(name = "emicount")
	private Integer emiCount;
	
	@Column(name = "instcharges")
	private BigDecimal instcharges;
	
	@Column(name = "emiamt")
	private BigDecimal emiAmount;
	
	@Column(name = "upfrontcharges")
	private BigDecimal upFrontCharges;
	
	@Column(name = "cpemacaddr")
	private String cpemacAddr;

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getCpeModel() {
		return cpeModel;
	}

	public void setCpeModel(String cpeModel) {
		this.cpeModel = cpeModel;
	}

	public Integer getEmiCount() {
		return emiCount;
	}

	public void setEmiCount(Integer emiCount) {
		this.emiCount = emiCount;
	}

	public BigDecimal getInstcharges() {
		return instcharges;
	}

	public void setInstcharges(BigDecimal instcharges) {
		this.instcharges = instcharges;
	}

	public BigDecimal getEmiAmount() {
		return emiAmount;
	}

	public void setEmiAmount(BigDecimal emiAmount) {
		this.emiAmount = emiAmount;
	}

	public BigDecimal getUpFrontCharges() {
		return upFrontCharges;
	}

	public void setUpFrontCharges(BigDecimal upFrontCharges) {
		this.upFrontCharges = upFrontCharges;
	}

	public String getCpemacAddr() {
		return cpemacAddr;
	}

	public void setCpemacAddr(String cpemacAddr) {
		this.cpemacAddr = cpemacAddr;
	}
	
}
