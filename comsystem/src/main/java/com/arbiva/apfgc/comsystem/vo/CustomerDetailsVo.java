package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;

public class CustomerDetailsVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String subscribercode;
	
	private String servicepack;
	
	private String expirydate;
	

	public String getSubscribercode() {
		return subscribercode;
	}

	public void setSubscribercode(String subscribercode) {
		this.subscribercode = subscribercode;
	}


	public String getServicepack() {
		return servicepack;
	}

	public void setServicepack(String servicepack) {
		this.servicepack = servicepack;
	}

	public String getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(String expirydate) {
		this.expirydate = expirydate;
	}

	public String getPackageCodesList() {
		// TODO Auto-generated method stub
		return this.getServicepack();
	}

}
