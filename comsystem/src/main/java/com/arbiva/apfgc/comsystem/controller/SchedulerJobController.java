package com.arbiva.apfgc.comsystem.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.comsystem.util.ApplictionAutoWires;
import com.arbiva.apfgc.provision.dto.AAAUpdateDTO;
import com.arbiva.apfgc.provision.models.ChangeDevice;

@RestController
public class SchedulerJobController {

	private static final Logger LOGGER = Logger.getLogger(SchedulerJobController.class);

	@Autowired
	ApplictionAutoWires aw;

	@RequestMapping(value = "/mothlyPaymentStoreProcedure")
	public String mothlyPaymentStoreProcedure() {
		String returnValue = "";
		boolean flag;
		try {
			LOGGER.info("Inside SchedulerJobController :: mothlyPaymentStoreProcedure()");
			flag = aw.getStoredProcedureDAO().processRegPayments();
			if (flag)
				returnValue = "Success";
			else
				returnValue = "Failure";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	@RequestMapping(value = "/processProvisioningRequests")
	public String processProvisioningRequests(@RequestParam(value = "cafNo") String cafNo, @RequestParam(value = "cafoutputFlag") int cafoutputFlag ) {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: processIPTVCorpusServicePacks()");
			aw.getProvisioningBusinessServiceImpl().processProvisioningRequests(cafNo, cafoutputFlag);
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	@RequestMapping(value = "/processHSIMultiCastPackages")
	public String processHSIMultiCastPackages() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: processHSIMultiCastPackages()");
			aw.getProvisioningBusinessServiceImpl().processHSIMultiCastPackages();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	//mahesh-16-02-17 for AAA Server
	/*@RequestMapping(value = "/processAAAServerService")
	public String processAAAServerService() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: processAAAServerService()");
			aw.getProvisioningBusinessServiceImpl().processAAAServerService();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}	*/
	
	@RequestMapping(value = "/processIPTVCorpusServicePacks")
	public String processIPTVCorpusServicePacks() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: processIPTVCorpusServicePacks()");
			aw.getProvisioningBusinessServiceImpl().processIPTVCorpusServicePacks();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}

	@RequestMapping(value = "/postProvisioningActivities")
	public String postProvisioningActivities() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: postProvisioningActivities()");
			aw.getProvisioningBusinessServiceImpl().postProvisioningActivities();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}

	/*@RequestMapping(value = "/suspendIPTVCorpusServicePacks")
	public String suspendIPTVCorpusServicePacks() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: suspendIPTVCorpusServicePacks()");
			aw.getProvisioningBusinessServiceImpl().suspendIPTVCorpusServicePacks();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}

	@RequestMapping(value = "/resumeIPTVCorpusServicePacks")
	public String resumeIPTVCorpusServicePacks() {

		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: resumeIPTVCorpusServicePacks()");
			aw.getProvisioningBusinessServiceImpl().resumeIPTVCorpusServicePacks();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}

	@RequestMapping(value = "/deActivateIPTVCorpusServicePacks")
	public String deActivateIPTVCorpusServicePacks() {
		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: deActivateIPTVCorpusServicePacks()");
			aw.getProvisioningBusinessServiceImpl().deActivateIPTVCorpusServicePacks();
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}*/
	
	@RequestMapping(value = "/changeCpeProvForSTB",method = RequestMethod.POST)
	public String changeCpeProv(@RequestBody ChangeDevice changeDevice) {
		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: changeCpeProvForSTB()");
			aw.getProvisioningBusinessServiceImpl().changeCpeProvForSTB(changeDevice);
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::changeCpeProvForSTB(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	@RequestMapping(value = "/changeCpeProvForONU",method = RequestMethod.POST)
	public String changeCpeProvForONU(@RequestBody ChangeDevice changeDevice) {
		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: changeCpeProvForONU()");
			aw.getProvisioningBusinessServiceImpl().changeCpeProvForONU(changeDevice);
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::changeCpeProvForONU(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	@RequestMapping(value = "/processDeleteRequests",method = RequestMethod.POST)
	public String processDeleteRequests(@RequestParam (value = "cafNo") String cafNo) {
		String returnValue = "";
		try {
			LOGGER.info("Inside SchedulerJobController :: processDeleteRequests()");
			aw.getProvisioningBusinessServiceImpl().processDeleteRequests(cafNo);
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::processDeleteRequests(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	@RequestMapping(value="/processAAAUpdationRequests",method = RequestMethod.POST)
	public String processUpdationRequests(@RequestBody AAAUpdateDTO updateDTO){
		String returnValue ="";
		try {
			LOGGER.info("Inside SchedulerJobController :: processUpdationRequests()");
			aw.getProvisioningBusinessServiceImpl().processUpdationRequests(updateDTO);
			returnValue = "Success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::processUpdationRequests(): " + e);
			e.printStackTrace();
		}		
		return returnValue;		
	}
	
	//added 17-04-2017 for Base Package Change
	@RequestMapping(value="/basePackageChange",method=RequestMethod.POST)
	public String basePackageChange(@RequestParam (value="prodCafNo") String prodCafNo, @RequestParam (value="prodCode") String prodCode){
		String returnValue="";
		try{
			LOGGER.info("Inside SchedulerJobController :: basePackageChange()");
			returnValue = aw.getProvisioningBusinessServiceImpl().basePackageChange(prodCafNo,prodCode);
		}
		catch(Exception e){
			LOGGER.error("error in SchedulerJobController::basePackageChange(): " + e);
			e.printStackTrace();
		}
		return returnValue;		
	}
	
	@RequestMapping(value = "/packageTermination", method = RequestMethod.POST)
	public String packageTermination(@RequestParam(value = "cafNo") String cafNo,
			@RequestParam(value = "prodCode") String prodCode, @RequestParam(value = "stbCafNo") String stbCafNo,
			@RequestParam(value = "nwSubsCode") String nwSubsCode) {
		String returnValue = "false";
		try {
			LOGGER.info("Inside SchedulerJobController :: packageTermination()");
			returnValue = aw.getProvisioningBusinessServiceImpl().packageTermination(cafNo, prodCode, stbCafNo, nwSubsCode);
		} catch (Exception e) {
			LOGGER.error("error in SchedulerJobController::packageTermination(): " + e);
			e.printStackTrace();
		}
		return returnValue;
	}
	
	//added 15/06/17-for AAA request
	@RequestMapping(value = "/requestForAAA", method = RequestMethod.POST)
	public String requestForAAA(@RequestParam(value = "cafNo") String cafNo) {
		String status = " ";
		try {
			LOGGER.info("Inside SchedulerJobController :: requestForAAA()");
			status = aw.getProvisioningBusinessServiceImpl().requestForAAA(cafNo);
		} catch (Exception e) {
			LOGGER.error("error in SchedulerJobController::requestForAAA(): " + e);
			e.printStackTrace();
		}

		return status;
	}
}