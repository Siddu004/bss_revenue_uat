/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="oltportdtls")
@IdClass(OLTPortDetailsPK.class)
public class OLTPortDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pop_olt_serialno")
	private String popOltSerialno;
	
	@Id
	@Column(name="cardid")
	private int cardId;
	
	@Id
	@Column(name="portno")
	private Integer portNo;
	
	@Column(name="lmocode")
	private String lmocode;
	
	@Column(name="slots")
	private String slots;
	
	@Column(name="l1slots")
	private String l1Slots;
	
	@Column(name="l3slotsavlbl")
	private String l3Slotsavlbl;
	
	@Column(name="l3slotsused")
	private String l3SlotsUsed;

	@ManyToOne
	@JoinColumn(name = "pop_olt_serialno", referencedColumnName = "pop_olt_serialno", nullable = false, insertable=false, updatable=false )
	private OLT olt;
	
	public OLT getOlt() {
		return olt;
	}

	public void setOlt(OLT olt) {
		this.olt = olt;
	}

	public String getPopOltSerialno() {
		return popOltSerialno;
	}

	public void setPopOltSerialno(String popOltSerialno) {
		this.popOltSerialno = popOltSerialno;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Integer getPortNo() {
		return portNo;
	}

	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}

	public String getLmocode() {
		return lmocode;
	}

	public void setLmocode(String lmocode) {
		this.lmocode = lmocode;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}

	public String getL1Slots() {
		return l1Slots;
	}

	public void setL1Slots(String l1Slots) {
		this.l1Slots = l1Slots;
	}

	public String getL3Slotsavlbl() {
		return l3Slotsavlbl;
	}

	public void setL3Slotsavlbl(String l3Slotsavlbl) {
		this.l3Slotsavlbl = l3Slotsavlbl;
	}

	public String getL3SlotsUsed() {
		return l3SlotsUsed;
	}

	public void setL3SlotsUsed(String l3SlotsUsed) {
		this.l3SlotsUsed = l3SlotsUsed;
	}
	
}
