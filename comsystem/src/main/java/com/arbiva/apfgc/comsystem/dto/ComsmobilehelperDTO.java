package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;

public class ComsmobilehelperDTO implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BulkCustomerDTO[] customerslist;
	
	//private String actualUserAmount;
	
	private String lmoWalletBalence;
	
	private String creditLimit;

	    public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

		public String getLmoWalletBalence() {
		return lmoWalletBalence;
	}

	public void setLmoWalletBalence(String lmoWalletBalence) {
		this.lmoWalletBalence = lmoWalletBalence;
	}

		/*public String getActualUserAmount() {
		return actualUserAmount;
	}

	public void setActualUserAmount(String actualUserAmount) {
		this.actualUserAmount = actualUserAmount;
	}*/

		public BulkCustomerDTO[] getCustomerslist ()
	    {
	        return customerslist;
	    }

	    public void setCustomerslist (BulkCustomerDTO[] customerslist)
	    {
	        this.customerslist = customerslist;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [customerslist = "+customerslist+"]";
	    }

}
