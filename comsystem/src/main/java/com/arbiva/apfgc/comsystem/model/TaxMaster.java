/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="taxmast")
@IdClass(TaxMasterPK.class)
public class TaxMaster extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "regioncode")
	private String regionCode;
	
	@Id
	@Column(name = "taxcode")
	private String taxCode;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectiveFrom;
	
	@Column(name = "effectiveto")
	private Date effectiveTo;
	
	@Column(name = "perc_or_abs")
	private Character percAbs;
	
	@Column(name = "taxperc")
	private BigDecimal taxPerc;

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public Character getPercAbs() {
		return percAbs;
	}

	public void setPercAbs(Character percAbs) {
		this.percAbs = percAbs;
	}

	public BigDecimal getTaxPerc() {
		return taxPerc;
	}

	public void setTaxPerc(BigDecimal taxPerc) {
		this.taxPerc = taxPerc;
	}
	
	
}
