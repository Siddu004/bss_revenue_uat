/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.comsystem.BO.BalAdjCafinvDtslBO;
import com.arbiva.apfgc.comsystem.BO.BalanceAdjustmentBO;
import com.arbiva.apfgc.comsystem.BO.CafOldPackagesBO;
import com.arbiva.apfgc.comsystem.BO.ChangePkgBO;
import com.arbiva.apfgc.comsystem.BO.CustomerCafBO;
import com.arbiva.apfgc.comsystem.BO.CustomerDetailsBO;
import com.arbiva.apfgc.comsystem.BO.FingerPrintBO;
import com.arbiva.apfgc.comsystem.BO.HsiBO;
import com.arbiva.apfgc.comsystem.BO.LmoDetailsBO;
import com.arbiva.apfgc.comsystem.BO.MonthlyPaymentBO;
import com.arbiva.apfgc.comsystem.BO.NoOfSTBsBO;
import com.arbiva.apfgc.comsystem.BO.OntIdCustTypeBO;
import com.arbiva.apfgc.comsystem.BO.TerminatePkgBO;
import com.arbiva.apfgc.comsystem.aadhar.ServicesPortTypeProxy;
import com.arbiva.apfgc.comsystem.dao.impl.CafDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafProductsDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafServicesDao;
import com.arbiva.apfgc.comsystem.dao.impl.CustomerDao;
import com.arbiva.apfgc.comsystem.dao.impl.OLTPortDetailsDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.dto.AadharDTO;
import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.CpeStock;
import com.arbiva.apfgc.comsystem.model.Cpecharges;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.Districts;
import com.arbiva.apfgc.comsystem.model.EntCafStage;
import com.arbiva.apfgc.comsystem.model.HSICummSummaryMonthlyCustViewDTO;
import com.arbiva.apfgc.comsystem.model.HSICummSummaryMonthlyViewDTO;
import com.arbiva.apfgc.comsystem.model.Lovs;
import com.arbiva.apfgc.comsystem.model.Mandals;
import com.arbiva.apfgc.comsystem.model.MultiAction;
import com.arbiva.apfgc.comsystem.model.OSDFingerPrintDetails;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNamesStage;
import com.arbiva.apfgc.comsystem.model.invAdjustment;
import com.arbiva.apfgc.comsystem.service.CPEInformationService;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.EnterpriseCustomerService;
import com.arbiva.apfgc.comsystem.service.OSDFingerPrintDetailsService;
import com.arbiva.apfgc.comsystem.service.UploadHistoryService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.util.FileUtil;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.BillingInfoVO;
import com.arbiva.apfgc.comsystem.vo.CafAndCpeChargesVO;
import com.arbiva.apfgc.comsystem.vo.CafsVO;
import com.arbiva.apfgc.comsystem.vo.CorpusFingerPrint;
import com.arbiva.apfgc.comsystem.vo.CorpusOSD;
import com.arbiva.apfgc.comsystem.vo.CorpusResponce;
import com.arbiva.apfgc.comsystem.vo.CorpusScrollText;
import com.arbiva.apfgc.comsystem.vo.CpeInformationVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.DataMap;
import com.arbiva.apfgc.comsystem.vo.FeatureParamsVO;
import com.arbiva.apfgc.comsystem.vo.FilterPackagesVO;
import com.arbiva.apfgc.comsystem.vo.FingerPrintJson;
import com.arbiva.apfgc.comsystem.vo.GenarateChargesVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSumPMonthVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthly;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthlyCustViewVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryPreviousMonthVO;
import com.arbiva.apfgc.comsystem.vo.InvoiceAmountDtls;
import com.arbiva.apfgc.comsystem.vo.OSDDataMap;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;
import com.arbiva.apfgc.comsystem.vo.ScrollTextDataMap;
import com.arbiva.apfgc.comsystem.vo.ServicesVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneNoVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneServcVO;
import com.arbiva.apfgc.comsystem.vo.VPNServiceExcelUploadVO;
import com.arbiva.apfgc.comsystem.vo.chargeTypesVO;
import com.google.gson.Gson;

import generated.Java;

/**
 * @author Lakshman
 *
 */
@Service
public class CafServiceImpl implements CafService {
	
	private static final Logger logger = Logger.getLogger(CafServiceImpl.class);

	@Value("${corpusCallSave}")
	private String corpusCallSave;
	
	@Value("${corpusUserName}")
	private String corpusUserName;
	
	@Value("${corpusPassword}")
	private String corpusPassword;
	
	@Autowired
	LovsServiceImpl lovsServiceImpl;
	
	@Autowired
	CafDao cafDao;

	@Autowired
	CafProductsDao cafProductsDao;

	@Autowired
	CafServicesDao cafServicesDao;
	
	@Autowired
	CustomerDao customerDao;

	@Autowired
	CafProductsService cafProductsService;

	@Autowired
	CafServicesService cafServicesService;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	OLTPortDetailsDao oltPortDetailsDao;

	@Autowired
	CPEInformationService cpeInformationService;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Autowired
	LovsServiceImpl lovsService;

	@Autowired
	OSDFingerPrintDetailsService oSDFingerPrintDetailsService;
	
	@Autowired
	EnterpriseCustomerService enterpriseCustomerService;
	
	@Autowired
	CafFeatureParamsService cafFeatureParamsService;
	
	@Autowired
	UploadHistoryService uploadHistoryService;

	@Override
	@Transactional
	public void saveCaf(CustomerCafVO customerCafVO, Long cafNo, Long customerId, String loginID, Integer custDistrict) {
		Caf caf = null;
		try {
			caf = new Caf(customerCafVO);
			caf.setCafNo(cafNo);
			caf.setCpeLeaseyn('N');
			if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				caf.setPmntCustCode(customerCafVO.getAadharNumber());
				caf.setPmntCustId(customerId);
				caf.setCustCode(customerCafVO.getAadharNumber());
				caf.setCustId(customerId);
				caf.setCustdistUid(custDistrict);
			} else {
				Long pmntCustId = storedProcedureDAO.executePaymentCustomer(customerId);
				caf.setCustdistUid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
				caf.setPmntCustCode(pmntCustId.toString());
				caf.setPmntCustId(pmntCustId);
				caf.setCustCode(customerCafVO.getEntCustomerCode());
				caf.setCustId(Long.parseLong(customerCafVO.getEntCustomerCode()));
			}
			caf.setLmoDeclaration(customerCafVO.getLmoDec() != null ? 'Y' : 'N');
			caf.setCustomerDeclaration(customerCafVO.getCustomerDec() != null ? 'Y' : 'N');
			caf.setCafDate(Calendar.getInstance());
			caf.setCreatedBy(loginID);
			caf.setModifiedBy(loginID);
			caf.setCreatedOn(Calendar.getInstance());
			caf.setModifiedOn(Calendar.getInstance());
			caf.setCreatedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			caf.setModifiedIPAddr(customerCafVO.getIpAddress() != null ? customerCafVO.getIpAddress(): httpServletRequest.getRemoteAddr());
			caf.setStatus(customerCafVO.getCafStatus().equalsIgnoreCase(ComsEnumCodes.Customer_Caf_BulkUpload_status.getCode()) || customerCafVO.getCafStatus().equalsIgnoreCase(ComsEnumCodes.Customer_Caf_Edit_BulkUpload_status.getCode())? ComsEnumCodes.Caf_Edit_BulkUpload_status.getStatus() : ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
			caf.setInstState("Andhra Pradesh");
			caf.setApsflUniqueId(customerCafVO.getApsflUniqueId());
			caf = cafDao.saveCaf(caf);
			customerCafVO.setCafStatus(""+caf.getStatus());
		} catch (Exception e) {
			logger.error("CafServiceImpl::saveCaf() " + e);
			e.printStackTrace();
		} finally {
			caf = null;
		}
	}

	@Override
	@Transactional
	public Caf findByCafNo(Long cafNo) {
		Caf caf = new Caf();
		try {
			caf = cafDao.findByCafNo(cafNo);
		} catch (Exception e) {
			logger.error("CafServiceImpl::findByCafNo() " + e);
			e.printStackTrace();
		} finally {

		}
		return caf;
	}
	
	@Override
	@Transactional
	public List<Caf> findListedCafs(List<Long> cafLists) {
		 List<Caf> cafList=null;
		try {
			cafList = cafDao.findListedCafs(cafLists);
		} catch (Exception e) {
			logger.error("CafServiceImpl::findByCafNo() " + e);
			e.printStackTrace();
		} finally {

		}
		return cafList;
	}

	@Override
	public List<CafAndCpeChargesVO> searchCafDetails(MultiAction multiAction, PageObject pageObject) {
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<Object[]> paymentList = new ArrayList<>();
		String iptvPackageCode = "";
		try {
			paymentList = cafDao.searchCafDetails(multiAction, pageObject);
			for (Object[] payment : paymentList) {
				CafAndCpeChargesVO cafAndCpeChargesVO = new CafAndCpeChargesVO();
				String coreSrvcCode = this.getCoreSrvcCodeByCafNo(Long.parseLong(payment[0].toString()));
				if(multiAction.getTenantType().equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
					List<Object[]> objectCafProductsList = cafProductsDao.getCafProducts(Long.parseLong(payment[0].toString()));
					for(Object[] object : objectCafProductsList) {
						if(object[2] != null && !object[2].toString().equalsIgnoreCase("B")) {
							if(iptvPackageCode.isEmpty()) {
								iptvPackageCode = object[0].toString()+"^"+object[1].toString();
							} else {
								iptvPackageCode = iptvPackageCode+","+object[0].toString()+"^"+object[1].toString();
							}
						}
					}
				}
				cafAndCpeChargesVO.setIptvPackages(iptvPackageCode);
				cafAndCpeChargesVO.setCoreSrvcCode(coreSrvcCode != null ? coreSrvcCode : "NA");
				cafAndCpeChargesVO.setCafNo(payment[0].toString());
				cafAndCpeChargesVO.setCustCode(payment[1].toString());
				cafAndCpeChargesVO.setCafdate(payment[2].toString());
				if(multiAction.getTenantType().equalsIgnoreCase("SI") && payment[3].toString().equalsIgnoreCase("PENDING FOR SELECT PACKAGES")) {
					cafAndCpeChargesVO.setStatusDesc("FAILED CONNECTIONS");
				} else {
					cafAndCpeChargesVO.setStatusDesc(payment[3].toString());
				}
				cafAndCpeChargesVO.setfName(payment[4] == null ? "" : payment[4].toString());
				cafAndCpeChargesVO.setProdName(payment[5] == null ? "--" : payment[5].toString());
				cafAndCpeChargesVO.setlName(payment[6] == null ? "" : payment[6].toString());
				cafAndCpeChargesVO.setCustTypelov(payment[7] != null ? payment[7].toString() : "");
				cafAndCpeChargesVO.setPopId(payment[8] == null ? "NA" : payment[8].toString());
				cafAndCpeChargesVO.setCustId(payment[9].toString());
				cafAndCpeChargesVO.setVillage(payment[10] == null ? null : payment[10].toString());
				cafAndCpeChargesVO.setDistrict(payment[11] == null ? null : payment[11].toString());
				cafAndCpeChargesVO.setMandal(payment[12] == null ? null : payment[12].toString());
				cafAndCpeChargesVO.setBillFrequency(payment[13] == null ? "" : payment[13].toString());
				cafAndCpeChargesVO.setCafStatus(payment[14] == null ? null : payment[14].toString());
				cafAndCpeChargesVO.setTenantCode(payment[15].toString());
				cafAndCpeChargesVO.setPopName(payment[16] == null ? "NA" : payment[16].toString());
				cafAndCpeChargesVO.setApsflTrackId(payment[17] == null ? "NA" : payment[17].toString());
				cafAndCpeChargesVO.setInsrAddress1(payment[18] == null ? "" : payment[18].toString());
				cafAndCpeChargesVO.setInsrAddress2(payment[19] == null ? "" : payment[19].toString());
				cafAndCpeChargesVO.setContactPersonMobileNo(payment[20] == null ? "" : payment[20].toString());
				cafAndCpeChargesVO.setCpeplace(payment[21] == null ? "" : payment[21].toString());
				cafAndCpeChargesVO.setContactPerson(payment[22] == null ? "" : payment[22].toString());
				cafAndCpeChargesVO.setAllocatedMobileNo(payment[23] == null ? "NA" : payment[23].toString());
				cafAndCpeChargesVO.setCafType(payment[24] == null ? "NA" : payment[24].toString());
				cafAndCpeChargesVO.setTenantType(multiAction.getTenantType());
				cafAndCpeChargesVO.setProdCafNo(payment[25] == null ? null : payment[25].toString());
				cafAndCpeChargesVO.setOltPortNo(payment[26] == null ? null : payment[26].toString());
				if(pageObject != null) {
					cafAndCpeChargesVO.setTotalDisplayCount(pageObject.getTotalDisplayCount());
				}
				cafAndCpeChargesList.add(cafAndCpeChargesVO);
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: searchCafDetails" + e);
			e.printStackTrace();
		} finally {
			paymentList = null;
		}
		return cafAndCpeChargesList;
	}

	@Override
	public CpeInformationVO getPaymentDetails(Long cafNo, String billCycle) {
		CafAndCpeChargesVO cafAndCpeChargesVO = null;
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<Object[]> paymentList = new ArrayList<>();
		BigDecimal totalCharge = new BigDecimal("0");
		BigDecimal securityDepositCharge = new BigDecimal("0");
		try {
			paymentList = cafDao.getPaymentDetails(cafNo);
			for (Object[] payment : paymentList) {
				cafAndCpeChargesVO = new CafAndCpeChargesVO();
				cafAndCpeChargesVO.setProdCode(payment[10].toString());
				cafAndCpeChargesVO.setProdName(payment[0].toString());
				cafAndCpeChargesVO.setSrvcName(payment[1].toString());
				if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_MONTHLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[8].toString());
				} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_QUARTERLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[8].toString()).multiply(new BigDecimal("3"));
				} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_HALFYEARLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[8].toString()).multiply(new BigDecimal("6"));
				} else {
					securityDepositCharge = new BigDecimal(payment[8].toString()).multiply(new BigDecimal("12"));
				}
				cafAndCpeChargesVO.setActivationCharge(payment[6].toString());
				cafAndCpeChargesVO.setActivationTax(payment[7].toString());
				cafAndCpeChargesVO.setSecDepositCharge(securityDepositCharge.toString());
				cafAndCpeChargesVO.setSecDepositTax(payment[9].toString());
				cafAndCpeChargesVO.setProdtype(payment[4].toString());
				cafAndCpeChargesVO.setCafdate(payment[5] != null ? payment[5].toString() : "NA");
				cafAndCpeChargesVO.setRecurringCharge(payment[2].toString());
				cafAndCpeChargesVO.setRecurringTax(payment[3].toString());
				cafAndCpeChargesVO.setAgrmtId(payment[12].toString());
				cafAndCpeChargesVO.setTenantCode(payment[11].toString());
				totalCharge = totalCharge.add(new BigDecimal(securityDepositCharge.toString())).add(new BigDecimal(payment[6].toString())).add(new BigDecimal(payment[7].toString()));
				cafAndCpeChargesList.add(cafAndCpeChargesVO);
			}
			cpeInformationVO.setTotalCharge(totalCharge.toString());
			cpeInformationVO.setCafAndCpeChargesList(cafAndCpeChargesList);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getPaymentDetails" + e);
			e.printStackTrace();
		} finally {
			cafAndCpeChargesList = null;
			cafAndCpeChargesVO = null;
			paymentList = null;
			totalCharge = null;
		}
		return cpeInformationVO;
	}

	@Override
	public List<MonthlyPaymentBO> getMonthlyCafDetails(String mobileNo, String tenantCode, Integer custId) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		try {
			cafList = cafDao.getMonthlyCafDetails(mobileNo, tenantCode, custId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMonthlyCafDetails" + e);
			e.printStackTrace();
		} finally {

		}
		return cafList;
	}

	@Override
	public List<Object[]> getCustomerInformationByTenantCode(String tenantCode) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			customerList = cafDao.getCustomerInformationByTenantCode(tenantCode);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCustomerInformationByTenantCode" + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}
	
	public AadharDTO getAadharDetails(String aadharNumber) throws RemoteException {
		AadharDTO aadharDTO = new AadharDTO();
		// Services services = new Services();
		// ServicesPortType service = services.getServicesSOAP11PortHttp();
		ServicesPortTypeProxy service = new ServicesPortTypeProxy();
		String aadharInfo = "";
		StringReader reader = null;
		try {
			aadharInfo = service.getAadhaarInfo(aadharNumber, aadharNumber);
			reader = new StringReader(aadharInfo);
			JAXBContext jaxbContext = JAXBContext.newInstance(Java.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Java javaObj = (Java) jaxbUnmarshaller.unmarshal(reader);
			generated.Java.Object obj = (generated.Java.Object) javaObj.getObject();
			List<Java.Object.Void> list = obj.getVoid();
			for (Java.Object.Void void1 : list) {
				if (void1.getProperty().equalsIgnoreCase("base64file")) {
					aadharDTO.setBase64file(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("buildingName")) {
					aadharDTO.setBuildingName(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("careof")) {
					aadharDTO.setCareof(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("district")) {
					aadharDTO.setDistrict(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("district_name")) {
					aadharDTO.setDistrict_name(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("dob")) {
					aadharDTO.setDob(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("eid")) {
					aadharDTO.setEid(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("gender")) {
					aadharDTO.setGender(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("mandal")) {
					aadharDTO.setMandal(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("mandal_name")) {
					aadharDTO.setMandal_name(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("name")) {
					aadharDTO.setName(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("phoneNo")) {
					aadharDTO.setPhoneNo(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("pincode")) {
					aadharDTO.setPincode(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("srdhwstxn")) {
					aadharDTO.setSrdhwstxn(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("statecode")) {
					aadharDTO.setStatecode(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("status")) {
					aadharDTO.setStatus(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("street")) {
					aadharDTO.setStreet(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("uid")) {
					aadharDTO.setUid(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("village")) {
					aadharDTO.setVillage(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				} else if (void1.getProperty().equalsIgnoreCase("village_name")) {
					aadharDTO.setVillage_name(void1.getString().equalsIgnoreCase("101") ? "" : void1.getString());
				}
			}
		} catch (JAXBException e) {
			logger.error("The Exception is CafServiceImpl :: getAadharDetails" + e);
			e.printStackTrace();
		} finally {
			service = null;
			aadharInfo = null;
			reader = null;
		}
		return aadharDTO;
	}

	@Override
	public Integer getMaxOltOnuNumber(String oltPortId, String oltId) {
		Integer maxOltNumber = null;
		try {
			maxOltNumber = cafDao.getMaxOltOnuNumber(oltPortId, oltId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMaxOltOnuNumber" + e);
			e.printStackTrace();
		} finally {

		}
		return maxOltNumber;
	}

	@Override
	public Float getProdChargeByRegionCode(String tenantCode, String prodCode, String regionCode) {
		Float prodChrg = null;
		try {
			prodChrg = cafDao.getProdChargeByRegionCode(tenantCode, prodCode, regionCode);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMaxOltOnuNumber" + e);
			e.printStackTrace();
		} finally {

		}
		return prodChrg;
	}

	@Override
	public String getRegionCodeByPinCode(String district, String mandal, String village) {
		String regionCode = "";
		try {
			regionCode = cafDao.getRegionCodeByPinCode(district, mandal, village);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMaxOltOnuNumber" + e);
			e.printStackTrace();
		} finally {

		}
		return regionCode;
	}
	
	@Override
	public Object[] getAllParticularCPE(Long cafNo) {
		Object[] object = null;
		try {
			object = cafDao.getAllParticularCPE(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getAllParticularCPE" + e);
			e.printStackTrace();
		} finally {

		}
		return object;
	}

	@Override
	public List<Object[]> getCafChages(Long cafNo) {
		List<Object[]> objectList = new ArrayList<>();
		try {
			objectList = cafDao.getCafCharges(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCafChages" + e);
			e.printStackTrace();
		} finally {

		}
		return objectList;
	}
	
	@Override
	public List<Object[]> getCafFeatureService(Long cafNo) {
		List<Object[]> objectList = new ArrayList<>();
		try {
			objectList = cafDao.getCafFeatureService(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCafFeatureService" + e);
			e.printStackTrace();
		} finally {

		}
		return objectList;
	}

	@Override
	public Object[] getCafChargeTaxPecentage(String chargeCode, String district, String mandal, String village) {
		Object[] object = null;
		try {
			object = cafDao.getCafChargeTaxPecentage(chargeCode, district, mandal, village);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCafChargeTaxPecentage" + e);
			e.printStackTrace();
		} finally {

		}
		return object;
	}

	/*@Override
	public List<Object[]> findCafDaoByCafNo(Long custId) {
		List<Object[]> objectList = new ArrayList<>();
		try {
			objectList = cafDao.findCafDaoByCafNo(custId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: findCafDaoByCafNo" + e);
			e.printStackTrace();
		} finally {

		}
		return objectList;
	}*/

	@Override
	public CustomerCafVO getPackageInformation(Long cafNumber, String tenantType) {
		CustomerCafVO customerCafVO = new CustomerCafVO();
		Object[] packageObject = null;
		String coreSrvcCode = "";
		String iptvPackageCode = "";
		try {
			packageObject = cafDao.getPackageInformation(cafNumber);
			coreSrvcCode = this.getCoreSrvcCodeByCafNo(cafNumber);
			customerCafVO.setCustId(Integer.parseInt(packageObject[0].toString()));
			customerCafVO.setCustType(packageObject[1].toString());
			customerCafVO.setCafNo(Long.parseLong(packageObject[2].toString()));
			customerCafVO.setCustCode(packageObject[3].toString());
			customerCafVO.setAadharNumber(packageObject[3].toString());
			customerCafVO.setFirstName(packageObject[4].toString());
			customerCafVO.setLastName(packageObject[5] != null ? packageObject[5].toString() : "");
			customerCafVO.setPinCode(packageObject[6] != null ? packageObject[6].toString() : "");
			customerCafVO.setBillCycle(packageObject[7] != null ? packageObject[7].toString() : null);
			customerCafVO.setDistrict(packageObject[8].toString());
			customerCafVO.setCity(packageObject[9].toString());
			customerCafVO.setMandal(packageObject[10].toString());
			customerCafVO.setStatus(packageObject[11].toString());
			customerCafVO.setLmoCode(packageObject[12].toString());
			customerCafVO.setPopId(packageObject[13] != null ? packageObject[13].toString() : "");
			customerCafVO.setCustCode(packageObject[14].toString());
			customerCafVO.setEntCustType(packageObject[15] != null ? packageObject[15].toString() : "");
			customerCafVO.setCafStatus(packageObject[16].toString());
			customerCafVO.setCoreSrvcCode(coreSrvcCode);
			customerCafVO.setPopDistrict(packageObject[17] != null ? packageObject[17].toString() : null);
			customerCafVO.setPopMandal(packageObject[18] != null ? packageObject[18].toString() : null);
			customerCafVO.setCustDistrictId(packageObject[19] != null ? packageObject[19].toString() : null);
			if(tenantType.equalsIgnoreCase(ComsEnumCodes.SI_Tenant_Type.getCode())) {
				List<Object[]> objectCafProductsList = cafProductsDao.getCafProducts(cafNumber);
				for(Object[] object : objectCafProductsList) {
					if(object[2] != null && !object[2].toString().equalsIgnoreCase("B")) {
						if(iptvPackageCode.isEmpty()) {
							iptvPackageCode = object[0].toString()+"^"+object[1].toString();
						} else {
							iptvPackageCode = iptvPackageCode+","+object[0].toString()+"^"+object[1].toString();
						}
					}
				}
				customerCafVO.setIptvPackage(iptvPackageCode);
				Object[] filterPkgDtls = cafProductsDao.getFilterPackageDetails(cafNumber);
				customerCafVO.setProdCode(filterPkgDtls[0].toString());
				customerCafVO.setTenantCode(filterPkgDtls[1].toString());
				customerCafVO.setAgruniqueid(filterPkgDtls[2].toString());
				List<Object[]> cafdtls=cafDao.getCafInfo(cafNumber.toString());
				for(Object[] i: cafdtls) {
					customerCafVO.setCpeId(i[1].toString());
				}
				
			}
			
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getPackageInformation" + e);
			e.printStackTrace();
		} finally {
			packageObject = null;
		}
		return customerCafVO;
	}

	@Override
	public String getSubCode(Long cafNo) {
		String stdCode = "";
		try {
			stdCode = cafDao.getSubCode(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getSubCode" + e);
			e.printStackTrace();
		} finally {

		}
		return stdCode;
	}

	@Override
	public String corpusCall(FingerPrintJson fingerPrintJson) {
		Gson gson = new Gson();
		String status = "";
		Integer jsonSeqNo = 0;
		String url = "";
		HttpHeaders headers = new HttpHeaders();
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		List<String> subscriberCodeList = new ArrayList<>();
		CorpusFingerPrint corpusFingerPrint = new CorpusFingerPrint();
		CorpusOSD corpusOSD = new CorpusOSD();
		CorpusScrollText corpusScrollText = new CorpusScrollText();
		DataMap dataMap = null;
		OSDDataMap osdDataMap = null;
		ScrollTextDataMap scrollTextDataMap = null;
		HttpEntity<String> httpEntity;
		ResponseEntity<String> response;
		String corpusStatus = "";
		CorpusResponce corpusResponce = new CorpusResponce();
		String requestJson = "";
		String responseJson = "";
		OSDFingerPrintDetails oSDFingerPrintDetails = new OSDFingerPrintDetails();
		try {
			headers.add("username", corpusUserName);
			headers.add("apikey", corpusPassword);
			url = corpusCallSave;
			Float subScriberCount = new Float(fingerPrintJson.getSelectSubScribersCodes().length) / 5000;
			int begin = 0;
			int end = 0;
			for (Float i = new Float("0"); i < subScriberCount; i++) {
				jsonSeqNo++;
				subscriberCodeList.clear();
				begin = end == 0 ? 0 : end + 1;
				if (fingerPrintJson.getSelectSubScribersCodes().length > 5000) {
					if (end < fingerPrintJson.getSelectSubScribersCodes().length - end)
						end = end == 0 ? 4999 : end + 5000;
					else
						end = fingerPrintJson.getSelectSubScribersCodes().length - 1;
				} else {
					end = fingerPrintJson.getSelectSubScribersCodes().length - 1;
				}
				subscriberCodeList.addAll(Arrays.asList(fingerPrintJson.getSelectSubScribersCodes()).subList(begin, end + 1));
				if (fingerPrintJson.getFpStatus().equalsIgnoreCase("F")) {
					dataMap = new DataMap();
					corpusFingerPrint.setSubscriberCodes(subscriberCodeList);
					corpusFingerPrint.setModule("DRM");
					corpusFingerPrint.setCommand("FINGER_PRINT");
					dataMap.setDuration(fingerPrintJson.getFpDuration());
					dataMap.setFontColor(fingerPrintJson.getFpFontColor());
					dataMap.setFontSize(fingerPrintJson.getFpFontSize());
					dataMap.setFontType(fingerPrintJson.getFpFontType());
					dataMap.setPosition(fingerPrintJson.getFpPosition());
					dataMap.setFingerPrintType(fingerPrintJson.getFpfingerPrintType());
					dataMap.setChannel(fingerPrintJson.getFpChannel().equalsIgnoreCase("0") ? null: fingerPrintJson.getFpChannel());
					dataMap.setBgColor(fingerPrintJson.getFpBgColor());
					corpusFingerPrint.setDataMap(dataMap);
					
					requestJson = gson.toJson(corpusFingerPrint);
					httpEntity = new HttpEntity<String>(requestJson, headers);
					response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
					// response.getBody();
					corpusStatus = response.getBody();
					corpusResponce = mapper.readValue(corpusStatus, CorpusResponce.class);
					responseJson = gson.toJson(corpusResponce);
					if (corpusResponce.getResponseStatus().getStatusCode().equalsIgnoreCase("202")) {
						oSDFingerPrintDetails = oSDFingerPrintDetailsService.saveOSDFingerPrintDetails(corpusFingerPrint.getSubscriberCodes().toString(), requestJson, responseJson, fingerPrintJson.getLoginId(), jsonSeqNo, "FINGER_PRINT", corpusResponce.getTrackingId(), "NA");
						if (oSDFingerPrintDetails != null)
							status = "Finger Print Request sent Successfully";
						else {
							status = "Finger Print Request Failed";
						}
					} else {
						status = "Finger Print Request Failed";
					}
				} else if (fingerPrintJson.getFpStatus().equalsIgnoreCase("O")) {
					osdDataMap = new OSDDataMap();
					corpusOSD.setSubscriberCodes(subscriberCodeList);
					corpusOSD.setModule("DRM");
					corpusOSD.setCommand("OSD");
					osdDataMap.setDuration(fingerPrintJson.getFpDuration());
					osdDataMap.setBgColor(fingerPrintJson.getFpBgColor());
					osdDataMap.setFontColor(fingerPrintJson.getFpFontColor());
					osdDataMap.setFontSize(fingerPrintJson.getFpFontSize());
					osdDataMap.setFontType(fingerPrintJson.getFpFontType());
					osdDataMap.setPosition(fingerPrintJson.getFpPosition());
					osdDataMap.setMessage(fingerPrintJson.getFpMessage());
					osdDataMap.setUserCanCloseMessage(fingerPrintJson.getFpUserCanCloseMessage());
					corpusOSD.setDataMap(osdDataMap);

					requestJson = gson.toJson(corpusOSD);
					httpEntity = new HttpEntity<String>(requestJson, headers);
					response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
					// response.getBody();
					corpusStatus = response.getBody();
					corpusResponce = mapper.readValue(corpusStatus, CorpusResponce.class);
					responseJson = gson.toJson(corpusResponce);
					if (corpusResponce.getResponseStatus().getStatusCode().equalsIgnoreCase("202")) {
						oSDFingerPrintDetails = oSDFingerPrintDetailsService.saveOSDFingerPrintDetails(corpusOSD.getSubscriberCodes().toString(), requestJson, responseJson, fingerPrintJson.getLoginId(), jsonSeqNo, "OSD", corpusResponce.getTrackingId(), corpusOSD.getDataMap().getMessage());
						if (oSDFingerPrintDetails != null)
							status = "OSD Request sent Successfully";
						else {
							status = "OSD Request Failed";
						}
					} else {
						status = "OSD Request Failed";
					}
				} else if (fingerPrintJson.getFpStatus().equalsIgnoreCase("S")) {
					scrollTextDataMap = new ScrollTextDataMap();
					corpusScrollText.setSubscriberCodes(subscriberCodeList);
					corpusScrollText.setModule("DRM");
					corpusScrollText.setCommand("SCROLL_TEXT");
					scrollTextDataMap.setMessage(fingerPrintJson.getFpMessage());
					scrollTextDataMap.setDuration(fingerPrintJson.getFpDuration());
					corpusScrollText.setDataMap(scrollTextDataMap);
					
					requestJson = gson.toJson(corpusScrollText);
					httpEntity = new HttpEntity<String>(requestJson, headers);
					response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
					// response.getBody();
					corpusStatus = response.getBody();
					corpusResponce = mapper.readValue(corpusStatus, CorpusResponce.class);
					responseJson = gson.toJson(corpusResponce);
					if (corpusResponce.getResponseStatus().getStatusCode().equalsIgnoreCase("202")) {
						oSDFingerPrintDetails = oSDFingerPrintDetailsService.saveOSDFingerPrintDetails(corpusScrollText.getSubscriberCodes().toString(), requestJson, responseJson, fingerPrintJson.getLoginId(), jsonSeqNo, "SCROLL_TEXT", corpusResponce.getTrackingId(), corpusScrollText.getDataMap().getMessage());
						if (oSDFingerPrintDetails != null)
							status = "Scroll Text Request sent Successfully";
						else {
							status = "Scroll Text Request Failed";
						}
					} else {
						status = "Scroll Text Request Failed";
					}
				}
			}
			logger.info("FingerPrint RequestJson :: "+ requestJson);
			logger.info("FingerPrint ResponseJson :: "+ responseJson);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: corpusCall" + e);
			e.printStackTrace();
		} finally {
			jsonSeqNo = null;
			gson = null;
			url = null;
			headers = null;
			restTemplate = null;
			mapper = null;
			subscriberCodeList = null;
			corpusFingerPrint = null;
			corpusOSD = null;
			corpusScrollText = null;
			dataMap = null;
			osdDataMap = null;
			scrollTextDataMap = null;
			httpEntity = null;
			response = null;
			corpusStatus = null;
			corpusResponce = null;
			requestJson = null;
			responseJson = null;
			oSDFingerPrintDetails = null;
		}
		return status;
	}
	 
	@Override
	public String getVATAmountByRegion(String regionCode, String billCycle) {
		String VAT = "";
		BigDecimal VATAmount = new BigDecimal("0");
		try {
			VAT = cafDao.getVATAmountByRegion(regionCode);
			VATAmount = new BigDecimal(VAT);
			if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_MONTHLY.getCode())) {
				VATAmount = VATAmount.multiply(new BigDecimal("1"));
			} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_QUARTERLY.getCode())) {
				VATAmount = VATAmount.multiply(new BigDecimal("3"));
			} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_HALFYEARLY.getCode())) {
				VATAmount = VATAmount.multiply(new BigDecimal("6"));
			} else {
				VATAmount = VATAmount.multiply(new BigDecimal("12"));
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getVATAmountByRegion" + e);
			e.printStackTrace();
		} finally {
			VAT = null;
		}
		return VATAmount.toString();
	}

	@Override
	@Transactional
	public String deActivateCaf(Long cafNo) {
		String status = "";
		try {
			status = cafDao.deActivateCaf(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: deActivateCaf" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	@Override
	public String getCpeModel(String cpeModal) {
		String status = "";
		try {
			status = cafDao.getCpeModel(cpeModal);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCpeModel" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	@Override
	public List<FeatureParamsVO> getFeatureParamList(String featureCodes) {
		List<FeatureParamsVO> FeatureParamList = new ArrayList<>();
		FeatureParamsVO featureParamsVO = null;
		try {
			List<Object[]> FeatureParamsList = cafDao.getFeatureParamList(featureCodes);
			for(Object[] object : FeatureParamsList) {
				featureParamsVO = new FeatureParamsVO();
				featureParamsVO.setParamLabel(object[0].toString());
				featureParamsVO.setParamValueType(object[1].toString());
				featureParamsVO.setParamLovName(object[2].toString());
				featureParamsVO.setMaxParamValues(object[3].toString());
				featureParamsVO.setFeatureCode(object[4].toString());
				featureParamsVO.setPrmCode(object[5].toString());
				if(featureParamsVO.getParamValueType().equalsIgnoreCase(ComsEnumCodes.SERVICE_FEATURE_PARAM_TYPE.getCode())){
					List<Lovs> featureList = lovsService.getLovValuesByLovName(featureParamsVO.getParamLovName());
					featureParamsVO.setFeatureList(featureList);
				}
				FeatureParamList.add(featureParamsVO);
			}
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getFeatureParamList" + e);
			e.printStackTrace();
		} finally {
			featureParamsVO = null;
		}
		return FeatureParamList;
	}

	@Override
	public String getStdCodeByVillage(Integer district, Integer mandal, Integer village) {
		String stdCode = "";
		try {
			stdCode = cafDao.getStdCodeByVillage(district, mandal, village);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCpeModel" + e);
			e.printStackTrace();
		} finally {

		}
		return stdCode;
	}

	// TelephoneService
	@Override
	public List<TelephoneServcVO> getTelephoneServc(Long cafNo) {
		List<TelephoneServcVO> telephonesrvcList = new ArrayList<>();
		List<TelephoneServcVO> telephonesrvcList1 = new ArrayList<>();
		List<TelephoneServcVO> telephonesrvcList2 = new ArrayList<>();
		List<Object[]> TelephoneList = new ArrayList<>();
		TelephoneServcVO telephoneServcVO1 = null;
		try {
			// TelephoneService
			TelephoneList = cafDao.getTelephoneServc(cafNo);
			for (Object[] object : TelephoneList) {
				TelephoneServcVO telephoneServcVO = new TelephoneServcVO();
				telephoneServcVO.setFeaturecode(object[0] == null || object[0] == "" ? "NA" : object[0].toString());
				telephoneServcVO.setPrmcode(object[1] == null || object[1] == "" ? "NA" : object[1].toString());
				telephoneServcVO.setPrmvalue(object[2] == null || object[2] == "" ? "NA" : object[2].toString());
				telephonesrvcList1.add(telephoneServcVO);
			}

			// for featurecodes
			List<String> FeaturecodesLists = cafDao.getFeatureCodes(cafNo);
			for (String fcString : FeaturecodesLists) {
				int flag = 0;
				telephoneServcVO1 = new TelephoneServcVO();
				telephoneServcVO1.setFeaturecode(fcString == null || fcString == "null" || fcString == "" ? "NA" : fcString);
				telephoneServcVO1.setPrmcode("NA");
				telephoneServcVO1.setPrmvalue("NA");
				for (int k = 0; k < telephonesrvcList1.size(); k++) {
					if (telephonesrvcList1.get(k).getFeaturecode().contains(telephoneServcVO1.getFeaturecode())) {
						flag++;
					}
				}
				if (flag == 0) {
					telephonesrvcList2.add(telephoneServcVO1);
				}
			}
			telephonesrvcList2 = new ArrayList<>(new HashSet<>(telephonesrvcList2));
			telephonesrvcList.addAll(telephonesrvcList1);
			telephonesrvcList.addAll(telephonesrvcList2);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getTelephoneServc" + e);
			e.printStackTrace();
		} finally {
			telephonesrvcList1 = null;
			telephonesrvcList2 = null;
			telephoneServcVO1 = null;
			TelephoneList = null;
		}
		return telephonesrvcList;
	}

	// TelephoneNo
	public List<TelephoneNoVO>  getTelephoneNo(Long cafNo) {
		List<TelephoneNoVO> telephoneNo=new ArrayList<>();
		try {
			telephoneNo = cafDao.getTelephoneNo(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getTelephoneNo" + e);
			e.printStackTrace();
		} finally {

		}
		return telephoneNo;
	}

	// Billing Info
	@Override
	public List<BillingInfoVO> getBillingInfo(Long cafNo) {
		List<BillingInfoVO> billingList = new ArrayList<>();
		List<Object[]> BillingInfoList = new ArrayList<>();
		try {
			BillingInfoList = cafDao.getBillingInfo(cafNo);
			for (Object[] object : BillingInfoList) {
				BillingInfoVO billinfoVO = new BillingInfoVO();
				billinfoVO.setBillMonth(object[0] == null || object[1] == "" ? "NA" : object[0].toString());
				billinfoVO.setAmount(object[2] == null || object[2] == "" ? "NA" : object[2].toString());
				billinfoVO.setInvDate(object[3] == null || object[3] == "" ? "NA" : object[3].toString());
				billinfoVO.setDueDate(object[4] == null || object[4] == "" ? "NA" : object[4].toString());
				billinfoVO.setFilePath(object[1] == null || object[1] == "" ? "NA" : object[1].toString());
				billingList.add(billinfoVO);
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getBillingInfo" + e);
			e.printStackTrace();
		} finally {
			BillingInfoList = null;
		}
		return billingList;
	}

	/*@Override
	public OnuStbChargesVO getOnuStbChargeDetails(Long cafNo) {
		OnuStbChargesVO onuStbChargesVO = new OnuStbChargesVO();
		try {
			Object[] object = cafDao.getOnuStbChargeDetails(cafNo);
			if (object[0] != null && object[1] != null) {
				onuStbChargesVO.setOnuInstChrg(object[0].toString());
				onuStbChargesVO.setOnuInstCount(object[1].toString());
			}
			if (object[2] != null) {
				onuStbChargesVO.setOnuCost(object[2].toString());
			}
			if (object[3] != null) {
				onuStbChargesVO.setInstCharge(object[3].toString());
			}
			if (object[4] != null) {
				onuStbChargesVO.setExtraCableChrg(object[4].toString());
			}
			if (object[5] != null) {
				onuStbChargesVO.setStbCost(object[5].toString());
			}
			if (object[6] != null && object[7] != null) {
				onuStbChargesVO.setStbInstChrg(object[6].toString());
				onuStbChargesVO.setStbInstCount(object[7].toString());
			}

		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getOnuStbChargeDetails" + e);
			e.printStackTrace();
		}
		return onuStbChargesVO;
	}*/

	// getOldPackageInformation
	@Override
	public List<CafOldPackagesBO> getOldPackageInformation(Long cafNumber) {
		List<CafOldPackagesBO> cafOldPackagesList = new ArrayList<>();
		try {
			cafOldPackagesList = cafDao.getOldPackageInformation(cafNumber);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getOldPackageInformation" + e);
			e.printStackTrace();
		} finally {
		}
		return cafOldPackagesList;
	}

	@Override
	@Transactional
	public void updateCpeStockPaymentStatus(Long cafNo, String srlNo, String loginId, String lmoCode) {
		try {
			cafDao.updateCpeStockPaymentStatus(cafNo, srlNo, loginId, lmoCode);
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: updateCpeStockPaymentStatus" +e);
			e.printStackTrace();
		} finally {
			
		}
	}

	@Override
	public FilterPackagesVO getFilterPackagessList(CustomerCafVO customerCafVO, Map<String, Integer> packagesMap) {
		FilterPackagesVO filterPackagesVO = new FilterPackagesVO();
		List<ProductsVO> selectedProdList = new ArrayList<ProductsVO>();
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<ProductsVO> productsList = new ArrayList<>();
		String[] agruniqueids = null;
		String[] prodCodes = null;
		String[] tenantCodes = null;
		try {
			agruniqueids = customerCafVO.getAgruniqueid().split(",");
			prodCodes = customerCafVO.getProdCode().split(",");
			tenantCodes = customerCafVO.getTenantCode().split(",");
			productsList = customerCafVO.getProducts();
			for (ProductsVO product : productsList) {
				for (int i = 0; i < agruniqueids.length; i++)
					if (product.getAgruniqueid().equals(Long.parseLong(agruniqueids[i])) && product.getProdcode().equalsIgnoreCase(prodCodes[i]) && product.getTenantcode().equalsIgnoreCase(tenantCodes[i]))
						selectedProdList.add(product);
			}
			
			BigDecimal totalTax = new BigDecimal("0");
			BigDecimal totalCharge = new BigDecimal("0");
			BigDecimal securityDepositCharge = new BigDecimal("0");
			BigDecimal securityDepositTax = new BigDecimal("0");
			for(ProductsVO products : selectedProdList) {
				List<ServicesVO> servicesVOList = new ArrayList<ServicesVO>();
				CafAndCpeChargesVO cafAndCpeChargesVO = new CafAndCpeChargesVO();
				List<chargeTypesVO> chargeTypeList = products.getChargeTypeList();
				List<AddtionalServicesVO> serviceList = products.getServicesList();
				for(chargeTypesVO chargeTypesVO : chargeTypeList) {
					if(chargeTypesVO.getChargeType().equalsIgnoreCase(ComsEnumCodes.CHARGE_TYPE_ACTIVATION.getCode())) {
						if(packagesMap.get(products.getProdcode()) != null) {
							BigDecimal activationChrg = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							BigDecimal activationTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							cafAndCpeChargesVO.setActivationCharge(activationChrg.toString());
							cafAndCpeChargesVO.setActivationTax(activationTax.toString());
						} else {
							cafAndCpeChargesVO.setActivationCharge(chargeTypesVO.getChargeAmt());
							cafAndCpeChargesVO.setActivationTax(chargeTypesVO.getTaxAmt());
						}
					} else if(chargeTypesVO.getChargeType().equalsIgnoreCase(ComsEnumCodes.CHARGE_TYPE_DEPOSIT.getCode())) {
						if(customerCafVO.getBillCycle().equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_MONTHLY.getCode())) {
							if(packagesMap.get(products.getProdcode()) != null) {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							} else {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt());
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt());
							}
						} else if(customerCafVO.getBillCycle().equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_QUARTERLY.getCode())) {
							if(packagesMap.get(products.getProdcode()) != null) {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("3")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("3")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							} else {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("3"));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("3"));
							}
						} else if(customerCafVO.getBillCycle().equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_HALFYEARLY.getCode())) {
							if(packagesMap.get(products.getProdcode()) != null) {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("6")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("6")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							} else {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("6"));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("6"));
							}
						} else if(customerCafVO.getBillCycle().equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_YEARLY.getCode())) {
							if(packagesMap.get(products.getProdcode()) != null) {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("12")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("12")).multiply(new BigDecimal(packagesMap.get(products.getProdcode()).toString()));
							} else {
								securityDepositCharge = new BigDecimal(chargeTypesVO.getChargeAmt()).multiply(new BigDecimal("12"));
								securityDepositTax = new BigDecimal(chargeTypesVO.getTaxAmt()).multiply(new BigDecimal("12"));
							}
						}
						cafAndCpeChargesVO.setSecDepositCharge(securityDepositCharge.toString());
						cafAndCpeChargesVO.setSecDepositTax(securityDepositTax.toString());
					}
				}
				for(AddtionalServicesVO services : serviceList) {
					ServicesVO servicesVO = new ServicesVO();
					servicesVO.setCoreSrvcCode(services.getCoreServiceCode());
					servicesVO.setSrvcName(services.getServiceName());
					servicesVOList.add(servicesVO);
				}
				cafAndCpeChargesVO.setProdName(products.getProdname());
				cafAndCpeChargesVO.setProdtype(products.getProdType());
				cafAndCpeChargesVO.setServices(servicesVOList);
				cafAndCpeChargesList.add(cafAndCpeChargesVO);
				//totalTax = totalTax.add(new BigDecimal(cafAndCpeChargesVO.getRecurringTax())).add(new BigDecimal(cafAndCpeChargesVO.getActivationTax())).add(new BigDecimal(cafAndCpeChargesVO.getSecDepositTax()));
				totalTax = totalTax.add(new BigDecimal(cafAndCpeChargesVO.getActivationTax())).add(new BigDecimal(cafAndCpeChargesVO.getSecDepositTax()));
				totalCharge = totalCharge.add(new BigDecimal(cafAndCpeChargesVO.getActivationCharge())).add(new BigDecimal(cafAndCpeChargesVO.getActivationTax())).add(new BigDecimal(cafAndCpeChargesVO.getSecDepositCharge()));
			}
			filterPackagesVO.setCafAndCpeChargesList(cafAndCpeChargesList);
			filterPackagesVO.setTotalAmount(totalCharge.toString());
			filterPackagesVO.setTotalTax(totalTax.toString());
			filterPackagesVO.setSelectedPackagesList(selectedProdList);
			
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getFilterPackagessList" +e);
			e.printStackTrace();
		} finally {
			selectedProdList = null;
			cafAndCpeChargesList = null;
			prodCodes = null;
			productsList = null;
			tenantCodes = null;
			agruniqueids = null;
		}
		
		return filterPackagesVO;
	}

	@Override
	public String getCpeStackStatusBySrlNo(String serialNo, String cpeModel) {
		String status = "";
		try {
			status = cafDao.getCpeStackStatusBySrlNo(serialNo, cpeModel);
			status = status.equalsIgnoreCase("false") ? "No Record" : status;
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCpeModel" + e);
			e.printStackTrace();
		} finally {

		}
		return status;
	}

	@Override
	public List<FingerPrintBO> getIndividualCafFPActivationInformation(PageObject pageObject) {
		List<FingerPrintBO> customerList = new ArrayList<>();
		try {
			customerList = cafDao.getIndividualCafFPActivationInformation(pageObject);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getIndividualCafFPActivationInformation" + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public List<FingerPrintBO> getGroupCafFPActivationInformation(FingerPrintBO fingerPrintBO) {
		List<FingerPrintBO> customerList = new ArrayList<>();
		try {
			customerList = cafDao.getGroupCafFPActivationInformation(fingerPrintBO);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getGroupCafFPActivationInformation" + e);
			e.printStackTrace();
		} finally {

		}
		return customerList;
	}

	@Override
	public Object[] getCpeChargeDetailsForMobileApp(String chargeCode, String regionCode) {
		Object[] tax = null;
		try {
			tax = cafDao.getCpeChargeDetailsForMobileApp(chargeCode, regionCode );
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCpeChargeDetailsForMobileApp" + e);
			e.printStackTrace();
		} finally {

		}
		return tax;
	}

	@Override
	@Transactional
	public Caf saveBulkUploadCafs(CustomerCafVO customerCafVO, String loginId, Long uploadId) {
		Customer enterpriseCustomer = null;
		Caf caf = null;
		List<EntCafStage> failedCafList = new ArrayList<>();
		List<EntCafStage> entCafStagesList = new ArrayList<>();
		List<Mandals> mandalList = new ArrayList<>();
		List<Districts> districtList = new ArrayList<>();
		Long successRecordCount = 1l;
		try {
			if(customerCafVO.getBulkUpload().equalsIgnoreCase("MultipleCafs")) {
				Long pmntCustCode = storedProcedureDAO.executePaymentCustomer(Long.parseLong(customerCafVO.getCustCode()));
				caf = new Caf();
				entCafStagesList = uploadHistoryService.getAllEntCafStageByStatus(uploadId);
				for(EntCafStage entCafStage : entCafStagesList) {
					String status = FileUtil.BulkUploadValidation(entCafStage);
					if(status.equalsIgnoreCase("true")) {
						districtList = lovsServiceImpl.getDistrictListByDistrictName(entCafStage.getDistrict());
						if(!districtList.isEmpty()) {
							mandalList = lovsServiceImpl.getmandalListByDistrictIdAndMandalName(districtList.get(0).getDistrictUid(), entCafStage.getMandal());
						}
						if(districtList.isEmpty()) {
							status = "Invalid Districtname";
						} else if(mandalList.isEmpty()) {
							status = "Invalid Mandalname";
						} else if(!lovsServiceImpl.checkLmocodeValidation(entCafStage.getLmocode())) {
							status = "Invalid LMOCode";
						} else if(!lovsServiceImpl.CheckAPSFLCode(entCafStage.getApsflUniqueId())) {
							status = "Duplicate APSFL Code";
						} else if((entCafStage.getContactEmail() == null || entCafStage.getContactEmail().isEmpty()) && entCafStage.getPmntrespFlag().equalsIgnoreCase("Yes")) {
							status = "Email Id Mandatory";
						} else {
							status = "true";
						}
					}
					if(status.equalsIgnoreCase("true")) {
						if(entCafStage.getPmntrespFlag().equalsIgnoreCase("Yes")) {
							enterpriseCustomer = new Customer();
							enterpriseCustomer = enterpriseCustomerService.saveEnterpriseCustomer(customerCafVO, entCafStage, districtList.get(0).getDistrictUid().toString(), mandalList.get(0).getMandalSlno().toString());
							logger.info("The EnterpriseNode Customer Created Successfully");
							
							caf = new Caf(customerCafVO, entCafStage);
							Long cafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
							caf.setCafNo(cafNo);
							caf.setCustId(enterpriseCustomer.getCustId());
							caf.setCustCode(enterpriseCustomer.getCustId().toString());
							caf.setPmntCustCode(enterpriseCustomer.getCustId().toString());
							caf.setPmntCustId(enterpriseCustomer.getCustId());
							caf.setCustdistUid(0);
							//caf.setCustdistUid(Integer.parseInt((districtList.get(0).getDistrictUid().toString())));
							caf.setInstDistrict(districtList.get(0).getDistrictUid().toString());
							caf.setInstMandal(mandalList.get(0).getMandalSlno().toString());
							caf = cafDao.saveCaf(caf);
							logger.info("The Caf Created Successfully");
							
							customerCafVO.setCafNo(cafNo);
							cafProductsService.createCafProducts(customerCafVO, cafNo, customerCafVO.getLoginId(), enterpriseCustomer.getCustId().toString(), 0l, "", null, 0l);
							logger.info("The cafProducts Created Successfully");
							
							/*cafProductsService.createMSOPackages(customerCafVO);
							logger.info("The MSO cafProducts Created Successfully");*/
							
							uploadHistoryService.updateUploadHistory(entCafStage.getUploadId(), successRecordCount);
							logger.info("The uploadHistory Updated Successfully");
							
							uploadHistoryService.updateEntCafStage(entCafStage, status);
							logger.info("The EntCafStage Updated Successfully");
							
						} else if(entCafStage.getPmntrespFlag().equalsIgnoreCase("No")) {
							caf = new Caf(customerCafVO, entCafStage);
							Long cafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
							//Customer customer = customerDao.findByCustomerId(pmntCustCode);
							caf.setCafNo(cafNo);
							caf.setCustId(Long.parseLong(customerCafVO.getCustCode()));
							caf.setCustCode(customerCafVO.getCustCode());
							caf.setPmntCustCode(pmntCustCode.toString());
							caf.setPmntCustId(pmntCustCode);
							caf.setCustdistUid(0);
							//caf.setCustdistUid(customer.getCustdistUid());
							caf.setInstDistrict(districtList.get(0).getDistrictUid().toString());
							caf.setInstMandal(mandalList.get(0).getMandalSlno().toString());
							
							caf = cafDao.saveCaf(caf);
							logger.info("The Caf Created Successfully");
							
							customerCafVO.setCafNo(cafNo);
							cafProductsService.createCafProducts(customerCafVO, cafNo, customerCafVO.getLoginId(), customerCafVO.getCustCode().toString(), 0l, "", null, 0l);
							logger.info("The cafProducts Created Successfully");
							
							/*cafProductsService.createMSOPackages(customerCafVO);
							logger.info("The MSO cafProducts Created Successfully");*/
							
							uploadHistoryService.updateUploadHistory(entCafStage.getUploadId(), successRecordCount);
							logger.info("The uploadHistory Updated Successfully");
							
							uploadHistoryService.updateEntCafStage(entCafStage, status);
							logger.info("The EntCafStage Updated Successfully");
						}
						successRecordCount++;
					} else {
						uploadHistoryService.updateEntCafStage(entCafStage, status);
						logger.info("The EntCafStage Updated Successfully");
						failedCafList.add(entCafStage);
					}
				}
				caf.setSuccessRecordCount(String.valueOf(entCafStagesList.size() - failedCafList.size()));
				caf.setFailedRecordCount(String.valueOf(failedCafList.size()));
			} else if(customerCafVO.getBulkUpload().equalsIgnoreCase("SingalCaf")) {
				Long pmntCustCode = storedProcedureDAO.executePaymentCustomer(Long.parseLong(customerCafVO.getCustCode()));
				for(Caf cafObject : customerCafVO.getCafList()) {
					caf = new Caf();
					Long cafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
					caf.setAadharNo(customerCafVO.getAadharNumber());
					caf.setCustType(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode());
					caf.setBillfreqLov(customerCafVO.getBillCycle());
					caf.setCafNo(cafNo);
					caf.setCafDate(Calendar.getInstance());
					caf.setContactEmail(cafObject.getContactEmail());
					caf.setContactmobileNo(cafObject.getContactmobileNo());
					caf.setContactPerson(cafObject.getContactPerson());
					caf.setCreatedBy(customerCafVO.getLoginId());
					caf.setCreatedOn(Calendar.getInstance());
					caf.setCreatedIPAddr(customerCafVO.getIpAddress());
					caf.setCustId(Long.parseLong(customerCafVO.getCustCode()));
					caf.setCustCode(customerCafVO.getCustCode());
					caf.setPmntCustCode(pmntCustCode.toString());
					caf.setPmntCustId(pmntCustCode);
					caf.setInstAddress1(cafObject.getInstAddress1());
					//caf.setCustdistUid(Integer.parseInt(cafObject.getInstDistrict()));
					caf.setCustdistUid(0);
					caf.setInstDistrict(cafObject.getInstDistrict());
					caf.setInstMandal(cafObject.getInstMandal());
					caf.setCpePlace(cafObject.getCpePlace());
					caf.setInstLocality(cafObject.getInstAddress1().split(",")[1]);
					caf.setBillfreqLov(customerCafVO.getBillCycle());
					caf.setStatus(ComsEnumCodes.Caf_BulkUpload_status.getStatus());
					caf.setModifiedOn(Calendar.getInstance());
					caf.setLmoCode(cafObject.getLmoCode());
					caf.setOltCardno(1);
					caf.setInstState("Andhra Pradesh");
					//caf.setStbLeaseyn('N');
					caf.setCpeLeaseyn('N');
					caf.setLmoDeclaration('N');
					caf.setCustomerDeclaration('N');
					caf.setInstPin("");
					caf.setApsflUniqueId(cafObject.getApsflUniqueId());
					caf.setLongitude(cafObject.getLongitude());
					caf.setLattitude(cafObject.getLattitude());
					caf = cafDao.saveCaf(caf);
					logger.info("The Caf Created Successfully");
					
					customerCafVO.setCafNo(cafNo);
					cafProductsService.createCafProducts(customerCafVO, cafNo, customerCafVO.getLoginId(), customerCafVO.getCustCode().toString(), 0l, "", null, 0l);
					logger.info("The cafProducts Created Successfully");
					
					/*cafProductsService.createMSOPackages(customerCafVO);
					logger.info("The MSO cafProducts Created Successfully");*/
				}
			}
		} catch (Exception e) {
			logger.error("CafServiceImpl::saveCaf() " + e);
			e.printStackTrace();
		} finally {
		}
		return caf;
	}

	@Override
	public String getCoreSrvcCodeByCafNo(Long cafNo) {
		String coreSrvcCode = "";
		try {
			coreSrvcCode = cafDao.getCoreSrvcCodeByCafNo(cafNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getCoreSrvcCodeByCafNo" + e);
			e.printStackTrace();
		} finally {

		}
		return coreSrvcCode;
	}

	@Override
	@Transactional
	public Caf updateCafDetails(CustomerCafVO customerCafVO) {
		Caf caf = new Caf();
		try {
			caf = cafDao.findByCafNo(customerCafVO.getCafNo());
			caf.setInstAddress1(customerCafVO.getDistrict()+","+customerCafVO.getMandal());
			caf.setCpePlace(customerCafVO.getLocation()+","+customerCafVO.getFirstName());
			caf.setContactmobileNo(customerCafVO.getMobileNo());
			caf.setContactPerson(customerCafVO.getPocName()+"("+customerCafVO.getPocDesignation()+")");
			caf.setContactEmail(customerCafVO.getEmailId());
			caf.setLmoCode(customerCafVO.getLmoCode());
			if(customerCafVO.getLongitude() != null) {
				if(!customerCafVO.getLongitude().isEmpty()) {
					caf.setLongitude(Float.parseFloat(customerCafVO.getLongitude()));
				}
			}
			if(customerCafVO.getLatitude() != null) {
				if(!customerCafVO.getLatitude().isEmpty()) {
					caf.setLattitude(Float.parseFloat(customerCafVO.getLatitude()));
				}
			}
			caf.setModifiedBy(customerCafVO.getLoginId());
			caf.setModifiedIPAddr(customerCafVO.getIpAddress());
			caf.setModifiedOn(Calendar.getInstance());
			caf = cafDao.saveCaf(caf);
			logger.info("The Caf Updated Successfully.");
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: updateCafDetails" + e);
			e.printStackTrace();
		} finally {
			
		}
		return caf;
	}
	
	@Override
	@Transactional
	public VPNSrvcNames saveVPNServicesUpload(VPNServiceExcelUploadVO vpnServiceExcelUploadVO, Long uploadId) {
		VPNSrvcNames vpnSrvcNames = null;
		List<VPNSrvcNamesStage> failedCafList = new ArrayList<>();
		List<VPNSrvcNamesStage> vpnSrvcNamesStageList = new ArrayList<>();
		Long successRecordCount = 1l;
		String status = "";
		try {
			vpnSrvcNames = new VPNSrvcNames();
			vpnSrvcNamesStageList = uploadHistoryService.getAllVPNSrvcNamesStageStatus(uploadId);
			for (VPNSrvcNamesStage vpnSrvcNamesStage : vpnSrvcNamesStageList) {
				if(vpnSrvcNamesStage.getVpnsrvcName() == null || vpnSrvcNamesStage.getVpnsrvcName().isEmpty() || vpnSrvcNamesStage.getVpnsrvcName().length() > 255) {
					status = "VPN Service Name more than 255 Characters";
				} else if (cpeInformationService.getOLTSLNOBySubstationsrlno(vpnSrvcNamesStage.getSubstnUid()).isEmpty()) {
					status = "Invalid SubStationPOPId";
				} else if(cpeInformationService.getOLTSLNOAndSubstationsrlno(vpnSrvcNamesStage.getSubstnUid(),vpnSrvcNamesStage.getOltSerialNo()).isEmpty()) {
					status = "Invalid OLT SerialNo";
				} else if(!cpeInformationService.check_SubstUid_OltSrNo_VPNSrNm(vpnSrvcNamesStage.getSubstnUid(), vpnSrvcNamesStage.getOltSerialNo(), vpnSrvcNamesStage.getVpnsrvcName())) {
					status = "Duplicate Record Entry";
				} else {
					status = "true";
				}
				if (status.equalsIgnoreCase("true")) {
					vpnSrvcNames = new VPNSrvcNames();
					vpnSrvcNames.setSubstnUid(vpnSrvcNamesStage.getSubstnUid());
					vpnSrvcNames.setCreatedBy(vpnServiceExcelUploadVO.getLoginId());
					vpnSrvcNames.setCreatedOn(Calendar.getInstance());
					vpnSrvcNames.setOltSerialNo(vpnSrvcNamesStage.getOltSerialNo());
					vpnSrvcNames.setVpnsrvcName(vpnSrvcNamesStage.getVpnsrvcName());
					vpnSrvcNames.setDistrictUid(vpnSrvcNamesStage.getDistrictUid());
					vpnSrvcNames.setMandalSlno(vpnSrvcNamesStage.getMandalSlno());
					vpnSrvcNames.setStatus(ComsEnumCodes.ENT_Caf_Pending.getCode());
					vpnSrvcNames = cafDao.saveVPNSrvcNames(vpnSrvcNames);
					logger.info("The VPNSrvcNames Inserted Successfully");

					uploadHistoryService.updateUploadHistory(vpnSrvcNamesStage.getUploadId(), successRecordCount);
					logger.info("The uploadHistory Updated Successfully");

					uploadHistoryService.updateVPNSrvcNamesStage(vpnSrvcNamesStage, status);
					logger.info("The VPNSrvcNamesStage Updated Successfully");
					successRecordCount++;
				} else {
					uploadHistoryService.updateVPNSrvcNamesStage(vpnSrvcNamesStage, status);
					logger.info("The VPNSrvcNamesStage Updated Successfully");
					failedCafList.add(vpnSrvcNamesStage);
				}
			}
			vpnSrvcNames.setSuccessRecordCount(String.valueOf(vpnSrvcNamesStageList.size() - failedCafList.size()));
			vpnSrvcNames.setFailedRecordCount(String.valueOf(failedCafList.size()));
		} catch (Exception e) {
			logger.error("CafServiceImpl::saveVPNServicesUpload() " + e);
			e.printStackTrace();
		} finally {
		}
		return vpnSrvcNames;
	}

	@Override
	public Object[] getMacAddressBySerialNo(String srlNo) {
		Object[] object = null;
		try {
			object = cafDao.getMacAddressBySerialNo(srlNo);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMacAddressBySerialNo" + e);
			e.printStackTrace();
		} finally {

		}
		return object;
	}
	
	@Override
	public CpeInformationVO getSISelectedPackages(String prodCodes, String billCycle, String regionCode) {
		CafAndCpeChargesVO cafAndCpeChargesVO = null;
		CpeInformationVO cpeInformationVO = new CpeInformationVO();
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<Object[]> packegesList = new ArrayList<>();
		BigDecimal totalCharge = new BigDecimal("0");
		BigDecimal securityDepositCharge = new BigDecimal("0");
		try {
			packegesList = cafDao.getSISelectedPackages(prodCodes, billCycle, regionCode);
			for (Object[] payment : packegesList) {
				cafAndCpeChargesVO = new CafAndCpeChargesVO();
				cafAndCpeChargesVO.setProdCode(payment[0] != null ? payment[0].toString() : "");
				cafAndCpeChargesVO.setProdName(payment[1] != null ? payment[1].toString() : "");
				cafAndCpeChargesVO.setProdtype(payment[2] != null ? payment[2].toString() : "");
				cafAndCpeChargesVO.setSrvcName(payment[3] != null ? payment[3].toString() : "");
				if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_MONTHLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[6].toString());
				} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_QUARTERLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[6].toString()).multiply(new BigDecimal("3"));
				} else if (billCycle.equalsIgnoreCase(ComsEnumCodes.BILLCYCLE_HALFYEARLY.getCode())) {
					securityDepositCharge = new BigDecimal(payment[6].toString()).multiply(new BigDecimal("6"));
				} else {
					securityDepositCharge = new BigDecimal(payment[6].toString()).multiply(new BigDecimal("12"));
				}
				cafAndCpeChargesVO.setActivationCharge(payment[4].toString());
				cafAndCpeChargesVO.setActivationTax(payment[5].toString());
				cafAndCpeChargesVO.setSecDepositCharge(securityDepositCharge.toString());
				cafAndCpeChargesVO.setSecDepositTax("0");
				totalCharge = totalCharge.add(new BigDecimal(securityDepositCharge.toString())).add(new BigDecimal(payment[4].toString())).add(new BigDecimal(payment[5].toString()));
				cafAndCpeChargesList.add(cafAndCpeChargesVO);
			}
			cpeInformationVO.setTotalCharge(totalCharge.toString());
			cpeInformationVO.setCafAndCpeChargesList(cafAndCpeChargesList);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getPaymentDetails" + e);
			e.printStackTrace();
		} finally {
			cafAndCpeChargesList = null;
			cafAndCpeChargesVO = null;
			packegesList = null;
			totalCharge = null;
		}
		return cpeInformationVO;
	}

	@Override
	public List<String> getFingerPrintDetailsFromConfig() {
		List<String> fPlist = new ArrayList<>();
		try {
			fPlist = cafDao.getFingerPrintDetailsFromConfig();
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getFingerPrintDetailsFromConfig" + e);
			e.printStackTrace();
		} finally {

		}
		return fPlist;
	}
	
/*	public ComsHelperDTO findCafDaoByCafNo(ComsHelperDTO comsHelperDTO) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			cafsList = cafDao.findCafDaoByCafNo(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(), comsHelperDTO.getPageObject());
			PageObject pageObject = comsHelperDTO.getPageObject();
			comsHelperDTO.setCafsList(cafsList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}*/
	
	@Override
	public List<Object[]> findCafByCafNo(Long custId) {
		List<Object[]> objectList = new ArrayList<>();
		try {
			objectList = cafDao.findCafByCafNo(custId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: findCafDaoByCafNo" + e);
			e.printStackTrace();
		} finally {

		}
		return objectList;
	}

	@Override
	public List<MonthlyPaymentBO> getMonthlyCafDetailsForMobile(String mobileNo, String tenantCode) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		try {
			cafList = cafDao.getMonthlyCafDetailsForMobile(mobileNo, tenantCode);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMonthlyCafDetailsForMobile" + e);
			e.printStackTrace();
		} finally {

		}
		return cafList;
	}
	
	@Override
	public List<OntIdCustTypeBO> getOntIdandCustTypeList(String cafNo) {
		List<OntIdCustTypeBO> ontIdCustTypeList = new ArrayList<>();
		try {
			ontIdCustTypeList = cafDao.getOntIdandCustTypeList(cafNo);
			} catch(Exception e) {
				logger.error("The Exception is CafServiceImpl :: getOntIdandCustTypeList" + e);
			}
		return ontIdCustTypeList;
	}
	
	@Override
	public CustomerDetailsBO getCustomerDetailsByCafNo(Long cafNo) {
		CustomerDetailsBO customerDetails = null;
		try {
			customerDetails = cafDao.getCustomerDetailsByCafNo(cafNo);
			} catch(Exception e) {
				logger.error("The Exception is CafServiceImpl :: getOntIdandCustTypeList" + e);
			}
		return customerDetails;
	}
	
	@Override
	public String hasIPTVBoxForCaf(Long cafNumber) {
		String status = null;
		status = cafDao.hasIPTVBoxForCaf(cafNumber);
		return status;
	}

	@Override
	public String getCustomerBalance(Integer custId, String tenantCode) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		String bal = "0";
		try {
			cafList = cafDao.getMonthlyCafDetails("", tenantCode, custId);
			if(cafList.size() > 0) {
				bal = cafList.get(0).getDueAmount();
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMonthlyCafDetails" + e);
			e.printStackTrace();
		} finally {

		}
		return bal;
	}

	@Override
	public List<NoOfSTBsBO> getNoOfSTBsForCaf(Long cafNumber) {
		List<NoOfSTBsBO> noOfSTBsList  = new ArrayList<>();
		try {
			noOfSTBsList = cafDao.getNoOfSTBsForCaf(cafNumber);
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getNoOfSTBsForCaf" + e);
			e.printStackTrace();
		} finally {
			
		}
		return noOfSTBsList;
	}
	
	@Override
	public ChangePkgBO getProdCodeForCaf(Long cafNumber) {
		ChangePkgBO changePkgBO = new ChangePkgBO();
		try {
			changePkgBO = cafDao.getProdCodeForCaf(cafNumber);
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getProdCodeForCaf" + e);
			e.printStackTrace();
		} finally {
			
		}
		return changePkgBO;
	}

	@Override
	public String getSecurityDepositByProdcode(String prodCafNo, String prodCode, String regionCode) {
		String secDeposit = "0";
		try {
			secDeposit = cafDao.getSecurityDepositByProdcode(prodCafNo, prodCode, regionCode);
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getSecurityDepositByProdcode" + e);
			e.printStackTrace();
		} finally {
			
		}
		return secDeposit;
	}
	
	@Override
	public String[] getGolbalOrGroupSubCodes(String district, String mandal, String village) {
		List<String> subsCodesList  = new ArrayList<>();
		String[] subsCodesArray = null;
		try {
			subsCodesList = cafDao.getGolbalOrGroupSubCodes(district, mandal, village);
			subsCodesArray = new String[subsCodesList.size()];
			subsCodesList.toArray(subsCodesArray);
		} catch(Exception e) {
			logger.error("The Exception is CafServiceImpl :: getNoOfSTBsForCaf" + e);
			e.printStackTrace();
		} finally {
			
		}
		return subsCodesArray;
	}
	
	@Override
	@Transactional
	public void save(Caf caf) {
		cafDao.saveCaf(caf);
	}
	
	@Override
	public List<TerminatePkgBO> getProdInfoByCafNo(Long cafNo) {
		List<TerminatePkgBO> prodList = new ArrayList<>();
		try {
			prodList = cafDao.getProdInfoByCafNo(cafNo);
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("The Exception is CafServiceImpl :: getProdInfoByCafNo" + e);
		}
		return prodList;
	}

	public ComsHelperDTO viewEntCafDetails(ComsHelperDTO comsHelperDTO) {
		List<Object[]> cafList = new ArrayList<>();
		List<CafsVO> cafsList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: viewEntCafDetails() :: Start");
			cafList = cafDao.findEntCafDtls(comsHelperDTO.getCustId(), comsHelperDTO.getPageObject());
			for(Object[] obj:cafList){
				Map<String,String> map=new HashMap<String,String>();
				CafsVO cafsVO=new CafsVO();
				cafsVO.setCafNo(obj[0] == null ? "NA" : obj[0].toString());
				cafsVO.setCpeslNo(obj[1] == null ? "NA" : obj[1].toString());
				cafsVO.setCpeAddr(obj[2] == null ? "NA" : obj[2].toString());
				cafsVO.setfName(obj[3] == null ? "" : obj[3].toString());
				cafsVO.setlName(obj[4] == null ? "" : obj[4].toString());
				cafsVO.setCpeMacAddr(obj[5] == null ? "NA" : obj[5].toString());
				cafsVO.setStbSrlNo(obj[6] == null ? "NA" : obj[6].toString());
				cafsVO.setStbMacAddr(obj[7] == null ? "NA" : obj[7].toString());
				cafsVO.setCafPhoneNo(obj[8] == null ? "NA" : obj[8].toString());
				cafsVO.setStatus(obj[9] == null ? "NA" : obj[9].toString());
				List<String> stbsrlNO = Arrays.asList(cafsVO.getStbSrlNo().split(","));
			    List<String> stbmacAddr = Arrays.asList(cafsVO.getStbMacAddr().split(","));
			    if(stbsrlNO.size() > 0){
				    for (int i=0; i<stbsrlNO.size(); i++) {
				        map.put(stbsrlNO.get(i), stbmacAddr.get(i));
				    }
				    cafsVO.setStbSrlNoMacAddr(map);
			    }
			    cafsList.add(cafsVO);
			}
			PageObject pageObject = comsHelperDTO.getPageObject();
			comsHelperDTO.setCafList(cafsList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: viewEntCafDetails() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}

	@Override
	public List<HsiBO> getHSIParameters(String srvcCode) {
		List<HsiBO> hsiList = new ArrayList<>();
		try {
			hsiList = cafDao.getProdInfoByCafNo(srvcCode);
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("The Exception is CafServiceImpl :: getHSIParameters" + e);
		}
		return hsiList;
	}

	@Override
	public List<CafsVO> getCafInfo(Long cafNo) {
		List<CafsVO> cafsVOList = new ArrayList<>();
		List<Object[]> cafsList = new ArrayList<>();
		try {
			cafsList = cafDao.getCafInfo(cafNo.toString());
			for (Object[] obj : cafsList) {
				Map<String, String> map = new HashMap<String, String>();
				CafsVO cafsVO = new CafsVO();
				cafsVO.setCafNo(obj[0] == null ? "NA" : obj[0].toString());
				cafsVO.setCpeslNo(obj[1] == null ? "NA" : obj[1].toString());
				cafsVO.setCpeAddr(obj[2] == null ? "NA" : obj[2].toString());
				cafsVO.setCpeMacAddr(obj[3] == null ? "NA" : obj[3].toString());
				cafsVO.setStbSrlNo(obj[4] == null ? "NA" : obj[4].toString());
				cafsVO.setStbMacAddr(obj[5] == null ? "NA" : obj[5].toString());
				cafsVO.setCafPhoneNo(obj[6] == null ? "NA" : obj[6].toString());

				List<String> stbsrlNO = Arrays.asList(cafsVO.getStbSrlNo().split(","));
				List<String> stbmacAddr = Arrays.asList(cafsVO.getStbMacAddr().split(","));
				if (stbsrlNO.size() > 0) {
					for (int i = 0; i < stbsrlNO.size(); i++) {
						map.put(stbsrlNO.get(i), stbmacAddr.get(i));
					}
					cafsVO.setStbSrlNoMacAddr(map);
				}
				cafsVOList.add(cafsVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("The Exception is CafServiceImpl :: getHSIParameters" + e);
		}
		return cafsVOList;
	}
	
	@Override
	public List<CafAndCpeChargesVO> getMultiActionCafList(MultiAction multiAction) {
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<Object[]> cafsList = new ArrayList<>();
		try {
			cafsList = cafDao.getMultiActionCafList(multiAction);
			for (Object[] caf : cafsList) {
				Map<String,String> map=new HashMap<String,String>();
				CafAndCpeChargesVO cafAndCpeChargesVO = new CafAndCpeChargesVO();
				cafAndCpeChargesVO.setfName(caf[0] == null ? "" : caf[0].toString());
				cafAndCpeChargesVO.setlName(caf[1] == null ? "" : caf[1].toString());
				cafAndCpeChargesVO.setCafType(caf[2] == null ? "" : caf[2].toString());
				cafAndCpeChargesVO.setApsflTrackId(caf[3] == null ? "" : caf[3].toString());
				cafAndCpeChargesVO.setCpeplace(caf[4] == null ? "" : caf[4].toString());
				cafAndCpeChargesVO.setTenantCode(caf[5] == null ? "--" : caf[5].toString());
				cafAndCpeChargesVO.setCafNo(caf[6] == null ? "" : caf[6].toString());
				cafAndCpeChargesVO.setCustCode(caf[7] != null ? caf[7].toString() : "");
				cafAndCpeChargesVO.setPopName(caf[8] == null ? "NA" : caf[8].toString());
				cafAndCpeChargesVO.setCafdate(caf[9] == null ? "" : caf[9].toString());
				cafAndCpeChargesVO.setStatusDesc(caf[10] == null ? "" : caf[10].toString());
				cafAndCpeChargesVO.setCpeMacAddr(caf[11] == null ? "" : caf[11].toString());
				cafAndCpeChargesVO.setOltSrlNo(caf[12] == null ? "NA" : caf[12].toString());
				cafAndCpeChargesVO.setOltMacAddr(caf[13] == null ? "NA" : caf[13].toString());
				cafAndCpeChargesVO.setCpeSlno(caf[14] == null ? "" : caf[14].toString());
				cafAndCpeChargesVO.setContactPersonMobileNo(caf[15] == null ? "" : caf[15].toString());
				
				List<String> stbsrlNO = Arrays.asList(cafAndCpeChargesVO.getOltSrlNo().split(","));
			    List<String> stbmacAddr = Arrays.asList(cafAndCpeChargesVO.getOltMacAddr().split(","));
			    if(stbsrlNO.size() > 0){
				    for (int i=0; i<stbsrlNO.size(); i++) {
				        map.put(stbsrlNO.get(i), stbmacAddr.get(i));
				    }
				    cafAndCpeChargesVO.setStbSrlNoMacAddrMap(map);
			    }
				cafAndCpeChargesList.add(cafAndCpeChargesVO);
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: searchCafDetails" + e);
			e.printStackTrace();
		} finally {
			cafsList = null;
		}
		return cafAndCpeChargesList;
	}

	@Override
	public List<BalanceAdjustmentBO> searchBalanceAdjustment(String custId, String aadharNo) {
		List<BalanceAdjustmentBO> custInvData= new ArrayList<>();
		custInvData = cafDao.searchBalanceAdjustment(custId, aadharNo);
		return custInvData;
	}

	@Override
	public List<BalanceAdjustmentBO> showCafinvDetailsByCustinvno(String custInvno) {
		List<BalanceAdjustmentBO> cafData = new ArrayList<>();
		cafData = cafDao.showCafinvDetailsByCustinvno(custInvno);
		return cafData;
	}

	@Override
	public List<BalAdjCafinvDtslBO> checkedCafInvCafNos(String acctCafNo, String billNo) {
		List<BalAdjCafinvDtslBO> cafinvdtlsData = new ArrayList<>();
		cafinvdtlsData = cafDao.checkedCafInvCafNos(acctCafNo, billNo);
		return cafinvdtlsData;
	}

	@Override
	@Transactional
	public String saveBalanceAdjustment(List<BalAdjCafinvDtslBO> balAdjCafinvDtslBO, String tenantCodeLMO) {
		try{
			for(BalAdjCafinvDtslBO balAdj : balAdjCafinvDtslBO){
				invAdjustment invresult = null;
				invAdjustment invAdj = new invAdjustment();
				invAdj.setAcctcafno(Long.valueOf(balAdj.getAcctcafno()));
				invAdj.setAdjdate(Calendar.getInstance().getTime());
				invAdj.setAdjdesc("Balance Adjustment");
				invAdj.setAdjmm(DateUtill.getMonth());
				invAdj.setAdjrefId(Long.valueOf(storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.INVADJREF_ID.getCode()).toString()));
				invAdj.setAdjyyyy(DateUtill.getYear());
				invAdj.setApplytaxflag(0);
				invAdj.setApprovededby(balAdj.getTenantcode());
				invAdj.setApprovedipaddr(httpServletRequest.getRemoteAddr());
				invAdj.setApprovedon(Calendar.getInstance());
				invAdj.setCafinvno(Long.valueOf(balAdj.getCafinvno()));
				invAdj.setChargeamt(-Float.valueOf(balAdj.getChargeAmt()));
				invAdj.setChargecode(balAdj.getChargeCode());
				invAdj.setCreatedby(tenantCodeLMO);
				invAdj.setCreatedipaddr(httpServletRequest.getRemoteAddr());
				invAdj.setCreatedon(Calendar.getInstance());
				invAdj.setCustdistuid(Integer.valueOf(balAdj.getCustDistUid()));
				invAdj.setCustinvno(Long.valueOf(balAdj.getCustinvno()));
				invAdj.setEnttax(0);
				invAdj.setFeaturecode(balAdj.getFeatureCode());
				invAdj.setInvdtlid(Long.valueOf(balAdj.getInvdtlId()));
				invAdj.setKisantax(0);
				invAdj.setPmntcustid(Long.valueOf(balAdj.getPmnCustId()));
				invAdj.setProdcafno(Long.valueOf(balAdj.getProdcafno()));
				invAdj.setProdcode(balAdj.getProdCode());
				invAdj.setSrvccode(balAdj.getSrvccode());
				invAdj.setSrvctax(0);
				invAdj.setStatus(Integer.valueOf(balAdj.getStatus()));
				invAdj.setStbcafno(Long.valueOf(balAdj.getStbcafno()));
				invAdj.setSwatchtax(0);
				invAdj.setTenantcode(balAdj.getTenantcode());
				invresult = cafDao.saveBalAdj(invAdj);
				
				GenarateChargesVO genarateChargesVO = new GenarateChargesVO();
				
				genarateChargesVO.setAcctCafno(invresult.getAcctcafno().toString());
				genarateChargesVO.setAdjrefId(invresult.getAdjrefId().toString());
				genarateChargesVO.setChargeAmt(String.valueOf(invresult.getChargeamt()));
				genarateChargesVO.setChargeCode(invresult.getChargecode());
				genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeDescription(null);
				genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
				genarateChargesVO.setDistrict(invresult.getCustdistuid().toString());
				genarateChargesVO.setEntTaxamt("0");
				genarateChargesVO.setFeatureCode(invresult.getFeaturecode());
				genarateChargesVO.setIgnoreBalenceFlag("N");
				genarateChargesVO.setKisanTaxamt("0");
				genarateChargesVO.setPaidAmount(String.valueOf(invresult.getChargeamt()));
				genarateChargesVO.setPmntCustId(invresult.getPmntcustid().toString());
				genarateChargesVO.setProdCafno(invresult.getProdcafno().toString());
				genarateChargesVO.setProdCode(invresult.getProdcode());
				genarateChargesVO.setRsAgrUid("0");
				genarateChargesVO.setSrvcCode(invresult.getSrvccode());
				genarateChargesVO.setSrvcTaxamt("0");
				genarateChargesVO.setStbCafno(invresult.getStbcafno().toString());
				genarateChargesVO.setSwatchTaxamt("0");
				genarateChargesVO.setTenantCode(invresult.getTenantcode());
				
				storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
			}
		}catch(Exception e){
			logger.error("The Exception is CafServiceImpl :: searchCafDetails" + e);
			e.printStackTrace();
			return "false";
		}
		return "success";
	}

	@Override
	@Transactional
	public String changeProfileForCPE(String cpeSrlNo, String newProfileId, String tenantCode) {
		String returnVal = null;
		CpeStock cpeStock = null;
		String tCode = null;
		Cpecharges cpeChargesOld = null;
		Cpecharges cpeChargesNew = null;
		String sameCpeType = null;
		
		try {
			cpeStock = cafDao.findCpeStockBySrlNo(cpeSrlNo);
			if (cpeStock != null) {
				if (cpeStock.getStatus() != 4) {

					if (cpeStock.getStatus() != 99) {
						tCode = cpeStock.getStatus() == 3 ? cpeStock.getLmoCode() : cpeStock.getMspCode();
						sameCpeType = cafDao.findSameCpeType(cpeStock.getProfileId(), Long.parseLong(newProfileId));

						if (sameCpeType.equalsIgnoreCase("true")) {
							if (tCode.equalsIgnoreCase(tenantCode)) {
								cpeChargesOld = cafDao.findCpeCharge(cpeStock.getProfileId(),cpeSrlNo);
								cpeChargesNew = cafDao.findCpeCharge(Long.parseLong(newProfileId),cpeSrlNo);
								cpeStock.setProfileId(Long.parseLong(newProfileId));
								cafDao.saveOrUpdateAnyObject(cpeStock);
								cafDao.updateTenatWallet(cpeChargesOld.getUpFrontCharges(),cpeChargesNew.getUpFrontCharges(), tenantCode);
								returnVal = "Updated Successfully....";
							} else {
								returnVal = "The provided CPE is not allocated to the Tenant...";
							}
						} else {
							returnVal = "CPE Device Model is mismatched...";
						}
					} else {
						returnVal = "CPE Device is not working...";
					}
				} else {
					returnVal = "CPE Device is already in use...";
				}
			} else {
				returnVal = "CPE device is not found....";
			}
		}catch(Exception e){
			logger.error("The Exception is changeProfileForCPE ::" + e);
			returnVal = e.getMessage();
			e.printStackTrace();
		}
		return returnVal;
	}
	
	public List<HSICummSummaryMonthlyCustViewVO> getTotalAAAUsagePerMonthCustDayWise(String month, String year,String cafNo){
		List<Object[]> list= new ArrayList<>();
		List<HSICummSummaryMonthlyCustViewDTO> custlist = new ArrayList<>();
		List<HSICummSummaryMonthlyCustViewVO> usagelist = new ArrayList<>();
		
		try{
			custlist=cafDao.getTotalAAAUsagePerMonthCustomerDayWise(month,year,cafNo);
			
			for(HSICummSummaryMonthlyCustViewDTO obj : custlist){
				HSICummSummaryMonthlyCustViewVO hsiObj=new HSICummSummaryMonthlyCustViewVO();
				
				hsiObj.setUsageYYYY(obj.getUsageYYYY() != null ? obj.getUsageYYYY().toString() : "NA");
				hsiObj.setUsageMM(obj.getUsageMM() != null ? obj.getUsageMM().toString() : "NA");
				hsiObj.setAcctCafNo(obj.getAcctCafNo() != null ? obj.getAcctCafNo().toString() : "NA");
				hsiObj.setDay(obj.getDay() != null ? obj.getDay().toString() : "NA");
				hsiObj.setDaydnldusage(obj.getDaydnldusage() != null ? obj.getDaydnldusage().toString() : "NA");
				hsiObj.setDayupldusage(obj.getDayupldusage() != null ? obj.getDayupldusage().toString() : "NA");
				hsiObj.setDayTotalUsage(obj.getDayTotalUsage() != null ? obj.getDayTotalUsage().toString() : "NA");
				hsiObj.setPlanUsage(obj.getPlanUsage());
				hsiObj.setCustName(obj.getCustName() != null ? obj.getCustName().toString() : "NA");
				
				hsiObj.setDlh01(new BigDecimal(obj.getDlh01() == null ? "0" : obj.getDlh01().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh02(new BigDecimal(obj.getDlh02() == null ? "0" : obj.getDlh02().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh03(new BigDecimal(obj.getDlh03() == null ? "0" : obj.getDlh03().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh04(new BigDecimal(obj.getDlh04() == null ? "0" : obj.getDlh04().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh05(new BigDecimal(obj.getDlh05() == null ? "0" : obj.getDlh05().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh06(new BigDecimal(obj.getDlh06() == null ? "0" : obj.getDlh06().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh07(new BigDecimal(obj.getDlh07() == null ? "0" : obj.getDlh07().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh08(new BigDecimal(obj.getDlh08() == null ? "0" : obj.getDlh08().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh09(new BigDecimal(obj.getDlh09() == null ? "0" : obj.getDlh09().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh10(new BigDecimal(obj.getDlh10() == null ? "0" : obj.getDlh10().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh11(new BigDecimal(obj.getDlh11() == null ? "0" : obj.getDlh11().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh12(new BigDecimal(obj.getDlh12() == null ? "0" : obj.getDlh12().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh13(new BigDecimal(obj.getDlh13() == null ? "0" : obj.getDlh13().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh14(new BigDecimal(obj.getDlh14() == null ? "0" : obj.getDlh14().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh15(new BigDecimal(obj.getDlh15() == null ? "0" : obj.getDlh15().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh16(new BigDecimal(obj.getDlh16() == null ? "0" : obj.getDlh16().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh17(new BigDecimal(obj.getDlh17() == null ? "0" : obj.getDlh17().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh18(new BigDecimal(obj.getDlh18() == null ? "0" : obj.getDlh18().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh19(new BigDecimal(obj.getDlh19() == null ? "0" : obj.getDlh19().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh20(new BigDecimal(obj.getDlh20() == null ? "0" : obj.getDlh20().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh21(new BigDecimal(obj.getDlh21() == null ? "0" : obj.getDlh21().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh22(new BigDecimal(obj.getDlh22() == null ? "0" : obj.getDlh22().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh23(new BigDecimal(obj.getDlh23() == null ? "0" : obj.getDlh23().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setDlh24(new BigDecimal(obj.getDlh24() == null ? "0" : obj.getDlh24().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				
				hsiObj.setUlh01(new BigDecimal(obj.getUlh01() == null ? "0" : obj.getUlh01().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh02(new BigDecimal(obj.getUlh02() == null ? "0" : obj.getUlh02().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh03(new BigDecimal(obj.getUlh03() == null ? "0" : obj.getUlh03().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh04(new BigDecimal(obj.getUlh04() == null ? "0" : obj.getUlh04().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh05(new BigDecimal(obj.getUlh05() == null ? "0" : obj.getUlh05().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh06(new BigDecimal(obj.getUlh06() == null ? "0" : obj.getUlh06().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh07(new BigDecimal(obj.getUlh07() == null ? "0" : obj.getUlh07().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh08(new BigDecimal(obj.getUlh08() == null ? "0" : obj.getUlh08().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh09(new BigDecimal(obj.getUlh09() == null ? "0" : obj.getUlh09().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh10(new BigDecimal(obj.getUlh10() == null ? "0" : obj.getUlh10().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh11(new BigDecimal(obj.getUlh11() == null ? "0" : obj.getUlh11().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh12(new BigDecimal(obj.getUlh12() == null ? "0" : obj.getUlh12().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh13(new BigDecimal(obj.getUlh13() == null ? "0" : obj.getUlh13().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh14(new BigDecimal(obj.getUlh14() == null ? "0" : obj.getUlh14().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh15(new BigDecimal(obj.getUlh15() == null ? "0" : obj.getUlh15().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh16(new BigDecimal(obj.getUlh16() == null ? "0" : obj.getUlh16().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh17(new BigDecimal(obj.getUlh17() == null ? "0" : obj.getUlh17().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh18(new BigDecimal(obj.getUlh18() == null ? "0" : obj.getUlh18().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh19(new BigDecimal(obj.getUlh19() == null ? "0" : obj.getUlh19().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh20(new BigDecimal(obj.getUlh20() == null ? "0" : obj.getUlh20().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh21(new BigDecimal(obj.getUlh21() == null ? "0" : obj.getUlh21().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh22(new BigDecimal(obj.getUlh22() == null ? "0" : obj.getUlh22().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh23(new BigDecimal(obj.getUlh23() == null ? "0" : obj.getUlh23().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				hsiObj.setUlh24(new BigDecimal(obj.getUlh24() == null ? "0" : obj.getUlh24().toString()).divide(new BigDecimal("1024")).setScale(3, RoundingMode.CEILING).toString());
				
				hsiObj.setMonthDay(obj.getMonthDay() != null ? obj.getMonthDay().toString() : "NA");
				hsiObj.setTotalUplSize(obj.getTotalUplSize() != null ? obj.getTotalUplSize().toString() : "NA");
				hsiObj.setTotalDwlSize(obj.getTotalDwlSize() != null ? obj.getTotalDwlSize().toString() : "NA");
				hsiObj.setSubsCount(obj.getSubsCount() != null ? obj.getSubsCount().toString() : "NA");
				
				usagelist.add(hsiObj);
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return usagelist;
	}
	
	
	public List<HSICummSummaryMonthly> getTotalAAAUsagePerMonthDayWise(String month, String year){
		List<HSICummSummaryMonthlyViewDTO> custlist = new ArrayList<>();
		List<HSICummSummaryMonthly> usagelist = new ArrayList<>();
		
		try{
			custlist=cafDao.getTotalAAAUsagePerMonthDaywise(month,year);
			
			for(HSICummSummaryMonthlyViewDTO obj : custlist){
				HSICummSummaryMonthly hsiObj=new HSICummSummaryMonthly();
				
				hsiObj.setDay(obj.getDay() != null ? obj.getDay().toString() : "NA");
				hsiObj.setDaydnldusage(obj.getDaydnldusage() != null ? obj.getDaydnldusage().toString() : "NA");
				hsiObj.setDayupldusage(obj.getDayupldusage() != null ? obj.getDayupldusage().toString() : "NA");
				hsiObj.setDayTotalUsage(obj.getDayTotalUsage() != null ? obj.getDayTotalUsage().toString() : "NA");
				
				hsiObj.setDlh01(obj.getDlh01() != null ? obj.getDlh01().toString() : "NA");
				hsiObj.setDlh02(obj.getDlh02() != null ? obj.getDlh02().toString() : "NA");
				hsiObj.setDlh03(obj.getDlh03() != null ? obj.getDlh03().toString() : "NA");
				hsiObj.setDlh04(obj.getDlh04() != null ? obj.getDlh04().toString() : "NA");
				hsiObj.setDlh05(obj.getDlh05() != null ? obj.getDlh05().toString() : "NA");
				hsiObj.setDlh06(obj.getDlh06() != null ? obj.getDlh06().toString() : "NA");
				hsiObj.setDlh07(obj.getDlh07() != null ? obj.getDlh07().toString() : "NA");
				hsiObj.setDlh08(obj.getDlh08() != null ? obj.getDlh08().toString() : "NA");
				hsiObj.setDlh09(obj.getDlh09() != null ? obj.getDlh09().toString() : "NA");
				hsiObj.setDlh10(obj.getDlh10() != null ? obj.getDlh10().toString() : "NA");
				hsiObj.setDlh11(obj.getDlh11() != null ? obj.getDlh11().toString() : "NA");
				hsiObj.setDlh12(obj.getDlh12() != null ? obj.getDlh12().toString() : "NA");
				hsiObj.setDlh13(obj.getDlh13() != null ? obj.getDlh13().toString() : "NA");
				hsiObj.setDlh14(obj.getDlh14() != null ? obj.getDlh14().toString() : "NA");
				hsiObj.setDlh15(obj.getDlh15() != null ? obj.getDlh15().toString() : "NA");
				hsiObj.setDlh16(obj.getDlh16() != null ? obj.getDlh16().toString() : "NA");
				hsiObj.setDlh17(obj.getDlh17() != null ? obj.getDlh17().toString() : "NA");
				hsiObj.setDlh18(obj.getDlh18() != null ? obj.getDlh18().toString() : "NA");
				hsiObj.setDlh19(obj.getDlh19() != null ? obj.getDlh19().toString() : "NA");
				hsiObj.setDlh20(obj.getDlh20() != null ? obj.getDlh20().toString() : "NA");
				hsiObj.setDlh21(obj.getDlh21() != null ? obj.getDlh21().toString() : "NA");
				hsiObj.setDlh22(obj.getDlh22() != null ? obj.getDlh22().toString() : "NA");
				hsiObj.setDlh23(obj.getDlh23() != null ? obj.getDlh23().toString() : "NA");
				hsiObj.setDlh24(obj.getDlh24() != null ? obj.getDlh24().toString() : "NA");
				
				hsiObj.setUlh01(obj.getUlh01() != null ? obj.getUlh01().toString() : "NA");
				hsiObj.setUlh02(obj.getUlh02() != null ? obj.getUlh02().toString() : "NA");
				hsiObj.setUlh03(obj.getUlh03() != null ? obj.getUlh03().toString() : "NA");
				hsiObj.setUlh04(obj.getUlh04() != null ? obj.getUlh04().toString() : "NA");
				hsiObj.setUlh05(obj.getUlh05() != null ? obj.getUlh05().toString() : "NA");
				hsiObj.setUlh06(obj.getUlh06() != null ? obj.getUlh06().toString() : "NA");
				hsiObj.setUlh07(obj.getUlh07() != null ? obj.getUlh07().toString() : "NA");
				hsiObj.setUlh08(obj.getUlh08() != null ? obj.getUlh08().toString() : "NA");
				hsiObj.setUlh09(obj.getUlh09() != null ? obj.getUlh09().toString() : "NA");
				hsiObj.setUlh10(obj.getUlh10() != null ? obj.getUlh10().toString() : "NA");
				hsiObj.setUlh11(obj.getUlh11() != null ? obj.getUlh11().toString() : "NA");
				hsiObj.setUlh12(obj.getUlh12() != null ? obj.getUlh12().toString() : "NA");
				hsiObj.setUlh13(obj.getUlh13() != null ? obj.getUlh13().toString() : "NA");
				hsiObj.setUlh14(obj.getUlh14() != null ? obj.getUlh14().toString() : "NA");
				hsiObj.setUlh15(obj.getUlh15() != null ? obj.getUlh15().toString() : "NA");
				hsiObj.setUlh16(obj.getUlh16() != null ? obj.getUlh16().toString() : "NA");
				hsiObj.setUlh17(obj.getUlh17() != null ? obj.getUlh17().toString() : "NA");
				hsiObj.setUlh18(obj.getUlh18() != null ? obj.getUlh18().toString() : "NA");
				hsiObj.setUlh19(obj.getUlh19() != null ? obj.getUlh19().toString() : "NA");
				hsiObj.setUlh20(obj.getUlh20() != null ? obj.getUlh20().toString() : "NA");
				hsiObj.setUlh21(obj.getUlh21() != null ? obj.getUlh21().toString() : "NA");
				hsiObj.setUlh22(obj.getUlh22() != null ? obj.getUlh22().toString() : "NA");
				hsiObj.setUlh23(obj.getUlh23() != null ? obj.getUlh23().toString() : "NA");
				hsiObj.setUlh24(obj.getUlh24() != null ? obj.getUlh24().toString() : "NA");
				
				hsiObj.setSch01(obj.getSch01() != null ? obj.getSch01().toString() : "NA");
				hsiObj.setSch02(obj.getSch02() != null ? obj.getSch02().toString() : "NA");
				hsiObj.setSch03(obj.getSch03() != null ? obj.getSch03().toString() : "NA");
				hsiObj.setSch04(obj.getSch04() != null ? obj.getSch04().toString() : "NA");
				hsiObj.setSch05(obj.getSch05() != null ? obj.getSch05().toString() : "NA");
				hsiObj.setSch06(obj.getSch06() != null ? obj.getSch06().toString() : "NA");
				hsiObj.setSch07(obj.getSch07() != null ? obj.getSch07().toString() : "NA");
				hsiObj.setSch08(obj.getSch08() != null ? obj.getSch08().toString() : "NA");
				hsiObj.setSch09(obj.getSch09() != null ? obj.getSch09().toString() : "NA");
				hsiObj.setSch10(obj.getSch10() != null ? obj.getSch10().toString() : "NA");
				hsiObj.setSch11(obj.getSch11() != null ? obj.getSch11().toString() : "NA");
				hsiObj.setSch12(obj.getSch12() != null ? obj.getSch12().toString() : "NA");
				hsiObj.setSch13(obj.getSch13() != null ? obj.getSch13().toString() : "NA");
				hsiObj.setSch14(obj.getSch14() != null ? obj.getSch14().toString() : "NA");
				hsiObj.setSch15(obj.getSch15() != null ? obj.getSch15().toString() : "NA");
				hsiObj.setSch16(obj.getSch16() != null ? obj.getSch16().toString() : "NA");
				hsiObj.setSch17(obj.getSch17() != null ? obj.getSch17().toString() : "NA");
				hsiObj.setSch18(obj.getSch18() != null ? obj.getSch18().toString() : "NA");
				hsiObj.setSch19(obj.getSch19() != null ? obj.getSch19().toString() : "NA");
				hsiObj.setSch20(obj.getSch20() != null ? obj.getSch20().toString() : "NA");
				hsiObj.setSch21(obj.getSch21() != null ? obj.getSch21().toString() : "NA");
				hsiObj.setSch22(obj.getSch22() != null ? obj.getSch22().toString() : "NA");
				hsiObj.setSch23(obj.getSch23() != null ? obj.getSch23().toString() : "NA");
				hsiObj.setSch24(obj.getSch24() != null ? obj.getSch24().toString() : "NA");
				
				hsiObj.setMonthDay(obj.getMonthDay() != null ? obj.getMonthDay().toString() : "NA");
				hsiObj.setTotalUplSize(obj.getTotalUplSize() != null ? obj.getTotalUplSize().toString() : "NA");
				hsiObj.setTotalDwlSize(obj.getTotalDwlSize() != null ? obj.getTotalDwlSize().toString() : "NA");
				hsiObj.setSubsCount(obj.getSubsCount() != null ? obj.getSubsCount().toString() : "NA");
				
				usagelist.add(hsiObj);
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return usagelist;
	}
	
	public HSICummSummaryPreviousMonthVO getPreviousMonthHSIData(){
		HSICummSummaryPreviousMonthVO dto= null;
		try {
			 dto=new HSICummSummaryPreviousMonthVO();
			List<Object[]> hsiSum=new ArrayList<>();
			Calendar cal=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("MM yyyy");
			//cal.add(Calendar.MONTH, -1);
			String yearMM=sdf.format(cal.getTime());
			String yy=yearMM.split(" ")[1];
			String mm=yearMM.split(" ")[0];
			hsiSum=cafDao.getPrevHSIData("I", yy, mm);
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setCurrentMonthIndDownloadSize(totDwn.toString());
				dto.setCurrentMonthIndUploadSize(totUp.toString());
			}
			hsiSum=cafDao.getPrevHSIData("E", yy, mm);
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setCurrentMonthEntDownloadSize(totDwn.toString());
				dto.setCurrentMonthEntUploadSize(totUp.toString());
			}
			cal.add(Calendar.MONTH, -1);
			yearMM=sdf.format(cal.getTime());
			yy=yearMM.split(" ")[1];
			mm=yearMM.split(" ")[0];
			hsiSum=cafDao.getPrevHSIData("I", yy, mm);
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setPreMonthIndDownloadSize(totDwn.toString());
				dto.setPrevMonthIndUploadSize(totUp.toString());
			}
			hsiSum=cafDao.getPrevHSIData("E", yy, mm);
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setPreMonthEntDownloadSize(totDwn.toString());
				dto.setPrevMonthEntUploadSize(totUp.toString());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dto;
	}
	
	
	public HSICummSumPMonthVO getPreviousMonthHSISUMData(){
		HSICummSumPMonthVO dto= null;
		try {
			 dto=new HSICummSumPMonthVO();
			List<Object[]> hsiSum=new ArrayList<>();
			Calendar cal=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("MM yyyy");
			//cal.add(Calendar.MONTH, -1);
			String yearMM=sdf.format(cal.getTime());
			String yy=yearMM.split(" ")[1];
			String mm=yearMM.split(" ")[0];
			String ymc=yy+mm;
			hsiSum=cafDao.getPrevHSISUMData(ymc);
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setCurrentMonthDownloadSize(totDwn.toString());
				dto.setCurrentMonthUploadSize(totUp.toString());
			}
			
			cal.add(Calendar.MONTH, -1);
			yearMM=sdf.format(cal.getTime());
			yy=yearMM.split(" ")[1];
			mm=yearMM.split(" ")[0];
			String ymp=yy+mm;
			hsiSum=cafDao.getPrevHSISUMData( ymp);
			
			for (Object[] obj : hsiSum) {
				BigDecimal totDwn = new BigDecimal(obj[0] == null ? "0" : obj[0].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				BigDecimal totUp = new BigDecimal(obj[1] == null ? "0" : obj[1].toString()).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).divide(new BigDecimal("1024")).setScale(0, RoundingMode.CEILING);
				dto.setPreMonthDownloadSize(totDwn.toString());
				dto.setPrevMonthUploadSize(totUp.toString());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dto;
	}
	
	
	public InvoiceAmountDtls getPreviousMonthInvAmount(){
		InvoiceAmountDtls dto= null;
		try {
			 dto=new InvoiceAmountDtls();
			List<Object> hsiSum=new ArrayList<>();
			Calendar cal=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("MM yyyy");
			cal.add(Calendar.MONTH, -1);
			String yearMM=sdf.format(cal.getTime());
			String yy=yearMM.split(" ")[1];
			String mm=yearMM.split(" ")[0];
			hsiSum=cafDao.getPrevInvoiceDtls("I", yy, mm);
			for (Object obj : hsiSum) {
				dto.setCurrentMonthIndInvAmount(obj == null ? "0" : obj.toString());
			}
			hsiSum=cafDao.getPrevInvoiceDtls("E", yy, mm);
			for (Object obj : hsiSum) {
				dto.setCurrentMonthEntInvAmount(obj == null ? "0" : obj.toString());
			}
			cal.add(Calendar.MONTH, -2);
			yearMM=sdf.format(cal.getTime());
			yy=yearMM.split(" ")[1];
			mm=yearMM.split(" ")[0];
			hsiSum=cafDao.getPrevInvoiceDtls("I", yy, mm);
			for (Object obj : hsiSum) {
				dto.setPrevMonthIndInvAmount(obj == null ? "0" : obj.toString());
			}
			hsiSum=cafDao.getPrevInvoiceDtls("E", yy, mm);
			for (Object obj : hsiSum) {
				dto.setPrevMonthEntInvAmount(obj == null ? "0" : obj.toString());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dto;
	}

// new code 10\1\2018
	

	@Override
	public List<CafAndCpeChargesVO> getMultiActionCafList1(MultiAction multiAction) {
		List<CafAndCpeChargesVO> cafAndCpeChargesList = new ArrayList<CafAndCpeChargesVO>();
		List<Object[]> cafsList = new ArrayList<>();
		try {
			cafsList = cafDao.getMultiActionCafList1(multiAction);
			for (Object[] caf : cafsList) {
				Map<String,String> map=new HashMap<String,String>();
				CafAndCpeChargesVO cafAndCpeChargesVO = new CafAndCpeChargesVO();
				cafAndCpeChargesVO.setCafNo(caf[0] == null ? "" : caf[0].toString());
				cafAndCpeChargesVO.setfName(caf[1] == null ? "" : caf[1].toString());
				cafAndCpeChargesVO.setlName(caf[2] == null ? "" : caf[2].toString());
				cafAndCpeChargesVO.setCustCode(caf[3] != null ? caf[3].toString() : "");
				cafAndCpeChargesVO.setApsflTrackId(caf[4] == null ? "" : caf[4].toString());
				cafAndCpeChargesVO.setCpeSlno(caf[5] == null ? "NA" : caf[5].toString());
				cafAndCpeChargesVO.setCafdate(caf[6] == null ? "" : caf[6].toString());
				cafAndCpeChargesVO.setContactMob(caf[7] == null ? "" : caf[7].toString());
				cafAndCpeChargesVO.setSecDepositCharge(caf[8] == null ? "" : caf[8].toString());
				cafAndCpeChargesVO.setStatusDesc(caf[9] == null ? "" : caf[9].toString());
				cafAndCpeChargesVO.setMonthlyCharges((BigDecimal) (caf[10] == null ? "" : caf[10]));
				cafAndCpeChargesVO.setOnetimeCharges((BigDecimal) (caf[11] == null ? "" : caf[11]));

				cafAndCpeChargesVO.setTelePhoneCharges((BigDecimal) (caf[12] == null ? "" : caf[12]));

//				cafAndCpeChargesVO.setServiceTax((BigDecimal) (caf[13] == null ? "" : caf[13]));

				cafAndCpeChargesVO.setTotal((BigDecimal) (caf[13] == null ? "" : caf[13]));
				cafAndCpeChargesVO.setTotalPayable((BigDecimal) (caf[14] == null ? "" : caf[14]));


				cafAndCpeChargesList.add(cafAndCpeChargesVO);
			}
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: searchCafDetails" + e);
			e.printStackTrace();
		} finally {
			cafsList = null;
		}
		return cafAndCpeChargesList;
	}

	@Override
	public List<MonthlyPaymentBO> getMonthlyCafDetails(String mobileNo, String CafNo, String tenantCode,
			Integer custId) {
		// TODO Auto-generated method stub
		return null;
	}


// new code 30/1/2018

            @Override
		public ComsHelperDTO findParticularCustomer1(ComsHelperDTO comsHelperDTO) {
			LmoDetailsBO  lmosList = new LmoDetailsBO();
			try {
				logger.info("CustomerServiceImpl :: LMOTotalAmount() :: Start");
				lmosList = cafDao.findParticularCustomer1(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype());
				comsHelperDTO.setSumreg(lmosList.getRegBal());
				comsHelperDTO.setSumpmnt(lmosList.getPmntAmt());
			} catch (Exception e) {
				logger.error("CustomerServiceImpl :: LMOTotalAmount() :: " + e);
				e.printStackTrace();
			} finally {

			}
			return comsHelperDTO;
		}

// Change code 09-jan-2018
	
	@Override
	public List<MonthlyPaymentBO> getMonthlyCafDetails1(String cafNo, String tenantCode, Integer custId) {
		List<MonthlyPaymentBO> cafList = new ArrayList<>();
		try {
			cafList = cafDao.getMonthlyCafDetails1(cafNo, tenantCode, custId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMonthlyCafDetails" + e);
			e.printStackTrace();
		} finally {

		}
		return cafList;
	}

	
	@Override
	public BulkCustomerDTO[] getMonthlyCafDetails2(String cafNo, String tenantCode, String custId) {
		List<BulkCustomerDTO> cafList=new ArrayList<>();
		try {
			cafList = cafDao.getMonthlyCafDetails2(cafNo, tenantCode, custId);
		} catch (Exception e) {
			logger.error("The Exception is CafServiceImpl :: getMonthlyCafDetails" + e);
			e.printStackTrace();
		} finally {

		}
		BulkCustomerDTO[] mlist2=new BulkCustomerDTO[cafList.size()];
		mlist2=cafList.toArray(mlist2);
		return mlist2;
	}

	
//new code vijaya
	
	public ComsHelperDTO findCafDaoByCafNo(ComsHelperDTO comsHelperDTO) {
		List<CustomerCafBO> cafsList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: findCustomers() :: Start");
			if(!((comsHelperDTO.getDistrictid()==null)||(comsHelperDTO.getDistrictid().isEmpty())))
			{
				cafsList = cafDao.findCafDaoByCafNo(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(),comsHelperDTO.getDistrictid(),comsHelperDTO.getLmocode1(),comsHelperDTO.getPageObject());
			}
			else if(!((comsHelperDTO.getLmocode1()==null)||(comsHelperDTO.getLmocode1().isEmpty())))
			{
				cafsList = cafDao.findCafDaoByCafNo(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(),comsHelperDTO.getDistrictid(),comsHelperDTO.getLmocode1(), comsHelperDTO.getPageObject());
			}
			else if(!((comsHelperDTO.getMsoCode()==null)||(comsHelperDTO.getMsoCode().isEmpty())))
			{

				cafsList = cafDao.findCafDaoByCafNo(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(),comsHelperDTO.getMsoCode(), comsHelperDTO.getPageObject());
			}
		//	else
//			{
//				cafsList = cafDao.findCafDaoByCafNo(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(), comsHelperDTO.getPageObject());
//
//			}
			else if(!((comsHelperDTO.getUsageMM()==null)||(comsHelperDTO.getUsageMM().isEmpty())))
			{
				cafsList = cafDao.findCafDaoByCafNo1(comsHelperDTO.getTenantCode(), comsHelperDTO.getTenanttype(),comsHelperDTO.getUsageMM(),comsHelperDTO.getUsageYYYY(),comsHelperDTO.getPageObject());
			}
			PageObject pageObject = comsHelperDTO.getPageObject();
			comsHelperDTO.setCafsList(cafsList);
			comsHelperDTO.setCount(pageObject.getTotalDisplayCount());
		} catch (Exception e) {
			logger.error("CustomerServiceImpl :: findCustomers() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return comsHelperDTO;
	}


}
