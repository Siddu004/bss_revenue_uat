package com.arbiva.apfgc.comsystem.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class BulkCustomerDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String prevbal;
	private String pmntcustid;

	public String getPmntcustid() {
		return pmntcustid;
	}

	public void setPmntcustid(String pmntcustid) {
		this.pmntcustid = pmntcustid;
	}

	public String getPrevbal() {
		return prevbal;
	}

	public void setPrevbal(String prevbal) {
		this.prevbal = prevbal;
	}

	@Column(name="cafno")
	private String cafno;

	@Column(name="custname")
	private String customername;
	
	@Column(name="pocmob1")
	private String mobile;
	
	@Column(name="regbal")
	private String dueamount;

    

    public String getCafno ()
    {
        return cafno;
    }

    public void setCafno (String cafno)
    {
        this.cafno = cafno;
    }

    public String getDueamount ()
    {
        return dueamount;
    }

    public void setDueamount (String dueamount)
    {
        this.dueamount = dueamount;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getCustomername ()
    {
        return customername;
    }

    public void setCustomername (String customername)
    {
        this.customername = customername;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cafno = "+cafno+", dueamount = "+dueamount+", mobile = "+mobile+", customername = "+customername+"]";
    }

}
