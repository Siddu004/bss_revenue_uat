/**
 * 
 */
package com.arbiva.apfgc.comsystem.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lakshman
 *
 */
public class CorpusResponseVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CorpusResponceStatus responseStatus;
	
	private List<String> invalidSubsriberCodes;
	
	private List<String> validSubsriberCodes;
	
	private String trackingId;

	public CorpusResponceStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(CorpusResponceStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public List<String> getInvalidSubsriberCodes() {
		return invalidSubsriberCodes;
	}

	public void setInvalidSubsriberCodes(List<String> invalidSubsriberCodes) {
		this.invalidSubsriberCodes = invalidSubsriberCodes;
	}

	public List<String> getValidSubsriberCodes() {
		return validSubsriberCodes;
	}

	public void setValidSubsriberCodes(List<String> validSubsriberCodes) {
		this.validSubsriberCodes = validSubsriberCodes;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	
}
