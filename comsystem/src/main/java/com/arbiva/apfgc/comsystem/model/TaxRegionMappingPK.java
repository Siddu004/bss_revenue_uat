/**
 * 
 */
package com.arbiva.apfgc.comsystem.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lakshman
 *
 */
public class TaxRegionMappingPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pinCode;
	
	private Date effectiveFrom;

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
		result = prime * result + ((pinCode == null) ? 0 : pinCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxRegionMappingPK other = (TaxRegionMappingPK) obj;
		if (effectiveFrom == null) {
			if (other.effectiveFrom != null)
				return false;
		} else if (!effectiveFrom.equals(other.effectiveFrom))
			return false;
		if (pinCode == null) {
			if (other.pinCode != null)
				return false;
		} else if (!pinCode.equals(other.pinCode))
			return false;
		return true;
	}

	
}
