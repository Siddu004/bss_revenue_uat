/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.comsystem.model.BillCycle;

/**
 * @author Lakshman
 *
 */
@Component("billCycleDao")
public class BillCycleDao {

	private static final Logger LOGGER = Logger.getLogger(BillCycleDao.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<BillCycle> findAllBillCycle() {
		List<BillCycle> billCycle = new ArrayList<BillCycle>();
		TypedQuery<BillCycle> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(BillCycle.class.getSimpleName());
		try {
			LOGGER.info("START::findAllBillCycle()");
			query = getEntityManager().createQuery(builder.toString(), BillCycle.class);
			billCycle = query.getResultList();
			LOGGER.info("END::getBillCycle()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION BillCycleDao :: getBillCycle() " + e);
		} finally {
			builder = null;
			query = null;
		}
		return billCycle;
	}

}
