/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import com.arbiva.apfgc.comsystem.BO.BalAdjCafinvDtslBO;
import com.arbiva.apfgc.comsystem.BO.BalanceAdjustmentBO;
import com.arbiva.apfgc.comsystem.BO.CafOldPackagesBO;
import com.arbiva.apfgc.comsystem.BO.ChangePkgBO;
import com.arbiva.apfgc.comsystem.BO.CustomerDetailsBO;
import com.arbiva.apfgc.comsystem.BO.FingerPrintBO;
import com.arbiva.apfgc.comsystem.BO.HsiBO;
import com.arbiva.apfgc.comsystem.BO.MonthlyPaymentBO;
import com.arbiva.apfgc.comsystem.BO.NoOfSTBsBO;
import com.arbiva.apfgc.comsystem.BO.OntIdCustTypeBO;
import com.arbiva.apfgc.comsystem.BO.TerminatePkgBO;
import com.arbiva.apfgc.comsystem.dto.AadharDTO;
import com.arbiva.apfgc.comsystem.dto.BulkCustomerDTO;
import com.arbiva.apfgc.comsystem.dto.ComsHelperDTO;
import com.arbiva.apfgc.comsystem.dto.PageObject;
import com.arbiva.apfgc.comsystem.model.Caf;
import com.arbiva.apfgc.comsystem.model.MultiAction;
import com.arbiva.apfgc.comsystem.model.VPNSrvcNames;
import com.arbiva.apfgc.comsystem.vo.BillingInfoVO;
import com.arbiva.apfgc.comsystem.vo.CafAndCpeChargesVO;
import com.arbiva.apfgc.comsystem.vo.CafsVO;
import com.arbiva.apfgc.comsystem.vo.CpeInformationVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.FeatureParamsVO;
import com.arbiva.apfgc.comsystem.vo.FilterPackagesVO;
import com.arbiva.apfgc.comsystem.vo.FingerPrintJson;
import com.arbiva.apfgc.comsystem.vo.HSICummSumPMonthVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthly;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryMonthlyCustViewVO;
import com.arbiva.apfgc.comsystem.vo.HSICummSummaryPreviousMonthVO;
import com.arbiva.apfgc.comsystem.vo.InvoiceAmountDtls;
import com.arbiva.apfgc.comsystem.vo.TelephoneNoVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneServcVO;
import com.arbiva.apfgc.comsystem.vo.VPNServiceExcelUploadVO;

/**
 * @author Arbiva
 *
 */
public interface CafService {
	
	public abstract Caf findByCafNo(Long cafNo);

	public abstract void saveCaf(CustomerCafVO customerCafVO, Long cafNo, Long customerId, String loginID, Integer custDistrict);
	
	public List<CafAndCpeChargesVO> searchCafDetails(MultiAction multiAction, PageObject pageObject);

	public abstract CpeInformationVO getPaymentDetails(Long cafNo, String billCycle);

	public abstract List<MonthlyPaymentBO> getMonthlyCafDetails(String mobileNo, String tenantCode, Integer custId);

	public abstract List<Object[]> getCustomerInformationByTenantCode(String tenantCode);

	public Integer getMaxOltOnuNumber(String oltPortId, String oltId);

	public Float getProdChargeByRegionCode(String tenantCode, String prodCode, String regionCode);
	
	public String getRegionCodeByPinCode(String district, String mandal, String village);
	
	public abstract ComsHelperDTO findCafDaoByCafNo(ComsHelperDTO comsHelperDTO);
	
	public abstract Object[] getAllParticularCPE(Long cafNo);

	public abstract List<Object[]> getCafChages(Long cafNo);
	
	public abstract List<Object[]> getCafFeatureService(Long cafNo);

	public abstract Object[] getCafChargeTaxPecentage(String chargeCode, String district, String mandal, String village);

	public abstract AadharDTO getAadharDetails(String aadharNo) throws RemoteException;

	public abstract CustomerCafVO getPackageInformation(Long cafNumber, String tenantType);

	public abstract String getSubCode(Long cafNo);
	
	//public abstract String corpusCallStatus(FingerPrintJson fingerPrintJson);

	public abstract String corpusCall(FingerPrintJson fingerPrintJson);
	
	public abstract String getVATAmountByRegion(String regionCode, String billCycle);

	public abstract String deActivateCaf(Long cafNo);

	public abstract String getCpeModel(String cpeModal);

	public abstract List<FeatureParamsVO> getFeatureParamList(String featureCodes);
	
	public String getStdCodeByVillage(Integer district, Integer mandal, Integer village);
	
	public abstract List<TelephoneServcVO> getTelephoneServc(Long cafNo);
	
	public List<TelephoneNoVO> getTelephoneNo(Long cafNo);
	
	public abstract List<BillingInfoVO>  getBillingInfo(Long cafNo);
	
	//public abstract OnuStbChargesVO getOnuStbChargeDetails(Long cafNo);
	
	public abstract List<CafOldPackagesBO> getOldPackageInformation(Long cafNumber);

	public abstract void updateCpeStockPaymentStatus(Long cafNo, String srlNo, String loginId, String lmoCode);

	public abstract FilterPackagesVO getFilterPackagessList(CustomerCafVO customerCafVO, Map<String, Integer> packagesMap);

	public abstract String getCpeStackStatusBySrlNo(String cpeId, String cpeModel);

	public abstract List<FingerPrintBO> getIndividualCafFPActivationInformation(PageObject pageObject);

	public abstract List<FingerPrintBO> getGroupCafFPActivationInformation(FingerPrintBO fingerPrintBO);

	public abstract Object[] getCpeChargeDetailsForMobileApp(String chargeCode, String regionCode);

	public abstract Caf saveBulkUploadCafs(CustomerCafVO customerCafVO, String loginId, Long uploadId);

	public abstract String getCoreSrvcCodeByCafNo(Long cafNo);

	public abstract Caf updateCafDetails(CustomerCafVO customerCafVO);

	public abstract VPNSrvcNames saveVPNServicesUpload(VPNServiceExcelUploadVO vpnServiceExcelUploadVO, Long uploadId);

	public abstract CpeInformationVO getSISelectedPackages(String prodCodes, String billCycle, String regionCode);

	public abstract List<String> getFingerPrintDetailsFromConfig();
	
	public abstract Object[] getMacAddressBySerialNo(String srlNo);

	List<Object[]> findCafByCafNo(Long custId);

	public abstract List<MonthlyPaymentBO> getMonthlyCafDetailsForMobile(String mobileNo, String tenantCode);

	public abstract List<OntIdCustTypeBO> getOntIdandCustTypeList(String cafNo);
	
	public abstract CustomerDetailsBO getCustomerDetailsByCafNo(Long cafNo);

	public abstract String hasIPTVBoxForCaf(Long cafNumber);

	public abstract String getCustomerBalance(Integer custId, String tenantCode);

	public abstract List<NoOfSTBsBO> getNoOfSTBsForCaf(Long cafNumber);

	public abstract ChangePkgBO getProdCodeForCaf(Long cafNumber);

	public abstract String getSecurityDepositByProdcode(String prodCafNo, String prodCode, String regionCode);

	String[] getGolbalOrGroupSubCodes(String district, String mandal, String villge);
	
	public abstract void save(Caf caf);

	public abstract List<TerminatePkgBO> getProdInfoByCafNo(Long cafNo);

	public abstract ComsHelperDTO viewEntCafDetails(ComsHelperDTO comsHelperDTO);

	public abstract List<HsiBO> getHSIParameters(String srvcCode);

	public abstract List<CafsVO> getCafInfo(Long cafNo);

	List<CafAndCpeChargesVO> getMultiActionCafList(MultiAction multiAction);

	public abstract List<BalanceAdjustmentBO> searchBalanceAdjustment(String custId, String aadharNo);

	public abstract List<BalanceAdjustmentBO> showCafinvDetailsByCustinvno(String custInvno);

	public abstract List<BalAdjCafinvDtslBO> checkedCafInvCafNos(String acctCafNo, String billNo);

	public abstract String saveBalanceAdjustment(List<BalAdjCafinvDtslBO> balAdjCafinvDtslBO, String tenantCodeLMO);

	public abstract String changeProfileForCPE(String cpeSrlNo, String newProfileId, String tenantCode);

	public abstract List<HSICummSummaryMonthlyCustViewVO> getTotalAAAUsagePerMonthCustDayWise(String month, String year,
			String cafNo);
	
	public List<HSICummSummaryMonthly> getTotalAAAUsagePerMonthDayWise(String month, String year);

	public HSICummSummaryPreviousMonthVO getPreviousMonthHSIData();
	
	public InvoiceAmountDtls getPreviousMonthInvAmount();
	
	public List<Caf> findListedCafs(List<Long> cafLists);

        public abstract ComsHelperDTO findParticularCustomer1(ComsHelperDTO comsHelperDTO);

	List<MonthlyPaymentBO> getMonthlyCafDetails1(String cafNo, String tenantCode, Integer custId);

	List<CafAndCpeChargesVO> getMultiActionCafList1(MultiAction multiAction);

	List<MonthlyPaymentBO> getMonthlyCafDetails(String mobileNo, String CafNo, String tenantCode, Integer custId);

	public abstract HSICummSumPMonthVO getPreviousMonthHSISUMData();

	public BulkCustomerDTO[] getMonthlyCafDetails2(String cafNo, String tenantCode, String custId);

}
