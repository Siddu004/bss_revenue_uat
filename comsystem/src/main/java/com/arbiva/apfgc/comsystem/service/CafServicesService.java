/**
 * 
 */
package com.arbiva.apfgc.comsystem.service;

import java.util.List;

import com.arbiva.apfgc.comsystem.model.CafServices;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;

/**
 * @author Arbiva
 *
 */
public interface CafServicesService {


	public abstract void createCafServices(CustomerCafVO customerCafVO, Long cafNo, String loginID, String pmntCustId, Long stbCafNo, String stbPkgCode, Long addPkgCafNo);

	public abstract List<Object[]> getSrvcCodeAndCoreSrvcCodeByCafNo(Long cafNo);

	public abstract CafServices getCafServicesByCafnoAndSrvcCode(Long cafNo, String srvcCode);

	public abstract boolean updateCafServices(String prodCafNo, String prodCode, String loginId, String ipAddress);
}
