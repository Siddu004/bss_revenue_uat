/**
 * 
 */
package com.arbiva.apfgc.comsystem.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;

/**
 * @author Arbiva
 *
 */
@Repository
public class CafSrvcPhoneNosDao {

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public CafSrvcPhoneNos saveCafSrvcPhoneNos(CafSrvcPhoneNos cafSrvcPhoneNos) {
		return getEntityManager().merge(cafSrvcPhoneNos);
	}
}
