package com.arbiva.apfgc.comsystem.vo;

public class HSICummSummaryPreviousMonthVO {
	
	private String currentMonthIndUploadSize;
	
	private String currentMonthIndDownloadSize;
	
	private String currentMonthEntUploadSize;
	
	private String currentMonthEntDownloadSize;
	
	private String prevMonthIndUploadSize;
	
	private String preMonthIndDownloadSize;
	
	private String prevMonthEntUploadSize;
	
	private String preMonthEntDownloadSize;

	public String getCurrentMonthIndUploadSize() {
		return currentMonthIndUploadSize;
	}

	public void setCurrentMonthIndUploadSize(String currentMonthIndUploadSize) {
		this.currentMonthIndUploadSize = currentMonthIndUploadSize;
	}

	public String getCurrentMonthIndDownloadSize() {
		return currentMonthIndDownloadSize;
	}

	public void setCurrentMonthIndDownloadSize(String currentMonthIndDownloadSize) {
		this.currentMonthIndDownloadSize = currentMonthIndDownloadSize;
	}

	public String getCurrentMonthEntUploadSize() {
		return currentMonthEntUploadSize;
	}

	public void setCurrentMonthEntUploadSize(String currentMonthEntUploadSize) {
		this.currentMonthEntUploadSize = currentMonthEntUploadSize;
	}

	public String getCurrentMonthEntDownloadSize() {
		return currentMonthEntDownloadSize;
	}

	public void setCurrentMonthEntDownloadSize(String currentMonthEntDownloadSize) {
		this.currentMonthEntDownloadSize = currentMonthEntDownloadSize;
	}

	public String getPrevMonthIndUploadSize() {
		return prevMonthIndUploadSize;
	}

	public void setPrevMonthIndUploadSize(String prevMonthIndUploadSize) {
		this.prevMonthIndUploadSize = prevMonthIndUploadSize;
	}

	public String getPreMonthIndDownloadSize() {
		return preMonthIndDownloadSize;
	}

	public void setPreMonthIndDownloadSize(String preMonthIndDownloadSize) {
		this.preMonthIndDownloadSize = preMonthIndDownloadSize;
	}

	public String getPrevMonthEntUploadSize() {
		return prevMonthEntUploadSize;
	}

	public void setPrevMonthEntUploadSize(String prevMonthEntUploadSize) {
		this.prevMonthEntUploadSize = prevMonthEntUploadSize;
	}

	public String getPreMonthEntDownloadSize() {
		return preMonthEntDownloadSize;
	}

	public void setPreMonthEntDownloadSize(String preMonthEntDownloadSize) {
		this.preMonthEntDownloadSize = preMonthEntDownloadSize;
	}	
	
	
}
