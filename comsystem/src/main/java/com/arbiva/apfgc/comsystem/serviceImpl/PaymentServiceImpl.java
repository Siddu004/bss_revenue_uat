/**
 * 
 */
package com.arbiva.apfgc.comsystem.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.dao.impl.CafDao;
import com.arbiva.apfgc.comsystem.dao.impl.CafServicesDao;
import com.arbiva.apfgc.comsystem.dao.impl.PaymentDao;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.CafSrvcPhoneNos;
import com.arbiva.apfgc.comsystem.model.Customer;
import com.arbiva.apfgc.comsystem.model.OLPayment;
import com.arbiva.apfgc.comsystem.model.OLTPortDetails;
import com.arbiva.apfgc.comsystem.service.CPEInformationService;
import com.arbiva.apfgc.comsystem.service.CafAccountService;
import com.arbiva.apfgc.comsystem.service.CafChargesService;
import com.arbiva.apfgc.comsystem.service.CafFeatureParamsService;
import com.arbiva.apfgc.comsystem.service.CafProductsService;
import com.arbiva.apfgc.comsystem.service.CafService;
import com.arbiva.apfgc.comsystem.service.CafServicesService;
import com.arbiva.apfgc.comsystem.service.CafSrvcPhoneNosService;
import com.arbiva.apfgc.comsystem.service.CustomerService;
import com.arbiva.apfgc.comsystem.service.PaymentService;
import com.arbiva.apfgc.comsystem.service.PhonenosService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.util.DateUtill;
import com.arbiva.apfgc.comsystem.vo.AddtionalServicesVO;
import com.arbiva.apfgc.comsystem.vo.BillingInfoVO;
import com.arbiva.apfgc.comsystem.vo.CafDetailsVO;
import com.arbiva.apfgc.comsystem.vo.CafStbsVO;
import com.arbiva.apfgc.comsystem.vo.ChargeVO;
import com.arbiva.apfgc.comsystem.vo.CustomerCafVO;
import com.arbiva.apfgc.comsystem.vo.GenarateChargesVO;
import com.arbiva.apfgc.comsystem.vo.IPTVBoxVO;
import com.arbiva.apfgc.comsystem.vo.PaymentDetailsVO;
import com.arbiva.apfgc.comsystem.vo.PaymentVO;
import com.arbiva.apfgc.comsystem.vo.ProductsVO;
import com.arbiva.apfgc.comsystem.vo.RecentPaymentVO;
import com.arbiva.apfgc.comsystem.vo.TaxesVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneNoVO;
import com.arbiva.apfgc.comsystem.vo.TelephoneServcVO;
import com.arbiva.apfgc.comsystem.vo.TenantWalletVO;
import com.arbiva.apfgc.comsystem.vo.TicketDetailsVO;
import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;
import com.arbiva.apfgc.provision.dto.CustomerDTO;

/**
 * @author Lakshman
 *
 */
@Service
public class PaymentServiceImpl implements PaymentService {
	
	private static final Logger logger = Logger.getLogger(PaymentServiceImpl.class);
	
	@Autowired
	PaymentDao paymentDao;
	
	@Autowired
	CafDao cafDao;
	
	@Autowired
	CafServicesDao cafServicesDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	@Autowired
	CafSrvcPhoneNosService cafSrvcPhoneNosService;
	
	@Autowired
	CafService cafService;
	
	@Autowired
	TenantWalletServiceImpl tenantWalletService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	CafServicesService cafServicesService;
	
	@Autowired
	CafProductsService cafProductsService;
	
	@Autowired
	CafAccountService cafAccountService;
	
	@Autowired
	CafChargesService cafChargesService;
	
	@Autowired
	PhonenosService phonenosService;
	
	@Autowired
	CafFeatureParamsService cafFeatureParamsService;
	
	@Autowired
	CPEInformationService cpeInformationService;
	
	@Autowired
	OLTPortDetailsServiceImpl oltPortDetailsService;
	
	@Autowired
	ProvisioningBusinessServiceImpl provisioningBusinessService;
	
	@Override
	@Transactional
	//modified by chaitanya_xyz
	public String createPayment(PaymentVO paymentVO, String loginID, String onuNumber,int cardnum) throws Exception {
		CafSrvcPhoneNos cafSrvcPhoneNos = null;
		OLTPortDetails oltPortDetails = null;
		String phoneNo = "";
		String status = "";
		Long paymentId;
		OLPayment oLPayment;
		TenantWalletVO tenantWallet = new TenantWalletVO();
		try {
			paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
			paymentVO.setPmntId(paymentId.toString());
			oLPayment = new OLPayment(paymentVO);
			oLPayment.setPmntId(paymentId.longValue());
			oLPayment.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo());
			if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				oLPayment.setCustId(paymentVO.getCustomerCafVO().getCustId().longValue());
			} else {
				oLPayment.setCustId(Long.parseLong(paymentVO.getCustomerCafVO().getCustCode()));
			}
			oLPayment.setPmntRefNo(paymentVO.getTransactionNo());
			if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				Customer customer = customerService.findByCustomerId(paymentVO.getCustomerCafVO().getCustId().longValue());
				oLPayment.setDistrictuid(customer.getCustdistUid());
			} else {
				oLPayment.setDistrictuid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
			}
			oLPayment.setCreatedBy(loginID);
			oLPayment.setStatus(ComsEnumCodes.WORKORDER_PAYMENT_STATUS.getStatus());
			oLPayment.setCreatedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			tenantWallet = tenantWalletService.getTenantWalletDetails(paymentVO.getCustomerCafVO().getLmoCode());
			if (tenantWallet.getActualUserAmount() >= paymentVO.getPaidAmount()) {
				paymentDao.savePayment(oLPayment);
				logger.info("The Payment Details Created Successfully");
				
				if(!ComsEnumCodes.SI_Tenant_Type.getCode().equalsIgnoreCase(paymentVO.getCustomerCafVO().getTenantType())) {
					oltPortDetails = oltPortDetailsService.updateOLTPortDetails(paymentVO.getCustomerCafVO(),paymentDao.getCard(paymentVO.getOltSrlNo()));
					if(oltPortDetails == null) {
						throw new RuntimeException("Slot is already assigned. Please try anotherone.");
					} 	
					logger.info("The OLTPortDetails Updated Successfully");
				}
				
				if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_VOIP.getCode()) != -1 ) {
					cafSrvcPhoneNos = cafSrvcPhoneNosService.saveCafSrvcPhoneNos(paymentVO.getCustomerCafVO().getCafNo(), loginID, Integer.parseInt(paymentVO.getCustomerCafVO().getDistrict()), Integer.parseInt(paymentVO.getCustomerCafVO().getMandal()), Integer.parseInt(paymentVO.getCustomerCafVO().getCity()), paymentVO.getCustomerCafVO().getIpAddress(), paymentVO.getCustomerCafVO().getEntCustType(), paymentVO.getCustomerCafVO().getNoOfTPConn(), 0l, null);
					logger.info("cafSrvcPhoneNosService save Successfully");
					
					String telePhoneNos[] = cafSrvcPhoneNos.getTelePhoneNos().split(",");
					for(String telephone : telePhoneNos) {
						phonenosService.updatePhonenos(telephone, loginID, paymentVO.getCustomerCafVO().getIpAddress());
						logger.info("PhoneNos Table Updated Successfully");
					}
				}

				if (paymentVO.getCustomerCafVO().getInstallmentCount() != null && !paymentVO.getCustomerCafVO().getInstallmentCount().isEmpty()) {
					if (new BigDecimal(paymentVO.getCustomerCafVO().getCpePrice()).compareTo(BigDecimal.ZERO) > 0 && paymentVO.getCustomerCafVO().getInstallmentCount().equalsIgnoreCase("0")) {
						cafChargesService.saveCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
						logger.info("The cafCharges Details Created Successfully");
					} else if(new BigDecimal(paymentVO.getCustomerCafVO().getCpePrice()).compareTo(BigDecimal.ZERO) > 0 && !paymentVO.getCustomerCafVO().getInstallmentCount().equalsIgnoreCase("0")) {
						cafChargesService.saveUpfrontCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
						logger.info("The cafCharges Details Created Successfully");
						
						cafChargesService.saveCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
						logger.info("The cafCharges Details Created Successfully");
					}
				}
				if (paymentVO.getCustomerCafVO().getInstCharge() != null && paymentVO.getCustomerCafVO().getInstCharge() > 0) {
					cafChargesService.createCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
				}
				if (paymentVO.getCustomerCafVO().getCableCharge() != null && paymentVO.getCustomerCafVO().getCableCharge().isEmpty() && new BigDecimal(paymentVO.getCustomerCafVO().getCableCharge()).compareTo(BigDecimal.ZERO) > 0) {
					cafChargesService.createExtraCableCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
				}
				//modified by chaitanya_xyz
				//customerService.updateCustomerPaymentStatus(paymentVO.getCustomerCafVO(), paymentVO, loginID, onuNumber,1);
				customerService.updateCustomerPaymentStatus(paymentVO.getCustomerCafVO(), paymentVO, loginID, onuNumber,cardnum);
				logger.info("The Customer and Caf Status Updated Successfully");
				
				cafService.updateCpeStockPaymentStatus(paymentVO.getCustomerCafVO().getCafNo(), paymentVO.getCustomerCafVO().getCpeId(), loginID, paymentVO.getCustomerCafVO().getLmoCode());
				logger.info("Customer and Caf Status Updated Successfully");
				
				tenantWalletService.updateTenantWallet(paymentVO, loginID);
				logger.info("The LMO Wallet Balence Updated Successfully");
				
				status = this.generateChargesAndWorkorderProcessPayments(paymentVO);
				if(status.equalsIgnoreCase("Success")) {
					logger.info("The cafInvDetails Details Created Successfully");
				} else {
					throw new RuntimeException("Insufficient Payment");
				}
				phoneNo = cafSrvcPhoneNos != null ? cafSrvcPhoneNos.getTelePhoneNos() + " and Password(s) is " + cafSrvcPhoneNos.getTelePhonePwds() : phoneNo;
			} else {
				phoneNo = "";
				throw new RuntimeException("In Sufficient Wallet Balance.");
			}
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::createPayment() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			cafSrvcPhoneNos = null;
			paymentId = null;
			oLPayment = null;
			tenantWallet = null;
		}
		return phoneNo;
	}
	
	@Override
	@Transactional
	//modified by chaitanya_xyz
	public String saveEnterpriseCustomerPayment(PaymentVO paymentVO, String loginID, String onuNumber,int cardnum) throws Exception {
		CafSrvcPhoneNos cafSrvcPhoneNos = null;
		OLTPortDetails oltPortDetails = null;
		String phoneNo = "";
		String status = "";
		try {
			tenantWalletService.updateTenantWallet(paymentVO, loginID);
			logger.info("The LMO Wallet Balence Updated Successfully");
			
			if(!ComsEnumCodes.SI_Tenant_Type.getCode().equalsIgnoreCase(paymentVO.getCustomerCafVO().getTenantType())) {
				oltPortDetails = oltPortDetailsService.updateOLTPortDetails(paymentVO.getCustomerCafVO(),paymentDao.getCard(paymentVO.getOltSrlNo()));
				if(oltPortDetails == null) {
					throw new RuntimeException("Slot is already assigned. Please try anotherone.");
				} 	
				logger.info("The OLTPortDetails Updated Successfully");
			}
			
			if(paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_VOIP.getCode()) != -1 ) {
				cafSrvcPhoneNos = cafSrvcPhoneNosService.saveCafSrvcPhoneNos(paymentVO.getCustomerCafVO().getCafNo(), loginID, Integer.parseInt(paymentVO.getCustomerCafVO().getDistrict()), Integer.parseInt(paymentVO.getCustomerCafVO().getMandal()), Integer.parseInt(paymentVO.getCustomerCafVO().getCity()), paymentVO.getCustomerCafVO().getIpAddress(), paymentVO.getCustomerCafVO().getEntCustType(), paymentVO.getCustomerCafVO().getNoOfTPConn(), 0l, null);
				logger.info("cafSrvcPhoneNosService save Successfully");
	
				String telePhoneNos[] = cafSrvcPhoneNos.getTelePhoneNos().split(",");
				for(String telephone : telePhoneNos) {
					phonenosService.updatePhonenos(telephone, loginID, paymentVO.getCustomerCafVO().getIpAddress());
					logger.info("PhoneNos Table Updated Successfully");
				}
			}
			
			if (paymentVO.getCustomerCafVO().getInstallmentCount() != null) {
				if (!paymentVO.getCustomerCafVO().getInstallmentCount().isEmpty() && new BigDecimal(paymentVO.getCustomerCafVO().getCpePrice()).compareTo(BigDecimal.ZERO) > 0 && paymentVO.getCustomerCafVO().getInstallmentCount().equalsIgnoreCase("0")) {
					cafChargesService.saveCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
				} else {
					cafChargesService.saveUpfrontCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
					
					cafChargesService.saveCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
				}
			}
			if (paymentVO.getCustomerCafVO().getInstCharge() != null && paymentVO.getCustomerCafVO().getInstCharge() > 0) {
				cafChargesService.createCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
				logger.info("The cafCharges Details Created Successfully");
			}
			if (paymentVO.getCustomerCafVO().getCableCharge() != null) {
				if (!paymentVO.getCustomerCafVO().getCableCharge().isEmpty() && new BigDecimal(paymentVO.getCustomerCafVO().getCableCharge()).compareTo(BigDecimal.ZERO) > 0) {
					cafChargesService.createExtraCableCafCharges(paymentVO.getCustomerCafVO(), paymentVO.getCustomerCafVO().getCafNo(), loginID, paymentVO.getCustomerCafVO().getCustId().longValue());
					logger.info("The cafCharges Details Created Successfully");
				}
			}
			//modified by chaitanya_xyz
			//customerService.updateCustomerPaymentStatus(paymentVO.getCustomerCafVO(), paymentVO, loginID, onuNumber);
			customerService.updateCustomerPaymentStatus(paymentVO.getCustomerCafVO(), paymentVO, loginID, onuNumber,cardnum);
			logger.info("The Customer and Caf Status Updated Successfully");
			
			cafService.updateCpeStockPaymentStatus(paymentVO.getCustomerCafVO().getCafNo(), paymentVO.getCustomerCafVO().getCpeId(), loginID, paymentVO.getCustomerCafVO().getLmoCode());
			logger.info("Customer and Caf Status Updated Successfully");
			
			status = this.generateChargesAndWorkorderProcessPayments(paymentVO);
			if(status.equalsIgnoreCase("Success")) {
				logger.info("The cafInvDetails Details Created Successfully");
			} else {
				throw new RuntimeException("Insufficient Payment");
			}
			phoneNo = cafSrvcPhoneNos != null ? cafSrvcPhoneNos.getTelePhoneNos() + " and Password(s) is " + cafSrvcPhoneNos.getTelePhonePwds() : phoneNo;
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::createPayment() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			cafSrvcPhoneNos = null;
		}
		return phoneNo;
	}
	
	@Override
	@Transactional
	public String saveAddpackage(PaymentVO paymentVO, List<ProductsVO> changePkgList, String message) throws Exception {
		String status = "";
		String phoneNo = "";
		Long paymentId;
		OLPayment oLPayment;
		TenantWalletVO tenantWallet = new TenantWalletVO();
		try {
			if((message.equalsIgnoreCase("success") && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) || (paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()))) {
				Long addPkgCafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
				if(paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) || paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
					CustomerCafVO customerCafVO = paymentVO.getCustomerCafVO();
					customerCafVO.setProdCafNo(addPkgCafNo.toString());
					paymentVO.setCustomerCafVO(customerCafVO);
				}
				if(paymentVO.getPaidAmount() != null && paymentVO.getPaidAmount() > 0) {
					paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
					paymentVO.setPmntId(paymentId.toString());
					oLPayment = new OLPayment(paymentVO);
					oLPayment.setAcctCafNo(paymentVO.getCustomerCafVO().getCafNo());
					oLPayment.setPmntId(paymentId.longValue());
					if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
						oLPayment.setCustId(paymentVO.getCustomerCafVO().getCustId().longValue());
					} else {
						oLPayment.setCustId(Long.parseLong(paymentVO.getCustomerCafVO().getCustCode()));
					}
					oLPayment.setPmntRefNo(paymentVO.getTransactionNo());
					if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
						Customer customer = customerService.findByCustomerId(paymentVO.getCustomerCafVO().getCustId().longValue());
						oLPayment.setDistrictuid(customer.getCustdistUid());
					} else {
						oLPayment.setDistrictuid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
					}
					oLPayment.setStatus(ComsEnumCodes.WORKORDER_PAYMENT_STATUS.getStatus());
					oLPayment.setCreatedBy(paymentVO.getLoginId());
					oLPayment.setCreatedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
					tenantWallet = tenantWalletService.getTenantWalletDetails(paymentVO.getCustomerCafVO().getLmoCode());
					if (tenantWallet.getActualUserAmount() >= paymentVO.getPaidAmount()) {
						paymentDao.savePayment(oLPayment);
						logger.info("The Payment Details Created Successfully");

						tenantWalletService.updateTenantWallet(paymentVO, paymentVO.getLoginId());
						logger.info("The LMO Wallet Balence Updated Successfully");
						
						if(paymentVO.getCustomerCafVO().getIptvBoxList() != null && !paymentVO.getCustomerCafVO().getIptvBoxList().isEmpty() && paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) != -1) {
							for(IPTVBoxVO iPTVBoxVO : paymentVO.getCustomerCafVO().getIptvBoxList()) {
								if(iPTVBoxVO.getStbSerialNo() != null && iPTVBoxVO.getMacAddress() != null && !iPTVBoxVO.getStbSerialNo().isEmpty() && !iPTVBoxVO.getMacAddress().isEmpty() ) {
									cafProductsService.createCafProducts(paymentVO.getCustomerCafVO(), paymentVO.getCafNo(), paymentVO.getLoginId(), paymentVO.getCustomerCafVO().getCustCode(), Long.parseLong(iPTVBoxVO.getStbCafNo()), iPTVBoxVO.getIptvSrvcCodes(), paymentVO.getPaidAmount(), addPkgCafNo);
									logger.info("The cafProducts Created Successfully");
								}
							}
						} else {
							cafProductsService.createCafProducts(paymentVO.getCustomerCafVO(), paymentVO.getCafNo(), paymentVO.getLoginId(), paymentVO.getCustomerCafVO().getCustCode(), 0l, "", null, addPkgCafNo);
							logger.info("The cafProducts Created Successfully");
						}
						
						if(paymentVO.getCustomerCafVO().getChangePkgFlag() != null && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
							status = this.generateChargesForReverseSecDeposit(paymentVO.getCustomerCafVO(), changePkgList);
							if(status.equalsIgnoreCase("Success")) {
								status = this.generateChargesAndAddPackagePayments(paymentVO.getCustomerCafVO(), addPkgCafNo, paymentVO.getPaidAmount(), 0l);
								logger.info("The cafInvDetails Details Created Successfully");
								if(status.equalsIgnoreCase("Success")) {
									logger.info("The cafInvDetails Details Created Successfully");
								} else {
									throw new RuntimeException("The cafInvDetails Details Insertion Failed");
								}
							} else {
								throw new RuntimeException("Insufficient Payment");
							}
						}
					} else {
						phoneNo = "";
						throw new RuntimeException("In Sufficient Wallet Balance.");
					}
				} else {
					if(paymentVO.getCustomerCafVO().getIptvBoxList() != null && !paymentVO.getCustomerCafVO().getIptvBoxList().isEmpty() && paymentVO.getCustomerCafVO().getCoreSrvcCode().indexOf(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()) != -1) {
						for(IPTVBoxVO iPTVBoxVO : paymentVO.getCustomerCafVO().getIptvBoxList()) {
							if(iPTVBoxVO.getStbSerialNo() != null && iPTVBoxVO.getMacAddress() != null && !iPTVBoxVO.getStbSerialNo().isEmpty() && !iPTVBoxVO.getMacAddress().isEmpty() ) {
								cafProductsService.createCafProducts(paymentVO.getCustomerCafVO(), paymentVO.getCafNo(), paymentVO.getLoginId(), paymentVO.getCustomerCafVO().getCustCode(), Long.parseLong(iPTVBoxVO.getStbCafNo()), iPTVBoxVO.getIptvSrvcCodes(), null, addPkgCafNo);
								logger.info("The cafProducts Created Successfully");
							}
						}
					} else {
						cafProductsService.createCafProducts(paymentVO.getCustomerCafVO(), paymentVO.getCafNo(), paymentVO.getLoginId(), paymentVO.getCustomerCafVO().getCustCode(), 0l, "", null, addPkgCafNo);
						logger.info("The cafProducts Created Successfully");
					}
					
					if(paymentVO.getCustomerCafVO().getChangePkgFlag() != null && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Change_Pkg_Flag.getCode())) {
						status = this.generateChargesForReverseSecDeposit(paymentVO.getCustomerCafVO(), changePkgList);
						if(status.equalsIgnoreCase("Success")) {
							status = this.generateChargesAndAddPackagePayments(paymentVO.getCustomerCafVO(), addPkgCafNo, paymentVO.getPaidAmount(), 0l);
							logger.info("The cafInvDetails Details Created Successfully");
							if(status.equalsIgnoreCase("Success")) {
								logger.info("The cafInvDetails Details Created Successfully");
							} else {
								throw new RuntimeException("The cafInvDetails Details Insertion Failed");
							}
						} else {
							throw new RuntimeException("The cafInvDetails Details Insertion Failed");
						}
					}
					
					phoneNo = "";
				}
			} else {
				throw new RuntimeException("The provisioning requestes creation failed.");
			}
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::createPayment() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			paymentId = null;
			oLPayment = null;
			tenantWallet = null;
		}
		return phoneNo;
	}
	
	@Override
	@Transactional
	public String processWOPayments(PaymentVO paymentVO) {
		String result = "";
		Float PaidAmount = 0f;
		try {
			PaidAmount = paymentVO.getPaidAmount() != null ? paymentVO.getPaidAmount() : 0f;
			String prodCafNo = paymentVO.getCustomerCafVO().getChangePkgFlag() != null && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) ? paymentVO.getCustomerCafVO().getProdCafNo() : paymentVO.getCustomerCafVO().getCafNo().toString();
			Long pmntCustId = paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? paymentVO.getCustomerCafVO().getCustId().longValue() : Long.parseLong(paymentVO.getCustomerCafVO().getCustCode());
			result = storedProcedureDAO.processWOPayments(Integer.parseInt(paymentVO.getCustomerCafVO().getDistrict()), pmntCustId, Long.parseLong(paymentVO.getPmntId()), PaidAmount );
			if((result.equalsIgnoreCase("0") && paymentVO.getCustomerCafVO().getChangePkgFlag() == null) || (result.equalsIgnoreCase("0") && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) ) {
				provisioningBusinessService.processProvisioningRequests(prodCafNo, 1);
				logger.info("Provisioning Request Jsons Created Successfully");
			}
		} catch(Exception e) {
			logger.error("PaymentServiceImpl::processWOPayments() " + e);
			e.printStackTrace();
		} finally {
			PaidAmount = null;
		}
		return result;
	}
	
	@Override
	@Transactional
	public void saveMonthlyPayment(PaymentVO paymentVO, String loginId) throws Exception {
		Long paymentId;
		OLPayment oLPayment = new OLPayment();
		TenantWalletVO tenantWallet;
		String from="";
		try {
			CustomerCafVO customerCafVO = paymentVO.getCustomerCafVO();
			customerCafVO.setLmoCode(paymentVO.getTenantCode());
			customerCafVO.setIpAddress(paymentVO.getIpAddress());
			from=paymentVO.getDdDate();
			if(from!=null && from.length()<=7) {
				from=paymentVO.getDdDate()+"-31";
				paymentVO.setDdDate(null);
				paymentVO.setCustomerCafVO(customerCafVO);
				paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
				oLPayment = new OLPayment(paymentVO);
				oLPayment.setPmntId(paymentId.longValue());
				oLPayment.setAcctCafNo(ComsEnumCodes.CAF_PENDING_FOR_PACKAGE_STATUS.getStatus());
				oLPayment.setCustId(paymentVO.getCustId());
				oLPayment.setPmntMode(paymentVO.getPaymentMode());
				oLPayment.setPmntRefNo(paymentVO.getTransactionNo());
				if (paymentVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					oLPayment.setDistrictuid(Integer.parseInt(paymentVO.getDistrict()));
				} else {
					oLPayment.setDistrictuid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
				}
				oLPayment.setStatus(ComsEnumCodes.MONTHLY_PAYMENT_STAUTS.getStatus());
				oLPayment.setCreatedBy(loginId);
				oLPayment.setCreatedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
				if(paymentVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
					tenantWallet = tenantWalletService.getTenantWalletDetails(paymentVO.getCustomerCafVO().getLmoCode());
					// if (tenantWallet.getWalletAmount() >= paymentVO.getPaidAmount()) 
					if(tenantWallet.getCreditAmount()+tenantWallet.getWalletAmount()>0)
					{
						paymentDao.savePayment(oLPayment);
						logger.info("The Payment Details Created Successfully");

						tenantWalletService.updateTenantWallet(paymentVO, loginId);
						paymentVO.setDdDate(from);
						tenantWalletService.updateCustInv(paymentVO);
						logger.info("The LMO Wallet Balence Updated Successfully");
					} else {
						throw new RuntimeException("In Sufficient Wallet Balance.");
					}
				} else {
					paymentDao.savePayment(oLPayment);
					logger.info("The Payment Details Created Successfully");
				}
			}else {
				paymentVO.setCustomerCafVO(customerCafVO);
				paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
				oLPayment = new OLPayment(paymentVO);
				oLPayment.setPmntId(paymentId.longValue());
				oLPayment.setAcctCafNo(ComsEnumCodes.CAF_PENDING_FOR_PACKAGE_STATUS.getStatus());
				oLPayment.setCustId(paymentVO.getCustId());
				oLPayment.setPmntRefNo(paymentVO.getTransactionNo());
				if (paymentVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					oLPayment.setDistrictuid(Integer.parseInt(paymentVO.getDistrict()));
				} else {
					oLPayment.setDistrictuid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
				}
				oLPayment.setStatus(ComsEnumCodes.MONTHLY_PAYMENT_STAUTS.getStatus());
				oLPayment.setCreatedBy(loginId);
				oLPayment.setCreatedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
				if(paymentVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
					tenantWallet = tenantWalletService.getTenantWalletDetails(paymentVO.getCustomerCafVO().getLmoCode());
					if (tenantWallet.getActualUserAmount() >= paymentVO.getPaidAmount()) {
						paymentDao.savePayment(oLPayment);
						logger.info("The Payment Details Created Successfully");

						tenantWalletService.updateTenantWallet(paymentVO, loginId);
						logger.info("The LMO Wallet Balence Updated Successfully");
					} else {
						throw new RuntimeException("In Sufficient Wallet Balance.");
					}
				} else {
					paymentDao.savePayment(oLPayment);
					logger.info("The Payment Details Created Successfully");
				}
			}
			
			
			
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::saveMonthlyPayment() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			paymentId = null;
			oLPayment = null;
			tenantWallet = null;
		}
	}
	
	@Override
	@Transactional
	public void saveBulkMonthlyPayment(PaymentVO paymentVO, String loginId) throws Exception {
		Long paymentId;
		OLPayment oLPayment = new OLPayment();
		TenantWalletVO tenantWallet;
		try {
			CustomerCafVO customerCafVO = paymentVO.getCustomerCafVO();
			customerCafVO.setLmoCode(paymentVO.getTenantCode());
			customerCafVO.setIpAddress(paymentVO.getIpAddress());
			paymentVO.setCustomerCafVO(customerCafVO);
			String from=paymentVO.getDdDate().toString();
			if(from.length()<=7) paymentVO.setDdDate(from+"-31");
			paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
			oLPayment = new OLPayment(paymentVO);
			oLPayment.setPmntId(paymentId.longValue());
			oLPayment.setAcctCafNo(ComsEnumCodes.CAF_PENDING_FOR_PACKAGE_STATUS.getStatus());
			oLPayment.setCustId(paymentVO.getCustId());
			oLPayment.setPmntRefNo(paymentVO.getTransactionNo());
			if (paymentVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
				oLPayment.setDistrictuid(Integer.parseInt(paymentVO.getDistrict()));
			} else {
				oLPayment.setDistrictuid(ComsEnumCodes.PENDING_FOR_PACKAGE_STATUS.getStatus());
			}
			oLPayment.setStatus(ComsEnumCodes.MONTHLY_PAYMENT_STAUTS.getStatus());
			oLPayment.setCreatedBy(loginId);
			oLPayment.setCreatedIPAddr(paymentVO.getIpAddress() != null ? paymentVO.getIpAddress() : httpServletRequest.getRemoteAddr());
			if(paymentVO.getTenantType().equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode())) {
				tenantWallet = tenantWalletService.getTenantWalletDetails(paymentVO.getCustomerCafVO().getLmoCode());
				if (tenantWallet.getActualUserAmount() >= paymentVO.getPaidAmount()) {
					paymentDao.savePayment(oLPayment);
					logger.info("The Payment Details Created Successfully");

					tenantWalletService.updateTenantWallet(paymentVO, loginId);
					tenantWalletService.updateCustInv(paymentVO);
					logger.info("The LMO Wallet Balence Updated Successfully");
				} else {
					throw new RuntimeException("In Sufficient Wallet Balance.");
				}
			} else {
				paymentDao.savePayment(oLPayment);
				logger.info("The Payment Details Created Successfully");
			}
			
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::saveMonthlyPayment() " + e);
			e.printStackTrace();
			throw e;
		} finally {
			paymentId = null;
			oLPayment = null;
			tenantWallet = null;
		}
	}
	

	@Override
	public CafDetailsVO getRecentPayment(Long cafNo) {
		CafDetailsVO cafDetailsVO = new CafDetailsVO();
		List<RecentPaymentVO> recentPaymentList = new ArrayList<RecentPaymentVO>();
		List<PaymentDetailsVO> paymentDetailsVOList = new ArrayList<PaymentDetailsVO>();
		List<CafStbsVO> stbInfoLists = new ArrayList<CafStbsVO>();
		List<TicketDetailsVO> ticketList = new ArrayList<TicketDetailsVO>();
		List<CustomerDTO> custList = new ArrayList<CustomerDTO>();
		List<Object[]> paymentList;
		List<Object[]> stbInfoList;
		Object[] cafAndCpeDetails;
		List<TelephoneServcVO> telephoneser;
		List<TelephoneNoVO> telephoneNo;
		List<BillingInfoVO> billInfo;
		List<Object[]> cafList;
		List<Object[]> ticketsList;
		List<Object[]> customersList;
		try {
			// get Payment Information
			paymentList = paymentDao.getRecentPayment(cafNo);
			if(!paymentList.isEmpty()) {
				for (Object[] payment : paymentList) {
					RecentPaymentVO recentPaymentVO = new RecentPaymentVO();
					recentPaymentVO.setPmntAmt(payment[0] != null ? payment[0].toString() : "0");
					recentPaymentVO.setPmntMode(payment[1] != null ? payment[1].toString() : "--");
					recentPaymentVO.setPmntRefNo(payment[2] != null ? payment[2].toString() : "--");
					recentPaymentVO.setPmntDate(payment[3].toString());
					recentPaymentList.add(recentPaymentVO);
				}
			}
			// caf and cpe details
			cafAndCpeDetails = cafService.getAllParticularCPE(cafNo);

			// telephoneService
			telephoneser = cafService.getTelephoneServc(cafNo);

			// TelePhoneNo
			telephoneNo = cafService.getTelephoneNo(cafNo);

			// Billing Information
			billInfo = cafService.getBillingInfo(cafNo);

			// get Packages Information
			cafList = cafDao.getPaymentDetails(cafNo);
			for (Object[] caf : cafList) {
				PaymentDetailsVO paymentDetailsVO = new PaymentDetailsVO();
				paymentDetailsVO.setProdname(caf[0].toString());
				paymentDetailsVO.setSrvcname(caf[1].toString());
				paymentDetailsVO.setCreatedon(caf[5] != null ? caf[5].toString() : "NA");
				paymentDetailsVO.setProdcharge(caf[2].toString());
				paymentDetailsVO.setProdtax(caf[3].toString());
				//paymentDetailsVO.setStbcafno(caf[9].toString().equalsIgnoreCase("0") ? "NA" : caf[9].toString());
				//paymentDetailsVO.setCoresrvccode(caf[14] !=null ? caf[14].toString() : "NA");
				if (caf[4].toString().equalsIgnoreCase("B"))
					paymentDetailsVO.setProdtype("Base");
				else if (caf[4].toString().equalsIgnoreCase("O"))
					paymentDetailsVO.setProdtype("One Time");
				else if (caf[4].toString().equalsIgnoreCase("A"))
					paymentDetailsVO.setProdtype("Add On");
				paymentDetailsVOList.add(paymentDetailsVO);
				
			}
			
			//STB INFO
			stbInfoList = paymentDao.getStbInfo(cafNo);
			if(!stbInfoList.isEmpty()) {
				for (Object[] stb : stbInfoList) {
					CafStbsVO cafStbsVO = new CafStbsVO();
					cafStbsVO.setStbcafno(stb[0] != null ? stb[0].toString() : "0");
					cafStbsVO.setStbprofileid(stb[1] != null ? stb[1].toString() : "NA");
					cafStbsVO.setStbleaseyn(stb[2] != null ? stb[2].toString() : "NA");
					cafStbsVO.setStbboxcharge(stb[3] != null ? stb[3].toString() : "0.00");
					cafStbsVO.setStbinstcharge(stb[4] != null ? stb[4].toString() : "0.00");
					cafStbsVO.setStbemicount(stb[5] != null ? stb[5].toString() : "0");
					cafStbsVO.setStbslno(stb[6] != null ? stb[6].toString() : "NA");
					cafStbsVO.setStbmacaddr(stb[7] != null ? stb[7].toString() : "NA");
					cafStbsVO.setNwsubscode(stb[8] != null ? stb[8].toString() : "NA");
					stbInfoLists.add(cafStbsVO);
				}
			}
			
			
			ticketsList = cafDao.getTicketInfo(cafNo);
			for (Object[] ticket : ticketsList) {
				TicketDetailsVO ticketDetailsVO = new TicketDetailsVO();
				ticketDetailsVO.setTicket_no(ticket[0] != null ? ticket[0].toString() : "NA");
				ticketDetailsVO.setSubmitted_on(ticket[1] != null ? ticket[1].toString() : "NA");
				ticketDetailsVO.setDescription(ticket[2] != null ? ticket[2].toString() : "NA");
				ticketDetailsVO.setStatusId(ticket[3]!= null ? ticket[3].toString() : "NA");
				ticketDetailsVO.setStatus(ticket[5] != null ? ticket[5].toString() : "NA");
				if (ticketDetailsVO.getStatusId().equalsIgnoreCase("4"))
					ticketDetailsVO.setClosed_on(ticket[4].toString());
				else {
					ticketDetailsVO.setClosed_on("NA");
				}
				ticketList.add(ticketDetailsVO);
			}
			
			//CUSTOMER INFO BY CafNO
			customersList = cafDao.findCustomerInfoByCafNo(cafNo);
			for (Object[] customer : customersList) {
				CustomerDTO customerDTO = new CustomerDTO();
				customerDTO.setFirstname(customer[0] != null ? customer[0].toString() : "");
				customerDTO.setMname(customer[1]!= null ? customer[1].toString() : "");
				customerDTO.setLastname(customer[2] != null ? customer[2].toString() : "");
				customerDTO.setEmailid(customer[3]!= null ? customer[3].toString() : "NA");
				customerDTO.setDob(customer[4]!= null ? customer[4].toString() : "NA");
				customerDTO.setContactno(customer[6]!= null ? customer[6].toString() : "NA");
				customerDTO.setBillFrequency(customer[7]!= null ? customer[7].toString() : "NA");
				customerDTO.setCustType(customer[8]!= null ? customer[8].toString() : "NA");
				customerDTO.setLmoCode(customer[9]!= null ? customer[9].toString() : "NA");
				custList.add(customerDTO);
			}
			
			cafDetailsVO.setNewCafList(paymentDetailsVOList);
			cafDetailsVO.setPaymentList(recentPaymentList);
			cafDetailsVO.setCafDao(cafAndCpeDetails);
			cafDetailsVO.setTelephoneser(telephoneser);
			cafDetailsVO.setTelephoneNo(telephoneNo);
			cafDetailsVO.setBillInfo(billInfo);
			cafDetailsVO.setStbInfoList(stbInfoLists);
			cafDetailsVO.setTicketInfoList(ticketList);
			cafDetailsVO.setCustomerInfoList(custList);
			
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::getRecentPayment() " + e);
			e.printStackTrace();
		} finally {
			recentPaymentList = null;
			paymentDetailsVOList = null;
			paymentList = null;
			cafAndCpeDetails = null;
			telephoneser = null;
			billInfo = null;
			cafList = null;
		}
		return cafDetailsVO;
	}
	
	@Override
	@Transactional
	public String generateChargesForReverseSecDeposit(CustomerCafVO customerCafVO, List<ProductsVO> changePkgList) {
		String status = "Success";
		GenarateChargesVO genarateChargesVO = new GenarateChargesVO();
		List<AddtionalServicesVO> serviceList = new ArrayList<>();
		List<ChargeVO> chargeTypesList = new ArrayList<>();
		try {
			String regionCode = cafService.getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			regionCode = regionCode.isEmpty() ? "GENERAL" : regionCode;
			for (ProductsVO products : changePkgList) {
				serviceList = products.getServicesList();
				for (AddtionalServicesVO services : serviceList) {
					chargeTypesList = services.getChargeList();
					for (ChargeVO charges : chargeTypesList) {
						 if(charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.SecurityDeposit_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
							genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
							genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
							if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
								genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
							} else {
								genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
							}
							genarateChargesVO.setChargeAmt("-"+charges.getChargeAmt());
							genarateChargesVO.setPaidAmount("0");
							genarateChargesVO.setChargeCode(charges.getChargeCode());
							genarateChargesVO.setFeatureCode(charges.getFeatureCode());
							genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
							genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
							genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
							genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
							genarateChargesVO.setEntTaxamt("0");
							genarateChargesVO.setKisanTaxamt("0");
							genarateChargesVO.setProdCafno(customerCafVO.getProdCafNo());
							genarateChargesVO.setStbCafno("0");
							genarateChargesVO.setProdCode(products.getProdcode());
							genarateChargesVO.setSrvcCode(services.getServiceCode());
							genarateChargesVO.setSrvcTaxamt("0");
							genarateChargesVO.setSwatchTaxamt("0");
							genarateChargesVO.setTenantCode(products.getTenantcode());
							genarateChargesVO.setChargeDescription(null);
							if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
								genarateChargesVO.setIgnoreBalenceFlag("Y");
							} else {
								genarateChargesVO.setIgnoreBalenceFlag("N");
							}
							genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
							genarateChargesVO.setAdjrefId(null);
							status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::generateChargesAndWorkorderProcessPayments() " + e);
			e.printStackTrace();
		} finally {
			genarateChargesVO = null;
			serviceList = null;
			chargeTypesList = null;
		}
		return status;
	}
	
	@Override
	@Transactional
	public String generateChargesAndAddPackagePayments(CustomerCafVO customerCafVO, Long addPkgCafNo, Float paidAmount, Long stbCafNo) {
		String status = "Success";
		GenarateChargesVO genarateChargesVO = new GenarateChargesVO();
		List<AddtionalServicesVO> serviceList = new ArrayList<>();
		List<ProductsVO> selectedProductsList = new ArrayList<>();
		List<ChargeVO> chargeTypesList = new ArrayList<>();
		TaxesVO taxesVO = new TaxesVO();
		try {
			String regionCode = cafService.getRegionCodeByPinCode(customerCafVO.getDistrict(), customerCafVO.getMandal(), customerCafVO.getCity());
			regionCode = regionCode.isEmpty() ? "GENERAL" : regionCode;
			selectedProductsList = customerCafVO.getProducts();
			for (ProductsVO products : selectedProductsList) {
				serviceList = products.getServicesList();
				for (AddtionalServicesVO services : serviceList) {
					if((products.getProdType().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode()) && !services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode())) || (products.getProdType().equalsIgnoreCase(ComsEnumCodes.ADDON_PACKAGE_TYPE_CODE.getCode())) || (products.getProdType().equalsIgnoreCase(ComsEnumCodes.ONE_PACKAGE_TYPE_CODE.getCode()))) {
						chargeTypesList = services.getChargeList();
						for (ChargeVO charges : chargeTypesList) {
							if (charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.Activation_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
								taxesVO = this.getTaxAmounts(services.getServiceCode(), charges.getChargeCode(), regionCode, ComsEnumCodes.Service_TaxLevelFlag.getCode(), charges.getChargeAmt());
								genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
								genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
								if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
								} else {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
								}
								genarateChargesVO.setChargeAmt(charges.getChargeAmt());
								genarateChargesVO.setPaidAmount(paidAmount != null ? String.valueOf(paidAmount) : "0");
								genarateChargesVO.setChargeCode(charges.getChargeCode());
								genarateChargesVO.setFeatureCode(charges.getFeatureCode());
								genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
								genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
								genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
								genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
								genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
								genarateChargesVO.setProdCafno(addPkgCafNo.toString());
								if(services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode())) {
									genarateChargesVO.setStbCafno(stbCafNo.toString());
								} else {
									genarateChargesVO.setStbCafno("0");
								}
								genarateChargesVO.setProdCode(products.getProdcode());
								genarateChargesVO.setSrvcCode(services.getServiceCode());
								genarateChargesVO.setTenantCode(products.getTenantcode());
								genarateChargesVO.setChargeDescription(null);
								if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									genarateChargesVO.setIgnoreBalenceFlag("Y");
								} else {
									genarateChargesVO.setIgnoreBalenceFlag("N");
								}
								genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
								genarateChargesVO.setAdjrefId(null);
								status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
							} else if(charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.SecurityDeposit_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
								genarateChargesVO.setDistrict(customerCafVO.getCustDistrictId());
								genarateChargesVO.setAcctCafno(customerCafVO.getCafNo().toString());
								if (customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustId().toString());
								} else {
									genarateChargesVO.setPmntCustId(customerCafVO.getCustCode());
								}
								genarateChargesVO.setChargeAmt(charges.getChargeAmt());
								genarateChargesVO.setPaidAmount(paidAmount != null ? String.valueOf(paidAmount) : "0");
								genarateChargesVO.setChargeCode(charges.getChargeCode());
								genarateChargesVO.setFeatureCode(charges.getFeatureCode());
								genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
								genarateChargesVO.setEntTaxamt("0");
								genarateChargesVO.setKisanTaxamt("0");
								genarateChargesVO.setProdCafno(addPkgCafNo.toString());
								genarateChargesVO.setStbCafno("0");
								genarateChargesVO.setProdCode(products.getProdcode());
								genarateChargesVO.setSrvcCode(services.getServiceCode());
								genarateChargesVO.setSrvcTaxamt("0");
								genarateChargesVO.setSwatchTaxamt("0");
								genarateChargesVO.setTenantCode(products.getTenantcode());
								genarateChargesVO.setChargeDescription(null);
								genarateChargesVO.setAdjrefId(null);
								if(customerCafVO.getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && customerCafVO.getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									genarateChargesVO.setIgnoreBalenceFlag("Y");
								} else {
									genarateChargesVO.setIgnoreBalenceFlag("N");
								}
								genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
								status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::generateChargesAndWorkorderProcessPayments() " + e);
			e.printStackTrace();
		} finally {
			genarateChargesVO = null;
			serviceList = null;
			selectedProductsList = null;
			chargeTypesList = null;
			taxesVO = null;
		}
		return status;
	}
	
	@Override
	@Transactional
	public String generateChargesAndWorkorderProcessPayments(PaymentVO paymentVO) {
		String status = "Success";
		GenarateChargesVO genarateChargesVO = null;
		List<AddtionalServicesVO> serviceList = new ArrayList<>();
		List<ProductsVO> selectedProductsList = new ArrayList<>();
		List<ChargeVO> chargeTypesList = new ArrayList<>();
		TaxesVO taxesVO = new TaxesVO();
		String regionCode = "GENERAL";
		String onuPrice = "0";
		String cpeInstallationPrice = "0";
		String cpeExtCablePrice = "0";
		try {
			regionCode = cafService.getRegionCodeByPinCode(paymentVO.getCustomerCafVO().getDistrict(), paymentVO.getCustomerCafVO().getMandal(), paymentVO.getCustomerCafVO().getCity());
			selectedProductsList = paymentVO.getCustomerCafVO().getProducts();
			for (ProductsVO products : selectedProductsList) {
				serviceList = products.getServicesList();
				for (AddtionalServicesVO services : serviceList) {
					if((products.getProdType().equalsIgnoreCase(ComsEnumCodes.BASE_PACKAGE_TYPE_CODE.getCode())) || (!services.getCoreServiceCode().equalsIgnoreCase(ComsEnumCodes.CORE_SERVICE_IPTV.getCode()))) {
						chargeTypesList = services.getChargeList();
						for (ChargeVO charges : chargeTypesList) {
							if (charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.Activation_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
								genarateChargesVO = new GenarateChargesVO();
								taxesVO = this.getTaxAmounts(services.getServiceCode(), charges.getChargeCode(), regionCode, ComsEnumCodes.Service_TaxLevelFlag.getCode(), charges.getChargeAmt());
								genarateChargesVO.setDistrict(paymentVO.getCustomerCafVO().getCustDistrictId());
								genarateChargesVO.setAcctCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
								if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
									genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustId().toString());
								} else {
									genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustCode());
								}
								genarateChargesVO.setChargeAmt(charges.getChargeAmt());
								genarateChargesVO.setPaidAmount(paymentVO.getPaidAmount() != null ? String.valueOf(paymentVO.getPaidAmount()) : "0");
								genarateChargesVO.setChargeCode(charges.getChargeCode());
								genarateChargesVO.setFeatureCode(charges.getFeatureCode());
								genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
								genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
								genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
								genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
								genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
								genarateChargesVO.setProdCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
								genarateChargesVO.setStbCafno("0");
								genarateChargesVO.setProdCode(products.getProdcode());
								genarateChargesVO.setSrvcCode(services.getServiceCode());
								genarateChargesVO.setTenantCode(products.getTenantcode());
								genarateChargesVO.setChargeDescription(null);
								if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									genarateChargesVO.setIgnoreBalenceFlag("Y");
								} else {
									genarateChargesVO.setIgnoreBalenceFlag("N");
								}
								genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
								genarateChargesVO.setAdjrefId(null);
								status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
							} else if(charges.getChargeTypeFlag().equalsIgnoreCase(ComsEnumCodes.SecurityDeposit_ChargeTypeFlag.getCode()) && new BigDecimal(charges.getChargeAmt()).compareTo(BigDecimal.ZERO) > 0 ) {
								genarateChargesVO = new GenarateChargesVO();
								genarateChargesVO.setDistrict(paymentVO.getCustomerCafVO().getCustDistrictId());
								genarateChargesVO.setAcctCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
								if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
									genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustId().toString());
								} else {
									genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustCode());
								}
								genarateChargesVO.setChargeAmt(charges.getChargeAmt());
								genarateChargesVO.setPaidAmount(paymentVO.getPaidAmount() != null ? String.valueOf(paymentVO.getPaidAmount()) : "0");
								genarateChargesVO.setChargeCode(charges.getChargeCode());
								genarateChargesVO.setFeatureCode(charges.getFeatureCode());
								genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
								genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
								genarateChargesVO.setEntTaxamt("0");
								genarateChargesVO.setKisanTaxamt("0");
								genarateChargesVO.setProdCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
								genarateChargesVO.setStbCafno("0");
								genarateChargesVO.setProdCode(products.getProdcode());
								genarateChargesVO.setSrvcCode(services.getServiceCode());
								genarateChargesVO.setSrvcTaxamt("0");
								genarateChargesVO.setSwatchTaxamt("0");
								genarateChargesVO.setTenantCode(products.getTenantcode());
								genarateChargesVO.setChargeDescription(null);
								if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
									genarateChargesVO.setIgnoreBalenceFlag("Y");
								} else {
									genarateChargesVO.setIgnoreBalenceFlag("N");
								}
								genarateChargesVO.setRsAgrUid(products.getAgruniqueid().toString());
								status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
							}
						}
					}
				}
			}
			if (paymentVO.getCustomerCafVO().getInstCharge() != null && paymentVO.getCustomerCafVO().getInstCharge() > 0) {
				genarateChargesVO = new GenarateChargesVO();
				cpeInstallationPrice = paymentVO.getCustomerCafVO().getInstCharge().toString();
				taxesVO = this.getTaxAmounts("", ComsEnumCodes.CPE_INSTALLATION_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), cpeInstallationPrice);
				genarateChargesVO.setDistrict(paymentVO.getCustomerCafVO().getCustDistrictId());
				genarateChargesVO.setAcctCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
				if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustId().toString());
				} else {
					genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustCode());
				}
				genarateChargesVO.setChargeAmt(paymentVO.getCustomerCafVO().getInstCharge().toString());
				genarateChargesVO.setPaidAmount(paymentVO.getPaidAmount() != null ? String.valueOf(paymentVO.getPaidAmount()) : "0");
				genarateChargesVO.setChargeCode(ComsEnumCodes.CPE_INSTALLATION_CODE.getCode());
				genarateChargesVO.setFeatureCode("0");
				genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
				genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
				genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
				genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
				genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
				genarateChargesVO.setProdCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
				genarateChargesVO.setStbCafno("0");
				genarateChargesVO.setProdCode("");
				genarateChargesVO.setSrvcCode("");
				genarateChargesVO.setTenantCode("");
				genarateChargesVO.setChargeDescription(null);
				genarateChargesVO.setAdjrefId(null);
				if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
					genarateChargesVO.setIgnoreBalenceFlag("Y");
				} else {
					genarateChargesVO.setIgnoreBalenceFlag("N");
				}
				genarateChargesVO.setRsAgrUid("0");
				status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
			}
			if (!paymentVO.getCustomerCafVO().getCableCharge().isEmpty() && new BigDecimal(paymentVO.getCustomerCafVO().getCableCharge()).compareTo(BigDecimal.ZERO) > 0) {
				genarateChargesVO = new GenarateChargesVO();
				cpeExtCablePrice = paymentVO.getCustomerCafVO().getCableCharge();
				taxesVO = this.getTaxAmounts("", ComsEnumCodes.CPE_EXTRACABLE_CHARGE_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), cpeExtCablePrice);
				genarateChargesVO.setDistrict(paymentVO.getCustomerCafVO().getCustDistrictId());
				genarateChargesVO.setAcctCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
				if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
					genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustId().toString());
				} else {
					genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustCode());
				}
				genarateChargesVO.setChargeAmt(paymentVO.getCustomerCafVO().getCableCharge());
				genarateChargesVO.setPaidAmount(paymentVO.getPaidAmount() != null ? String.valueOf(paymentVO.getPaidAmount()) : "0");
				genarateChargesVO.setChargeCode(ComsEnumCodes.CPE_EXTRACABLE_CHARGE_CODE.getCode());
				genarateChargesVO.setFeatureCode("0");
				genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
				genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
				genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
				genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
				genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
				genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
				genarateChargesVO.setProdCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
				genarateChargesVO.setStbCafno("0");
				genarateChargesVO.setProdCode("");
				genarateChargesVO.setSrvcCode("");
				genarateChargesVO.setTenantCode("");
				genarateChargesVO.setChargeDescription(null);
				genarateChargesVO.setAdjrefId(null);
				if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
					genarateChargesVO.setIgnoreBalenceFlag("Y");
				} else {
					genarateChargesVO.setIgnoreBalenceFlag("N");
				}
				genarateChargesVO.setRsAgrUid("0");
				status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
			}
			if (paymentVO.getCustomerCafVO().getInstallmentCount() != null && !paymentVO.getCustomerCafVO().getInstallmentCount().isEmpty() && new BigDecimal(paymentVO.getCustomerCafVO().getCpePrice()).compareTo(BigDecimal.ZERO) > 0) {
				genarateChargesVO = new GenarateChargesVO();
				onuPrice = paymentVO.getCustomerCafVO().getCpePrice();
				if (paymentVO.getCustomerCafVO().getInstallmentCount().equalsIgnoreCase("0")) {
					taxesVO = this.getTaxAmounts("", ComsEnumCodes.ONU_COST_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), onuPrice);
					genarateChargesVO.setChargeCode(ComsEnumCodes.ONU_COST_CODE.getCode());
				} else {
					taxesVO = this.getTaxAmounts("", ComsEnumCodes.ONU_UPFRONT_CODE.getCode(), regionCode, ComsEnumCodes.Caf_TaxLevelFlag.getCode(), onuPrice);
					genarateChargesVO.setChargeCode(ComsEnumCodes.ONU_UPFRONT_CODE.getCode());
				}
					genarateChargesVO.setDistrict(paymentVO.getCustomerCafVO().getCustDistrictId());
					genarateChargesVO.setAcctCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
					if (paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode())) {
						genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustId().toString());
					} else {
						genarateChargesVO.setPmntCustId(paymentVO.getCustomerCafVO().getCustCode());
					}
					genarateChargesVO.setChargeAmt(paymentVO.getCustomerCafVO().getCpePrice());
					genarateChargesVO.setPaidAmount(paymentVO.getPaidAmount() != null ? String.valueOf(paymentVO.getPaidAmount()) : "0");
					
					genarateChargesVO.setFeatureCode("0");
					genarateChargesVO.setChargedDate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
					genarateChargesVO.setChargeFdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
					genarateChargesVO.setChargeTdate(DateUtill.cafInvDate(Calendar.getInstance().getTime()));
					genarateChargesVO.setChargeType(ComsEnumCodes.CafInv_ChargeType.getCode());
					genarateChargesVO.setEntTaxamt(taxesVO.getEntTax());
					genarateChargesVO.setKisanTaxamt(taxesVO.getKissanTax());
					genarateChargesVO.setSrvcTaxamt(taxesVO.getSrvcTax());
					genarateChargesVO.setSwatchTaxamt(taxesVO.getSwatchTax());
					genarateChargesVO.setProdCafno(paymentVO.getCustomerCafVO().getCafNo().toString());
					genarateChargesVO.setStbCafno("0");
					genarateChargesVO.setProdCode("");
					genarateChargesVO.setSrvcCode("");
					genarateChargesVO.setTenantCode("");
					genarateChargesVO.setChargeDescription(null);
					genarateChargesVO.setAdjrefId(null);
					if(paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.ENTCUST_TYPE_CODE.getCode()) && paymentVO.getCustomerCafVO().getEntCustType().equalsIgnoreCase(ComsEnumCodes.GOVT.getCode())) {
						genarateChargesVO.setIgnoreBalenceFlag("Y");
					} else {
						genarateChargesVO.setIgnoreBalenceFlag("N");
					}
					genarateChargesVO.setRsAgrUid("0");
					status = storedProcedureDAO.generateChargeProcedure(genarateChargesVO);
			}
		} catch (Exception e) {
			logger.error("PaymentServiceImpl::generateChargesAndWorkorderProcessPayments() " + e);
			e.printStackTrace();
		} finally {
			genarateChargesVO = null;
			serviceList = null;
			selectedProductsList = null;
			chargeTypesList = null;
			taxesVO = null;
			regionCode = null;
			onuPrice = null;
			cpeInstallationPrice = null;
			cpeExtCablePrice = null;
		}
		return status;
	}

	@Override
	public TaxesVO getTaxAmounts(String srvcCode, String chrgCode, String region, String taxLevelFlag, String chrgAmount) {
		TaxesVO taxesVO = new TaxesVO();
		List<Object[]> taxsList = new ArrayList<>();
		BigDecimal TaxAmt = new BigDecimal("0");
		try {
			taxesVO.setEntTax("0");
			taxesVO.setKissanTax("0");
			taxesVO.setSrvcTax("0");
			taxesVO.setSwatchTax("0");
			taxsList = paymentDao.getTaxAmounts(srvcCode, chrgCode, region, taxLevelFlag);
			for(Object[] object : taxsList) {
				if(ComsEnumCodes.Srvc_TaxCode.getCode().equalsIgnoreCase(object[2].toString())) {
					TaxAmt = (new BigDecimal(chrgAmount).multiply(new BigDecimal(object[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(object[1].toString()));
					taxesVO.setSrvcTax(TaxAmt.toString());
				} else if(ComsEnumCodes.Swatch_TaxCode.getCode().equalsIgnoreCase(object[2].toString())) {
					TaxAmt = (new BigDecimal(chrgAmount).multiply(new BigDecimal(object[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(object[1].toString()));
					taxesVO.setSwatchTax(TaxAmt.toString());
				} else if(ComsEnumCodes.Kissan_TaxCode.getCode().equalsIgnoreCase(object[2].toString())) {
					TaxAmt = (new BigDecimal(chrgAmount).multiply(new BigDecimal(object[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(object[1].toString()));
					taxesVO.setKissanTax(TaxAmt.toString());
				} else if(ComsEnumCodes.Ent_TaxCode.getCode().equalsIgnoreCase(object[2].toString())) {
					TaxAmt = (new BigDecimal(chrgAmount).multiply(new BigDecimal(object[0].toString()))).divide(new BigDecimal("100")).add(new BigDecimal(object[1].toString()));
					taxesVO.setEntTax(TaxAmt.toString());
				}
			}
			logger.info("Laxman::  SrvcCode == "+srvcCode+"  TaxAmount == "+taxesVO.getTotalTax());
		} catch(Exception e) {
			logger.error("PaymentServiceImpl::getTaxAmounts() " + e);
			e.printStackTrace();
		} finally {
			taxsList = null;
			TaxAmt = null;
		}
		return taxesVO;
	}

	@Override
	public Map<String, String> getCafUsage(String yyyy, String mm, String cafNo) {
		return cafDao.getCafUsage(yyyy,mm,cafNo);
	}

	@Override
	public List<PaymentDetailsVO> getTTPayment(Long cafNo) {
		List<PaymentDetailsVO> paymentDetailsVOList = new ArrayList<PaymentDetailsVO>();
		try {
			List<Object[]> cafList = cafDao.getPaymentDetails(cafNo);
			for (Object[] caf : cafList) {
				PaymentDetailsVO paymentDetailsVO = new PaymentDetailsVO();
				paymentDetailsVO.setProdname(caf[0].toString());
				paymentDetailsVO.setSrvcname(caf[1].toString());
				paymentDetailsVO.setCreatedon(caf[5] != null ? caf[5].toString() : "NA");
				paymentDetailsVO.setProdcharge(caf[2].toString());
				paymentDetailsVO.setProdtax(caf[3].toString());
				paymentDetailsVO.setStbcafno(caf[13].toString().equalsIgnoreCase("0") ? "NA" : caf[13].toString());
				if (caf[4].toString().equalsIgnoreCase("B"))
					paymentDetailsVO.setProdtype("Base");
				else if (caf[4].toString().equalsIgnoreCase("O"))
					paymentDetailsVO.setProdtype("One Time");
				else if (caf[4].toString().equalsIgnoreCase("A"))
					paymentDetailsVO.setProdtype("Add On");
				paymentDetailsVOList.add(paymentDetailsVO);

			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("PaymentServiceImpl::getTTPayment() " + e);
		} finally {
			
		}
		return paymentDetailsVOList;
	}
	
	
	@Override
	@Transactional
	public String processCSSWOPayments(PaymentVO paymentVO) {
		String result = "";
		Float PaidAmount = 0f;
		try {
			PaidAmount = paymentVO.getPaidAmount() != null ? paymentVO.getPaidAmount() : 0f;
			String prodCafNo = paymentVO.getCustomerCafVO().getChangePkgFlag() != null && paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode()) ? paymentVO.getCustomerCafVO().getProdCafNo() : paymentVO.getCustomerCafVO().getCafNo().toString();
			Long pmntCustId = paymentVO.getCustomerCafVO().getCustType().equalsIgnoreCase(ComsEnumCodes.CUST_TYPE_CODE.getCode()) ? paymentVO.getCustomerCafVO().getCustId().longValue() : Long.parseLong(paymentVO.getCustomerCafVO().getCustCode());
			if(( paymentVO.getCustomerCafVO().getChangePkgFlag() == null) || (paymentVO.getCustomerCafVO().getChangePkgFlag().equalsIgnoreCase(ComsEnumCodes.Add_Pkg_Flag.getCode())) ) 
			{
				provisioningBusinessService.processProvisioningRequests(prodCafNo, 1);
			}
			result="0";
			logger.info("Provisioning Request Jsons Created Successfully");
		} catch(Exception e) {
			logger.error("PaymentServiceImpl::processWOPayments() " + e);
			e.printStackTrace();
		} finally {
			PaidAmount = null;
		}
		return result;
	}
	//added by @chaitanya_XYZ
	@Override
	public int getCardDetails(String popslno) {
		// TODO Auto-generated method stub
		int card=paymentDao.getCard(popslno);
		return card;
	}
	
	public void updateCafCard(long cafno,int cardnum){
		paymentDao.updateCard(cafno, cardnum);
	}

	@Override
	public int getCardDetailsByCaf(String cafno) {
		int cardnum=paymentDao.getCardByCaf(cafno);
		return cardnum;
	}
	


	@Override
	public PaymentVO getCafDetails(Long cafno,String date) {
		// TODO Auto-generated method stub
		PaymentVO paymentvo=new PaymentVO();
		try {
			paymentvo=paymentDao.getCafDetails(cafno,date);
		}catch(Exception e) {
			
		}
		return paymentvo;
	}
		
	public boolean checkOltstatus(String popId, String oltSerialNumber) {
		String ipAddress=null;
		boolean oltDownStatus=true;
		ipAddress=paymentDao.getIpaddressBySerialNumberAndPopId(popId, oltSerialNumber);
		if (ipAddress !=null){
			
			oltDownStatus= paymentDao.getStatus(ipAddress);

		}
		
		return oltDownStatus;
	}

	@Override
	public Boolean checkOltstatus(String oltSrlNo) {
		// TODO Auto-generated method stub
		return paymentDao.getStatus(oltSrlNo);

	}
	
	
}
