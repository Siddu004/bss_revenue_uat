package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CafCountDetailsdistrictwiseDTO implements Serializable{
	
	private static final long serialVersionUID = 1375257101389840942L;
	@Id
	@Column(name="District_Name")
	private String district;
	@Column(name="Districtid")
	private String districtId;

	@Id
	@Column(name="Mandal_Name")
	private String mandal;
	@Column(name="Mandalid")
	private String mandalId;
	@Id
	@Column(name="Village_Name")
	private String village;
	@Column(name="Villageid")
	private String villageId;
	@Id
	@Column(name="regionname")
	private String regionname;
	@Id
	@Column(name="cust_type")
	private String cust_type;
	@Id
	@Column(name="Sub_type")
	private String Sub_type;
	@Column(name="No_Connections")
	private String No_Connections;
	
	
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getMandal() {
		return mandal;
	}
	public void setMandal(String mandal) {
		this.mandal = mandal;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getRegionname() {
		return regionname;
	}
	public void setRegionname(String regionname) {
		this.regionname = regionname;
	}
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
	public String getSub_type() {
		return Sub_type;
	}
	public void setSub_type(String sub_type) {
		Sub_type = sub_type;
	}
	public String getNo_Connections() {
		return No_Connections;
	}
	public void setNo_Connections(String no_Connections) {
		No_Connections = no_Connections;
	}
	
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public String getMandalId() {
		return mandalId;
	}
	public void setMandalId(String mandalId) {
		this.mandalId = mandalId;
	}
	public String getVillageId() {
		return villageId;
	}
	public void setVillageId(String villageId) {
		this.villageId = villageId;
	}
	

}
