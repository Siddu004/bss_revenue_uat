/**
 * 
 */
package com.arbiva.apfgc.comsystem.BO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author kiran
 *
 */
@Entity
public class BalAdjCafinvDtslBO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="invdtlid")
	private String invdtlId;
	
	@Column(name="acctcafno")
	private String acctcafno;
	
	@Column(name="chargecode")
	private String chargeCode;
	
	@Column(name="prodcode")
	private String prodCode;
	
	@Column(name="chargeamt")
	private String chargeAmt;
	
	@Column(name="taxamt")
	private String taxAmt;
	
	@Column(name="custdistuid")
	private String custDistUid;
	
	@Column(name="pmntcustid")
	private String pmnCustId;
	
	@Column(name="paidAmount")
	private String paidAmount;
	
	@Column(name="chargeddate")
	private String chargedDate;
	
	@Column(name="chargefdate")
	private String chargeFDate;
	
	@Column(name="chargetdate")
	private String chargeTDate;
	
	@Column(name="rectype")
	private String rectype;
	
	@Column(name="prodcafno")
	private String prodcafno;
	
	@Column(name="stbcafno")
	private String stbcafno;
	
	@Column(name="srvccode")
	private String srvccode;
	
	@Column(name="tenantcode")
	private String tenantcode;
	
	@Column(name="rsagruid")
	private String rsagruid;
	
	@Column(name="custinvno")
	private String custinvno;
	
	@Column(name = "enttax")
	private String entTax;
	
	@Column(name = "kisantax")
	private String kisanTax;
	
	@Column(name = "srvctax")
	private String srvcTax;
	
	@Column(name = "swatchtax")
	private String swatchTax;
	
	@Column(name = "featurecode")
	private String featureCode;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "cafinvno")
	private String cafinvno;

	public String getCafinvno() {
		return cafinvno;
	}

	public void setCafinvno(String cafinvno) {
		this.cafinvno = cafinvno;
	}

	public String getEntTax() {
		return entTax;
	}

	public void setEntTax(String entTax) {
		this.entTax = entTax;
	}

	public String getKisanTax() {
		return kisanTax;
	}

	public void setKisanTax(String kisanTax) {
		this.kisanTax = kisanTax;
	}

	public String getSrvcTax() {
		return srvcTax;
	}

	public void setSrvcTax(String srvcTax) {
		this.srvcTax = srvcTax;
	}

	public String getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(String swatchTax) {
		this.swatchTax = swatchTax;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustinvno() {
		return custinvno;
	}

	public void setCustinvno(String custinvno) {
		this.custinvno = custinvno;
	}

	public String getCustDistUid() {
		return custDistUid;
	}

	public void setCustDistUid(String custDistUid) {
		this.custDistUid = custDistUid;
	}

	public String getPmnCustId() {
		return pmnCustId;
	}

	public void setPmnCustId(String pmnCustId) {
		this.pmnCustId = pmnCustId;
	}

	public String getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getChargedDate() {
		return chargedDate;
	}

	public void setChargedDate(String chargedDate) {
		this.chargedDate = chargedDate;
	}

	public String getChargeFDate() {
		return chargeFDate;
	}

	public void setChargeFDate(String chargeFDate) {
		this.chargeFDate = chargeFDate;
	}

	public String getChargeTDate() {
		return chargeTDate;
	}

	public void setChargeTDate(String chargeTDate) {
		this.chargeTDate = chargeTDate;
	}

	public String getRectype() {
		return rectype;
	}

	public void setRectype(String rectype) {
		this.rectype = rectype;
	}

	public String getProdcafno() {
		return prodcafno;
	}

	public void setProdcafno(String prodcafno) {
		this.prodcafno = prodcafno;
	}

	public String getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(String stbcafno) {
		this.stbcafno = stbcafno;
	}

	public String getSrvccode() {
		return srvccode;
	}

	public void setSrvccode(String srvccode) {
		this.srvccode = srvccode;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getRsagruid() {
		return rsagruid;
	}

	public void setRsagruid(String rsagruid) {
		this.rsagruid = rsagruid;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getChargeAmt() {
		return chargeAmt;
	}

	public void setChargeAmt(String chargeAmt) {
		this.chargeAmt = chargeAmt;
	}

	public String getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(String taxAmt) {
		this.taxAmt = taxAmt;
	}

	public String getInvdtlId() {
		return invdtlId;
	}

	public void setInvdtlId(String invdtlId) {
		this.invdtlId = invdtlId;
	}

	public String getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}
	
	

}
