package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IPTVCorpusProvReqResDTO {

	private static final long serialVersionUID = 1L;

	public IPTVCorpusProvReqResDTO() {
	
	}
	
	private String requestid;
	
	private int seqnum;
	
	private String response;
	
	private String request;
	
	private long id;

	/**
	 * @return the requestid
	 */
	public String getRequestid() {
		return requestid;
	}

	/**
	 * @param requestid the requestid to set
	 */
	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	/**
	 * @return the seqnum
	 */
	public int getSeqnum() {
		return seqnum;
	}

	/**
	 * @param seqnum the seqnum to set
	 */
	public void setSeqnum(int seqnum) {
		this.seqnum = seqnum;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
