package com.arbiva.apfgc.provision.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

/**
 * 
 * @author srinivasa
 *
 */
public class JasperUtility {
	
	/**
	 * 
	 * @param jrxmlFile
	 * @param destFile
	 * @param params
	 */
	@SuppressWarnings("serial")
	public static void generatePDF(String jrxmlFile, String destFile, final Map<String, Object> params) {
		JasperReport jasperReport = null;
		JRDataSource dataSource = null;
		JasperPrint jasperPrint = null;
		InputStream inputStream = null;
		try {
			// Compile jrxml file.
			inputStream = JasperUtility.class.getResourceAsStream(jrxmlFile);
			jasperReport = JasperCompileManager.compileReport(inputStream);
			dataSource = new JRMapCollectionDataSource(new ArrayList<Map<String,?>>() {
				{
					add(params);
				}
			});
			jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), dataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, destFile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jasperReport = null;
			dataSource = null;
			jasperPrint = null;
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
