package com.arbiva.apfgc.provision.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServicePackageDTO {

	private static final long serialVersionUID = 1L;

	public ServicePackageDTO() {

	}

	@JsonProperty("subscribercode")
	private String subscribercode;

	private List<ServicePackDTO> servicepacks;

	/**
	 * @return the subscribercode
	 */
	public String getSubscribercode() {
		return subscribercode;
	}

	/**
	 * @param subscribercode
	 *            the subscribercode to set
	 */
	public void setSubscribercode(String subscribercode) {
		this.subscribercode = subscribercode;
	}

	/**
	 * @return the servicepacks
	 */
	public List<ServicePackDTO> getServicepacks() {
		return servicepacks;
	}

	/**
	 * @param servicepacks
	 *            the servicepacks to set
	 */
	public void setServicepacks(List<ServicePackDTO> servicepacks) {
		this.servicepacks = servicepacks;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
