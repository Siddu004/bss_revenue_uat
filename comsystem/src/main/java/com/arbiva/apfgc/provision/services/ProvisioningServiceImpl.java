/*package com.arbiva.apfgc.provision.services;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.provision.businessService.ProvisioningBusinessServiceImpl;

//@Service("provisioningServiceImpl")
//@Path("/provision")

@RestController
@RequestMapping(value = "/provision")
public class ProvisioningServiceImpl extends BaseServiceImpl {
	
	@Autowired
	private ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;
	
	*//**
	 * 	 
	 * @param request
	 * @return
	 *//*
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/activationrequests")
	@RequestMapping(value = "/activationrequests", method = RequestMethod.GET)
	public Response processProvisioningRequests(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.processProvisioningRequests();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}
	
	*//**
	 * 	 
	 * @param request
	 * @return
	 *//*
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/iptvcorpusservicepacks")
	@RequestMapping(value = "/iptvcorpusservicepacks", method = RequestMethod.GET)
	public Response processIPTVCorpusServicePacks(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.processIPTVCorpusServicePacks();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}
	
	*//**
	 * 
	 * @param request
	 * @return
	 *//*
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hsimulticast")
	@RequestMapping(value = "/hsimulticast", method = RequestMethod.GET)
	public Response processHSIMultiCastPackages(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.processHSIMultiCastPackages();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}	
	
	*//**
	 * 	 
	 * @param request
	 * @return
	 *//*
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postprovision")
	@RequestMapping(value = "/postprovision", method = RequestMethod.GET)
	public Response postProvisioningActivities(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.postProvisioningActivities();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}
	
	
	*//**
	 * 	 
	 * @param request
	 * @return
	 *//*
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/suspendiptv")
	@RequestMapping(value = "/suspendiptv", method = RequestMethod.GET)
	public Response suspendIPTVCorpusServicePacks(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.suspendIPTVCorpusServicePacks();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}
	
	*//**
	 * 	 
	 * @param request
	 * @return
	 *//*
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/resumeiptv")
	@RequestMapping(value = "/resumeiptv", method = RequestMethod.GET)
	public Response resumeIPTVCorpusServicePacks(@Context HttpServletRequest request) {
		ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		try {			
			provisioningBusinessServiceImpl.resumeIPTVCorpusServicePacks();
			builder = Response.status(Status.OK);
		} catch (Exception exc) {
			exc.printStackTrace();
			builder = handleExceptions(exc);
		}
		return builder.build();
	}
}*/