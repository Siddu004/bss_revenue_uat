package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ChangeDeviceDTO {

	@JsonProperty("subscriberCode")
	private String subscriberCode;

	@JsonProperty("oldDevice")
	private String oldDevice;

	@JsonProperty("newDevice")
	private String newDevice;
	
	@JsonProperty("serialNumber")
	private String serialNumber;
	
	@JsonProperty("url")
	private String url;
	
	@JsonProperty("registerType")
	private String registerType;
	
	

	public String getRegisterType() {
		return registerType;
	}

	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	public String getUrl(String url) {
		return url;
	}

	public String setUrl(String url) {
		return  url;
	}

	/*public String getserialNumber() {
		return serialNumber;
	}

	public void setserialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}*/

	public String getSubscriberCode() {
		return subscriberCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setSubscriberCode(String subscriberCode) {
		this.subscriberCode = subscriberCode;
	}

	public String getOldDevice() {
		return oldDevice;
	}

	public void setOldDevice(String oldDevice) {
		this.oldDevice = oldDevice;
	}

	public String getNewDevice() {
		return newDevice;
	}

	public void setNewDevice(String newDevice) {
		this.newDevice = newDevice;
	}
}
