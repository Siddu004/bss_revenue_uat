package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HSIMultiCastProvDTO {

	private static final long serialVersionUID = 1L;

	public HSIMultiCastProvDTO() {
	
	}
	
	private String requestid;
	
	private String dpndrequestid;
	
	private String url;
	
	private int seq;
	
	private String nwsubscode; 
	
	private String accessId;
	
	private String upStream;
	
	private String downStream;
	
	private String addlSrvcCode;
	
	private String profileName;
		
	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getUpStream() {
		return upStream;
	}

	public void setUpStream(String upStream) {
		this.upStream = upStream;
	}

	public String getDownStream() {
		return downStream;
	}

	public void setDownStream(String downStream) {
		this.downStream = downStream;
	}

	/**
	 * @return the requestid
	 */
	public String getRequestid() {
		return requestid;
	}

	/**
	 * @param requestid the requestid to set
	 */
	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	/**
	 * @return the dpndrequestid
	 */
	public String getDpndrequestid() {
		return dpndrequestid;
	}

	/**
	 * @param dpndrequestid the dpndrequestid to set
	 */
	public void setDpndrequestid(String dpndrequestid) {
		this.dpndrequestid = dpndrequestid;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the seq
	 */
	public int getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getNwsubscode() {
		return nwsubscode;
	}

	public void setNwsubscode(String nwsubscode) {
		this.nwsubscode = nwsubscode;
	}

	public String getAddlSrvcCode() {
		return addlSrvcCode;
	}

	public void setAddlSrvcCode(String addlSrvcCode) {
		this.addlSrvcCode = addlSrvcCode;
	}

	
}
