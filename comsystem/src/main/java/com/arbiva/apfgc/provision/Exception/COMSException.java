package com.arbiva.apfgc.provision.Exception;

import com.arbiva.apfgc.provision.dto.COMSErrorMessageDTO;

/**
 * {@link COMSException} is an use defined exception which holds the application
 * level errors.
 * 
 * @author srinivasa
 *
 */
public class COMSException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private COMSErrorMessageDTO comsErrorMessageDTO;

	public COMSException() {
		super();
	}

	public COMSException(String message) {
		super(message);
	}

	public COMSException(String message, Throwable cause) {
		super(message, cause);
	}

	public COMSException(COMSErrorMessageDTO messageDTO) {
		super();
		this.comsErrorMessageDTO = messageDTO;
	}

	public COMSErrorMessageDTO getComsErrorMessageDTO() {
		return comsErrorMessageDTO;
	}

	public void setComsErrorMessageDTO(COMSErrorMessageDTO comsErrorMessageDTO) {
		this.comsErrorMessageDTO = comsErrorMessageDTO;
	}

}
