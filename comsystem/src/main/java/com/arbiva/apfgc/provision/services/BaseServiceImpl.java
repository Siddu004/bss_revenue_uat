package com.arbiva.apfgc.provision.services;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.provision.Exception.COMSException;
import com.arbiva.apfgc.provision.dto.COMSErrorMessageDTO;
import com.arbiva.apfgc.provision.utils.COMSErrorCode.COMSErrorCodes;

/**
 * 
 * @author srinivasa
 * 
 */
@Component("baseServieImpl")
public class BaseServiceImpl {
	private static final Logger LOGGER = Logger.getLogger(BaseServiceImpl.class);

	/**
	 * 
	 * @param exc
	 * @param builder
	 * @return
	 */
	public ResponseBuilder handleExceptions(Exception exc) {
		ResponseBuilder builder = null;
		LOGGER.error("EXCEPTION::handleExceptions() :" + exc);
		if (exc instanceof COMSException) {
			builder = Response.status(Status.BAD_REQUEST).entity(
					((COMSException) exc).getComsErrorMessageDTO());
		} else {
			builder = Response.status(Status.INTERNAL_SERVER_ERROR).entity(
					new COMSErrorMessageDTO(COMSErrorCodes.GAE001, exc
							.getMessage()));
		}
		return builder;
	}
}