package com.arbiva.apfgc.provision.dao;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.model.OLPayment;
import com.arbiva.apfgc.comsystem.model.Payments;
import com.arbiva.apfgc.provision.dto.CAFOutputDTO;
import com.arbiva.apfgc.provision.dto.CAFRequestDTO;
import com.arbiva.apfgc.provision.dto.ChargeDetailsDTO;
import com.arbiva.apfgc.provision.dto.CustomerDTO;
import com.arbiva.apfgc.provision.dto.DunningCafDTO;
import com.arbiva.apfgc.provision.dto.FeatureParamsDTO;
import com.arbiva.apfgc.provision.dto.HSIMultiCastProvDTO;
import com.arbiva.apfgc.provision.dto.IPTVCorpusProvReqResDTO;
import com.arbiva.apfgc.provision.dto.ONURebootDTO;
import com.arbiva.apfgc.provision.dto.ProvisionInfoDTO;
import com.arbiva.apfgc.provision.dto.StbDTO;
import com.arbiva.apfgc.provision.models.ChangeDevice;
import com.arbiva.apfgc.provision.utils.COMSConstants;


/**
 * 
 * @author srinivasa
 *
 */
@Repository("commonDAO")
public class CommonDAO {
	
	private static final Logger LOGGER = Logger.getLogger(CommonDAO.class);

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public DataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	private EntityManager em;
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	/**
	 * 
	 * @return
	 */
	@Transactional
	public List<OLPayment> getPaymentCAFs() {
		
		List<OLPayment> olPaymentsList = new ArrayList<OLPayment>();
		TypedQuery<OLPayment> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(OLPayment.class.getSimpleName());
		builder.append(" WHERE status = 21 ");
		try {
			LOGGER.info("START::getPaymentCAFs");
			query = getEntityManager().createQuery(builder.toString(), OLPayment.class);
			olPaymentsList = query.getResultList();
			LOGGER.info("END::getPaymentCAFs");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getPaymentCAFs" + ex);
		}
		finally{
			builder = null;
			query = null;
		}
		return olPaymentsList;
	}
	
	@Transactional
	public OLPayment getOLPaymentCAFs(Long cafNo) {
		OLPayment olp = new OLPayment();
		List<OLPayment> olPaymentsList = new ArrayList<OLPayment>();
		TypedQuery<OLPayment> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(OLPayment.class.getSimpleName());
		builder.append(" WHERE status = 21 and acctcafno = ? ");
		try {
			LOGGER.info("START::getPaymentCAFs");
			query = getEntityManager().createQuery(builder.toString(), OLPayment.class);
			query.setParameter(1, cafNo);
			olPaymentsList = query.getResultList();
			if(olPaymentsList != null && !olPaymentsList.isEmpty())
				olp = olPaymentsList.get(0);
			LOGGER.info("END::getPaymentCAFs");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getPaymentCAFs" + ex);
		}
		finally{
			builder = null;
			query = null;
			olPaymentsList =null;
		}
		return olp;
	}
	
	@Transactional
	public void insertPaymentCAFs(List<OLPayment> olPaymentsList) {
		
		try {
			LOGGER.info("START::insertPaymentCAFs");
			for(OLPayment olp : olPaymentsList)
			{
				Payments payments = new Payments(olp);
				getEntityManager().persist(payments);
			}
			LOGGER.info("END::insertPaymentCAFs");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::insertPaymentCAFs" + ex);
		}
	}
	
	@Transactional
	public void updatePaymentCAFs(OLPayment olp, int status) {
				
		try {
			LOGGER.info("START::updatePaymentCAFs");
			
			//insertion into payments table
			Payments payments = new Payments(olp);
			getEntityManager().merge(payments);
				
			//update or delete in olpayments table
			olp.setStatus(status);
			getEntityManager().merge(olp);
			
			LOGGER.info("END::updatePaymentCAFs");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updatePaymentCAFs" + ex);
		}
	}

	/**
	 * 
	 * @param cafNo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ProvisionInfoDTO> getProvisioningRequests(long cafNo, String type) {
		
		List<Object[]> list = null;
		List<ProvisionInfoDTO> provisionInfoDTOList = new ArrayList<>();
		String qry = null;
		Query query = null;
		String INTERNET_SERVICE_CODE = COMSConstants.INTERNET_SERVICE_CODE;
		String TELEPHONY_SERVICE_CODE = COMSConstants.TELEPHONY_SERVICE_CODE;
		StringBuilder builder  = new StringBuilder();
		try {
			LOGGER.info("Fetching Requests for Provisioning for caf no:"+cafNo);
			
			builder.append(" SELECT caf.olt_id, caf.olt_portid  as olt_portid, caf.olt_onuid, cppm.cpe_profilename, caf.cpeslno, coresrvc.srvccodeprov, coresrvc.srvccode AS coresrvccode, cafsrvc.cafno prodcafno, caf.custid, ");
			builder.append(" cafsrvc.srvccode AS srvccodeaddl, cafsrvc.prodcode, om.pop_olt_ipaddress, products.prodtype, (select cstb.stbmacaddr from cafstbs cstb where cstb.stbcafno = cafsrvc.stbcafno and cstb.parentcafno = caf.cafno) stbmacaddr, (SELECT GROUP_CONCAT(REPLACE(phoneno,'-',''),'-', passwrd) phonenopwd FROM cafsrvcphonenos WHERE cafno = caf.cafno AND coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"' GROUP BY cafno) phonenopwd, ");
			builder.append(" (SELECT stdcode FROM villages v, cafs caff WHERE caff.inst_district = v.districtuid AND caff.inst_mandal = v.mandalslno AND caff.inst_city_village = v.villageslno AND caff.cafno=caf.cafno AND coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"') stdcode, caf.custtypelov, cafsrvc.vpnsrvcname, cafsrvc.expdate, caf.cafno acctcafno, (select cstb.stbcafno from cafstbs cstb where cstb.stbcafno = cafsrvc.stbcafno and cstb.parentcafno = caf.cafno) stbcafno, cpe_modeldetails ");
			builder.append(" , caf.aaacode, cppm.nativevlanflag, cppm.voipgponflag FROM cafs caf, cafsrvcs cafsrvc, srvcs srvc, coresrvcs coresrvc, oltmaster om, cpe_profilemaster cppm, cafprods cafprods, products products ");
			builder.append(" WHERE caf.cafno = cafsrvc.parentcafno ");
			builder.append(" AND cafsrvc.srvccode = srvc.srvccode ");
			builder.append(" AND coresrvc.srvccode = srvc.coresrvccode ");
			builder.append(" AND caf.olt_id = om.pop_olt_serialno ");
			builder.append(" AND caf.cpeprofileid = cppm.profile_id ");
			builder.append(" AND caf.cafno = cafprods.parentcafno ");
			builder.append(" AND products.prodcode = cafprods.prodcode ");
			builder.append(" AND cafprods.prodcode = cafsrvc.prodcode ");
			builder.append(" AND cafprods.cafno = cafsrvc.cafno ");
			builder.append(" AND current_date() between srvc.effectivefrom and srvc.effectiveto ");
			builder.append(" AND current_date() between products.effectivefrom and products.effectiveto ");
			if(type.equals(INTERNET_SERVICE_CODE))
				builder.append(" AND coresrvc.srvccodeprov='"+INTERNET_SERVICE_CODE+"' ");
			builder.append(" AND cafsrvc.cafno = :cafno ");
			builder.append(" ORDER BY cafsrvc.stbcafno, (CASE WHEN coresrvc.srvccodeprov='"+INTERNET_SERVICE_CODE+"' THEN 1 ELSE 2 END), ");
			builder.append(" (CASE WHEN products.prodtype='B' THEN 1 WHEN products.prodtype='A' THEN 2 ELSE 3 END), ");
			builder.append(" (CASE WHEN srvc.ftaflag=1 THEN 1 ELSE 2 END) ");
			
			qry = builder.toString();
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("cafno", cafNo);
			String alacartcafno="";
			
			list = (List<Object[]>) query.getResultList();

			
			for (Object[] object : list) {
				ProvisionInfoDTO provisionInfoDTO = new ProvisionInfoDTO();
				provisionInfoDTO.setOltid(object[0] == null ? "" : object[0].toString());
				provisionInfoDTO.setPortid(object[1] == null ? "" : object[1].toString());
				provisionInfoDTO.setOnuid(object[2] == null ? "" : object[2].toString());
				provisionInfoDTO.setModel(object[3] == null ? "" : object[3].toString());
				provisionInfoDTO.setCpeslno(object[4] == null ? "" : object[4].toString());
				provisionInfoDTO.setSrvccodeprov(object[5] == null ? "" : object[5].toString());
				provisionInfoDTO.setCoresrvccode(object[6] == null ? "" : object[6].toString());
				provisionInfoDTO.setProdcafno(object[7] == null ? "" : object[7].toString());
				provisionInfoDTO.setCustid(object[8] == null ? "" : object[8].toString());
				provisionInfoDTO.setSrvccodeaddl(object[9] == null ? "" : object[9].toString());
				provisionInfoDTO.setProdcode(object[10] == null ? "" : object[10].toString());
				provisionInfoDTO.setIpaddress(object[11] == null ? "" : object[11].toString());
				provisionInfoDTO.setStbMacAddr(object[13] == null ? "" : object[13].toString());
				provisionInfoDTO.setPhoneNopwd(object[14] == null ? "" : object[14].toString());
				provisionInfoDTO.setStdCode(object[15] == null ? "" : object[15].toString());
				provisionInfoDTO.setCustomerType(object[16] == null ? "" : object[16].toString());
				provisionInfoDTO.setVpnServiceName(object[17] == null ? "" : object[17].toString());
				provisionInfoDTO.setExpDate(object[18] == null ? "" : object[18].toString());
				provisionInfoDTO.setAcctcafno(object[19] == null ? "" : object[19].toString());
				provisionInfoDTO.setStbcafno(object[20] == null ? "" : object[20].toString());
				provisionInfoDTO.setTps(object[21] == null ? "" : object[21].toString());
				provisionInfoDTO.setClient(object[22] == null ? "" : object[22].toString());
				provisionInfoDTO.setNativeVlan(object[23] == null ? "" : object[23].toString());
				provisionInfoDTO.setVoipGponFlag(object[24] == null ? "" : object[24].toString());
				
				alacartcafno=provisionInfoDTO.getAcctcafno();
				String alacartecardnum=getEntityManager().createNativeQuery("select olt_cardno from cafs where cafno ="+alacartcafno).getSingleResult().toString();
				
				//int cardno=Integer.parseInt(cardnum);
				int cardno=Integer.parseInt(alacartecardnum);
				int portid=Integer.parseInt(provisionInfoDTO.getPortid());
				provisionInfoDTO.setCardnum(cardno+"");
				if(cardno==1) provisionInfoDTO.setPortid((portid+8)+"");
				provisionInfoDTOList.add(provisionInfoDTO);
			}
			
			//String cardnum=getEntityManager().createNativeQuery("select olt_cardno from cafs where cafno ="+cafNo).getSingleResult().toString();
			//String alacartecardnum=getEntityManager().createNativeQuery("select olt_cardno from cafs where cafno ="+alacartcafno).getSingleResult().toString();
			
			
			LOGGER.info("Fetching provisioning requests completed");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getProvisioningRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			list = null;
			builder = null;
		}
		return provisionInfoDTOList;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public ProvisionInfoDTO getProvisioningRequestsByReqid(long cafNo, String requestid) {
		
		List<Object[]> list = null;
		//List<ProvisionInfoDTO> provisionInfoDTOList = new ArrayList<>();
		String qry = null;
		Query query = null;
		String INTERNET_SERVICE_CODE = COMSConstants.INTERNET_SERVICE_CODE;
		String TELEPHONY_SERVICE_CODE = COMSConstants.TELEPHONY_SERVICE_CODE;
		ProvisionInfoDTO provisionInfoDTO = new ProvisionInfoDTO();
		try {
			LOGGER.info("START::getProvisioningRequests");
				   qry = "SELECT caf.olt_id, caf.olt_portid, caf.olt_onuid, cppm.cpe_profilename, caf.cpeslno, coresrvc.srvccodeprov, coresrvc.srvccode as coresrvccode, caf.cafno, caf.custid, "
						+" cafsrvc.srvccode as srvccodeaddl, cafsrvc.prodcode, om.pop_olt_ipaddress, products.prodtype, caf.stbmacaddr, (select phoneno from cafsrvcphonenos where cafno = caf.cafno and coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"') phoneno " 
						+" FROM cafs caf, cafsrvcs cafsrvc, srvcs srvc, coresrvcs coresrvc, oltmaster om, cpe_profilemaster cppm, cafprods cafprods, products products, olprovrequests req " 
						+" WHERE caf.cafno = cafsrvc.cafno "  
						+" and cafsrvc.srvccode = srvc.srvccode " 
						+" and coresrvc.srvccode = srvc.coresrvccode " 
						+" and caf.olt_id = om.pop_olt_serialno " 
						+" and caf.cpeprofileid = cppm.profile_id  "
						+" and caf.cafno = cafprods.cafno " 
						+" and products.prodcode = cafprods.prodcode " 
						+" and cafprods.prodcode = cafsrvc.prodcode " 
						+" and caf.cafno = :cafno " 
						+" and caf.cafno = req.acctcafno "
						+" and cafsrvc.srvccode = req.srvccodeaddl "
						+" and req.requestid = :requestid "
						+" order by cafno, (case when coresrvc.srvccodeprov='"+INTERNET_SERVICE_CODE+"' THEN 1 ELSE 2 END), " 
						+" (case when products.prodtype='B' THEN 1 when products.prodtype='A' THEN 2 ELSE 3 END), " 
						+" (case when srvc.ftaflag=1 THEN 1 ELSE 2 END) ";


			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("cafno", cafNo);
			query.setParameter("requestid", requestid);
			
			list = (List<Object[]>) query.getResultList();
			
			if(list != null && !list.isEmpty()){
				Object[] object = list.get(0);
				provisionInfoDTO.setOltid(object[0] == null ? "" : object[0].toString());
				provisionInfoDTO.setPortid(object[1] == null ? "" : object[1].toString());
				provisionInfoDTO.setOnuid(object[2] == null ? "" : object[2].toString());
				provisionInfoDTO.setModel(object[3] == null ? "" : object[3].toString());
				provisionInfoDTO.setCpeslno(object[4] == null ? "" : object[4].toString());
				provisionInfoDTO.setSrvccodeprov(object[5] == null ? "" : object[5].toString());
				provisionInfoDTO.setCoresrvccode(object[6] == null ? "" : object[6].toString());
				provisionInfoDTO.setCafno(object[7] == null ? "" : object[7].toString());
				provisionInfoDTO.setCustid(object[8] == null ? "" : object[8].toString());
				provisionInfoDTO.setSrvccodeaddl(object[9] == null ? "" : object[9].toString());
				provisionInfoDTO.setProdcode(object[10] == null ? "" : object[10].toString());
				provisionInfoDTO.setIpaddress(object[11] == null ? "" : object[11].toString());
				provisionInfoDTO.setStbMacAddr(object[13] == null ? "" : object[13].toString());
				provisionInfoDTO.setPhoneNo(object[14] == null ? "" : object[14].toString());
				//provisionInfoDTOList.add(provisionInfoDTO);
			}
			
			LOGGER.info("END::getProvisioningRequests");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getProvisioningRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			list = null;
		}
		return provisionInfoDTO;
	}*/

	/**
	 * 
	 * @param custid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public CustomerDTO getCustomerInfo(long cafno, String customerType) {		
		List<Object[]> objectList = null;
		CustomerDTO customerDTO = new CustomerDTO();
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::getCustomerInfo");
			if(customerType.equalsIgnoreCase("INDIVIDUAL")){
				qry = "select c.aadharno, c.actualdob, c.titlelov, c.fname,  CASE WHEN ifnull(c.lname,'') = '' THEN c.pocname ELSE c.lname END lname, c.pocmob1, c.email1, "
					 +" CONCAT_WS(', ', CASE WHEN caf.inst_addr1 ='' THEN null ELSE caf.inst_addr1 END "
					 +" 			  , CASE WHEN caf.inst_addr2 ='' THEN null ELSE caf.inst_addr2 END "
					 +"            , CASE WHEN caf.inst_locality ='' THEN null ELSE caf.inst_locality END) addr, v.villagename, caf.inst_state, m.mandalname, d.districtname "
					 +" from customers c, cafs caf, districts d, mandals m, villages v "
					 +" where c.custid = caf.custid "
					 +" AND caf.inst_district = d.districtuid "
					 +" AND caf.inst_mandal = m.mandalslno "
					 +" AND caf.inst_district = m.districtuid " 
					 +" AND caf.inst_city_village = v.villageslno " 
					 +" AND caf.inst_mandal = v.mandalslno " 
					 +" AND caf.inst_district = v.districtuid " 
					 +" AND caf.cafno = ? ";
			}
			else if(customerType.equalsIgnoreCase("ENTERPRISE")){
				qry = " select c.regncode, ifnull(c.dateofinc,'') dateofinc, 'Ms.', c.custname,  pocname lname, c.pocmob1, c.email1, "
						+" CONCAT_WS(', ', CASE WHEN caf.inst_addr1 ='' THEN null ELSE caf.inst_addr1 END "
						+" , CASE WHEN caf.inst_addr2 ='' THEN null ELSE caf.inst_addr2 END "
						+" , CASE WHEN caf.inst_locality ='' THEN null ELSE caf.inst_locality END) addr, v.villagename, caf.inst_state, m.mandalname, d.districtname " 
					 +" from entcustomers c, cafs caf, districts d, mandals m, villages v "
					 +" where c.custid = caf.custid "
					 +" AND caf.inst_district = d.districtuid "
					 +" AND caf.inst_mandal = m.mandalslno "
					 +" AND caf.inst_district = m.districtuid  "
					 +" AND caf.inst_city_village = v.villageslno " 
					 +" AND caf.inst_mandal = v.mandalslno " 
					 +" AND caf.inst_district = v.districtuid " 
					 +" AND caf.cafno = ? ";
			}
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objectList = query.getResultList();
			if(!objectList.isEmpty()){
				Object[] object = objectList.get(0);
				//customerDTO.setAadharno(object[0] == null ? "" : object[0].toString());
				//customerDTO.setDob(object[1] == null ? "" : object[1].toString());
				customerDTO.setTitle(object[2] == null ? "" : object[2].toString());
				customerDTO.setFirstname(object[3] == null ? "" : object[3].toString());
				customerDTO.setLastname(object[4] == null ? "" : object[4].toString());
				customerDTO.setContactno(object[5] == null ? "" : object[5].toString());
				customerDTO.setEmailid(object[6] == null ? "" : object[6].toString());
				customerDTO.setAddress(object[7] == null ? "" : object[7].toString());
				customerDTO.setVillage(object[8] == null ? "" : object[8].toString());
				customerDTO.setStateCode(object[9] == null ? "" : object[9].toString());
				customerDTO.setMandal(object[10] == null ? "" : object[10].toString());
				customerDTO.setDistrictCode(object[11] == null ? "" : object[11].toString());
				customerDTO.setCountryCode("India");
			}			
			LOGGER.info("END::getCustomerInfo");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCustomerInfo" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return customerDTO;
	}	
	
	/**
	 * 
	 * @param cafNo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> getProductNames(long cafNo) {
		
		List<String> list = null;
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::getProductNames");
			qry = "SELECT prod.prodname FROM cafprods cafprods, products prod WHERE cafprods.prodcode=prod.prodcode and cafprods.cafno = :cafNo ";
			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("cafNo", cafNo);
			
			list = (List<String>) query.getResultList();
			
			LOGGER.info("END::getProductNames");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getProductNames" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return list;
	}
	
	/**
	 * 
	 * @param custid
	 * @return
	 */
	public String getServiceParamValue(String serviceParamName, String addlSrvcCode) {		
		String qry = null;
		Query query = null;
		String serviceParamValue = null;
		try {
			LOGGER.info("START::getServiceParamValue");
			//qry = "SELECT prmvalue FROM srvcprmlst WHERE (prmtypelov = 'PROVISIONING' OR prmtypelov = 'BOTH') AND UPPER(prmname) = :serviceParamName AND coresrvccode = :coreSrvcCode ";
			qry = " SELECT prmvalue "+
				  " FROM srvcprms "+
				  " WHERE UPPER(prmcode) = :serviceParamName "+
				  " AND srvccode = :addlSrvcCode ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("serviceParamName", serviceParamName.toUpperCase());
			query.setParameter("addlSrvcCode", addlSrvcCode);
			serviceParamValue = (String) query.getSingleResult();
			LOGGER.info("END::getServiceParamValue");
		}
		catch (NoResultException nre){
			LOGGER.error(nre.getMessage());
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getServiceParamValue" + ex);
		}
		finally {
			query = null;
			qry = null;
			serviceParamValue = serviceParamValue == null ? "" : serviceParamValue;
		}
		return serviceParamValue;
	}
	
	/**
	 * 
	 * @param custid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<IPTVCorpusProvReqResDTO> getIPTVCorpusPrevProvJSONResponse() {		
		String qry = null;
		Query query = null;
		List<IPTVCorpusProvReqResDTO> listOfRequests = new ArrayList<>();
		List<IPTVCorpusProvReqResDTO> listOfResponses = new ArrayList<>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getIPTVCorpusPrevProvJSONResponse");
			qry = "SELECT a.requestid, b.seqnum, b.req FROM olprovrequests a, olprovjsons b WHERE a.requestid=b.requestid and b.status=3 "
					+ " and b.servicetype='IPTV'";
			query = getEntityManager().createNativeQuery(qry);
			
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				IPTVCorpusProvReqResDTO iptvCorpusProvReqResDTO = new IPTVCorpusProvReqResDTO();
				iptvCorpusProvReqResDTO.setRequestid(object[0] == null ? "" : object[0].toString());
				iptvCorpusProvReqResDTO.setSeqnum(object[1] == null ? 0 : Integer.parseInt(object[1].toString()));
				iptvCorpusProvReqResDTO.setRequest(object[2] == null ? "" : object[2].toString());
				listOfRequests.add(iptvCorpusProvReqResDTO);
			}
			
			for(IPTVCorpusProvReqResDTO dto: listOfRequests) {
				if(dto != null) {
					qry = "SELECT b.nwsubscode from olprovrequests a, olprovjsons b where a.requestid=b.requestid "
							+" and b.servicetype='IPTV' "
							+" and seqnum = 2 "
							+" and b.status = 1 "
							+" and stbcafno "
							+" in (select distinct stbcafno from olprovrequests a, olprovjsons b where a.requestid=b.requestid and b.requestid=?)";
					query = getEntityManager().createNativeQuery(qry);
					query.setParameter(1, dto.getRequestid());
					//query.setParameter(2, dto.getSeqnum()-incr);
					
					List<Object> objList = query.getResultList();
					if(!objList.isEmpty()){
						Object obj = objList.get(0);
						dto.setResponse(obj == null ? "" : obj.toString());
					}
					/*List<Object[]> oltObjList = query.getResultList();
					Object[] object = null;
					if(!oltObjList.isEmpty()){
						object = oltObjList.get(0);
						//dto.setRequestid(object[0] == null ? "" : object[0].toString());
						//dto.setSeqnum(object[1] == null ? 0 : Integer.parseInt(object[1].toString()));
						dto.setResponse(object[0] == null ? "" : object[0].toString());
						//dto.setRequest(object[3] == null ? "" : object[3].toString());
					}*/
					
					listOfResponses.add(dto);
				}						
			}
			LOGGER.info("END::getIPTVCorpusPrevProvJSONResponse");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getPreviousProvJSONResponse" + ex);
		}
		finally {
			query = null;
			qry = null;
			listOfRequests = null;
			ObjList = null;
		}
		return listOfResponses;
	}
	
	@SuppressWarnings("unchecked")
	public IPTVCorpusProvReqResDTO getIPTVCorpusProvJSON(String requestId, int seqnum) {
		IPTVCorpusProvReqResDTO dto = new IPTVCorpusProvReqResDTO();
		String qry = null;
		Query query = null;
		Object[] object = null;
		try{
			qry = "SELECT requestid, seqnum, resp, req FROM olprovjsons WHERE requestid=? and seqnum=?";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, requestId);
			query.setParameter(2, seqnum);
			
			List<Object[]> oltObjList = query.getResultList();
			if(!oltObjList.isEmpty()){
				object = oltObjList.get(0);
				dto.setRequestid(object[0] == null ? "" : object[0].toString());
				dto.setSeqnum(object[1] == null ? 0 : Integer.parseInt(object[1].toString()));
				dto.setResponse(object[2] == null ? "" : object[2].toString());
				dto.setRequest(object[3] == null ? "" : object[3].toString());
			}
			
		}catch(Exception e){
			LOGGER.error("ERROR::getIPTVCorpusNextProvJSON" + e);
		}
		finally{
			 qry = null;
			 query = null;
			 object = null;
		}
		return dto;
	}
	
	/**
	 * 
	 * @param custid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<HSIMultiCastProvDTO> getHSIMultiCastRequests() {		
		String qry = null;
		Query query = null;
		List<HSIMultiCastProvDTO> listOfRequests = new ArrayList<>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getHSIMultiCastRequests");
			qry = "SELECT a.requestid, a.dpndrequestid, b.url FROM olprovrequests a, olprovjsons b WHERE a.requestid=b.requestid and b.status=6 and b.servicetype='IPTV' and b.seqnum=0 ";
			query = getEntityManager().createNativeQuery(qry);
			
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				HSIMultiCastProvDTO dto = new HSIMultiCastProvDTO();
				dto.setRequestid(object[0] == null ? "" : object[0].toString());
				dto.setDpndrequestid(object[1] == null ? "" : object[1].toString());
				dto.setUrl(object[2] == null ? "" : object[2].toString());
				dto.setSeq(0);
				listOfRequests.add(dto);
			}
			LOGGER.info("END::getHSIMultiCastRequests");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getHSIMultiCastRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return listOfRequests;
	}	
	
	//mahesh-16-02-17 for AAA server
	@SuppressWarnings("unchecked")
	public List<HSIMultiCastProvDTO> getAAAServiceRequests() {	
		
		String qry = null;
		Query query = null;
		List<HSIMultiCastProvDTO> listOfRequests = new ArrayList<>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getAAAServiceRequests");
		//	qry = "SELECT b.requestid, b.url, (SELECT IFNULL(nwsubscode,0) FROM olprovjsons a WHERE a.requestid = b.requestid AND a.seqnum = 2 AND servicetype = 'HSI') nwsubscode FROM olprovjsons b WHERE seqnum = 3 AND servicetype = 'AAA'";
			
			qry = "SELECT b.requestid, b.url, (SELECT IFNULL(nwsubscode,0) FROM olprovjsons a WHERE a.requestid = b.requestid AND a.seqnum = 2 AND servicetype = 'HSI') nwsubscode , olreq.srvccodeaddl, cp.cpe_profilename"
					+ " FROM olprovjsons b, olprovrequests olreq , cafs c ,cpe_profilemaster cp "
					+ " WHERE seqnum = 3 AND servicetype = 'AAA' AND olreq.requestid = b.requestid "
					+ " and olreq.acctcafno = c.cafno and c.cpeprofileid = cp.profile_id";
			query = getEntityManager().createNativeQuery(qry);
	
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				HSIMultiCastProvDTO dto = new HSIMultiCastProvDTO();
				dto.setRequestid(object[0] == null ? "" : object[0].toString());
				dto.setUrl(object[1] == null ? "" : object[1].toString());
				dto.setNwsubscode(object[2] == null ? "" : object[2].toString());
				dto.setAddlSrvcCode(object[3] == null ? "" : object[3].toString());
				dto.setProfileName(object[4] == null ? "" : object[4].toString());
				listOfRequests.add(dto);
			}
			LOGGER.info("END::getAAAServiceRequests");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getAAAServiceRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return listOfRequests;
	}	
	
	
	@SuppressWarnings("unchecked")
	public List<HSIMultiCastProvDTO> getProfileANDSrvcCode(String nwSubsCode,String srvcCodeProv) {	
		
		String qry = null;
		Query query = null;
		List<HSIMultiCastProvDTO> listOfRequests = new ArrayList<>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		
		try {
			LOGGER.info("START::getProfileANDSrvcCode");
			qry = " SELECT cppm.cpe_profilename, cafsrvc.srvccode FROM cafs caf, cpe_profilemaster cppm,  cafsrvcs cafsrvc, srvcs srvc, coresrvcs coresrvc  WHERE caf.cpeprofileid = cppm.profile_id " +
					" AND caf.cafno = cafsrvc.cafno AND cafsrvc.srvccode = srvc.srvccode AND coresrvc.srvccode = srvc.coresrvccode " +
					" AND coresrvc.srvccodeprov = ?  AND agorahsisubscode = ? ";
			
			query = getEntityManager().createNativeQuery(qry);
			
			query.setParameter(1, srvcCodeProv);
			query.setParameter(2, nwSubsCode);
			
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				HSIMultiCastProvDTO dto = new HSIMultiCastProvDTO();
				dto.setProfileName(object[0] == null ? "" : object[0].toString());
				dto.setAddlSrvcCode(object[1] == null ? "" : object[1].toString());
				listOfRequests.add(dto);
			}
			LOGGER.info("END::getAAAServiceRequests");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getAAAServiceRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return listOfRequests;
	}	
	
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	@Transactional
	public long generateSequence(String type) {
		long sequenceNo = 0;
		SimpleJdbcCall generateSequenceSP = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("get_nextid").withoutProcedureColumnMetaDataAccess().useInParameterNames(
					"p_idname").declareParameters(new SqlParameter("p_idname",Types.VARCHAR), new SqlOutParameter("p_nextid", Types.BIGINT), 
							new SqlOutParameter("p_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_idname", type);
			out = generateSequenceSP.execute(in);			
			if(out.get("p_nextid") != null)
				sequenceNo = Long.valueOf(out.get("p_nextid").toString());
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during generateSequence(): " + ex); 
			throw ex;
		}
		finally {
			generateSequenceSP = null;
			in = null;
			out = null;
		}
		return sequenceNo;
	}
	
	/**
	 * 
	 * @param serviceParamNames
	 * @param coreSrvcCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getServiceParamValues(String serviceParamNames, String addlSrvcCode) {		
		String qry = null;
		Query query = null;
		HashMap<String, String> servicePrmLst = new HashMap<String, String>();
		List<Object[]> objList = new ArrayList<>();
		try {
			LOGGER.info("START::getServiceParamValues");
			//qry = "SELECT prmname, prmvalue FROM srvcprmlst WHERE UPPER(prmtypelov) = 'PROVISIONING' AND UPPER(prmname) IN ("+serviceParamNames+") AND coresrvccode = '"+coreSrvcCode+"'";
			qry = " SELECT prmcode, prmvalue "+
				  " FROM srvcprms "+
				  " WHERE UPPER(prmcode) IN ("+serviceParamNames+") "+
				  " AND srvccode = '"+addlSrvcCode+"'";
			query = getEntityManager().createNativeQuery(qry);
			//query.setParameter("serviceParamNames", serviceParamNames.toUpperCase());
			//query.setParameter("coreSrvcCode", coreSrvcCode);
			objList = query.getResultList();
			for(Object[] object : objList)
			{
				servicePrmLst.put(object[0] == null ? "" : object[0].toString(),object[1] == null ? "" : object[1].toString());
			}
						
			LOGGER.info("END::getServiceParamValues");
		}
		catch (NoResultException nre){
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getServiceParamValues" + ex);
		}
		finally {
			query = null;
			qry = null;
			objList = null;
		}
		return servicePrmLst;
	}
	
	@SuppressWarnings("unchecked")
	public String getFeaturecodes(String srvccodeprov, String cafno) {		
		String qry = null;
		Query query = null;
		String featureCodes = "";
		List<Object> objList = new ArrayList<>();
		try {
			LOGGER.info("START::getServiceParamValues");
			qry = "select cs.featurecodes "
					+" from cafsrvcs cs, srvcs s, coresrvcs csrvc "
					+" where cs.srvccode = s.srvccode "
					+" and s.coresrvccode = csrvc.srvccode "
					+" and csrvc.srvccodeprov = ? "
					+" and cs.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, srvccodeprov);
			query.setParameter(2, cafno);
			objList = query.getResultList();
			if(!objList.isEmpty()){
				featureCodes = objList.get(0) == null ? "" :objList.get(0).toString();
			}
						
			LOGGER.info("END::getServiceParamValues");
		}
		catch (NoResultException nre){
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getServiceParamValues" + ex);
		}
		finally {
			query = null;
			qry = null;
			objList = null;
		}
		return featureCodes;
	}
	
	@SuppressWarnings("unchecked")
	public List<FeatureParamsDTO> getCafFeaturePrms(String featurecode, String cafno) {		
		String qry = null;
		Query query = null;
		List<FeatureParamsDTO> cafFeaturePrms = new ArrayList<>();
		List<Object[]> objList = new ArrayList<>();
		try {
			LOGGER.info("START::getServiceParamValues");
			qry = "select prmcode, prmvalue "
					+" from caffeatureprms "
					+" where featurecode = ? "
					+" and cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, featurecode);
			query.setParameter(2, cafno);
			objList = query.getResultList();
			for(Object[] obj : objList)
			{
				FeatureParamsDTO featureParamsDTO = new FeatureParamsDTO();
				featureParamsDTO.setPrmcode(obj[0] == null ? "" : obj[0].toString());
				featureParamsDTO.setPrmvalue(obj[1].toString().split(","));
				cafFeaturePrms.add(featureParamsDTO);
			}
						
			LOGGER.info("END::getServiceParamValues");
		}
		catch (NoResultException nre){
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getServiceParamValues" + ex);
		}
		finally {
			query = null;
			qry = null;
			objList = null;
		}
		return cafFeaturePrms;
	}
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CAFRequestDTO> getCAFList(String INTERNET_SERVICE_CODE, String IPTV_SERVICE_CODE , String cafno) {
		String qry = null;
		Query query = null;
		List<CAFRequestDTO> cafRequestList = new ArrayList<CAFRequestDTO>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getCAFList");
			qry =   " SELECT distinct caf.inst_district, request.acctcafno, cafprods.tenantcode, cafprods.prodcode, cafsrvcs.srvccode, "
					+" (CASE WHEN request.srvccodeprov = '"+INTERNET_SERVICE_CODE+"' THEN 'AGORA' "
					+" WHEN request.srvccodeprov = '"+IPTV_SERVICE_CODE+"' THEN 'CORPUS' ELSE 'ZTE' END) nwsource, request.nwsubscode, "
					+" caf.custtypelov, request.requestid, caf.pmntcustid, "
					+" request.requestaction, DATE_FORMAT(NOW(),'%Y%m%d') actiondate, request.prodcafno, request.stbcafno "
					+" FROM olprovrequests request, cafs caf, cafprods cafprods, cafsrvcs cafsrvcs "
					+" WHERE caf.cafno=request.acctcafno  "
					+" and caf.cafno=cafprods.parentcafno "
					+" and caf.cafno=cafsrvcs.parentcafno  "
					+" and cafprods.prodcode = cafsrvcs.prodcode "
					//+" and request.stbcafno = cafsrvcs.stbcafno "
					+" and request.srvccodeaddl = cafsrvcs.srvccode  "
					+" and request.status = 1  "
					+" and request.prodcafno = :cafno ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("cafno", cafno);
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				CAFRequestDTO dto = new CAFRequestDTO();
				dto.setDistrictid(object[0] == null ? "" : object[0].toString());
				dto.setAcctcafno(object[1] == null ? "" : object[1].toString());
				dto.setTenantcode(object[2] == null ? "" : object[2].toString());
				dto.setProdcode(object[3] == null ? "" : object[3].toString());
				dto.setSrvccode(object[4] == null ? "" : object[4].toString());
				dto.setNwsource(object[5] == null ? "" : object[5].toString());
				dto.setNwsubscode(object[6] == null ? "" : object[6].toString());
				dto.setCustomertype(object[7] == null ? "" : object[7].toString());
				dto.setRequestid(object[8] == null ? "" : object[8].toString());
				dto.setCustomerid(object[9] == null ? "" : object[9].toString());
				dto.setRequestaction(object[10] == null ? "" : object[10].toString());
				dto.setActiondate(object[11] == null ? "" : object[11].toString());
				dto.setProdcafno(object[12] == null ? "" : object[12].toString());
				dto.setStbCafNo(object[13] == null ? "" : object[13].toString());
				cafRequestList.add(dto);
			}
									
			LOGGER.info("END::getCAFList");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFList" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFList" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return cafRequestList;
	}
	
	/*@SuppressWarnings("unchecked")
	@Transactional
	public List<CAFRequestDTO> getProvisionedCAFList() {
		String qry = null;
		Query query = null;
		List<CAFRequestDTO> cafRequestList = new ArrayList<CAFRequestDTO>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getCAFList");
			qry =    "SELECT caf.inst_district, request.acctcafno, cafprods.tenantcode, cafprods.prodcode, cafsrvcs.srvccode, " 
					 +" (CASE WHEN request.srvccodeprov = 'HSI' THEN 'AGORA'  "
					 +" WHEN request.srvccodeprov = 'IPTV' THEN 'CORPUS' ELSE '' END) nwsource, request.nwsubscode,  "
					 +" DATE_FORMAT(request.modifiedon,'%Y%m%d') actdate, caf.custtypelov, request.requestid, caf.pmntcustid, request.requestaction, DATE_FORMAT(NOW(),'%Y%m%d') actiondate " 
					 +" FROM olprovrequests request, cafs caf, cafprods cafprods, cafsrvcs cafsrvcs " 
					 +" WHERE cafsrvcs.cafno=request.acctcafno " 
					 +" and caf.cafno=cafprods.parentcafno " 
					 +" and cafprods.prodcode=cafsrvcs.prodcode "
					 +" and caf.cafno=cafsrvcs.parentcafno "
					 +" and cafprods.cafno=cafsrvcs.cafno "
					 +" and request.srvccodeaddl=cafsrvcs.srvccode "
					 +" and request.status = 1 ";
		  
			query = getEntityManager().createNativeQuery(qry);
			ObjList = query.getResultList();
			for(Object[] object : ObjList)
			{
				CAFRequestDTO dto = new CAFRequestDTO();
				dto.setDistrictid(object[0] == null ? "" : object[0].toString());
				dto.setCafno(object[1] == null ? "" : object[1].toString());
				dto.setTenantcode(object[2] == null ? "" : object[2].toString());
				dto.setProdcode(object[3] == null ? "" : object[3].toString());
				dto.setSrvccode(object[4] == null ? "" : object[4].toString());
				dto.setNwsource(object[5] == null ? "" : object[5].toString());
				dto.setNwsubscode(object[6] == null ? "" : object[6].toString());
				dto.setActivationdate(object[7] == null ? "" : object[7].toString());
				dto.setCustomertype(object[8] == null ? "" : object[8].toString());
				dto.setRequestid(object[9] == null ? "" : object[9].toString());
				dto.setCustomerid(object[10] == null ? "" : object[10].toString());
				dto.setRequestaction(object[11] == null ? "" : object[11].toString());
				dto.setActiondate(object[12] == null ? "" : object[12].toString());
				
				cafRequestList.add(dto);
			}
									
			LOGGER.info("END::getCAFList");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFList" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFList" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return cafRequestList;
	}*/
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getCafWiseReqCount() {
		String qry = null;
		Query query = null;
		Map<String,String> cafWiseReqMap = new LinkedHashMap<String,String>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getCafWiseReqCount");
			qry =" select prodcafno, count(status) "
				+" from olprovrequests "
				+" group by prodcafno "
				+" order by  prodcafno ";   
		  
			query = getEntityManager().createNativeQuery(qry);
			
			ObjList = query.getResultList();
			if(!ObjList.isEmpty()){
				for(Object[] object : ObjList)
				{
					cafWiseReqMap.put(object[0].toString(), object[1].toString());
				}
			}
			LOGGER.info("END::getCafWiseReqCount");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCafWiseReqCount" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCafWiseReqCount" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return cafWiseReqMap;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String,String> getCafWiseActCount() {
		String qry = null;
		Query query = null;
		Map<String,String> cafWiseActMap = new LinkedHashMap<String,String>();
		List<Object[]> ObjList = new ArrayList<Object[]>();
		try {
			LOGGER.info("START::getCafWiseActCount");
			qry =" select prodcafno, count(status) "
				+" from olprovrequests "
				+" where status = 1 "
				+" group by prodcafno "
				+" order by  prodcafno ";   
		  
			query = getEntityManager().createNativeQuery(qry);
			
			ObjList = query.getResultList();
			if(!ObjList.isEmpty()){
				for(Object[] object : ObjList)
				{
					cafWiseActMap.put(object[0].toString(), object[1].toString());
				}
			}
			LOGGER.info("END::getCafWiseActCount");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCafWiseActCount" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCafWiseActCount" + ex);
		}
		finally {
			query = null;
			qry = null;
			ObjList = null;
		}
		return cafWiseActMap;
	}
	
	@Transactional
	public String executePostProvisionProcess(CAFRequestDTO dto) {
		String output = null;
		SimpleJdbcCall generateSequenceSP = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			LOGGER.info("executePostProvisionProcess() Input DistrictId:" + dto.getDistrictid() + " cafNo:" + dto.getProdcafno() + " tenantCode:" + dto.getTenantcode() 
						   +" prodCode:" + dto.getProdcode() + " srvcCode:" + dto.getSrvccode() + " nwSource:" + dto.getNwsource() + " nwSourceCode:" + dto.getNwsubscode()
						   + " activationDate:" + dto.getActivationdate() + " customerType:" +  dto.getCustomertype() + " requestId:" + dto.getRequestid() 
						   + " Customer Id:" + dto.getCustomerid());
			generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("postprovupd").withoutProcedureColumnMetaDataAccess().
					useInParameterNames("p_districtuid", "p_pmntcustid", "p_acctcafno", "p_tenantcode", "p_prodcode", "p_srvccode", "p_prodcafno", 
							"p_stbcafno", "p_nwsource", "p_nwsubsccode", "p_requestid", "p_requestaction", "p_actiondate")
					.declareParameters(new SqlParameter("p_districtuid",Types.BIGINT), 
							new SqlParameter("p_pmntcustid", Types.BIGINT),
							new SqlParameter("p_acctcafno", Types.BIGINT),
							new SqlParameter("p_tenantcode", Types.VARCHAR),
							new SqlParameter("p_prodcode", Types.VARCHAR),
							new SqlParameter("p_srvccode", Types.VARCHAR),
							new SqlParameter("p_prodcafno", Types.BIGINT),
							new SqlParameter("p_stbcafno", Types.BIGINT),
							new SqlParameter("p_nwsource", Types.VARCHAR),
							new SqlParameter("p_nwsubsccode", Types.VARCHAR),
							new SqlParameter("p_requestid", Types.VARCHAR),
							new SqlParameter("p_requestaction", Types.VARCHAR),
							new SqlParameter("p_actiondate", Types.VARCHAR),
							new SqlOutParameter("p_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_districtuid", Long.valueOf(dto.getDistrictid()));
			in.put("p_pmntcustid", Long.valueOf(dto.getCustomerid()));
			in.put("p_acctcafno", Long.valueOf(dto.getAcctcafno()));
			in.put("p_tenantcode", dto.getTenantcode());
			in.put("p_prodcode", dto.getProdcode());
			in.put("p_srvccode", dto.getSrvccode());
			in.put("p_prodcafno", dto.getProdcafno());
			in.put("p_stbcafno", dto.getStbCafNo());
			in.put("p_nwsource", dto.getNwsource());
			in.put("p_nwsubsccode", dto.getNwsubscode());
			in.put("p_requestid", dto.getRequestid());
			in.put("p_requestaction", dto.getRequestaction());
			in.put("p_actiondate", dto.getActiondate());
			out = generateSequenceSP.execute(in);			
			if(out.get("p_result") != null)
				output = out.get("p_result").toString();
			
			LOGGER.info("executePostProvisionProcess() Output: " + output);
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during executePostProvisionProcess(): " + ex); 
			throw ex;
		}
		finally {
			generateSequenceSP = null;
			in = null;
			out = null;
		}
		
		return output;
	}	
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getCAFOutputDetails(String prodcafno, String acctcafno) {
		String qry = null;
		Query query = null;
		CAFOutputDTO cafOutputDTO = new CAFOutputDTO();
		List<Object> object = new ArrayList<>();
		String custType = "";
		try {
			LOGGER.info("START::getCAFOutputDetails");
			qry =" select custtypelov from cafs where cafno = ?";
		 	query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, acctcafno);
			object = query.getResultList();
			if(object != null && !object.isEmpty()){
				Object obj1 = object.get(0);
				custType = obj1 == null ? "" : obj1.toString();
			}
			if(custType.equalsIgnoreCase("INDIVIDUAL"))
				cafOutputDTO = getIndividualCustomerInfo(cafOutputDTO, acctcafno);
			else if(custType.equalsIgnoreCase("ENTERPRISE"))
				cafOutputDTO = getEnterpriseCustomerInfo(cafOutputDTO, acctcafno);
			cafOutputDTO = getPackageInfo(cafOutputDTO, acctcafno);
			cafOutputDTO = getCPEInfo(cafOutputDTO, acctcafno);
			cafOutputDTO = getSTBInfo(cafOutputDTO, acctcafno);
			cafOutputDTO = getPaymentInfo(cafOutputDTO, acctcafno);
			cafOutputDTO = getSubscriptionInfo(cafOutputDTO, prodcafno);
			cafOutputDTO = getChargesInfo(cafOutputDTO, prodcafno); 	
			LOGGER.info("END::getCAFOutputDetails");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFOutputDetails" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFOutputDetails" + ex);
		}
		finally {
			query = null;
			qry = null;
			object = null;
			custType =null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getPaymentInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		try{
			qry = " SELECT pm.pmntamt amtpaid, pm.pmntmodelov, pm.pmntamt, DATE(pm.pmntdate) pmntdate, pm.pmntrefno, pm.pmntbank, pm.pmntbranch "
					 +" FROM cafs caf, payments pm  "
					 +" WHERE	caf.cafno = pm.acctcafno "
					 +" AND DATE(pm.pmntdate) = CURDATE() "
					 +" AND caf.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				cafOutputDTO.setAmountpaid(obj[0] == null ? new BigDecimal("0") : new BigDecimal(obj[0].toString()));
				cafOutputDTO.setMode(obj[1] == null ? "" : obj[1].toString());
				cafOutputDTO.setAmount(obj[2] == null ? new BigDecimal("0") : new BigDecimal(obj[2].toString()));
				cafOutputDTO.setDate(obj[3] == null ? "" : obj[3].toString());
				cafOutputDTO.setDdno(obj[4] == null ? "" : obj[4].toString());
				cafOutputDTO.setBankdrawn(obj[5] == null ? "" : obj[5].toString());
				cafOutputDTO.setBranch(obj[6] == null ? "" : obj[6].toString());
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getSubscriptionInfo(CAFOutputDTO cafOutputDTO, String prodcafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		List<ChargeDetailsDTO> subscriptionList=new ArrayList<>();
		try{
			qry = "SELECT  a.cafno, a.chargename,CASE WHEN a.billfreqlov='MONTHLY' THEN a.chargeamt  WHEN a.billfreqlov='QUARTERLY' THEN a.chargeamt * 3 "
					+" WHEN a.billfreqlov='HALFYEARLY' THEN a.chargeamt * 6  WHEN a.billfreqlov='YEARLY' THEN a.chargeamt * 12 END chargeamt, "
					+" a.billfreqlov FROM ( SELECT cafprod.cafno, cc.chargename, SUM(pc.chargeamt) chargeamt, caf.billfreqlov "
					+" FROM cafs caf, cafprods cafprod, prodcharges pc, chargecodes cc "
					+" WHERE pc.chargecode = cc.chargecode "
					+" AND caf.cafno = cafprod.parentcafno "
					+" AND cc.chargetypeflag = 1 "
					+" AND cafprod.prodcode = pc.prodcode "
					+" AND CURDATE() between pc.effectivefrom AND pc.effectiveto "
					+" AND cafprod.cafno = ? "
					+" GROUP BY cc.chargecode, caf.billfreqlov) a ";		
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, prodcafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				ChargeDetailsDTO chargeDetailsDTO = new ChargeDetailsDTO();
				chargeDetailsDTO.setChargename(obj[1] == null ? "" : obj[1].toString());
				chargeDetailsDTO.setChargeamt(obj[2] == null ? new BigDecimal("0") : new BigDecimal(obj[2].toString()));
				//chargeDetailsDTO.setBillfreqlov(obj[3] == null ? "" : obj[3].toString());
				subscriptionList.add(chargeDetailsDTO);
			}
			cafOutputDTO.setSubscriptionList(subscriptionList);
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getChargesInfo(CAFOutputDTO cafOutputDTO, String prodcafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		List<ChargeDetailsDTO> chargeList=new ArrayList<>();
		try{
			qry = "SELECT a.acctcafno, a.chargename, sum(a.chargeamt) "
				+" FROM(SELECT ci.acctcafno, cc.chargename, ci.chargeamt+ci.srvctax+ci.swatchtax+ci.kisantax+ci.enttax AS chargeamt "
						+" FROM cafinvdtls ci, chargecodes cc "
						+" WHERE ci.chargecode = cc.chargecode "
						+" AND rectype = 'CHARGED' "
						+" AND ci.prodcafno = ?) a "
						+" GROUP BY a.acctcafno, a.chargename ";	
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, prodcafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				ChargeDetailsDTO chargeDetailsDTO = new ChargeDetailsDTO();
				chargeDetailsDTO.setChargename(obj[1] == null ? "" : obj[1].toString());
				chargeDetailsDTO.setChargeamt(obj[2] == null ? new BigDecimal("0") : new BigDecimal(obj[2].toString()));
				chargeList.add(chargeDetailsDTO);
			}
			cafOutputDTO.setChargeList(chargeList);
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getCPEInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		try{
			qry = " SELECT caf.cpeslno, cpm.cpe_model,(SELECT GROUP_CONCAT(phoneno) FROM cafsrvcphonenos WHERE cafno = caf.cafno GROUP BY cafno) telephoneallocated, "
				 +" CASE WHEN caf.cpeleaseyn='O' THEN 'OWN' "
				 +"  WHEN caf.cpeleaseyn='N' THEN 'NO' "
				 +"  WHEN caf.cpeleaseyn='P' THEN 'PURCHASE' "
				 +"  WHEN caf.cpeleaseyn='I' THEN 'INSTALLMENT' END cpeleaseyn, caf.cpemacaddr "
				 +" FROM cafs caf, cpe_profilemaster cpm "
				 +" WHERE caf.cpeprofileid = cpm.profile_id "
				 +" AND caf.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				cafOutputDTO.setCpeserialno(obj[0] == null ? "" : obj[0].toString());
				cafOutputDTO.setCpemodel(obj[1] == null ? "" : obj[1].toString());
				cafOutputDTO.setTelephoneallocated(obj[2] == null ? "" : obj[2].toString());
				cafOutputDTO.setCpeleaseyn(obj[3] == null ? "" : obj[3].toString());
				cafOutputDTO.setCpemacaddr(obj[4] == null ? "" : obj[4].toString());
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getSTBInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		List<StbDTO> stbList = new ArrayList<>();
		try{
			qry = " select stbmacaddr, CASE WHEN cstb.stbleaseyn='O' THEN 'OWN' "
				 +"  WHEN cstb.stbleaseyn='N' THEN 'NO' "
				 +"  WHEN cstb.stbleaseyn='P' THEN 'PURCHASE' "
				 +"  WHEN cstb.stbleaseyn='I' THEN 'INSTALLMENT' END stbleaseyn, cpm.cpe_model, stbslno " 
				 +" from cafs caf, cafstbs cstb, cpe_profilemaster cpm "
				 +" where caf.cafno = cstb.parentcafno "
				 +" and cstb.stbprofileid = cpm.profile_id "
				 +" and caf.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				StbDTO stbDTO = new StbDTO();
				stbDTO.setStbMacAddr(obj[0] == null ? "" : obj[0].toString());
				stbDTO.setStbLeaseyn(obj[1] == null ? "" : obj[1].toString());
				stbDTO.setStbModel(obj[2] == null ? "" : obj[2].toString());
				stbDTO.setStbSlno(obj[3] == null ? "" : obj[3].toString());
				stbList.add(stbDTO);
			}
			cafOutputDTO.setStbList(stbList);
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getPackageInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		String prodtype = "";
		try{
			qry = "SELECT GROUP_CONCAT(prod.prodname) prodname, prod.prodtype "
					+" FROM cafprods cp, products prod "
					+" WHERE cp.prodcode = prod.prodcode "
					+" AND cp.cafno = ? "
					+" GROUP BY prodtype; ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				prodtype = (obj[1] == null ? "" : obj[1].toString());
				if(prodtype.equalsIgnoreCase("A"))
					cafOutputDTO.setAdditionalpackage(obj[0] == null ? "" : obj[0].toString());
				else if(prodtype.equalsIgnoreCase("B"))
					cafOutputDTO.setBundledservicepackage(obj[0] == null ? "" : obj[0].toString());
				else if(prodtype.equalsIgnoreCase("O"))
					cafOutputDTO.setOntimepackage(obj[0] == null ? "" : obj[0].toString());
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
			prodtype = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getIndividualCustomerInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		try{
			qry = "SELECT customer.custid, caf.cafno, customer.titlelov, customer.fname, customer.mname, customer.lname,caf.inst_addr1, "
					+" caf.inst_addr2, caf.inst_locality, caf.inst_area, v.villagename, m.mandalname, d.districtname, caf.inst_pin, customer.pocmob1, "
					+" customer.pocmob2, customer.landline1, '' a, '' b, "
					+" CASE WHEN caf.custtypelov='INDIVIDUAL' THEN CONCAT_WS(' ', nullif(customer.fname,''), nullif(customer.mname,''), nullif(customer.lname,'')) "
						 +" ELSE (SELECT custname FROM entcustomers WHERE custid = caf.pmntcustid) END paymentresponsible, "
					+" tenant.tenantname, tenant.regoff_pocmob1, tenant.regoff_addr1, customer.aadharno ,caf.custtypelov, customer.stdcode, caf.billfreqlov, customer.gender, customer.email1 "
					+" FROM cafs caf, customers customer, tenants tenant, districts d, mandals m, villages v "
					+" WHERE caf.custid = customer.custid  "
					+" AND caf.lmocode = tenant.tenantcode  "
					+" AND caf.inst_district = d.districtuid  "
					+" AND caf.inst_mandal = m.mandalslno  "
					+" AND caf.inst_district = m.districtuid  "
					+" AND caf.inst_city_village = v.villageslno  "
					+" AND caf.inst_mandal = v.mandalslno  "
					+" AND caf.inst_district = v.districtuid  "
					+" AND caf.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				cafOutputDTO.setCustidno(obj[0] == null ? "" : obj[0].toString());
				cafOutputDTO.setCafno(obj[1] == null ? "" : obj[1].toString());
				cafOutputDTO.setTitle(obj[2] == null ? "" : obj[2].toString());
				cafOutputDTO.setFname(obj[3] == null ? "" : obj[3].toString());
				cafOutputDTO.setMname(obj[4] == null ? "" : obj[4].toString());
				cafOutputDTO.setLname(obj[5] == null ? "" : obj[5].toString());
				cafOutputDTO.setInstaddr1(obj[6] == null ? "" : obj[6].toString());
				cafOutputDTO.setInstaddr2(obj[7] == null ? "" : obj[7].toString());
				cafOutputDTO.setInstlocality(obj[8] == null ? "" : obj[8].toString());
				cafOutputDTO.setInstarea(obj[9] == null ? "" : obj[9].toString());
				cafOutputDTO.setInstcity(obj[10] == null ? "" : obj[10].toString());
				cafOutputDTO.setInstmandal(obj[11] == null ? "" : obj[11].toString());
				cafOutputDTO.setInstdistrict(obj[12] == null ? "" : obj[12].toString());
				cafOutputDTO.setInstpincode(obj[13] == null ? "" : obj[13].toString());
				cafOutputDTO.setInstmob1(obj[14] == null ? "" : obj[14].toString());
				cafOutputDTO.setInstmob2(obj[15] == null ? "" : obj[15].toString());
				cafOutputDTO.setLandline1(obj[16] == null ? "" : obj[16].toString());
				cafOutputDTO.setPoi(obj[17] == null ? "" : obj[17].toString());
				cafOutputDTO.setPoa(obj[18] == null ? "" : obj[18].toString());
				cafOutputDTO.setPaymentresponsible(obj[19] == null ? "" : obj[19].toString());
				cafOutputDTO.setLmoname(obj[20] == null ? "" : obj[20].toString());
				cafOutputDTO.setLmocontactnumber(obj[21] == null ? "" : obj[21].toString());
				cafOutputDTO.setLmoaddress(obj[22] == null ? "" : obj[22].toString());
				cafOutputDTO.setAadharno(obj[23] == null ? "" : obj[23].toString());
				cafOutputDTO.setCusttypelov(obj[24] == null ? "" : obj[24].toString());
				cafOutputDTO.setStdcode(obj[25] == null ? "" : obj[25].toString());
				cafOutputDTO.setBillfreqlov(obj[26] == null ? "" : obj[26].toString());
				cafOutputDTO.setGender(obj[27] == null ? "" : obj[27].toString());
				cafOutputDTO.setEmail(obj[28] == null ? "" : obj[28].toString());
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	@SuppressWarnings("unchecked")
	public CAFOutputDTO getEnterpriseCustomerInfo(CAFOutputDTO cafOutputDTO, String cafno){
		String qry = null;
		Query query = null;
		List<Object[]> objects = new ArrayList<>();
		try{
			qry = "SELECT customer.custid, caf.cafno, 'Ms.', customer.custname, '' mname, '' lname, "    
					+" caf.inst_addr1, caf.inst_addr2, caf.inst_locality, caf.inst_area, v.villagename, m.mandalname, d.districtname, caf.inst_pin, customer.pocmob1, customer.pocmob2, "   
					+" customer.landline1, '' a, '' b, (select custname from entcustomers where custid = caf.pmntcustid) paymentresponsible, "
					+" tenant.tenantname, tenant.regoff_pocmob1, tenant.regoff_addr1, customer.regncode ,caf.custtypelov, customer.stdcode, caf.billfreqlov, customer.email1, customer.pocname "  
					+" FROM  cafs caf, entcustomers customer, tenants tenant, districts d, mandals m, villages v " 
					+" WHERE caf.custid = customer.custid "
					+" AND caf.lmocode = tenant.tenantcode "
					+" AND caf.inst_district = d.districtuid "   
					+" AND caf.inst_mandal = m.mandalslno "
					+" AND caf.inst_district = m.districtuid "   
					+" AND caf.inst_city_village = v.villageslno "   
					+" AND caf.inst_mandal = v.mandalslno "
					+" AND caf.inst_district = v.districtuid "
					+" AND caf.cafno = ? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			objects = query.getResultList();
			for(Object[] obj : objects){
				cafOutputDTO.setCustidno(obj[0] == null ? "" : obj[0].toString());
				cafOutputDTO.setCafno(obj[1] == null ? "" : obj[1].toString());
				cafOutputDTO.setTitle(obj[2] == null ? "" : obj[2].toString());
				cafOutputDTO.setFname(obj[3] == null ? "" : obj[3].toString());
				cafOutputDTO.setMname(obj[4] == null ? "" : obj[4].toString());
				cafOutputDTO.setLname(obj[5] == null ? "" : obj[5].toString());
				cafOutputDTO.setInstaddr1(obj[6] == null ? "" : obj[6].toString());
				cafOutputDTO.setInstaddr2(obj[7] == null ? "" : obj[7].toString());
				cafOutputDTO.setInstlocality(obj[8] == null ? "" : obj[8].toString());
				cafOutputDTO.setInstarea(obj[9] == null ? "" : obj[9].toString());
				cafOutputDTO.setInstcity(obj[10] == null ? "" : obj[10].toString());
				cafOutputDTO.setInstmandal(obj[11] == null ? "" : obj[11].toString());
				cafOutputDTO.setInstdistrict(obj[12] == null ? "" : obj[12].toString());
				cafOutputDTO.setInstpincode(obj[13] == null ? "" : obj[13].toString());
				cafOutputDTO.setInstmob1(obj[14] == null ? "" : obj[14].toString());
				cafOutputDTO.setInstmob2(obj[15] == null ? "" : obj[15].toString());
				cafOutputDTO.setLandline1(obj[16] == null ? "" : obj[16].toString());
				cafOutputDTO.setPoi(obj[17] == null ? "" : obj[17].toString());
				cafOutputDTO.setPoa(obj[18] == null ? "" : obj[18].toString());
				cafOutputDTO.setPaymentresponsible(obj[19] == null ? "" : obj[19].toString());
				cafOutputDTO.setLmoname(obj[20] == null ? "" : obj[20].toString());
				cafOutputDTO.setLmocontactnumber(obj[21] == null ? "" : obj[21].toString());
				cafOutputDTO.setLmoaddress(obj[22] == null ? "" : obj[22].toString());
				cafOutputDTO.setAadharno(obj[23] == null ? "" : obj[23].toString());
				cafOutputDTO.setCusttypelov(obj[24] == null ? "" : obj[24].toString());
				cafOutputDTO.setStdcode(obj[25] == null ? "" : obj[25].toString());
				cafOutputDTO.setBillfreqlov(obj[26] == null ? "" : obj[26].toString());
				cafOutputDTO.setEmail(obj[27] == null ? "" : obj[27].toString());
				cafOutputDTO.setContactperson(obj[28] == null ? "" : obj[28].toString());
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR::getIndividualCustomerInfo" + e);
		}
		finally {
			query = null;
			qry = null;
			objects = null;
		}
		
		return cafOutputDTO;
	}
	
	public CAFOutputDTO getEnterpriseCafOutputDtls(CAFOutputDTO cafOutputDTO, String cafno){
		return cafOutputDTO;
	}
	/**
	 * 
	 * @return
	 */
	@Transactional
	public void updateProvRequest(String cafno) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateProvRequest");
			qry = "UPDATE olprovrequests SET status=10, modifiedon=now(), modifiedby='system' WHERE acctcafno=?";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafno);
			query.executeUpdate();
						
			LOGGER.info("END::updateProvRequest");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateProvRequest" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateProvRequest" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@Transactional
	public CAFOutputDTO getCAFPackages(CAFOutputDTO dto) {
		String qry = null;
		Query query = null;
		String bundledservicepackage = null, addonpackages = null;
		try {
			LOGGER.info("START::getCAFPackages");
			qry = "SELECT b.prodname FROM cafprods a, products b  WHERE a.prodcode=b.prodcode and b.prodtype='B' and a.cafno = ?";			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, dto.getCafno());
			bundledservicepackage = (String) query.getSingleResult();
			dto.setBundledservicepackage(bundledservicepackage);
			
			qry = "SELECT GROUP_CONCAT(b.prodname) FROM cafprods a, products b  WHERE a.prodcode=b.prodcode and b.prodtype='A' and a.cafno = ?";			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, dto.getCafno());
			addonpackages = (String) query.getSingleResult();
			dto.setAdditionalpackage(addonpackages);			
									
			LOGGER.info("END::getCAFPackages");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFPackages" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFPackages" + ex);
		}
		finally {
			query = null;
			qry = null;
			bundledservicepackage = null;
			addonpackages = null;
		}
		return dto;
	}
	
	/**
	 * 
	 * @return
	 */
	/*@Transactional
	public CAFOutputDTO getCAFPaymentDetails(CAFOutputDTO dto) {
		String qry = null;
		Query query = null;
		CAFPaymentDTO paymentDto = null;
		try {
			LOGGER.info("START::getCAFPaymentDetails");
			qry = "SELECT pmntamt, pmntmodelov, pmntamt, pmntdate, pmntrefno, pmntbank, pmntbranch FROM payments WHERE acctcafno = ? and status=21";			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, dto.getCafno());
			paymentDto = (CAFPaymentDTO) query.getSingleResult();
			
			dto.setAmountpaid(paymentDto.getAmountpaid());
			dto.setMode(paymentDto.getMode());
			dto.setAmount(paymentDto.getAmount());
			dto.setDate(paymentDto.getDate());
			dto.setDdno(paymentDto.getDdno());
			dto.setBankdrawn(paymentDto.getBankdrawn());
			dto.setBranch(paymentDto.getBranch());
									
			LOGGER.info("END::getCAFPaymentDetails");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFPaymentDetails" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFPaymentDetails" + ex);
		}
		finally {
			query = null;
			qry = null;
			paymentDto = null;
		}
		return dto;
	}*/
	
	/**
	 * 
	 * @return
	 */
	@Transactional
	public String getCAFSrvcNWSubscriberCode(long cafNo) {
		String qry = null;
		Query query = null;
		String nwSubscriberCode = "";
		try {
			LOGGER.info("START::getCAFSrvcNWSubscriberCode");
			qry = "SELECT ifnull(nwsubscode, 0) FROM cafstbs WHERE stbcafno=?";			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			//query.setParameter(2, srvcCode);
			nwSubscriberCode = (String) query.getSingleResult();
			
			LOGGER.info("END::getCAFSrvcNWSubscriberCode");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getCAFSrvcNWSubscriberCode" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getCAFSrvcNWSubscriberCode" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return nwSubscriberCode;
	}
	
	/**
	 * 
	 * @return
	 */
	@Transactional
	public String getAgoraNWSubscriberCode(String requestId) {
		String qry = null;
		Query query = null;
		String nwSubscriberCode = "0";
		try {
			LOGGER.info("START::getAgoraNWSubscriberCode");
			qry = "SELECT ifnull(nwsubscode, 0) FROM olprovjsons WHERE requestid=? and seqnum=2 and servicetype='HSI'";			
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, requestId);
			nwSubscriberCode = (String) query.getSingleResult();
			
			LOGGER.info("END::getAgoraNWSubscriberCode");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getAgoraNWSubscriberCode" + nre+" "+requestId);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getAgoraNWSubscriberCode" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return nwSubscriberCode;
	}
	
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DunningCafDTO> getSuspendedCafs() {
		String qry = null;
		Query query = null;
		List<DunningCafDTO> dunningCafList = new ArrayList<DunningCafDTO>();
		List<Object[]> oltObjList = null;
		try {
			LOGGER.info("START::getSuspendedCafs");
			qry = "SELECT distinct acctcafno, '' FROM dunningcafs where DATE(duedate) = CURDATE() and status = 1";
			query = getEntityManager().createNativeQuery(qry);
			
			oltObjList = query.getResultList();
			for (Object[] object : oltObjList) {
				DunningCafDTO dto = new DunningCafDTO();
				dto.setAcctcafno(object[0] == null ? "" : object[0].toString());
				/*dto.setTenantcode(object[1] == null ? "" : object[1].toString());
				dto.setProductcode(object[2] == null ? "" : object[2].toString());
				dto.setSrvccode(object[3] == null ? "" : object[3].toString());
				dto.setActdate(object[4] == null ? "" : object[4].toString());
				dto.setDuedate(object[5] == null ? "" : object[5].toString());
				dto.setPayamt(object[6] == null ? "" : object[6].toString());
				dto.setPaidamt(object[7] == null ? "" : object[7].toString());
				dto.setStatus(object[8] == null ? "" : object[8].toString());*/

				dunningCafList.add(dto);
			}
			
			LOGGER.info("END::getSuspendedCafs");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getSuspendedCafs" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getSuspendedCafs" + ex);
		}
		finally {
			query = null;
			qry = null;
			oltObjList = null;
		}
		return dunningCafList;
	}
	
	/**
	 * 
	 * @param cafNo
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public String getOLPaymentForSuspension(String cafNo) {		
		String qry = null;
		Query query = null;
		String checkCnt = "0";
		Object[] object = null;
		try {
			LOGGER.info("START::getOLPaymentForSuspension");
			qry = "SELECT * FROM olpayments WHERE acctcafno=? and status=22";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, Long.parseLong(cafNo));
			
			List<Object[]> oltObjList = query.getResultList();
			if(!oltObjList.isEmpty()){
				object = oltObjList.get(0);
				checkCnt = "1";
			}
						
			LOGGER.info("END::getOLPaymentForSuspension");
		}
		catch (NoResultException nre){
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getOLPaymentForSuspension" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return checkCnt;
	}
	
	/**
	 * 
	 * @param cafNo
	 */
	@Transactional
	public void updateDunningCaf(String cafNo, String type) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateDunningCafStatus");
			if(type.equals("S"))
				qry = "UPDATE dunningcafs SET status=1, duedate=STR_TO_DATE('31/12/2999', '%d/%m/%Y') WHERE acctcafno=? ";
			else if(type.equals("R"))
				qry = "UPDATE dunningcafs SET status=1, duedate=STR_TO_DATE('31/12/2999', '%d/%m/%Y') WHERE acctcafno=? and status=2 ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			query.executeUpdate();
						
			LOGGER.info("END::updateDunningCafStatus");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateDunningCafStatus" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateDunningCafStatus" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	/**
	 * 
	 * @param cafNo
	 */
	@Transactional
	public int updateDunningCafStatus(String cafNo) {
		String qry = null;
		Query query = null;
		int res = 0;
		try {
			LOGGER.info("START::updateDunningCafStatus");
			qry = "UPDATE dunningcafs SET status=2 WHERE acctcafno=? and status=1";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			res = query.executeUpdate();
						
			LOGGER.info("END::updateDunningCafStatus");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateDunningCafStatus" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateDunningCafStatus" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
		return res;
	}
	
	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DunningCafDTO> getReadyForResumeCafs() {
		String qry = null;
		Query query = null;
		List<DunningCafDTO> dunningCafList = new ArrayList<DunningCafDTO>();
		List<Object[]> oltObjList = null;
		try {
			LOGGER.info("START::getReadyForResumeCafs");
			qry = "SELECT acctcafno, tenantcode, productcode, srvccode, actdate, duedate, payamt, paidamt, status FROM dunningcafs where status=2";
			query = getEntityManager().createNativeQuery(qry);
			
			oltObjList = query.getResultList();
			for (Object[] object : oltObjList) {
				DunningCafDTO dto = new DunningCafDTO();
				dto.setAcctcafno(object[0] == null ? "" : object[0].toString());
				dto.setTenantcode(object[1] == null ? "" : object[1].toString());
				dto.setProductcode(object[2] == null ? "" : object[2].toString());
				dto.setSrvccode(object[3] == null ? "" : object[3].toString());
				dto.setActdate(object[4] == null ? "" : object[4].toString());
				dto.setDuedate(object[5] == null ? "" : object[5].toString());
				dto.setPayamt(object[6] == null ? "" : object[6].toString());
				dto.setPaidamt(object[7] == null ? "" : object[7].toString());
				dto.setStatus(object[8] == null ? "" : object[8].toString());

				dunningCafList.add(dto);
			}
						
			LOGGER.info("END::getReadyForResumeCafs");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getReadyForResumeCafs" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getReadyForResumeCafs" + ex);
		}
		finally {
			query = null;
			qry = null;
			oltObjList = null;
		}
		return dunningCafList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getProvRequestType(String reqId) {
		String qry = null;
		Query query = null;
		String reqType = "";
		List<String> objList = null;
		try {
			LOGGER.info("START::getProvRequestType");
			qry = "select case when requestaction = 1 then 'A' "
					+" when requestaction = 2 then 'S' "
		            +" when requestaction = 3 then 'R' "
		            +" when requestaction = 4 then 'D' end as requestaction " 
		            +" from olprovrequests where requestid = :reqId ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("reqId", reqId);
			
			objList = query.getResultList();
			//Object[] object = null;
			if(!objList.isEmpty()){
				//object = objList.get(0);
				reqType = objList.get(0) == null ? "" : objList.get(0);
			}
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getProvRequestType" + ex);
		}
		finally {
			query = null;
			qry = null;
			objList = null;
		}
		return reqType;
	}
	
	@Transactional
	public void updateCafSrvcs(String cafNo, String srvcCode, String type) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateCafSrvcs");
			if(type.equalsIgnoreCase("S")){
				qry = "UPDATE cafsrvcs "
						+" SET status=7, suspdate=current_date() "
						+"    ,suspdates=CASE WHEN suspdates IS NULL THEN current_date() ELSE concat(suspdates,',',current_date()) END " 
						+" WHERE cafno = ? and srvccode = ?";
			}
			else if(type.equalsIgnoreCase("R")){
				qry = "UPDATE cafsrvcs "
						+" SET status=6, resumedate=current_date() "
						+"    ,resumedates=CASE WHEN resumedates IS NULL THEN current_date() ELSE concat(resumedates,',',current_date()) END " 
						+" WHERE cafno = ? and srvccode = ?";
			}
			else if(type.equalsIgnoreCase("D")){
				qry = "UPDATE cafsrvcs "
						+" SET status=8, deactdate=current_date() "
						+" WHERE cafno = ? and srvccode = ?";
			}
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			query.setParameter(2, srvcCode);
			query.executeUpdate();
						
			LOGGER.info("END::updateCafSrvcs");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateCafSrvcs" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateCafSrvcs" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	@Transactional
	public void updateCafProd(String cafNo, String type) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateAcct");
			if(type.equalsIgnoreCase("S")){
				qry = "update cafprods cp, products p  "
					+" set cp.status = 7 "
					+" where cp.prodcode = p.prodcode "
					+" and prodtype in('A','O') "
					+" and cp.cafno = ?";
			}
			else if(type.equalsIgnoreCase("R")){
				qry = "update cafprods cp, products p  "
						+" set cp.status = 6 "
						+" where cp.prodcode = p.prodcode "
						+" and prodtype in('A','O') "
						+" and cp.cafno = ?";
			}
			else if(type.equalsIgnoreCase("D")){
				qry = "update cafprods cp, products p  "
						+" set cp.status = 8 "
						+" where cp.prodcode = p.prodcode "
						+" and prodtype in('A','O') "
						+" and cp.cafno = ?";
			}
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			query.executeUpdate();
						
			LOGGER.info("END::updateAcct");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateAcct" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateAcct" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	@Transactional
	public void updateCaf(String cafNo, String type) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateCaf");
			if(type.equalsIgnoreCase("S")){
				qry = "UPDATE cafs "
						+" SET status=7 " 
						+" WHERE cafno = ?";
			}
			else if(type.equalsIgnoreCase("R")){
				qry = "UPDATE cafs "
						+" SET status=6 " 
						+" WHERE cafno = ?"; 
			}
			else if(type.equalsIgnoreCase("D")){
				qry = "UPDATE cafs "
						+" SET status=8 " 
						+" WHERE cafno = ?"; 
			}
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			query.executeUpdate();
						
			LOGGER.info("END::updateCaf");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateCaf" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateCaf" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	@Transactional
	public void updateCafBlackListing(String cafNo) {
		String qry = null;
		Query query = null;
		try {
			LOGGER.info("START::updateCafBlackListing");
			qry = " update cafs "
				 +" set blacklistdate = current_date() "
				 +" where cafno = ?";
		
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			query.executeUpdate();
						
			LOGGER.info("END::updateCafBlackListing");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::updateCafBlackListing" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::updateCafBlackListing" + ex);
		}
		finally {
			query = null;
			qry = null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<DunningCafDTO> getdeActivatedCafs() {
		String qry = null, qry1 = null;
		Query query = null, query1 = null;
		List<DunningCafDTO> dunningCafList = new ArrayList<DunningCafDTO>();
		List<Object[]> oltObjList = null;
		try {
			LOGGER.info("START::getdeActivatedCafs");
			qry = "SELECT acctcafno, tenantcode, productcode, srvccode, actdate, duedate, "
					+ "payamt, paidamt, status FROM dunningcafs where status=3";
			query = getEntityManager().createNativeQuery(qry);
			
			oltObjList = query.getResultList();
			for (Object[] object : oltObjList) {
				DunningCafDTO dto = new DunningCafDTO();
				dto.setAcctcafno(object[0] == null ? "" : object[0].toString());
				dto.setTenantcode(object[1] == null ? "" : object[1].toString());
				dto.setProductcode(object[2] == null ? "" : object[2].toString());
				dto.setSrvccode(object[3] == null ? "" : object[3].toString());
				dto.setActdate(object[4] == null ? "" : object[4].toString());
				dto.setDuedate(object[5] == null ? "" : object[5].toString());
				dto.setPayamt(object[6] == null ? "" : object[6].toString());
				dto.setPaidamt(object[7] == null ? "" : object[7].toString());
				dto.setStatus(object[8] == null ? "" : object[8].toString());

				dunningCafList.add(dto);
				
				qry1 = "UPDATE dunningcafs set status=4 where acctcafno = ?";
				query1 = getEntityManager().createNativeQuery(qry1);
				query1.setParameter(1, dto.getAcctcafno());
				query1.executeUpdate();
			}
			
			
			LOGGER.info("END::getdeActivatedCafs");
		}
		catch (NoResultException nre){
			LOGGER.error("ERROR::getdeActivatedCafs" + nre);
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getdeActivatedCafs" + ex);
		}
		finally {
			query = null;
			qry = null;
			query1 = null;
			qry1 = null;
			oltObjList = null;
		}
		return dunningCafList;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getNwSubsciberCodeList(String cafNo) {
		String qry = null;
		Query query = null;
		List<Object> oltObjList = null;
		String nwSubsciberCode = "";
		try {
			LOGGER.info("START::getNwSubsciberCodeList");
			qry = "SELECT distinct nwsubscode "
				+" FROM cafsrvcs cs, srvcs s, coresrvcs crs "
				+" WHERE cs.srvccode =s.srvccode "
				+" and s.coresrvccode = crs.srvccode "
				+" and crs.srvccodeprov = 'IPTV' "
				+" and cs.cafno=? "
				+" and ifnull(nwsubscode,'') <> '' ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			oltObjList = query.getResultList();
			if(oltObjList != null && !oltObjList.isEmpty()){
				Object obj = oltObjList.get(0);
				nwSubsciberCode = obj == null ? "" : obj.toString();
			}	
			LOGGER.info("END::getNwSubsciberCodeList");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getNwSubsciberCodeList" + ex);
		}
		finally {
			query = null;
			qry = null;
			oltObjList = null;
		}
		return nwSubsciberCode;
	}

	@Transactional
	public ChangeDevice saveOrUpdate(ChangeDevice changeDevice) {
		try {
			changeDevice = getEntityManager().merge(changeDevice);
		}
		catch(Exception ex) {
			LOGGER.error("EXCEPTION::saveOrUpdate() " + ex);
			throw ex;
		}
		return changeDevice;
	}
	
	//added 22-02-17-for De-Activation
	@SuppressWarnings("unchecked")
	public List<ProvisionInfoDTO> getDeleteRequests(long cafNo,String requestid) {
		
		List<Object[]> list = null;
		List<ProvisionInfoDTO> provisionInfoDTOList = new ArrayList<>();
		String qry = null;
		Query query = null;
		String INTERNET_SERVICE_CODE = COMSConstants.INTERNET_SERVICE_CODE;
		String IPTV_SERVICE_CODE = COMSConstants.IPTV_SERVICE_CODE;
		String TELEPHONY_SERVICE_CODE = COMSConstants.TELEPHONY_SERVICE_CODE;
		String VPN_SERVICE_CODE = COMSConstants.VPN_SERVICE_CODE;
		StringBuilder builder  = new StringBuilder();
		try {
			LOGGER.info("START::getDeleteRequests");
							
			builder.append(" SELECT caf.olt_id, caf.olt_portid + 8 AS olt_portid, caf.olt_onuid, cppm.cpe_profilename, caf.cpeslno, coresrvc.srvccodeprov, coresrvc.srvccode AS coresrvccode, cafsrvc.cafno prodcafno, caf.custid, ");
			builder.append(" cafsrvc.srvccode AS srvccodeaddl, cafsrvc.prodcode, om.pop_olt_ipaddress, products.prodtype, (SELECT cstb.stbmacaddr FROM cafstbs cstb WHERE cstb.stbcafno = cafsrvc.stbcafno AND cstb.parentcafno = caf.cafno) stbmacaddr, (SELECT GROUP_CONCAT(REPLACE(phoneno,'-',''),'-', ifnull(passwrd,'')) phonenopwd FROM cafsrvcphonenos WHERE cafno = caf.cafno AND coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"' GROUP BY cafno) phonenopwd, ");
			builder.append(" (SELECT stdcode FROM villages v, cafs caff WHERE caff.inst_district = v.districtuid AND caff.inst_mandal = v.mandalslno AND caff.inst_city_village = v.villageslno AND caff.cafno=caf.cafno AND coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"') stdcode, caf.custtypelov, cafsrvc.vpnsrvcname, cafsrvc.expdate, caf.cafno acctcafno, (SELECT cstb.stbcafno FROM cafstbs cstb WHERE cstb.stbcafno = cafsrvc.stbcafno AND cstb.parentcafno = caf.cafno) stbcafno, ");
			builder.append(" CASE WHEN srvccodeprov = '"+INTERNET_SERVICE_CODE+"' THEN caf.agorahsisubscode WHEN srvccodeprov = '"+IPTV_SERVICE_CODE+"' THEN (SELECT nwsubscode FROM cafstbs cst WHERE cst.stbcafno = cafsrvc.stbcafno AND cst.parentcafno = caf.cafno) END nwsubscode ");
			builder.append(" , caf.aaacode, caf.agorahsisubscode, cppm.voipgponflag ");
			builder.append(" FROM cafs caf, cafsrvcs cafsrvc, srvcs srvc, coresrvcs coresrvc, oltmaster om, cpe_profilemaster cppm, cafprods cafprods, products products ");
			builder.append(" WHERE caf.cafno = cafsrvc.parentcafno ");
			builder.append(" AND cafsrvc.srvccode = srvc.srvccode ");
			builder.append(" AND coresrvc.srvccode = srvc.coresrvccode ");
			builder.append(" AND caf.olt_id = om.pop_olt_serialno ");
			builder.append(" AND caf.cpeprofileid = cppm.profile_id ");
			builder.append(" AND caf.cafno = cafprods.parentcafno ");
			builder.append(" AND products.prodcode = cafprods.prodcode ");
			builder.append(" AND cafprods.prodcode = cafsrvc.prodcode ");
			builder.append(" AND cafprods.cafno = cafsrvc.cafno ");
			builder.append(" AND CURRENT_DATE() BETWEEN srvc.effectivefrom AND srvc.effectiveto ");
			builder.append(" AND CURRENT_DATE() BETWEEN products.effectivefrom AND products.effectiveto ");
			builder.append(" AND cafsrvc.cafno = :cafno ");
			builder.append(" ORDER BY CASE WHEN coresrvc.srvccodeprov='"+TELEPHONY_SERVICE_CODE+"' THEN 1 ");
			builder.append(" WHEN coresrvc.srvccodeprov='"+IPTV_SERVICE_CODE+"' THEN 2  WHEN coresrvc.srvccodeprov='"+VPN_SERVICE_CODE+"' THEN 3 WHEN coresrvc.srvccodeprov='"+INTERNET_SERVICE_CODE+"' THEN 4 END ");
			                                 
			qry = builder.toString();
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter("cafno", cafNo);
			if(!requestid.isEmpty())
				query.setParameter("reqid", requestid);
			
			list = (List<Object[]>) query.getResultList();
			
			for (Object[] object : list) {
				ProvisionInfoDTO provisionInfoDTO = new ProvisionInfoDTO();
				provisionInfoDTO.setOltid(object[0] == null ? "" : object[0].toString());
				provisionInfoDTO.setPortid(object[1] == null ? "" : object[1].toString());
				provisionInfoDTO.setOnuid(object[2] == null ? "" : object[2].toString());
				provisionInfoDTO.setModel(object[3] == null ? "" : object[3].toString());
				provisionInfoDTO.setCpeslno(object[4] == null ? "" : object[4].toString());
				provisionInfoDTO.setSrvccodeprov(object[5] == null ? "" : object[5].toString());
				provisionInfoDTO.setCoresrvccode(object[6] == null ? "" : object[6].toString());
				provisionInfoDTO.setProdcafno(object[7] == null ? "" : object[7].toString());
				provisionInfoDTO.setCustid(object[8] == null ? "" : object[8].toString());
				provisionInfoDTO.setSrvccodeaddl(object[9] == null ? "" : object[9].toString());
				provisionInfoDTO.setProdcode(object[10] == null ? "" : object[10].toString());
				provisionInfoDTO.setIpaddress(object[11] == null ? "" : object[11].toString());
				provisionInfoDTO.setStbMacAddr(object[13] == null ? "" : object[13].toString());
				provisionInfoDTO.setPhoneNopwd(object[14] == null ? "" : object[14].toString());
				provisionInfoDTO.setStdCode(object[15] == null ? "" : object[15].toString());
				provisionInfoDTO.setCustomerType(object[16] == null ? "" : object[16].toString());
				provisionInfoDTO.setVpnServiceName(object[17] == null ? "" : object[17].toString());
				provisionInfoDTO.setExpDate(object[18] == null ? "" : object[18].toString());
				provisionInfoDTO.setAcctcafno(object[19] == null ? "" : object[19].toString());
				provisionInfoDTO.setStbcafno(object[20] == null ? "" : object[20].toString());
				provisionInfoDTO.setNwSubscode(object[21] == null ? "" : object[21].toString());
				provisionInfoDTO.setClient(object[22] == null ? "" : object[22].toString());
				provisionInfoDTO.setAgoraHsiSubsCode(object[23] == null ? "" : object[23].toString());
				provisionInfoDTO.setVoipGponFlag(object[24] == null ? "" : object[24].toString());
				provisionInfoDTOList.add(provisionInfoDTO);
			}
			
			LOGGER.info("END::getDeleteRequests");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getDeleteRequests" + ex);
		}
		finally {
			query = null;
			qry = null;
			list = null;
			builder = null;
		}
		return provisionInfoDTOList;
	}
	
	//added 09/03/17
	@SuppressWarnings("unchecked")
	@Transactional
	public String getHSIIPTVNWSubscriberCode(Long cafNo) {
		String qry = null;
		Query query = null;
		List<Object> oltObjList = null;
		String nwSubsciberCode = "";
		try {
			LOGGER.info("START::getHSIIPTVNWSubscriberCode");
			qry = "SELECT agorahsisubscode FROM cafs WHERE cafno=? ";
			query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1, cafNo);
			oltObjList = query.getResultList();
			
			if(oltObjList != null && !oltObjList.isEmpty()){
				Object obj = oltObjList.get(0);
				nwSubsciberCode = obj == null ? "" : obj.toString();
			}	
			LOGGER.info("END::getHSIIPTVNWSubscriberCode");
		}
		catch(Exception ex) {
			LOGGER.error("ERROR::getHSIIPTVNWSubscriberCode" + ex);
		}
		finally {
			query = null;
			qry = null;
			oltObjList = null;
		}
		return nwSubsciberCode;
	}	

	
	@SuppressWarnings("unchecked")
	@Transactional
	public String getAcessId(Long productCafNO) {
		String qry=null;
		Query query=null;
		List<Object> list=null;
		String macAddr="";
		try{
		LOGGER.info("START:getProfileName()");
		 qry="select LOWER(c.cpemacaddr) from cafs c where c.cafno=?";
		 query=getEntityManager().createNativeQuery(qry);
		 query.setParameter(1, productCafNO);
		 list=query.getResultList();
		 
		 if(list !=null && !list.isEmpty()){
			 Object obj= list.get(0);
			 macAddr=obj ==null? "" :obj.toString();
		 }
		 LOGGER.info("END::getProfileName()");		 
		}
		catch(Exception e){
			LOGGER.error("ERROR::START:getProfileName()" + e);
		}
		finally {
			query = null;
			qry = null;
			list = null;
		}				 
		return macAddr;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public String getClientName(Long productCafNO) {
		String qry=null;
		Query query=null;
		List<Object> list=null;
		String clientName="";
		try{
		LOGGER.info("START:getClientName()");
		qry=" select CONCAT(olt.oltaccessnode,':',c.olt_cardno,':',c.olt_portid,':',c.olt_onuid) As ClientName from cafs c,oltmaster olt "
			+ " where c.cafno=? AND c.olt_id=olt.pop_olt_serialno ";
		query=getEntityManager().createNativeQuery(qry);
		query.setParameter(1, productCafNO);
		list=query.getResultList();
		if(list!= null && !list.isEmpty()){
			Object object=list.get(0);
			clientName = object == null ? "" :object.toString();			
		  }
		}
		catch(Exception e){
			LOGGER.error("ERROR::START:getClientName()" + e);
		}
		finally {
			query = null;
			qry = null;
			list = null;
		}	
		return clientName;
	}

	@SuppressWarnings("unchecked")
	public Object[] getClientInformation(long acctcafno) {

		Object[] obj = null;
		StringBuilder bilder = new StringBuilder("");
		bilder.append("SELECT ");
		bilder.append(" CASE WHEN STATUS = 2 THEN aaathrsdspdstr ELSE '' END AS throtDown,");
		bilder.append(" CASE WHEN STATUS = 2 THEN aaathrsuspdstr ELSE '' END AS throtUp,");
		bilder.append(" CASE WHEN STATUS = 1 THEN aaabasedspdstr ELSE '' END AS BaseDown,");
		bilder.append(" CASE WHEN STATUS = 1 THEN aaabaseuspdstr ELSE '' END AS BaseUp,");
		bilder.append(" clientid ");
		bilder.append(" FROM hsicumusage ");
		bilder.append(" WHERE acctcafno = '"+acctcafno+"'");
		bilder.append(" AND usageyyyy = (SELECT MAX(usageyyyy) FROM hsicumusage WHERE acctcafno = '"+acctcafno+"')");
		bilder.append(" AND usagemm = (SELECT MAX(usagemm) FROM hsicumusage WHERE acctcafno = '"+acctcafno+"') ");
		
		try{
			
			Query query = getEntityManager().createNativeQuery(bilder.toString());
			List<Object[]> list  = query.getResultList();
			if(list.size() > 0)
				obj = list.get(0);
		}catch(Exception e){
			LOGGER.error("ERROR::getClientInformation()" + e);
			e.printStackTrace();
		}
		
		return obj;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProvisionInfoDTO> getBasePackagesList(Long cafNo, String prodCode) {
		ProvisionInfoDTO dto=new ProvisionInfoDTO();
		List<ProvisionInfoDTO> basePkgList=new ArrayList<>();
		Query qry=null;
		String query=null;
		List<Object[]> list=null;
		StringBuilder builder=new StringBuilder(); 
		String INTERNET_SERVICE_CODE = COMSConstants.INTERNET_SERVICE_CODE ;
		
		try{
			LOGGER.info("commonDAO:::::::::: getBasePackagesList() ::: START:::");
			builder.append(" SELECT cs.cafno prodcafno, c.cafno,c.custid,c.aaacode, cs.prodcode,cs.srvccode, cor.srvccodeprov, c.agorahsisubscode ");
			builder.append(" FROM cafsrvcs cs, cafs c, coresrvcs cor, srvcs s ");
			builder.append(" WHERE cs.parentcafno = c.cafno ");
			builder.append(" AND cs.srvccode = s.srvccode ");
			builder.append(" AND s.coresrvccode = cor.srvccode ");
			builder.append(" AND cs.prodcode = :prodcode ");
			builder.append(" AND cs.cafno = :cafno ");
			builder.append(" AND CURRENT_DATE BETWEEN s.effectivefrom AND s.effectiveto ");
			builder.append(" AND CURRENT_DATE BETWEEN cor.effectivefrom AND cor.effectiveto ");
			builder.append(" AND cor.srvccode = '"+INTERNET_SERVICE_CODE+"'" );
			
			query= builder.toString();
			LOGGER.info("query for base pack change "+query);
			qry=getEntityManager().createNativeQuery(query);
			
			qry.setParameter("prodcode", prodCode);
			qry.setParameter("cafno", cafNo);
			
			list = (List<Object[]>)qry.getResultList();
			
			for(Object[] object : list){
				dto.setProdcafno(object[0] == null ? "" : object[0].toString());
				dto.setAcctcafno(object[1] == null ? "" : object[1].toString());
				dto.setCustid(object[2] == null ? "" : object[2].toString());
				dto.setClient(object[3] == null ? "" : object[3].toString());
				dto.setProdcode(object[4] == null ? "" : object[4].toString());
				dto.setSrvccodeaddl(object[5] == null ? "" : object[5].toString());
				dto.setSrvccodeprov(object[6] == null ? "" : object[6].toString());
				dto.setNwSubscode(object[7] == null ? "" : object[7].toString());
				
				basePkgList.add(dto);
				}
			LOGGER.info("commonDAO:::::::::: getBasePackagesList() ::: END:::");
		}
		catch(Exception e){
			LOGGER.error("Error in getBasePackagesList()"+ e);
			throw e;			
		}
		finally{
			list=null;
			qry=null;
			query=null;
		}
		return basePkgList;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProvisionInfoDTO> getPackageTerminationList(Long cafNo, String prodCode,String stbCafNo,String nwSubsCode ) {
		ProvisionInfoDTO dto=new ProvisionInfoDTO();
		List<ProvisionInfoDTO> pkgTerList=new ArrayList<>();
		Query qry=null;
		String query=null;
		List<Object[]> list=null;
		StringBuilder builder=new StringBuilder(); 
		String IPTV_SERVICE_CODE = COMSConstants.IPTV_SERVICE_CODE ;
		
		try{
			LOGGER.info("commonDAO:::::::::: getPackageTerminationList() ::: START:::");
			builder.append(" SELECT cs.cafno prodcafno, c.cafno, c.custid,cs.prodcode,cs.srvccode, cor.srvccodeprov , cstb.nwsubscode, cstb.stbcafno");
			builder.append(" FROM cafsrvcs cs, cafs c, coresrvcs cor, srvcs s , cafstbs cstb ");
			builder.append(" WHERE cs.parentcafno = c.cafno ");
			builder.append(" AND cs.srvccode = s.srvccode ");
			builder.append(" AND s.coresrvccode = cor.srvccode ");
			builder.append(" AND cs.prodcode = :prodcode ");
			//builder.append(" AND cstb.nwsubscode = :nwsubscode ");
			builder.append(" AND cstb.stbcafno=  :stbcafno ");
			builder.append(" AND cstb.parentcafno = c.cafno ");
			builder.append(" AND cs.cafno = :cafno ");
			builder.append(" AND CURRENT_DATE BETWEEN s.effectivefrom AND s.effectiveto ");
			builder.append(" AND CURRENT_DATE BETWEEN cor.effectivefrom AND cor.effectiveto ");
			builder.append(" AND cor.srvccode = '"+IPTV_SERVICE_CODE+"' " );
			
			query= builder.toString();
			LOGGER.info("query for  pack termination "+query);
			qry=getEntityManager().createNativeQuery(query);
			
			qry.setParameter("prodcode", prodCode);
			//qry.setParameter("nwsubscode", nwSubsCode);
			qry.setParameter("stbcafno", stbCafNo);
			qry.setParameter("cafno", cafNo);
			
			list = (List<Object[]>)qry.getResultList();
			
			for(Object[] object : list){
				dto.setProdcafno(object[0] == null ? "" : object[0].toString());
				dto.setAcctcafno(object[1] == null ? "" : object[1].toString());
				dto.setCustid(object[2] == null ? "" : object[2].toString());				
				dto.setProdcode(object[3] == null ? "" : object[3].toString());
				dto.setSrvccodeaddl(object[4] == null ? "" : object[4].toString());
				dto.setSrvccodeprov(object[5] == null ? "" : object[5].toString());
				dto.setNwSubscode(object[6] == null ? "" : object[6].toString());
				dto.setStbcafno(object[7] == null ? "" : object[7].toString());
				
				pkgTerList.add(dto);
				}
			LOGGER.info("commonDAO:::::::::: getPackageTerminationList() ::: END:::");
		}
		catch(Exception e){
			LOGGER.error("Error in getPackageTerminationList()"+ e);
			throw e;			
		}
		finally{
			list=null;
			qry=null;
			query=null;
		}
		return pkgTerList;

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<ProvisionInfoDTO> getAlaCartePackageTerminationList(Long cafNo, String prodCode,String stbCafNo,String nwSubsCode ) {
		ProvisionInfoDTO dto=new ProvisionInfoDTO();
		List<ProvisionInfoDTO> pkgTerList=new ArrayList<>();
		Query qry=null;
		String query=null;
		List<Object[]> list=null;
		StringBuilder builder=new StringBuilder(); 
		String IPTV_SERVICE_CODE = COMSConstants.IPTV_SERVICE_CODE ;
		
		try{
			LOGGER.info("commonDAO:::::::::: getPackageTerminationList() ::: START:::");
			builder.append(" SELECT cs.cafno prodcafno, c.cafno, c.custid,cs.prodcode,cs.srvccode, cor.srvccodeprov , cstb.nwsubscode, cstb.stbcafno");
			builder.append(" FROM cafsrvcs cs, cafs c, coresrvcs cor, srvcs s , cafstbs cstb ");
			builder.append(" WHERE cs.parentcafno = c.cafno ");
			builder.append(" AND cs.srvccode = s.srvccode ");
			builder.append(" AND s.coresrvccode = cor.srvccode ");
			builder.append(" AND cs.prodcode = :prodcode ");
			builder.append(" AND cstb.nwsubscode = :nwsubscode ");
			//builder.append(" AND cstb.stbcafno=  :stbcafno ");
			builder.append(" AND cstb.parentcafno = c.cafno ");
			builder.append(" AND cs.cafno = :cafno ");
			builder.append(" AND CURRENT_DATE BETWEEN s.effectivefrom AND s.effectiveto ");
			builder.append(" AND CURRENT_DATE BETWEEN cor.effectivefrom AND cor.effectiveto ");
			builder.append(" AND cor.srvccode = '"+IPTV_SERVICE_CODE+"' " );
			
			query= builder.toString();
			LOGGER.info("query for  pack termination "+query);
			qry=getEntityManager().createNativeQuery(query);
			
			qry.setParameter("prodcode", prodCode);
			qry.setParameter("nwsubscode", nwSubsCode);
			//qry.setParameter("stbcafno", stbCafNo);
			qry.setParameter("cafno", cafNo);
			
			list = (List<Object[]>)qry.getResultList();
			
			for(Object[] object : list){
				dto.setProdcafno(object[0] == null ? "" : object[0].toString());
				dto.setAcctcafno(object[1] == null ? "" : object[1].toString());
				dto.setCustid(object[2] == null ? "" : object[2].toString());				
				dto.setProdcode(object[3] == null ? "" : object[3].toString());
				dto.setSrvccodeaddl(object[4] == null ? "" : object[4].toString());
				dto.setSrvccodeprov(object[5] == null ? "" : object[5].toString());
				dto.setNwSubscode(object[6] == null ? "" : object[6].toString());
				dto.setStbcafno(object[7] == null ? "" : object[7].toString());
				
				pkgTerList.add(dto);
				}
			LOGGER.info("commonDAO:::::::::: getPackageTerminationList() ::: END:::");
		}
		catch(Exception e){
			LOGGER.error("Error in getPackageTerminationList()"+ e);
			throw e;			
		}
		finally{
			list=null;
			qry=null;
			query=null;
		}
		return pkgTerList;

	}
	
	// added 01/05/2017 for ONU Reboot
	@SuppressWarnings("unchecked")
	public List<ONURebootDTO> ONURebootList(long cafNo){
		String qry=null;
		Query query= null;
		ONURebootDTO dto=new ONURebootDTO();
		StringBuilder builder=new StringBuilder();
		List<Object[]> list=null;
		List<ONURebootDTO> dtoList = new ArrayList<ONURebootDTO>();
		try{
			LOGGER.info("in side listOfRecords() :: START");
			
			builder.append(" SELECT olt.pop_olt_ipaddress,caf.olt_cardno,caf.olt_portid +8  olt_portid ,caf.olt_onuid ");
			builder.append(" FROM cafs caf , oltmaster olt ");
			builder.append(" WHERE caf.olt_id = olt.pop_olt_serialno AND caf.cafno=? ");
			qry = builder.toString();
			LOGGER.info(qry);
			
			query=getEntityManager().createNativeQuery(qry);			
			query.setParameter(1, cafNo);
			
			list=(List<Object[]>)query.getResultList();
			
			for( Object[] obj : list){
				dto.setIpAddress(obj[0] == null ? "" : obj[0].toString());
				dto.setCard(obj[1] == null ? "" : obj[1].toString());
				dto.setTp(obj[2] == null ? ""  : obj[2].toString());
				dto.setOnuId(obj[3] == null ? "" : obj[3].toString());
				dtoList.add(dto);				
			}			
		}
		catch (Exception e) {
			LOGGER.error("ERROR in listOfRecords() ::: CommonDAO" + e);
		} finally {
			qry = null;
			query = null;
			list = null;
		}
		return dtoList;		
	}
	
	/*@SuppressWarnings("unchecked")
    @Transactional
    public boolean isProvRequestsExist(long cafNo) {
        String qry = null;
        Query query = null;
        boolean flag=false;
        List<String> objList = null;
        try {
            LOGGER.info("Checking for pro requests exist or not");
            qry = "select acctcafno from olprovrequests where status != 9 and acctcafno =:cafNo";
            query = getEntityManager().createNativeQuery(qry);
            query.setParameter("cafNo", cafNo);
            objList = query.getResultList();
            if(!objList.isEmpty()){
                flag=true;
            }
        }
        catch(Exception ex) {
            LOGGER.error("ERROR::Checking for pro requests exist or not" + ex);
        }
        finally {
            query = null;
            qry = null;
            objList = null;
        }
        return flag;
    }*/
}
