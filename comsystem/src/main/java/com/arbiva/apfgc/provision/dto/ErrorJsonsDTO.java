package com.arbiva.apfgc.provision.dto;

public class ErrorJsonsDTO {
	
	private String req;
	private String resp;
	private String createdDate;
	private String executedDate;
	private String status;
	
	public String getReq() {
		return req;
	}
	public void setReq(String req) {
		this.req = req;
	}
	public String getResp() {
		return resp;
	}
	public void setResp(String resp) {
		this.resp = resp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getExecutedDate() {
		return executedDate;
	}
	public void setExecutedDate(String executedDate) {
		this.executedDate = executedDate;
	}
	
	
	
}
