package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AIDDTO {

	private static final long serialVersionUID = 1L;

	public AIDDTO() {
	
	}
	
	@JsonProperty("ipAddress")
	private String ipAddress;
	
	@JsonProperty("card")
	private String card;
	
	@JsonProperty("tp")
	private String tp;
	
	@JsonProperty("onuId")
	private String onuId;

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the card
	 */
	public String getCard() {
		return card;
	}

	/**
	 * @param card the card to set
	 */
	public void setCard(String card) {
		this.card = card;
	}

	/**
	 * @return the tp
	 */
	public String getTp() {
		return tp;
	}

	/**
	 * @param tp the tp to set
	 */
	public void setTp(String tp) {
		this.tp = tp;
	}

	/**
	 * @return the onuId
	 */
	public String getOnuId() {
		return onuId;
	}

	/**
	 * @param onuId the onuId to set
	 */
	public void setOnuId(String onuId) {
		this.onuId = onuId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
