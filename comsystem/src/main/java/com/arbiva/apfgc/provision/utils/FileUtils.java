package com.arbiva.apfgc.provision.utils;

import java.io.FileWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.arbiva.apfgc.provision.Exception.COMSException;
import com.arbiva.apfgc.provision.dto.COMSErrorMessageDTO;
import com.arbiva.apfgc.provision.utils.COMSErrorCode.COMSErrorCodes;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * {@link FileUtils} holding the application specific constants.
 * 
 * @author srinivasa
 *
 */
public class FileUtils {
	
	private static final Logger LOGGER = Logger.getLogger(FileUtils.class);
	
	
	/**
	 * 
	 * @param content
	 * @param filePath
	 */
	public static void write(String content, String filePath) {
		if(StringUtils.isBlank(filePath)) {
			throw new COMSException(new COMSErrorMessageDTO(COMSErrorCodes.GAE002, "File path should not be null"));
		}
		try (FileWriter fileWritter = new FileWriter(filePath)) {
			fileWritter.write(content);
			fileWritter.flush();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(String.format("Error occured while creating invoice json file for account number:"), e);
		}
	}

	/**
	 * 
	 * @param json
	 * @param filePath
	 */
	public static void write(Object json, String filePath) {
		if(StringUtils.isBlank(filePath)) {
			throw new COMSException(new COMSErrorMessageDTO(COMSErrorCodes.GAE002, "File path should not be null"));
		}
		try (FileWriter fileWritter = new FileWriter(filePath)) {
			fileWritter.write(new ObjectMapper().writerWithDefaultPrettyPrinter()
					.writeValueAsString(json));
			fileWritter.flush();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(String.format("Error occured while creating invoice json file for account number:"), e);
		}
	}
	
}
