package com.arbiva.apfgc.provision.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AgoraGPONActivationDTO {

	private static final long serialVersionUID = 1L;

	public AgoraGPONActivationDTO() {
	
	}
	
	private AIDDTO aid;
	
	private List<TPSDTO> tps;
	
	@JsonProperty("admin")
	private String admin;
	
	@JsonProperty("networkServiceName")
	private String networkServiceName;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("nativeVlan")
	private String nativeVlan;
	
	

	/**
	 * @return the nativeVlan
	 */
	public String getNativeVlan() {
		return nativeVlan;
	}

	/**
	 * @param nativeVlan the nativeVlan to set
	 */
	public void setNativeVlan(String nativeVlan) {
		this.nativeVlan = nativeVlan;
	}

	/**
	 * @return the aid
	 */
	public AIDDTO getAid() {
		return aid;
	}

	/**
	 * @param aid the aid to set
	 */
	public void setAid(AIDDTO aid) {
		this.aid = aid;
	}

	/**
	 * @return the tps
	 */
	public List<TPSDTO> getTps() {
		return tps;
	}

	/**
	 * @param tps the tps to set
	 */
	public void setTps(List<TPSDTO> tps) {
		this.tps = tps;
	}

	/**
	 * @return the admin
	 */
	public String getAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(String admin) {
		this.admin = admin;
	}

	/**
	 * @return the networkServiceName
	 */
	public String getNetworkServiceName() {
		return networkServiceName;
	}

	/**
	 * @param networkServiceName the networkServiceName to set
	 */
	public void setNetworkServiceName(String networkServiceName) {
		this.networkServiceName = networkServiceName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
