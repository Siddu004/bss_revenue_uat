package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SubRegResponseDTO {

	private static final long serialVersionUID = 1L;

	public SubRegResponseDTO() {

	}

	@JsonProperty("responseStatus")
	private SubRegResponseStatusDTO responseStatus;

	@JsonProperty("subscribercode")
	private String subscribercode;

	/**
	 * @return the responseStatus
	 */
	public SubRegResponseStatusDTO getResponseStatus() {
		return responseStatus;
	}

	/**
	 * @param responseStatus
	 *            the responseStatus to set
	 */
	public void setResponseStatus(SubRegResponseStatusDTO responseStatus) {
		this.responseStatus = responseStatus;
	}

	/**
	 * @return the subscribercode
	 */
	public String getSubscribercode() {
		return subscribercode;
	}

	/**
	 * @param subscribercode
	 *            the subscribercode to set
	 */
	public void setSubscribercode(String subscribercode) {
		this.subscribercode = subscribercode;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
