package com.arbiva.apfgc.provision.dto;

import java.math.BigDecimal;
import java.util.List;
import com.arbiva.apfgc.provision.utils.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CAFOutputDTO {

	private static final long serialVersionUID = 1L;

	public CAFOutputDTO() {

	}

	@JsonProperty("aadhar_num")
	private String aadharno;

	@JsonProperty("reg_id")
	private String registrationid;

	@JsonProperty("caf_num")
	private String cafno;

	@JsonProperty("title")
	private String title;

	@JsonProperty("first_nm")
	private String fname;

	@JsonProperty("mid_nm")
	private String mname;

	@JsonProperty("last_nm")
	private String lname;

	@JsonProperty("bstp_purchase")
	private String bundledservicepackage;

	@JsonProperty("atp_purchase")
	private String additionalpackage;
	
	@JsonProperty("otp_purchase")
	private String ontimepackage;

	@JsonProperty("fac_pur_in_tele")
	private String facilitiespurchasesintelephone;

	@JsonProperty("contact_person")
	private String contactperson;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("email")
	private String email;

	@JsonProperty("cpe_ser_num")
	private String cpeserialno;

	@JsonProperty("cpe_model")
	private String cpemodel;

	@JsonProperty("cpe_telephone")
	private String telephoneallocated;
	
	@JsonProperty("addr_1")
	private String instaddr1;
	
	@JsonProperty("addr_2")
	private String instaddr2;
	
	@JsonProperty("locality")
	private String instlocality;
	
	@JsonProperty("area")
	private String instarea;
	
	@JsonProperty("city")
	private String instcity;
	
	@JsonProperty("mandal")
	private String instmandal;

	@JsonProperty("district")
	private String instdistrict;

	@JsonProperty("pin_code")
	private String instpincode;

	@JsonProperty("mob_num_1")
	private String instmob1;

	@JsonProperty("mob_num_2")
	private String instmob2;

	@JsonProperty("landline")
	private String instlandline;

	@JsonProperty("poi_num")
	private String poi;

	@JsonProperty("poa_num")
	private String poa;

	@JsonProperty("payment")
	private String paymentresponsible;

	@JsonSerialize(using = MoneySerializer.class)
	private BigDecimal amountpaid = BigDecimal.ZERO;

	@JsonProperty("mode")
	private String mode;

	@JsonSerialize(using = MoneySerializer.class)
	private BigDecimal amount = BigDecimal.ZERO;
	
	@JsonSerialize(using = MoneySerializer.class)
	private BigDecimal totalAmount = BigDecimal.ZERO;

	@JsonProperty("date")
	private String date;

	@JsonProperty("dd_num")
	private String ddno;

	@JsonProperty("drawn_on")
	private String bankdrawn;

	@JsonProperty("branch")
	private String branch;

	@JsonProperty("lmo_nm")
	private String lmoname;

	@JsonProperty("lmo_contact_num")
	private String lmocontactnumber;

	@JsonProperty("lmo_address")
	private String lmoaddress;
	
	@JsonProperty("custid_no")
	private String custidno;
	
	@JsonProperty("custid_no")
	private String tenantname;
	
	@JsonProperty("regoff_pocmob")
	private String regoff_pocmob1;
	
	@JsonProperty("regoff_addr")
	private String regoff_addr1;

	@JsonProperty("stbmacaddr")
	private String stbmacaddr;  
	
	@JsonProperty("stdcode")
	private String stdcode; 
	
	@JsonProperty("landline1")
	private String landline1; 
	
	@JsonProperty("custtypelov")
	private String custtypelov; 
	
	@JsonProperty("stbleaseyn")
	private String stbleaseyn; 
	
	@JsonProperty("stbslno")
	private String stbslno; 
	
	@JsonProperty("stbslno")
	private String stbModel; 
	
	@JsonProperty("cpeleaseyn")
	private String cpeleaseyn; 
	
	@JsonProperty("cpemacaddr")
	private String cpemacaddr; 
	
	@JsonProperty("chargename")
	private String chargename; 
	
	@JsonSerialize(using = MoneySerializer.class)
	private BigDecimal chargeamt = BigDecimal.ZERO;
	
	@JsonProperty("billfreqlov")
	private String billfreqlov; 
	
	@JsonProperty("chargeList")
	private List<ChargeDetailsDTO> chargeList;
	
	@JsonProperty("subscriptionList")
	private List<ChargeDetailsDTO> subscriptionList;
	
	@JsonSerialize(using = MoneySerializer.class)
	private BigDecimal totalSubscriptionAmount = BigDecimal.ZERO;
	
	@JsonProperty("stbList")
	private List<StbDTO> stbList;
	

	public List<StbDTO> getStbList() {
		return stbList;
	}

	public void setStbList(List<StbDTO> stbList) {
		this.stbList = stbList;
	}

	public String getOntimepackage() {
		return ontimepackage;
	}

	public void setOntimepackage(String ontimepackage) {
		this.ontimepackage = ontimepackage;
	}

	public BigDecimal getTotalSubscriptionAmount() {
		totalSubscriptionAmount = BigDecimal.ZERO;
		if(subscriptionList != null && !subscriptionList.isEmpty()){
			for (ChargeDetailsDTO chargeDetailsDTO : subscriptionList) {
				totalSubscriptionAmount = totalSubscriptionAmount.add(chargeDetailsDTO.getChargeamt());
			}
		}
		return totalSubscriptionAmount;
	}

	public void setTotalSubscriptionAmount(BigDecimal totalSubscriptionAmount) {
		this.totalSubscriptionAmount = totalSubscriptionAmount;
	}

	public List<ChargeDetailsDTO> getSubscriptionList() {
		return subscriptionList;
	}

	public void setSubscriptionList(List<ChargeDetailsDTO> subscriptionList) {
		this.subscriptionList = subscriptionList;
	}

	public String getStbmacaddr() {
		return stbmacaddr;
	}

	public void setStbmacaddr(String stbmacaddr) {
		this.stbmacaddr = stbmacaddr;
	}
	
	public String getStdcode() {
		return stdcode;
	}

	public void setStdcode(String stdcode) {
		this.stdcode = stdcode;
	}

	public String getLandline1() {
		return landline1;
	}

	public void setLandline1(String landline1) {
		this.landline1 = landline1;
	}
	
	public String getCusttypelov() {
		return custtypelov;
	}

	public void setCusttypelov(String custtypelov) {
		this.custtypelov = custtypelov;
	}
	
	public String getStbleaseyn() {
		return stbleaseyn;
	}

	public void setStbleaseyn(String stbleaseyn) {
		this.stbleaseyn = stbleaseyn;
	}
	
	public String getStbslno() {
		return stbslno;
	}

	public void setStbslno(String stbslno) {
		this.stbslno = stbslno;
	}

	public String getChargename() {
		return chargename;
	}

	public void setChargename(String chargename) {
		this.chargename = chargename;
	}
	
	public BigDecimal getChargeamt() {
		return chargeamt;
	}

	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}

	public String getBillfreqlov() {
		return billfreqlov;
	}

	public void setBillfreqlov(String billfreqlov) {
		this.billfreqlov = billfreqlov;
	}
	

	public List<ChargeDetailsDTO> getChargeList() {
		return chargeList;
	}

	public void setChargeList(List<ChargeDetailsDTO> chargeList) {
		this.chargeList = chargeList;
	}

	public String getCustidno() {
		return custidno;
	}

	public void setCustidno(String custidno) {
		this.custidno = custidno;
	}

	public String getTenantname() {
		return tenantname;
	}

	public void setTenantname(String tenantname) {
		this.tenantname = tenantname;
	}

	public String getRegoff_pocmob1() {
		return regoff_pocmob1;
	}

	public void setRegoff_pocmob1(String regoff_pocmob1) {
		this.regoff_pocmob1 = regoff_pocmob1;
	}

	public String getRegoff_addr1() {
		return regoff_addr1;
	}

	public void setRegoff_addr1(String regoff_addr1) {
		this.regoff_addr1 = regoff_addr1;
	}

	/**
	 * @return the aadharno
	 */
	public String getAadharno() {
		return aadharno;
	}

	/**
	 * @param aadharno
	 *            the aadharno to set
	 */
	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}

	/**
	 * @return the registrationid
	 */
	public String getRegistrationid() {
		return registrationid;
	}

	/**
	 * @param registrationid
	 *            the registrationid to set
	 */
	public void setRegistrationid(String registrationid) {
		this.registrationid = registrationid;
	}

	/**
	 * @return the cafno
	 */
	public String getCafno() {
		return cafno;
	}

	/**
	 * @param cafno
	 *            the cafno to set
	 */
	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname
	 *            the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the mname
	 */
	public String getMname() {
		return mname;
	}

	/**
	 * @param mname
	 *            the mname to set
	 */
	public void setMname(String mname) {
		this.mname = mname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname
	 *            the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the bundledservicepackage
	 */
	public String getBundledservicepackage() {
		return bundledservicepackage;
	}

	/**
	 * @param bundledservicepackage
	 *            the bundledservicepackage to set
	 */
	public void setBundledservicepackage(String bundledservicepackage) {
		this.bundledservicepackage = bundledservicepackage;
	}

	/**
	 * @return the additionalpackage
	 */
	public String getAdditionalpackage() {
		return additionalpackage;
	}

	/**
	 * @param additionalpackage
	 *            the additionalpackage to set
	 */
	public void setAdditionalpackage(String additionalpackage) {
		this.additionalpackage = additionalpackage;
	}

	/**
	 * @return the facilitiespurchasesintelephone
	 */
	public String getFacilitiespurchasesintelephone() {
		return facilitiespurchasesintelephone;
	}

	/**
	 * @param facilitiespurchasesintelephone
	 *            the facilitiespurchasesintelephone to set
	 */
	public void setFacilitiespurchasesintelephone(
			String facilitiespurchasesintelephone) {
		this.facilitiespurchasesintelephone = facilitiespurchasesintelephone;
	}

	/**
	 * @return the contactperson
	 */
	public String getContactperson() {
		return contactperson;
	}

	/**
	 * @param contactperson
	 *            the contactperson to set
	 */
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the cpeserialno
	 */
	public String getCpeserialno() {
		return cpeserialno;
	}

	/**
	 * @param cpeserialno
	 *            the cpeserialno to set
	 */
	public void setCpeserialno(String cpeserialno) {
		this.cpeserialno = cpeserialno;
	}

	/**
	 * @return the cpemodel
	 */
	public String getCpemodel() {
		return cpemodel;
	}

	/**
	 * @param cpemodel
	 *            the cpemodel to set
	 */
	public void setCpemodel(String cpemodel) {
		this.cpemodel = cpemodel;
	}

	/**
	 * @return the telephoneallocated
	 */
	public String getTelephoneallocated() {
		return telephoneallocated;
	}

	/**
	 * @param telephoneallocated
	 *            the telephoneallocated to set
	 */
	public void setTelephoneallocated(String telephoneallocated) {
		this.telephoneallocated = telephoneallocated;
	}

	/**
	 * @return the instaddr1
	 */
	public String getInstaddr1() {
		return instaddr1;
	}

	/**
	 * @param instaddr1
	 *            the instaddr1 to set
	 */
	public void setInstaddr1(String instaddr1) {
		this.instaddr1 = instaddr1;
	}

	/**
	 * @return the instaddr2
	 */
	public String getInstaddr2() {
		return instaddr2;
	}

	/**
	 * @param instaddr2
	 *            the instaddr2 to set
	 */
	public void setInstaddr2(String instaddr2) {
		this.instaddr2 = instaddr2;
	}

	/**
	 * @return the instlocality
	 */
	public String getInstlocality() {
		return instlocality;
	}

	/**
	 * @param instlocality
	 *            the instlocality to set
	 */
	public void setInstlocality(String instlocality) {
		this.instlocality = instlocality;
	}

	/**
	 * @return the instarea
	 */
	public String getInstarea() {
		return instarea;
	}

	/**
	 * @param instarea
	 *            the instarea to set
	 */
	public void setInstarea(String instarea) {
		this.instarea = instarea;
	}

	/**
	 * @return the instcity
	 */
	public String getInstcity() {
		return instcity;
	}

	/**
	 * @param instcity
	 *            the instcity to set
	 */
	public void setInstcity(String instcity) {
		this.instcity = instcity;
	}

	/**
	 * @return the instmandal
	 */
	public String getInstmandal() {
		return instmandal;
	}

	/**
	 * @param instmandal
	 *            the instmandal to set
	 */
	public void setInstmandal(String instmandal) {
		this.instmandal = instmandal;
	}

	/**
	 * @return the instdistrict
	 */
	public String getInstdistrict() {
		return instdistrict;
	}

	/**
	 * @param instdistrict
	 *            the instdistrict to set
	 */
	public void setInstdistrict(String instdistrict) {
		this.instdistrict = instdistrict;
	}

	/**
	 * @return the instpincode
	 */
	public String getInstpincode() {
		return instpincode;
	}

	/**
	 * @param instpincode
	 *            the instpincode to set
	 */
	public void setInstpincode(String instpincode) {
		this.instpincode = instpincode;
	}

	/**
	 * @return the instmob1
	 */
	public String getInstmob1() {
		return instmob1;
	}

	/**
	 * @param instmob1
	 *            the instmob1 to set
	 */
	public void setInstmob1(String instmob1) {
		this.instmob1 = instmob1;
	}

	/**
	 * @return the instmob2
	 */
	public String getInstmob2() {
		return instmob2;
	}

	/**
	 * @param instmob2
	 *            the instmob2 to set
	 */
	public void setInstmob2(String instmob2) {
		this.instmob2 = instmob2;
	}

	/**
	 * @return the instlandline
	 */
	public String getInstlandline() {
		return instlandline;
	}

	/**
	 * @param instlandline
	 *            the instlandline to set
	 */
	public void setInstlandline(String instlandline) {
		this.instlandline = instlandline;
	}

	/**
	 * @return the poi
	 */
	public String getPoi() {
		return poi;
	}

	/**
	 * @param poi
	 *            the poi to set
	 */
	public void setPoi(String poi) {
		this.poi = poi;
	}

	/**
	 * @return the poa
	 */
	public String getPoa() {
		return poa;
	}

	/**
	 * @param poa
	 *            the poa to set
	 */
	public void setPoa(String poa) {
		this.poa = poa;
	}

	/**
	 * @return the paymentresponsible
	 */
	public String getPaymentresponsible() {
		return paymentresponsible;
	}

	/**
	 * @param paymentresponsible
	 *            the paymentresponsible to set
	 */
	public void setPaymentresponsible(String paymentresponsible) {
		this.paymentresponsible = paymentresponsible;
	}

	public BigDecimal getAmountpaid() {
		return amountpaid;
	}

	public void setAmountpaid(BigDecimal amountpaid) {
		this.amountpaid = amountpaid;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the ddno
	 */
	public String getDdno() {
		return ddno;
	}

	/**
	 * @param ddno
	 *            the ddno to set
	 */
	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	/**
	 * @return the bankdrawn
	 */
	public String getBankdrawn() {
		return bankdrawn;
	}

	/**
	 * @param bankdrawn
	 *            the bankdrawn to set
	 */
	public void setBankdrawn(String bankdrawn) {
		this.bankdrawn = bankdrawn;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch
	 *            the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the lmoname
	 */
	public String getLmoname() {
		return lmoname;
	}

	/**
	 * @param lmoname
	 *            the lmoname to set
	 */
	public void setLmoname(String lmoname) {
		this.lmoname = lmoname;
	}

	/**
	 * @return the lmocontactnumber
	 */
	public String getLmocontactnumber() {
		return lmocontactnumber;
	}

	/**
	 * @param lmocontactnumber
	 *            the lmocontactnumber to set
	 */
	public void setLmocontactnumber(String lmocontactnumber) {
		this.lmocontactnumber = lmocontactnumber;
	}

	/**
	 * @return the lmoaddress
	 */
	public String getLmoaddress() {
		return lmoaddress;
	}

	/**
	 * @param lmoaddress
	 *            the lmoaddress to set
	 */
	public void setLmoaddress(String lmoaddress) {
		this.lmoaddress = lmoaddress;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getTotalAmount() {
		totalAmount = BigDecimal.ZERO;
		if(chargeList != null && !chargeList.isEmpty()){
			for (ChargeDetailsDTO chargeDetailsDTO : chargeList) {
				totalAmount = totalAmount.add(chargeDetailsDTO.getChargeamt());
			}
		}
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStbModel() {
		return stbModel;
	}

	public void setStbModel(String stbModel) {
		this.stbModel = stbModel;
	}

	public String getCpeleaseyn() {
		return cpeleaseyn;
	}

	public void setCpeleaseyn(String cpeleaseyn) {
		this.cpeleaseyn = cpeleaseyn;
	}

	public String getCpemacaddr() {
		return cpemacaddr;
	}

	public void setCpemacaddr(String cpemacaddr) {
		this.cpemacaddr = cpemacaddr;
	}
	
	

}
