package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IGMPOptionsDTO {

	private static final long serialVersionUID = 1L;

	public IGMPOptionsDTO() {
	
	}
	
	@JsonProperty("useGlobal")
	private boolean useGlobal;
	
	@JsonProperty("enable")
	private boolean enable;

	/**
	 * @return the useGlobal
	 */
	public boolean isUseGlobal() {
		return useGlobal;
	}

	/**
	 * @param useGlobal the useGlobal to set
	 */
	public void setUseGlobal(boolean useGlobal) {
		this.useGlobal = useGlobal;
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * @param enable the enable to set
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
