package com.arbiva.apfgc.provision.utils;

import java.text.SimpleDateFormat;

public class TestDate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String fromDate = "2016-02-01";
			String toDate = "2016-03-10";
			System.out.println(formatter.parse(fromDate));
			System.out.println(formatter.parse(toDate));
		}
		catch(Exception ex) {
			System.out.println(ex);
		}
	}

}
