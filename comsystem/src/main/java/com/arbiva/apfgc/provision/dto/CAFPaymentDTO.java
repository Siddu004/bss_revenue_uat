package com.arbiva.apfgc.provision.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author srinivasa
 *
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CAFPaymentDTO {

	private static final long serialVersionUID = 1L;

	public CAFPaymentDTO() {

	}

	@JsonProperty("tot_amt_paid")
	private String amountpaid;

	@JsonProperty("mode")
	private String mode;

	@JsonProperty("amount")
	private String amount;

	@JsonProperty("date")
	private String date;

	@JsonProperty("dd_num")
	private String ddno;

	@JsonProperty("drawn_on")
	private String bankdrawn;

	@JsonProperty("branch")
	private String branch;

	/**
	 * @return the amountpaid
	 */
	public String getAmountpaid() {
		return amountpaid;
	}

	/**
	 * @param amountpaid the amountpaid to set
	 */
	public void setAmountpaid(String amountpaid) {
		this.amountpaid = amountpaid;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the ddno
	 */
	public String getDdno() {
		return ddno;
	}

	/**
	 * @param ddno the ddno to set
	 */
	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	/**
	 * @return the bankdrawn
	 */
	public String getBankdrawn() {
		return bankdrawn;
	}

	/**
	 * @param bankdrawn the bankdrawn to set
	 */
	public void setBankdrawn(String bankdrawn) {
		this.bankdrawn = bankdrawn;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
