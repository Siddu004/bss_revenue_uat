package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AgoraRegisterDTO {

	public String getFec() {
		return fec;
	}

	public void setFec(String fec) {
		this.fec = fec;
	}

	private static final long serialVersionUID = 1L;

	public AgoraRegisterDTO() {
	
	}
	
	private AIDDTO aid;
	
	@JsonProperty("admin")
	private String admin;
	
	@JsonProperty("fec")
	private String fec;
	
	@JsonProperty("swUpgradeMode")
	private String swUpgradeMode;

	@JsonProperty("serialNumber")
	private String serialNumber;
	
	@JsonProperty("profileName")
	private String profileName;
	
	@JsonProperty("registerType")
	private String registerType;
	
	@JsonProperty("name")
	private String name;
	

	/**
	 * @return the aid
	 */
	public AIDDTO getAid() {
		return aid;
	}

	/**
	 * @param aid the aid to set
	 */
	public void setAid(AIDDTO aid) {
		this.aid = aid;
	}

	/**
	 * @return the admin
	 */
	public String getAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(String admin) {
		this.admin = admin;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * @return the registerType
	 */
	public String getRegisterType() {
		return registerType;
	}

	/**
	 * @param registerType the registerType to set
	 */
	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getSwUpgradeMode() {
		return swUpgradeMode;
	}

	public void setSwUpgradeMode(String swUpgradeMode) {
		this.swUpgradeMode = swUpgradeMode;
	}
	
	
	
}
