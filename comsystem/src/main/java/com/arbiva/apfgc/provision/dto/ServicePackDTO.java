package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServicePackDTO {

	private static final long serialVersionUID = 1L;

	public ServicePackDTO() {

	}

	@JsonProperty("servicepack")
	private String servicepack;

	@JsonProperty("expirydate")
	private String expirydate;
	
	@JsonProperty("reason")
	private String reason;

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the servicepack
	 */
	public String getServicepack() {
		return servicepack;
	}

	/**
	 * @param servicepack
	 *            the servicepack to set
	 */
	public void setServicepack(String servicepack) {
		this.servicepack = servicepack;
	}

	/**
	 * @return the expirydate
	 */
	public String getExpirydate() {
		return expirydate;
	}

	/**
	 * @param expirydate
	 *            the expirydate to set
	 */
	public void setExpirydate(String expirydate) {
		this.expirydate = expirydate;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
