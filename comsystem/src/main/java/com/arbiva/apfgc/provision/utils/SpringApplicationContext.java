package com.arbiva.apfgc.provision.utils;

import javax.ws.rs.core.Context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * This singleton encapsulates access to the beans registered in
 * applicationContext.xml
 */
@Component("springApplicationContext")
public class SpringApplicationContext implements ApplicationContextAware {
	@Context
	private static ApplicationContext appContext;

	// Private constructor prevents instantiation from other classes
	private SpringApplicationContext() {
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		appContext = applicationContext;
	}

	public static Object getBean(String beanName) {
		return appContext.getBean(beanName);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object getBean(Class beanType) {
		return appContext.getBean(beanType);
	}

	public static Object getBean(String beanName, Object... args) {
		return appContext.getBean(beanName, args);
	}
}