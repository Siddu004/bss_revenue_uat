package com.arbiva.apfgc.provision.dto;

public class PendingProvisonErrorsDTO {

	private String createdDate;
	private String executedDate;
	private String req;
	private String requestid;
	private String resp;
	private String status;

	public String getCreatedDate() {
		return createdDate;
	}

	public String getExecutedDate() {
		return executedDate;
	}

	public String getReq() {
		return req;
	}

	public String getRequestid() {
		return requestid;
	}

	public String getResp() {
		return resp;
	}

	public String getStatus() {
		return status;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setExecutedDate(String executedDate) {
		this.executedDate = executedDate;
	}

	public void setReq(String req) {
		this.req = req;
	}

	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	public void setResp(String resp) {
		this.resp = resp;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
