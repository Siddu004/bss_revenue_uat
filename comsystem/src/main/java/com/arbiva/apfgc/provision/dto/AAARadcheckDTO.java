package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AAARadcheckDTO {

	private static final long serialVersionUID = 1L;

	public AAARadcheckDTO() {
	
	}
	
	private AIDDTO aid;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("password")
	private String password;

	/**
	 * @return the aid
	 */
	public AIDDTO getAid() {
		return aid;
	}

	/**
	 * @param aid the aid to set
	 */
	public void setAid(AIDDTO aid) {
		this.aid = aid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
