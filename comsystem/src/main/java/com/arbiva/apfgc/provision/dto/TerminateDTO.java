package com.arbiva.apfgc.provision.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class TerminateDTO {
	private static final long serialVersionUID = 1L;

	public TerminateDTO() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("subscriberCodes")
	private List<String> subscriberCodes;


	public List<String> getSubscriberCodes() {
		return subscriberCodes;
	}


	public void setSubscriberCodes(List<String> subscriberCodes) {
		this.subscriberCodes = subscriberCodes;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
