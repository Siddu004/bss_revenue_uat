package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SubscriberRegistrationDTO {

	private static final long serialVersionUID = 1L;

	public SubscriberRegistrationDTO() {
	
	}
	
	private String deviceid;
	
	private String devicecategory;
		
	private CustomerDTO subscriber;

	/**
	 * @return the deviceid
	 */
	public String getDeviceid() {
		return deviceid;
	}

	/**
	 * @param deviceid the deviceid to set
	 */
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	/**
	 * @return the devicecategory
	 */
	public String getDevicecategory() {
		return devicecategory;
	}

	/**
	 * @param devicecategory the devicecategory to set
	 */
	public void setDevicecategory(String devicecategory) {
		this.devicecategory = devicecategory;
	}

	/**
	 * @return the subscriber
	 */
	public CustomerDTO getSubscriber() {
		return subscriber;
	}

	/**
	 * @param subscriber the subscriber to set
	 */
	public void setSubscriber(CustomerDTO subscriber) {
		this.subscriber = subscriber;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
