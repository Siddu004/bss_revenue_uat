package com.arbiva.apfgc.provision.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.arbiva.apfgc.provision.Exception.COMSException;
import com.arbiva.apfgc.provision.dto.COMSErrorMessageDTO;
import com.arbiva.apfgc.provision.utils.COMSErrorCode.COMSErrorCodes;

/**
 * Helper class to maintain helper methods that are used many times across
 * services.
 * 
 * @author srinivasa
 *
 */
@Component("serviceUtility")
public class ServiceUtility {

	//private static Logger log = LoggerFactory.getLogger(ServiceUtility.class
			//.getName());


	/**
	 * 
	 * @return
	 */
	public static Date getCurrentTime() {
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);
		return now.getTime();
	}
	
	/**
	 * 
	 * @param value
	 * @param type
	 */
	public static void validateCode(String type, String value) {
			Pattern p = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(value);
			try {
				if (m.find()) {
					throw new COMSException(new COMSErrorMessageDTO(COMSErrorCodes.GAE005, String.format(
							"%s: %s should not contains any special characters", type, value)));
				}
			} catch (Exception e) {
				throw e;
			} finally {
				p = null;
				m = null;
				value = null;
				type = null;
			}
	}
}
