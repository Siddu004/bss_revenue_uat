package com.arbiva.apfgc.provision.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Entity
public class ProvisionInfoDTO {

	private static final long serialVersionUID = 1L;
	
	

	@Column(name="olt_id")
	private String oltid;
	
	@Column(name="olt_portid")
	private String portid;
	
	@Column(name="olt_onuid")
	private String onuid;
	
	@Column(name="cpe_profilename")
	private String model;
	
	@Column(name="cpeslno")
	private String cpeslno;
	
	@Column(name="srvccodeprov")
	private String srvccodeprov;
	
	@Column(name="coresrvccode")
	private String coresrvccode;
	
	@Column(name="prodcafno")
	private String prodcafno;
	
	@Column(name="custid")
	private String custid;

	@Column(name="srvccodeaddl")
	private String srvccodeaddl;
	
	@Column(name="prodcode")
	private String prodcode;
	
	@Column(name="pop_olt_ipaddress")
	private String ipaddress;
	
	@Column(name="stbmacaddr")
	private String stbMacAddr;
	
	@Column(name="phonenopwd")
	private String phoneNopwd;
	
	@Column(name="stdcode")
	private String stdCode;
	
	@Column(name="custtypelov")
	private String customerType;
	
	@Column(name="vpnsrvcname")
	private String vpnServiceName;
	
	@Column(name="expdate")
	private String expDate;
	
	@Column(name="acctcafno")
	private String acctcafno;
	
	@Column(name="stbcafno")
	private String stbcafno;
	
	@Column(name="cpe_modeldetails")
	private String tps;
	
	@Column(name="aaacode")
	private String client;
	
	private String agoraHsiSubsCode;
	
	private String nativeVlan;
	
	private String voipGponFlag;
	
	@Transient
	private Long dependencyRequestId;
	
	@Transient
	private String phoneNo;
	
	@Transient
	private String passwrd;
	
	@Transient
	private String nwSubscode;
		
	protected String cardnum;
	
	/**
	 * @return the voipGponFlag
	 */
	public String getVoipGponFlag() {
		return voipGponFlag;
	}

	/**
	 * @param voipGponFlag the voipGponFlag to set
	 */
	public void setVoipGponFlag(String voipGponFlag) {
		this.voipGponFlag = voipGponFlag;
	}

	/**
	 * @return the nativeVlan
	 */
	public String getNativeVlan() {
		return nativeVlan;
	}

	/**
	 * @param nativeVlan the nativeVlan to set
	 */
	public void setNativeVlan(String nativeVlan) {
		this.nativeVlan = nativeVlan;
	}

	public String getAgoraHsiSubsCode() {
		return agoraHsiSubsCode;
	}

	public void setAgoraHsiSubsCode(String agoraHsiSubsCode) {
		this.agoraHsiSubsCode = agoraHsiSubsCode;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getTps() {
		return tps;
	}

	public void setTps(String tps) {
		this.tps = tps;
	}

	public String getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(String stbcafno) {
		this.stbcafno = stbcafno;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getVpnServiceName() {
		return vpnServiceName;
	}

	public void setVpnServiceName(String vpnServiceName) {
		this.vpnServiceName = vpnServiceName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getStbMacAddr() {
		return stbMacAddr;
	}

	public void setStbMacAddr(String stbMacAddr) {
		this.stbMacAddr = stbMacAddr;
	}

	public Long getDependencyRequestId() {
		return dependencyRequestId;
	}

	public void setDependencyRequestId(Long dependencyRequestId) {
		this.dependencyRequestId = dependencyRequestId;
	}

	/**
	 * @return the ipaddress
	 */
	public String getIpaddress() {
		return ipaddress;
	}

	/**
	 * @param ipaddress the ipaddress to set
	 */
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	/**
	 * @return the portid
	 */
	public String getPortid() {
		return portid;
	}

	/**
	 * @param portid the portid to set
	 */
	public void setPortid(String portid) {
		this.portid = portid;
	}

	/**
	 * @return the onuid
	 */
	public String getOnuid() {
		return onuid;
	}

	/**
	 * @param onuid the onuid to set
	 */
	public void setOnuid(String onuid) {
		this.onuid = onuid;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the cpeslno
	 */
	public String getCpeslno() {
		return cpeslno;
	}

	/**
	 * @param cpeslno the cpeslno to set
	 */
	public void setCpeslno(String cpeslno) {
		this.cpeslno = cpeslno;
	}

	/**
	 * @return the srvccodeprov
	 */
	public String getSrvccodeprov() {
		return srvccodeprov;
	}

	/**
	 * @param srvccodeprov the srvccodeprov to set
	 */
	public void setSrvccodeprov(String srvccodeprov) {
		this.srvccodeprov = srvccodeprov;
	}

	/**
	 * @return the coresrvccode
	 */
	public String getCoresrvccode() {
		return coresrvccode;
	}

	/**
	 * @param coresrvccode the coresrvccode to set
	 */
	public void setCoresrvccode(String coresrvccode) {
		this.coresrvccode = coresrvccode;
	}

	/**
	 * @return the cafno
	 */
	public String getAcctcafno() {
		return acctcafno;
	}

	public String getNwSubscode() {
		return nwSubscode;
	}

	public void setNwSubscode(String nwSubscode) {
		this.nwSubscode = nwSubscode;
	}

	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}

	/**
	 * @return the custid
	 */
	public String getCustid() {
		return custid;
	}

	/**
	 * @param custid the custid to set
	 */
	public void setCustid(String custid) {
		this.custid = custid;
	}

	
	/**
	 * @return the prodcafno
	 */
	public String getProdcafno() {
		return prodcafno;
	}

	/**
	 * @param prodcafno the prodcafno to set
	 */
	public void setProdcafno(String prodcafno) {
		this.prodcafno = prodcafno;
	}

	/**
	 * @return the srvccodeaddl
	 */
	public String getSrvccodeaddl() {
		return srvccodeaddl;
	}

	/**
	 * @param srvccodeaddl the srvccodeaddl to set
	 */
	public void setSrvccodeaddl(String srvccodeaddl) {
		this.srvccodeaddl = srvccodeaddl;
	}

	/**
	 * @return the prodcode
	 */
	public String getProdcode() {
		return prodcode;
	}

	/**
	 * @param prodcode the prodcode to set
	 */
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getOltid() {
		return oltid;
	}

	public void setOltid(String oltid) {
		this.oltid = oltid;
	}

	public String getPasswrd() {
		return passwrd;
	}

	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getPhoneNopwd() {
		return phoneNopwd;
	}

	public void setPhoneNopwd(String phoneNopwd) {
		this.phoneNopwd = phoneNopwd;
	}

	public String getCardnum() {
		return cardnum;
	}

	public void setCardnum(String cardnum) {
		this.cardnum = cardnum;
	}
	
	

	
}
