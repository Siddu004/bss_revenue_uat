package com.arbiva.apfgc.provision.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * @author srinivasa
 *
 */
public class DateUtils {

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static String calculateDateDifferences(Date startDate, Date endDate) {
		long milliSeconds = endDate.getTime() - startDate.getTime();

		long seconds = milliSeconds / 1000 % 60;
		long minutes = milliSeconds / (60 * 1000) % 60;
		long hours = milliSeconds / (60 * 60 * 1000) % 24;
		long days = milliSeconds / (24 * 60 * 60 * 1000);

		return String.format("%sDays %sHours %sMinutes %sSeconds", days, hours,
				minutes, seconds);
	}

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param type
	 * @return
	 */
	public static long calculateDateDifferences(Date startDate, Date endDate,
			String type) {
		long milliSeconds = endDate.getTime() - startDate.getTime();

		switch (type) {
		case "DAYS":
			return milliSeconds / (24 * 60 * 60 * 1000);
		case "HOURS":
			return milliSeconds / (60 * 60 * 1000) % 24;
		case "MINUTES":
			return milliSeconds / (60 * 1000) % 60;
		case "SECONDS":
			return milliSeconds / 1000 % 60;
		}
		return 0;
	}

	/**
	 * 
	 * @param dateInString
	 * @return
	 * @throws Exception 
	 * @throws ParseException
	 */
	public static Date convertStringToDate(String dateInString, String dateFormat) {
		Date date = null;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		try {
			date = format.parse(dateInString);
		} catch (Exception ex) {
			ex.printStackTrace();
			try {
				throw ex;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return date;
	}
	
	public static void main(String[] args) {
		System.out.println(new SimpleDateFormat("ddMMyyyyHHmmss").format(Calendar.getInstance().getTime()));
	}
	
	/**
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String convertDateToString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	public static void reduceXDays(Date d, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DATE, -days);
		d.setTime(c.getTime().getTime());
	}
	
	/**
	 * 
	 * @param ageInYears
	 * @return
	 */
	public static Date calculateDOBFromAge(int ageInYears) {
		Calendar dob = new GregorianCalendar();
		dob.add(Calendar.YEAR, -ageInYears - 1);
		return dob.getTime();
	}
	
	/**
	 * 
	 * @param dob
	 * @return
	 */
	public static int calculateAgeFromDOB(Date dob) {
		 Calendar birth = new GregorianCalendar();
	        birth.setTime(dob);
	        Calendar now = new GregorianCalendar();
	        now.setTime( new Date() );
	        int adjust = 0;
	        if ( now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
	            adjust = -1;
	        }
	        return now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
	}
	
	/**
	 * 
	 * @param dob
	 * @return
	 */
	public static int calculateAgeFromDOB(String dob) {
		Date date = DateUtils.convertStringToDate(dob, COMSConstants.DATE_STRING_PATTERN);
	    Calendar birth = new GregorianCalendar();
        birth.setTime(date);
        Calendar now = new GregorianCalendar();
        now.setTime( new Date() );
        int adjust = 0;
        if ( now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
            adjust = -1;
        }
        return now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
	}
	
	public static String getCurrentYearMonth() {
		return new SimpleDateFormat("YYYY_MMM").format(Calendar.getInstance().getTime());
	}
	
	public static String getCurrentTimeStamp() {
		return new SimpleDateFormat("ddMMyyyyHHmmss").format(Calendar.getInstance().getTime());
	}
	
	
	
	public static void main2(String[] args) {
		int ageInYears = 27;
		Calendar dob = new GregorianCalendar();
		dob.add(Calendar.YEAR, -ageInYears - 1);
		System.out.println(dob.getTime());
	}
	
	public static void main1(String[] args) {
		    Date date = DateUtils.convertStringToDate("10/05/1988", "dd/MM/yyyy");
		    Calendar birth = new GregorianCalendar();
	        birth.setTime(date);
	        Calendar now = new GregorianCalendar();
	        now.setTime( new Date() );
	        int adjust = 0;
	        if ( now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
	            adjust = -1;
	        }
	        int age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
	        System.out.println(age);
	}
	
	
}
