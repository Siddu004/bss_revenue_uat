package com.arbiva.apfgc.provision.businessService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletContextResource;

import com.arbiva.apfgc.provision.dao.CommonDAO;
import com.arbiva.apfgc.provision.dto.ProvisionInfoDTO;
import com.arbiva.apfgc.provision.models.ProvJsons;
import com.arbiva.apfgc.provision.utils.COMSConstants;


/**
 * 
 * @author srinivasa
 *
 */
@Component("telephonyProvBusinessServiceImpl")
public class TelephonyProvBusinessServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(TelephonyProvBusinessServiceImpl.class);

	@Autowired 
	private ServletContext servletContext;
	
	@Autowired
	private CommonDAO commonDAO;		
	
	@Value("${zte.server.url}")
	private String ZTEServerUrl;
	
	@Value("${zte.server.url.sss}")
	private String ZTEServerUrlSSS;
	
	@Value("${zte.service.createpvi.post.calltype}")
	private String ZTECreatePVIPostCallType;
	
	@Value("${zte.service.createpui.post.calltype}")
	private String ZTECreatePUIPostCallType;
	
	@Value("${zte.service.setimpregset.post.calltype}")
	private String ZTESetImpRegSetPostCallType;
	
	@Value("${zte.service.setaliasgrp.post.calltype}")
	private String ZTESetAliasGrpPostCallType;
	
	@Value("${zte.service.addenum.post.calltype}")
	private String ZTEAddEnumPostCallType;
	
	@Value("${zte.service.addtp.post.calltype}")
	private String ZTEAddTpPostCallType;
	
	@Value("${zte.service.addsbr.post.calltype}")
	private String ZTEAddSBRPostCallType;
	
	@Value("${zte.service.addctx.post.calltype}")
	private String ZTEAddCTXPostCallType;
	
	@Value("${zte.service.addctxuser.post.calltype}")
	private String ZTEAddCTXUserPostCallType;
	
	public ProvJsons createPVI(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;		
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/CreatePVI.xml");
			
			//List of Service Parameters required for CreatePVI Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.SECVER);
			lst.add(COMSConstants.PECFN);
			lst.add(COMSConstants.PCCFN);
			lst.add(COMSConstants.SECFN);
			lst.add(COMSConstants.SCCFN);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
			
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<USERNAME>") && line.contains("</USERNAME>")) {
					soapXMLContent.append("<USERNAME>+91"+dto.getPhoneNo()+"@vskp.apsflims.in</USERNAME>");
					keyFound = 1;
				}
				if(line.contains("<PASSWORD>") && line.contains("</PASSWORD>")) {
					soapXMLContent.append("<PASSWORD>"+dto.getPasswrd()+"</PASSWORD>");
					keyFound = 1;
				}
				if(line.contains("<PVI>") && line.contains("</PVI>")){
					soapXMLContent.append("<PVI>+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PVI>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + servicePrmLst.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTECreatePVIPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in createPVI: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
			
		}		
		return provReq;
	}
	
	public ProvJsons createPUI(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;		
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/CreatePUI.xml");
			
			//List of Service Parameters required for CreatePUI Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.BARFLAG);
			lst.add(COMSConstants.SPID);
			lst.add(COMSConstants.ROAMSCHEMEID);
			lst.add(COMSConstants.SCSCFNAMELIST);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	        
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<PUI>") && line.contains("</PUI>")){
					soapXMLContent.append("<PUI>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PUI>");
					keyFound = 1;
				}
				if(line.contains("<PVILIST>") && line.contains("</PVILIST>")){
					soapXMLContent.append("<PVILIST>+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PVILIST>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + servicePrmLst.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTECreatePUIPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in createPUI: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	public ProvJsons createDefaultPUI(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;		
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/CreateDefaultPUI.xml");
			
			//List of Service Parameters required for CreatePUI Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.BARFLAG);
			lst.add(COMSConstants.SPID);
			lst.add(COMSConstants.ROAMSCHEMEID);
			lst.add(COMSConstants.SCSCFNAMELIST);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	        
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<PUI>") && line.contains("</PUI>")){
					soapXMLContent.append("<PUI>tel:+91"+dto.getPhoneNo()+"</PUI>");
					keyFound = 1;
				}
				if(line.contains("<PVILIST>") && line.contains("</PVILIST>")){
					soapXMLContent.append("<PVILIST>+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PVILIST>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + servicePrmLst.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTECreatePUIPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in createPUI: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	public ProvJsons setImpRegSet(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		ServletContextResource resource = null;
		HashMap<String, String> servicePrmLst = new LinkedHashMap<>();		
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/SetImpRegSet.xml");
			
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	        
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<PUILIST>") && line.contains("</PUILIST>")){
					soapXMLContent.append("<PUILIST>tel:+91"+dto.getPhoneNo()+"$sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PUILIST>");
					keyFound = 1;
				}
				if(line.contains("<DEFAULTPUI>") && line.contains("</DEFAULTPUI>")){
					soapXMLContent.append("<DEFAULTPUI>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</DEFAULTPUI>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + servicePrmLst.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTESetImpRegSetPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in setImpRegSet: " + ex);
		}
		finally {
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	public ProvJsons setAliaseGrp(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		//StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		//ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = new LinkedHashMap<>();
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/SetAliaseGrp.xml");

			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<PUILIST>") && line.contains("</PUILIST>")){
					soapXMLContent.append("<PUILIST>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in$tel:+91"+dto.getPhoneNo()+"</PUILIST>");
					keyFound = 1;
				}
				if(line.contains("<ALIASGROUPID>") && line.contains("</ALIASGROUPID>")){
					soapXMLContent.append("<ALIASGROUPID>"+dto.getPhoneNo()+"</ALIASGROUPID>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + servicePrmLst.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTESetAliasGrpPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in setAliaseGrp: " + ex);
		}
		finally {
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	@SuppressWarnings("unused")
	public ProvJsons addSubscriberFeaturesProvReq(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = new LinkedHashMap<>();
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/SubscriberSpecificFeatures.xml");
			//List of Service Parameters required for add Enum Service
			/*lst = new ArrayList<String>();
			lst.add(COMSConstants.ZONENAME);
			lst.add(COMSConstants.ORDER);
			lst.add(COMSConstants.PREFERENCE);
			lst.add(COMSConstants.FLAGS);
			lst.add(COMSConstants.SERVICE);
			lst.add(COMSConstants.REGEXP);*/

			//Prepare Service Parameter List to simple string separated by comma
			//serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			//servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	            
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<m:IMPU>") && line.contains("</m:IMPU>")){
					soapXMLContent.append("<m:IMPU>tel:+91"+dto.getPhoneNo()+"</m:IMPU>");
					keyFound = 1;
				}
				if(line.contains("<m:LATA>") && line.contains("</m:LATA>")){
					soapXMLContent.append("<m:LATA>"+dto.getStdCode()+"</m:LATA>");
					keyFound = 1;
				}
				if(line.contains("<m:AUCCODE>") && line.contains("</m:AUCCODE>")){
					soapXMLContent.append("<m:AUCCODE>"+dto.getStdCode()+"</m:AUCCODE>");
					keyFound = 1;
				}
				if(line.contains("<m:CALLERAS>") && line.contains("</m:CALLERAS>")){
					soapXMLContent.append("<m:CALLERAS>1"+dto.getStdCode()+"</m:CALLERAS>");
					keyFound = 1;
				}
				if(line.contains("<m:CALLEDAS>") && line.contains("</m:CALLEDAS>")){
					soapXMLContent.append("<m:CALLEDAS>2"+dto.getStdCode()+"</m:CALLEDAS>");
					keyFound = 1;
				}
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<m:" + key + ">") && line.contains("</m:" + key + ">")) {
	                	soapXMLContent.append("<m:" + key + ">" + servicePrmLst.get(key) + "</m:" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddEnumPostCallType);
			provReq.setUrl(ZTEServerUrlSSS);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addEnum: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	/*public List<ProvJsons> addSupplementaryServices(ProvisionInfoDTO dto, long requestId, Calendar now) {
		List<ProvJsons> ProvJsonsList = new ArrayList<>();
		ProvJsons provJson = null;
		String featurecodes = null;
		String fcode[] = null;
		try {
			featurecodes = commonDAO.getFeaturecodes(COMSConstants.TELEPHONY_SERVICE_CODE, dto.getCafno());
			fcode = featurecodes.split(",");
			for(String fc: fcode)
			{
				if(fc.equalsIgnoreCase("VBOX")){
					provJson = getVoiceMail(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("NSABBR")){
					provJson = getAbbreviatedDialing(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("HOTLINE")){
					provJson = getHotline(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("NSCUG")){
					provJson = getClosedUserGroup(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("NSADVANCONF")){
					provJson = getConferencing(fc, dto);
					ProvJsonsList.add(provJson);
				}		
				else if(fc.equalsIgnoreCase("CGT")){
					provJson = getHuntGroup(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("NSCFU")){
					provJson = getCallForwardingUnconditional(fc, dto);
					ProvJsonsList.add(provJson);
				}	
				else if(fc.equalsIgnoreCase("NSCFB")){
					provJson = getCallForwardingBusy(fc, dto);
					ProvJsonsList.add(provJson);
				}		
				else if(fc.equalsIgnoreCase("NSCFNR")){
					provJson = getCallForwardingNoReply(fc, dto);
					ProvJsonsList.add(provJson);
				}
				else if(fc.equalsIgnoreCase("NSCFNRC")){
					provJson = getCallForwardingNotReachable(fc, dto);
					ProvJsonsList.add(provJson);
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addSupplementaryServices: " + ex);
		}
		finally {
			featurecodes = null;
			fcode = null;
			provJson = null;
		}		
		return ProvJsonsList;
	}*/
	
	
	/*public ProvJsons getCallForwardingUnconditional(String featureCode, ProvisionInfoDTO dto) {
		List<FeatureParamsDTO> cafFeaturePrms = new ArrayList<>();
		int s = 0;
		ProvJsons provJson = null;
		StringBuilder sb = new StringBuilder();
		try{
			cafFeaturePrms = commonDAO.getCafFeaturePrms(featureCode, dto.getCafno());
			s = cafFeaturePrms.get(0).getPrmvalue().length;
			for(int i=0;i<s;i++)
			{
				
			}
		}
		catch(Exception e){
			LOGGER.error("Exception occurred in getVoiceMail: " + e);
		}
		finally{
			cafFeaturePrms = null;
			sb = null;
		}
		return provJson;
	}*/
	
	/*public ProvJsons addEnum(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/AddEnum.xml");
			//List of Service Parameters required for add Enum Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.ZONENAME);
			lst.add(COMSConstants.ORDER);
			lst.add(COMSConstants.PREFERENCE);
			lst.add(COMSConstants.FLAGS);
			lst.add(COMSConstants.SERVICE);
			lst.add(COMSConstants.REGEXP);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			//soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	            
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			while((line = bufferedReader.readLine()) != null) {
				for (String key : servicePrmLst.keySet()) {
					if(line.contains("<m:" + key + ">") && line.contains("</m:" + key + ">")) {
	                	soapXMLContent.append("<m:" + key + ">" + servicePrmLst.get(key) + "</m:" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddEnumPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(6);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addEnum: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	public ProvJsons addTP(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons provReq = null;
		String soapXML = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/AddTP.xml");
			//List of Service Parameters required for add TP Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.TEMPLATEIDX);
			lst.add(COMSConstants.UTYPE);
			lst.add(COMSConstants.NSCFU);
			lst.add(COMSConstants.NSRIO);
			lst.add(COMSConstants.NSCW);
			lst.add(COMSConstants.NSNPTY);
			lst.add(COMSConstants.NSFAX);
			lst.add(COMSConstants.NSABBR);
			lst.add(COMSConstants.NSVT);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	            
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXML);
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddTpPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(7);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addTP: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			soapXML = null;
			resource = null;
		}		
		return provReq;
	}
	
	public ProvJsons addSBR(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons provReq = null;
		String soapXML = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/AddSBR.xml");
			//List of Service Parameters required for add SBR Service
			lst = new ArrayList<String>();
			lst.add(COMSConstants.TEMPLATEIDX);

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	            
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXML);
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddSBRPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(8);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addSBR: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			soapXML = null;
			resource = null;
		}		
		return provReq;
	}
	
	public ProvJsons addCTX(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons provReq = null;
		String soapXML = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/AddCTX.xml");
			//List of Service Parameters required for add CTX Service
			lst = new ArrayList<String>();

			//Prepare Service Parameter List to simple string separated by comma
			serviceParamBuilder = prepareServiceParamList(lst);
			
			//Prepare HashMap with key, value pair having Service Param Key & Value
			servicePrmLst = commonDAO.getServiceParamValues(serviceParamBuilder.toString(), dto.getSrvccodeaddl());
			
			//Loop SOAP XML and replace with key with values and generate final SOAP XML
			soapXML = getSOAPXMLContent(resource, servicePrmLst, dto.getPhoneNo()).toString();
	            
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXML);
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddCTXPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(9);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addCTX: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			soapXML = null;
			resource = null;
		}		
		return provReq;
	}
	
	public ProvJsons addCTXUser(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons provReq = null;
		String soapXML = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		ArrayList<String> lst = null;
		HashMap<String, String> servicePrmLst = null;
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/AddCTXUser.xml");
	            
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXML);
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddCTXUserPostCallType);
			provReq.setUrl(ZTEServerUrl);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(10);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addCTXUser: " + ex);
		}
		finally {
			lst = null;
			serviceParamBuilder = null;
			servicePrmLst = null;
			soapXML = null;
			resource = null;
		}		
		return provReq;
	}*/
	
	public StringBuilder getSOAPXMLContent(ServletContextResource resource, HashMap<String, String> serviceParamList, String phoneNo) {
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;		
		try {			
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<time>") && line.contains("</time>")) {
					soapXMLContent.append("<time>"+stringDate+"</time>");
					keyFound = 1;
				}
				if(line.contains("<PVI>") && line.contains("</PVI>")){
					soapXMLContent.append("<PVI>+91"+phoneNo+"@vskp.apsflims.in</PVI>");
					keyFound = 1;
				}
				if(line.contains("<PUI>") && line.contains("</PUI>")){
					soapXMLContent.append("<PUI>sip:+91"+phoneNo+"@vskp.apsflims.in</PUI>");
					keyFound = 1;
				}
				if(line.contains("<PVILIST>") && line.contains("</PVILIST>")){
					soapXMLContent.append("<PVILIST>+91"+phoneNo+"@vskp.apsflims.in</PVILIST>");
					keyFound = 1;
				}
				if(line.contains("<PUILIST>") && line.contains("</PUILIST>")){
					soapXMLContent.append("<PUILIST>tel:+91"+phoneNo+"$sip:+91"+phoneNo+"@vskp.apsflims.in</PUILIST>");
					keyFound = 1;
				}
				if(line.contains("<DEFAULTPUI>") && line.contains("</DEFAULTPUI>")){
					soapXMLContent.append("<DEFAULTPUI>$sip:+91"+phoneNo+"@vskp.apsflims.in</DEFAULTPUI>");
					keyFound = 1;
				}
				for (String key : serviceParamList.keySet()) {
					if(line.contains("<" + key + ">") && line.contains("</" + key + ">")) {
	                	soapXMLContent.append("<" + key + ">" + serviceParamList.get(key) + "</" + key + ">");
	                	keyFound = 1;
	                	break;
	                }
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }			
	        bufferedReader.close();
	    }
		catch(Exception ex) {
			LOGGER.error("Exception occurred in getSOAPXMLContent: " + ex);
		}
		finally {
			line = null;
			resource = null;
			bufferedReader = null;
		}		
		return soapXMLContent;
	}
	
	public StringBuilder prepareServiceParamList(ArrayList<String> serviceParamList) {
		StringBuilder strBuilder = null;
		try {			
			strBuilder = new StringBuilder();
			for (String serviceParam : serviceParamList) {
				   strBuilder.append("'" + serviceParam + "',");					
			}
			
			strBuilder = new StringBuilder(strBuilder.substring(0, strBuilder.length()-1));
	    }
		catch(Exception ex) {
			LOGGER.error("Exception occurred in prepareServiceParamList: " + ex);
		}
		finally {

		}		
		return strBuilder;
	}
	
	@SuppressWarnings("unused")
	public ProvJsons addOIP(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/addOIP.xml");
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<m:IMPU>") && line.contains("</m:IMPU>")){
					soapXMLContent.append("<m:IMPU>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</m:IMPU>");
					keyFound = 1;
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddEnumPostCallType);
			provReq.setUrl(ZTEServerUrlSSS);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addOIP: " + ex);
		}
		finally {
			serviceParamBuilder = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	@SuppressWarnings("unused")
	public ProvJsons regUserPwd(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/RegUserPwd.xml");
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<m:IMPU>") && line.contains("</m:IMPU>")){
					soapXMLContent.append("<m:IMPU>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</m:IMPU>");
					keyFound = 1;
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddEnumPostCallType);
			provReq.setUrl(ZTEServerUrlSSS);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addOIP: " + ex);
		}
		finally {
			serviceParamBuilder = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	
	@SuppressWarnings("unused")
	public ProvJsons regOcb(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
		ProvJsons provReq = null;
		StringBuilder serviceParamBuilder = null;
		ServletContextResource resource = null;
		StringBuilder soapXMLContent = null;
		BufferedReader bufferedReader = null;
		String line = null;	
		try {			
			resource = new ServletContextResource(servletContext, "/WEB-INF/templates/RegOcb.xml");
			soapXMLContent = new StringBuilder();
			bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			int keyFound = 0;
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stringDate = df.format(dt);
			while((line = bufferedReader.readLine()) != null) {
				if(line.contains("<m:IMPU>") && line.contains("</m:IMPU>")){
					soapXMLContent.append("<m:IMPU>tel:+91"+dto.getPhoneNo()+"</m:IMPU>");
					keyFound = 1;
				}
				if(keyFound == 0)
					soapXMLContent.append(line);
				
				keyFound = 0;
            }
			bufferedReader.close();
			
			provReq = new ProvJsons();									
			provReq.setRequestId(requestId);
			provReq.setReq(soapXMLContent.toString());
			provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			provReq.setCallType(ZTEAddEnumPostCallType);
			provReq.setUrl(ZTEServerUrlSSS);
			provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			provReq.setSeqNum(cnt);
			provReq.setCreatedDate(now.getTime());						
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in addOIP: " + ex);
		}
		finally {
			serviceParamBuilder = null;
			resource = null;
			line = null;
			bufferedReader = null;
		}		
		return provReq;
	}
	
	//22-02-17-mahesh
		@SuppressWarnings("unused")
		public ProvJsons delSub(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelSub.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<m:IMPU>") && line.contains("</m:IMPU>")){
						soapXMLContent.append("<m:IMPU>tel:+91"+dto.getPhoneNo()+"</m:IMPU>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrlSSS);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delSub: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}
		
		@SuppressWarnings("unused")
		public ProvJsons delAliasGrp(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelAliasGrp.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<time>") && line.contains("</time>")) {
						soapXMLContent.append("<time>"+stringDate+"</time>");
						keyFound = 1;
					}
					if(line.contains("<PUI>") && line.contains("</PUI>")){
						soapXMLContent.append("<PUI>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PUI>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrl);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delAliasGrp: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}
	
		@SuppressWarnings("unused")
		public ProvJsons delImpRegSet(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelImpRegSet.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<time>") && line.contains("</time>")) {
						soapXMLContent.append("<time>"+stringDate+"</time>");
						keyFound = 1;
					}
					if(line.contains("<PUI>") && line.contains("</PUI>")){
						soapXMLContent.append("<PUI>tel:+91"+dto.getPhoneNo()+"</PUI>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrl);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delImpRegSet: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}
		
		@SuppressWarnings("unused")
		public ProvJsons delPUITel(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelPUITel.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<time>") && line.contains("</time>")) {
						soapXMLContent.append("<time>"+stringDate+"</time>");
						keyFound = 1;
					}
					if(line.contains("<PUI>") && line.contains("</PUI>")){
						soapXMLContent.append("<PUI>tel:+91"+dto.getPhoneNo()+"</PUI>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrl);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delPUITel: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}
		
		@SuppressWarnings("unused")
		public ProvJsons delPUISip(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelPUISip.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<time>") && line.contains("</time>")) {
						soapXMLContent.append("<time>"+stringDate+"</time>");
						keyFound = 1;
					}
					if(line.contains("<PUI>") && line.contains("</PUI>")){
						soapXMLContent.append("<PUI>sip:+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PUI>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrl);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delPUISip: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}	
		
		@SuppressWarnings("unused")
		public ProvJsons delPVI(ProvisionInfoDTO dto, long requestId, Calendar now, int cnt) {
			ProvJsons provReq = null;
			StringBuilder serviceParamBuilder = null;
			ServletContextResource resource = null;
			StringBuilder soapXMLContent = null;
			BufferedReader bufferedReader = null;
			String line = null;	
			try {			
				resource = new ServletContextResource(servletContext, "/WEB-INF/templates/DelPVI.xml");
				soapXMLContent = new StringBuilder();
				bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
				int keyFound = 0;
				Date dt = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stringDate = df.format(dt);
				while((line = bufferedReader.readLine()) != null) {
					if(line.contains("<time>") && line.contains("</time>")) {
						soapXMLContent.append("<time>"+stringDate+"</time>");
						keyFound = 1;
					}
					if(line.contains("<PVI>") && line.contains("</PVI>")){
						soapXMLContent.append("<PVI>+91"+dto.getPhoneNo()+"@vskp.apsflims.in</PVI>");
						keyFound = 1;
					}
					if(keyFound == 0)
						soapXMLContent.append(line);
					
					keyFound = 0;
	            }
				bufferedReader.close();
				
				provReq = new ProvJsons();									
				provReq.setRequestId(requestId);
				provReq.setReq(soapXMLContent.toString());
				provReq.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
				provReq.setCallType(ZTEAddEnumPostCallType);
				provReq.setUrl(ZTEServerUrl);
				provReq.setStatus(COMSConstants.TO_BE_PROCESSED);
				provReq.setSeqNum(cnt);
				provReq.setCreatedDate(now.getTime());						
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurred in delPVI: " + ex);
			}
			finally {
				serviceParamBuilder = null;
				resource = null;
				line = null;
				bufferedReader = null;
			}		
			return provReq;
		}
		
}
