package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonProperty;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AAAUpdateDTO {

	@JsonProperty("nwSubsCode")
	private String nwSubsCode;

	@JsonProperty("UPSpeed")
	private String UPSpeed;

	@JsonProperty("DOWNSpeed")
	private String DOWNSpeed;
	
	@JsonProperty("prodcafNo")
	private String prodcafNo;
	
	@JsonProperty("acessId")
	private String acessId;

	public String getNwSubsCode() {
		return nwSubsCode;
	}

	public void setNwSubsCode(String nwSubsCode) {
		this.nwSubsCode = nwSubsCode;
	}

	public String getUPSpeed() {
		return UPSpeed;
	}

	public void setUPSpeed(String uPSpeed) {
		UPSpeed = uPSpeed;
	}

	public String getDOWNSpeed() {
		return DOWNSpeed;
	}

	public void setDOWNSpeed(String dOWNSpeed) {
		DOWNSpeed = dOWNSpeed;
	}

	public String getProdcafNo() {
		return prodcafNo;
	}

	public void setProdcafNo(String prodcafNo) {
		this.prodcafNo = prodcafNo;
	}

	public String getAcessId() {
		return acessId;
	}

	public void setAcessId(String acessId) {
		this.acessId = acessId;
	}
}
