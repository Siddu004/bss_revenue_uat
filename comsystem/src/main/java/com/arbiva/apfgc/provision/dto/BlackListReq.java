package com.arbiva.apfgc.provision.dto;

import java.io.Serializable;
import java.util.List;

public class BlackListReq implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<String> subscriberCodes;

	public List<String> getSubscriberCodes() {
		return subscriberCodes;
	}

	public void setSubscriberCodes(List<String> subscriberCodes) {
		this.subscriberCodes = subscriberCodes;
	}
	
	

}
