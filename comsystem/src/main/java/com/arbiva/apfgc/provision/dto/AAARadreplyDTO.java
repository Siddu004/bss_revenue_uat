package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AAARadreplyDTO {

	private static final long serialVersionUID = 1L;

	public AAARadreplyDTO() {
	
	}
	
	private AIDDTO aid;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("downloadlimit")
	private String downloadlimit;
	
	@JsonProperty("downloadlimitunit")
	private String downloadlimitunit;
	
	@JsonProperty("uploadlimit")
	private String uploadlimit;
	
	@JsonProperty("uploadlimitunit")
	private String uploadlimitunit;

	/**
	 * @return the aid
	 */
	public AIDDTO getAid() {
		return aid;
	}

	/**
	 * @param aid the aid to set
	 */
	public void setAid(AIDDTO aid) {
		this.aid = aid;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the downloadlimit
	 */
	public String getDownloadlimit() {
		return downloadlimit;
	}

	/**
	 * @param downloadlimit the downloadlimit to set
	 */
	public void setDownloadlimit(String downloadlimit) {
		this.downloadlimit = downloadlimit;
	}

	/**
	 * @return the downloadlimitunit
	 */
	public String getDownloadlimitunit() {
		return downloadlimitunit;
	}

	/**
	 * @param downloadlimitunit the downloadlimitunit to set
	 */
	public void setDownloadlimitunit(String downloadlimitunit) {
		this.downloadlimitunit = downloadlimitunit;
	}

	/**
	 * @return the uploadlimit
	 */
	public String getUploadlimit() {
		return uploadlimit;
	}

	/**
	 * @param uploadlimit the uploadlimit to set
	 */
	public void setUploadlimit(String uploadlimit) {
		this.uploadlimit = uploadlimit;
	}

	/**
	 * @return the uploadlimitunit
	 */
	public String getUploadlimitunit() {
		return uploadlimitunit;
	}

	/**
	 * @param uploadlimitunit the uploadlimitunit to set
	 */
	public void setUploadlimitunit(String uploadlimitunit) {
		this.uploadlimitunit = uploadlimitunit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
