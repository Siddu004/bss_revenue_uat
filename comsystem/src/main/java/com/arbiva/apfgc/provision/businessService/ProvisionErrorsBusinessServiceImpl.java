package com.arbiva.apfgc.provision.businessService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.provision.dao.CommonDAO;
import com.arbiva.apfgc.provision.dao.ProvErrorDAO;
import com.arbiva.apfgc.provision.dto.ErrorJsonsDTO;
import com.arbiva.apfgc.provision.dto.ErrorsDTO;
import com.arbiva.apfgc.provision.dto.PendingProvisonErrorsDTO;
import com.arbiva.apfgc.provision.dto.ProvisionInfoDTO;
import com.arbiva.apfgc.provision.models.ProvJsons;
import com.arbiva.apfgc.provision.utils.COMSConstants;

@Service
public class ProvisionErrorsBusinessServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(ProvisionErrorsBusinessServiceImpl.class);
	
	@Autowired
	private ProvErrorDAO provErrorDAO;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Autowired
	private ProvisioningBusinessServiceImpl provisioningBusinessServiceImpl;
	
	@Autowired
	private TelephonyProvBusinessServiceImpl telephonyProvBusinessServiceImpl;
	
	public List<ErrorsDTO> getProvisionErrorList() {
		List<ErrorsDTO> provisionErrorList =  new ArrayList<>();
		List<Object[]> provErrList = null;
		try{
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getProvisionErrorList() :: START");
			provErrList= provErrorDAO.getProvisionErrorList();
			for(Object[] obj : provErrList)
			{
				ErrorsDTO errorsDTO = new ErrorsDTO();
				errorsDTO.setCafNo(obj[0] == null ? "" : obj[0].toString());	
				errorsDTO.setRequestId(obj[1] == null ? "" : obj[1].toString());
				errorsDTO.setProdCode(obj[2] == null ? "" : obj[2].toString());
				errorsDTO.setSrvcCode(obj[3] == null ? "" : obj[3].toString());
				errorsDTO.setCreatedOn(obj[4] == null ? "" : obj[4].toString());
				provisionErrorList.add(errorsDTO);
			}
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getProvisionErrorList() :: END");
		}
		catch(Exception e){
			LOGGER.error("ProvisionErrorsBusinessServiceImpl :: getProvisionErrorList() :: " + e);
			throw e;
		}
		finally{
			provErrList = null;
		}
		return provisionErrorList;
	}
	
	public List<ErrorJsonsDTO> getErrJsons(String reqid) {
		List<ErrorJsonsDTO> ErrorJsonsList =  new ArrayList<>();
		List<Object[]> jsonErrList = null;
		try{
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getErrJsons() :: START");
			jsonErrList= provErrorDAO.getErrJsons(reqid);
			for(Object[] obj : jsonErrList)
			{
				ErrorJsonsDTO errJsonsDTO = new ErrorJsonsDTO();
				errJsonsDTO.setReq(obj[0] == null ? "" : obj[0].toString());	
				errJsonsDTO.setResp(obj[1] == null ? "" : obj[1].toString());
				errJsonsDTO.setCreatedDate(obj[2] == null ? "" : obj[2].toString());
				errJsonsDTO.setExecutedDate(obj[3] == null ? "" :obj[3].toString());
				errJsonsDTO.setStatus(obj[4] == null ? "" : obj[4].toString());
				ErrorJsonsList.add(errJsonsDTO);
			}
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getErrJsons() :: END");
		}
		catch(Exception e){
			LOGGER.error("ProvisionErrorsBusinessServiceImpl :: getErrJsons() :: " + e);
			throw e;
		}
		finally{
			jsonErrList = null;
		}
		return ErrorJsonsList;
	}

	/*public int updateProvRequests(String reqid) {
		int res = 0;
		try{
			res = provErrorDAO.updateProvRequests(reqid);
		}catch(Exception e)
		{
			LOGGER.error("Exception occurred in updateProvRequests: " + e);
			throw e;
		}
		return res;
	}*/
	
	@Transactional
	public int updateProvTables(String arr) {
		int res = 0;
		String request = "";
		String status = null;
		String maxSeqNo = null;
		String nwSubscriberCode = null;
		String type = null;
		ProvisionInfoDTO dto = null;
		List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
		String cafNo = null;
		int seqNum = 99;
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);
		boolean HSIFlag;
		try{
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvTables() :: START");
			for(String reqid : arr.split(","))
			{
				seqNum = provErrorDAO.getSeqNum(reqid);
				cafNo = provErrorDAO.getCafNo(reqid);
				//listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(cafNo), "", reqid);
				dto = listOfProvisioningRequests.get(0);
				ProvJsons provJson = new ProvJsons();
				if(seqNum != 99){
						if(null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.INTERNET_SERVICE_CODE))
						{
							switch(seqNum)
							{
								case 1: provJson = provisioningBusinessServiceImpl.createAgoraCustomerRegister(dto, Long.valueOf(reqid), now);
							 		   	break;
								case 2: provJson = provisioningBusinessServiceImpl.createAgoraServiceActivation(dto, Long.valueOf(reqid), now);
					 		   			break;
								/*case 3: provJson = provisioningBusinessServiceImpl.createAAARedcheck(dto, Long.valueOf(reqid), now);
					 		   			break;
								case 4: provJson = provisioningBusinessServiceImpl.createAAARedreply(dto, Long.valueOf(reqid), now);
					 		   			break;*/
							}
						}
						else if(null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.IPTV_SERVICE_CODE))
						{
							nwSubscriberCode = commonDAO.getCAFSrvcNWSubscriberCode(Long.valueOf(dto.getStbcafno()));
							type = commonDAO.getProvRequestType(reqid);
							switch(seqNum)
							{
								case 0: provJson = provisioningBusinessServiceImpl.createAgoraMultiCastChannelPackage(dto, Long.valueOf(reqid), now);
										break;
								case 1: provJson = provisioningBusinessServiceImpl.createAgoraGPONActivation(dto, Long.valueOf(reqid), now);
							 		   	break;
								case 2: provJson = provisioningBusinessServiceImpl.createCorpusRegistration(dto, Long.valueOf(reqid), now, COMSConstants.TO_BE_PROCESSED);
					 		   			break;
								default: provJson = provisioningBusinessServiceImpl.createCorpusServicePackage(dto, Long.valueOf(reqid), now, nwSubscriberCode, type, seqNum);
					 		   			break;
							}
							
						}
						else if(null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.TELEPHONY_SERVICE_CODE)) 
						{					
							if(seqNum % 9 == 1)
								provJson = telephonyProvBusinessServiceImpl.createPVI(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 2)
								provJson = telephonyProvBusinessServiceImpl.createPUI(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 3)
								provJson = telephonyProvBusinessServiceImpl.createDefaultPUI(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 4)
								provJson = telephonyProvBusinessServiceImpl.setImpRegSet(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 5)
								provJson = telephonyProvBusinessServiceImpl.setAliaseGrp(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 6)
								provJson = telephonyProvBusinessServiceImpl.addSubscriberFeaturesProvReq(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 7)
								provJson = telephonyProvBusinessServiceImpl.addOIP(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 8)
								provJson = telephonyProvBusinessServiceImpl.regUserPwd(dto, Long.valueOf(reqid), now, seqNum);
							else if(seqNum % 9 == 9)
								provJson = telephonyProvBusinessServiceImpl.regOcb(dto, Long.valueOf(reqid), now, seqNum);
						}
						else if(null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.VPN_SERVICE_CODE))
						{
							HSIFlag = provErrorDAO.checkHSIFlag(Long.valueOf(cafNo));
							if(HSIFlag)
							{
								switch(seqNum)
								{
									case 1: provJson = provisioningBusinessServiceImpl.createAgoraCustomerRegister(dto, Long.valueOf(reqid), now);
								 		   	break;
									case 2: provJson = provisioningBusinessServiceImpl.createVPNAgoraServiceActivation(dto, Long.valueOf(reqid), now, 2);
						 		   			break;
								}
								
							}
							else 
								provJson = provisioningBusinessServiceImpl.createVPNAgoraServiceActivation(dto, Long.valueOf(reqid), now, 1);
						}
						request = provJson.getReq();
						res = provErrorDAO.updateProvJson(reqid, request, seqNum);
						maxSeqNo = provErrorDAO.getMaxSeqNo(reqid);
						for(int i=seqNum+1; i<=Integer.parseInt(maxSeqNo); i++){
							status = provErrorDAO.getStatus(reqid, i);
							if(!status.equalsIgnoreCase("3") && !status.isEmpty())
								res = provErrorDAO.updateJsons(reqid, i);	
						}
					
						res = provErrorDAO.updateProvRequests(reqid);
				}
			}
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvTables() :: END");
		}catch(Exception e)
		{
			LOGGER.error("ProvisionErrorsBusinessServiceImpl :: updateProvTables() :: " + e);
			throw e;
		}
		finally{
			request = null;
			status = null;
			maxSeqNo = null;
			nwSubscriberCode = null;
			type = null;
			dto = null;
			cafNo = null;
			tz = null;
			now = null;
		}
		
		return res;
	}
	
	
	public List<PendingProvisonErrorsDTO> getPendingProvisonErrors() {
		List<PendingProvisonErrorsDTO> ErrorJsonsList =  new ArrayList<>();
		List<Object[]> jsonErrList = null;
		try{
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getPendingProvisonErrors() :: START");
			jsonErrList= provErrorDAO.getPendingProvisonJsons();
			for(Object[] obj : jsonErrList)
			{
				PendingProvisonErrorsDTO pendingProvisonErrorsDTO = new PendingProvisonErrorsDTO();
				pendingProvisonErrorsDTO.setRequestid(obj[0]== null ? "" :obj[0].toString());
				pendingProvisonErrorsDTO.setReq(obj[1] == null ? "" : obj[1].toString());	
				pendingProvisonErrorsDTO.setResp(obj[2] == null ? "" : obj[2].toString());
				pendingProvisonErrorsDTO.setCreatedDate(obj[3] == null ? "" : obj[3].toString());
				pendingProvisonErrorsDTO.setExecutedDate(obj[4] == null ? "" :obj[4].toString());
				pendingProvisonErrorsDTO.setStatus(obj[5] == null ? "" : obj[5].toString());
				ErrorJsonsList.add(pendingProvisonErrorsDTO);
			}
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: getPendingProvisonErrors() :: END");
		}
		catch(Exception e){
			LOGGER.error("ProvisionErrorsBusinessServiceImpl :: getPendingProvisonErrors() :: " + e);
			throw e;
		}
		finally{
			jsonErrList = null;
		}
		return ErrorJsonsList;
	}

	
	
	
	
	@Transactional
	public int updateProvisionErrors(String arr) {
		int res = 0;		
		String cafNo = null;
		int seqNum = 99;
		
		try{
			LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvisionErrors() :: START");
			for(String reqid : arr.split(","))
				
			{
				String[] reqIdsForCafs;
				seqNum = provErrorDAO.getSeqNum(reqid);
				cafNo = provErrorDAO.getCafNo(reqid);
				reqIdsForCafs=provErrorDAO.getReqIdsByCafNo(cafNo);
				if(seqNum != 99){
					LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvJsons() :: START");
					provErrorDAO.updateProvJsons(reqIdsForCafs);
					LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvTables() :: END");
					LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvRequest() :: START");
					res = provErrorDAO.updateProvRequest(reqIdsForCafs);
					LOGGER.info("ProvisionErrorsBusinessServiceImpl :: updateProvRequest() :: END");
				}
				
			}
			
		}catch(Exception e)
		{
			LOGGER.error("ProvisionErrorsBusinessServiceImpl :: updateProvTables() :: " + e);
			throw e;
		}
		
		
		return res;
	}
	
	
	
	
}
