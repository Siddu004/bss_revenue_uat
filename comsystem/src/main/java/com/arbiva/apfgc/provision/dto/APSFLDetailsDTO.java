package com.arbiva.apfgc.provision.dto;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class APSFLDetailsDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	public APSFLDetailsDTO() {

	}

	@Value("${apsfl.registrationno}")
	private String registrationno;

	@Value("${apsfl.servicetaxno}")
	private String servicetaxno;

	@Value("${apsfl.warranty}")
	private String warranty;
	
	@Value("${apsfl.entertainmenttaxno}")
	private String entertainmenttaxno;

	@Value("${apsfl.licenceauthority}")
	private String licenceauthority;

	@Value("${apsfl.postwarentycharges}")
	private String postwarentycharges;

	@Value("${apsfl.custcareno}")
	private String custcareno;

	@Value("${apsfl.othercustcareno}")
	private String othercustcareno;

	@Value("${apsfl.custcareemail}")
	private String custcareemail;

	public String getRegistrationno() {
		return registrationno;
	}

	public void setRegistrationno(String registrationno) {
		this.registrationno = registrationno;
	}

	public String getServicetaxno() {
		return servicetaxno;
	}

	public void setServicetaxno(String servicetaxno) {
		this.servicetaxno = servicetaxno;
	}

	public String getWarranty() {
		return warranty;
	}

	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}

	public String getEntertainmenttaxno() {
		return entertainmenttaxno;
	}

	public void setEntertainmenttaxno(String entertainmenttaxno) {
		this.entertainmenttaxno = entertainmenttaxno;
	}

	public String getLicenceauthority() {
		return licenceauthority;
	}

	public void setLicenceauthority(String licenceauthority) {
		this.licenceauthority = licenceauthority;
	}

	public String getPostwarentycharges() {
		return postwarentycharges;
	}

	public void setPostwarentycharges(String postwarentycharges) {
		this.postwarentycharges = postwarentycharges;
	}

	public String getCustcareno() {
		return custcareno;
	}

	public void setCustcareno(String custcareno) {
		this.custcareno = custcareno;
	}

	public String getOthercustcareno() {
		return othercustcareno;
	}

	public void setOthercustcareno(String othercustcareno) {
		this.othercustcareno = othercustcareno;
	}

	public String getCustcareemail() {
		return custcareemail;
	}

	public void setCustcareemail(String custcareemail) {
		this.custcareemail = custcareemail;
	}

	

}
