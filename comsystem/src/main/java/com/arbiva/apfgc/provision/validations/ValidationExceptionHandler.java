package com.arbiva.apfgc.provision.validations;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.arbiva.apfgc.provision.Exception.COMSException;
import com.arbiva.apfgc.provision.dto.COMSErrorMessageDTO;

/** 
 *
 * All the Uncaught <code>com.yms.validations</code> from the <code>ValidationAspect</code> are caught here and
 * build more meaning full response
 *
 */
@Provider
public class ValidationExceptionHandler implements ExceptionMapper<COMSException>
{

    @Override
    public Response toResponse(COMSException exception)
    {
        ResponseBuilder responseBuilder = Response.status(Response.Status.BAD_REQUEST);
        handleException(responseBuilder, exception);
        return responseBuilder.build();
    }

    public void handleException(ResponseBuilder respBuilder, Exception ex)
    {
        COMSErrorMessageDTO errDTO = new COMSErrorMessageDTO(Response.Status.BAD_REQUEST.getStatusCode(),ex.getMessage());
        respBuilder.status(errDTO.getCode()).type(MediaType.APPLICATION_JSON).entity(errDTO);
    }

}
