package com.arbiva.apfgc.provision.dto;

public class FeatureParamsDTO {
	
	private String prmcode;
	private String[] prmvalue;
	
	public String getPrmcode() {
		return prmcode;
	}
	public void setPrmcode(String prmcode) {
		this.prmcode = prmcode;
	}
	public String[] getPrmvalue() {
		return prmvalue;
	}
	public void setPrmvalue(String[] prmvalue) {
		this.prmvalue = prmvalue;
	}
	
	

}
