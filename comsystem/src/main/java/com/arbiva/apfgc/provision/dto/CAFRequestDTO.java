package com.arbiva.apfgc.provision.dto;


/**
 * 
 * @author srinivasa
 *
 */
public class CAFRequestDTO {

	private static final long serialVersionUID = 1L;

	public CAFRequestDTO() {
	
	}
	
	private String districtid;
	
	private String acctcafno;
	
	private String tenantcode;
	
	private String prodcode;
		
	private String srvccode;
	
	private String nwsource;
	
	private String nwsubscode;
	
	private String activationdate;
	
	private String customertype;
	
	private String requestid;
	
	private String customerid;
	
	private String requestaction;
	
	private String actiondate;
	
	private String prodcafno;
	
	private String stbCafNo;

	
	public String getStbCafNo() {
		return stbCafNo;
	}

	public void setStbCafNo(String stbCafNo) {
		this.stbCafNo = stbCafNo;
	}

	public String getRequestaction() {
		return requestaction;
	}

	public void setRequestaction(String requestaction) {
		this.requestaction = requestaction;
	}

	public String getActiondate() {
		return actiondate;
	}

	public void setActiondate(String actiondate) {
		this.actiondate = actiondate;
	}

	/**
	 * @return the districtid
	 */
	public String getDistrictid() {
		return districtid;
	}

	/**
	 * @param districtid the districtid to set
	 */
	public void setDistrictid(String districtid) {
		this.districtid = districtid;
	}

	/**
	 * @return the tenantcode
	 */
	public String getTenantcode() {
		return tenantcode;
	}

	/**
	 * @param tenantcode the tenantcode to set
	 */
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	/**
	 * @return the prodcode
	 */
	public String getProdcode() {
		return prodcode;
	}

	/**
	 * @param prodcode the prodcode to set
	 */
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	/**
	 * @return the srvccode
	 */
	public String getSrvccode() {
		return srvccode;
	}

	/**
	 * @param srvccode the srvccode to set
	 */
	public void setSrvccode(String srvccode) {
		this.srvccode = srvccode;
	}

	/**
	 * @return the nwsource
	 */
	public String getNwsource() {
		return nwsource;
	}

	/**
	 * @param nwsource the nwsource to set
	 */
	public void setNwsource(String nwsource) {
		this.nwsource = nwsource;
	}

	/**
	 * @return the nwsubscode
	 */
	public String getNwsubscode() {
		return nwsubscode;
	}

	/**
	 * @param nwsubscode the nwsubscode to set
	 */
	public void setNwsubscode(String nwsubscode) {
		this.nwsubscode = nwsubscode;
	}

	/**
	 * @return the activationdate
	 */
	public String getActivationdate() {
		return activationdate;
	}

	/**
	 * @param activationdate the activationdate to set
	 */
	public void setActivationdate(String activationdate) {
		this.activationdate = activationdate;
	}

	/**
	 * @return the customertype
	 */
	public String getCustomertype() {
		return customertype;
	}

	/**
	 * @param customertype the customertype to set
	 */
	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}

	/**
	 * @return the requestid
	 */
	public String getRequestid() {
		return requestid;
	}

	/**
	 * @param requestid the requestid to set
	 */
	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	
	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}

	public String getProdcafno() {
		return prodcafno;
	}

	public void setProdcafno(String prodcafno) {
		this.prodcafno = prodcafno;
	}
}
