package com.arbiva.apfgc.provision.utils;

/**
 * {@link COMSErrorCode} is an utility class, which holds the
 * {@link COMSErrorCodes} which contains error codes.
 * 
 * @author srinivasa
 *
 */
public class COMSErrorCode {

	/**
	 * {@link COMSErrorCodes} is an enum class, which holds the application level
	 * error informations.
	 * 
	 * @author srinivasa
	 *
	 */
	public enum COMSErrorCodes {

		GAE001(400, "Internal Server Error"), 
		GAE002(400, "Invalid empty parameter"),
		GAE003(400, "Entity already exists"),
		GAE004(400, "Entity not found"),
		GAE005(400, "Invalid Code"),
		DBE001(400, "Error occured while executing database query");
		

		private final int code;
		private final String description;

		private COMSErrorCodes(int code, String description) {
			this.code = code;
			this.description = description;
		}

		public int getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

}
