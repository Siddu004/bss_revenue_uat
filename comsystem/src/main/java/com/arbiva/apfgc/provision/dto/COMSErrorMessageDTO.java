package com.arbiva.apfgc.provision.dto;

import com.arbiva.apfgc.provision.utils.COMSErrorCode.COMSErrorCodes;

/**
 * {@link COMSErrorMessageDTO} is custom dto to hold the exception/error
 * information.
 * 
 * @author srinivasa
 *
 */
public class COMSErrorMessageDTO {

	private int code;

	private String message;

	private String description;

	public COMSErrorMessageDTO() {
	}

	public COMSErrorMessageDTO(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public COMSErrorMessageDTO(COMSErrorCodes errorCodes) {
		this.code = errorCodes.getCode();
		this.message = errorCodes.getDescription();

	}

	public COMSErrorMessageDTO(COMSErrorCodes errorCodes, String description) {
		this.code = errorCodes.getCode();
		this.message = errorCodes.getDescription();
		this.description = description;

	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
