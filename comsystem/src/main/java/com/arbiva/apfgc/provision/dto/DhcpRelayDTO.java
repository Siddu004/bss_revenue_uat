package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DhcpRelayDTO {

	private static final long serialVersionUID = 1L;

	public DhcpRelayDTO() {
	
	}
	
	@JsonProperty("remoteId")
	private String remoteId;
	
	@JsonProperty("useGlobalDhcp")
	private boolean useGlobalDhcp;
	
	@JsonProperty("op82")
	private boolean op82;
	
	@JsonProperty("op18")
	private boolean op18;
	
	public boolean isOp18() {
		return op18;
	}

	public void setOp18(boolean op18) {
		this.op18 = op18;
	}

	public boolean isOp37() {
		return op37;
	}

	public void setOp37(boolean op37) {
		this.op37 = op37;
	}

	@JsonProperty("op37")
	private boolean op37;

	public boolean isOp82() {
		return op82;
	}

	public void setOp82(boolean op82) {
		this.op82 = op82;
	}

	/**
	 * @return the remoteId
	 */
	public String getRemoteId() {
		return remoteId;
	}

	/**
	 * @param remoteId the remoteId to set
	 */
	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	/**
	 * @return the useGlobalDhcp
	 */
	public boolean isUseGlobalDhcp() {
		return useGlobalDhcp;
	}

	/**
	 * @param useGlobalDhcp the useGlobalDhcp to set
	 */
	public void setUseGlobalDhcp(boolean useGlobalDhcp) {
		this.useGlobalDhcp = useGlobalDhcp;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
