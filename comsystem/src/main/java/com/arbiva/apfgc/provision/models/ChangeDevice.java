package com.arbiva.apfgc.provision.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "olchangecpeprov")
public class ChangeDevice {
	
	@Id
	@Column(name="requestid")
	private long requestid;
	
	@Column(name="acctcafno")
	private long acctcafno;
	
	@Column(name="customerid")
	private long customerid;
	
	@Column(name="stbcafno")
	private long stbcafno;
	
	@Column(name="nwsubscode")
	private String nwsubscode;
	
	@Column(name="outcpeslno")
	private String outcpeslno;
	
	@Column(name="outcpemacaddr")
	private String outcpemacaddr;
	
	@Column(name="outprofile_id")
	private long outprofile_id;
	
	@Column(name="incpeslno")
	private String incpeslno;
	
	@Column(name="incpemacaddr")
	private  String incpemacaddr;
	
	@Column(name="inprofile_id")
	private long inprofile_id;
		
	@Column(name="req")
	private String request;
	
	
	@Column(name="resp")
	private String response;
	
	@Column(name="url")
	private String url;
	
	@Column(name="calltype")
	private String calltype;

	@Column(name="status")
	private long status;
	
	@Column(name="executeddate")
	private Date executedDate;
	
	@Column(name="lmoid")
	private long lmoId;
	
	@Column(name="mspid")
	private long mspId;
	
	@Column(name="createdon")
	private Date createdOn;
	
	@Column(name="createdby")
	private String createdBy;
	
	@Column(name="createdipaddr")
	private String createdIpAddr;
	
	

	public long getRequestid() {
		return requestid;
	}

	public void setRequestid(long requestid) {
		this.requestid = requestid;
	}

	public long getAcctcafno() {
		return acctcafno;
	}

	public void setAcctcafno(long acctcafno) {
		this.acctcafno = acctcafno;
	}

	public long getCustomerid() {
		return customerid;
	}

	public void setCustomerid(long customerid) {
		this.customerid = customerid;
	}

	public long getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(long stbcafno) {
		this.stbcafno = stbcafno;
	}

	public String getNwsubscode() {
		return nwsubscode;
	}

	public String setNwsubscode(String nwsubscode) {
		return this.nwsubscode = nwsubscode;
	}

	public String getOutcpeslno() {
		return outcpeslno;
	}

	public void setOutcpeslno(String outcpeslno) {
		this.outcpeslno = outcpeslno;
	}

	public String getOutcpemacaddr() {
		return outcpemacaddr;
	}

	public void setOutcpemacaddr(String outcpemacaddr) {
		this.outcpemacaddr = outcpemacaddr;
	}

	public long getOutprofile_id() {
		return outprofile_id;
	}

	public void setOutprofile_id(long outprofile_id) {
		this.outprofile_id = outprofile_id;
	}

	public String getIncpeslno() {
		return incpeslno;
	}

	public void setIncpeslno(String incpeslno) {
		this.incpeslno = incpeslno;
	}

	public String getIncpemacaddr() {
		return incpemacaddr;
	}

	public void setIncpemacaddr(String incpemacaddr) {
		this.incpemacaddr = incpemacaddr;
	}

	public long getInprofile_id() {
		return inprofile_id;
	}

	public void setInprofile_id(long inprofile_id) {
		this.inprofile_id = inprofile_id;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public Date getExecutedDate() {
		return executedDate;
	}

	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}

	public long getLmoId() {
		return lmoId;
	}

	public void setLmoId(long lmoId) {
		this.lmoId = lmoId;
	}

	public long getMspId() {
		return mspId;
	}

	public void setMspId(long mspId) {
		this.mspId = mspId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedIpAddr() {
		return createdIpAddr;
	}

	public void setCreatedIpAddr(String createdIpAddr) {
		this.createdIpAddr = createdIpAddr;
	}
	
	public String getUrl() {
		return url;
	}

	public String setUrl(String url) {
		return this.url = url;
	}

	public String getCalltype() {
		return calltype;
	}

	public void setCalltype(String calltype) {
		this.calltype = calltype;
	}
}
