package com.arbiva.apfgc.provision.models;

import java.io.Serializable;

public class ProvJsonsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long requestId;
	
	private int seqNum;
	
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public int getSeqNum() {
		return seqNum;
	}
	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (requestId ^ (requestId >>> 32));
		result = prime * result + seqNum;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvJsonsPK other = (ProvJsonsPK) obj;
		if (requestId != other.requestId)
			return false;
		if (seqNum != other.seqNum)
			return false;
		return true;
	}
	
	

}
