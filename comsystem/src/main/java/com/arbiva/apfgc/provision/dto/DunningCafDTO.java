package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DunningCafDTO {

	private static final long serialVersionUID = 1L;

	public DunningCafDTO() {
	
	}
	
	private String acctcafno;
	
	private String tenantcode;
		
	private String productcode;
	
	private String srvccode;
	
	private String actdate;
	
	private String duedate;
	
	private String payamt;
	
	private String paidamt;
	
	private String status;

	/**
	 * @return the acctcafno
	 */
	public String getAcctcafno() {
		return acctcafno;
	}

	/**
	 * @param acctcafno the acctcafno to set
	 */
	public void setAcctcafno(String acctcafno) {
		this.acctcafno = acctcafno;
	}

	/**
	 * @return the tenantcode
	 */
	public String getTenantcode() {
		return tenantcode;
	}

	/**
	 * @param tenantcode the tenantcode to set
	 */
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	/**
	 * @return the productcode
	 */
	public String getProductcode() {
		return productcode;
	}

	/**
	 * @param productcode the productcode to set
	 */
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	/**
	 * @return the srvccode
	 */
	public String getSrvccode() {
		return srvccode;
	}

	/**
	 * @param srvccode the srvccode to set
	 */
	public void setSrvccode(String srvccode) {
		this.srvccode = srvccode;
	}

	/**
	 * @return the actdate
	 */
	public String getActdate() {
		return actdate;
	}

	/**
	 * @param actdate the actdate to set
	 */
	public void setActdate(String actdate) {
		this.actdate = actdate;
	}

	/**
	 * @return the duedate
	 */
	public String getDuedate() {
		return duedate;
	}

	/**
	 * @param duedate the duedate to set
	 */
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	/**
	 * @return the payamt
	 */
	public String getPayamt() {
		return payamt;
	}

	/**
	 * @param payamt the payamt to set
	 */
	public void setPayamt(String payamt) {
		this.payamt = payamt;
	}

	/**
	 * @return the paidamt
	 */
	public String getPaidamt() {
		return paidamt;
	}

	/**
	 * @param paidamt the paidamt to set
	 */
	public void setPaidamt(String paidamt) {
		this.paidamt = paidamt;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
