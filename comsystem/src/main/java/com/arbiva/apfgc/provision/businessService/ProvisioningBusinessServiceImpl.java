package com.arbiva.apfgc.provision.businessService;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.comsystem.BO.CafProvisioningBO;
import com.arbiva.apfgc.comsystem.dao.impl.StoredProcedureDAO;
import com.arbiva.apfgc.comsystem.model.OSDFingerPrintDetails;
import com.arbiva.apfgc.comsystem.service.CorpusAPIService;
import com.arbiva.apfgc.comsystem.service.OSDFingerPrintDetailsService;
import com.arbiva.apfgc.comsystem.util.ComsEnumCodes;
import com.arbiva.apfgc.comsystem.vo.CorpusResponce;
import com.arbiva.apfgc.provision.dao.CommonDAO;
import com.arbiva.apfgc.provision.dao.ProvRequestsDAO;
import com.arbiva.apfgc.provision.dto.AAAUpdateDTO;
import com.arbiva.apfgc.provision.dto.AIDDTO;
import com.arbiva.apfgc.provision.dto.APSFLDetailsDTO;
import com.arbiva.apfgc.provision.dto.AgoraActivatioVOIPDTO;
import com.arbiva.apfgc.provision.dto.AgoraActivationDTO;
import com.arbiva.apfgc.provision.dto.AgoraGPONActivationDTO;
import com.arbiva.apfgc.provision.dto.AgoraRegisterDTO;
import com.arbiva.apfgc.provision.dto.BlackListReq;
import com.arbiva.apfgc.provision.dto.CAFOutputDTO;
import com.arbiva.apfgc.provision.dto.CAFRequestDTO;
import com.arbiva.apfgc.provision.dto.CafProvisioningDTO;
import com.arbiva.apfgc.provision.dto.ChangeDeviceDTO;
import com.arbiva.apfgc.provision.dto.ChargeDetailsDTO;
import com.arbiva.apfgc.provision.dto.CustomerDTO;
import com.arbiva.apfgc.provision.dto.DhcpRelayDTO;
import com.arbiva.apfgc.provision.dto.HSIMultiCastProvDTO;
import com.arbiva.apfgc.provision.dto.IGMPOptionsDTO;
import com.arbiva.apfgc.provision.dto.IPTVCorpusProvReqResDTO;
import com.arbiva.apfgc.provision.dto.ONURebootDTO;
import com.arbiva.apfgc.provision.dto.ProvisionInfoDTO;
import com.arbiva.apfgc.provision.dto.ServicePackDTO;
import com.arbiva.apfgc.provision.dto.ServicePackageDTO;
import com.arbiva.apfgc.provision.dto.StbDTO;
import com.arbiva.apfgc.provision.dto.SubscriberRegistrationDTO;
import com.arbiva.apfgc.provision.dto.TPSDTO;
import com.arbiva.apfgc.provision.dto.TerminateDTO;
import com.arbiva.apfgc.provision.models.ChangeDevice;
import com.arbiva.apfgc.provision.models.ProvJsons;
import com.arbiva.apfgc.provision.models.ProvRequests;
import com.arbiva.apfgc.provision.utils.COMSConstants;
import com.arbiva.apfgc.provision.utils.DateUtils;
import com.arbiva.apfgc.provision.utils.VelocityEngineUtils;
import com.google.gson.Gson;

/**
 * 
 * @author srinivasa
 *
 */
@Component("provisioningBusinessServiceImpl")
public class ProvisioningBusinessServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(ProvisioningBusinessServiceImpl.class);

	@Autowired
	private CommonDAO commonDAO;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Autowired
	APSFLDetailsDTO apsflDTO;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	private ProvRequestsDAO provRequestsDAO;

	@Autowired
	private TelephonyProvBusinessServiceImpl telephonyProvBusinessServiceImpl;

	@Autowired
	private VelocityEngineUtils velocityEngineUtils;

	@Autowired
	OSDFingerPrintDetailsService oSDFingerPrintDetailsService;

	@Autowired
	CorpusAPIService corpusAPIServiceImpl;	

	@Value("${agora.server.ip.registercustomer.url}")
	private String IPAgoraServerRegisterCustomerUrl;

	@Value("${agora.server.ip.registercustomer.calltype}")
	private String IPAgoraServerRegisterCustomerCallType;

	@Value("${agora.server.ip.serviceactivation.url}")
	private String IPAgoraServerServiceActivationUrl;

	@Value("${agora.server.ip.serviceactivation.calltype}")
	private String IPAgoraServerServiceActivationCallType;

	@Value("${aaa.server.ip.radcheck.url}")
	private String IPAaaServerRadcheckUrl;

	@Value("${aaa.server.ip.radcheck.calltype}")
	private String IPAaaServerRadcheckCallType;

	@Value("${aaa.server.ip.radreply.url}")
	private String IPAaaServerRadreplyUrl;

	@Value("${aaa.server.ip.radreply.calltype}")
	private String IPAaaServerRadreplyCallType;

	@Value("${agora.server.hsi.multicast.url}")
	private String HSIAgoraMultiCastUrl;

	@Value("${agora.server.hsi.multicast.calltype}")
	private String HSIAgoraMultiCastCallType;

	@Value("${agora.server.iptv.gpon.client.activation.url}")
	private String IPTVAgoraGPONActivationUrl;

	@Value("${agora.server.iptv.gpon.client.activation.calltype}")
	private String IPTVAgoraGPONActivationCallType;

	@Value("${corpus.server.iptv.subscriber.registration.url}")
	private String IPTVCorpusRegistrationUrl;

	@Value("${corpus.server.iptv.subscriber.registration.calltype}")
	private String IPTVCorpusRegistrationCallType;

	@Value("${corpus.server.iptv.service.pack.url}")
	private String IPTVCorpusServicePackUrl;

	@Value("${corpus.server.iptv.service.pack.disconnect.url}")
	private String IPTVCorpusServicePackDisconnectUrl;

	@Value("${corpus.server.iptv.service.pack.renew.url}")
	private String IPTVCorpusServicePackReNewUrl;

	@Value("${corpus.server.iptv.service.pack.calltype}")
	private String IPTVCorpusServicePackCallType;

	@Value("${caf.form.dest.loc}")
	private String cafFormDestLocation;

	@Value("${model.zte.origianl.first4chars}")
	private String zteOrgChars;

	@Value("${model.zte.toreplace.first4chars}")
	private String zteToReplaceChars;

	@Value("${model.dasan.original.first4chars}")
	private String dasanOrgChars;

	@Value("${model.dasan.toreplace.first4chars}")
	private String dasanToReplaceChars;

	@Value("${corpus.server.iptv.service.pack.blackList.url}")
	private String blackListingURL;
	
	//added 21/02/17 for CPE change(ONU & STB)
	@Value("${agora.server.hsi.changeONU.url}")
	private String changeONUURL;
	
	@Value("${action.reboot}")
	private Boolean actionReboot;
	
	@Value("${agora.server.hsi.changeONU.calltype}")
	private String changeONUCalltype;
	
	@Value("${corpus.server.iptv.changeSTB.url}")
	private String changeSTBURL;
	
	@Value("${corpus.server.iptv.changeSTB.calltype}")
	private String changeSTBCalltype;
	
	@Value("${aaa.server.ip.radcheck.delete.url}")
	private String delAAAUrl;
	
	@Value("${agora.server.hsi.deleteGPON.url}")
	private String delHSIGponUrl;
	
	@Value("${agora.server.hsi.deleteONU.url}")
	private String delHSIOnuUrl;
	
	@Value("${agora.server.hsi.calltype}")
	private String delHSICalltype;
	
	@Value("${aaa.server.ip.radcheck.update.url}")
	private String updateAAAUrl;
	
	@Value("${corpus.server.iptv.disconnect.service.package.calltype}")
	private String terminateCallType;
	
	
	@Value("${corpus.server.iptv.disconnect.service.package.url}")
	private String terminateUrl;
	
	@Value("${agora.server.hsi.onuReboot.url}")
	private String reBootURL;
	
	@Value("${agora.server.hsi.onuReboot.calltype}")
	private String reBootCalltype;
	
	@Value("${aaa.server.ip.radcheck.AAAupdate.url}")
	private String urlForAAA;
	
	

	public void processProvisioningRequests(String prodcafNo, int cafoutputFlag) {
		List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
		try {
			LOGGER.info("Provisioning for activating alacart channels started:"+prodcafNo);
			listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(prodcafNo), "");
			fillProvisonTables(listOfProvisioningRequests, "A");
			if (listOfProvisioningRequests.size() > 0 && cafoutputFlag != 0)
				generateCAFOutputForm(prodcafNo, listOfProvisioningRequests.get(0).getAcctcafno());
			LOGGER.info("Provisioning for activating alacart channels ended:"+prodcafNo);
		} catch (Exception ex) {
			LOGGER.error(prodcafNo+" Exception for activating ala cart channels" + ex);
			throw ex;
		} finally {
			listOfProvisioningRequests = null;
		}
	}
	
	@Transactional
	public String fillProvisonTables(List<ProvisionInfoDTO> listOfProvisioningRequests, String type) {
		LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: START");
		Long dependentRequestId = null;
		Long iptvDependentRequestId = null;
		String status = "";
		String verifyCafno = "";
		boolean verifyHSICafno = true;
		int iptvSeqIncr = 0, cnt = 1;
		int seqNo = 3;
		Set<String> stbList = new HashSet<String>();
		try {
			for (ProvisionInfoDTO dto : listOfProvisioningRequests) {
				TimeZone tz = TimeZone.getTimeZone("GMT");
				Calendar now = Calendar.getInstance(tz);

				long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
				LOGGER.info("requestId 267 "+ requestId);
				if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.INTERNET_SERVICE_CODE) && verifyHSICafno) 
				{
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();

					// Define Provisioning Request Table
					ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

					//if(!verifyHSICafno.equalsIgnoreCase(String.valueOf(dto.getProdcafno())))
					//{
					dto.setDependencyRequestId(null);
					dependentRequestId = requestId;

					// DEFINE Agora Server Interface Register ONU
					ProvJsons agoraRegisterProvReq = createAgoraCustomerRegister(dto, requestId, now);
					provJsonList.add(agoraRegisterProvReq);

					// DEFINE Agora Server Interface GPON client service
					// activation
					ProvJsons agoraActivationProvReq = createAgoraServiceActivation(dto, requestId, now);
					provJsonList.add(agoraActivationProvReq);
					
					//added for VOIP-20/04/2017
					if(dto.getVoipGponFlag().equalsIgnoreCase("TRUE")){
						ProvJsons voipRequest = createAgoraServiceActivationForVOIP(dto, requestId, now, seqNo);
						provJsonList.add(voipRequest);
						seqNo = 4;
					}
					
					
				//Added on 13/03/17 for AAA-URL and delete Scheduler for AAA
					 String upstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSPEED ,dto.getSrvccodeaddl());
	                 String downstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWNSPEED ,dto.getSrvccodeaddl());
	                 String upAndDownProfileName = upstreamProfileName+"_"+downstreamProfileName;					 
					
	                 String accessId=commonDAO.getAcessId(Long.valueOf(dto.getProdcafno()));
	                 String macAddress=accessId.replaceAll(":", "").replaceAll("(.{4})(?!$)", "$1.");
	                 
	                // String clientName=commonDAO.getClientName(Long.valueOf(dto.getProdcafno()));
	                 String url = IPAaaServerRadcheckUrl.replace("nwsubscode", dto.getClient()).replace("model", macAddress).replace("UPDOWN",upAndDownProfileName);
	                 
					
					//mahesh-16-02-17
					// for AAA Server
					// Commented 
	                ProvJsons json= createAAAServiceActivation(requestId, now ,url,seqNo);
					provJsonList.add(json);

					provRequest.setProvJsons(provJsonList);

					provRequestsDAO.saveOrUpdate(provRequest);
					LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());
					//}

					verifyHSICafno = false;
				} 
				else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.IPTV_SERVICE_CODE))
				{
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
					if (iptvDependentRequestId == null) {
						dto.setDependencyRequestId(dependentRequestId);
						iptvDependentRequestId = requestId;
					} else
						dto.setDependencyRequestId(iptvDependentRequestId);

					// String agoraNwSubscriberCode =
					// commonDAO.getAgoraNWSubscriberCode(String.valueOf(dependentRequestId));

					// Define Provisioning Request Table
					ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

					if (verifyCafno.equalsIgnoreCase(String.valueOf(dto.getProdcafno())))
						iptvSeqIncr++;
					else
						iptvSeqIncr = 1;

					// Check whether network subscriber code is available or not
					// so that activation & registration can be sent
					String nwSubscriberCode = commonDAO.getCAFSrvcNWSubscriberCode(Long.valueOf(dto.getStbcafno()));
					if (nwSubscriberCode.equals("0") && !verifyCafno.equalsIgnoreCase(String.valueOf(dto.getProdcafno()))) 
					{
						// DEFINE MultiCast Package to an HSI Service
						ProvJsons agoraMultiCastProvReq = createAgoraMultiCastChannelPackage(dto, requestId, now);
						provJsonList.add(agoraMultiCastProvReq);

						// DEFINE IPTV Agora Server GPON Client Activation
						// Interface
						ProvJsons agoraRegisterProvReq = createAgoraGPONActivation(dto, requestId, now);
						provJsonList.add(agoraRegisterProvReq);

						// DEFINE IPTV Corpus Server Subscriber Registration
						// Interface Properties
						ProvJsons corpusRegistrationProvReq = createCorpusRegistration(dto, requestId, now, COMSConstants.REPLACE_AND_PROCESS);
						provJsonList.add(corpusRegistrationProvReq);
						stbList.add(dto.getStbcafno());

						iptvSeqIncr = 3;
						agoraMultiCastProvReq = null;
						agoraRegisterProvReq = null;
						corpusRegistrationProvReq = null;
					}

					if (!stbList.contains(dto.getStbcafno()) && !verifyHSICafno)
					{
						// DEFINE IPTV Corpus Server Subscriber Registration
						// Interface Properties
						ProvJsons corpusRegistrationProvReq = createCorpusRegistration(dto, requestId, now, COMSConstants.TO_BE_PROCESSED);
						provJsonList.add(corpusRegistrationProvReq);
						stbList.add(dto.getStbcafno());
					}

					// DEFINE IPTV Corpus Server Service Package Interface
					// Properties
					ProvJsons corpusServicePackageProvReq = createCorpusServicePackage(dto, requestId, now, nwSubscriberCode, type, iptvSeqIncr);
					provJsonList.add(corpusServicePackageProvReq);
					
					provRequest.setNwSubsCode(nwSubscriberCode);

					provRequest.setProvJsons(provJsonList);

					provRequestsDAO.saveOrUpdate(provRequest);
					LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());

					corpusServicePackageProvReq = null;
					provRequest = null;
					provJsonList = null;

					verifyCafno = String.valueOf(dto.getProdcafno());
				} 
				else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.TELEPHONY_SERVICE_CODE))
				{
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
					dto.setDependencyRequestId(dependentRequestId);

					// Define Provisioning Request Table
					ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

					String arrPhNos[] = dto.getPhoneNopwd().split(",");
					for (String phNoAndpwd : arrPhNos) 
					{
						String arrPhNoPwds[] = phNoAndpwd.split("-");
						dto.setPhoneNo(arrPhNoPwds[0]);
						dto.setPasswrd(arrPhNoPwds[1]);

						// Create PVI Activation Interface
						ProvJsons createPVIProvReq = telephonyProvBusinessServiceImpl.createPVI(dto, requestId, now, cnt);
						provJsonList.add(createPVIProvReq);
						cnt++;

						// Create PUI Activation Interface
						ProvJsons createPUIProvReq = telephonyProvBusinessServiceImpl.createPUI(dto, requestId, now, cnt);
						provJsonList.add(createPUIProvReq);
						cnt++;

						// Create Default PUI Activation Interface
						ProvJsons createDefaultPUIProvReq = telephonyProvBusinessServiceImpl.createDefaultPUI(dto, requestId, now, cnt);
						provJsonList.add(createDefaultPUIProvReq);
						cnt++;

						// Create SetImpRegSet Interface
						ProvJsons setImpRegSetProvReq = telephonyProvBusinessServiceImpl.setImpRegSet(dto, requestId, now, cnt);
						provJsonList.add(setImpRegSetProvReq);
						cnt++;

						// Create SetAliaseGrp Interface
						ProvJsons setAliaseGrpProvReq = telephonyProvBusinessServiceImpl.setAliaseGrp(dto, requestId, now, cnt);
						provJsonList.add(setAliaseGrpProvReq);
						cnt++;

						// Create Subscriber Interface
						ProvJsons addSubscriberFeaturesProvReq = telephonyProvBusinessServiceImpl.addSubscriberFeaturesProvReq(dto, requestId, now, cnt);
						provJsonList.add(addSubscriberFeaturesProvReq);
						cnt++;

						// Create OIP
						ProvJsons addOIP = telephonyProvBusinessServiceImpl.addOIP(dto, requestId, now, cnt);
						provJsonList.add(addOIP);
						cnt++;

						// Create OIP
						ProvJsons regUserPwd = telephonyProvBusinessServiceImpl.regUserPwd(dto, requestId, now, cnt);
						provJsonList.add(regUserPwd);
						cnt++;

						// Create OIP
						ProvJsons regOcb = telephonyProvBusinessServiceImpl.regOcb(dto, requestId, now, cnt);
						provJsonList.add(regOcb);
						cnt++;
					}

					provRequest.setProvJsons(provJsonList);

					provRequestsDAO.saveOrUpdate(provRequest);
					LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());

					provRequest = null;
					provJsonList = null;
				} 
				else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.VPN_SERVICE_CODE))
				{
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
					dto.setDependencyRequestId(dependentRequestId);

					// Define Provisioning Request Table
					ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

					if(verifyHSICafno) 
					{
						// DEFINE Agora Server Interface Create Register
						// Customer
						ProvJsons agoraRegisterProvReq = createAgoraCustomerRegister(dto, requestId, now);
						provJsonList.add(agoraRegisterProvReq);

						// DEFINE Agora Server Interface GPON client service
						// activation
						ProvJsons agoraActivationProvReq = createVPNAgoraServiceActivation(dto, requestId, now, 2);
						provJsonList.add(agoraActivationProvReq);
					}
					else
					{
						// DEFINE Agora Server Interface GPON client service
						// activation
						ProvJsons agoraActivationProvReq = createVPNAgoraServiceActivation(dto, requestId, now, 1);
						provJsonList.add(agoraActivationProvReq);
					}

					provRequest.setProvJsons(provJsonList);

					provRequestsDAO.saveOrUpdate(provRequest);
					LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());

					// agoraActivationProvReq = null;
					// agoraRegisterProvReq = null;
					provRequest = null;
					provJsonList = null;
				}
				LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: END");
				now = null;
				tz = null;
			}
			status = "success";
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + e);
			status = "fail";
			throw e;
		}

		return status;
	}

	public ProvRequests createProvisionRequest(ProvisionInfoDTO dto, long requestId, Calendar now, String type) {
		ProvRequests provRequest = new ProvRequests();
		try {
			// Define Provisioning Request Table
			provRequest.setRequestId(requestId);
			provRequest.setDpndrequestid(dto.getDependencyRequestId());
			provRequest.setCafNo(Long.parseLong(dto.getAcctcafno()));
			provRequest.setCustomerId(Long.parseLong(dto.getCustid()));
			provRequest.setProdCafNo(Long.parseLong(dto.getProdcafno()));
			provRequest.setStbCafNo(dto.getStbcafno() == null || dto.getStbcafno().isEmpty() ? 0 : Long.parseLong(dto.getStbcafno()));
			provRequest.setTenantCode("APSFL");
			provRequest.setProdCode(dto.getProdcode());
			provRequest.setServiceCodeAddl(dto.getSrvccodeaddl());
			provRequest.setServiceCodeProv(dto.getSrvccodeprov());
			if (type.equalsIgnoreCase("A"))
				provRequest.setRequestAction(COMSConstants.ACTIVATION);
			else if (type.equalsIgnoreCase("S"))
				provRequest.setRequestAction(COMSConstants.SUSPEND);
			else if (type.equalsIgnoreCase("R"))
				provRequest.setRequestAction(COMSConstants.RESUME);
			else if (type.equalsIgnoreCase("D"))
				provRequest.setRequestAction(COMSConstants.DEACTIVATE);
			provRequest.setStatus(COMSConstants.TO_BE_PROCESSED);
			provRequest.setCreatedOn(now.getTime());
			provRequest.setCreatedBy(COMSConstants.SYSTEM);
			provRequest.setModifiedOn(now.getTime());
			provRequest.setModifiedBy(COMSConstants.SYSTEM);
			provRequest.setNwSubsCode(dto.getNwSubscode());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createProvisionRequest() :: " + ex);
		}
		return provRequest;
	}

	public ProvJsons createAgoraCustomerRegister(ProvisionInfoDTO dto, long requestId, Calendar now) {
		AgoraRegisterDTO agoraRegisterDTO = null;
		ProvJsons agoraRegisterProvReq = null;
		Gson gson = null;
		String agoraRegisterJson = null;
		AIDDTO aidDTO = null;
		CustomerDTO customerDTO = null;
		String cpeSlno = "";
		String cafNo = dto.getProdcafno();
		String name = "";
		try {
			// DEFINE Agora Server Interface Create Register Customer
			agoraRegisterDTO = new AgoraRegisterDTO();
			agoraRegisterProvReq = new ProvJsons();
			aidDTO = new AIDDTO();

			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(dto.getCardnum());
			aidDTO.setTp(dto.getPortid());
			aidDTO.setOnuId(dto.getOnuid());

			agoraRegisterDTO.setAid(aidDTO);
			agoraRegisterDTO.setAdmin("1");
			agoraRegisterDTO.setFec("true");
			agoraRegisterDTO.setSwUpgradeMode("2");
			agoraRegisterDTO.setProfileName(dto.getModel());
			cpeSlno = dto.getCpeslno();
			if (cpeSlno.substring(0, 4).equalsIgnoreCase(zteOrgChars))
				agoraRegisterDTO.setSerialNumber(cpeSlno.replaceFirst(cpeSlno.substring(0, 4), zteToReplaceChars));
			else if (cpeSlno.substring(0, 4).equalsIgnoreCase(dasanOrgChars))
				agoraRegisterDTO.setSerialNumber(cpeSlno.replaceFirst(cpeSlno.substring(0, 4), dasanToReplaceChars));
			else
				agoraRegisterDTO.setSerialNumber(dto.getCpeslno());
			agoraRegisterDTO.setRegisterType("1");
			customerDTO = commonDAO.getCustomerInfo(Long.valueOf(dto.getAcctcafno()), dto.getCustomerType());
			name = customerDTO.getFirstname();
			name = name.length() > 50 ? name.substring(0, 50) : name;
			name = name.replaceAll("[^a-zA-Z0-9]+", "");
			cafNo = cafNo.length() > 10 ? cafNo.substring(0, 10) : cafNo;
			agoraRegisterDTO.setName(name + cafNo);

			gson = new Gson();
			agoraRegisterJson = gson.toJson(agoraRegisterDTO);

			agoraRegisterProvReq.setRequestId(requestId);
			agoraRegisterProvReq.setReq(agoraRegisterJson);
			agoraRegisterProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			agoraRegisterProvReq.setCallType(IPAgoraServerRegisterCustomerCallType);
			agoraRegisterProvReq.setUrl(IPAgoraServerRegisterCustomerUrl);
			agoraRegisterProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			agoraRegisterProvReq.setSeqNum(1);
			agoraRegisterProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraCustomerRegister() :: " + ex);
		} finally {
			agoraRegisterJson = null;
			gson = null;
			agoraRegisterDTO = null;
			aidDTO = null;
			cafNo = null;
			name = null;
		}
		return agoraRegisterProvReq;
	}

	public ProvJsons createAgoraServiceActivationForVOIP(ProvisionInfoDTO dto, long requestId, Calendar now, int seqNo) {
		ProvJsons agoraActivationProvReq = null;
		TPSDTO tpsDTO = null;
		List<Integer> tps = null;
		AgoraActivatioVOIPDTO agoraActivatioVOIPDTO = null;
		AIDDTO aidDTO = null;
		List<TPSDTO> tpsList = null;
		DhcpRelayDTO dhcpRelayDTO = null;
		String agoraActivationJson = null;
		IGMPOptionsDTO igmpOptionsDTO = null;
		Gson gson = null;
		try {
			agoraActivationProvReq = new ProvJsons();
			tpsDTO = new TPSDTO();
			aidDTO = new AIDDTO();
			tps = new ArrayList<Integer>();
			agoraActivatioVOIPDTO = new AgoraActivatioVOIPDTO();
			tpsList = new ArrayList<TPSDTO>();
			dhcpRelayDTO = new DhcpRelayDTO();
			igmpOptionsDTO = new IGMPOptionsDTO();

			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(dto.getCardnum());
			aidDTO.setTp(dto.getPortid());
			aidDTO.setOnuId(dto.getOnuid());
			
			
			String[] tpsArray = dto.getTps().split(",");
			for(String t : tpsArray)
				tps.add(Integer.parseInt(t));
			
			//tpsDTO.setCard(dto.getCardnum());
			tpsDTO.setCard("1");
			tpsDTO.setTps(tps);
			tpsList.add(tpsDTO);

			agoraActivatioVOIPDTO.setAid(aidDTO);
			agoraActivatioVOIPDTO.setTps(tpsList);
			agoraActivatioVOIPDTO.setAdmin(1);
			agoraActivatioVOIPDTO.setName("VOIP");
			agoraActivatioVOIPDTO.setNetworkServiceName(dto.getSrvccodeprov());
			agoraActivatioVOIPDTO.setNativeVlan("false");
			agoraActivatioVOIPDTO.setUniCtag(11);
			agoraActivatioVOIPDTO.setEncryption(true);
			agoraActivatioVOIPDTO.setIpManagement(true);
			

			String upstreamTrafficProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSTREAM_TRAFFIC_PROFILE_NAME, dto.getSrvccodeaddl());
			String downstreamTrafficProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWN_STREAM_TRAFFIC_PROFILE_NAME, dto.getSrvccodeaddl());
			agoraActivatioVOIPDTO.setUpstreamTrafficProfileName((upstreamTrafficProfileName == null) ? "" : upstreamTrafficProfileName);
			agoraActivatioVOIPDTO.setDownstreamTrafficProfileName((downstreamTrafficProfileName == null) ? "" : downstreamTrafficProfileName);
			upstreamTrafficProfileName = null;

			dhcpRelayDTO.setOp18(false);
			dhcpRelayDTO.setOp37(false);
			dhcpRelayDTO.setOp82(true);			
			dhcpRelayDTO.setRemoteId("ID");				
			dhcpRelayDTO.setUseGlobalDhcp(false);
			
			agoraActivatioVOIPDTO.setL2DhcpRelay(dhcpRelayDTO);

			igmpOptionsDTO.setUseGlobal(false);
			igmpOptionsDTO.setEnable(false);
			agoraActivatioVOIPDTO.setIgmpOptions(igmpOptionsDTO);
			
			//xyz rajesh s
		    if(agoraActivatioVOIPDTO.getName().equalsIgnoreCase("VOIP") && agoraActivatioVOIPDTO.getUpstreamTrafficProfileName().equalsIgnoreCase("APSFL_7mbps"))
		    	agoraActivatioVOIPDTO.setUpstreamTrafficProfileName("APSFL_1mbps");

			gson = new Gson();
			agoraActivationJson = gson.toJson(agoraActivatioVOIPDTO);
			agoraActivationProvReq.setRequestId(requestId);
			agoraActivationProvReq.setReq(agoraActivationJson);
			agoraActivationProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			agoraActivationProvReq.setCallType(IPTVAgoraGPONActivationCallType);
			agoraActivationProvReq.setUrl(IPTVAgoraGPONActivationUrl);
			agoraActivationProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			agoraActivationProvReq.setSeqNum(seqNo);
			agoraActivationProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraServiceActivationForVOIP() :: " + ex);
		} finally {
			agoraActivationJson = null;
			gson = null;
			dhcpRelayDTO = null;
			igmpOptionsDTO = null;
			tpsDTO = null;
			tps = null;
			aidDTO = null;
			tpsList = null;
			agoraActivatioVOIPDTO = null;
		}
		return agoraActivationProvReq;
	}
	
	public ProvJsons createAgoraServiceActivation(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons agoraActivationProvReq = null;
		TPSDTO tpsDTO = null;
		List<Integer> tps = null;
		AgoraActivationDTO agoraActivationDTO = null;
		AIDDTO aidDTO = null;
		List<TPSDTO> tpsList = null;
		DhcpRelayDTO dhcpRelayDTO = null;
		String agoraActivationJson = null;
		IGMPOptionsDTO igmpOptionsDTO = null;
		Gson gson = null;
		try {
			// DEFINE Agora Server Interface GPON client service activation
			agoraActivationProvReq = new ProvJsons();
			tpsDTO = new TPSDTO();
			aidDTO = new AIDDTO();
			tps = new ArrayList<Integer>();
			agoraActivationDTO = new AgoraActivationDTO();
			tpsList = new ArrayList<TPSDTO>();
			dhcpRelayDTO = new DhcpRelayDTO();
			igmpOptionsDTO = new IGMPOptionsDTO();

			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(dto.getCardnum());
			aidDTO.setTp(dto.getPortid());
			aidDTO.setOnuId(dto.getOnuid());
			
			
			String[] tpsArray = dto.getTps().split(",");
			for(String t : tpsArray)
				tps.add(Integer.parseInt(t));
			
			//tpsDTO.setCard(dto.getCardnum());
			tpsDTO.setCard("1");
			tpsDTO.setTps(tps);
			tpsList.add(tpsDTO);

			agoraActivationDTO.setAid(aidDTO);
			agoraActivationDTO.setTps(tpsList);
			agoraActivationDTO.setAdmin(1);
			agoraActivationDTO.setName(dto.getSrvccodeprov());
			agoraActivationDTO.setNetworkServiceName(dto.getSrvccodeprov());
			agoraActivationDTO.setNativeVlan(dto.getNativeVlan());
			// PENDING AS THIS NEEDS TO BE PICK FROM SERVICE PARAMS FOR
			// RESPECTIVE PARAMETER

			String upstreamTrafficProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSTREAM_TRAFFIC_PROFILE_NAME, dto.getSrvccodeaddl());
			String downstreamTrafficProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWN_STREAM_TRAFFIC_PROFILE_NAME, dto.getSrvccodeaddl());
			agoraActivationDTO.setUpstreamTrafficProfileName((upstreamTrafficProfileName == null) ? "" : upstreamTrafficProfileName);
			agoraActivationDTO.setDownstreamTrafficProfileName((downstreamTrafficProfileName == null) ? "" : downstreamTrafficProfileName);
			upstreamTrafficProfileName = null;

			dhcpRelayDTO.setOp18(false);
			dhcpRelayDTO.setOp37(false);
			dhcpRelayDTO.setOp82(true);			
			dhcpRelayDTO.setRemoteId("ID");				
			dhcpRelayDTO.setUseGlobalDhcp(false);
			
			agoraActivationDTO.setL2DhcpRelay(dhcpRelayDTO);

			igmpOptionsDTO.setUseGlobal(false);
			igmpOptionsDTO.setEnable(true);
			agoraActivationDTO.setIgmpOptions(igmpOptionsDTO);

			gson = new Gson();
			agoraActivationJson = gson.toJson(agoraActivationDTO);
			agoraActivationProvReq.setRequestId(requestId);
			agoraActivationProvReq.setReq(agoraActivationJson);
			agoraActivationProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			agoraActivationProvReq.setCallType(IPTVAgoraGPONActivationCallType);
			agoraActivationProvReq.setUrl(IPTVAgoraGPONActivationUrl);
			agoraActivationProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			agoraActivationProvReq.setSeqNum(2);
			agoraActivationProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraServiceActivation() :: " + ex);
		} finally {
			agoraActivationJson = null;
			gson = null;
			dhcpRelayDTO = null;
			igmpOptionsDTO = null;
			tpsDTO = null;
			tps = null;
			aidDTO = null;
			tpsList = null;
			agoraActivationDTO = null;
		}
		return agoraActivationProvReq;
	}
	
	//mahesh-16-02-17
	// for AAA server	
	public ProvJsons createAAAServiceActivation(long requestId, Calendar now ,String url, int seqNo) {
		ProvJsons aaaActivationProvReq = new ProvJsons();
		try{
			aaaActivationProvReq.setRequestId(requestId);
			aaaActivationProvReq.setReq("NULL");
			aaaActivationProvReq.setServiceType(COMSConstants.AAA_SERVICE_CODE);
			aaaActivationProvReq.setCallType(IPAaaServerRadcheckCallType);
			aaaActivationProvReq.setUrl(url);
			aaaActivationProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			aaaActivationProvReq.setSeqNum(seqNo);
			aaaActivationProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAAAServiceActivation() :: " + ex);
		}
		return aaaActivationProvReq;
	}
	
	public ProvJsons deleteHSIServiceONU(ProvisionInfoDTO dto,long requestId, Calendar now,int seqNo) {
		ProvJsons hsiReqDel = new ProvJsons();
		try{
			hsiReqDel.setRequestId(requestId);
			hsiReqDel.setReq("NULL");
			hsiReqDel.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			hsiReqDel.setCallType(delHSICalltype);
			
			String agoraNWSubId = dto.getNwSubscode().replace("-HSI", "");		
			
			if (!agoraNWSubId.equals("0")){
				hsiReqDel.setUrl(delHSIOnuUrl.concat(agoraNWSubId));
			}	
			
			hsiReqDel.setStatus(COMSConstants.TO_BE_PROCESSED);
			hsiReqDel.setSeqNum(seqNo);
			hsiReqDel.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: deleteHSIServiceONU() :: " + ex);
		}
		return hsiReqDel;
	}
	
	public ProvJsons deleteAgoraServiceVOIP(ProvisionInfoDTO dto,long requestId, Calendar now ,int seqNO) {
		ProvJsons agoraReqDel = new ProvJsons();
		try{
			agoraReqDel.setRequestId(requestId);
			agoraReqDel.setReq("NULL");
			agoraReqDel.setServiceType(COMSConstants.TELEPHONY_SERVICE_CODE);
			agoraReqDel.setCallType(delHSICalltype);
			String agoraNWSubId = dto.getNwSubscode().replace("HSI", "VOIP");	
			if (!agoraNWSubId.equals("0")){
				agoraReqDel.setUrl(delHSIGponUrl.concat(agoraNWSubId));
			}	
			agoraReqDel.setStatus(COMSConstants.TO_BE_PROCESSED);
			agoraReqDel.setSeqNum(seqNO);
			agoraReqDel.setCreatedDate(now.getTime());
			LOGGER.info("agoraNWSubId : " + agoraNWSubId);
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: deleteAgoraServiceVOIP() :: " + ex);
		}
		return agoraReqDel;
	}
	
	public ProvJsons deleteHSIServiceGPON(ProvisionInfoDTO dto,long requestId, Calendar now,int seqNO) {
		ProvJsons hsiReqDel = new ProvJsons();
		try{
			hsiReqDel.setRequestId(requestId);
			hsiReqDel.setReq("NULL");
			hsiReqDel.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			hsiReqDel.setCallType(delHSICalltype);			
			String agoraNWSubId = dto.getNwSubscode();		

			if (!agoraNWSubId.equals("0")){
				hsiReqDel.setUrl(delHSIGponUrl.concat(agoraNWSubId));
			}	
			
			hsiReqDel.setStatus(COMSConstants.TO_BE_PROCESSED);
			hsiReqDel.setSeqNum(seqNO);
			hsiReqDel.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: deleteHSIServiceGPON() :: " + ex);
		}
		return hsiReqDel;
	}
	
	//added 09/03/17
	public ProvJsons deleteHSIServiceGpon(String agoraHsiSbcsCode,long requestId, Calendar now, int seqNo) {
		ProvJsons hsiReqDel = new ProvJsons();
		try {
			hsiReqDel.setRequestId(requestId);
			hsiReqDel.setReq("NULL");
			hsiReqDel.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			hsiReqDel.setCallType(delHSICalltype);
			hsiReqDel.setUrl(delHSIGponUrl.concat(agoraHsiSbcsCode));
			hsiReqDel.setStatus(COMSConstants.TO_BE_PROCESSED);
			hsiReqDel.setSeqNum(seqNo);
			hsiReqDel.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: deleteHSIServiceGpon() :: " + ex);
		}
		return hsiReqDel;
	}
	
	
	public ProvJsons deleteAAAServiceActivation(long requestId, Calendar now ,ProvisionInfoDTO dto) {
		ProvJsons aaaDeletionReq = new ProvJsons();
		try{
			aaaDeletionReq.setRequestId(requestId);
			aaaDeletionReq.setReq("NULL");
			aaaDeletionReq.setServiceType(COMSConstants.AAA_SERVICE_CODE);
			aaaDeletionReq.setCallType(IPAaaServerRadcheckCallType);
			
			
		/*	String agoraNWSubId = dto.getNwSubscode();//172.16.242.99-1-14-31-HSI

			
			if (agoraNWSubId != null && !agoraNWSubId.equals("0") && !agoraNWSubId.isEmpty()){
				int n=agoraNWSubId.lastIndexOf('-');
				String s1=agoraNWSubId.substring(0, n);
				String nwSubId=s1.replace('-', ':');*/
				
				aaaDeletionReq.setUrl(delAAAUrl.replace("nwsubscode", dto.getClient()));
			//}
				
			aaaDeletionReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			aaaDeletionReq.setSeqNum(1);
			aaaDeletionReq.setCreatedDate(now.getTime());
		}catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: deleteAAAServiceActivation() :: " + ex);
		}
		return aaaDeletionReq;
	}
	

	public ProvJsons createVPNAgoraServiceActivation(ProvisionInfoDTO dto, long requestId, Calendar now, int seqno) {
		ProvJsons agoraActivationProvReq = null;
		TPSDTO tpsDTO = null;
		List<Integer> tps = null;
		AgoraActivationDTO agoraActivationDTO = null;
		AIDDTO aidDTO = null;
		List<TPSDTO> tpsList = null;
		// DhcpRelayDTO dhcpRelayDTO = null;
		String agoraActivationJson = null;
		IGMPOptionsDTO igmpOptionsDTO = null;
		Gson gson = null;
		try {
			// DEFINE Agora Server Interface GPON client service activation
			agoraActivationProvReq = new ProvJsons();
			tpsDTO = new TPSDTO();
			aidDTO = new AIDDTO();
			tps = new ArrayList<Integer>();
			agoraActivationDTO = new AgoraActivationDTO();
			tpsList = new ArrayList<TPSDTO>();
			// dhcpRelayDTO = new DhcpRelayDTO();
			 igmpOptionsDTO = new IGMPOptionsDTO();

			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(dto.getCardnum());
			aidDTO.setTp(dto.getPortid());
			aidDTO.setOnuId(dto.getOnuid());
			/*
			 * if(dto.getCpeslno().substring(0,
			 * 4).equalsIgnoreCase(zteOrgChars)) tps.add(new Integer(5)); else
			 * if(dto.getCpeslno().substring(0,
			 * 4).equalsIgnoreCase(dasanOrgChars)) tps.add(new Integer(6)); else
			 * tps.add(new Integer(6));
			 */
			//tps.add(new Integer(1));
			if (dto.getCpeslno().substring(0, 4).equalsIgnoreCase(zteOrgChars))
                tps.add(new Integer(1));
            else
                tps.add(new Integer(2));
			
			//tpsDTO.setCard(dto.getCardnum());
			tpsDTO.setCard("1");
			tpsDTO.setTps(tps);
			tpsList.add(tpsDTO);

			agoraActivationDTO.setAid(aidDTO);
			agoraActivationDTO.setTps(tpsList);
			agoraActivationDTO.setAdmin(1);
			agoraActivationDTO.setName(dto.getSrvccodeprov());
			agoraActivationDTO.setNetworkServiceName(dto.getVpnServiceName());
			// PENDING AS THIS NEEDS TO BE PICK FROM SERVICE PARAMS FOR
			// RESPECTIVE PARAMETER

			String upstreamTrafficProfileName = commonDAO
					.getServiceParamValue(COMSConstants.UPSTREAM_TRAFFIC_PROFILE_NAME, dto.getSrvccodeaddl());
			// String downstreamTrafficProfileName =
			// commonDAO.getServiceParamValue(COMSConstants.DOWN_STREAM_TRAFFIC_PROFILE_NAME,
			// dto.getSrvccodeaddl());
			agoraActivationDTO.setUpstreamTrafficProfileName(
					(upstreamTrafficProfileName == null) ? "" : upstreamTrafficProfileName);
			// agoraActivationDTO.setDownstreamTrafficProfileName((downstreamTrafficProfileName
			// == null) ? "" : downstreamTrafficProfileName);
			upstreamTrafficProfileName = null;
			// downstreamTrafficProfileName = null;

			// dhcpRelayDTO.setRemoteId("ID");
			// dhcpRelayDTO.setUseGlobalDhcp(true);
			// agoraActivationDTO.setL2DhcpRelay(dhcpRelayDTO);

			//PRIYA 9-29-2017
			 igmpOptionsDTO.setUseGlobal(false);
			 igmpOptionsDTO.setEnable(false);
			 agoraActivationDTO.setIgmpOptions(igmpOptionsDTO);
			 //agoraActivationDTO.setNativeVlan(dto.getNativeVlan());
			 agoraActivationDTO.setNativeVlan(String.valueOf(true));
			 agoraActivationDTO.setEncryption(String.valueOf(true));
			//PRIYA 9-29-2017
			gson = new Gson();
			agoraActivationJson = gson.toJson(agoraActivationDTO);
			agoraActivationProvReq.setRequestId(requestId);
			agoraActivationProvReq.setReq(agoraActivationJson);
			agoraActivationProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			agoraActivationProvReq.setCallType(IPTVAgoraGPONActivationCallType);
			agoraActivationProvReq.setUrl(IPTVAgoraGPONActivationUrl);
			agoraActivationProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			agoraActivationProvReq.setSeqNum(seqno);
			agoraActivationProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraServiceActivation() :: " + ex);
		} finally {
			agoraActivationJson = null;
			gson = null;
			// dhcpRelayDTO = null;
			// igmpOptionsDTO = null;
			tpsDTO = null;
			tps = null;
			aidDTO = null;
			tpsList = null;
			agoraActivationDTO = null;
		}
		return agoraActivationProvReq;
	}

	public ProvJsons createAgoraMultiCastChannelPackage(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons agoraMultiChannelProvReq = null;
		AgoraActivationDTO agoraActivationDTO = null;
		String agoraActivationJson = null;
		Gson gson = null;
		try {
			// DEFINE Agora Server Interface Multi Channel Package
			agoraMultiChannelProvReq = new ProvJsons();
			agoraActivationDTO = new AgoraActivationDTO();
			agoraActivationDTO.setName("IPTV");

			gson = new Gson();
			agoraActivationJson = gson.toJson(agoraActivationDTO);
			agoraMultiChannelProvReq.setRequestId(requestId);
			agoraMultiChannelProvReq.setReq(agoraActivationJson);
			agoraMultiChannelProvReq.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			agoraMultiChannelProvReq.setCallType(HSIAgoraMultiCastCallType);
			agoraMultiChannelProvReq.setUrl(HSIAgoraMultiCastUrl);
			agoraMultiChannelProvReq.setStatus(COMSConstants.REPLACE_AND_PROCESS);
			agoraMultiChannelProvReq.setSeqNum(0);
			agoraMultiChannelProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraMultiCastChannelPackage() :: " + ex);
		} finally {
			agoraActivationJson = null;
			gson = null;
			agoraActivationDTO = null;
		}
		return agoraMultiChannelProvReq;
	}

	/*public ProvJsons createAAARedcheck(ProvisionInfoDTO dto, long requestId, Calendar now) {
		CustomerDTO customerDTO = null;
		AAAUserDTO aaaUserDTO = null;
		AIDDTO aidDTO = null;
		String aaaUserJson = null;
		ProvJsons aaaUserProvReq = null;
		Gson gson = null;
		try {
			// DEFINE AAA (Radius) Server Interface for Radcheck table
			customerDTO = commonDAO.getCustomerInfo(Long.valueOf(dto.getCustid()), dto.getCustomerType());
			aaaUserDTO = new AAAUserDTO();

			aidDTO = new AIDDTO();
			// String ipAddr =
			// commonDAO.getServiceParamValue(COMSConstants.AAA_IPADDRESS,
			// dto.getCoresrvccode());
			// aidDTO.setIpAddress((ipAddr == null) ? "" : ipAddr);
			// ipAddr = null;
			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(null);
			aidDTO.setTp(null);
			aidDTO.setOnuId(null);

			aaaUserDTO.setAid(aidDTO);
			aaaUserDTO.setUsername(customerDTO.getAadharno());
			aaaUserDTO.setPassword(customerDTO.getDob().replaceAll("/", "").replaceAll("-", ""));

			gson = new Gson();
			aaaUserJson = gson.toJson(aaaUserDTO);

			aaaUserProvReq = new ProvJsons();
			aaaUserProvReq.setRequestId(requestId);
			aaaUserProvReq.setReq(aaaUserJson);
			aaaUserProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			aaaUserProvReq.setCallType(IPAaaServerRadcheckCallType);
			aaaUserProvReq.setUrl(IPAaaServerRadcheckUrl);
			aaaUserProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			aaaUserProvReq.setSeqNum(3);
			aaaUserProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAAARedcheck() :: " + ex);
		} finally {
			aaaUserJson = null;
			gson = null;
			customerDTO = null;
			aaaUserDTO = null;
			aidDTO = null;
		}
		return aaaUserProvReq;
	}

	public ProvJsons createAAARedreply(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons aaaUserPolicyProvReq = null;
		AAAUserPolicyDTO aaaUserPolicyDTO = null;
		AIDDTO aidDTO = null;
		CustomerDTO customerDTO = null;
		String aaaUserPolicyJson = null;
		Gson gson = null;
		try {
			// DEFINE AAA (Radius) Server Interface for Radreply table
			aaaUserPolicyProvReq = new ProvJsons();
			aaaUserPolicyDTO = new AAAUserPolicyDTO();
			aidDTO = new AIDDTO();

			// String ipAddr =
			// commonDAO.getServiceParamValue(COMSConstants.AAA_IPADDRESS,
			// dto.getCoresrvccode());
			// aidDTO.setIpAddress((ipAddr == null) ? "" : ipAddr);
			// ipAddr = null;
			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(null);
			aidDTO.setTp(null);
			aidDTO.setOnuId(null);

			customerDTO = commonDAO.getCustomerInfo(Long.valueOf(dto.getCustid()), dto.getCustomerType());

			aaaUserPolicyDTO.setAid(aidDTO);
			aaaUserPolicyDTO.setUsername(customerDTO.getAadharno());
			String downloadLimit = commonDAO.getServiceParamValue(COMSConstants.DOWNLOAD_LIMIT, dto.getSrvccodeaddl());
			String downloadLimitUnit = commonDAO.getServiceParamValue(COMSConstants.DOWNLOAD_LIMIT_UNIT,
					dto.getSrvccodeaddl());
			String uploadLimit = commonDAO.getServiceParamValue(COMSConstants.UPLOAD_LIMIT, dto.getSrvccodeaddl());
			String uploadLimitUnit = commonDAO.getServiceParamValue(COMSConstants.UPLOAD_LIMIT_UNIT,
					dto.getSrvccodeaddl());

			aaaUserPolicyDTO.setDownloadlimit((downloadLimit == null) ? "" : downloadLimit);
			aaaUserPolicyDTO.setDownloadlimitunit((downloadLimitUnit == null) ? "" : downloadLimitUnit);
			aaaUserPolicyDTO.setUploadlimit((uploadLimit == null) ? "" : uploadLimit);
			aaaUserPolicyDTO.setUploadlimitunit((uploadLimitUnit == null) ? "" : uploadLimitUnit);
			downloadLimit = null;
			downloadLimitUnit = null;
			uploadLimit = null;
			uploadLimitUnit = null;

			gson = new Gson();
			aaaUserPolicyJson = gson.toJson(aaaUserPolicyDTO);

			aaaUserPolicyProvReq.setRequestId(requestId);
			aaaUserPolicyProvReq.setReq(aaaUserPolicyJson);
			aaaUserPolicyProvReq.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			aaaUserPolicyProvReq.setCallType(IPAaaServerRadreplyCallType);
			aaaUserPolicyProvReq.setUrl(IPAaaServerRadreplyUrl);
			aaaUserPolicyProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
			aaaUserPolicyProvReq.setSeqNum(4);
			aaaUserPolicyProvReq.setCreatedDate(now.getTime());

		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAAARedreply() :: " + ex);
		} finally {
			aaaUserPolicyJson = null;
			gson = null;
			aaaUserPolicyDTO = null;
			aidDTO = null;
			customerDTO = null;
		}
		return aaaUserPolicyProvReq;
	}*/

	public ProvJsons createAgoraGPONActivation(ProvisionInfoDTO dto, long requestId, Calendar now) {
		ProvJsons agoraGPONActivationProvReq = null;
		TPSDTO tpsDTO = null;
		List<Integer> tps = null;
		AgoraGPONActivationDTO agoraGPONActivationDTO = null;
		AIDDTO aidDTO = null;
		List<TPSDTO> tpsList = null;
		String agoraGPONActivationJson = null;
		Gson gson = null;
		try {
			// DEFINE Agora Server Interface GPON client service activation
			agoraGPONActivationProvReq = new ProvJsons();
			tpsDTO = new TPSDTO();
			aidDTO = new AIDDTO();
			tps = new ArrayList<Integer>();
			agoraGPONActivationDTO = new AgoraGPONActivationDTO();
			tpsList = new ArrayList<TPSDTO>();

			aidDTO.setIpAddress(dto.getIpaddress());
			aidDTO.setCard(dto.getCardnum());
			aidDTO.setTp(dto.getPortid());
			aidDTO.setOnuId(dto.getOnuid());

			/*if (dto.getCpeslno().substring(0, 4).equalsIgnoreCase(zteOrgChars))
				tps.add(new Integer(5));
			else if (dto.getCpeslno().substring(0, 4).equalsIgnoreCase(dasanOrgChars))
				tps.add(new Integer(6));
			else
				tps.add(new Integer(6));*/
			String[] tpsArray = dto.getTps().split(",");
			for(String t : tpsArray)
				tps.add(Integer.parseInt(t));
			
			//tpsDTO.setCard(dto.getCardnum());
			tpsDTO.setCard("1");
			tpsDTO.setTps(tps);
			tpsList.add(tpsDTO);

			agoraGPONActivationDTO.setAid(aidDTO);
			agoraGPONActivationDTO.setTps(tpsList);
			agoraGPONActivationDTO.setAdmin("1");
			agoraGPONActivationDTO.setNetworkServiceName("IPTV");
			agoraGPONActivationDTO.setName("IPTV");
			agoraGPONActivationDTO.setNativeVlan(dto.getNativeVlan());

			gson = new Gson();
			agoraGPONActivationJson = gson.toJson(agoraGPONActivationDTO);
			agoraGPONActivationProvReq.setRequestId(requestId);
			agoraGPONActivationProvReq.setReq(agoraGPONActivationJson);
			agoraGPONActivationProvReq.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			agoraGPONActivationProvReq.setCallType(IPAgoraServerServiceActivationCallType);
			agoraGPONActivationProvReq.setUrl(IPAgoraServerServiceActivationUrl);
			agoraGPONActivationProvReq.setStatus(COMSConstants.REPLACE_AND_PROCESS);
			agoraGPONActivationProvReq.setSeqNum(1);
			agoraGPONActivationProvReq.setCreatedDate(now.getTime());
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createAgoraGPONActivation() :: " + ex);
		} finally {
			agoraGPONActivationJson = null;
			gson = null;
			tpsDTO = null;
			tps = null;
			aidDTO = null;
			tpsList = null;
			agoraGPONActivationDTO = null;
		}
		return agoraGPONActivationProvReq;
	}

	public ProvJsons createCorpusRegistration(ProvisionInfoDTO dto, long requestId, Calendar now, int status) {
		ProvJsons corpusRegistrationProvReq = null;
		SubscriberRegistrationDTO subscriberRegistrationDTO = null;
		CustomerDTO customerDTO = null;
		String corpusRegistrationJson = null;
		Gson gson = null;
		try {
			// DEFINE AAA (Radius) Server Interface for Radreply table
			corpusRegistrationProvReq = new ProvJsons();
			subscriberRegistrationDTO = new SubscriberRegistrationDTO();

			customerDTO = commonDAO.getCustomerInfo(Long.valueOf(dto.getAcctcafno()), dto.getCustomerType());
			//customerDTO.setAadharno(null);
			//customerDTO.setDob(null);
			subscriberRegistrationDTO.setDeviceid(dto.getStbMacAddr());
			subscriberRegistrationDTO.setDevicecategory(COMSConstants.DEVICE_CATEGORY);
			subscriberRegistrationDTO.setSubscriber(customerDTO);

			gson = new Gson();
			corpusRegistrationJson = gson.toJson(subscriberRegistrationDTO);

			corpusRegistrationProvReq.setRequestId(requestId);
			corpusRegistrationProvReq.setReq(corpusRegistrationJson);
			corpusRegistrationProvReq.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			corpusRegistrationProvReq.setCallType(IPTVCorpusRegistrationCallType);
			corpusRegistrationProvReq.setUrl(IPTVCorpusRegistrationUrl);
			corpusRegistrationProvReq.setStatus(status);
			corpusRegistrationProvReq.setSeqNum(2);
			corpusRegistrationProvReq.setCreatedDate(now.getTime());

		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createCorpusRegistration() :: " + ex);
		} finally {
			corpusRegistrationJson = null;
			gson = null;
			subscriberRegistrationDTO = null;
			customerDTO = null;
		}
		return corpusRegistrationProvReq;
	}

	public ProvJsons createCorpusServicePackage(ProvisionInfoDTO dto, long requestId, Calendar now,String nwSubscriberCode, String type, int incr) {
		ProvJsons corpusServicePackageProvReq = null;
		ServicePackageDTO servicePackageDTO = null;
		ServicePackDTO servicePackDTO = null;
		String corpusServicePackageJson = null;
		Gson gson = null;
		List<ServicePackDTO> servicePackList = null;
		SimpleDateFormat dbdate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat jsondate = new SimpleDateFormat("yyyyMMdd");
		try {
			// DEFINE IPTV Corpus Server Service Package Interface Properties
			corpusServicePackageProvReq = new ProvJsons();
			servicePackageDTO = new ServicePackageDTO();
			servicePackList = new ArrayList<ServicePackDTO>();

			commonDAO.getProductNames(Long.valueOf(dto.getProdcafno()));
			// for(String productName: productNameList) {
			servicePackDTO = new ServicePackDTO();
			// servicePackDTO.setServicepack(productName);
			servicePackDTO.setServicepack(dto.getSrvccodeaddl());
			if (!type.equalsIgnoreCase("S") && !type.equalsIgnoreCase("D")) {
				if (dto.getExpDate() != null && !dto.getExpDate().isEmpty())
					servicePackDTO.setExpirydate(jsondate.format(dbdate.parse(dto.getExpDate())));
				else
					servicePackDTO.setExpirydate(COMSConstants.IPTV_EXPIRY_DATE);
			}

			servicePackList.add(servicePackDTO);
			servicePackDTO = null;
			// }
			if (nwSubscriberCode.equals("0"))
				servicePackageDTO.setSubscribercode("SUBSCRIBER_CODE");
			else
				servicePackageDTO.setSubscribercode(nwSubscriberCode);

			servicePackageDTO.setServicepacks(servicePackList);

			gson = new Gson();
			corpusServicePackageJson = gson.toJson(servicePackageDTO);

			corpusServicePackageProvReq.setRequestId(requestId);
			corpusServicePackageProvReq.setReq(corpusServicePackageJson);
			corpusServicePackageProvReq.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			corpusServicePackageProvReq.setCallType(IPTVCorpusServicePackCallType);
			if (type.equalsIgnoreCase("S") || type.equalsIgnoreCase("D"))
				corpusServicePackageProvReq.setUrl(IPTVCorpusServicePackDisconnectUrl);
			else if (type.equalsIgnoreCase("R"))
				corpusServicePackageProvReq.setUrl(IPTVCorpusServicePackReNewUrl);
			else if (type.equalsIgnoreCase("A"))
				corpusServicePackageProvReq.setUrl(IPTVCorpusServicePackUrl);

			if (nwSubscriberCode.equals("0"))
				corpusServicePackageProvReq.setStatus(COMSConstants.TO_BE_REPLACED_AND_PROCESSED);
			else
				corpusServicePackageProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);

			corpusServicePackageProvReq.setSeqNum(incr);
			corpusServicePackageProvReq.setCreatedDate(now.getTime());

		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: createCorpusServicePackage() :: " + ex);
		} finally {
			corpusServicePackageJson = null;
			gson = null;
			servicePackageDTO = null;
			servicePackList = null;
			servicePackageDTO = null;
		}
		return corpusServicePackageProvReq;
	}

	/**
	 * 
	 */
	public void processIPTVCorpusServicePacks() {
		List<IPTVCorpusProvReqResDTO> listOfIPTVCorpusProvResponses = null;
		// SubRegResponseDTO respDto = null;
		String nwSubsciberCode = null;
		String requestJson = null;
		try {
			listOfIPTVCorpusProvResponses = commonDAO.getIPTVCorpusPrevProvJSONResponse();

			for (IPTVCorpusProvReqResDTO dto : listOfIPTVCorpusProvResponses) {
				// ObjectMapper mapper = new ObjectMapper();
				if (dto.getResponse() != null && !dto.getResponse().isEmpty()) {
					nwSubsciberCode = dto.getResponse();

					// dto = commonDAO.getIPTVCorpusProvJSON(dto.getRequestid(),
					// dto.getSeqnum()+1);
					requestJson = dto.getRequest().replace("SUBSCRIBER_CODE", nwSubsciberCode);

					// Update Subscriber Code from Previous JSON
					provRequestsDAO.updateProvJsons(Long.valueOf(dto.getRequestid()), requestJson, dto.getSeqnum());
					// Update nwsubscode in olprovrequests
					provRequestsDAO.updateNwsubscodeProvRequests(Long.valueOf(dto.getRequestid()), nwSubsciberCode);
					// Update Request Status Back to "TO_BE_PROCESSED
					provRequestsDAO.updateProvRequestStatus(Long.valueOf(dto.getRequestid()));
					// Update Dunning Caf for temporary
					provRequestsDAO.updateDunningCafs(Long.valueOf(dto.getRequestid()));
				}

				// mapper = null;
				// respDto = null;
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in processIPTVCorpusServicePacks: " + ex);
		} finally {
			listOfIPTVCorpusProvResponses = null;
			nwSubsciberCode = null;
			requestJson = null;
		}
	}

	/**
	 * 
	 */
	public void processHSIMultiCastPackages() {
		List<HSIMultiCastProvDTO> list = null;
		try {
			list = commonDAO.getHSIMultiCastRequests();

			for (HSIMultiCastProvDTO dto : list) {
				// Update Agora NW Susbcriber Code from Previous JSON
				String agoraNWSubId = commonDAO.getAgoraNWSubscriberCode(String.valueOf(dto.getDpndrequestid()));
				if (!agoraNWSubId.equals("0")) {
					provRequestsDAO.updateProvJsonUrl(Long.valueOf(dto.getRequestid()),
							dto.getUrl().replace("HSIID", agoraNWSubId), dto.getSeq());
					// Update Request Status Back to "TO_BE_PROCESSED
					provRequestsDAO.updateProvRequestStatus(Long.valueOf(dto.getRequestid()));
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: processHSIMultiCastPackages() :: " + ex);
		} finally {
			list = null;
		}
	}
	
	//commented on 13/03/17
	//mahesh-16-02-17 for AAA Server
	/*public void processAAAServerService() {
		List<HSIMultiCastProvDTO> list = null;
		List<HSIMultiCastProvDTO> listAAA = null;
		
		try {
			list = commonDAO.getAAAServiceRequests();

			for (HSIMultiCastProvDTO dto : list) {
				// Update Agora NW Susbcriber Code from Previous JSON
				String agoraNWSubId = dto.getNwsubscode();
				
				if (!agoraNWSubId.equals("0")){
					int n=agoraNWSubId.lastIndexOf('-');
					String s1=agoraNWSubId.substring(0, n);
					String nwSubId=s1.replace('-', ':');
					
					listAAA=commonDAO.getProfileANDSrvcCode(agoraNWSubId,COMSConstants.INTERNET_SERVICE_CODE);
					//for (HSIMultiCastProvDTO dtoHSI : listAAA){
						profileName= listAAA.get(0).getProfileName();
						addlSrvcCode=  listAAA.get(0).getAddlSrvcCode();
					//}
					
					  String upstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSPEED ,dto.getAddlSrvcCode());
	                  String downstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWNSPEED ,dto.getAddlSrvcCode());
	                  String upAndDownProfileName = upstreamProfileName+"_"+downstreamProfileName;
					
					provRequestsDAO.updateProvJsonUrlForAAA(Long.valueOf(dto.getRequestid()),
							dto.getUrl().replace("nwsubscode", nwSubId).replace("model",dto.getProfileName()).replace("UPDOWN",upAndDownProfileName), dto.getSeq());
					// Update Request Status Back to "TO_BE_PROCESSED"
					provRequestsDAO.updateProvRequestStatus(Long.valueOf(dto.getRequestid()));
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: processAAAServerService() :: " + ex);
		} finally {
			list = null;
		}
	}*/
	

	public void postProvisioningActivities() {
		List<CAFRequestDTO> cafRequestList = null;
		Map<String, String> cafWiseReqCount = new LinkedHashMap<String, String>();
		Map<String, String> cafWiseActCount = new LinkedHashMap<String, String>();
		try {
			cafWiseReqCount = commonDAO.getCafWiseReqCount();
			cafWiseActCount = commonDAO.getCafWiseActCount();
			for (Map.Entry<String, String> entry : cafWiseActCount.entrySet())
			{
				String reqCount = cafWiseReqCount.get(entry.getKey());
				if (reqCount.equalsIgnoreCase(entry.getValue()))
				{
					cafRequestList = commonDAO.getCAFList(COMSConstants.INTERNET_SERVICE_CODE, COMSConstants.IPTV_SERVICE_CODE, entry.getKey());
					for (CAFRequestDTO dto : cafRequestList) 
					{
						String output = commonDAO.executePostProvisionProcess(dto);
						LOGGER.info("postactvnupd SP OUTPUT: " + output);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: postProvisioningActivities() :: " + ex);
		}
	}

	public String generateCAFOutputForm(String prodcafno, String acctcafno) {
		String cafOPFile = null;
		String template = null;
		String res = null;
		new ChargeDetailsDTO();

		try (InputStream is1 = ProvisioningBusinessServiceImpl.class
				.getResourceAsStream("/templates/APSFL _Revised_Logo.png");
				InputStream is2 = ProvisioningBusinessServiceImpl.class
						.getResourceAsStream("/templates/APFIBER_Revised_Logo.png")) {
			// System.out.println("hello");
			final byte[] content = Base64.encodeBase64(IOUtils.toByteArray(is1));
			final byte[] content1 = Base64.encodeBase64(IOUtils.toByteArray(is2));
			final CAFOutputDTO cafOutputDTO = commonDAO.getCAFOutputDetails(prodcafno, acctcafno);
			final List<ChargeDetailsDTO> chargelist = cafOutputDTO.getChargeList();
			final List<ChargeDetailsDTO> subscriptionList = cafOutputDTO.getSubscriptionList();
			final List<StbDTO> stbList = cafOutputDTO.getStbList();
			try {
				template = velocityEngineUtils.getTemplateData(new VelocityContext() {
					{
						put("caf", cafOutputDTO);
						put("chargelist", chargelist);
						put("subscriptionList", subscriptionList);
						put("stbList", stbList);
						put("apsfl", apsflDTO);
						put("StringUtils", new StringUtils());
						put("image_content", new String(content));
						put("image_content_1", new String(content1));
					}
				}, "templates/velocity.vm");
				cafOPFile = getCafFormDestFile(cafOutputDTO.getCafno());
				com.arbiva.apfgc.provision.utils.FileUtils.write(template, cafOPFile);
				// mailManager.sendMail(cafOPFile);
				res = "success";
			} catch (Exception e) {
				LOGGER.error("Exception occurred in generateCAFOutputForm: " + e);
				// TODO Auto-generated catch block
				e.printStackTrace();
				res = "fail";
			}

		} catch (Exception ex) {
			LOGGER.error("Exception occurred in generateCAFOutputForm update or sendEmail: " + ex);
		} finally {
			cafOPFile = null;
			template = null;
		}
		return res;
	}

	private String getCafFormDestFile(String registerId) {
		String destLocation = String.format(cafFormDestLocation.endsWith("/") ? "%s%s" : "%s//%s//",
				cafFormDestLocation, DateUtils.getCurrentYearMonth());
		createDirectoryIfnotExists(destLocation);
		return String.format("%s/%s_%s.html", destLocation, registerId, DateUtils.getCurrentTimeStamp());
	}

	private void createDirectoryIfnotExists(String directory) {
		try {
			new File(directory).mkdirs();
		} catch (Exception e) {
			LOGGER.error("Exception occurred while creating caf form destination directory: " + e);
		}
	}

	/*
	public void suspendIPTVCorpusServicePacks() {
		List<DunningCafDTO> dunningCafList = null;
		try {
			dunningCafList = commonDAO.getSuspendedCafs();

			for (DunningCafDTO dto : dunningCafList) {

				String checkCnt = commonDAO.getOLPaymentForSuspension(dto.getAcctcafno());
				if (Integer.parseInt(checkCnt) > 0) {
					commonDAO.updateDunningCaf(dto.getAcctcafno(), "S");
				} else {
					commonDAO.updateDunningCafStatus(dto.getAcctcafno());
					List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
					listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(dto.getAcctcafno()), "IPTV", "");
					fillProvisonTables(listOfProvisioningRequests, "S");
					// commonDAO.updateCafSrvcs(dto.getAcctcafno(),
					// listOfProvisioningRequests.get(0).getSrvccodeaddl(),"S");
					// commonDAO.updateCafProd(dto.getAcctcafno(),"S");
					// commonDAO.updateCaf(dto.getAcctcafno(),"S");
					listOfProvisioningRequests = null;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in suspendIPTVCorpusServicePacks: " + ex);
		} finally {
			dunningCafList = null;
		}
	}

	*//**
	 * 
	 *//*
	public void resumeIPTVCorpusServicePacks() {
		List<DunningCafDTO> dunningCafList = null;
		try {
			dunningCafList = commonDAO.getReadyForResumeCafs();

			for (DunningCafDTO dto : dunningCafList) {

				String checkCnt = commonDAO.getOLPaymentForSuspension(dto.getAcctcafno());
				if (Integer.parseInt(checkCnt) > 0) {
					commonDAO.updateDunningCaf(dto.getAcctcafno(), "R");
					List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
					listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(dto.getAcctcafno()), "IPTV", "");
					fillProvisonTables(listOfProvisioningRequests, "R");
					// commonDAO.updateCafSrvcs(dto.getAcctcafno(),
					// listOfProvisioningRequests.get(0).getSrvccodeaddl(),"R");
					// commonDAO.updateCafProd(dto.getAcctcafno(),"R");
					// commonDAO.updateCaf(dto.getAcctcafno(),"R");
					listOfProvisioningRequests = null;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in resumeIPTVCorpusServicePacks: " + ex);
		} finally {
			dunningCafList = null;
		}
	}

	public void deActivateIPTVCorpusServicePacks() {
		List<DunningCafDTO> dunningCafList = null;
		try {
			dunningCafList = commonDAO.getdeActivatedCafs();

			for (DunningCafDTO dto : dunningCafList) {
				List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
				listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(dto.getAcctcafno()), "IPTV", "");
				fillProvisonTables(listOfProvisioningRequests, "D");
				// commonDAO.updateCafSrvcs(dto.getAcctcafno(),
				// listOfProvisioningRequests.get(0).getSrvccodeaddl(),"D");
				// commonDAO.updateCafProd(dto.getAcctcafno(),"D");
				// commonDAO.updateCaf(dto.getAcctcafno(),"D");
				listOfProvisioningRequests = null;
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in deActivateIPTVCorpusServicePacks: " + ex);
		} finally {
			dunningCafList = null;
		}
	}*/

	public String blacklistSubscribers(String nwsubscode) {
		String status = "";
		List<String> nwSubscriberCodeList = new ArrayList<>();
		// String nwSubscriberCode = "";
		BlackListReq blackListReq = new BlackListReq();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<BlackListReq> httpEntity = null;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		String requestJson = "";
		String responseJson = "";
		String corpusStatus = "";
		CorpusResponce corpusResponce = new CorpusResponce();
		ObjectMapper mapper = new ObjectMapper();
		Gson gson = new Gson();
		OSDFingerPrintDetails blackListDetails = new OSDFingerPrintDetails();
		String userName = "";
		try {
			LOGGER.info("ProvisioningBusinessServiceImpl :: blacklistSubscribers() :: START");
			/*
			 * for (String cafNo : cafList) {
			 * commonDAO.updateCafBlackListing(cafNo); nwSubscriberCode =
			 * commonDAO.getNwSubsciberCodeList(cafNo);
			 * if(!nwSubscriberCode.isEmpty())
			 * nwSubscriberCodeList.add(nwSubscriberCode); }
			 */
			nwSubscriberCodeList.add(nwsubscode);
			blackListReq.setSubscriberCodes(nwSubscriberCodeList);
			headers.add("username", "teraadmin");
			headers.add("apikey", "6ed73c1a-7817-49ab-b185-981f97cf5fd8");
			httpEntity = new HttpEntity<BlackListReq>(blackListReq, headers);
			response = restTemplate.exchange(blackListingURL, HttpMethod.POST, httpEntity, String.class);
			corpusStatus = response.getBody();
			corpusResponce = mapper.readValue(corpusStatus, CorpusResponce.class);
			requestJson = gson.toJson(blackListReq);
			responseJson = gson.toJson(corpusResponce);
			if (corpusResponce.getResponseStatus().getStatusCode().equalsIgnoreCase("202")) {
				blackListDetails = oSDFingerPrintDetailsService.saveOSDFingerPrintDetails(
						blackListReq.getSubscriberCodes().toString(), requestJson, responseJson, userName, 0,
						"blacklist", corpusResponce.getTrackingId(), "NA");
				if (blackListDetails != null)
					status = "ACCEPTED";
				else {
					status = "FAILED";
				}
			} else {
				status = "FAILED";
			}
			LOGGER.info("ProvisioningBusinessServiceImpl :: blacklistSubscribers() :: END");
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in blacklistSubscribers: " + ex);
			status = "FAILED";
		} finally {
			gson = null;
			headers = null;
			restTemplate = null;
			mapper = null;
			blackListDetails = null;
			blackListReq = null;
			httpEntity = null;
			response = null;
			corpusStatus = null;
			corpusResponce = null;
			requestJson = null;
			responseJson = null;
		}
		return status;
	}

	public String addMSOChannels(String distId, String mandId, String villId, String corpusPackageCode) {
		String status;

		try {

			List<CafProvisioningBO> list = provRequestsDAO.getAllCafs(distId, mandId, villId, corpusPackageCode);

			for (CafProvisioningBO cafBo : list) {
				Long prodCafNo = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.CAF_NO.getCode());
				CafProvisioningDTO caf = new CafProvisioningDTO(cafBo, distId, mandId, villId, corpusPackageCode,
						prodCafNo);
				corpusAPIServiceImpl.saveDataInCafProdsAndCafSrvcs(caf);
				this.processProvisioningRequests(String.valueOf(caf.getProdCafNo()), 0);
			}
			status = "Test OK with distId=" + distId + " mandId=" + mandId + " villId=" + villId + " corpusPackageCode="
					+ corpusPackageCode;
		} catch (Exception e) {
			status = e.getMessage();
		}

		return status;
	}

	public void changeCpeProvForSTB(ChangeDevice changeDevice) {
		LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForSTB() :: START");
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);

		try {
			long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
			changeDevice = createCpeRequestForSTB(changeDevice, requestId,now);
			
			commonDAO.saveOrUpdate(changeDevice);
			LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForSTB() :: ");
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: changeCpeProvForSTB() :: " + e);
			throw e;
		}

		LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForSTB() :: END");	
	}

	private ChangeDevice createCpeRequestForSTB(ChangeDevice changeDevice, long requestId,Calendar now) {
		Gson gson = null;
		String changeDeviceJSON = null;
		
		try {
			
			ChangeDeviceDTO changeDeviceDTO=new ChangeDeviceDTO();
			changeDeviceDTO.setSubscriberCode(changeDevice.getNwsubscode());
			changeDeviceDTO.setOldDevice(changeDevice.getOutcpemacaddr());
			changeDeviceDTO.setNewDevice(changeDevice.getIncpemacaddr());
			gson = new Gson();
			changeDeviceJSON = gson.toJson(changeDeviceDTO);

			changeDevice.setRequestid(requestId);
			changeDevice.setAcctcafno(changeDevice.getAcctcafno());
			changeDevice.setCustomerid(changeDevice.getCustomerid());
			changeDevice.setStbcafno(changeDevice.getStbcafno());
			changeDevice.setNwsubscode(changeDevice.getNwsubscode());
			changeDevice.setOutcpeslno(changeDevice.getOutcpeslno());
			changeDevice.setOutcpemacaddr(changeDevice.getOutcpemacaddr());
			changeDevice.setOutprofile_id(changeDevice.getOutprofile_id());
			changeDevice.setIncpeslno(changeDevice.getIncpeslno());
			changeDevice.setIncpemacaddr(changeDevice.getIncpemacaddr());
			changeDevice.setInprofile_id(changeDevice.getInprofile_id());
			changeDevice.setUrl(changeSTBURL);
			changeDevice.setCalltype(changeSTBCalltype);
			changeDevice.setStatus(COMSConstants.TO_BE_PROCESSED);
			changeDevice.setExecutedDate(now.getTime());
			changeDevice.setLmoId(changeDevice.getLmoId());
			changeDevice.setMspId(changeDevice.getMspId());
			changeDevice.setCreatedOn(now.getTime());
			changeDevice.setCreatedBy(changeDevice.getCreatedBy());
			
			changeDevice.setRequest(changeDeviceJSON);
		} catch (Exception e) {

			LOGGER.error("ProvisioningBusinessServiceImpl :: createRequest() :: " + e);
		} finally {
			changeDeviceJSON = null;
			gson = null;
		}
		return changeDevice;

	}
	
	public void changeCpeProvForONU(ChangeDevice changeDevice) {
		LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForONU() :: START");
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);
		long requestId ;

		try {
			 requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
			changeDevice.setRequestid(requestId);
			changeDevice = createCpeRequestForONUAgora(changeDevice, now);
			commonDAO.saveOrUpdate(changeDevice);
			
			 requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
			changeDevice.setRequestid(requestId);
			changeDevice = createCpeRequestForONUAAA(changeDevice, now);
			commonDAO.saveOrUpdate(changeDevice);
			LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForONU() :: ");
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: changeCpeProvForONU() :: " + e);
			throw e;
		}

		LOGGER.info("ProvisioningBusinessServiceImpl :: changeCpeProvForONU() :: END");
	}
	
	private ChangeDevice createCpeRequestForONUAgora(ChangeDevice changeDevice, Calendar now) {
		Gson gson = null;
		String changeDeviceJSON = null;
		String inCpeSlno = null;
		try{
			ChangeDeviceDTO changeDeviceDTO=new ChangeDeviceDTO();
			
			inCpeSlno = changeDevice.getIncpeslno();
			if (inCpeSlno.substring(0, 4).equalsIgnoreCase(zteOrgChars))
				changeDeviceDTO.setSerialNumber(inCpeSlno.replaceFirst(inCpeSlno.substring(0, 4), zteToReplaceChars));
			else if (inCpeSlno.substring(0, 4).equalsIgnoreCase(dasanOrgChars))
				changeDeviceDTO.setSerialNumber(inCpeSlno.replaceFirst(inCpeSlno.substring(0, 4), dasanToReplaceChars));
			else
				changeDeviceDTO.setSerialNumber(changeDevice.getIncpeslno());
			changeDeviceDTO.setRegisterType("1");
			gson = new Gson();
			changeDeviceJSON = gson.toJson(changeDeviceDTO);
			changeDevice.setRequest(changeDeviceJSON);
			
			String nwsubscode=changeDevice.getNwsubscode();
			//String nwsubscode=changeDeviceDTO.getSubscriberCode();//172.16.242.99-1-10-9-HSI
			
			int count=nwsubscode.lastIndexOf('-');
			String subscrCode=nwsubscode.substring(0, count);//172.16.242.99-1-10-9
			
			String url=changeDevice.setUrl(changeONUURL);//http://10.112.83.21:8880/agorang/rest/v1/eml/onu/nwsubscode
			
			int n=url.lastIndexOf('/');
			String s1=url.substring(0, n+1);//http://10.112.83.21:8880/agorang/rest/v1/eml/onu/
			String onuURL=s1.concat(subscrCode);//http://10.112.83.21:8880/agorang/rest/v1/eml/onu/172.16.242.99-1-10-9
			changeDevice.setUrl(onuURL);

			changeDevice.setCalltype(changeONUCalltype);
			changeDevice.setStatus(COMSConstants.TO_BE_PROCESSED);
			changeDevice.setExecutedDate(now.getTime());
			changeDevice.setCreatedOn(now.getTime());
					
		}
		catch(Exception e){LOGGER.error("ProvisioningBusinessServiceImpl :: createRequest() :: " + e);
		} finally {
			changeDeviceJSON = null;
			gson = null;
		}
		return changeDevice;  
	}
	
	private ChangeDevice createCpeRequestForONUAAA(ChangeDevice changeDevice, Calendar now) {
		try{
			String upAndDownProfileName = null;
			String clientName = null;
			Object[] hsiCumulative = commonDAO.getClientInformation(changeDevice.getAcctcafno());
			LOGGER.info("Hsi Cumulative Data =====> "+hsiCumulative.toString());
			if(hsiCumulative[0].toString().equalsIgnoreCase("")){
				  upAndDownProfileName = hsiCumulative[3].toString()+"_"+hsiCumulative[2].toString();	
			}else{
				 upAndDownProfileName = hsiCumulative[1].toString()+"_"+hsiCumulative[0].toString();
			}
				clientName = 	hsiCumulative[4].toString();	
				
             String macAddress = commonDAO.getAcessId(Long.valueOf(changeDevice.getAcctcafno())).replaceAll(":", "").replaceAll("(.{4})(?!$)", "$1.");
             
             String url = updateAAAUrl.replace("nwsubscode", clientName).replace("model", macAddress).replace("UPDOWN",upAndDownProfileName);
			changeDevice.setRequest(null);
			changeDevice.setUrl(url);
			changeDevice.setCalltype(IPAaaServerRadcheckCallType);
			changeDevice.setStatus(COMSConstants.TO_BE_PROCESSED);
			changeDevice.setExecutedDate(now.getTime());
			changeDevice.setCreatedOn(now.getTime());
			changeDevice.setNwsubscode(clientName);
					
		}
		catch(Exception e){
			LOGGER.error("ProvisioningBusinessServiceImpl :: createRequest() :: " + e);
		} finally {
		}
		return changeDevice;  
	}
	
		//added 22-02-17-for De-Activation
		public void processDeleteRequests(String prodcafNo) {
			List<ProvisionInfoDTO> listOfDeletingRequests = new ArrayList<>();
			try {
				LOGGER.info("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: START");
				listOfDeletingRequests = commonDAO.getDeleteRequests(Long.valueOf(prodcafNo),"");
				fillDeleteTables(listOfDeletingRequests,"D");
				/*if (listOfProvisioningRequests.size() > 0 && cafoutputFlag != 0)
					generateCAFOutputForm(prodcafNo, listOfProvisioningRequests.get(0).getAcctcafno());*/
				LOGGER.info("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: END");
			} catch (Exception ex) {
				LOGGER.error("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: " + ex);
				throw ex;
			} finally {
				listOfDeletingRequests = null;
			}
		}
	
	
	/*public void processDeleteRequests(String prodcafNo) {
        List<ProvisionInfoDTO> listOfDeletingRequests = new ArrayList<>();
        try {
            boolean flag=commonDAO.isProvRequestsExist(Long.valueOf(prodcafNo));
            if(flag){
                LOGGER.info("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: START");
                listOfDeletingRequests = commonDAO.getDeleteRequests(Long.valueOf(prodcafNo),"");
                fillDeleteTables(listOfDeletingRequests,"D");
                if (listOfProvisioningRequests.size() > 0 && cafoutputFlag != 0)
                    generateCAFOutputForm(prodcafNo, listOfProvisioningRequests.get(0).getAcctcafno());
                LOGGER.info("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: END");
            }else{
                LOGGER.info("****** ERROR::Provisioning Requests already exist *********");
            }
        } catch (Exception ex) {
            LOGGER.error("ProvisioningBusinessServiceImpl :: processDeleteRequests() :: " + ex);
            throw ex;
        } finally {
            listOfDeletingRequests = null;
        }
    }*/
		
		//added 22-02-17-for De-Activation
		@Transactional
		public String fillDeleteTables(List<ProvisionInfoDTO> listOfProvisioningRequests, String type) {
			LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: START");
			Long dependentRequestId = null;
			boolean isDependentReqSet = false;
			boolean verifyIPTV = true;
			String status = "";
			//boolean verifyHSICafno = true;
			int cnt = 1;
			try {
				for (ProvisionInfoDTO dto : listOfProvisioningRequests) {
					TimeZone tz = TimeZone.getTimeZone("GMT");
					Calendar now = Calendar.getInstance(tz);

					long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);

					if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.TELEPHONY_SERVICE_CODE))
					{
						List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();

						// Define Provisioning Request Table
						ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

						dto.setDependencyRequestId(null);
						dependentRequestId = requestId;
						isDependentReqSet = true;
						
						String arrPhNos[] = dto.getPhoneNopwd().split(",");
						for (String phNoAndpwd : arrPhNos) 
						{
							String arrPhNoPwds[] = phNoAndpwd.split("-");
							dto.setPhoneNo(arrPhNoPwds[0]);
							dto.setPasswrd(arrPhNoPwds[1]);
							
							ProvJsons delSub = telephonyProvBusinessServiceImpl.delSub(dto, requestId, now, cnt);
							provJsonList.add(delSub);
							cnt++;
							
							ProvJsons delAliasGrp = telephonyProvBusinessServiceImpl.delAliasGrp(dto, requestId, now, cnt);
							provJsonList.add(delAliasGrp);
							cnt++;
							
							ProvJsons delImpRegSet = telephonyProvBusinessServiceImpl.delImpRegSet(dto, requestId, now, cnt);
							provJsonList.add(delImpRegSet);
							cnt++;
							
							ProvJsons delPUITel = telephonyProvBusinessServiceImpl.delPUITel(dto, requestId, now, cnt);
							provJsonList.add(delPUITel);
							cnt++;
							
							ProvJsons delPUISip = telephonyProvBusinessServiceImpl.delPUISip(dto, requestId, now, cnt);
							provJsonList.add(delPUISip);
							cnt++;
							
							ProvJsons delPVI = telephonyProvBusinessServiceImpl.delPVI(dto, requestId, now, cnt);
							provJsonList.add(delPVI);
							cnt++;
							
						}

						provRequest.setProvJsons(provJsonList);

						provRequestsDAO.saveOrUpdate(provRequest);
						LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: " + dto.getSrvccodeprov());

						provRequest = null;
						provJsonList = null;
					} 
					else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.IPTV_SERVICE_CODE) && verifyIPTV)
					{
						List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
						
						if(isDependentReqSet){
							dto.setDependencyRequestId(dependentRequestId);
							dependentRequestId = requestId;
						}
						else{
							dto.setDependencyRequestId(null);
							dependentRequestId = requestId;
							isDependentReqSet = true;	
						}

						// Define Provisioning Request Table
						ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

						// Check whether network subscriber code is available or not
						// so that activation & registration can be sent
						//String HSInwSubscriberCode = commonDAO.getHSIIPTVNWSubscriberCode(Long.valueOf(dto.getAcctcafno()));
						String nwSubscriberCode= dto.getNwSubscode();
						String agoraHsiSubsCode = dto.getAgoraHsiSubsCode().replace("HSI", "IPTV");
						
							ProvJsons corpusServicePackageProvReq = terminateCorpusServicePackage(dto, requestId, now,nwSubscriberCode, type, 1);
							provJsonList.add(corpusServicePackageProvReq);

							ProvJsons agoraDeleteGPONReq = deleteHSIServiceGpon(agoraHsiSubsCode, requestId, now,2);
							provJsonList.add(agoraDeleteGPONReq);
	
							provRequest.setProvJsons(provJsonList);
	
							provRequestsDAO.saveOrUpdate(provRequest);
							LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: " + dto.getSrvccodeprov());
	
							provRequest = null;
							provJsonList = null;
							
							verifyIPTV = false;					
					}//else-if
					else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.VPN_SERVICE_CODE))
					{
							List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
							
							if(isDependentReqSet){
								dto.setDependencyRequestId(dependentRequestId);
								dependentRequestId = requestId;
							}
							else{
								dto.setDependencyRequestId(null);
								dependentRequestId = requestId;
								isDependentReqSet = true;
							}
							
							// Define Provisioning Request Table
							ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);
							
							//for Agora
							ProvJsons agoraDeleteGPONReq = deleteHSIServiceGPON(dto, requestId, now ,1);
							provJsonList.add(agoraDeleteGPONReq);
							
							provRequest.setProvJsons(provJsonList);
							
							provRequestsDAO.saveOrUpdate(provRequest);
							
							LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());
							provRequest = null;
							provJsonList = null;
					}
					else if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.INTERNET_SERVICE_CODE)) 
					{
						
						List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();

						if(isDependentReqSet){
							dto.setDependencyRequestId(dependentRequestId);
							dependentRequestId = requestId;
						}
						else{
							dto.setDependencyRequestId(null);
							dependentRequestId = requestId;
							isDependentReqSet = true;
						}
						
						// Define Provisioning Request Table
						ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);

						// for AAA Server
						ProvJsons aaaActivationProvReq = deleteAAAServiceActivation(requestId, now , dto);
						provJsonList.add(aaaActivationProvReq);
						
						//for Agora
						ProvJsons agoraDeleteGPONReq=new ProvJsons();
						boolean isVoipgponexist = false;
						if (null!=dto.getVoipGponFlag() && dto.getVoipGponFlag().equalsIgnoreCase("TRUE")){
                            ProvJsons voipDeleteGPONReq = deleteAgoraServiceVOIP(dto, requestId, now,2 );//2
                            provJsonList.add(voipDeleteGPONReq);
                            isVoipgponexist=true;
                            LOGGER.info("isVoipgponexist : " + isVoipgponexist);
                        }
                        //for Agora 3
                        int seqNo;
                        if (isVoipgponexist){
                            seqNo=3;
                        }else{
                            seqNo=2;
                        }

						agoraDeleteGPONReq = deleteHSIServiceGPON(dto, requestId, now ,seqNo);
						provJsonList.add(agoraDeleteGPONReq);
						
						ProvJsons agoraDeleteONUReq = deleteHSIServiceONU(dto, requestId, now,seqNo+1);
						provJsonList.add(agoraDeleteONUReq);
						
						provRequest.setProvJsons(provJsonList);
						
						provRequestsDAO.saveOrUpdate(provRequest);
						LOGGER.info("ProvisioningBusinessServiceImpl :: fillProvisonTables() :: " + dto.getSrvccodeprov());
						provRequest = null;
						provJsonList = null;
					}
					LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: END");
					now = null;
					tz = null;
				}
				status = "success";
			} catch (Exception e) {
				LOGGER.error("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: " + e);
				status = "fail";
				throw e;
			}

			return status;
		}
		
		//added for AAA updation
		public void processUpdationRequests(AAAUpdateDTO updateDTO ) {
			List<ProvisionInfoDTO> listOfProvisioningRequests = new ArrayList<>();
			try {
				LOGGER.info("ProvisioningBusinessServiceImpl :: processUpdationRequests() :: START");
				listOfProvisioningRequests = commonDAO.getProvisioningRequests(Long.valueOf(updateDTO.getProdcafNo()), "HSI");
				fillUpdateTables(listOfProvisioningRequests, "A" , updateDTO);
				LOGGER.info("ProvisioningBusinessServiceImpl :: processUpdationRequests() :: END");
			} catch (Exception ex) {
				LOGGER.error("ProvisioningBusinessServiceImpl :: processUpdationRequests() :: " + ex);
				throw ex;
				
			} finally {
				listOfProvisioningRequests = null;
			}
		}
		
		//added for AAA updation
		@Transactional
		public String fillUpdateTables(List<ProvisionInfoDTO> listOfProvisioningRequests, String type , AAAUpdateDTO updateDTO ) {
			LOGGER.info("ProvisioningBusinessServiceImpl :: fillUpdateTables() :: START");
			String status = "";
			try {
					TimeZone tz = TimeZone.getTimeZone("GMT");
					Calendar now = Calendar.getInstance(tz);
					for (ProvisionInfoDTO dto : listOfProvisioningRequests) {
						long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
						List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();
	
						// Define Provisioning Request Table
						ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);
						provRequest.setNwSubsCode(dto.getClient());
						
		                 String upAndDownProfileName = updateDTO.getUPSpeed()+"_"+updateDTO.getDOWNSpeed();					 
		                 String macAddress = commonDAO.getAcessId(Long.valueOf(dto.getProdcafno())).replaceAll(":", "").replaceAll("(.{4})(?!$)", "$1.");
		                 
		                // String clientName=commonDAO.getClientName(Long.valueOf(dto.getProdcafno()));
		                 String clientName= dto.getClient();
		                 String url = updateAAAUrl.replace("nwsubscode", clientName).replace("model", macAddress).replace("UPDOWN",upAndDownProfileName);
		                 LOGGER.info("ProvisioningBusinessServiceImpl :: Update AAA URL :: "+ url);
		               
						ProvJsons aaaUpdationProvReq = updateAAAServiceActivation(requestId, now ,url);
						provJsonList.add(aaaUpdationProvReq);
						
						 if(actionReboot){
								aaaUpdationProvReq =  onuReboot(requestId, 2, now,Long.parseLong(dto.getAcctcafno()));
								provJsonList.add(aaaUpdationProvReq);
			                }
	
						provRequest.setProvJsons(provJsonList);
	
						provRequestsDAO.saveOrUpdate(provRequest);
						LOGGER.info("ProvisioningBusinessServiceImpl :: updateProvisonTables() :: " + dto.getSrvccodeprov());
						provRequest = null;
						provJsonList = null;
						LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: END");
					}
					status = "success";
					now = null;
					tz = null;
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: " + e);
			LOGGER.info("ProvisioningBusinessServiceImpl :: fillDeleteTables() :: END");
			
			status = "fail";
			throw e;
		}
		return status;
	}	
		
	public ProvJsons updateAAAServiceActivation(long requestId, Calendar now , String url) {
	ProvJsons aaaUpdationProvReq = new ProvJsons();
	try{
		aaaUpdationProvReq.setRequestId(requestId);
		aaaUpdationProvReq.setReq("NULL");
		aaaUpdationProvReq.setServiceType(COMSConstants.AAA_SERVICE_CODE);
		aaaUpdationProvReq.setCallType(IPAaaServerRadcheckCallType);
		aaaUpdationProvReq.setUrl(url);
		aaaUpdationProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);
		aaaUpdationProvReq.setSeqNum(1);
		aaaUpdationProvReq.setCreatedDate(now.getTime());
	} catch (Exception ex) {
		LOGGER.error("ProvisioningBusinessServiceImpl :: updateAAAServiceActivation() :: " + ex);
	}
	return aaaUpdationProvReq;
 } 
	//added 12/04/17 for IPTV service pack termination
	public ProvJsons terminateCorpusServicePackage(ProvisionInfoDTO dto, long requestId, Calendar now,String nwSubscriberCode, String type, int incr) {
		ProvJsons corpusServicePackageProvReq = null;
		TerminateDTO terminateDTO = null;
		List<String> nwScbCodes = new ArrayList<>();
		Gson gson = null;
		String jsonResponse = null;
		try {
			corpusServicePackageProvReq = new ProvJsons();
			terminateDTO = new TerminateDTO();
			nwScbCodes.add(nwSubscriberCode);
			terminateDTO.setSubscriberCodes(nwScbCodes);

			gson = new Gson();
			jsonResponse = gson.toJson(terminateDTO);

			corpusServicePackageProvReq.setRequestId(requestId);
			corpusServicePackageProvReq.setReq(jsonResponse);
			corpusServicePackageProvReq.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			corpusServicePackageProvReq.setCallType(IPTVCorpusServicePackCallType);
			
			if (type.equalsIgnoreCase("D"))
				corpusServicePackageProvReq.setUrl(IPTVCorpusServicePackDisconnectUrl);
			
			corpusServicePackageProvReq.setStatus(COMSConstants.TO_BE_PROCESSED);

			corpusServicePackageProvReq.setSeqNum(incr);
			corpusServicePackageProvReq.setCreatedDate(now.getTime());

		} catch (Exception ex) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: terminateCorpusServicePackage() :: " + ex);
		} finally {
			jsonResponse = null;
			gson = null;
			terminateDTO = null;
		}
		return corpusServicePackageProvReq;
	}
	
	// added 17-04-2017 for Base Package Change for HSI
	public String basePackageChange(String prodCafNo, String prodCode) {
		List<ProvisionInfoDTO> listOfBasePkgs = new ArrayList<>();
		String status = "false";
		try {
			LOGGER.info("ProvisioningBusinessServiceImpl :::::::: basePackageChange() :::: START ::");
			listOfBasePkgs = commonDAO.getBasePackagesList(Long.valueOf(prodCafNo), prodCode);
			if(listOfBasePkgs.size() > 0) {
				status = fillbasePackageChangeTables(listOfBasePkgs, "A");
			}
			LOGGER.info("ProvisioningBusinessServiceImpl :::::::: basePackageChange() :::: END ::");
		} catch (Exception e) {
			LOGGER.error("ERROR in ProvisioningBusinessServiceImpl :::::::: basePackageChange()" + e);
			e.printStackTrace();
			throw e;
		} finally {
			listOfBasePkgs = null;
		}
		return status;
	}

	@Transactional
	private String fillbasePackageChangeTables(List<ProvisionInfoDTO> listOfBasePkgs, String type) {
		LOGGER.info("ProvisioningBusinessServiceImpl :::::::: fillbasePackageChangeTables() :::: START ");
		String status = "false";
		try {
			for (ProvisionInfoDTO provisionInfoDTO : listOfBasePkgs) {
				TimeZone tz = TimeZone.getTimeZone("GMT");
				Calendar now = Calendar.getInstance(tz);

				long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
				LOGGER.info("requestId : " + requestId);
				if (null != provisionInfoDTO && provisionInfoDTO.getSrvccodeprov().equalsIgnoreCase(COMSConstants.INTERNET_SERVICE_CODE)) {
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();

					ProvRequests provRequests = createProvisionRequest(provisionInfoDTO, requestId, now, type);
					provisionInfoDTO.setDependencyRequestId(null);

					String upstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSPEED, provisionInfoDTO.getSrvccodeaddl());
					String downstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWNSPEED, provisionInfoDTO.getSrvccodeaddl());

					String upAndDownProfileName = upstreamProfileName + "_" + downstreamProfileName;

					String macAddress = commonDAO.getAcessId(Long.valueOf(provisionInfoDTO.getProdcafno())).replaceAll(":", "").replaceAll("(.{4})(?!$)", "$1.");

					String url = updateAAAUrl.replace("nwsubscode", provisionInfoDTO.getClient()).replace("model", macAddress).replace("UPDOWN", upAndDownProfileName);

					ProvJsons json = updateAAAServiceActivation(requestId, now, url);
					provJsonList.add(json);
						
					 if(actionReboot){
						 json =  onuReboot(requestId, 2, now,Long.parseLong(provisionInfoDTO.getAcctcafno()));
						 provJsonList.add(json);
					 }
					 

					provRequests.setProvJsons(provJsonList);

					provRequests = provRequestsDAO.saveOrUpdate(provRequests);
					if(Objects.nonNull(provRequests)) {
						status = "success";
					}
				}
				now = null;
				tz = null;
			}
		} catch (Exception e) {
			LOGGER.error("ERROR occured in fillbasePackageChangeTables() " + e);
			status = "fail";
			throw e;
		}
		return status;
	}

	public String packageTermination(String cafNo, String prodCode,String stbCafNo,String nwSubsCode) {
		List<ProvisionInfoDTO> listOfPkgs = new ArrayList<>();
		String status = "false";
		try{
			LOGGER.info("ProvisioningBusinessServiceImpl :::::::: packageTermination() :::: START ::");
			listOfPkgs=commonDAO.getPackageTerminationList(Long.valueOf(cafNo),prodCode,stbCafNo,nwSubsCode);
			if(listOfPkgs.size() > 0) {
				status = fillPkgTerminationTables(listOfPkgs,"D");
				LOGGER.info("ProvisioningBusinessServiceImpl :::::::: packageTermination() :::: END ::");
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR in ProvisioningBusinessServiceImpl :::::::: packageTermination()"+e);
			e.printStackTrace();
			throw e;			
		}
		finally{
			listOfPkgs= null;
		}
		return status;
	}
	
	public String alaCartepackageTermination(String cafNo, String prodCode,String stbCafNo,String nwSubsCode) {
		List<ProvisionInfoDTO> listOfPkgs = new ArrayList<>();
		String status = "false";
		try{
			LOGGER.info("ProvisioningBusinessServiceImpl :::::::: alaCartepackageTermination() :::: START ::");
			listOfPkgs=commonDAO.getAlaCartePackageTerminationList(Long.valueOf(cafNo),prodCode,stbCafNo,nwSubsCode);
			if(listOfPkgs.size() > 0) {
				status = fillPkgTerminationTables(listOfPkgs,"D");
				LOGGER.info("ProvisioningBusinessServiceImpl :::::::: alaCartepackageTermination() :::: END ::");
			}
		}
		catch(Exception e){
			LOGGER.error("ERROR in ProvisioningBusinessServiceImpl :::::::: alaCartepackageTermination()"+e);
			e.printStackTrace();
			throw e;			
		}
		finally{
			listOfPkgs= null;
		}
		return status;
	}

	@Transactional
	private String fillPkgTerminationTables(List<ProvisionInfoDTO> listOfPkgs, String type) {
		LOGGER.info("ProvisioningBusinessServiceImpl :::::::: fillPkgTerminationTables() :::: START ");
		String status = "false";
		try {
			for (ProvisionInfoDTO provisionInfoDTO : listOfPkgs) {
				TimeZone tz = TimeZone.getTimeZone("GMT");
				Calendar now = Calendar.getInstance(tz);

				long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
				if (null != provisionInfoDTO && provisionInfoDTO.getSrvccodeprov().equalsIgnoreCase(COMSConstants.IPTV_SERVICE_CODE)) {
					List<ProvJsons> provJsonList = new ArrayList<ProvJsons>();

					ProvRequests provRequests = createProvisionRequest(provisionInfoDTO, requestId, now, type);
					provisionInfoDTO.setDependencyRequestId(null);

					ProvJsons json = terminatePackage(provisionInfoDTO, requestId, now);
					provJsonList.add(json);

					provRequests.setProvJsons(provJsonList);

					provRequests = provRequestsDAO.saveOrUpdate(provRequests);
					if (Objects.nonNull(provRequests)) {
						status = "success";
					}

				}
				now = null;
				tz = null;
			}
		} catch (Exception e) {
			LOGGER.error("ERROR occured in fillPkgTerminationTables() " + e);
			throw e;
		}
		return status;

	}
			
	public ProvJsons terminatePackage(ProvisionInfoDTO provisionInfoDTO, long requestId, Calendar now) {
		ProvJsons json = new ProvJsons();
		Gson gson = null;
		ServicePackageDTO dto = new ServicePackageDTO();
		List<ServicePackDTO> srvDto = new ArrayList<>();
		ServicePackDTO sDto = new ServicePackDTO();
		String srvcPack = provisionInfoDTO.getSrvccodeaddl();

		sDto.setServicepack(srvcPack);
		sDto.setReason("");
		srvDto.add(sDto);

		dto.setServicepacks(srvDto);
		dto.setSubscribercode(provisionInfoDTO.getNwSubscode());
		try {
			gson = new Gson();
			String req = gson.toJson(dto);

			json.setRequestId(requestId);
			json.setReq(req);
			json.setServiceType(COMSConstants.IPTV_SERVICE_CODE);
			json.setCallType(terminateCallType);
			json.setUrl(terminateUrl);
			json.setStatus(COMSConstants.TO_BE_PROCESSED);
			json.setSeqNum(1);
			json.setCreatedDate(now.getTime());
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :: terminatePackage() :: " + e);
		}
		return json;
	}
	
	// added 01/05/2017 for ONU Reboot
	public ProvJsons onuReboot(long requestId, int seqNo, Calendar now, long cafNo) {
		ProvJsons json = new ProvJsons();
		Gson gson = null;
		List<ONURebootDTO> list=new ArrayList<ONURebootDTO>();
		list = commonDAO.ONURebootList(cafNo);
		String request = null;
		
		try {
			
			for(ONURebootDTO onuRebootDto : list){
				LOGGER.info("inside onuReboot()");
				gson = new Gson();
				request = gson.toJson(onuRebootDto);
			}
			

			json.setRequestId(requestId);
			json.setReq(request);
			json.setUrl(reBootURL);
			json.setServiceType(COMSConstants.INTERNET_SERVICE_CODE);
			json.setCallType(reBootCalltype);
			json.setSeqNum(seqNo);
			json.setStatus(COMSConstants.TO_BE_PROCESSED);
			json.setCreatedDate(now.getTime());
		} catch (Exception e) {
			LOGGER.error("ProvisioningBusinessServiceImpl :::: onuReboot() " + e);
		}
		return json;
	}
	
	// added 15/06/17-for general AAA-Request
	public String requestForAAA(String cafNo) {
		List<ProvisionInfoDTO> list = new ArrayList<ProvisionInfoDTO>();
		String status = "";
		try {
			LOGGER.info("ProvisioningBUsinessServiceImpl:::requestForAAA()::::START");
			list = commonDAO.getProvisioningRequests(Long.valueOf(cafNo), "");
			if(list.size()>0){
			status = fillOLProvTables(list, "A");
			LOGGER.info("ProvisioningBusinessServiceImpl ::: requestForAAA() :::: END ::");
			}
		} catch (Exception e) {
			LOGGER.error("error in ProvisioningBusinessServiceIMpl:::requestForAAA:::");
			status = "fail";
			throw e;
		}
		return status;
	}

	@Transactional
	public String fillOLProvTables(List<ProvisionInfoDTO> requestList, String type) {
		String status = "";
		List<ProvJsons> jsonList = new ArrayList<ProvJsons>();

		LOGGER.info("ProvisioningBUsinessServiceIMpl:::fillOLProvTables:::BEGIN");
		try {
			for (ProvisionInfoDTO dto : requestList) {
				TimeZone tz = TimeZone.getTimeZone("GMT");
				Calendar now = Calendar.getInstance(tz);

				long requestId = commonDAO.generateSequence(COMSConstants.PROV_REQUEST_SEQUENCE);
				if (null != dto && dto.getSrvccodeprov().equalsIgnoreCase(COMSConstants.INTERNET_SERVICE_CODE)) {
					// for olprovrequest Table
					ProvRequests provRequest = createProvisionRequest(dto, requestId, now, type);
					provRequest.setDpndrequestid(null);
					provRequest.setNwSubsCode(dto.getClient());

					// for olprovJSON table
					String upstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.UPSPEED,dto.getSrvccodeaddl());
					String downstreamProfileName = commonDAO.getServiceParamValue(COMSConstants.DOWNSPEED,dto.getSrvccodeaddl());

					String upAndDownProfileName = upstreamProfileName + "_" + downstreamProfileName;

					String macAddress = commonDAO.getAcessId(Long.valueOf(dto.getProdcafno())).replaceAll(":", "").replaceAll("(.{4})(?!$)", "$1.");

					String url = urlForAAA.replace("nwsubscode", dto.getClient()).replace("model", macAddress).replace("UPDOWN", upAndDownProfileName);
					ProvJsons jSon = jsonAAARequest(requestId, now, url);
					jsonList.add(jSon);

					provRequest.setProvJsons(jsonList);
					provRequestsDAO.saveOrUpdate(provRequest);
				}
				now = null;
				tz = null;
			}
			status = "Success";
		} catch (Exception e) {
			LOGGER.error("error in ProvisioningBUsinessServiceIMpl:::fillOLProvTables:::");
			status = "fail";
			throw e;
		}
		return status;
	}

	private ProvJsons jsonAAARequest(long requestId, Calendar now, String url) {
		ProvJsons jSon = new ProvJsons();
		try {
			jSon.setRequestId(requestId);
			jSon.setCallType(IPAaaServerRadcheckCallType);
			jSon.setReq("NULL");
			jSon.setSeqNum(1);
			jSon.setStatus(COMSConstants.TO_BE_PROCESSED);
			jSon.setUrl(url);
			jSon.setServiceType(COMSConstants.AAA_SERVICE_CODE);
			jSon.setCreatedDate(now.getTime());
		} catch (Exception e) {
			LOGGER.error("error in jsonAAARequest:");
			throw e;
		}
		return jSon;
	}
}




