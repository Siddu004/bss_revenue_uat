package com.arbiva.apfgc.provision.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AgoraActivatioVOIPDTO {

	private static final long serialVersionUID = 1L;

	public AgoraActivatioVOIPDTO() {
	
	}
	
	private AIDDTO aid;
	
	private List<TPSDTO> tps;
	
	@JsonProperty("admin")
	private int admin;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("networkServiceName")
	private String networkServiceName;
	
	@JsonProperty("upstreamTrafficProfileName")
	private String upstreamTrafficProfileName;
	
	@JsonProperty("downstreamTrafficProfileName")
	private String downstreamTrafficProfileName;
	
	@JsonProperty("l2DhcpRelay")
	DhcpRelayDTO l2DhcpRelay;
	
	@JsonProperty("igmpOptions")
	IGMPOptionsDTO igmpOptions;
	
	@JsonProperty("nativeVlan")
	private String nativeVlan;
	
	
	@JsonProperty("encryption")
	private Boolean encryption;
	
	
	@JsonProperty("uniCtag")
	private int uniCtag;
	
	@JsonProperty("ipManagement")
	private Boolean ipManagement;
	
	
	
	
	/**
	 * @return the ipManagement
	 */
	public Boolean getIpManagement() {
		return ipManagement;
	}

	/**
	 * @param ipManagement the ipManagement to set
	 */
	public void setIpManagement(Boolean ipManagement) {
		this.ipManagement = ipManagement;
	}

	/**
	 * @return the encryption
	 */
	public Boolean getEncryption() {
		return encryption;
	}

	/**
	 * @param encryption the encryption to set
	 */
	public void setEncryption(Boolean encryption) {
		this.encryption = encryption;
	}

	/**
	 * @return the uniCtag
	 */
	public int getUniCtag() {
		return uniCtag;
	}

	/**
	 * @param uniCtag the uniCtag to set
	 */
	public void setUniCtag(int uniCtag) {
		this.uniCtag = uniCtag;
	}
	

	/**
	 * @return the nativeVlan
	 */
	public String getNativeVlan() {
		return nativeVlan;
	}

	/**
	 * @param nativeVlan the nativeVlan to set
	 */
	public void setNativeVlan(String nativeVlan) {
		this.nativeVlan = nativeVlan;
	}

	/**
	 * @return the aid
	 */
	public AIDDTO getAid() {
		return aid;
	}

	/**
	 * @param aid the aid to set
	 */
	public void setAid(AIDDTO aid) {
		this.aid = aid;
	}

	/**
	 * @return the tps
	 */
	public List<TPSDTO> getTps() {
		return tps;
	}

	/**
	 * @param tps the tps to set
	 */
	public void setTps(List<TPSDTO> tps) {
		this.tps = tps;
	}

	/**
	 * @return the admin
	 */
	public int getAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(int admin) {
		this.admin = admin;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the networkServiceName
	 */
	public String getNetworkServiceName() {
		return networkServiceName;
	}

	/**
	 * @param networkServiceName the networkServiceName to set
	 */
	public void setNetworkServiceName(String networkServiceName) {
		this.networkServiceName = networkServiceName;
	}

	/**
	 * @return the upstreamTrafficProfileName
	 */
	public String getUpstreamTrafficProfileName() {
		return upstreamTrafficProfileName;
	}

	/**
	 * @param upstreamTrafficProfileName the upstreamTrafficProfileName to set
	 */
	public void setUpstreamTrafficProfileName(String upstreamTrafficProfileName) {
		this.upstreamTrafficProfileName = upstreamTrafficProfileName;
	}

	/**
	 * @return the l2DhcpRelay
	 */
	public DhcpRelayDTO getL2DhcpRelay() {
		return l2DhcpRelay;
	}

	/**
	 * @param l2DhcpRelay the l2DhcpRelay to set
	 */
	public void setL2DhcpRelay(DhcpRelayDTO l2DhcpRelay) {
		this.l2DhcpRelay = l2DhcpRelay;
	}

	/**
	 * @return the igmpOptions
	 */
	public IGMPOptionsDTO getIgmpOptions() {
		return igmpOptions;
	}

	/**
	 * @param igmpOptions the igmpOptions to set
	 */
	public void setIgmpOptions(IGMPOptionsDTO igmpOptions) {
		this.igmpOptions = igmpOptions;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDownstreamTrafficProfileName() {
		return downstreamTrafficProfileName;
	}

	public void setDownstreamTrafficProfileName(String downstreamTrafficProfileName) {
		this.downstreamTrafficProfileName = downstreamTrafficProfileName;
	}

}
