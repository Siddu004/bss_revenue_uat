package com.arbiva.apfgc.provision.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.provision.utils.COMSConstants;

@Repository
public class ProvErrorDAO {
	
	private static final Logger LOGGER = Logger.getLogger(ProvErrorDAO.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getProvisionErrorList() {
		List<Object[]> provErrList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder("select acctcafno, requestid, prodcode, srvccodeaddl,  Date_Format(createdon, '%d-%m-%Y')  createdon "+
									" from olprovrequests "+
									" where status in ('7','8','9')");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			provErrList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getProvisionErrorList() " + ex);
		}
		finally{
			builder = null;
			query = null;
		}
		return provErrList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getErrJsons(String reqid) {
		List<Object[]> jsonErrList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder(" select req, resp, Date_Format(created_date, '%d-%m-%Y')  created_date, Date_Format(executed_date, '%d-%m-%Y')  executed_date, "+
				" case when status=0 then 'Ready for Processing' "+
				" when status=1 then 'Processed Successfully' "+
				" when status=3 then 'To be Processed Later' "+
				" when status=4 then 'Picked up for Processing' "+
				" when status=5 then 'Sent Req to WS' "+
				" when status=6 then 'To be Processed Later' "+
				" when status=7 then 'TIME out No resp from WS' "+
				" when status=8 then 'No Status code in resp' "+
				" when status=9 then 'Failure response from server' end as status "+
				" from olprovjsons  "+
				" where requestid = :reqid ");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			jsonErrList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getErrJsons() " + ex);
		}
		finally{
			builder = null;
			query = null;
		}
		return jsonErrList;
	}
	
	@SuppressWarnings("unchecked")
	public int getSeqNum(String reqid) {
		int seqNum = 99;
		StringBuilder builder = new StringBuilder(" select seqnum "+
												  " from olprovjsons "+
												  " where requestid = :reqid "+
												  " and status in ('7','8','9')");
		Query query = null;
		List<Object> results = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			results = query.getResultList();
			if(!results.isEmpty()){
				seqNum = results.get(0) == null ? 99 : (int) results.get(0);
			}
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getSeqNum() " + ex);
		}finally{
			builder = null;
			query = null;
			results = null;
		}
		return seqNum;
	}
	
	@SuppressWarnings("unchecked")
	public String getCafNo(String reqid) {
		String cafNo = "0";
		StringBuilder builder = new StringBuilder(" select prodcafno "+
												  " from olprovrequests "+
												  " where requestid = :reqid ");
		Query query = null;
		List<Object> results = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			results = query.getResultList();
			if(!results.isEmpty()){
				cafNo = results.get(0) == null ? "0" : results.get(0).toString() ;		
			}
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getCafNo() " + ex);
		}
		finally{
			builder = null;
			query = null;
			results = null;
		}
		return cafNo;
	}
	
	public int updateProvRequests(String reqid) {
		int i = 0; 
		StringBuilder builder = new StringBuilder(" update olprovrequests "+
												  " set status = '0' "+
												  " where requestid = :reqid ");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			i = query.executeUpdate();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateProvRequests() " + ex);
		}finally{
			builder = null;
			query = null;
		}
		return i;
	}
	
	public int updateProvJson(String reqid, String request, int seqNo) {
		int i = 0; 
		StringBuilder builder = new StringBuilder(" update olprovjsons "+
												  " set status = '0', req = :request , resp = null "+
												  " where requestid = :reqid "+
												  " and seqnum = :seqNo ");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			query.setParameter("request", request);
			query.setParameter("seqNo", seqNo);
			i = query.executeUpdate();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateProvJsons() " + ex);
		}finally{
			builder =  null;
			query = null;
		}
		return i;
	}
	
	@SuppressWarnings("unchecked")
	public String getMaxSeqNo(String reqid) {
		String maxSeqNo = "0";
		StringBuilder builder = new StringBuilder(" select max(seqnum) seqnum "+
												  " from  olprovjsons "+
												  " where requestid = :reqid ");
		Query query = null;
		List<Object> results = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			results = query.getResultList();
			if(!results.isEmpty()){
				maxSeqNo = results.get(0) == null ? "0" : results.get(0).toString() ;		
			}
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getMaxSeqNo() " + ex);
		}finally{
			builder = null; 
			query = null;
			results = null;
		}
		return maxSeqNo;
	}
	
	public int updateJsons(String reqid, int seqNo) {
		int i = 0; 
		StringBuilder builder = new StringBuilder(" update olprovjsons "+
												  " set status = '0' "+
												  " where requestid = :reqid "+
												  " and seqnum = :seqNo ");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			query.setParameter("seqNo", seqNo);
			i = query.executeUpdate();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateJsons() " + ex);
		}finally{
			builder = null;
		}
		return i;
	}
	
	@SuppressWarnings("unchecked")
	public String getStatus(String reqid, int seqNo) {
		String s = ""; 
		StringBuilder builder = new StringBuilder(" select status "+
												  " from olprovjsons "+
												  " where requestid = :reqid "+
												  " and seqnum = :seqNo ");
		List<Object> results = null;
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter("reqid", reqid);
			query.setParameter("seqNo", seqNo);
			results = query.getResultList();
			if(!results.isEmpty()){
				s = results.get(0) == null ? "" : results.get(0).toString() ;		
			}
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateJsons() " + ex);
		}finally{
			builder = null;
			query = null;
			results = null;
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public boolean checkHSIFlag(long cafNo) {
		String s = "";
		boolean status = false;
		String INTERNET_SERVICE_CODE = COMSConstants.INTERNET_SERVICE_CODE;
		StringBuilder builder = new StringBuilder(" select count(1) from olprovrequests where prodcafno = ? and srvccodeprov = ? ");
		List<Object> results = null;
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());	
			query.setParameter(1, cafNo);
			query.setParameter(2, INTERNET_SERVICE_CODE);
			results = query.getResultList();
			if(!results.isEmpty()){
				s = results.get(0) == null ? "0" : results.get(0).toString() ;
				if(Integer.parseInt(s) > 0)
					status = true;
			}
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateJsons() " + ex);
		}finally{
			builder = null;
			query = null;
			results = null;
		}
		return status;
	}

	/**
	 * @author rajesh s xyz This method return records from olprovjsons
	 * @query select requestid, req, resp, Date_Format(created_date, '%d-%m-%Y')
	 *        created_date, Date_Format(executed_date, '%d-%m-%Y') executed_date
	 *        from olprovjsons where status='9';
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getPendingProvisonJsons() {
		List<Object[]> ProvisonErrList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder(
				" select requestid, req, resp, Date_Format(created_date, '%d-%m-%Y')  created_date, Date_Format(executed_date, '%d-%m-%Y')  executed_date, status  from olprovjsons where status='9' ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			ProvisonErrList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getErrJsons() " + ex);
		} finally {
			builder = null;
			query = null;
		}
		return ProvisonErrList;
	}

	
	@SuppressWarnings({ "unchecked", "null" })
	@Transactional
	public String[] getReqIdsByCafNo(String cafNo) {
		List<Object> resultList = new ArrayList<Object>();
		String[] reqIds = null;
		int i=0;
		StringBuilder builder = new StringBuilder(
				"select requestid from  olprovrequests where acctcafno= '" +cafNo+"'");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			resultList = query.getResultList();
			if (resultList.isEmpty());
			 reqIds = new String[resultList.size()];
			for (Object reqId : resultList ){
				reqIds[i]=reqId.toString() ;
				i++;
			}
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getErrJsons() " + ex);
		} finally {
			builder = null;
			query = null;
		}
		return reqIds;
	}
	
	
	@Transactional
	public int updateProvJsons(String[] reqids) {
		int i = 0; 
		StringBuilder reqIdsForCaf=new StringBuilder();
		for (String reqid : reqids) {
			   
			  {
				  reqIdsForCaf.append("'" + reqid + "'" + ",");
			   }
			}
		reqIdsForCaf.deleteCharAt(reqIdsForCaf.length()-1);
		StringBuilder builder = new StringBuilder(" update olprovjsons "+
												  " set status = '0' "+
												  " where requestid in "+
												  "( "+ reqIdsForCaf.toString()+") and (( status = 9) OR (status=4)) " );
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());				
			i = query.executeUpdate();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateProvJsons() " + ex);
		}finally{
			builder =  null;
			query = null;
		}
		return i;
	}
@Transactional
	public int updateProvRequest(String[] reqIdsForCafs) {
		
		int i = 0; 
		StringBuilder reqIdsForCaf=new StringBuilder();
		for (String reqid : reqIdsForCafs) {
			   
			  {
				  reqIdsForCaf.append("'" + reqid + "'" + ",");
			   }
			}
		reqIdsForCaf.deleteCharAt(reqIdsForCaf.length()-1);
		StringBuilder builder = new StringBuilder(" update olprovrequests "+
												  " set status = '0' "+
												  " where requestid in "+
												  "( "+ reqIdsForCaf.toString()+") and (( status = 9) OR (status=4)) " );
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());				
			i = query.executeUpdate();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::updateProvJsons() " + ex);
		}finally{
			builder =  null;
			query = null;
		}
		return i;
	}
	
	

}
