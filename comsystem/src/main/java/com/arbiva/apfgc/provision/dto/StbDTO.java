package com.arbiva.apfgc.provision.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StbDTO {
	
	@JsonProperty("stbMacAddr")
	private String stbMacAddr;
	
	@JsonProperty("stbLeaseyn")
	private String stbLeaseyn;
	
	@JsonProperty("stbModel")
	private String stbModel;
	
	@JsonProperty("stbSlno")
	private String stbSlno;
	
	public String getStbMacAddr() {
		return stbMacAddr;
	}
	public void setStbMacAddr(String stbMacAddr) {
		this.stbMacAddr = stbMacAddr;
	}
	public String getStbLeaseyn() {
		return stbLeaseyn;
	}
	public void setStbLeaseyn(String stbLeaseyn) {
		this.stbLeaseyn = stbLeaseyn;
	}
	public String getStbModel() {
		return stbModel;
	}
	public void setStbModel(String stbModel) {
		this.stbModel = stbModel;
	}
	public String getStbSlno() {
		return stbSlno;
	}
	public void setStbSlno(String stbSlno) {
		this.stbSlno = stbSlno;
	}
	

}
