package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SubRegResponseStatusDTO {

	private static final long serialVersionUID = 1L;

	public SubRegResponseStatusDTO() {

	}

	@JsonProperty("statusCode")
	private String statusCode;

	@JsonProperty("statusMessage")
	private String statusMessage;

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage
	 *            the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
