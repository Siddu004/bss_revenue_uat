package com.arbiva.apfgc.provision.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AreaDTO {

	private static final long serialVersionUID = 1L;

	public AreaDTO() {
	
	}
	
	@JsonProperty("Enrollment_Number")
	private String Enrollment_Number;
	
	@JsonProperty("AreaID")
	private String AreaID;
	
	private String REF_ID;
	
	private String type;
	
	@JsonProperty("Area_Name")
	private String Area_Name;
	
	@JsonProperty("Cable_Type")
	private String Cable_Type;
	
	@JsonProperty("Running_Legnth_Of_Cable")
	private String Running_Legnth_Of_Cable;
	
	@JsonProperty("State_Name")
	private String State_Name;
	
	@JsonProperty("District_Name")
	private String District_Name;
	
	@JsonProperty("Mandal_Name")
	private String Mandal_Name;
	
	@JsonProperty("Village_Name")
	private String Village_Name;
	
	@JsonProperty("State_ID")
	private String State_ID;
	
	@JsonProperty("District_ID")
	private String District_ID;
	
	@JsonProperty("Mandal_ID")
	private String Mandal_ID;
	
	@JsonProperty("Village_ID")
	private String Village_ID;
	
	@JsonProperty("No_of_Subscriptions")
	private String No_of_Subscriptions;
	
	@JsonProperty("No_of_Connections")
	private String No_of_Connections;
	
	@JsonProperty("No_of_Digital_Connections")
	private String No_of_Digital_Connections;
	
	@JsonProperty("No_of_Analog_Connections")
	private String No_of_Analog_Connections;

	/**
	 * @return the enrollment_Number
	 */
	public String getEnrollment_Number() {
		return Enrollment_Number;
	}

	/**
	 * @param enrollment_Number the enrollment_Number to set
	 */
	public void setEnrollment_Number(String enrollment_Number) {
		Enrollment_Number = enrollment_Number;
	}

	/**
	 * @return the areaID
	 */
	public String getAreaID() {
		return AreaID;
	}

	/**
	 * @param areaID the areaID to set
	 */
	public void setAreaID(String areaID) {
		AreaID = areaID;
	}

	
	/**
	 * @return the rEF_ID
	 */
	public String getREF_ID() {
		return REF_ID;
	}

	/**
	 * @param rEF_ID the rEF_ID to set
	 */
	public void setREF_ID(String rEF_ID) {
		REF_ID = rEF_ID;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the area_Name
	 */
	public String getArea_Name() {
		return Area_Name;
	}

	/**
	 * @param area_Name the area_Name to set
	 */
	public void setArea_Name(String area_Name) {
		Area_Name = area_Name;
	}

	/**
	 * @return the cable_Type
	 */
	public String getCable_Type() {
		return Cable_Type;
	}

	/**
	 * @param cable_Type the cable_Type to set
	 */
	public void setCable_Type(String cable_Type) {
		Cable_Type = cable_Type;
	}

	/**
	 * @return the running_Legnth_Of_Cable
	 */
	public String getRunning_Legnth_Of_Cable() {
		return Running_Legnth_Of_Cable;
	}

	/**
	 * @param running_Legnth_Of_Cable the running_Legnth_Of_Cable to set
	 */
	public void setRunning_Legnth_Of_Cable(String running_Legnth_Of_Cable) {
		Running_Legnth_Of_Cable = running_Legnth_Of_Cable;
	}

	/**
	 * @return the state_Name
	 */
	public String getState_Name() {
		return State_Name;
	}

	/**
	 * @param state_Name the state_Name to set
	 */
	public void setState_Name(String state_Name) {
		State_Name = state_Name;
	}

	/**
	 * @return the district_Name
	 */
	public String getDistrict_Name() {
		return District_Name;
	}

	/**
	 * @param district_Name the district_Name to set
	 */
	public void setDistrict_Name(String district_Name) {
		District_Name = district_Name;
	}

	/**
	 * @return the mandal_Name
	 */
	public String getMandal_Name() {
		return Mandal_Name;
	}

	/**
	 * @param mandal_Name the mandal_Name to set
	 */
	public void setMandal_Name(String mandal_Name) {
		Mandal_Name = mandal_Name;
	}

	/**
	 * @return the village_Name
	 */
	public String getVillage_Name() {
		return Village_Name;
	}

	/**
	 * @param village_Name the village_Name to set
	 */
	public void setVillage_Name(String village_Name) {
		Village_Name = village_Name;
	}

	/**
	 * @return the state_ID
	 */
	public String getState_ID() {
		return State_ID;
	}

	/**
	 * @param state_ID the state_ID to set
	 */
	public void setState_ID(String state_ID) {
		State_ID = state_ID;
	}

	/**
	 * @return the district_ID
	 */
	public String getDistrict_ID() {
		return District_ID;
	}

	/**
	 * @param district_ID the district_ID to set
	 */
	public void setDistrict_ID(String district_ID) {
		District_ID = district_ID;
	}

	/**
	 * @return the mandal_ID
	 */
	public String getMandal_ID() {
		return Mandal_ID;
	}

	/**
	 * @param mandal_ID the mandal_ID to set
	 */
	public void setMandal_ID(String mandal_ID) {
		Mandal_ID = mandal_ID;
	}

	/**
	 * @return the village_ID
	 */
	public String getVillage_ID() {
		return Village_ID;
	}

	/**
	 * @param village_ID the village_ID to set
	 */
	public void setVillage_ID(String village_ID) {
		Village_ID = village_ID;
	}

	/**
	 * @return the no_of_Subscriptions
	 */
	public String getNo_of_Subscriptions() {
		return No_of_Subscriptions;
	}

	/**
	 * @param no_of_Subscriptions the no_of_Subscriptions to set
	 */
	public void setNo_of_Subscriptions(String no_of_Subscriptions) {
		No_of_Subscriptions = no_of_Subscriptions;
	}

	/**
	 * @return the no_of_Connections
	 */
	public String getNo_of_Connections() {
		return No_of_Connections;
	}

	/**
	 * @param no_of_Connections the no_of_Connections to set
	 */
	public void setNo_of_Connections(String no_of_Connections) {
		No_of_Connections = no_of_Connections;
	}

	/**
	 * @return the no_of_Digital_Connections
	 */
	public String getNo_of_Digital_Connections() {
		return No_of_Digital_Connections;
	}

	/**
	 * @param no_of_Digital_Connections the no_of_Digital_Connections to set
	 */
	public void setNo_of_Digital_Connections(String no_of_Digital_Connections) {
		No_of_Digital_Connections = no_of_Digital_Connections;
	}

	/**
	 * @return the no_of_Analog_Connections
	 */
	public String getNo_of_Analog_Connections() {
		return No_of_Analog_Connections;
	}

	/**
	 * @param no_of_Analog_Connections the no_of_Analog_Connections to set
	 */
	public void setNo_of_Analog_Connections(String no_of_Analog_Connections) {
		No_of_Analog_Connections = no_of_Analog_Connections;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AreaID == null) ? 0 : AreaID.hashCode());
		result = prime * result
				+ ((Area_Name == null) ? 0 : Area_Name.hashCode());
		result = prime * result
				+ ((Cable_Type == null) ? 0 : Cable_Type.hashCode());
		result = prime * result
				+ ((District_ID == null) ? 0 : District_ID.hashCode());
		result = prime * result
				+ ((District_Name == null) ? 0 : District_Name.hashCode());
		result = prime
				* result
				+ ((Enrollment_Number == null) ? 0 : Enrollment_Number
						.hashCode());
		result = prime * result
				+ ((Mandal_ID == null) ? 0 : Mandal_ID.hashCode());
		result = prime * result
				+ ((Mandal_Name == null) ? 0 : Mandal_Name.hashCode());
		result = prime
				* result
				+ ((No_of_Analog_Connections == null) ? 0
						: No_of_Analog_Connections.hashCode());
		result = prime
				* result
				+ ((No_of_Connections == null) ? 0 : No_of_Connections
						.hashCode());
		result = prime
				* result
				+ ((No_of_Digital_Connections == null) ? 0
						: No_of_Digital_Connections.hashCode());
		result = prime
				* result
				+ ((No_of_Subscriptions == null) ? 0 : No_of_Subscriptions
						.hashCode());
		result = prime
				* result
				+ ((Running_Legnth_Of_Cable == null) ? 0
						: Running_Legnth_Of_Cable.hashCode());
		result = prime * result
				+ ((State_ID == null) ? 0 : State_ID.hashCode());
		result = prime * result
				+ ((State_Name == null) ? 0 : State_Name.hashCode());
		result = prime * result
				+ ((Village_ID == null) ? 0 : Village_ID.hashCode());
		result = prime * result
				+ ((Village_Name == null) ? 0 : Village_Name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AreaDTO other = (AreaDTO) obj;
		if (AreaID == null) {
			if (other.AreaID != null)
				return false;
		} else if (!AreaID.equals(other.AreaID))
			return false;
		if (Area_Name == null) {
			if (other.Area_Name != null)
				return false;
		} else if (!Area_Name.equals(other.Area_Name))
			return false;
		if (Cable_Type == null) {
			if (other.Cable_Type != null)
				return false;
		} else if (!Cable_Type.equals(other.Cable_Type))
			return false;
		if (District_ID == null) {
			if (other.District_ID != null)
				return false;
		} else if (!District_ID.equals(other.District_ID))
			return false;
		if (District_Name == null) {
			if (other.District_Name != null)
				return false;
		} else if (!District_Name.equals(other.District_Name))
			return false;
		if (Enrollment_Number == null) {
			if (other.Enrollment_Number != null)
				return false;
		} else if (!Enrollment_Number.equals(other.Enrollment_Number))
			return false;
		if (Mandal_ID == null) {
			if (other.Mandal_ID != null)
				return false;
		} else if (!Mandal_ID.equals(other.Mandal_ID))
			return false;
		if (Mandal_Name == null) {
			if (other.Mandal_Name != null)
				return false;
		} else if (!Mandal_Name.equals(other.Mandal_Name))
			return false;
		if (No_of_Analog_Connections == null) {
			if (other.No_of_Analog_Connections != null)
				return false;
		} else if (!No_of_Analog_Connections
				.equals(other.No_of_Analog_Connections))
			return false;
		if (No_of_Connections == null) {
			if (other.No_of_Connections != null)
				return false;
		} else if (!No_of_Connections.equals(other.No_of_Connections))
			return false;
		if (No_of_Digital_Connections == null) {
			if (other.No_of_Digital_Connections != null)
				return false;
		} else if (!No_of_Digital_Connections
				.equals(other.No_of_Digital_Connections))
			return false;
		if (No_of_Subscriptions == null) {
			if (other.No_of_Subscriptions != null)
				return false;
		} else if (!No_of_Subscriptions.equals(other.No_of_Subscriptions))
			return false;
		if (Running_Legnth_Of_Cable == null) {
			if (other.Running_Legnth_Of_Cable != null)
				return false;
		} else if (!Running_Legnth_Of_Cable
				.equals(other.Running_Legnth_Of_Cable))
			return false;
		if (State_ID == null) {
			if (other.State_ID != null)
				return false;
		} else if (!State_ID.equals(other.State_ID))
			return false;
		if (State_Name == null) {
			if (other.State_Name != null)
				return false;
		} else if (!State_Name.equals(other.State_Name))
			return false;
		if (Village_ID == null) {
			if (other.Village_ID != null)
				return false;
		} else if (!Village_ID.equals(other.Village_ID))
			return false;
		if (Village_Name == null) {
			if (other.Village_Name != null)
				return false;
		} else if (!Village_Name.equals(other.Village_Name))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AreaDTO [Enrollment_Number=" + Enrollment_Number + ", AreaID="
				+ AreaID + ", Area_Name=" + Area_Name + ", Cable_Type="
				+ Cable_Type + ", Running_Legnth_Of_Cable="
				+ Running_Legnth_Of_Cable + ", State_Name=" + State_Name
				+ ", District_Name=" + District_Name + ", Mandal_Name="
				+ Mandal_Name + ", Village_Name=" + Village_Name
				+ ", State_ID=" + State_ID + ", District_ID=" + District_ID
				+ ", Mandal_ID=" + Mandal_ID + ", Village_ID=" + Village_ID
				+ ", No_of_Subscriptions=" + No_of_Subscriptions
				+ ", No_of_Connections=" + No_of_Connections
				+ ", No_of_Digital_Connections=" + No_of_Digital_Connections
				+ ", No_of_Analog_Connections=" + No_of_Analog_Connections
				+ "]";
	}
	
}
