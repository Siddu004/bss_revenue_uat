package com.arbiva.apfgc.provision.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.arbiva.apfgc.provision.Exception.COMSException;

/**
 * 
 * @author srinivasa
 *
 */
@Component
@Provider
public class MyApplicationExceptionHandler implements ExceptionMapper<Exception>
{
    @Override
    public Response toResponse(Exception exception)
    {
    	if(exception instanceof COMSException) {
    		return Response.status(Status.BAD_REQUEST).entity(((COMSException)exception).getComsErrorMessageDTO()).build(); 
    	} else {
    		return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build(); 
		}
        
    }
}