package com.arbiva.apfgc.provision.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author srinivasa
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TPSDTO {

	private static final long serialVersionUID = 1L;

	public TPSDTO() {
	
	}
	
	@JsonProperty("card")
	private String card;
	
	private List<Integer> tps;

	/**
	 * @return the card
	 */
	public String getCard() {
		return card;
	}

	/**
	 * @param card the card to set
	 */
	public void setCard(String card) {
		this.card = card;
	}

	/**
	 * @return the tps
	 */
	public List<Integer> getTps() {
		return tps;
	}

	/**
	 * @param tps the tps to set
	 */
	public void setTps(List<Integer> tps) {
		this.tps = tps;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
