package com.arbiva.apfgc.provision.utils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component("mailManager")
public class MailManager {
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Value("${mail.smtp.toreceipients}")
	private String toReciepients;
	
	@Value("${mail.smtp.user.name}")
	private String userName;
	
	@Value("${mail.smtp.subject}")
	private String subject;
	
	@Value("${mail.smtp.msg.body}")
	private String messageBody;
	
	public void sendMail(String attachmentFilePath) {

		MimeMessage message = mailSender.createMimeMessage();
		FileSystemResource file = null;
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(userName);
			helper.setTo(toReciepients);
			helper.setSubject(subject);
			helper.setText(messageBody);

			if(StringUtils.isNotBlank(attachmentFilePath)) {
					file = new FileSystemResource(attachmentFilePath);
					helper.addAttachment(file.getFilename(), file);
			}
			mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new MailParseException(e);
		} finally {
			message = null;
			file = null;
		}
	}
}
