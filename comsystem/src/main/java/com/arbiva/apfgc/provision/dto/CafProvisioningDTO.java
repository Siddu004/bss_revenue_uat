package com.arbiva.apfgc.provision.dto;

import com.arbiva.apfgc.comsystem.BO.CafProvisioningBO;

public class CafProvisioningDTO {
	
	private Long parentCafNo;
	
	private String lmoCode;
	
	private Long pmtCustId;
	
	private String nwSubscriberCode;
	
	private Long prodCafNo;  
	
	private String distId;
	
	private String mandId;
	
	private String villId;
	
	private String packageCode;
	
	

	public CafProvisioningDTO(CafProvisioningBO cafBo, String distId, String mandId, String villId,
			String prodCode, Long prodCafNo) {
		
		this.setDistId(distId);
		this.setLmoCode(cafBo.getLmoCode());
		this.setMandId(mandId);
		this.setNwSubscriberCode(cafBo.getNwSubscriberCode());
		this.setPackageCode(prodCode);
		this.setParentCafNo(cafBo.getParentCafNo());
		this.setPmtCustId(cafBo.getPmtCustId());
		this.setProdCafNo(prodCafNo);
		this.setVillId(villId);
		
	}

	public Long getParentCafNo() {
		return parentCafNo;
	}

	public void setParentCafNo(Long parentCafNo) {
		this.parentCafNo = parentCafNo;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Long getPmtCustId() {
		return pmtCustId;
	}

	public void setPmtCustId(Long pmtCustId) {
		this.pmtCustId = pmtCustId;
	}

	public String getNwSubscriberCode() {
		return nwSubscriberCode;
	}

	public void setNwSubscriberCode(String nwSubscriberCode) {
		this.nwSubscriberCode = nwSubscriberCode;
	}

	public Long getProdCafNo() {
		return prodCafNo;
	}

	public void setProdCafNo(Long prodCafNo) {
		this.prodCafNo = prodCafNo;
	}

	public String getDistId() {
		return distId;
	}

	public void setDistId(String distId) {
		this.distId = distId;
	}

	public String getMandId() {
		return mandId;
	}

	public void setMandId(String mandId) {
		this.mandId = mandId;
	}

	public String getVillId() {
		return villId;
	}

	public void setVillId(String villId) {
		this.villId = villId;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	
	

}
