package com.arbiva.apfgc.provision.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.comsystem.BO.CafProvisioningBO;
import com.arbiva.apfgc.provision.models.ProvRequests;
import com.arbiva.apfgc.provision.utils.COMSConstants;

/**
 * 
 * @author srinivasa
 *
 */
@Repository("provRequestsDAO")
public class ProvRequestsDAO {
	
	private static final Logger LOGGER = Logger.getLogger(ProvRequestsDAO.class);

	private EntityManager em;
	
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	/**
	 * 
	 * @param provRequests
	 * @return
	 */
	@Transactional
	public ProvRequests saveOrUpdate(ProvRequests provRequests) {
		try {
			provRequests = getEntityManager().merge(provRequests);
		}
		catch(Exception ex) {
			LOGGER.error("EXCEPTION::saveOrUpdate() " + ex);
			throw ex;
		}
		return provRequests;
	}
	
	/**
	 * 
	 * @param requestId
	 * @return
	 */
	public ProvRequests findRequestInfo(long requestId) {
		ProvRequests requestInfo = null;
		try {
			requestInfo = getEntityManager().find(ProvRequests.class, requestId);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::findRequestInfo() " + e);
		}
		return requestInfo;
	}
	
	/**
	 * 
	 * @param id
	 * @param request
	 */
	@Transactional
	public void updateProvJsons(long requestid, String request, int seqnum) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("UPDATE olprovjsons set req = :req, status = :stat WHERE requestid = :reqid and seqnum = :seqnum ");
			query.setParameter("req", request);
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqid", requestid);
			query.setParameter("seqnum", seqnum);
			
			query.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateProvJsons() " + e);
		}
		finally {
			query = null;
		}
	}
	
	/**
	 * 
	 * @param id
	 * @param request
	 */
	@Transactional
	public void updateProvJsonUrl(long requestid, String url, int seqnum) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("UPDATE olprovjsons set url = :url, status = :stat WHERE requestid = :reqid and seqnum = :seqnum and servicetype='IPTV' ");
			query.setParameter("url", url);
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqid", requestid);
			query.setParameter("seqnum", 0);
			
			query.executeUpdate();	
			
			query = getEntityManager().createNativeQuery("UPDATE olprovjsons set status = :stat WHERE requestid = :reqid and seqnum in (1,2) and servicetype='IPTV' ");
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqid", requestid);
			
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateProvJsonUrl() " + e);
		}
		finally {
			query = null;
		}
	}
	
	//mahesh-16-01-17 for AAA server
	@Transactional
	public void updateProvJsonUrlForAAA(long requestid, String url, int seqnum) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("UPDATE olprovjsons set url = :url, status = :stat WHERE requestid = :reqid and seqnum = :seqnum and servicetype='AAA' ");
			query.setParameter("url", url);
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqid", requestid);
			query.setParameter("seqnum", 3);
			
			query.executeUpdate();	
			
			/*query = getEntityManager().createNativeQuery("UPDATE olprovjsons set status = :stat WHERE requestid = :reqid and seqnum in (1,2) and servicetype='HSI' ");
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqid", requestid);
			
			query.executeUpdate();*/
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateProvJsonUrlforAAA() " + e);
		}
		finally {
			query = null;
		}
	}
	
	
	
	/**
	 * 
	 * @param id
	 * @param request
	 */
	@Transactional
	public void updateProvRequestStatus(long requestId) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("UPDATE olprovrequests set status = :stat WHERE requestid = :reqId ");
			query.setParameter("stat", COMSConstants.TO_BE_PROCESSED);
			query.setParameter("reqId", requestId);
			
			query.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateProvRequestStatus() " + e);
		}
		finally {
			query = null;
		}
	}
	
	
	@Transactional
	public void updateNwsubscodeProvRequests(long requestId, String nwSubsciberCode) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("UPDATE olprovrequests a "
					+" SET a.nwsubscode = ? "
					+" WHERE a.stbcafno = (select * from (select distinct stbcafno from olprovrequests where requestid=?) tmp) "
					+" AND a.status = 0");
			query.setParameter(1, nwSubsciberCode);
			query.setParameter(2, requestId);
			
			query.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateProvRequestStatus() " + e);
		}
		finally {
			query = null;
		}
	}
	
	/**
	 * 
	 * @param id
	 * @param request
	 */
	@Transactional
	public void updateDunningCafs(long requestId) {
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery("insert into dunningcafs(acctcafno, tenantcode, productcode, srvccode, actdate, duedate, payamt, paidamt, status) "
					+ "select acctcafno, tenantcode, prodcode, srvccodeaddl, now(), STR_TO_DATE('31/12/2999', '%d/%m/%Y'), null, null, 1 from olprovrequests where requestid = :reqId ");
			query.setParameter("reqId", requestId);
			
			query.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateDunningCafs() " + e);
		}
		finally {
			query = null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CafProvisioningBO> getAllCafs(String distId, String mandId, String villId, String corpusPackageCode) {
		List<CafProvisioningBO> list =  null;
		StringBuilder builder = new StringBuilder();
		
		/*builder.append(" select caf.cafno, cs.nwsubscode, caf.lmocode, caf.pmntcustid ");
		builder.append(" from cafs caf, cafsrvcs cs");
		builder.append(" where caf.cafno = cs.parentcafno");
		builder.append(" and srvccode in (select srvccode from srvcs where coresrvccode = 'IPTV')");
		builder.append(" and caf.cafno not in (select cp.parentcafno from cafprods cp where cp.prodcode= :pkeCode)");
		builder.append(" and caf.inst_district= :distCode");
		builder.append(" and caf.inst_mandal= :mandalCode");
		builder.append(" and caf.inst_city_village= :villageCode");*/
		
		builder.append(" SELECT DISTINCT cf1.cafno,cs1.nwsubscode, cf1.lmocode, cf1.pmntcustid  ");
		builder.append("  FROM cafs cf1,cafsrvcs cs1,srvcs s1  ");
		builder.append("  WHERE cf1.inst_district= :distCode AND cf1.inst_mANDal= :mandalCode AND cf1.inst_city_village= :villageCode");
		builder.append(" AND cf1.cafno=cs1.parentcafno AND cs1.srvccode=s1.srvccode AND cs1.status in (2,6) AND s1.coresrvccode='IPTV' ");
		builder.append("  AND NOT EXISTS (SELECT 1 FROM cafprods cp2 WHERE cp2.parentcafno=cf1.cafno AND cp2.tenantcode='APSFL' AND ");
		builder.append(" cp2.prodcode= :pkeCode AND cp2.status in (2,6))");
		
		
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString(), CafProvisioningBO.class);
			query.setParameter("pkeCode", corpusPackageCode);
			query.setParameter("distCode", distId);
			query.setParameter("mandalCode", mandId);
			query.setParameter("villageCode", villId);
			list = query.getResultList();		
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::updateDunningCafs() " + e);
		}
		finally {
			query = null;
		}
		
		return list;
	}
}
