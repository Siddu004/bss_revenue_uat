package com.arbiva.apfgc.provision.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.arbiva.apfgc.provision.utils.MoneySerializer;

public class ChargeDetailsDTO {
	
public ChargeDetailsDTO() {

}

@JsonProperty("chargename")
private String chargename;

@JsonSerialize(using = MoneySerializer.class)
private BigDecimal chargeamt = BigDecimal.ZERO;


public String getChargename() {
	return chargename;
}

public void setChargename(String chargename) {
	this.chargename = chargename;
}

public BigDecimal getChargeamt() {
	return chargeamt;
}

public void setChargeamt(BigDecimal chargeamt) {
	this.chargeamt = chargeamt;
}

}
