package com.arbiva.apfgc.invoice.dto;


public class CafChargeInfoDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	public CafChargeInfoDTO() {
	}

	private String districtid;

	private String cafno;

	/**
	 * @return the districtid
	 */
	public String getDistrictid() {
		return districtid;
	}

	/**
	 * @param districtid the districtid to set
	 */
	public void setDistrictid(String districtid) {
		this.districtid = districtid;
	}

	/**
	 * @return the cafno
	 */
	public String getCafno() {
		return cafno;
	}

	/**
	 * @param cafno the cafno to set
	 */
	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
