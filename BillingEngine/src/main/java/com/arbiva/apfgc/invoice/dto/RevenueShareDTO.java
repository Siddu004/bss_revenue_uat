package com.arbiva.apfgc.invoice.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class RevenueShareDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5416685485829392130L;
	
	private String invno;
	
	private String cafno;
	
	private String chargecode;
	
	private BigDecimal apsflshare;
	
	private BigDecimal lmoshare;
	
	private BigDecimal msoshare;

	public String getInvno() {
		return invno;
	}

	public void setInvno(String invno) {
		this.invno = invno;
	}

	public String getCafno() {
		return cafno;
	}

	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	public String getChargecode() {
		return chargecode;
	}

	public void setChargecode(String chargecode) {
		this.chargecode = chargecode;
	}

	public BigDecimal getApsflshare() {
		return apsflshare;
	}

	public void setApsflshare(BigDecimal apsflshare) {
		this.apsflshare = apsflshare;
	}

	public BigDecimal getLmoshare() {
		return lmoshare;
	}

	public void setLmoshare(BigDecimal lmoshare) {
		this.lmoshare = lmoshare;
	}

	public BigDecimal getMsoshare() {
		return msoshare;
	}

	public void setMsoshare(BigDecimal msoshare) {
		this.msoshare = msoshare;
	}


}
