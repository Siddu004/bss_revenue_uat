
package com.arbiva.apfgc.invoice.dto;
/**
Gowthami
*/
public class CafDetailsDTO {

	private String cafNo;
	
	private String district;

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
}

