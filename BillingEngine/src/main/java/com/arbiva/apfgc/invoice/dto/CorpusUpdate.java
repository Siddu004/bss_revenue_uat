
package com.arbiva.apfgc.invoice.dto;
/**
Gowthami
*/
public class CorpusUpdate {

	private String subscribercode;
	
	private String subject;
	
	private String message;

	public String getSubscribercode() {
		return subscribercode;
	}

	public void setSubscribercode(String subscribercode) {
		this.subscribercode = subscribercode;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

