package com.arbiva.apfgc.invoice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	public ContactDTO() {
	}

	private int id;

	private String phone_1;

	private String phone_2;

	private String mobile;

	private String fax;

	@JsonProperty("email_id")
	private String emailId;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the phone_1
	 */
	public String getPhone_1() {
		return phone_1;
	}

	/**
	 * @param phone_1
	 *            the phone_1 to set
	 */
	public void setPhone_1(String phone_1) {
		this.phone_1 = phone_1;
	}

	/**
	 * @return the phone_2
	 */
	public String getPhone_2() {
		return phone_2;
	}

	/**
	 * @param phone_2
	 *            the phone_2 to set
	 */
	public void setPhone_2(String phone_2) {
		this.phone_2 = phone_2;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}
