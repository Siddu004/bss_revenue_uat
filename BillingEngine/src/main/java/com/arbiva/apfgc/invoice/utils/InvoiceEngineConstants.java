package com.arbiva.apfgc.invoice.utils;

/**
 * {@link InvoiceEngineConstants} holding the application specific constants.
 * 
 * @author srinivasa
 *
 */
public class InvoiceEngineConstants {

	// the followings are the args which are in same order of basefilePath, billPeriod, accountNumber, billPeriod
	public static String INVOICE_JSON_FILE_PATH = "%s/%s/%s/%s/%s_bill_period_%s.json";
	
	public static String INVOICE_REPORT_FILE_NAME = "%s_bill_period_%s.html";
	
	public static String MONTH_PATTERN_IN_VALUE = "M";
	public static String MONTH_PATTERN_IN_NAME = "MMM";
	public static String YEAR_PATTERN = "YYYY";
	
	public static String MONTH_PATTERN_IN_VALUE_WITH_ZERO = "MM";
}
