package com.arbiva.apfgc.invoice.dto;

public class DataUsageDTO {

	public DataUsageDTO() {
	}

	private String units;

	private String duration;

	private String descr;

	

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	

}
