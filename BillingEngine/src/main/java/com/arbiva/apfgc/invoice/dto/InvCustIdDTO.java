package com.arbiva.apfgc.invoice.dto;

public class InvCustIdDTO {
	
	private String invno;
	
	private String pmntcustid;
	
	private String custTypeLOV;

	public String getInvno() {
		return invno;
	}

	public void setInvno(String invno) {
		this.invno = invno;
	}

	public String getPmntcustid() {
		return pmntcustid;
	}

	public void setPmntcustid(String pmntcustid) {
		this.pmntcustid = pmntcustid;
	}

	public String getCustTypeLOV() {
		return custTypeLOV;
	}

	public void setCustTypeLOV(String custTypeLOV) {
		this.custTypeLOV = custTypeLOV;
	}
	
}
