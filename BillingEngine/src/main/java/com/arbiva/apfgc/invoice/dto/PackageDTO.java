package com.arbiva.apfgc.invoice.dto;

public class PackageDTO {

	public PackageDTO() {
	}

	private String prodcode;
	
	private String date;

	private String description;

	/**
	 * @return the prodcode
	 */
	public String getProdcode() {
		return prodcode;
	}

	/**
	 * @param prodcode the prodcode to set
	 */
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
}
