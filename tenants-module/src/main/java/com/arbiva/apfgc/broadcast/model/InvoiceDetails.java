package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name ="broadcastinvdtl")
@IdClass(InvoiceDetailsPK.class)
public class InvoiceDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "invno")
	private Long invoiceNumber;
	
	@Id
	@Column(name = "pricegroupcode")
	private String priceGroupCode;
	
	@Column(name = "subscibercnt")
	private Long subscriberCount;
	
	@Column(name = "unitprice")
	private float unitPrice;
	
	@Column(name = "srvctax")
	private float serviceTax;
	
	@Column(name = "swatchtax")
	private float swatchTax;
	
	@Column(name = "kisantax")
	private float kissanTax;

	public Long getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Long invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPriceGroupCode() {
		return priceGroupCode;
	}

	public void setPriceGroupCode(String priceGroupCode) {
		this.priceGroupCode = priceGroupCode;
	}

	public Long getSubscriberCount() {
		return subscriberCount;
	}

	public void setSubscriberCount(Long subscriberCount) {
		this.subscriberCount = subscriberCount;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public float getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(float serviceTax) {
		this.serviceTax = serviceTax;
	}

	public float getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(float swatchTax) {
		this.swatchTax = swatchTax;
	}

	public float getKissanTax() {
		return kissanTax;
	}

	public void setKissanTax(float kissanTax) {
		this.kissanTax = kissanTax;
	}
	
	

}
