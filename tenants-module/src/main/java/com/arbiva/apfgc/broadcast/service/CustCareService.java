package com.arbiva.apfgc.broadcast.service;

import java.util.List;

import com.arbiva.apfgc.broadcast.model.AadharHelperDTO;


public interface CustCareService {
	
	AadharHelperDTO getCustomersDetails(Long custId);
	
	AadharHelperDTO getCustAdharDetails(Long aadharNo);
	 
	 List<Object[]> custCafDtls(Long aadharNo);

}
