package com.arbiva.apfgc.broadcast.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.broadcast.dao.CustCareDao;



@Repository
public class CustCareDaoImpl implements CustCareDao {
	
private static final Logger logger = Logger.getLogger(CustCareDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCustomersDetails(Long custId) {
		Query query = null;
		List<Object[]> customerList = new ArrayList<Object[]>();
		StringBuilder builder;
		try {
			logger.info("CustCareDaoImpl :: getCustomersDetails() :: Start");
			if((String.valueOf(custId).length()) < 10 ) {
				builder = new StringBuilder("SELECT cust.custname,cust.lname,cf.cafno,cf.custid,(cust.regbal+cust.chargedbal) balance,cust.custdistuid,cust.custid as custid1 FROM customermst cust,cafs cf ");
	            builder.append(" WHERE cust.custid=cf.custid AND cf.custid = '"+custId+"' ");
			}else {
				builder = new StringBuilder("SELECT cust.custname,cust.lname,cf.cafno,cf.custid,(cust.regbal+cust.chargedbal) balance,cust.custdistuid,cust.custid as custid1 FROM customermst cust,cafs cf  ");
	            builder.append(" WHERE cf.custid IN (SELECT custid FROM customermst WHERE pocmob1 = '"+custId+"') AND cust.custid=cf.custid  ");
			}
			query = getEntityManager() .createNativeQuery(builder.toString());
			customerList = query.getResultList();
			logger.info("CustCareDaoImpl :: getCustomersDetails() :: End");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerDetailsDAO :: getCustomersDetails()" + e);
		}
		return customerList;
	}
	
	public <T> T saveOrUpdate(T t) {
		logger.info("CustCareDaoImpl :: saveOrUpdate() :: Start");
		return getEntityManager().merge(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCustAdharDetails(Long aadharNo) {
		Query query = null;
		List<Object[]> customerList = null;
		StringBuilder builder;
		try {
			logger.info("CustCareDaoImpl :: getCustomersDetails() :: Start");
				/*builder = new StringBuilder("SELECT cust.custname,cust.lname,cf.cafno,(cust.regbal+cust.chargedbal) balance,cust.custdistuid,cust.custid,cust.custtypelov,  ");
				builder.append(" cust.pocmob1,cust.email1,cust.addr1,cust.addr2,cust.locality,v.villagename,CONCAT(cust.stdcode,' ',cust.landline1) landline, ");
	            builder.append(" cust.lmocode  FROM customermst cust,cafs cf,villages v WHERE cust.custid=cf.custid AND v.villageslno = cust.city_village ");
	            builder.append(" AND v.mandalslno = cust.mandal AND v.districtuid = cust.district AND cust.aadharno =  '"+aadharNo+"'");*/
			
			builder = new StringBuilder(" SELECT cf.cafno, cf.aadharno, DATE_FORMAT(cf.cafdate,'%d/%m/%Y'), w.statusdesc, c.custname,c.lname, ");
			builder.append(" (SELECT GROUP_CONCAT(DISTINCT p1.prodname) products FROM cafprods cp1,products p1 ");
            builder.append(" WHERE cp1.parentcafno = cf.cafno AND cp1.tenantcode = p1.tenantcode ");
            builder.append(" AND cp1.prodcode = p1.prodcode AND cf.cafdate BETWEEN p1.effectivefrom AND p1.effectiveto ) products, ");
            builder.append("  c.enttypelov, cf.pop_substnno, cf.custid, cf.inst_city_village, cf.inst_district, cf.inst_mandal,  ");
            builder.append(" c.billfreqlov, cf.status, cf.lmocode,  (SELECT substnname FROM substations WHERE substnuid = cf.pop_substnno) AS popname,");
            builder.append(" cf.apsfluniqueid, cf.inst_addr1, cf.inst_addr2, cf.contactmob, cf.cpeplace, cf.contactperson, ");
            builder.append(" (SELECT GROUP_CONCAT(phoneno) FROM cafsrvcphonenos WHERE parentcafno = cf.cafno) phoneNo, cf.custtypelov caftype,");
            builder.append(" (SELECT cafno FROM cafprods cp WHERE cp.parentcafno = cf.cafno  AND EXISTS  ");
            builder.append(" (SELECT 1 FROM cafsrvcs cs WHERE cs.cafno=cp.cafno AND cs.tenantcode=cp.tenantcode AND cs.prodcode=cp.prodcode ");
            builder.append(" AND cs.parentcafno=cp.parentcafno AND cs.status IN(6, 7))  AND EXISTS (SELECT 1 FROM products p  WHERE p.tenantcode=cp.tenantcode ");
            builder.append(" AND p.prodcode=cp.prodcode AND p.prodtype='B' )) prodcafno ,(c.regbal+c.chargedbal) balance  FROM cafs cf, customermst c ,wipstages w WHERE cf.custid = c.custid ");
            builder.append(" AND w.appcode = 'COMS' AND w.statuscode = cf.status  AND cf.status = 6  AND c.aadharno = '"+aadharNo+"' GROUP BY cf.cafno ORDER BY custname ASC  ");
            
	            query = getEntityManager().createNativeQuery(builder.toString());
				customerList = query.getResultList();
				logger.info("CustCareDaoImpl :: getCustomersDetails() :: End");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerDetailsDAO :: getCustomersDetails()" + e);
		}
		return customerList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> custCafDtls(Long aadharNo) {
		Query query = null;
		List<Object[]> customerList = new ArrayList<Object[]>();
		StringBuilder builder;
		try {
			logger.info("CustCareDaoImpl :: custCafDtls() :: Start");
				builder = new StringBuilder("SELECT cust.custname,cf.cafno,cust.lname,cust.aadharno,cf.pmntcustid,cust.custid,cust.custtypelov  ");
	            builder.append(" ,COALESCE(COALESCE(cust.landline1),COALESCE(cust.landline2)) AS landline,cust.pocmob1,cust.email1,cust.addr1 ");
	             
	            builder.append(" ,(SELECT districtname FROM districts WHERE districts.districtuid = cust.district) AS district_name");
	            builder.append(" ,(SELECT mandalname FROM mandals WHERE districtuid=cust.district AND mandalslno=cust.mandal) AS mandal_name,(SELECT villagename FROM villages WHERE districtuid=cust.district AND mandalslno=cust.mandal AND villageslno=cust.city_village) AS village_name ");
	            
	            builder.append(" FROM customermst cust,cafs cf ");
	            builder.append(" WHERE cust.custid=cf.pmntcustid AND cust.aadharno ='"+aadharNo+"' AND cf.status IN (2,6,7) AND cust.status='2'");
				
	            query = getEntityManager() .createNativeQuery(builder.toString());
				customerList = query.getResultList();
				logger.info("CustCareDaoImpl :: getCustomersDetails() :: End");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("CustomerDetailsDAO :: getCustomersDetails()" + e);
		}
		return customerList;
	}
}
