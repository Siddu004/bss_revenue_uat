package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;
import java.util.List;

public class AadharHelperDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<AadharDtlsVO> aadharDtlsVO;

	public List<AadharDtlsVO> getAadharDtlsVO() {
		return aadharDtlsVO;
	}

	public void setAadharDtlsVO(List<AadharDtlsVO> aadharDtlsVO) {
		this.aadharDtlsVO = aadharDtlsVO;
	}

	
}
