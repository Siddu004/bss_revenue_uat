package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="broadcastinv")
public class MonthlyInvoice  implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	public MonthlyInvoice() {
		
	}

	@Id
	@Column(name = "invno")
	private Long invoiceNumber;
	
	@Column(name = "broadcaster")
	private String broadcasterCode;
	
	@Column(name = "invdate")
	private Date invoiceDate;
	
	@Column(name = "invmon")
	private int invoiceMonth;
	
	@Column(name = "invamt")
	private float invoiceAmount;
	
	@Column(name = "srvctax")
	private float serviceTax;
	
	@Column(name = "swatchtax")
	private float swatchTax;
	
	@Column(name = "kisantax")
	private float kissanTax;

	public Long getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Long invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getBroadcasterCode() {
		return broadcasterCode;
	}

	public void setBroadcasterCode(String broadcasterCode) {
		this.broadcasterCode = broadcasterCode;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public int getInvoiceMonth() {
		return invoiceMonth;
	}

	public void setInvoiceMonth(int invoiceMonth) {
		this.invoiceMonth = invoiceMonth;
	}

	public float getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(float invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public float getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(float serviceTax) {
		this.serviceTax = serviceTax;
	}

	public float getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(float swatchTax) {
		this.swatchTax = swatchTax;
	}

	public float getKissanTax() {
		return kissanTax;
	}

	public void setKissanTax(float kissanTax) {
		this.kissanTax = kissanTax;
	}
	
	
}
