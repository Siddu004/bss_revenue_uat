package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="iptvchnlratecard")
public class IptvChannelRateCard implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = " broadcaster")
	 private String broadcaster;
	
	@Id
	@Column(name = "pricegroupcode")
	private String priceGroupCode;
	
	 @Id
	 @Column(name = "effectivefrom")
	 private Date effectiveFrom;
	
	 @Column(name = "unitprice") 
	 private float unitPrice;

	public String getBroadcaster() {
		return broadcaster;
	}

	public void setBroadcaster(String broadcaster) {
		this.broadcaster = broadcaster;
	}

	public String getPriceGroupCode() {
		return priceGroupCode;
	}

	public void setPriceGroupCode(String priceGroupCode) {
		this.priceGroupCode = priceGroupCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

}
