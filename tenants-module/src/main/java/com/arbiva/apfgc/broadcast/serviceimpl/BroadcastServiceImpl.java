package com.arbiva.apfgc.broadcast.serviceimpl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.broadcast.dao.BroadcastDao;
import com.arbiva.apfgc.broadcast.model.InvoiceDetails;
import com.arbiva.apfgc.broadcast.model.IptvChannelRateCard;
import com.arbiva.apfgc.broadcast.model.MonthlyInvoice;
import com.arbiva.apfgc.broadcast.service.BroadcastService;
import com.arbiva.apfgc.tanent.util.TaxPropertiesFile;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;

@Service
public class BroadcastServiceImpl implements BroadcastService {
	
	private static final Logger logger = Logger.getLogger(BroadcastServiceImpl.class);
	
	@Autowired
	BroadcastDao broadcastDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	TaxPropertiesFile taxPropertiesFile;	
	
	@Override
	@Transactional
	public  List<Object[]> getMonthlyInvoiceDetails() {
		return broadcastDao.getMonthlyInvoiceDetails();
	}
	

	@Override
	@Transactional
	public  List<IptvChannelRateCard> getBroadcasterCode() {
		return broadcastDao.getBroadcasterCode();
	}
	
	@Override
	@Transactional
	public  List<Object[]> getBroadcasterPriceDetails(String broadcasterCode) {
		return broadcastDao.getBroadcasterPriceDetails(broadcasterCode);
	}
	
	@Override
	@Transactional
	public  List<Object[]> getChannelDetails(String priceGroupCode) {
		return broadcastDao.getChannelDetails(priceGroupCode);
	}
	
	@Override
	@Transactional
	public  List<Object[]> getInvoiceDetails(Long invNo) {
		return broadcastDao.getInvoiceDetails(invNo);
	}


	@Override
	@Transactional
	public void invoiceDatePopulation() {
		Map<String, Float> map = new HashMap<>();
		List<Object[]> invoiceList = null;
		Long invoiceId = null;
		InvoiceDetails invoiceDatils = null;
		MonthlyInvoice monthlyInvoice = null;
		String monthDay = null;
		
		try {
			logger.info("BroadcastServicdeImpl :: invoiceDatePopulation :: START");
			invoiceList = broadcastDao.getInvoiceDateToPopulateData();
			monthDay = this.getPrevMonthYearInMMYY();
			for(Object[] obj : invoiceList){
				Float i = 0.0F ;
				Float total = Float.parseFloat(obj[2].toString()) * Float.parseFloat(obj[3].toString()) ;
				if(map.containsKey(obj[0].toString())){
					 i =  total + (map.get(obj[0].toString())); 
					map.put(obj[0].toString(), i);
				}
				else
					map.put(obj[0].toString(), total);
			}
			
			for (Entry<String,Float> ent : map.entrySet()){
				
				invoiceId = storedProcedureDAO.executeStoredProcedure("INVOICENO");
			    monthlyInvoice = new MonthlyInvoice();
				monthlyInvoice.setInvoiceNumber(invoiceId);
				monthlyInvoice.setBroadcasterCode(ent.getKey());
				monthlyInvoice.setInvoiceAmount(ent.getValue());
				monthlyInvoice.setInvoiceDate(Calendar.getInstance().getTime());
				monthlyInvoice.setInvoiceMonth(Integer.parseInt(monthDay));
				monthlyInvoice.setServiceTax(ent.getValue() * Float.parseFloat(taxPropertiesFile.getServiceTax()) / 100);
				monthlyInvoice.setKissanTax(ent.getValue() * Float.parseFloat(taxPropertiesFile.getKisanTax()) / 100);
				monthlyInvoice.setSwatchTax(ent.getValue() * Float.parseFloat(taxPropertiesFile.getSwatchTax()) / 100 );
				broadcastDao.save(monthlyInvoice);
				
				for(Object[] obj : invoiceList){
					if(obj[0].toString().equalsIgnoreCase(ent.getKey())){
						invoiceDatils = new InvoiceDetails();
						invoiceDatils.setInvoiceNumber(invoiceId);
						invoiceDatils.setPriceGroupCode(obj[1].toString());
						invoiceDatils.setSubscriberCount(Long.parseLong(obj[2].toString()));
						invoiceDatils.setUnitPrice(Float.parseFloat(obj[3].toString()));
						invoiceDatils.setServiceTax(Float.parseFloat(obj[3].toString()) * Float.parseFloat(taxPropertiesFile.getServiceTax()) / 100 );
						invoiceDatils.setKissanTax(Float.parseFloat(obj[3].toString()) * Float.parseFloat(taxPropertiesFile.getKisanTax()) / 100 );
						invoiceDatils.setSwatchTax(Float.parseFloat(obj[3].toString()) * Float.parseFloat(taxPropertiesFile.getSwatchTax()) / 100 );
						broadcastDao.save(invoiceDatils);
					}
				}
			}
			logger.info("BroadcastServicdeImpl :: invoiceDatePopulation :: END");
		}catch(Exception ex){
			
			logger.error("BroadcastServicdeImpl :: invoiceDatePopulation" +ex);
			
		}finally{
			map = null;
			invoiceList = null;
			invoiceId = null;
			invoiceDatils = null;
			monthlyInvoice = null;
			monthDay = null;
		}
		
	}
	
	public  String getPrevMonthYearInMMYY(){
		  StringBuffer year=new StringBuffer();
		  Calendar now = Calendar.getInstance();
		try {
			logger.info("BroadcastServicdeImpl :: getPrevMonthYearInMMYY :: START");
		    now.add(Calendar.MONTH,0);
		    year.append(now.get(Calendar.MONTH));
		    year.append(new StringBuffer().append(now.get(Calendar.YEAR)).substring(2));
		    logger.info("BroadcastServicdeImpl :: getPrevMonthYearInMMYY :: END");
		} catch(Exception e) {
			logger.error("BroadcastServicdeImpl :: getPrevMonthYearInMMYY" +e);
		} finally {
			now = null;
		}
	    return year.toString();
	}
}
