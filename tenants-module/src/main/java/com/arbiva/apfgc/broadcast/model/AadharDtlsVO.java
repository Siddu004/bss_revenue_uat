package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;

public class AadharDtlsVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String fname;
	
	private String lname;
	
	private String cafno;
	
	private String balance;
	
	private String districtno;

	private String custid;
	
	private String custtypelov;
	
	private String mobileNo;
	
	private String email;
	
	private String address;
	
	private String landline;
	
	private String tenantCode;
	
	private String lmoName;
	
	private String aadharNo;
	
	private String usageLimit;
	
	private String totalLimit;
	
	private String status;
	
	private String count;
	
	private String date;
	
	private String prodCafNo;
	
	public String getProdCafNo() {
		return prodCafNo;
	}

	public void setProdCafNo(String prodCafNo) {
		this.prodCafNo = prodCafNo;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCafno() {
		return cafno;
	}

	public void setCafno(String cafno) {
		this.cafno = cafno;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getDistrictno() {
		return districtno;
	}

	public void setDistrictno(String districtno) {
		this.districtno = districtno;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getCusttypelov() {
		return custtypelov;
	}

	public void setCusttypelov(String custtypelov) {
		this.custtypelov = custtypelov;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getLmoName() {
		return lmoName;
	}

	public void setLmoName(String lmoName) {
		this.lmoName = lmoName;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getUsageLimit() {
		return usageLimit;
	}

	public void setUsageLimit(String usageLimit) {
		this.usageLimit = usageLimit;
	}

	public String getTotalLimit() {
		return totalLimit;
	}

	public void setTotalLimit(String totalLimit) {
		this.totalLimit = totalLimit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	
}
