package com.arbiva.apfgc.broadcast.dao;

import java.util.List;

public interface CustCareDao {
	
    List<Object[]> getCustomersDetails(Long custId);
	
	<T> T saveOrUpdate(T t);
	
	List<Object[]> getCustAdharDetails(Long aadharNo);
	
	List<Object[]> custCafDtls(Long aadharNo); 


}
