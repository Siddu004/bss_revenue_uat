package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="iptvchnlgroups")
public class IptvChannelGroups implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "chnlcode")
	private String chnlCode;
	
	@Id
	@Column(name = "effectivefrom")
	 private Date effectiveFrom;
	
	@Column(name = "broadcaster")
	 private String broadcaster;
	
	@Column(name = "pricegroupcode")
	private String priceGroupCode;

	public String getChnlCode() {
		return chnlCode;
	}

	public void setChnlCode(String chnlCode) {
		this.chnlCode = chnlCode;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getBroadcaster() {
		return broadcaster;
	}

	public void setBroadcaster(String broadcaster) {
		this.broadcaster = broadcaster;
	}

	public String getPriceGroupCode() {
		return priceGroupCode;
	}

	public void setPriceGroupCode(String priceGroupCode) {
		this.priceGroupCode = priceGroupCode;
	}
	
	

}
