package com.arbiva.apfgc.broadcast.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.broadcast.model.InvoiceDetails;
import com.arbiva.apfgc.broadcast.model.IptvChannelRateCard;
import com.arbiva.apfgc.broadcast.model.MonthlyInvoice;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;

@Repository
public class BroadcastDao {

	private static final Logger logger = Logger.getLogger(BroadcastDao.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}


	@SuppressWarnings("unchecked")
	public List<Object[]> getMonthlyInvoiceDetails() {

		List<Object[]> monthlyInvoiceList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder();
		try {
			logger.info(" BroadcastDao :: getMonthlyInvoiceDetails  :: START");
			 builder.append("select invno, broadcaster, invdate, invmon, invamt, srvctax, swatchtax, kisantax,");
			builder.append(" CONCAT (SUBSTRING(invmon, 1,(CHAR_LENGTH(invmon)-2))," + "'/'" + ",SUBSTRING(invmon, CHAR_LENGTH(invmon)-1)) AS RES,");
			builder.append("(invamt+ srvctax+ swatchtax+ kisantax) as total from broadcastinv");
			logger.info("builder" + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			monthlyInvoiceList = query.getResultList();
			logger.info(" BroadcastDao :: getMonthlyInvoiceDetails  :: END");
			} catch (Exception e) {
			logger.error("The Exception is BroadcastDao :: getMonthlyInvoiceDetails" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}

		return monthlyInvoiceList;
	}

	@SuppressWarnings("unchecked")
	public List<IptvChannelRateCard> getBroadcasterCode() {

		List<IptvChannelRateCard> broadcasterCodeList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select distinct broadcaster from iptvchnlratecard");

		try {
			logger.info(" BroadcastDao :: getBroadcasterCode  :: START");
			query = getEntityManager().createNativeQuery(builder.toString());
			broadcasterCodeList = query.getResultList();
			logger.info(" BroadcastDao :: getBroadcasterCode  :: END");
		} catch (Exception e) {

			logger.error("The Exception is BroadcastDao :: getBroadcasterCode " + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return broadcasterCodeList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getBroadcasterPriceDetails(String broadcasterCode) {

		List<Object[]> broadcasterCodeList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select pricegroupcode,unitprice,broadcaster from iptvchnlratecard where broadcaster = '"	+ broadcasterCode + "' ");
		try {
			logger.info(" BroadcastDao :: getBroadcasterPriceDetails  :: START");
			query = getEntityManager().createNativeQuery(builder.toString());
			broadcasterCodeList = query.getResultList();
			logger.info(" BroadcastDao :: getBroadcasterPriceDetails  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastDao :: getBroadcasterPriceDetails" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return broadcasterCodeList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getChannelDetails(String priceGroupCode) {
		Query query = null;
		StringBuilder builder = new StringBuilder();
		List<Object[]> channelList = new ArrayList<Object[]>();
		try {
			logger.info(" BroadcastDao :: getChannelDetails  :: START");
			builder.append("select sf.featurecode , sf.featurename from srvcfeatures sf ");
			builder.append("inner join iptvchnlgroups cg on sf.featurecode = cg.chnlcode ");
			builder.append("inner join iptvchnlratecard rc on cg.pricegroupcode = rc.pricegroupcode and rc.pricegroupcode = '" + priceGroupCode + "'");
			
			query = getEntityManager().createNativeQuery(builder.toString());
			channelList = query.getResultList();
			logger.info(" BroadcastDao :: getChannelDetails  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastDao :: getChannelDetails" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return channelList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getInvoiceDetails(Long invNo) {
		StringBuilder builder = new StringBuilder();
		Query query = null;
		List<Object[]> invoiceList = new ArrayList<Object[]>();
		try {
			logger.info(" BroadcastDao :: getInvoiceDetails  :: START");
			builder.append("select dtl.invno, dtl.pricegroupcode, dtl.subscibercnt, dtl.unitprice, ");
			builder.append("dtl.srvctax, dtl.swatchtax, dtl.kisantax from broadcastinvdtl dtl , broadcastinv inv ");
			builder.append("where dtl.invno = inv.invno and inv.invno='" + invNo + "'");
			query = getEntityManager().createNativeQuery(builder.toString());
			invoiceList = query.getResultList();
			logger.info(" BroadcastDao :: getInvoiceDetails  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastDao :: getInvoiceDetails" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return invoiceList;
	}

	/*
	 * SELECT cg.broadcaster,cg.pricegroupcode,COUNT(DISTINCT
	 * cfsrv.cafno),rc.unitprice #,COUNT(cfsrv.cafno) FROM cafsrvcs cfsrv,srvcs
	 * s,iptvchnlgroups cg,iptvchnlratecard rc WHERE cfsrv.status>6 AND
	 * s.coresrvccode='IPTV' AND cfsrv.srvccode=s.srvccode AND
	 * INSTR(CONCAT(',',s.featurecodes,','),CONCAT(',',cg.chnlcode,',')) > 0 AND
	 * cg.effectivefrom = (SELECT MAX(cg1.effectivefrom) FROM iptvchnlgroups cg1
	 * WHERE cg1.chnlcode=cg.chnlcode AND cg1.effectivefrom <= CURRENT_DATE())
	 * AND cg.broadcaster = rc.broadcaster AND
	 * cg.pricegroupcode=rc.pricegroupcode AND rc.effectivefrom = (SELECT
	 * MAX(rc1.effectivefrom) FROM iptvchnlratecard rc1 WHERE rc1.broadcaster =
	 * rc.broadcaster AND rc1.pricegroupcode=rc.pricegroupcode AND
	 * rc1.effectivefrom <= CURRENT_DATE()) GROUP BY
	 * cg.broadcaster,cg.pricegroupcode,rc.unitprice
	 */

	@SuppressWarnings("unchecked")
	public List<Object[]> getInvoiceDateToPopulateData() {

		List<Object[]> invoiceList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			logger.info(" BroadcastDao :: getInvoiceDateToPopulateData  :: START");
			builder.append(" SELECT cg.broadcaster,cg.pricegroupcode,COUNT(DISTINCT cfsrv.cafno),rc.unitprice");
			builder.append(" FROM cafsrvcs cfsrv,srvcs s,iptvchnlgroups cg,iptvchnlratecard rc");
			builder.append(" WHERE cfsrv.status>3 AND s.coresrvccode='IPTV' AND cfsrv.srvccode=s.srvccode");
			builder.append(" AND INSTR(CONCAT(',',s.featurecodes,','),CONCAT(',',cg.chnlcode,',')) > 0");
			builder.append(" AND cg.effectivefrom = (SELECT MAX(cg1.effectivefrom) FROM iptvchnlgroups   cg1");
			builder.append(" WHERE cg1.chnlcode=cg.chnlcode AND cg1.effectivefrom <= CURRENT_DATE())");
			builder.append(" AND cg.broadcaster = rc.broadcaster AND cg.pricegroupcode=rc.pricegroupcode");
			builder.append(" AND rc.effectivefrom =  (SELECT MAX(rc1.effectivefrom) FROM iptvchnlratecard rc1");
			builder.append(" WHERE rc1.broadcaster = rc.broadcaster AND rc1.pricegroupcode=rc.pricegroupcode AND rc1.effectivefrom <= CURRENT_DATE())");
			builder.append(" GROUP BY cg.broadcaster,cg.pricegroupcode,rc.unitprice");
			query = getEntityManager().createNativeQuery(builder.toString());
			invoiceList = query.getResultList();
			logger.info(" BroadcastDao :: getInvoiceDateToPopulateData  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastDao :: getInvoiceDateToPopulateData" + e);
			e.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}
		return invoiceList;
	}

	public <T> T save(T t) {
		return getEntityManager().merge(t);
	}


}
