package com.arbiva.apfgc.broadcast.service;

import java.util.List;

import com.arbiva.apfgc.broadcast.model.DailyInvoice;
import com.arbiva.apfgc.broadcast.model.IptvChannelRateCard;
import com.arbiva.apfgc.broadcast.model.MonthlyInvoice;


public interface BroadcastService {
	
	public abstract List<Object[]> getMonthlyInvoiceDetails();

	public abstract List<IptvChannelRateCard> getBroadcasterCode();
	
	public abstract List<Object[]> getBroadcasterPriceDetails(String broadcasterCode);
	
	public abstract List<Object[]> getChannelDetails(String priceGroupCode);
	
	public abstract List<Object[]> getInvoiceDetails(Long invNo);

	public abstract void invoiceDatePopulation();
	
}
