package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

public class BroadcastPayments implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "pmntrefno")
	private String paymentRefNo;
	
	@Column(name = "pmntdate")
	private Date paymentDate;
	
	@Column(name = "pmntamt")
	private float paymentAmount;
	
	@Column(name = "invno")
	private Long invoiceNo;

	public String getPaymentRefNo() {
		return paymentRefNo;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		this.paymentRefNo = paymentRefNo;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public float getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(float paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Long getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Long invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	

}
