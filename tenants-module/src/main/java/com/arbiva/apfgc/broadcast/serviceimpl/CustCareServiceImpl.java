package com.arbiva.apfgc.broadcast.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.broadcast.dao.CustCareDao;
import com.arbiva.apfgc.broadcast.model.AadharDtlsVO;
import com.arbiva.apfgc.broadcast.model.AadharHelperDTO;
import com.arbiva.apfgc.broadcast.service.CustCareService;



@Service
public class CustCareServiceImpl implements CustCareService {
	
	private static final Logger logger = Logger.getLogger( CustCareServiceImpl.class); 
	
	@Autowired
	CustCareDao custCareDao;

	@Transactional
	@Override
	public AadharHelperDTO getCustomersDetails(Long custId) {
		List<Object[]> customerList = new ArrayList<>();
		AadharHelperDTO aadharDTO = new AadharHelperDTO();
		List<AadharDtlsVO> aadhardtsl = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: getCustomersDetails() :: Start");
		   customerList = custCareDao.getCustomersDetails(custId);
		   
		   for(Object[] obj : customerList) {
				
				AadharDtlsVO aadharVo = new AadharDtlsVO();
				aadharVo.setFname(obj[0] != null ? obj[0].toString() : "NA");
				aadharVo.setLname(obj[1] != null ? obj[1].toString() : "NA");
				aadharVo.setCafno(obj[2] != null ? obj[2].toString() : "NA");
				aadharVo.setBalance(obj[4] != null ? obj[4].toString() : "NA");
				aadharVo.setDistrictno(obj[5] != null ? obj[5].toString() : "NA");
				aadharVo.setCustid(obj[6] != null ? obj[6].toString() : "NA");
				aadhardtsl.add(aadharVo);
			}
		   aadharDTO.setAadharDtlsVO(aadhardtsl);
		   logger.info("CustomerServiceImpl :: getCustomersDetails() :: Start");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(" CustomerServiceImpl :: getCustomersDetails() " + e);
		}
		return aadharDTO;
	}

	@Override
	@Transactional
	public AadharHelperDTO getCustAdharDetails(Long aadharNo) {
		
		List<Object[]> customerList = new ArrayList<>();
		AadharHelperDTO aadharDTO = new AadharHelperDTO();
		List<AadharDtlsVO> aadhardtsl = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: getCustomersDetails() :: Start");
		   customerList = custCareDao.getCustAdharDetails(aadharNo);
		   
		   for(Object[] obj : customerList) {
				Object addr = obj[18]+","+obj[19];
				AadharDtlsVO aadharVo = new AadharDtlsVO();
				aadharVo.setFname(obj[4] != null ? obj[4].toString() : "NA");
				aadharVo.setLname(obj[5] != null ? obj[5].toString() : "NA");
				aadharVo.setCafno(obj[0] != null ? obj[0].toString() : "NA");
				aadharVo.setBalance(obj[26] != null ? obj[26].toString() : "NA");
				aadharVo.setDistrictno(obj[11] != null ? obj[11].toString() : "NA");
				aadharVo.setCustid(obj[9] != null ? obj[9].toString() : "NA");
				aadharVo.setCusttypelov(obj[7] != null ? obj[7].toString() : "NA");
				aadharVo.setMobileNo(obj[23] != null ? obj[23].toString() : "NA");
				//aadharVo.setEmail(obj[8] != null ? obj[8].toString() : "NA");
				aadharVo.setAddress((addr != null) ? addr.toString() : "NA");
				aadharVo.setLandline(obj[12] != null ? obj[12].toString() : "NA");
				aadharVo.setTenantCode(obj[15] != null ? obj[15].toString() : "NA");
				aadharVo.setProdCafNo(obj[25] != null ? obj[25].toString() : "NA");
				aadhardtsl.add(aadharVo);
			}
		   aadharDTO.setAadharDtlsVO(aadhardtsl);
		   logger.info("CustomerServiceImpl :: getCustomersDetails() :: Start");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(" CustomerServiceImpl :: getCustomersDetails() " + e);
		}
		return aadharDTO;
	}
	
	@Override
	@Transactional
	public List<Object[]> custCafDtls(Long aadharNo) {
		List<Object[]> customerList = new ArrayList<>();
		try {
			logger.info("CustomerServiceImpl :: custCafDtls() :: Start");
		   customerList = custCareDao.custCafDtls(aadharNo);
		   logger.info("CustomerServiceImpl :: custCafDtls() :: Start");
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(" CustomerServiceImpl :: custCafDtls() " + e);
		}
		return customerList;
	}
	

}
