package com.arbiva.apfgc.broadcast.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.broadcast.model.IptvChannelRateCard;
import com.arbiva.apfgc.broadcast.service.BroadcastService;


@RestController
public class BroadcastRestController {
	
	@Autowired
	BroadcastService broadcastService;
	
	
	
	private static final Logger logger = Logger.getLogger(BroadcastRestController.class);
	
	@RequestMapping(value = "/viewMonthlyInvoice", method = RequestMethod.GET)
	public List<Object[]> viewMonthlyInvoicePage() {
		
		List<Object[]> monthlyInvoiceList = new ArrayList<Object[]>();
		
		try {
			logger.info("BroadcastRestController :: viewMontylyInvoicePage  :: START");
			monthlyInvoiceList = broadcastService.getMonthlyInvoiceDetails();
			logger.info("BroadcastRestController :: viewMontylyInvoicePage  :: END");
		} catch (Exception e) {
			logger.error("BroadcastRestController :: viewMontylyInvoicePage " +e);
			e.printStackTrace();
		}finally {
			
		}
		
		return monthlyInvoiceList;
	}
	
	@RequestMapping(value = "/viewRateCard", method = RequestMethod.GET)
	public List<IptvChannelRateCard> viewBroadcastersPage() {
		
		List<IptvChannelRateCard> broadcasterCodeList = new ArrayList<>();
		
		try {
			logger.info("BroadcastRestController :: viewBroadcastersPage  :: START");
			broadcasterCodeList = broadcastService.getBroadcasterCode();
			logger.info("BroadcastRestController :: viewBroadcastersPage  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastRestController :: viewBroadcastersPage" +e);
			e.printStackTrace();
			
		} finally {
			
		}
		
		return broadcasterCodeList;
	}
	
	@RequestMapping(value = "/viewBroadcasterPriceDetails", method = RequestMethod.GET)
	public List<Object[]> viewRateCardPage(@RequestParam(value = "broadcasterCode", required = false) String broadcasterCode) {
		
		List<Object[]> broadcasterCodeList = new ArrayList<Object[]>();
		
		try {
			logger.info("BroadcastRestController :: viewRateCardPage  :: START");
			broadcasterCodeList = broadcastService.getBroadcasterPriceDetails(broadcasterCode);
			logger.info("BroadcastRestController :: viewRateCardPage  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastRestController :: viewRateCardPage" +e);
			e.printStackTrace();
			
		} finally {
			
		}
		
		return broadcasterCodeList;
	}
	
	@RequestMapping(value = "/viewChannelDetails", method = RequestMethod.GET)
	public List<Object[]> viewChannelListPage(@RequestParam(value = "priceGroupCode", required = false) String priceGroupCode) {
		
		List<Object[]> channelList = new ArrayList<Object[]>();
		try {
			logger.info("BroadcastRestController :: viewChannelListPage  :: START");
			channelList = broadcastService.getChannelDetails(priceGroupCode);
			logger.info("BroadcastRestController :: viewChannelListPage  :: END");
		} catch (Exception e) {
			logger.error("The Exception is BroadcastRestController :: viewChannelListPage" +e);
			e.printStackTrace();
		} finally {
		}
		return channelList;
	}
	
	@RequestMapping(value = "/viewInvoiceDetails", method = RequestMethod.GET)
	public List<Object[]> viewInvoiceDetailsPage(@RequestParam(value = "invNo", required = false) Long invNo) {
		
		List<Object[]> invoiceList = new ArrayList<Object[]>();
		
		try {
			logger.info("BroadcastRestController :: viewInvoiceDetailsPage  :: START");
			invoiceList = broadcastService.getInvoiceDetails(invNo);
			logger.info("BroadcastRestController :: viewInvoiceDetailsPage  :: END");
		} catch (Exception e) {
			
			logger.error("The Exception is BroadcastRestController :: viewInvoiceDetailsPage" +e);
			e.printStackTrace();
			
		} finally {
			
		}
		return invoiceList;
	}
	
	@RequestMapping(value = "/invoiceDatePopulation", method = RequestMethod.GET)
	public String invoiceDatePopulation() {
		String status = null;
		
		try {
			logger.info("BroadcastRestController :: invoiceDatePopulation  :: START");
			broadcastService.invoiceDatePopulation();
			status = "Success";
			logger.info("BroadcastRestController :: invoiceDatePopulation  :: END");
		} catch (Exception e) {
			
			logger.error("The Exception is BroadcastRestController :: invoiceDatePopulation" +e);
			e.printStackTrace();
			status = "Failure";
		} finally {
			
		}
		
		return status;
	}


}
