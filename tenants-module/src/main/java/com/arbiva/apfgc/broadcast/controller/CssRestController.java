package com.arbiva.apfgc.broadcast.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.broadcast.model.AadharHelperDTO;
import com.arbiva.apfgc.broadcast.service.CustCareService;



@RestController
public class CssRestController {
	
private static final Logger logger = Logger.getLogger(CssRestController.class);
	
	@Autowired
	CustCareService custCareSrvc;
	
	@RequestMapping(value = "/customerDetails", method = RequestMethod.GET)
	public AadharHelperDTO getCustomerDetails(@RequestParam(value = "custId", required = false) Long custId) {
		
		AadharHelperDTO aadharDTO = new AadharHelperDTO();
		
		try {
			logger.info("OnlineSelfCareRestController :: getCustomerDetails() :: START");
			
			aadharDTO = custCareSrvc.getCustomersDetails(custId);
			logger.info("OnlineSelfCareRestController :: getCustomerDetails() :: END");
			
		}catch(Exception e) {
			logger.error("CcRestController :: getCustomerDetails()" + e);
			e.printStackTrace();
		}
		
		return aadharDTO;
	}
	
	@RequestMapping(value = "/custAdharDtls", method = RequestMethod.GET)
	public AadharHelperDTO getCustAdharDtls(@RequestParam(value = "aadharNo", required = false) Long aadharNo) {
		
		AadharHelperDTO aadharDTO = new AadharHelperDTO();
		try {
			logger.info("OnlineSelfCareRestController :: getCustomerDetails() :: START");
			aadharDTO = custCareSrvc.getCustAdharDetails(aadharNo);
			logger.info("OnlineSelfCareRestController :: getCustomerDetails() :: END");
		}catch(Exception e) {
			logger.error("CcRestController :: getCustomerDetails()" + e);
			e.printStackTrace();
		}
		
		return aadharDTO;
	}
	
	@RequestMapping(value = "/custCafDtls", method = RequestMethod.GET)
	public List<Object[]> custCafDtls(@RequestParam(value = "aadharNo", required = false) Long aadharNo) {
		
		List<Object[]> customerList = new ArrayList<>();
		
		try {
			logger.info("OnlineSelfCareRestController :: custCafDtls() :: START");
			
			customerList = custCareSrvc.custCafDtls(aadharNo);
			logger.info("OnlineSelfCareRestController :: custCafDtls() :: END");
			
		}catch(Exception e) {
			logger.error("CcRestController :: custCafDtls()" + e);
			e.printStackTrace();
		}
		
		return customerList;
	}
}


