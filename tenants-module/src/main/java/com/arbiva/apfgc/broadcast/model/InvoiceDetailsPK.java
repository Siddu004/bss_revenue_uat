package com.arbiva.apfgc.broadcast.model;

import java.io.Serializable;

public class InvoiceDetailsPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long invoiceNumber;
	
	private String priceGroupCode;

	public Long getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Long invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPriceGroupCode() {
		return priceGroupCode;
	}

	public void setPriceGroupCode(String priceGroupCode) {
		this.priceGroupCode = priceGroupCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((priceGroupCode == null) ? 0 : priceGroupCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceDetailsPK other = (InvoiceDetailsPK) obj;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (priceGroupCode == null) {
			if (other.priceGroupCode != null)
				return false;
		} else if (!priceGroupCode.equals(other.priceGroupCode))
			return false;
		return true;
	}
	
	
	

}
