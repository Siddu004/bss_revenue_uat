package com.arbiva.apfgc.tenant.pg.businessserviceimpl;

import java.net.URI;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.pg.businessservice.BilldeskGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.businessservice.DigitSecureGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.businessservice.PaymentGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;
import com.arbiva.apfgc.tenant.pg.utils.GatewayTypes;
import com.arbiva.apfgc.tenant.pg.utils.PgHelper;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Component("paymentGatewayBusinessServiceImpl")
public class PaymentGatewayBusinessServiceImpl extends BaseBusinessServiceImpl implements PaymentGatewayBusinessService {

	private static final Logger LOGGER = Logger.getLogger(PaymentGatewayBusinessServiceImpl.class);
	
	@Autowired
	private BilldeskGatewayBusinessService billdeskGatewayBusinessService;
	
	@Autowired
	private DigitSecureGatewayBusinessService digitSecureGatewayBusinessService;
	
	@Transactional
	@Override
	public String processPaymentRequest(RequestDTO requestDTO) {
		String url= null;
		validateBasicPaymentRequest(requestDTO);
		if(StringUtils.equalsIgnoreCase(GatewayTypes.BILLDESK.name(), requestDTO.getGatewayType())) {
			url = billdeskGatewayBusinessService.processBilldeskPaymentRequest(requestDTO);
		} else {
			url = digitSecureGatewayBusinessService.processDigitSignaturePaymentRequest(requestDTO);
		} 
		//url = digitSecureGatewayBusinessService.processDigitSignaturePaymentRequest(requestDTO);
		return url;
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public String processBilldeskPaymentResponse(String response, String responseBy) {
		LOGGER.info("PaymentGatewayBusinessServiceImpl :: processBilldeskPaymentResponse() :: Start");
		return billdeskGatewayBusinessService.processBilldeskPaymentResponse(response, responseBy);
	}
	
	@Transactional
	@Override
	public String processDigitSignaturePaymentResponse(String response, String responseBy) {
		LOGGER.info("PaymentGatewayBusinessServiceImpl :: processDigitSignaturePaymentResponse() :: Start");
		return digitSecureGatewayBusinessService.processDigitSecurePaymentResponse(response, responseBy);
	}
	
	/**
	 * 
	 * @param dto
	 */
	private void validateBasicPaymentRequest(RequestDTO dto) {
		LOGGER.info("PaymentGatewayBusinessServiceImpl :: validateBasicPaymentRequest() :: Start");
		if (dto == null) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Payment Request details should not be null"));
		} 
		if(StringUtils.isBlank(dto.getGatewayType())) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Gateway type should not be null"));
		}
		if(!EnumUtils.isValidEnum(GatewayTypes.class, dto.getGatewayType())) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Gateway type should be either Billdesk or DigitSignature"));
		}
	}
	
	public List<Object[]> getPaymentRequestList() {
		LOGGER.info("PaymentGatewayBusinessServiceImpl :: getPaymentRequestList() :: Start");
		return billdeskGatewayBusinessService.getPaymentRequestList();
	}

}
