package com.arbiva.apfgc.tenant.pg.businessserviceimpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.ApsflHelper;
import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tenant.dao.CcDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.model.MerchantDetailsBO;
import com.arbiva.apfgc.tenant.pg.businessservice.DigitSecureGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.dao.PaymentGatewayDAO;
import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;
import com.arbiva.apfgc.tenant.pg.model.PaymentResponse;
import com.arbiva.apfgc.tenant.pg.utils.DigitSignatureGatewayInfo;
import com.arbiva.apfgc.tenant.pg.utils.DigitalSecureSignatureUtils;
import com.arbiva.apfgc.tenant.pg.utils.PaymentAuthStatus;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;
import com.arbiva.apfgc.tenant.pg.utils.RestTemplateUtils;
import com.arbiva.apfgc.tenant.service.CcService;
import com.arbiva.apfgc.tenant.service.CpeService;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Component("digitSecureGatewayBusinessServiceImpl")
public class DigitSecureGatewayBusinessServiceImpl extends BaseBusinessServiceImpl
		implements DigitSecureGatewayBusinessService {
	
	@Autowired
	IpAddressValues ipAddressValues;
	
	@Autowired
	CcDao ccDao;
	
	RestTemplate restTemplate = new RestTemplate();

	private static final Logger LOGGER = Logger.getLogger(DigitSecureGatewayBusinessServiceImpl.class);

	// combination of MerchantId, MerchantPassword, MerchantTrackId(requestId)
	// and amount
	private static final String DIGIT_SIGN_REQUEST_CONTENT_TO_SIGN = "%s%s%s%s";
	private static final String DIGIT_SIGN_REQUEST_CONTENT_TO_SIGN_RESPONSE = "%s%s%s";
	private static final String TRACK_ID_REGEX = "<TrackID>(.+?)</TrackID>";
	private static final String SIGNATURE_REGEX = "<Signature>(.+?)</Signature>";
	private static final String AMOUNT_REGEX = "<Amount>(.+?)</Amount>";
	private static final String RESPONSE_STATUS_REGEX = "<Result>(.+?)</Result>";
	private static final String CUSTOMER_ID = "<UDF1>(.+?)</UDF1>";
	private static final String PAYMENT_TYPE = "<UDF2>(.+?)</UDF2>";
	private static final String DELIVERY_ID = "<UDF3>(.+?)</UDF3>";
	private static final String PAYMENT_STATUS = "<Result>(.+?)</Result>";
	private static final String TRANSACTION_ID = "<TransactionID>(.+?)</TransactionID>";
	private static final String ERROR_MESSAGE = "<Message>(.+?)</Message>";

	@Autowired
	private PaymentGatewayDAO paymentGatewayDAO;

	@Autowired
	private DigitSignatureGatewayInfo digitSignatureGatewayInfo;
	
	@Autowired
	private StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CcService ccService;
	
	@Autowired
	CpeService cpeService;

	@Transactional
	@Override
	public String processDigitSignaturePaymentRequest(RequestDTO requestDTO) {
		PaymentRequest request = null;
		String requestBody = null;
		String signature = null;
		String uri = null;
		String custType = "";
	//	PgHelper pgHelper = new PgHelper();
		try {
			LOGGER.info("DigitSecureGatewayBusinessServiceImpl :: processDigitSignaturePaymentRequest() :: Start");
			String paymentType=requestDTO.getPaymentType();
			LOGGER.info("DigitSecureGatewayBusinessServiceImpl :: processDigitSignaturePaymentRequest() :: paymentType : " + paymentType);
			requestDTO.setAmount(requestDTO.getAmount());
			requestDTO.setRequestId(storedProcedureDAO.executeStoredProcedure("PGREQUESTID"));
			validateDigitSignaturePaymentRequest(requestDTO);
			request = paymentGatewayDAO.savePaymentRequest(new PaymentRequest(requestDTO));
			requestDTO.setRequestId(request.getRequestId());
			LOGGER.info("Signature Rquest Id ==========>"+request.getRequestId()+" Signature Amount ========>"+request.getAmount());
			//Get merchant id and password from database
			MerchantDetailsBO merchantDetails = paymentGatewayDAO.getMerchantInfo(requestDTO.getCusttypelov());
			String merchantID=merchantDetails.getMerchantid();
			String password=merchantDetails.getMerchantpwd();
			//signature = DigitalSecureSignatureUtils.sign(getDigitSignatureContent(request.getRequestId(), request.getAmount()));
			signature = DigitalSecureSignatureUtils.sign(getDigitSignatureContent(request.getRequestId(), request.getAmount(), merchantID, password ));
			LOGGER.info(" Request Sign ====> "+getDigitSignatureContent(request.getRequestId(), request.getAmount(),merchantID,password));
			LOGGER.info(" Request Signature ====> "+signature);
			if(paymentType.equalsIgnoreCase("onlineSelfCare")){
				requestBody = consolidatedDigitalSignatureRequestMesssageCss(requestDTO, signature, merchantID);
			} else if(paymentType.equalsIgnoreCase("onlineSelfCareBeforeLogin")) {
				requestBody = consolidatedDigitalSignatureRequestMesssageCssBeforeLogin(requestDTO, signature);
			} else {
				requestBody = consolidatedDigitalSignatureRequestMesssage(requestDTO, signature);
			}
			request.setRequest(requestBody.getBytes());
			paymentGatewayDAO.savePaymentRequest(request);
			uri = RestTemplateUtils.post(digitSignatureGatewayInfo.getRequestUrl(), requestBody,requestDTO.getGatewayType());
			LOGGER.info("DigitSecureGatewayBusinessServiceImpl :: processDigitSignaturePaymentRequest() :: End");
		} catch (Exception e) {
			LOGGER.error("DigitSecureGatewayBusinessServiceImpl :: processDigitSignaturePaymentRequest()"+e);
			handleExceptions(e, "Error occured while processing Payment Request with DigitSecure Gateway");
		} finally {
			request = null;
			signature = null;
		}
		/*LOGGER.info("requestBody ===> For DigitSecure ===>"+requestBody);
		LOGGER.info(" DigitSecure URL ===>   "+digitSignatureGatewayInfo.getRequestUrl());
		pgHelper.setRequestBody(requestBody);
		pgHelper.setUrl(digitSignatureGatewayInfo.getRequestUrl());*/
		return uri;
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public String processDigitSecurePaymentResponse(String response, String responseBy) {
		String amount = getHiddenAmount(response);
		String responseSignature = null;
		String calulatedSignature = null;
		String responseStatus = null;
		PaymentRequest request = null;
		PaymentResponse paymentResponse = null;
		String paymentType = null;
		String responseValue = "";
		String Id = this.getCustomerId(response);
		String[] Nos = Id.split(",");
		String mobileNo=ccDao.findMobileNo(Nos[0]);
		String message="";
		
		try {
			LOGGER.info("DigitSecureGatewayBusinessServiceImpl :: processDigitSecurePaymentResponse() :: Start");
			request = getPaymentRequestFromResponse(response);
			if (request.getStatus() == 0) {
				LOGGER.info(" Stauts Code ====> "+this.getHiddenStatus(response));
				if(StringUtils.equalsIgnoreCase(this.getHiddenStatus(response),PaymentAuthStatus.DIGIT_SECURE_SUCCESS.getCode())){
					LOGGER.info(" Transactio Id ====> "+getHiddenTranId(response));
				amount = getHiddenAmount(response);
				responseSignature = getHiddenSignature(response);
				calulatedSignature = DigitalSecureSignatureUtils.sign(getDigitSignatureResponseContent(getHiddenTranId(response), new BigDecimal(amount)));
				LOGGER.info(" calulatedSignature  ====> "+calulatedSignature);
				LOGGER.info(" responseSignature  ====> "+responseSignature);
				if (StringUtils.equalsIgnoreCase(responseSignature, calulatedSignature)) {
					responseStatus = getHiddenResponseStatus(response);
					paymentResponse = new PaymentResponse();
					paymentResponse.setId(storedProcedureDAO.executeStoredProcedure("PGRESEPONSEID"));
					paymentResponse.setResponseBy(responseBy);
					paymentResponse.setResponse(response.getBytes());
					paymentResponse.setRequestId(Long.valueOf(request.getRequestId()));
					paymentResponse.setResponseCode(responseStatus);
					paymentGatewayDAO.savePaymentResponse(paymentResponse);
						
						request.setStatus(1);
						paymentGatewayDAO.savePaymentRequest(request);
						paymentType = this.getPaymentType(response);
						LOGGER.info(" Payment Gateway Type == "+paymentType);
						
						if (paymentType.equalsIgnoreCase("cpe")) {
							LOGGER.info(" Creating record in tenantcpepmnts table");
							responseValue = cpeService.processPyament(this.getHiddenAmount(response),this.getDeliveryId(response),request.getRequestId());
						} else if  (paymentType.equalsIgnoreCase("onlineSelfCare") || paymentType.equalsIgnoreCase("onlineSelfCareBeforeLogin")){
							responseValue = ccService.processPyament(this.getHiddenAmount(response),this.getCustomerId(response),this.getTransactionId(response),this.getDeliveryId(response));
						}else{
							LOGGER.error("Error occured while processing Payment Response: ");
						}
					
					
				} else {
					throw new PaymentGatewayException(
							new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.PGRES001,
									"Response checksum is not macthed with calculated checksum of a request id: "
											+ request.getRequestId()));

				}
				} else if(!(StringUtils.equalsIgnoreCase(this.getHiddenStatus(response),PaymentAuthStatus.DIGIT_SECURE_SUCCESS.getCode()) && (StringUtils.equalsIgnoreCase(this.getErrorMessage(response),PaymentAuthStatus.DIGIT_SECURE_ERRORMESSGE.getDesc())))){
					
					message="Trasaction cancelled by the customer for the Account No: "+Nos[1];
					LOGGER.info(message);
				}
				else {
					 String originalAmt=amount.replaceFirst("^0+(?!$)", "");
					 message = "Your Payment of Rs."+originalAmt+" is  Failed for the Account No: "+Nos[1];
					LOGGER.info(message);
				}
			}
			
			HttpEntity<String> httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "sendSMS?mobileNo="+mobileNo+"&msg="+message;
			ResponseEntity<String> response1 = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			response1.getBody();
			LOGGER.info("Payment Failed message send to the customer");
			
		} catch (Exception e) {
			handleExceptions(e, "Error occured while processing Payment Response");
		} finally {
			request = null;
			responseStatus = null;
			paymentResponse = null;
		}
		
		return responseValue;
	}

	private String getHiddenTranId(String response) {
		return getTagValue(TRANSACTION_ID, response);
	}

	private String getDigitSignatureResponseContent(String requestId, BigDecimal amount) {
		return String.format(DIGIT_SIGN_REQUEST_CONTENT_TO_SIGN_RESPONSE, digitSignatureGatewayInfo.getMerchantId(), requestId, amount);
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private PaymentRequest getPaymentRequestFromResponse(String response) {
		if (StringUtils.isBlank(response)) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Payment Response should not be null"));
		}
		String requestId = getHiddenRequestId(response);
		if (StringUtils.isBlank(requestId)) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Request Id is not available as part of response body"));
		}
		PaymentRequest request = paymentGatewayDAO.findPaymentRequest(Long.valueOf(requestId));
		requestId = null;
		if (request == null) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.PGREQ001,
					"Payment Request is not exists for request id: " + requestId));
		}

		return request;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenRequestId(String response) {
		return getTagValue(TRACK_ID_REGEX, response);
	}
	
	private String getHiddenStatus(String response) {
		return getTagValue(PAYMENT_STATUS, response);
	}
	
	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getPaymentType(String response) {
		return getTagValue(PAYMENT_TYPE, response);
	}
	
	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getCustomerId(String response) {
		return getTagValue(CUSTOMER_ID, response);
	}
	
	private String getTransactionId(String response) {
		return getTagValue(TRANSACTION_ID, response);
	}
	
	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getDeliveryId(String response) {
		return getTagValue(DELIVERY_ID, response);
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenAmount(String response) {
		return getTagValue(AMOUNT_REGEX, response);
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenSignature(String response) {
		return getTagValue(SIGNATURE_REGEX, response);
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenResponseStatus(String response) {
		return getTagValue(RESPONSE_STATUS_REGEX, response);
	}

	
	/**
	 * 
	 * @param dto
	 * @param signature
	 * @return
	 */
	private String consolidatedDigitalSignatureRequestMesssage(RequestDTO dto, String signature) {
		return String.format(digitSignatureGatewayInfo.getRequestPayload(), dto.getRequestId(), dto.getAmount(),
				dto.getEmailId(), dto.getPhone(), signature, dto.getCustomerId(), dto.getPaymentType(),
				dto.getDeliveryId());
	}
	
	private String consolidatedDigitalSignatureRequestMesssageCss(RequestDTO dto, String signature) {
		return String.format(digitSignatureGatewayInfo.getRequestPayloadCss(), dto.getRequestId(), dto.getAmount(),
				dto.getEmailId(), dto.getPhone(), signature, dto.getCustomerId(), dto.getPaymentType(),
				dto.getDeliveryId());
	}
	
	private String consolidatedDigitalSignatureRequestMesssageCss(RequestDTO dto, String signature, String merchantID) {
		return String.format(digitSignatureGatewayInfo.getRequestPayloadCss(), merchantID, dto.getRequestId(), dto.getAmount(),
				dto.getEmailId(), dto.getPhone(), signature, dto.getCustomerId(), dto.getPaymentType(),
				dto.getDeliveryId());
	}

	private String consolidatedDigitalSignatureRequestMesssageCssBeforeLogin(RequestDTO dto, String signature) {
		return String.format(digitSignatureGatewayInfo.getRequestPayloadCssBeforeLogin(), dto.getRequestId(), dto.getAmount(),
				dto.getEmailId(), dto.getPhone(), signature, dto.getCustomerId(), dto.getPaymentType(),
				dto.getDeliveryId());
	}

	/**
	 * 
	 * @param requestId
	 * @param amount
	 * @return
	 */
	private String getDigitSignatureContent(long requestId, BigDecimal amount) {
		return String.format(DIGIT_SIGN_REQUEST_CONTENT_TO_SIGN, digitSignatureGatewayInfo.getMerchantId(),
				digitSignatureGatewayInfo.getPassword(), requestId, amount);
	}
	
	private String getDigitSignatureContent(long requestId, BigDecimal amount,String merchantID,String password) {
		return String.format(DIGIT_SIGN_REQUEST_CONTENT_TO_SIGN, merchantID, password, requestId, amount);
	}

	/**
	 * 
	 * @param dto
	 */
	private void validateDigitSignaturePaymentRequest(RequestDTO dto) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isBlank(dto.getCustomerId())) {
			builder.append("Customer Id should not be null").append(System.lineSeparator());
		}
		if (dto.getAmount() != null && dto.getAmount().compareTo(BigDecimal.ZERO) == 0) {
			builder.append("Payment Amount should not be null").append(System.lineSeparator());
		}
		if (StringUtils.isBlank(dto.getEmailId())) {
			builder.append("Customer EmailId should not be null").append(System.lineSeparator());
		}
		if (StringUtils.isBlank(dto.getPhone())) {
			builder.append("Customer Phone Number should not be null").append(System.lineSeparator());
		}
		if (builder.length() > 0) {
			throw new PaymentGatewayException(
					new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002, builder.toString()));
		}
	}

	/**
	 * 
	 * @param regex
	 * @param content
	 * @return
	 */
	private static String getTagValue(String regex, String content) {
		Matcher matcher = Pattern.compile(regex).matcher(content);
		while (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}
	
	private String getErrorMessage(String response) {
		return getTagValue(ERROR_MESSAGE, response);
	}
}
