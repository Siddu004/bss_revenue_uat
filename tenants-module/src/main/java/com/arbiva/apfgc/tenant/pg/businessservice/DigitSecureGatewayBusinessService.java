package com.arbiva.apfgc.tenant.pg.businessservice;

import java.net.URI;

import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.utils.PgHelper;

/**
 *
 * 
 * @author srinivasa
 *
 */
@Component("digitSecureGatewayBusinessService")
public interface DigitSecureGatewayBusinessService {

	public String processDigitSignaturePaymentRequest(RequestDTO requestDTO);

	public String processDigitSecurePaymentResponse(String response, String responseBy);

}
