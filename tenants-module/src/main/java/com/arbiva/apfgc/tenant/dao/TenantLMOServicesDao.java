/**
 * 
 *//*
package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantLMOServices;
import com.arbiva.apfgc.tenant.model.TenantLMOServicesPK;

*//**
 * @author Arbiva
 *
 *//*
public interface TenantLMOServicesDao extends JpaSpecificationExecutor<TenantLMOServices>, JpaRepository<TenantLMOServices, TenantLMOServicesPK> {

	@Query(value="SELECT * FROM lmosrvcs where mspcode = ?1 ",nativeQuery=true)
	public abstract TenantLMOServices findByMspCode(String mspCode);
	
	@Query(value="SELECT * FROM lmosrvcs where lmocode = ?1 ",nativeQuery=true)
	public abstract TenantLMOServices findByLmoCode(String lmoCode);
}
*/