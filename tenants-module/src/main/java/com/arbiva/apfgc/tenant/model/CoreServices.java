/**
 * 
 */
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name ="coresrvcs")
public class CoreServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "srvccode")
	private String srvcCode;
	
	@Column(name = "srvcname")
	private String srvcName;
	
	@Column(name = "srvccodeprov")
	private String srvcCodeprov;
	
	@Column(name = "ispnamelov")
	private String ispNamelov;
	
	@Column(name = "provtrgttypelov")
	private String provtrgtTypelov;
	
	@Column(name = "provtrgtvalue")
	private String provtrgtValue;
	
	@Column(name = "glcode")
	private String glCode;
	
	@Column(name = "multsrvcsallowed")
	private Character multsrvcsAllowed;
	
	@Column(name = "status")
	Integer  status;
	
	@Column(name = "createdon")
	Calendar createdDate;
	
	@Column(name = "createdby")
	String createdBy;
	
	@Column(name = "createdipaddr")
	String cratedIPAddress;
	
	@Column(name = "modifiedon")
	Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	String modifiedBy;
	
	@Column(name = "modifiedipaddr")
	String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private Calendar deactivatedDate;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpAddress;
	

	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSrvcCode() {
		return srvcCode;
	}
	public void setSrvcCode(String srvcCode) {
		this.srvcCode = srvcCode;
	}
	public String getSrvcName() {
		return srvcName;
	}
	public void setSrvcName(String srvcName) {
		this.srvcName = srvcName;
	}
	public String getSrvcCodeprov() {
		return srvcCodeprov;
	}
	public void setSrvcCodeprov(String srvcCodeprov) {
		this.srvcCodeprov = srvcCodeprov;
	}
	public String getIspNamelov() {
		return ispNamelov;
	}
	public void setIspNamelov(String ispNamelov) {
		this.ispNamelov = ispNamelov;
	}
	public String getProvtrgtTypelov() {
		return provtrgtTypelov;
	}
	public void setProvtrgtTypelov(String provtrgtTypelov) {
		this.provtrgtTypelov = provtrgtTypelov;
	}
	public String getProvtrgtValue() {
		return provtrgtValue;
	}
	public void setProvtrgtValue(String provtrgtValue) {
		this.provtrgtValue = provtrgtValue;
	}
	public String getGlCode() {
		return glCode;
	}
	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	public Character getMultsrvcsAllowed() {
		return multsrvcsAllowed;
	}
	public void setMultsrvcsAllowed(Character multsrvcsAllowed) {
		this.multsrvcsAllowed = multsrvcsAllowed;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Calendar getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCratedIPAddress() {
		return cratedIPAddress;
	}
	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}
	public Calendar getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Calendar getDeactivatedDate() {
		return deactivatedDate;
	}
	public void setDeactivatedDate(Calendar deactivatedDate) {
		this.deactivatedDate = deactivatedDate;
	}
	public String getDeactivatedBy() {
		return deactivatedBy;
	}
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}
	public String getDeactivatedIpAddress() {
		return deactivatedIpAddress;
	}
	public void setDeactivatedIpAddress(String deactivatedIpAddress) {
		this.deactivatedIpAddress = deactivatedIpAddress;
	}
	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}
	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}
	
	

}
