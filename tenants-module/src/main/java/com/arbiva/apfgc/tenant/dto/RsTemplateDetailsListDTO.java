package com.arbiva.apfgc.tenant.dto;

import java.io.Serializable;

import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;

public class RsTemplateDetailsListDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public RsTemplateDetailsListDTO()
	{

	}
	public RsTemplateDetailsListDTO(RevenueSharingTemplateDetails rsTemp)
	{
		this.tenantslno = String.valueOf(rsTemp.getTenantslno());
		this.tenanttype = rsTemp.getTenanttype();
		this.revshareperc = String.valueOf(rsTemp.getRevshareperc());
	}
	
	private String tenantslno;
	private String tenanttype;
	private String revshareperc;
	
	public String getTenantslno() {
		return tenantslno;
	}
	public void setTenantslno(String tenantslno) {
		this.tenantslno = tenantslno;
	}
	public String getTenanttype() {
		return tenanttype;
	}
	public void setTenanttype(String tenanttype) {
		this.tenanttype = tenanttype;
	}
	public String getRevshareperc() {
		return revshareperc;
	}
	public void setRevshareperc(String revshareperc) {
		this.revshareperc = revshareperc;
	}
	
	
	

}
