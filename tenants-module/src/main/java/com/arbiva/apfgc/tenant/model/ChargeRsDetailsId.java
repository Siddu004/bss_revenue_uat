package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

public class ChargeRsDetailsId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String rstmplCode;
	
	private String chargeCode;

	public String getRstmplCode() {
		return rstmplCode;
	}

	public void setRstmplCode(String rstmplCode) {
		this.rstmplCode = rstmplCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chargeCode == null) ? 0 : chargeCode.hashCode());
		result = prime * result + ((rstmplCode == null) ? 0 : rstmplCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChargeRsDetailsId other = (ChargeRsDetailsId) obj;
		if (chargeCode == null) {
			if (other.chargeCode != null)
				return false;
		} else if (!chargeCode.equals(other.chargeCode))
			return false;
		if (rstmplCode == null) {
			if (other.rstmplCode != null)
				return false;
		} else if (!rstmplCode.equals(other.rstmplCode))
			return false;
		return true;
	}
	
	

	
}
