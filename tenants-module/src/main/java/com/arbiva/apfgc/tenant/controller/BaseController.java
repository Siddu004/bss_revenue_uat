package com.arbiva.apfgc.tenant.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

import com.arbiva.apfgc.tanent.util.TenantErrorCodes;
import com.arbiva.apfgc.tanent.util.TmsHelper;
import com.arbiva.apfgc.tenant.exception.TenantException;
import com.arbiva.apfgc.tenant.vo.TenantJsonResponseVo;


@Controller
public class BaseController {
	private static final Logger logger = Logger.getLogger(BaseController.class);
public TenantJsonResponseVo handleExceptions(Exception exc, TenantJsonResponseVo tenantJsonResponseVo) {
		try {
			logger.info("BaseController :: handleExceptions :: START");
			if (exc instanceof TenantException) {
				
				tenantJsonResponseVo.setErrorCode(((TenantException) exc).getErrorCode());
				tenantJsonResponseVo.setErrorMessage(exc.getMessage());
				tenantJsonResponseVo.setDesc(((TenantException) exc).getErrorDescription());
				tenantJsonResponseVo.setOutPut("failure");
				
			} else {
				
				tenantJsonResponseVo.setOutPut("failure");
				tenantJsonResponseVo.setErrorCode(((TenantException) exc).getErrorCode());
				tenantJsonResponseVo.setErrorMessage(exc.getMessage());
				tenantJsonResponseVo.setDesc(((TenantException) exc).getErrorDescription());
				logger.info("BaseController :: handleExceptions :: END");
		}
		}catch(Exception e) {
			logger.error("BaseController :: handleExceptions "+e);
		} finally {
			
		}
		return tenantJsonResponseVo;
	}
	
	public ResponseBuilder handleRestExceptions(Exception exc,ResponseBuilder builder) {
		try {
			logger.info("BaseController :: handleRestExceptions :: START");
			if (exc instanceof TenantException) {
			builder = Response.status(Status.BAD_REQUEST).entity((TenantException) exc);
			} else {
				builder = Response.status(Status.INTERNAL_SERVER_ERROR).entity(
						new TenantException(TenantErrorCodes.TENT001, exc.getMessage()));
			}
			logger.info("BaseController :: handleRestExceptions :: END");
		} catch(Exception e)
		{
			logger.error("BaseController :: handleRestExceptions "+e);
			e.printStackTrace();
		} finally {
			
		}
		return builder;
	}

}
