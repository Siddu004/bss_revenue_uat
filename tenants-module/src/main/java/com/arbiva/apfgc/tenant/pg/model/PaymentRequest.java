package com.arbiva.apfgc.tenant.pg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Entity
@Table(name = "pgrequest")
public class PaymentRequest implements Serializable {

	public PaymentRequest() {
	}

	public PaymentRequest(RequestDTO dto) {
		setCustomerId(dto.getCustomerId());
		setAmount(dto.getAmount());
		setGatewayType(dto.getGatewayType());
		setEmail(dto.getEmailId());
		setPhone(dto.getPhone());
		setCreatedBy(dto.getCreatedBy());
		setStatus(0);
		setRequestId(dto.getRequestId());
		setCreatedOn(Calendar.getInstance().getTime());
		setModifiedOn(Calendar.getInstance().getTime());
	}

	private static final long serialVersionUID = 1L;
	private long requestId;
	private String customerId;
	private BigDecimal amount;
	private String gatewayType;
	private String email;
	private String phone;
	private int status;
	private Date createdOn;
	private Date modifiedOn;
	private long createdBy;
	private byte[] request;
	

	/**
	 * @return the requestId
	 */
	@Id
	@Column(name = "reqid")
	public long getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the customerId
	 */
	@Column(name = "custid")
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the amount
	 */
	@Column(name = "amount")
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the gatewayType
	 */
	@Column(name = "gatewaytype")
	public String getGatewayType() {
		return gatewayType;
	}

	/**
	 * @param gatewayType
	 *            the gatewayType to set
	 */
	public void setGatewayType(String gatewayType) {
		this.gatewayType = gatewayType;
	}

	/**
	 * @return the email
	 */
	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	@Column(name = "phonenumber")
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the status
	 */
	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the createdOn
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdon")
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedOn
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedon")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the createdBy
	 */
	@Column(name = "createdby")
	public long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	

	/**
	 * @return the request
	 */
	@Lob
	@Column(name = "request")
	public byte[] getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(byte[] request) {
		this.request = request;
	}

	@Transient
	private boolean isNew() {
		return this.requestId == 0;
	}

	@PrePersist
	@PreUpdate
	void createAndUpdateAtCallback() {

		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);

		setModifiedOn(now.getTime());
		if (isNew()) {
			setCreatedOn(now.getTime());
		}
	}
}