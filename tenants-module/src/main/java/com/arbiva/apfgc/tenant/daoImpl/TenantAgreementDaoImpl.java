/**
 * 
 *//*
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantAgreement;
import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreement;

*//**
 * @author Lakshman
 *
 *//*
@Repository
public class TenantAgreementDaoImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantAgreementDaoImpl.class);

	@PersistenceContext
	private EntityManager em;

	public TenantAgreement findByTenantCode(String tenantCode) {
		TenantAgreement tenantAgreement = new TenantAgreement();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantAgreement.class.getSimpleName())
				.append(" WHERE tenantcode=:tenantCode");
		try {
			LOGGER.info("START::findByTenantCode()");
			TypedQuery<TenantAgreement> query = em.createQuery(builder.toString(), TenantAgreement.class);
			query.setParameter("tenantCode", tenantCode);
			tenantAgreement = query.getSingleResult();
			LOGGER.info("END::findByTenantCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantCode() " + e);
		}
		return tenantAgreement;
	}

	public List<TenantAgreement> findAllTenantAgreements() {
		List<TenantAgreement> tenantAgreements = new ArrayList<TenantAgreement>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantAgreement.class.getSimpleName());
		try {
			LOGGER.info("START::findAllTenantAgreements()");
			TypedQuery<TenantAgreement> query = em.createQuery(builder.toString(), TenantAgreement.class);
			tenantAgreements = query.getResultList();
			LOGGER.info("END::findAllTenantAgreements()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenantAgreements() " + e);
		}
		return tenantAgreements;
	}

	public List<Tenant> findAllTenantAgreementsforAdmin(String roleName, String createdBy) {
		List<Tenant> tenants = new ArrayList<Tenant>();
		List<String> tenantAgreements = null;

		tenantAgreements = findTenantsIdsforAdmin(createdBy);
		tenantAgreements = findTenantsIdsforAPFAdmin(createdBy);
		StringBuffer str2 = new StringBuffer();
		for (String string : tenantAgreements) {
			str2.append(string);
			str2.append(",");
		}

		if (str2.length() > 0)
			str2.replace(str2.toString().length() - 1, str2.toString().length(), "");
		else
			str2.append("''");
		String str = " where tenantcode in (" + str2.toString() + ")";
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName()).append(str)
				.append(" order by createdon desc");
		try {
			LOGGER.info("START::findAllTenantAgreementsforAdmin()");
			TypedQuery<Tenant> query = em.createQuery(builder.toString(), Tenant.class);
			tenants = query.getResultList();
			LOGGER.info("END::findAllTenantAgreementsforAdmin()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenantAgreementsforAdmin() " + e);
		}
		return tenants;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> findAllTenantsforAdmin(String roleName, String createdBy) {
		List<Tenant> tenants = new ArrayList<Tenant>();
		
		 * List<String> tenantAgreements1 = new ArrayList<String>();
		 * List<String> tenantAgreements2 = new ArrayList<String>();
		 * List<String> tenantAgreements = new ArrayList<String>();
		 * 
		 * tenantAgreements1 = findTenantsIdsforAdmin(createdBy);
		 * tenantAgreements2 = findTenantsIdsforAPFAdmin(createdBy);
		 * tenantAgreements.addAll(tenantAgreements1);
		 * tenantAgreements.addAll(tenantAgreements2);
		 * 
		 * StringBuffer str2 =new StringBuffer(); for (String string :
		 * tenantAgreements) { str2.append(string); str2.append(","); }
		 * if(str2.length()> 0) str2.replace(str2.toString().length()-1,
		 * str2.toString().length(), ""); else str2.append("''"); String str =
		 * " where tenantcode not in ("+str2.toString()+")"; StringBuilder
		 * builder = new StringBuilder(" FROM "
		 * ).append(Tenant.class.getSimpleName()).append(str).append(
		 * " order by createdon desc"); try {
		 * LOGGER.info("START::findAllTenantAgreementsforAdmin()");
		 * TypedQuery<Tenant> query = em.createQuery(builder.toString(),
		 * Tenant.class); tenants = query.getResultList();
		 * LOGGER.info("END::findAllTenantAgreementsforAdmin()"); } catch
		 * (Exception e) { LOGGER.error(
		 * "EXCEPTION::findAllTenantAgreementsforAdmin() " + e); }
		 
		StringBuilder builder = null;
		Query query = null;
		builder = new StringBuilder("select * from tenants where tenantcode "
				+ " not in(select tenantcode from tenantsagr WHERE createdby='" + createdBy + "' and status != 4) "
				+ " order by createdon desc;");
		try {
			query = em.createNativeQuery(builder.toString(), Tenant.class);
			tenants = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::findAllTenantsforAdmin() " + ex);
		} finally {
			builder = null;
			query = null;
		}

		return tenants;
	}

	public List<String> findTenantsIdsforAdmin(String createdBy) {
		List<TenantLmoMspAgreement> tenants = new ArrayList<TenantLmoMspAgreement>();
		List<String> tenantCodes = new ArrayList<String>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLmoMspAgreement.class.getSimpleName())
				.append(" WHERE createdby=:createdBy and status != 4 ");
		try {
			LOGGER.info("START::findTenantsIdsforAdmin()");
			TypedQuery<TenantLmoMspAgreement> query = em.createQuery(builder.toString(), TenantLmoMspAgreement.class);
			query.setParameter("createdBy", createdBy);
			tenants = query.getResultList();
			for (TenantLmoMspAgreement tenantAgreement : tenants) {
				tenantCodes.add("'" + tenantAgreement.getLmoCode() + "'");
			}
			tenants.remove(tenants.size());
			LOGGER.info("END::findTenantsIdsforAdmin()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findTenantsIdsforAdmin() " + e);
		}
		return tenantCodes;
	}

	public List<String> findTenantsIdsforAPFAdmin(String createdBy) {
		List<TenantAgreement> tenants = new ArrayList<TenantAgreement>();
		List<String> tenantCodes = new ArrayList<String>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantAgreement.class.getSimpleName())
				.append(" WHERE createdby=:createdBy and status != 4 ");
		try {
			LOGGER.info("START::findTenantsIdsforAPFAdmin()");
			TypedQuery<TenantAgreement> query = em.createQuery(builder.toString(), TenantAgreement.class);
			query.setParameter("createdBy", createdBy);
			tenants = query.getResultList();
			for (TenantAgreement tenantAgreement : tenants) {
				tenantCodes.add("'" + tenantAgreement.getTenantCode() + "'");
			}
			tenants.remove(tenants.size());
			LOGGER.info("END::findTenantsIdsforAPFAdmin()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findTenantsIdsforAPFAdmin() " + e);
		}
		return tenantCodes;
	}

	public void saveTenantAgreement(TenantAgreement tenantAgreement) {
		try {
			em.merge(tenantAgreement);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::saveTenantAgreement() " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllAgreements(String roleName, String loginID) {

		List<Object[]> agreementsList;
		StringBuilder builder1 = new StringBuilder();
		StringBuilder builder2 = new StringBuilder();
		builder1.append("SELECT a.status ," + " t.tenantcode, " + " t.tenantname, " + " t.tenanttypelov, "
				+ " t.locoff_pocmob1," + " t.tenantid," + " a.createdby," + " 'tenantAgreement',"
				+ " a.tenantcode as tcode," + " a.agrfdate," + " 'dummyValue' " + " FROM tenantsagr a, tenants t "
				+ " WHERE a.createdby IN " + " (SELECT b.loginid FROM users b WHERE b.status=1 " + " AND (b.loginid='"
				+ loginID + "' OR b.rmuserid='" + loginID + "'))" + " And a.tenantcode = t.tenantcode ");

		builder2.append("SELECT a.status ," + " t.tenantcode, " + " t.tenantname, " + " t.tenanttypelov, "
				+ " t.locoff_pocmob1," + " t.tenantid," + " a.createdby," + " 'mspLmoAgreement'," + " a.mspcode,"
				+ " a.agrfdate," + " a.lmocode " + " FROM msplmoagr a, tenants t " + " WHERE a.createdby IN "
				+ " (SELECT b.loginid FROM users b WHERE b.status=1 " + " AND (b.loginid='" + loginID
				+ "' OR b.rmuserid='" + loginID + "'))" + " And a.mspcode = t.tenantcode ");

		Query query1 = em.createNativeQuery(builder1.toString());
		Query query2 = em.createNativeQuery(builder2.toString());
		agreementsList = query1.getResultList();
		agreementsList.addAll(query2.getResultList());
		return agreementsList;
	}

	public TenantAgreement findTenantAgreement(String tenantCode, String fromDate) {
		TenantAgreement tenantAgreement = null;
		// StringBuilder builder = new
		// StringBuilder("FROM").append(TenantAgreement.class.getSimpleName()).append("
		// where tenantcode='"+tenantCode+"' and agrfdate='"+fromDate+"'");
		StringBuilder builder = new StringBuilder(
				"SELECT * FROM tenantsagr where tenantcode='" + tenantCode + "' and agrfdate='" + fromDate + "'");
		try {
			Query query = em.createNativeQuery(builder.toString(), TenantAgreement.class);
			List result = query.getResultList();
			if (!result.isEmpty())
				tenantAgreement = (TenantAgreement) result.get(0);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

		return tenantAgreement;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> findAllTenantsBYCreatedBy(String loginID) {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		StringBuilder builder = new StringBuilder(" SELECT * FROM apfgc.tenants "
				+ " where createdby ='"+loginID+"' or "
				+ " createdby in(select loginid from users where rmuserid = '"+loginID+"') ");
		try {
			Query query = em.createNativeQuery(builder.toString(), Tenant.class);
			tenantsList  = query.getResultList();
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

		return tenantsList;
	}

}
*/