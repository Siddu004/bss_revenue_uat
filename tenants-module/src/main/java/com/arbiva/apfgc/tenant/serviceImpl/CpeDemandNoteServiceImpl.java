package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.CpeDemandNoteDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.dto.CpeDemandNoteDTO;
import com.arbiva.apfgc.tenant.dto.LmoCpeRequestDTO;
import com.arbiva.apfgc.tenant.model.MspCpeDmd;
import com.arbiva.apfgc.tenant.model.MspCpeDmdDtl;
import com.arbiva.apfgc.tenant.model.PortalMspLmos;

@Service
public class CpeDemandNoteServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(CpeDemandNoteServiceImpl.class);
	
	@Autowired
	CpeDemandNoteDaoImpl cpeDemandNoteDaoImpl;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	

	
	public List<Object[]> getAllLmosByTenantCodeMigrData(String tenantCode) {
		return cpeDemandNoteDaoImpl.getAllLmosByTenantCodeMigrData(tenantCode);
	}

	@Transactional
	public Map<String, Long> lmoCpeRequestSavePortal(CpeDemandNoteDTO cpeHelperDTO) {
		MspCpeDmd mspCpeDmd = new MspCpeDmd();
		Map<String, Long> map = new HashMap<String, Long>();
		Long emi0 = 0L;
		Long emi36 = 0L;
		Long emi48 = 0L;
		try{
			mspCpeDmd.setMspCode(cpeHelperDTO.getTenantCode());
			mspCpeDmd.setDmdTime(Calendar.getInstance());
			mspCpeDmd = cpeDemandNoteDaoImpl.saveOrUpdate(mspCpeDmd);
			for(LmoCpeRequestDTO lmpCpeRequest : cpeHelperDTO.getLmoCpeRequest()){
				
				if(lmpCpeRequest.getLmoName() != null && lmpCpeRequest.getLmoName() != ""){
					PortalMspLmos portalMspLmos = new PortalMspLmos();
					if(lmpCpeRequest.getMsoLmoId() == null || lmpCpeRequest.getMsoLmoId().equalsIgnoreCase("")){
						long val = storedProcedureDAO.executeStoredProcedure("ERRORID");
						portalMspLmos.setMso_lmo_id(String.valueOf(val));
						portalMspLmos.setLmo_name(lmpCpeRequest.getLmoName());
						portalMspLmos.setMso_enrollment_number(cpeHelperDTO.getTenantCode());
						portalMspLmos.setDemanddate(Calendar.getInstance());
						cpeDemandNoteDaoImpl.saveOrUpdate(portalMspLmos);
					}
						MspCpeDmdDtl mspCpeDmdDtl = new MspCpeDmdDtl();
					if (!(lmpCpeRequest.getNoEmiDemandQty().equalsIgnoreCase("0")
							&& lmpCpeRequest.getEmi36DemandQty().equalsIgnoreCase("0")
							&& lmpCpeRequest.getEmi48DemandQty().equalsIgnoreCase("0"))) {
						mspCpeDmdDtl.setDmdId(mspCpeDmd.getDmdid());
						mspCpeDmdDtl.setEmi36DemandQty(Long.parseLong(lmpCpeRequest.getEmi36DemandQty()));
						mspCpeDmdDtl.setEmi48DemandQty(Long.parseLong(lmpCpeRequest.getEmi48DemandQty()));
						mspCpeDmdDtl.setNoEmiDemandQty(Long.parseLong(lmpCpeRequest.getNoEmiDemandQty()));
						mspCpeDmdDtl.setLmoName(lmpCpeRequest.getLmoName());
						mspCpeDmdDtl = cpeDemandNoteDaoImpl.saveOrUpdate(mspCpeDmdDtl);
						emi0 = emi0 + mspCpeDmdDtl.getNoEmiDemandQty();
						emi36 = emi36 + mspCpeDmdDtl.getEmi36DemandQty();
						emi48 = emi48 + mspCpeDmdDtl.getEmi48DemandQty();
					}
				}
				
			}
			map.put("emi0", emi0);
			map.put("emi36", emi36);
			map.put("emi48", emi48);
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return map;
	}

}
