/**
 * 
 */
package com.arbiva.apfgc.tenant.serviceImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantWalletDaoImpl;
import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.model.TenantWallet;
import com.arbiva.apfgc.tenant.vo.TenantVO;

/**
 * @author Lakshman
 *
 */
@Service
public class TenantWalletServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantWalletServiceImpl.class);

	@Autowired
	TenantWalletDaoImpl tenantWalletDao;
	
	@Autowired
	TenantDaoImpl tenantDao;
	
	@Autowired
	TenantServiceImpl  tenantService;


	@Autowired
	HttpServletRequest httpServletRequest;

	/*@Transactional
	public void saveTenantWallet(TenantAgreementVO tenantAgreementVO, String action) {
		String loginID = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		if (loginID != null || loginID != "") {

			TenantWallet tenantWallet = null;
			
			tenantWallet = tenantWalletDao.findByTenantCode(tenantAgreementVO.getTenantCode());
			if(tenantWallet == null){
				tenantWallet = new TenantWallet();
			}
			tenantWallet.setTenantCode(tenantAgreementVO.getTenantCode());
			tenantWallet.setDepositAmount(tenantAgreementVO.getDepositAmount() == null ? 0 : tenantAgreementVO.getDepositAmount());
			tenantWallet.setDepostLastcramt(new BigDecimal(0));
			tenantWallet.setDepostLastdbamt(new BigDecimal(0));
			tenantWallet.setWalletLastcramt(new BigDecimal(0));
			tenantWallet.setWalletLastdbamt(new BigDecimal(0));
			tenantWallet.setWalletAmount(tenantAgreementVO.getWalletAmount() == null ? 0 : tenantAgreementVO.getWalletAmount());
			tenantWallet.setCratedIPAddress(httpServletRequest.getRemoteAddr());
			tenantWallet.setCreatedBy(loginID);
			tenantWallet.setCreatedDate(Calendar.getInstance());
			tenantWallet.setModifiedDate(Calendar.getInstance());
			tenantWallet.setStatus(1);

			tenantWalletDao.saveTenantWallet(tenantWallet);
		}

	}*/

	public List<TenantWallet> findAllTenantWallets() {
		List<TenantWallet> tenantWalletList = new ArrayList<TenantWallet>();
		tenantWalletList = tenantWalletDao.findAllTenantWallets();
		return tenantWalletList;
	}

	public TenantWallet findByTenantCode(String tenantCode) {
		return tenantWalletDao.findByTenantCode(tenantCode);
	}

	public void saveTenantWallet(TenantVO tenantVO, Integer tenantId) {
		String loginID = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		TenantWallet tenantWallet = null;
		try{
			
			if (loginID != null || loginID != "") {

				
				
				tenantWallet = tenantWalletDao.findByTenantCode(tenantVO.getTenantCode());
				if(tenantWallet == null){
					tenantWallet = new TenantWallet();
				}
				tenantWallet.setTenantCode(tenantVO.getTenantCode());
				tenantWallet.setDepositAmount(new BigDecimal(0));
				tenantWallet.setDepostLastcramt(new BigDecimal(0));
				tenantWallet.setDepostLastdbamt(new BigDecimal(0));
				tenantWallet.setWalletLastcramt(new BigDecimal(0));
				tenantWallet.setWalletLastdbamt(new BigDecimal(0));
				tenantWallet.setWalletAmount(new BigDecimal(0));
				tenantWallet.setCratedIPAddress(httpServletRequest.getRemoteAddr());
				tenantWallet.setCreatedBy(loginID);
				tenantWallet.setCreatedDate(Calendar.getInstance());
				tenantWallet.setModifiedDate(Calendar.getInstance());
				tenantWallet.setStatus(1);
				tenantWallet.setCrLimitAmt(tenantVO.getCreditLimit().isEmpty() ? new BigDecimal(0) : new BigDecimal(tenantVO.getCreditLimit()));
				tenantWallet.setUsedAmt(new BigDecimal(0));
				tenantWallet.setCrlimitLastdbamt(new BigDecimal(0));
				tenantWalletDao.saveTenantWallet(tenantWallet);
				LOGGER.info("saveTenantWallet :: Tenant Wallet Saved Successfully");
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			LOGGER.error("saveTenantWallet ::"+ex.getMessage());
		} finally {
			loginID = null;
			tenantWallet = null;
		}
		

	}
	
	
	public void updateTenantsWalletOnFirst(String invyr, String invmn) {
		List<CustomerInvDtlsDTO> custinvDtls = new ArrayList<CustomerInvDtlsDTO>();
		
		
		 try{
			 custinvDtls= tenantService.getMonthWiseShares(invyr, invmn);
			 
			 for (CustomerInvDtlsDTO custdtl : custinvDtls ){
				 tenantWalletDao.updateTenantsWalletOnFirst(custdtl); 
				 
			 }

					} catch (Exception e) {
						LOGGER.error("The Exception is TenantServiceImpl :: updateTenantsWalletOnFirst" + e);
						e.printStackTrace();
					} 

	}



	public boolean updateTenantsWalletOnEndOfMonth(String invyr, String invmn) {
		
		
		
	List<Object[]> resultinvpmntList = new ArrayList<>();
		
		BigDecimal tmptotalpaid=null;
		BigDecimal totalpaid=null;
		BigDecimal apsflshare=null;
		String lmocode="";
		Map<String,String> lmototalcollected = new HashMap<String, String>();
		boolean isDataPresent = false;

	    try{
		
	  
	    resultinvpmntList = tenantDao.getTotalForLMOForMonth(invyr,invmn);
	    if (resultinvpmntList.isEmpty())
	    	isDataPresent=false;
	    
	    
	    for (Object[] list : resultinvpmntList) {
	    	
	    	tmptotalpaid = new BigDecimal(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
	    	apsflshare = new BigDecimal(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
	    	tmptotalpaid=tmptotalpaid.add( apsflshare);
	    	
	    	lmocode=(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
	    	
	    	if (lmototalcollected!=null && lmototalcollected.containsKey(lmocode)) {
	    		totalpaid=totalpaid.add( tmptotalpaid);
	    		totalpaid=totalpaid.setScale(0, RoundingMode.HALF_UP);
	    		lmototalcollected.put(lmocode, String.valueOf(totalpaid));
	    	} else {
	    		lmototalcollected.put(lmocode, String.valueOf(tmptotalpaid));
	    		totalpaid=tmptotalpaid;
	    	}
	    	isDataPresent=true;
	    	
	    }
	    
	   
	    		for (Map.Entry<String, String> entry : lmototalcollected.entrySet())
	    		{
	    			tenantWalletDao.updateTenantsWalletOnEndOfMonth(entry.getKey(),entry.getValue());
	    		}
	    

					} catch (Exception e) {
						LOGGER.error("The Exception is TenantServiceImpl :: updateTenantsWalletOnFirst" + e);
						e.printStackTrace();
					} 
       return isDataPresent;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
