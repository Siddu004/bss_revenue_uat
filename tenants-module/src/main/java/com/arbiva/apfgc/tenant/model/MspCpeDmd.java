package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="mspcpedmd")
@Entity
public class MspCpeDmd {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long dmdid;
	
	@Column(name ="dmdtime")
	private Calendar dmdTime;
	
	@Column(name ="mspcode")
	private String mspCode;
	
	@Column(name = "createdipaddr", length = 20)
    private String createdipaddr;
	
	
	


	public Long getDmdid() {
		return dmdid;
	}

	public void setDmdid(Long dmdid) {
		this.dmdid = dmdid;
	}

	public Calendar getDmdTime() {
		return dmdTime;
	}

	public void setDmdTime(Calendar dmdTime) {
		this.dmdTime = dmdTime;
	}

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}
}
