package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.RegionDaoImpl;
import com.arbiva.apfgc.tenant.model.Region;

@Component("regionService")
@Transactional
public class RegionServiceImpl {
	
	@Autowired
	RegionDaoImpl regionDao;
	
	public List<Region> findAllRegions() {
		List<Region> regions = new ArrayList<Region>();
		regions = regionDao.findAllRegions();
		return regions;
	}
	
	public Region getRegionTypeByRegionName(String regionName) {
		Region region = new Region();
		region = regionDao.getRegionTypeByRegionName(regionName);
		return region;
	}
}
