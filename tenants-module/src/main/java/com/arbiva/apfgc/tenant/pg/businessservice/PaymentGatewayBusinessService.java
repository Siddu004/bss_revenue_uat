package com.arbiva.apfgc.tenant.pg.businessservice;

import java.net.URI;
import java.util.List;

import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;
import com.arbiva.apfgc.tenant.pg.utils.PgHelper;

/**
 *
 * 
 * @author srinivasa
 *
 */
@Component("paymentGatewayBusinessService")
public interface PaymentGatewayBusinessService {

	public String processPaymentRequest(RequestDTO requestDTO);

	public String processBilldeskPaymentResponse(String response, String responseBy);
	
	public String processDigitSignaturePaymentResponse(String response, String responseBy);

	public List<Object[]> getPaymentRequestList();

}
