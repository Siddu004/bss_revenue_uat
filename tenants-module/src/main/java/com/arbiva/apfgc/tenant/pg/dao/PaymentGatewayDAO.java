package com.arbiva.apfgc.tenant.pg.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.model.MerchantDetailsBO;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;
import com.arbiva.apfgc.tenant.pg.model.PaymentResponse;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Repository("paymentGatewayDAO")
public class PaymentGatewayDAO {

	private static final Logger LOGGER = Logger.getLogger(PaymentGatewayDAO.class);

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * 
	 * @param paymentRequest
	 * @return
	 */
	@Transactional
	public PaymentRequest savePaymentRequest(PaymentRequest paymentRequest) {
		try {
			paymentRequest = getEntityManager().merge(paymentRequest);
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::savePaymentRequest() " + ex);
			throw ex;
		}
		return paymentRequest;
	}

	/**
	 * 
	 * @param requestId
	 * @return
	 */
	public PaymentRequest findPaymentRequest(long requestId) {
		PaymentRequest paymentRequest = null;
		try {
			paymentRequest = getEntityManager().find(PaymentRequest.class,
					requestId);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("EXCEPTION::findPaymentRequest() " + e);
		}
		return paymentRequest;
	}

	/**
	 * 
	 * @param paymentResponse
	 * @return
	 */
	@Transactional
	public PaymentResponse savePaymentResponse(PaymentResponse paymentResponse) {
		try {
			paymentResponse = getEntityManager().merge(paymentResponse);
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::savePaymentResponse() " + ex);
			throw ex;
		}
		return paymentResponse;
	}

	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getPaymentRequestList() {
		List<Object[]> list = new ArrayList<>();
     try{		
	        StringBuilder builder = new StringBuilder("select reqid,custid,amount,gatewaytype,IFNULL(email,'-NA-') AS email,IFNULL(phonenumber,'-NA-') phonenumber,request,status,createdon,createdby,modifiedon,IFNULL(modifiedby,'-NA-') modifiedby from pgrequest");
			Query query = getEntityManager() .createNativeQuery(builder.toString());
			list = query.getResultList();
		} catch(Exception ex){
			LOGGER.error("EXCEPTION::getPaymentRequestList() " + ex);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public MerchantDetailsBO getMerchantInfo(String custType) {
		List<Object[]> list = new ArrayList<>();
		MerchantDetailsBO merchantDetails = new MerchantDetailsBO();
     try{		
	        StringBuilder builder = new StringBuilder("select * from merchantdtls where custType='"+custType+"' ");
			Query query = getEntityManager() .createNativeQuery(builder.toString(),MerchantDetailsBO.class);
			merchantDetails = (MerchantDetailsBO) query.getSingleResult();
		} catch(Exception ex){
			LOGGER.error("EXCEPTION::getPaymentRequestList() " + ex);
		}
		return merchantDetails;
	}
	
}
