package com.arbiva.apfgc.tenant.dao;

import java.util.List;

import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.model.CpeCharges;
import com.arbiva.apfgc.tenant.model.CpeProfileMaster;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.PortalMspLmos;
import com.arbiva.apfgc.tenant.model.TenantCpeDemand;

public interface CpeDao {

	List<Object> findAllCpeMasterList();

	List<CpeProfileMaster> findAllCpeModelByCpeType(String cpeType);

	CpeCharges getCpeChargesByProfileId(String getCpeChargesByProfileId);

	void saveCpeOrders(TenantCpeDemand tenantCpeDemand);

	List<Object[]> findAllCpeOrdersList(String tenantCode, String tenantType);

	Object[] findCpeDemandByDemandId(String demandId);

	List<Object[]> findAllcpeDlvByDemandId(String demandId);

	List<String> getAvailabileCpeCountByProfileId(String profileId);

	void saveOrUpdate(CpeStock cpeStock,String tenantCode,String cpesrlno);
	
	<T> T saveOrUpdate(T t);

	CpeStock getCpeByCpeSnoAndMac(String cpeSno, String cpeMacAdd);

	String findIsPaidByDlvId(String dlvid);

	void updateCpeStock(long dlvId);

	List<String> getAllCpeSnosByDlvId(String dlvId);

	void updateCpeDelivery(long dlvId);

	List<Object[]> findAllCpeModels(String string);

	CpeStock getCpeByCpeSnoAndMspCode(String trim, String tenantCode);
	
	CpeStock getCpeByCpeSno(String cpeSno);

	void updateCpeBlockedAmount(int size, String profileId, String tenantCode);
	
	void deleteCpeList( List<CpeStock> cpeStockList,String user);



}
