package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="cpestock")
public class CpeStock {

	@Id 
	@Column(name = "cpeslno")
	private String cpeslno;
	
	@Column(name = "cpemacaddr")
	private String cpeMacAddr;
	
	
	@Column(name = "profile_id")
	private Long profileId;
	
	@Column(name = "batchid")
	private String batchId;

	@Column(name = "batchdate")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date batchDate;
	
	@Column(name = "dlvid")
	private Long dlvId;
	
	@Column(name = "mspcode")
	private String mspCode;
	
	@Column(name = "lmocode")
	private String lmoCode;
	
	@Column(name = "cafno")
	private Long cafNo;
	
	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    @NotNull
    private int status;
	
	@Column(name = "modifiedon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar modifiedon;

	@Column(name = "modifiedby", length = 50)
    private String modifiedby;
	
	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id", nullable = false, insertable = false, updatable = false)
	private CpeProfileMaster cpeProfileMaster;
	
	@ManyToOne
	@JoinColumn(name = "dlvid", referencedColumnName = "dlvid", nullable = false, insertable = false, updatable = false)
	private TenantCpeDlv tenantCpeDemand;
	
	
	

	public String getCpeMacAddr() {
		return cpeMacAddr;
	}

	public void setCpeMacAddr(String cpeMacAddr) {
		this.cpeMacAddr = cpeMacAddr;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Calendar getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Calendar modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getCpeslno() {
		return cpeslno;
	}

	public void setCpeslno(String cpeslno) {
		this.cpeslno = cpeslno;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Long getDlvId() {
		return dlvId;
	}

	public void setDlvId(Long dlvId) {
		this.dlvId = dlvId;
	}

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Long getCafNo() {
		return cafNo;
	}

	public void setCafNo(Long cafNo) {
		this.cafNo = cafNo;
	}

	public CpeProfileMaster getCpeProfileMaster() {
		return cpeProfileMaster;
	}

	public void setCpeProfileMaster(CpeProfileMaster cpeProfileMaster) {
		this.cpeProfileMaster = cpeProfileMaster;
	}

	public TenantCpeDlv getTenantCpeDemand() {
		return tenantCpeDemand;
	}

	public void setTenantCpeDemand(TenantCpeDlv tenantCpeDemand) {
		this.tenantCpeDemand = tenantCpeDemand;
	}
	

}
