/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.model.GlCode;

/**
 * @author Lakshman
 *
 */
@Component("glCodeDao")
public class GlCodeDaoImpl {
	
	private static final Logger LOGGER = Logger.getLogger(GlCodeDaoImpl.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public List<GlCode> findAllGlCodes() {
		 List<GlCode> glCode = new ArrayList<GlCode>();
		 StringBuilder builder = new StringBuilder();
		 TypedQuery<GlCode> query = null;
			try {
				 LOGGER.info("GlCodeDaoImpl :: findAllGlCodes() :: SATRT");
		        builder.append(" FROM ").append(GlCode.class.getSimpleName());
			    query = getEntityManager().createQuery(builder.toString(), GlCode.class);
				glCode = query.getResultList();
				LOGGER.info("END::findAllGlCodes()");
			} catch (Exception e) {
				LOGGER.error("GlCodeDaoImpl::findAllGlCodes() " + e);
			} finally {
				query = null;
				builder = null;
			}
		  return glCode;
	}

}
