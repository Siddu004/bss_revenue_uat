/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.dto.OLT;
import com.arbiva.apfgc.tenant.dto.PageObject;
import com.arbiva.apfgc.tenant.dto.RevenueSharingDTO;
import com.arbiva.apfgc.tenant.dto.UploadHistDTO;
import com.arbiva.apfgc.tenant.model.AdditionalService;
import com.arbiva.apfgc.tenant.model.BlackListCust;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.Districts;
import com.arbiva.apfgc.tenant.model.Mandals;
import com.arbiva.apfgc.tenant.model.MspChnls;
import com.arbiva.apfgc.tenant.model.MspChnlsStg;
import com.arbiva.apfgc.tenant.model.OLTPortDetails;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.UploadHistory;
import com.arbiva.apfgc.tenant.pg.utils.ComsEnumCodes;


/**
 * @author Arbiva
 *
 */
@Repository
public class TenantDaoImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantDaoImpl.class);
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public DataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public Tenant findByTenantCode(String tenantCode) {
		Tenant tenant = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName())
				.append(" WHERE tenantcode=:tenantCode");
		TypedQuery<Tenant> query = null;
		try {
			LOGGER.info("START::findByTenantCode()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			query.setParameter("tenantCode", tenantCode);
			tenant = query.getSingleResult();
			LOGGER.info("END::findByTenantCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantCode() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenant;
	}

	public List<Tenant> findMspCodeByTenantType() {
		List<Tenant> tenantList = new ArrayList<Tenant>();
		TypedQuery<Tenant> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName())
				.append(" WHERE tenanttypelov = 'MSP'");
		try {
			LOGGER.info("START::findMspCodeByTenantType()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			tenantList = query.getResultList();
			LOGGER.info("END::findMspCodeByTenantType()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findMspCodeByTenantType() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenantList;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> findLmoCodeByTenantType(String tCode) {

		List<Tenant> tenantList = new ArrayList<Tenant>();
		// List<String> tenantAgreements = null;

		/*
		 * tenantAgreements = findTenantsIdsforAdmin(tCode); StringBuffer str2 =
		 * new StringBuffer(); if (tenantAgreements.size() > 0) { for (String
		 * tenantCode : tenantAgreements) { str2.append(tenantCode);
		 * str2.append(","); } str2.replace(str2.toString().length() - 1,
		 * str2.toString().length(), ""); } else str2.append("''");
		 * 
		 * String str =
		 * " where tenantcode in(select tenantcode from tenantsagr where status = 2)"
		 * + " and  tenantcode not in (" + str2 + ") ";
		 * 
		 * 
		 * StringBuilder builder = new StringBuilder(" FROM "
		 * ).append(Tenant.class.getSimpleName()).append(str) .append(
		 * " and tenanttypelov = 'LMO' order by createdon desc"); try {
		 * LOGGER.info("START::findLmoCodeByTenantType()"); TypedQuery<Tenant>
		 * query = getEntityManager().createQuery(builder.toString(),
		 * Tenant.class); tenantList = query.getResultList();
		 * LOGGER.info("END::findLmoCodeByTenantType()"); } catch (Exception e)
		 * { LOGGER.error("EXCEPTION::findLmoCodeByTenantType() " + e); }
		 */
		Query query = null;
		StringBuilder builder = new StringBuilder(
				"select * from tenants " + " where tenantcode in(select tenantcode from tenantsagr where status = 2) "
						+ " and tenantcode not in " + " (select lmocode from msplmoagr where mspcode='" + tCode
						+ "' and status != 4 ) " + " and tenanttypelov = 'LMO'  order by createdon desc ;");
		try {
			query = em.createNativeQuery(builder.toString(), Tenant.class);
			tenantList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::findTenantsIdsforAdmin() " + ex);
		} finally {
			query = null;
			builder = null;
		}

		return tenantList;
	}

	/*
	 * public List<String> findTenantsIdsforAdmin(String mspcode) {
	 * List<TenantLmoMspAgreement> tenants = new
	 * ArrayList<TenantLmoMspAgreement>(); List<String> tenantCodes = new
	 * ArrayList<String>(); StringBuilder builder = new StringBuilder(" FROM "
	 * ).append(TenantLmoMspAgreement.class.getSimpleName()) .append(
	 * " WHERE mspcode=:mspcode and status != 4 "); try {
	 * LOGGER.info("START::findTenantsIdsforAdmin()");
	 * TypedQuery<TenantLmoMspAgreement> query =
	 * em.createQuery(builder.toString(), TenantLmoMspAgreement.class);
	 * query.setParameter("mspcode", mspcode); tenants = query.getResultList();
	 * for (TenantLmoMspAgreement tenantAgreement : tenants) {
	 * tenantCodes.add("'" + tenantAgreement.getLmoCode() + "'"); }
	 * tenants.remove(tenants.size());
	 * LOGGER.info("END::findTenantsIdsforAdmin()"); } catch (Exception e) {
	 * LOGGER.error("EXCEPTION::findTenantsIdsforAdmin() " + e); } return
	 * tenantCodes; }
	 */

	/*
	 * public List<String> findTenantsIdsforAPFAdmin(String createdBy) {
	 * List<TenantAgreement> tenants = new ArrayList<TenantAgreement>();
	 * List<String> tenantCodes = new ArrayList<String>(); StringBuilder builder
	 * = new StringBuilder(" FROM "
	 * ).append(TenantAgreement.class.getSimpleName()) .append(
	 * " WHERE createdby=:createdBy"); try {
	 * LOGGER.info("START::findTenantsIdsforAPFAdmin()");
	 * TypedQuery<TenantAgreement> query = em.createQuery(builder.toString(),
	 * TenantAgreement.class); query.setParameter("createdBy", createdBy);
	 * tenants = query.getResultList(); for (TenantAgreement tenantAgreement :
	 * tenants) { tenantCodes.add("'" + tenantAgreement.getTenantCode() + "'");
	 * } tenants.remove(tenants.size());
	 * LOGGER.info("END::findTenantsIdsforAPFAdmin()"); } catch (Exception e) {
	 * LOGGER.error("EXCEPTION::findTenantsIdsforAPFAdmin() " + e); } return
	 * tenantCodes; }
	 */

	public List<Tenant> findAllTenants() {
		List<Tenant> Tenants = new ArrayList<Tenant>();
		TypedQuery<Tenant> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName());
		try {
			LOGGER.info("START::findAllTenants()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			Tenants = query.getResultList();
			LOGGER.info("END::findAllTenants()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenants() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return Tenants;
	}

	public Tenant findByTenantId(Integer tenantid) {
		Tenant tenant = new Tenant();
		TypedQuery<Tenant> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(Tenant.class.getSimpleName())
				.append(" WHERE tenantid=:tenantid");
		try {
			LOGGER.info("START::findByTenantId()");
			query = getEntityManager().createQuery(builder.toString(), Tenant.class);
			query.setParameter("tenantid", tenantid);
			tenant = query.getSingleResult();
			LOGGER.info("END::findByTenantId()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenant;
	}

	public Tenant saveTenant(Tenant tenant) {
		return getEntityManager().merge(tenant);
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> findAllTenantsBYCreatedBy(String loginID, PageObject pageObject) {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		Query query = null;
		String searchParameter = "";
		String whereClause = "";
		String orderByClause = "";
		Query querycount = null;
		StringBuilder builder = new StringBuilder();
		if (pageObject != null) {
			searchParameter = pageObject.getSearchParameter();
			if(loginID.equalsIgnoreCase("apfadmin")) {
				whereClause = whereClause + " where ";
			} else {
				whereClause = whereClause + " and ";
			} 
			
			whereClause = whereClause + " (tenantcode like '%" + searchParameter + "%' or portal_enrollmentno like '%"+ searchParameter + "%' or tenantname like '%" + searchParameter + "%' ";
			whereClause = whereClause + " or tenantTypeLov like '%" + searchParameter + "%'  or regoff_pocmob1 like '%"+ searchParameter + "%' or status like '%" + searchParameter + "%' ) ";
			orderByClause = " ORDER BY "+pageObject.getSortColumn()+" "+pageObject.getSortOrder();
		}
		
		if(loginID.equalsIgnoreCase("apfadmin")) {
			builder.append("SELECT * FROM tenants ");
			builder.append(whereClause +" "+orderByClause );
		} else {
			builder.append(" SELECT * FROM tenants  where tenantcode ='" + loginID + "' or "
				+ " createdby in(select loginid from users where rmuserid = '" + loginID + "') " );
			builder.append(whereClause +" "+orderByClause );
		}
		try {
			query = em.createNativeQuery(builder.toString(), Tenant.class);
			if(pageObject != null) {
				querycount = getEntityManager() .createNativeQuery(builder.toString());
				long totalDisplayCount = querycount.getResultList().size();
				pageObject.setTotalDisplayCount(String.valueOf(totalDisplayCount));
				
				query.setFirstResult(pageObject.getMinSize());
				query.setMaxResults(pageObject.getMaxSize());
			}
			tenantsList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}

		return tenantsList;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> getApprovedLMOTenants(String loginID) {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		Query query = null;
		List<Object[]> objList = new ArrayList<>();
		StringBuilder builder = new StringBuilder(
				"SELECT CONCAT(tenantname,' | ',tenantcode) as tenantname , tenantid FROM tenants ");
		// +" where createdby ='"+loginID+"'or createdby in(select loginid from
		// users where rmuserid = '"+loginID+"') "
		// +" and tenanttypelov = 'LMO' "
		// +" and status = 2 ");
		try {
			query = em.createNativeQuery(builder.toString());
			objList = query.getResultList();

			for (Object[] object : objList) {
				Tenant tenant = new Tenant();
				tenant.setName(object[0] == null ? "" : object[0].toString());
				tenant.setTenantId(object[1] == null ? 0 : Integer.parseInt(object[1].toString()));
				tenantsList.add(tenant);
			}
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			query = null;
			builder = null;
		}

		return tenantsList;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> findAllByTenantType(String tenantType) {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		Query query = null;
		StringBuilder builder = new StringBuilder(
				" select * from tenants where status = 2 and tenanttypelov = :tenantType order by tenantname");
		try {
			query = em.createNativeQuery(builder.toString(), Tenant.class);
			query.setParameter("tenantType", tenantType);
			tenantsList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return tenantsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getDistrictList(String stateID) {
		List<Object[]> districtList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT districtuid, districtname " + " FROM districts "
				+ " WHERE stateid= :stateID " + " ORDER BY districtname asc ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("stateID", stateID);
			districtList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getDistrictList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return districtList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getMandalList(String stateID, String districtID) {
		List<Object[]> mandalList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT mandalslno, mandalname FROM mandals "
				+ " WHERE stateid = :stateID " + " AND districtuid = :districtID " + " ORDER BY mandalname asc ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("stateID", stateID);
			query.setParameter("districtID", districtID);
			mandalList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getMandalList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return mandalList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSubstationsList(String stateID, String districtID) {
		List<Object[]> SubstationsList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT substnuid, substnname FROM substations "
				+ " WHERE districtuid = :districtID " + " ORDER BY substnname asc ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("districtID", districtID);
			SubstationsList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getMandalList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return SubstationsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getMandalbySubstation(String stateID, String districtID, String substationID) {
		List<Object> villageList = new ArrayList<Object>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select mandalslno "
												+" from substations subs "
												+" where districtuid = ? "
												+" and substnuid = ?");
		String mandalID = "";
		Object obj = null;
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter(1, districtID);
			query.setParameter(2, substationID);
			villageList = query.getResultList();
			if(villageList!= null && !villageList.isEmpty()){
				obj = villageList.get(0);
				mandalID = obj == null ? "" : obj.toString();
			}
			
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getMandalbySubstation() " + ex);
		} finally {
			query = null;
			builder = null;
			obj = null;
			villageList = null;
		}
		return mandalID;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getVillageList(String stateID, String districtID, String mandalID) {
		List<Object[]> villageList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT villageslno, villagename FROM villages "
				+ " WHERE stateid = :stateID " + " AND districtuid = :districtID " + " AND mandalslno = :mandalID "
				+ " ORDER BY villagename asc ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("stateID", stateID);
			query.setParameter("districtID", districtID);
			query.setParameter("mandalID", mandalID);
			villageList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getVillageList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return villageList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getcableList() {
		List<Object[]> cableList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT cabletypeid, cabletypename FROM cabletypes");
		try {
			query = em.createNativeQuery(builder.toString());
			cableList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getcableList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return cableList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getassetsList() {
		List<Object[]> assetsList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT assettypeid, assettypename FROM assettypes");
		try {
			query = em.createNativeQuery(builder.toString());
			assetsList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getassetsList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return assetsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getstateList() {
		List<Object[]> stateList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT stateid, statename FROM states");
		try {
			query = em.createNativeQuery(builder.toString());
			stateList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getstateList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return stateList;
	}

	public String checkTenentCode(String tenentCode) {
		String s = "Success";
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT count(1) FROM tenants where tenantcode = :tenentCode ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("tenentCode", tenentCode);
			Object obj = query.getSingleResult();
			String a = obj == null ? "" : obj.toString();
			if (a != "0")
				s = "Failure";
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::checkTenentCode() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return s;
	}

	public String checkTenentType() {
		String s = "Success";
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT count(1) FROM tenants where tenanttypelov = 'APSFL' ");
		try {
			query = em.createNativeQuery(builder.toString());
			Object obj = query.getSingleResult();
			String a = obj == null ? "" : obj.toString();
			if (a != "0")
				s = "Failure";
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::checkTenentCode() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return s;
	}

	public OLTPortDetails saveOltPorts(OLTPortDetails oltPortDetails) throws Exception {
		getEntityManager().persist(oltPortDetails);
		return oltPortDetails;
	}

	@SuppressWarnings("unchecked")
	public List<OLT> getLmoOltLovs(String subStnList) {
		List<Object[]> oltObjList = new ArrayList<Object[]>();
		List<OLT> oltLovs = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select pop_name,pop_olt_serialno, pop_oltlabelno "
				+ " from oltmaster " + " where pop_substnuid in (" + subStnList + ") "
				+ " group by pop_substnuid,pop_olt_serialno, pop_oltlabelno " + " order by pop_oltlabelno");
		try {
			query = em.createNativeQuery(builder.toString());
			oltObjList = query.getResultList();
			for (Object[] object : oltObjList) {
				OLT olt = new OLT();
				olt.setPopName(object[0] == null ? "" : object[0].toString());
				olt.setPopOltSerialno(object[1] == null ? "" : object[1].toString());
				olt.setPop_oltlabelno(object[2] == null ? "" : object[2].toString());
				oltLovs.add(olt);
			}
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getLmoOltLovs() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return oltLovs;
	}

	@SuppressWarnings("unchecked")
	public List<String> getOLTPortIds(String oltId) {
		List<Object> oltObjList = new ArrayList<Object>();
		List<String> oltPortList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder(
				"select portno from oltportdtls " + " where pop_olt_serialno = :oltId ");
		try {
			query = em.createNativeQuery(builder.toString());
			query.setParameter("oltId", oltId);
			oltObjList = query.getResultList();
			for (Object object : oltObjList) {
				oltPortList.add(object == null ? "" : object.toString());
			}
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getLmoOltLovs() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return oltPortList;
	}

	@SuppressWarnings("unchecked")
	public List<OLT> getLmoOltList(String tenantCode) {
		List<Object[]> oltObjList = new ArrayList<Object[]>();
		Query query = null;
		List<OLT> oltLmoList = new ArrayList<>();
		StringBuilder builder = new StringBuilder(
				"select om.pop_oltlabelno, op.pop_olt_serialno, GROUP_CONCAT(DISTINCT portno) portno "
						+ " from oltportdtls op, oltmaster om " + " where op.pop_olt_serialno = om.pop_olt_serialno "
						+ " and lmocode = '" + tenantCode + "' " + " group by pop_olt_serialno,lmocode "
						+ " order by pop_olt_serialno");
		try {
			query = em.createNativeQuery(builder.toString());
			oltObjList = query.getResultList();
			for (Object[] object : oltObjList) {
				OLT olt = new OLT();
				olt.setPop_oltlabelno(object[0] == null ? "" : object[0].toString());
				olt.setPopOltSerialno(object[1] == null ? "" : object[1].toString());
				olt.setOltPortId(object[2] == null ? "" : object[2].toString());
				oltLmoList.add(olt);
			}
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getOLTPortIds() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return oltLmoList;
	}

	@SuppressWarnings("unchecked")
	public List<Tenant> getTempTenantsbyStatus(String tenantType, int status) {
		List<Tenant> tenantsList = new ArrayList<Tenant>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select * from tenants "
				+ " where tenantcode in (select tenantcode from tmp_tenants where status = :status and tenant_type = :tenantType)");
		try {
			query = em.createNativeQuery(builder.toString(), Tenant.class);
			query.setParameter("status", status);
			query.setParameter("tenantType", tenantType);
			tenantsList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return tenantsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> searchCafDetailsForBlockList(String mobileNo, String stbNo, String tenantcode,String tenantType, String stbMac) {
		List<Object[]> cafList = null;
		Query query = null;
		StringBuilder builder;
		String whereClause = null;
		if (mobileNo != null && mobileNo.trim() != "") {
			whereClause = "AND cs.parentcafno IN (SELECT cafno FROM cafs  WHERE STATUS='6' AND custid IN ( SELECT custid FROM customers  WHERE lmocode='"+tenantcode+"' AND pocmob1=TRIM('" + mobileNo + "') ) )";
		}
		if (stbNo != null && stbNo.trim() != "") {
			whereClause = " AND cf.status='6' AND c.lmocode='"+tenantcode+"' AND cs.stbslno= TRIM('" + stbNo + "')";
		}
		if (stbMac != null && stbMac.trim() != "") {
			whereClause =  " AND cf.status='6' AND c.lmocode='"+tenantcode+"' AND cs.stbmacaddr=TRIM('" + stbMac + "')";
		}
		if(tenantType.equals("LMO")){
			builder = new StringBuilder("SELECT cf.cafno, c.aadharno, c.fname, c.lname, cs.stbslno, c.custid, cf.status, c.pocmob1, cs.stbmacaddr, cs.stbcafno ");
			builder.append(" FROM cafstbs cs, customers c, cafs cf WHERE c.custid = cf.custid AND  cs.parentcafno = cf.cafno ");
			builder.append(" "+whereClause);
		} else {
			builder = new StringBuilder("SELECT cf.cafno, c.aadharno, c.fname, c.lname, cf.stbslno, c.custid, cf.status, c.pocmob1, cf.stbmacaddr");
			builder.append(" FROM cafs cf, customers c WHERE c.custid = cf.custid  ");
			builder.append(" AND "+ whereClause);
			builder.append(" AND c.custid IN (SELECT  custid FROM customers WHERE lmocode IN (SELECT partnercode FROM rsagrpartners WHERE agruniqueid  IN (SELECT agruniqueid FROM rsagrpartners WHERE partnercode='"+tenantcode+"') AND  partnertype='LMO')) ");
		}
		try {
			LOGGER.info("START::searchCafDetailsForBlockList() ");
			query = getEntityManager().createNativeQuery(builder.toString());
			cafList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::searchCafDetailsForBlockList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return cafList;
	}

	@SuppressWarnings("unchecked")
	public String checkCustomer(String stbcafno) {
		Query query = null;
		List<Object> status = null;
		String countStatus = "";
		StringBuilder builder = new StringBuilder("SELECT status FROM Blackliststb WHERE stbcafno = '"+stbcafno+"' ");
		try {
			LOGGER.info("START::findByAadharNo()");
			query = getEntityManager().createNativeQuery(builder.toString());
			status = query.getResultList();
			for(Object o : status) {
				countStatus = o.toString();
			}
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::checkCustomer() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return countStatus;
	}
	
	public BlackListCust saveBlackCustomer(BlackListCust blackListCust, String countStatus) {
		Query query1 = null;
		StringBuilder builder1 = new StringBuilder("UPDATE blackliststb SET approvedby = '"+blackListCust.getCreatedby()+"' , effectivefrom=SUBSTRING_INDEX(SYSDATE(),' ',1), ");
		builder1.append(" reason='"+blackListCust.getReason()+"' ,createdon = SYSDATE(), createdipaddr='"+blackListCust.getCreatedipaddr()+ "'," );
		builder1.append("status='0' where stbcafno='"+blackListCust.getStbcafno()+"'");
		try {
			if(countStatus.equalsIgnoreCase("2")) {
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			query1.executeUpdate();
			} else {
				blackListCust = getEntityManager().merge(blackListCust);
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		} finally {
		}
		return blackListCust;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getBlockListedDetails(String loginID) {
		List<Object[]> BlockedList = null;
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT c.aadharno, cf.cafno,cs.stbslno, DATE_FORMAT(b.effectivefrom,'%d/%m/%Y'), b.reason, b.status, c.pocmob1, cs.stbmacaddr, DATE_FORMAT(b.approvedon,'%d/%m/%Y') ");
		builder.append(" FROM cafs cf, customers c ,blackliststb b, cafstbs cs WHERE c.custid = cf.custid  AND cs.parentcafno = cf.cafno AND b.stbcafno = cs.stbcafno ");
		builder.append(" AND  cs.stbcafno in ( SELECT stbcafno FROM blackliststb WHERE createdby='" + loginID + "')");

		try {
			LOGGER.info("START::findByAadharNo()");
			query = getEntityManager().createNativeQuery(builder.toString());
			BlockedList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return BlockedList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> viewDataToapproveBlockList(String loginID) {
		List<Object[]> BlockedList = null;
		Query query = null;
		StringBuilder builder = new StringBuilder(" SELECT b.*, c.custcode, c.pocmob1, cs.stbslno, cs.stbmacaddr, cf.cafno, cs.nwsubscode, DATE_FORMAT(b.effectivefrom,'%d/%m/%Y'), DATE_FORMAT(b.approvedon,'%d/%m/%Y')");
       builder.append(" FROM blackliststb b, cafs cf, customers c,cafstbs cs ");  
       builder.append(" WHERE b.createdby IN ( SELECT loginid  FROM users WHERE  rmuserid='"+loginID+"')");  
       builder.append(" AND b.stbcafno = cs.stbcafno  AND cf.custid=c.custid   AND cs.parentcafno = cf.cafno");
		try {
			LOGGER.info("START::findByAadharNo()");
			query = getEntityManager().createNativeQuery(builder.toString());
			BlockedList = query.getResultList();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return BlockedList;
	}

	public void MakeApproveBlackList(String effectivefrom1, String loginid, String ApprovedIP, String nwsubscode, String stbcafno) {
		Query query = null;
		StringBuilder builder = new StringBuilder("UPDATE blackliststb SET approvedby = '"+loginid+"' , blacklistflag='Y', ");
		builder.append(" approvedon=Sysdate() , approvedipaddr='"+ApprovedIP+ "',status='1' where stbcafno='"+stbcafno+"' and effectivefrom='"+effectivefrom1+"' ");
		//StringBuilder builder1 = new StringBuilder("update customers set status = 99 where custid ='"+custid+"' ");
		StringBuilder builder2 = new StringBuilder(" update cafstbs set stbstatus = '99' , modifiedon=Sysdate()  where stbcafno='"+stbcafno+"'");

		//StringBuilder builder3 = new StringBuilder("update cpestock set status = 99 where cafno in ('"+blackCaf+"') and profile_id in ('3','4')");
		//StringBuilder builder4 = new StringBuilder("update cpestock set status = 1 where cafno in ("+cafnotblacked+") and profile_id in ('3','4')");
		StringBuilder builder5 = new StringBuilder("UPDATE cafsrvcs SET STATUS=99 , modifiedon=Sysdate() WHERE stbcafno IN ("+stbcafno+") ");
		
		// StringBuilder builder5 = new StringBuilder("update cpestock set status = 1 where cafno in ("+provisionedNotStcafs+") and profile_id in ('3','4')");
		try {
			LOGGER.info("START::findByAadharNo()");
			query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();

			//Query query1 = getEntityManager().createNativeQuery(builder1.toString());
			//.executeUpdate();

			Query query2 = getEntityManager().createNativeQuery(builder2.toString());
			query2.executeUpdate();

			//Query query3 = getEntityManager().createNativeQuery(builder3.toString());
			//query3.executeUpdate();

			//Query query4 = getEntityManager().createNativeQuery(builder4.toString());
			//query4.executeUpdate();
			
			Query query5 = getEntityManager().createNativeQuery(builder5.toString());
			query5.executeUpdate();

		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		} finally {
			query = null;
			builder = null;
			//builder1 = null;
			//builder2 = null;
		}
	}

	public void blackListCorpusFail(String custid, String effectivefrom1, String loginid, String ApprovedIP,
			String affectedcafs) {
		StringBuilder builder = new StringBuilder("UPDATE blacklistcust SET approvedby = '" + loginid
				+ "' , blacklistflag='N', approvedon=Sysdate() , approvedipaddr='" + ApprovedIP
				+ "',status='4' where custuid='" + custid + "' and effectivefrom='" + effectivefrom1 + "' ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		} finally {
			query = null;
			builder = null;
		}
	}

	public void RejectBlackList(String effectivefrom1, String loginid, String ApprovedIP, String stbcafno) {
		StringBuilder builder = new StringBuilder("UPDATE blackliststb SET approvedby = '" + loginid
				+ "' , blacklistflag='N', approvedon=Sysdate() , approvedipaddr='" + ApprovedIP
				+ "',status='2' where stbcafno='"+stbcafno+"' and effectivefrom='" + effectivefrom1 + "' ");
		Query query = null;
		try {
			LOGGER.info("START::RejectBlackList()");
			query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getCpeStackStatusBySrlNo() " + ex);
		} finally {
			query = null;
			builder = null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllSubStationIdsByTenantCode(String tenantCode) {
		List<String> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder(
				" select substnuid from tenantbusareas where tenantcode =:tenantCode ");
		Query query = null;
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("tenantCode", tenantCode);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getOltPortsByTenantCode(String tenantcode, String oltSerno) {
		List<String> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder(" select portno from oltportdtls  where pop_olt_serialno = :oltSerno and lmocode = :tenantcode");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("tenantcode", tenantcode);
			query.setParameter("oltSerno", oltSerno);
			list = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		finally {
			query = null;
			builder = null;
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getOltSrlNosByTenantCode(String tenantcode) {
		List<Object[]> list = new ArrayList<>();
		StringBuilder builder = new StringBuilder(" SELECT distinct olpd.pop_olt_serialno, concat(olpd.pop_olt_serialno,'  |  ',oltm.pop_name) ");
						builder.append( " FROM oltportdtls olpd, oltmaster oltm where olpd.lmocode = :tenantCode ");
						builder.append( " and olpd.pop_olt_serialno = oltm.pop_olt_serialno");
		Query query = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("tenantCode", tenantcode);
			list = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		finally {
			query = null;
			builder = null;
		}
		return list;
	}

	public OLTPortDetails getL1SplitterByOltport(String oltPortNo, String oltSerNo, String tenantCode) {
		
		StringBuilder builder = new StringBuilder(" select * from oltportdtls where pop_olt_serialno = :oltSerNo and lmocode = :tenantCode and portno = :oltPortNo ");
		Query query = null;
		OLTPortDetails oltPortDetails = null;
		try{
			query = getEntityManager().createNativeQuery(builder.toString(),OLTPortDetails.class);
			query.setParameter("oltSerNo", oltSerNo);
			query.setParameter("tenantCode", tenantCode);
			query.setParameter("oltPortNo", oltPortNo);
			oltPortDetails =  (OLTPortDetails) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		finally {
			query = null;
			builder = null;
		}
		return oltPortDetails;
	}

	public List<String> getL2SplitterByL1Value(String oltPortNo, String l1Value, String oltSerNo) {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T saveOrUpdate(T t) {
		return getEntityManager().merge(t);
	}

	

	public MspChnlsStg saveMspChnlsStage(MspChnlsStg mspChnlsStg) {
		MspChnlsStg  mspChnlsStgg=new MspChnlsStg();
		try {
			mspChnlsStgg = getEntityManager().merge(mspChnlsStg);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION TenantDao :: saveMspChnlsStage() " + e);
			e.printStackTrace();
		} finally {

		}
		return mspChnlsStgg;
	}
	
	public MspChnls saveMspChnls(MspChnls mspChnls) {
		MspChnls  mspChnlss=new MspChnls();
		try {
			mspChnlss=getEntityManager().merge(mspChnls);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION TenantDao :: saveMspChnls() " + e);
			e.printStackTrace();
		} finally {

		}
		return mspChnlss;
	}

	
	
	public UploadHistory searchUploadHistory(Long uploadId) {
		UploadHistory uploadHistory = new UploadHistory();
		StringBuilder builder = null;
		TypedQuery<UploadHistory> query = null;
		try {
			builder = new StringBuilder(" FROM ").append(UploadHistory.class.getSimpleName()).append(" WHERE uploadid =:uploadId ");
			query = getEntityManager().createQuery(builder.toString(), UploadHistory.class);
			query.setParameter("uploadId", uploadId);
			uploadHistory = query.getSingleResult();
		} catch(Exception e) {
			LOGGER.error("The Exception in TenantDao::updateUploadHistory " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return uploadHistory;
	}

	public List<UploadHistory> viewMspChannels() {
		List<UploadHistory> channelList = new ArrayList<>(); 
		TypedQuery<UploadHistory> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(UploadHistory.class.getSimpleName()).append(" WHERE uploadtype = :uploadtype");
			try {
				LOGGER.info("START :: viewMspChannels()");
				query = getEntityManager().createQuery(builder.toString(), UploadHistory.class);
				query.setParameter("uploadtype", "MSO CHANNELS");
				channelList = query.getResultList();
				LOGGER.info("END :: viewMspChannels()");
				LOGGER.info("END :: viewMspChannels()");
				
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: viewMspChannels() " + e);
			}finally{
				query = null;
				builder = null;
			}
		  return channelList;
	}
	
	public List<MspChnlsStg> mspChannelUploadErrors(String uploadId) {
		List<MspChnlsStg> channelErrorList = new ArrayList<>(); 
		TypedQuery<MspChnlsStg> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(MspChnlsStg.class.getSimpleName()).append(" WHERE uploadId = :uploadId");
			try {
				LOGGER.info("START :: mspChannelUploadErrors()");
				query = getEntityManager().createQuery(builder.toString(), MspChnlsStg.class);
				query.setParameter("uploadId",Long.parseLong(uploadId));
				channelErrorList = query.getResultList();
				LOGGER.info("END :: mspChannelUploadErrors()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: mspChannelUploadErrors() " + e);
				e.printStackTrace();
			}finally{
				query = null;
				builder = null;
			}
		  return channelErrorList;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getChannelCodes(String mspcode) {
		List<String> channelcode = null;
		StringBuilder sb=new StringBuilder();
		String result = "";
		StringBuilder builder1 = new StringBuilder("SELECT chnlcode FROM mspchnlsstg WHERE mspcode = '"+mspcode+"'");
		Query query1 = null;
		try {
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			channelcode = query1.getResultList();
			for (String cc : channelcode) {
				sb.append(cc).append(",");
			}
			 result=sb.toString();
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getTelephoneServ() " + e);
		}finally{
			query1 = null;
			builder1 = null;
		}
		return result;
	}

	public AdditionalService findSrvcByMsoCode(String mspcode) {
		AdditionalService obj = null;;
		StringBuilder builder = null;
		TypedQuery<AdditionalService> query = null;
		List<AdditionalService> list;
		try {
			builder = new StringBuilder(" FROM ").append(AdditionalService.class.getSimpleName()).append(" WHERE srvccode =:srvcCode ");
			query = getEntityManager().createQuery(builder.toString(), AdditionalService.class);
			query.setParameter("srvcCode", mspcode);
			list = query.getResultList();
			if(list.size() > 0)
			 obj = list.get(0);
			
		} catch(Exception e) {
			LOGGER.error("The Exception in TenantDao::updateUploadHistory " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	public MspChnlsStg getMspChannelsStage(Long uploadId, String code, String mspCode) {
		MspChnlsStg obj = null;;
		StringBuilder builder = null;
		Query query = null;
		List<MspChnlsStg> list;
		try {
			builder = new StringBuilder(" select * from mspchnlsstg where uploadid = :uploadId and mspcode = :mspCode and chnlcode = :chanCode ");
			query = getEntityManager().createNativeQuery(builder.toString(), MspChnlsStg.class);
			query.setParameter("uploadId", uploadId);
			query.setParameter("mspCode", mspCode);
			query.setParameter("chanCode", code);
			list = query.getResultList();
			if(list.size() > 0)
			 obj = list.get(0);
		} catch(Exception e) {
			LOGGER.error("The Exception in TenantDao::getMspChannelsStage " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return obj;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UploadHistDTO> getSrvcInfo(String loginId) {
		List<Object[]> list = new ArrayList<>();
		List<UploadHistDTO> uploadHistDTOList = new ArrayList<>();
		List <AdditionalService> packageList= new ArrayList<>();
		StringBuilder builder1 ;
		Query query = null;
		
		try {
			builder1 = new StringBuilder(" FROM ").append(AdditionalService.class.getSimpleName()).append(" WHERE mspcode =:mspcode ");
			query = getEntityManager().createQuery(builder1.toString(), AdditionalService.class);
			query.setParameter("mspcode", loginId);
			packageList=query.getResultList();
			
			for(AdditionalService additionalService:packageList)
			{
				String ftrcode=additionalService.getFeaturecodes();
				String pkgname=additionalService.getServName();
					StringBuilder builder2 = new StringBuilder("SELECT ms.uploadid,ms.uploadrecno,ms.mspcode,ms.chnlcode, "
							+ " ms.chnlname,ms.pkgname,ms.upldstatus "
							+ " FROM mspchnlsstg ms WHERE ms.chnlcode "
							+ " IN ("+ftrcode+") AND upldstatus=01 AND ms.pkgname='"+pkgname+"' ");
					Query query2 = getEntityManager().createNativeQuery(builder2.toString());
					list = (List<Object[]>) query2.getResultList();
					for (Object[] object : list) {
						UploadHistDTO uploadHistDTO = new UploadHistDTO();
						uploadHistDTO.setUploadid(object[0] == null ? "" : object[0].toString());
						uploadHistDTO.setUploadrecno(object[1] == null ? "" : object[1].toString());
						uploadHistDTO.setChnlcode(object[3] == null ? "" : object[3].toString());
						uploadHistDTO.setChnlname(object[4] == null ? "" : object[4].toString());
						uploadHistDTO.setPkgname(object[5] == null ? "" : object[5].toString());
						uploadHistDTOList.add(uploadHistDTO);
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getTelephoneServ() " + e);
		}finally{
			builder1 = null;
		}
		return uploadHistDTOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AdditionalService> getpackageInfo(String loginId) {
		List<AdditionalService> additionalServiceList = new ArrayList<>();
		StringBuilder builder1 ;
		Query query = null;
		try {
			
			builder1 = new StringBuilder(" FROM ").append(AdditionalService.class.getSimpleName()).append(" WHERE mspcode =:mspcode ");
			query = getEntityManager().createQuery(builder1.toString(), AdditionalService.class);
			query.setParameter("mspcode", loginId);
			additionalServiceList=query.getResultList();
			
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getTelephoneServ() " + e);
		}finally{
			query = null;
			builder1 = null;
		}
		return additionalServiceList;
	}
	
	@Transactional
	public String executeProcessrevshare(String date) {
		String output = null;
		SimpleJdbcCall generateSequenceSP = null;
		Map<String,Object> in = null;
		Map<String, Object> out = null;
		try{
			LOGGER.debug("processrevshare() Input p_yyyymm:" + date  + " p_processtype:" + 'U');
			generateSequenceSP = new SimpleJdbcCall(jdbcTemplate).withProcedureName("processrevshare").withoutProcedureColumnMetaDataAccess().
					useInParameterNames("p_yyyymm" , "p_processtype") 
					.declareParameters(new SqlParameter("p_yyyymm",Types.BIGINT), 
							new SqlParameter("p_processtype", Types.VARCHAR),
							new SqlOutParameter("p_result", Types.VARCHAR));
			
			in = new HashMap<String, Object>();
			in.put("p_yyyymm",Integer.parseInt(date));
			in.put("p_processtype" , 'U');
			out = generateSequenceSP.execute(in);			
			if(out.get("p_result") != null)
				output = out.get("p_result").toString();
			LOGGER.debug("executeProcessrevshare() Output: " + output);
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred during executeProcessrevshare(): " + ex); 
			throw ex;
		}
		finally {
			generateSequenceSP = null;
			in = null;
			out = null;
		}
		
		return output;
	}

	@SuppressWarnings("unchecked")
	public List<RevenueSharingDTO> getRevSharingList(String revDate) {
		List<Object[]> list = new ArrayList<>();
		List<RevenueSharingDTO> revenueSharingDTOList = new ArrayList<>();
			try {
				LOGGER.info("START :: revSharingExcelDownload()");
				StringBuilder builder = new StringBuilder("SELECT rmi.tenantcode,rmi.formon,"
						+ " rmi.shareamt,rmi.taxamt,rmi.enttax,tbs.ifsccode,tbs.accountno,t.tenantname,rmi.grosspaidamt,rmi.netpaidamt "
						+ " FROM rsmonthlyin rmi, tenantbanks tbs, tenants t WHERE tbs.tenantcode = rmi.tenantcode "
						+ " AND t.tenantcode = rmi.tenantcode "
						+ " AND rmi.status = '1' "
						+ " AND rmi.formon="+revDate+" ");
				
				Query query2 = getEntityManager().createNativeQuery(builder.toString());
				list = (List<Object[]>) query2.getResultList();
				for (Object[] object : list) {
					RevenueSharingDTO revenueSharingDTO = new RevenueSharingDTO();
					revenueSharingDTO.setTenantcode(object[0] == null ? "" : object[0].toString());
					revenueSharingDTO.setFormon(object[1] == null ? "" : object[1].toString());
					revenueSharingDTO.setShareamt(object[2] == null ? "" : object[2].toString());
					revenueSharingDTO.setTaxstr(object[3] == null ? "" : object[3].toString());
					revenueSharingDTO.setEnttax(object[4] == null ? "" : object[4].toString());
					revenueSharingDTO.setIfscCode(object[5] == null ? "" : object[5].toString());
					revenueSharingDTO.setAccountNo(object[6] == null ? "" : object[6].toString());
					revenueSharingDTO.setTenantName(object[7] == null ? "" : object[7].toString());
					revenueSharingDTO.setGrosspaidamt(object[8] == null ? "" : object[8].toString());
					revenueSharingDTO.setNetpaidamt(object[9] == null ? "" : object[9].toString());
					revenueSharingDTOList.add(revenueSharingDTO);
			}
				LOGGER.info("END :: revSharingExcelDownload()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: revSharingExcelDownload() " + e);
				e.printStackTrace();
			}finally{
			}
		  return revenueSharingDTOList;
	}

	public void updateRsMonIn(RevenueSharingDTO revenue) {
		StringBuilder builder = new StringBuilder();
		builder.append("update rsmonthlyin SET status = 11 where tenantcode = '"+revenue.getTenantcode()+"' and formon ='"+revenue.getFormon()+"'");
		
		try{
			Query query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
		}catch(Exception e){
			LOGGER.error("EXCEPTION :: updateRsMonIn() " + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<RevenueSharingDTO> getRevenueSharingList(String date) {
		List<Object[]> list = new ArrayList<>();
		List<RevenueSharingDTO> revenueSharingDTOList = new ArrayList<>();
			try {
				LOGGER.info("START :: revSharingExcelDownload()");
				StringBuilder builder = new StringBuilder("");
				
				builder.append(" SELECT x.tenantcode,x.formon,x.shareamt,x.taxamt,x.enttax,tb.ifsccode,tb.accountno,x.tenantname,x.grosspaidamt,x.netpaidamt,x.status");
				builder.append(" FROM");
				builder.append(" (SELECT rmi.tenantcode,rmi.formon,rmi.shareamt,rmi.taxamt,rmi.enttax,t.tenantname,rmi.grosspaidamt,rmi.netpaidamt,rmi.status");
				builder.append(" 	FROM rsmonthlyin rmi,  tenants t ");
				builder.append(" WHERE t.tenantcode = rmi.tenantcode ");
				builder.append(" AND rmi.formon="+date+") x LEFT OUTER JOIN tenantbanks tb ");
				builder.append(" ON tb.tenantcode = x.tenantcode ");
				
				Query query2 = getEntityManager().createNativeQuery(builder.toString());
				list = (List<Object[]>) query2.getResultList();
				for (Object[] object : list) {
					RevenueSharingDTO revenueSharingDTO = new RevenueSharingDTO();
					revenueSharingDTO.setTenantcode(object[0] == null ? "" : object[0].toString());
					revenueSharingDTO.setFormon(object[1] == null ? "" : object[1].toString());
					revenueSharingDTO.setShareamt(object[2] == null ? "" : object[2].toString());
					revenueSharingDTO.setTaxstr(object[3] == null ? "" : object[3].toString());
					revenueSharingDTO.setEnttax(object[4] == null ? "" : object[4].toString());
					revenueSharingDTO.setIfscCode(object[5] == null ? "Not Found" : object[5].toString());
					revenueSharingDTO.setAccountNo(object[6] == null ? "Not Found" : object[6].toString());
					revenueSharingDTO.setTenantName(object[7] == null ? "" : object[7].toString());
					revenueSharingDTO.setGrosspaidamt(object[8] == null ? "" : object[8].toString());
					revenueSharingDTO.setNetpaidamt(object[9] == null ? "" : object[9].toString());
					revenueSharingDTO.setStatus(object[10] == null ? "" : object[10].toString());
					revenueSharingDTOList.add(revenueSharingDTO);
			}
				LOGGER.info("END :: revSharingExcelDownload()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION :: revSharingExcelDownload() " + e);
				e.printStackTrace();
			}finally{
			}
		  return revenueSharingDTOList;
	}

	@SuppressWarnings("unchecked")
	public List<Districts> findAllDistricts() {
		return (List<Districts>) getEntityManager()
				.createQuery(
						"Select districts from " + Districts.class.getSimpleName() + " districts order by districtname")
				.getResultList();
	}

	public List<Mandals> getMandalsByDistrictId(Integer districtId) {
		List<Mandals> mandalsList = new ArrayList<Mandals>();
		StringBuilder builder = new StringBuilder(" FROM ").append(Mandals.class.getSimpleName())
				.append(" WHERE districtuid=:districtId order by mandalname ");
		TypedQuery<Mandals> query = null;
		try {
			LOGGER.info("TenantDao : START::getMandalsByDistrictId()");
			query = getEntityManager().createQuery(builder.toString(), Mandals.class);
			query.setParameter("districtId", districtId);
			mandalsList = query.getResultList();
			LOGGER.info("TenantDao : END::getMandalsByDistrictId()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION in TenantDao::getMandalsByDistrictId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return mandalsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> searchOltsDetails(int districtuid, int mandalslno) {
		List<Object[]> list = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder(" SELECT a.substnuid, a.substnname, b.pop_olt_serialno ,");
				builder.append(" SUM(LENGTH(slots)-LENGTH(REPLACE(slots,'1',''))) subsciber_count FROM substations a, oltmaster b,");
				builder.append(" oltportdtls c WHERE a.substnuid = b.pop_substnuid AND  ");
				builder.append(" b.pop_olt_serialno = c.pop_olt_serialno AND a.districtuid = :districtuid AND ");
				builder.append(" a.mandalslno = :mandalslno GROUP BY  a.substnuid, a.substnname, b.pop_olt_serialno ORDER ");
				builder.append(" BY a.substnname, b.pop_olt_serialno ");
		
		/*StringBuilder builder = new StringBuilder("	SELECT DISTINCT a.substnuid, a.substnname, b.pop_olt_serialno, ");
		builder.append(" IFNULL(( SELECT MAX(createdon) FROM oltportdtls WHERE pop_olt_serialno = c.pop_olt_serialno),NULL) AS Createdon, ");
		builder.append(" IFNULL(( SELECT MAX(createdby) FROM oltportdtls WHERE pop_olt_serialno = c.pop_olt_serialno),NULL) AS Createdby, ");
		builder.append(" IFNULL(( SELECT MAX(modifiedon) FROM oltportdtls WHERE pop_olt_serialno = c.pop_olt_serialno),NULL) AS Modifiedon, ");
		builder.append(" IFNULL((SELECT Convert (COUNT(cafno)  USING utf8) FROM cafs cf WHERE cf.olt_id=c.pop_olt_serialno AND cf.status IN('6','2')),'0') AS subsciber_count ");
		builder.append(" FROM substations a, oltmaster b, oltportdtls c  ");
		builder.append(" WHERE a.substnuid = b.pop_substnuid  ");
		builder.append(" AND b.pop_olt_serialno = c.pop_olt_serialno ");
		builder.append(" AND a.districtuid = :districtuid AND a.mandalslno =:mandalslno ");
		builder.append(" GROUP BY a.substnuid, a.substnname, b.pop_olt_serialno, c.createdon, c.createdby,c.modifiedon ");
		builder.append(" ORDER BY a.substnname, b.pop_olt_serialno ");*/
		

		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("districtuid", districtuid);
			query.setParameter("mandalslno", mandalslno);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getTenantDetailBySubstnUid(String substnuid) {
		List<Object[]> tenantList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT a.tenantcode, b.tenantname FROM tenantbusareas a,");
				builder.append(" tenants b WHERE a.tenantcode = b.tenantcode AND a.substnuid = :substnuid AND b.tenanttypelov='LMO'");
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("substnuid", substnuid);
			tenantList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return tenantList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getProtDtlsByOltSrlNo(String oltno) {
		List<Object[]> tenantList = new ArrayList<>();
		Query query = null;
		StringBuilder builder = new StringBuilder(
				"SELECT portno,LENGTH(slots)-LENGTH(REPLACE(slots,'1','')),lmocode  ,a.createdon,a.modifiedon,a.createdby   ");
		builder.append(" FROM oltportdtls a WHERE pop_olt_serialno =" + oltno + "");
		
		/*StringBuilder builder = new StringBuilder("	SELECT portno,IFNULL((SELECT Convert (COUNT(cafno)  USING utf8) FROM cafs cf WHERE cf.olt_id=a.pop_olt_serialno AND cf.status IN('6','2') AND cf.olt_cardno=a.cardid AND cf.olt_portid=a.portno),'0') AS subsciber_count,");
		builder.append("  lmocode FROM oltportdtls a WHERE a.pop_olt_serialno =" + oltno + "");*/
		
		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			tenantList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return tenantList;
	}

	@SuppressWarnings("rawtypes")
	public OLTPortDetails getTenantByPortNo(String popOltSerialno, String portNo) {
		StringBuilder builder = new StringBuilder(
				" select * from oltportdtls where pop_olt_serialno = :popOltSerialno and portno = :portNo ");
		Query query = null;
		OLTPortDetails oltPortDetails = new OLTPortDetails();

		try {
			query = getEntityManager().createNativeQuery(builder.toString(), OLTPortDetails.class);
			query.setParameter("popOltSerialno", popOltSerialno);
			query.setParameter("portNo", portNo);
			List list = query.getResultList();
			if (list.size() > 0)
				oltPortDetails = (OLTPortDetails) query.getResultList().get(0);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return oltPortDetails;
	}
	
	public List<Object[]> getCpeDetails(String fromDate, String toDate, String tenantType, String tenantCode, String cpeType,String cpeSerialNumber, List<String> cpeSearchStockList) {
		List<Object[]> list = new ArrayList<>();
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		
		
		try {
			if(fromDate != null && !fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
				whereClause = whereClause + " and cps.batchdate BETWEEN '"+fromDate+"' and '"+toDate+"' ";
				
			}
			 if(tenantType != null && !tenantType.isEmpty()){
				 if(tenantType.equalsIgnoreCase(ComsEnumCodes.MSP_Tenant_Type.getCode()))
					{
					 if(tenantCode != null && !tenantCode.isEmpty()){
						 whereClause = whereClause + " and cps.mspcode = '"+tenantCode+"' ";
					 }else{
						 whereClause = whereClause + " and cps.mspcode != '' ";
					 }
					}
					if(tenantType.equalsIgnoreCase(ComsEnumCodes.LMO_Tenant_Type.getCode()))
					{
						if(tenantCode != null && !tenantCode.isEmpty()){
							whereClause = whereClause + " and cps.lmocode = '"+tenantCode+"' ";
						}else{
							 whereClause = whereClause + " and cps.lmocode != '' ";
						 }
					}
			 }else{
				 if(tenantCode != null && !tenantCode.isEmpty()){
					 whereClause = whereClause + " and (cps.lmocode = '"+tenantCode+"' OR cps.mspcode = '"+tenantCode+"') ";
					}
			 }
			
				
			if(cpeType != null && !cpeType.isEmpty()){
				whereClause = whereClause + "  AND cpm.cpetypelov='"+cpeType+"' ";
			}
			
			if(cpeSerialNumber != null && !cpeSerialNumber.isEmpty()){
				 whereClause = whereClause + " and (cps.cpeslno = '"+cpeSerialNumber+"') ";
				}
			
			if (cpeSearchStockList==null || cpeSearchStockList.size()==0){
			
								builder = new StringBuilder(" SELECT Distinct cps.cpeslno,cps.cpemacaddr, cps.profile_id, cps.batchid, cps.batchdate, cps.mspcode, cps.lmocode, cps.status ,cpm.cpetypelov, cpm.cpe_profilename, d.districtname, m.mandalname, v.villagename, ");
								builder.append(" cpm.cpe_model,cps.cafno,t.tenantcode  ");
								builder.append(" FROM cpestock cps , cpe_profilemaster cpm , tenants t, districts d, mandals m, villages v WHERE (cps.status=2 OR cps.status = 3 OR cps.status=99) ");
								builder.append(" AND cpm.profile_id=cps.profile_id ");
								builder.append(" AND (t.tenantcode=cps.lmocode OR t.tenantcode=cps.mspcode) ");
								builder.append(" AND d.districtuid=t.portal_districtid ");
								builder.append(" AND m.mandalslno=t.portal_mandalid ");
								builder.append(" AND v.villageslno=t.portal_villageid ");
								builder.append(" AND D.DISTRICTUID = m.DISTRICTUID AND m.mandalslno = v.mandalslno AND D.DISTRICTUID = v.DISTRICTUID " + whereClause + " order by cps.cpeslno");
			
			}else

			{//cpe stock changes
				StringBuilder queryString=new StringBuilder();
				for (String stock : cpeSearchStockList) {
					   
					  {
						  queryString.append("'" + stock + "'" + ",");
					   }
					}
				queryString.deleteCharAt(queryString.length()-1);
				
				whereClause = whereClause + "  AND cps.cpeslno in ( "+queryString.toString()+" ) ";
					builder = new StringBuilder(" SELECT DISTINCT cps.cpeslno, cps.cpemacaddr, cps.profile_id, cps.batchid, cps.batchdate, cps.mspcode, cps.lmocode, cps.status ,cpm.cpetypelov, cpm.cpe_profilename, d.districtname, m.mandalname, v.villagename, ");
					builder.append(" cpm.cpe_model,cps.cafno,t.tenantcode");
					builder.append(" FROM cpestock cps , cpe_profilemaster cpm , tenants t, districts d, mandals m, villages v WHERE (cps.status=2 OR cps.status = 3 OR cps.status=4 OR cps.status=99) ");
					builder.append(" AND cpm.profile_id=cps.profile_id ");
					builder.append(" AND (t.tenantcode=cps.lmocode OR t.tenantcode=cps.mspcode) ");
					builder.append(" AND d.districtuid=t.portal_districtid ");
					builder.append(" AND m.mandalslno=t.portal_mandalid ");
					builder.append(" AND v.villageslno=t.portal_villageid ");
					builder.append(" AND D.DISTRICTUID = m.DISTRICTUID AND m.mandalslno = v.mandalslno AND D.DISTRICTUID = v.DISTRICTUID " + whereClause + " order by cps.cpeslno");

			}		
			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
	}
	
	public void updateLMOCpeDetails(String cpeSrlNo,String lmoCode,String user) {
		StringBuilder builder = new StringBuilder();
		StringBuilder builderLog = new StringBuilder();
		Timestamp date=null;
		builder.append("update cpestock SET lmocode = '"+lmoCode+"' , mspcode = '', status = 3  where cpeslno = '"+cpeSrlNo+"' ");
		
		try{
			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: START" + builder);
			Query query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
			date= getModifiedDate(cpeSrlNo);
			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: START updating user in log table" + builder);
			if (date!=null){
			builderLog.append("update cpestock_history SET user = '"+user+"'   where cpesrlno = '"+cpeSrlNo+"' and transaction_date=" +"'"+date+"'"  );
			Query queryLog=getEntityManager().createNativeQuery(builderLog.toString());
			queryLog.executeUpdate();
			}
			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: END updating user in log table");
			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: END");
		}catch(Exception e){
			LOGGER.error("EXCEPTION :: updateLMOCpeDetails() " + e);
		}
	}
	
	public void updateMSPCpeDetails(String cpeSrlNo,String mspCode,String user) {
		StringBuilder builder = new StringBuilder();
		StringBuilder builderLog = new StringBuilder();
		Timestamp date=null;
		builder.append("update cpestock SET mspcode = '"+mspCode+"' , lmocode = '' , status = 2 where cpeslno = '"+cpeSrlNo+"' ");
		
		try{
			LOGGER.info("TenantDAOImpl :: updateMSPCpeDetails() :: END" + builder);
			Query query = getEntityManager().createNativeQuery(builder.toString());
			query.executeUpdate();
			date= getModifiedDate(cpeSrlNo);
			if (date!=null){
			builderLog.append("update cpestock_history SET user = '"+user+"'   where cpesrlno = '"+cpeSrlNo+"' and transaction_date=" +"'"+date+"'"  );
			Query queryLog=getEntityManager().createNativeQuery(builderLog.toString());
			queryLog.executeUpdate();
		    }
			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: END updating user in log table");
			LOGGER.info("TenantDAOImpl :: updateMSPCpeDetails() :: END");
		}catch(Exception e){
			LOGGER.error("EXCEPTION :: updateMSPCpeDetails() " + e);
		}
	}
	
	public int getCardNo(String oltSerialNo) {
		int cardNo = 0;
		StringBuilder builder = new StringBuilder();
		try {
			LOGGER.info("TenantDAOImpl :: getCardNo() :: START" + builder);
			builder.append("select pop_oltlabelno from oltmaster where pop_olt_serialno = '"+oltSerialNo+"' ");
			Query query=getEntityManager().createNativeQuery(builder.toString());
			cardNo = Integer.parseInt(query.getSingleResult().toString());
			LOGGER.info("TenantDAOImpl :: getCardNo() :: END");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION :: getCardNo() " + e);
		}
		return cardNo;
		
	}
	
	public int getCardId(String popslno) {
		String query ="";
		int cardnum=-1;
		int[] seq=new int[]{0,2,3,4,2,3,4,2,3,4};
		try{
			LOGGER.info("START::getCard()");
			query=getEntityManager().createNativeQuery("select pop_oltlabelno from oltmaster where pop_olt_serialno = "+popslno).getSingleResult().toString();
		cardnum=(Integer.parseInt(query.toString()));
			//1,2,3(91,92,93,94-1,  95-1,96,97)
			// 4-1
			//1-8---same ,91>
			if(cardnum<=8){
				return 1;
			}
			else{//91,92,93-2,3,4 && 94,95,96,-2,3,4
				cardnum=cardnum%10;	
			}
			
		}catch(Exception e){
			LOGGER.info("Error in::getCard()");
		}
		finally{
			LOGGER.info("End::getCard()");
		}
		return seq[cardnum];
		
	}
	
	
	@SuppressWarnings("rawtypes")
	public Timestamp getModifiedDate(String cpeslno) {
		StringBuilder builder = new StringBuilder(
				"select modifiedon from cpestock where cpeslno ="+"'"+cpeslno+"'");
		Query query = null;
		Timestamp date = null;

		try {
			query = getEntityManager().createNativeQuery(builder.toString());
			//query.setParameter("cpeslno", cpeslno);
			
			List list = query.getResultList();
			if (list.size() > 0)
				date =  (Timestamp) list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return date;
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getCustInvList(String monthStartdate, String MonthEndDate) {
		
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		List<Object[]> list = new ArrayList<>();

		try {
			if(monthStartdate != null && !monthStartdate.isEmpty() && MonthEndDate != null && !MonthEndDate.isEmpty()){
				whereClause = "where chargeddate BETWEEN '" +monthStartdate+"' and   '"+MonthEndDate+"' ";
				
			}
			
		//	builder = new StringBuilder(" select cf.invno, cf.custinvno,cf.acctcafno,cf.pmntcustid,cf.prodcode,cf.chargecode,cf.chargeamt,cf.srvctax from cafinvdtls cf where custinvno  in (SELECT custinvno FROM custinv where " + whereClause+")");

			builder = new StringBuilder("  select cf.invno, cf.custinvno,cf.acctcafno,cf.pmntcustid,cf.prodcode,cf.chargecode,cf.chargeamt,cf.srvctax from cafinvdtls cf  " + whereClause+ " and custinvno is not null");
			
			/*builder = new StringBuilder("select cf.invno, cf.custinvno,cf.acctcafno,cf.pmntcustid,cf.prodcode,cf.chargecode,cf.chargeamt,cf.srvctax,cu.invtdate,cu.invfdate,cu.invyr,cu.invmn,c.lmocode, cm.enttypelov");
			builder.append("  from cafinvdtls cf left join cafs c on cf.acctcafno=c.cafno left join");
			builder.append(" custinv cu on cf.custinvno=cu.custinvno left join  customermst cm on cm.parentcustcode=cf.pmntcustid");
			builder.append(" where chargeddate BETWEEN '2017/12/01' and   '2017/12/31'  and cf.custinvno is not null ");*/
 
			/*builder = new StringBuilder("	SELECT      cf.invno,     cf.custinvno,     cf.acctcafno,     cf.pmntcustid,     cf.prodcode,     cf.chargecode,     cf.chargeamt,     cf.srvctax,     cm.enttypelov FROM    ");
					builder.append(	 "cafinvdtls cf Left join customermst cm on cf.pmntcustid=cm.parentcustcode " +whereClause );   
				//	chargeddate BETWEEN '2017/12/01' AND '2017/12/31'         AND custinvno IS NOT NULL
*/			
			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list =  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getRevenueDetailsSummary(String invyr, String invmn) {
		
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		List<Object[]> list = new ArrayList<>();

		try {
			if(invyr != null && !invyr.isEmpty() && invmn != null && !invmn.isEmpty()){
				whereClause =  " where invyr ='"+invyr+"' and invmn= '"+invmn+"' ";
				
			}
			//Select paidamount from 
			builder = new StringBuilder("SELECT lmocode,mspcode, SUM(lmoshareamt) AS lmoshare,SUM(msoshareamt) AS msoshare,SUM(apsflshareamt) apsflshare " );
			builder.append(" FROM  invamtsharedtls " +whereClause+ " and lmocode <>''  GROUP BY lmocode ,mspcode having sum(lmoshareamt)+sum(msoshareamt)+sum(apsflshareamt)>0 ");
			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list =  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getTotalForLMOForMonth(String invyr, String invmn) {
		
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		List<Object[]> list = new ArrayList<>();

		try {
			if(invyr != null && !invyr.isEmpty() && invmn != null && !invmn.isEmpty()){
				whereClause =  "  cu.invyr ='"+invyr+"' and cu.invmn= '"+invmn+"' ";
				
			}
		/*	
			builder = new StringBuilder("SELECT (cu.srvctax + cu.invamt + cu.prevbal) as totalsum, cf.lmocode FROM custinv cu,cafs cf WHERE cu.prevpaidamt = '1' ");
					builder.append(" AND cu.pmntcustid = cf.custid AND "+ whereClause + "and cf.custid is not null GROUP BY cf.lmocode ,cu.custinvno ");*/
					
			builder = new StringBuilder("	select (cu.srvctax + cu.invamt + cu.prevbal-prevpymtreceived) as totalsum, cf.lmocode,Sum(inv.apsflshareamt)as apsflshare from custinv cu , invamtsharedtls inv, cafs cf where cu.custinvno=inv.custinvno and cu.pmntcustid=cf.custid and ");
					builder.append( whereClause + " and cu.prevpaidamt=1 group by cu.pmntcustid");

			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list =  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getCustInvListWithDates(String invyr, String invmn) {
		
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		List<Object[]> list = new ArrayList<>();

		try {
			if(invyr != null && !invyr.isEmpty() && invmn != null && !invmn.isEmpty()){
				whereClause =  " ci.invyr ='"+invyr+"' and ci.invmn= '"+invmn+"' ";
			}
			
			/*builder = new StringBuilder("  SELECT ci.custinvno,ci.invfdate,ci.invtdate,ci.invyr,ci.invmn,c.lmocode ");
			builder.append(" FROM custinv ci LEFT JOIN cafs c ON ci.pmntcustid = c.custid where   ' and  " +whereClause );
*/
			builder = new StringBuilder(" SELECT ci.custinvno,ci.invfdate,ci.invtdate,ci.invyr,ci.invmn,c.lmocode,cm.enttypelov FROM custinv ci LEFT JOIN cafs c ON ci.pmntcustid = c.custid left join customermst cm "); 
					builder.append(" on ci.pmntcustid = cm.parentcustcode  where "+ whereClause+"  group by ci.custinvno "); 
			
			
			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list =  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
		
	}
	
	
	
	
	

	
	
	/*@Transactional
	public boolean updateInvamtSharedtls(CustomerInvDtlsDTO custinv, BigDecimal apsflamt,BigDecimal lmoamt,BigDecimal msoamt) {
		String qry = null;
		Query query = null;
		
		boolean result;
		
		try {
			LOGGER.info("START::updateInvamtSharedtls()");
		
		qry ="insert into invamtsharedtls (invno, cafno, prodcode, chargecode, chargeamt,srvctax,lmoshareamt,msoshareamt,apsflshareamt,invfromdate,invtodate,lmocode,mspcode,invyr,invmn)values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
;
        	query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1,custinv.getInvno() );
			query.setParameter(2,custinv.getAcctcafno() );
			query.setParameter(3, custinv.getProdcode());
			query.setParameter(4, custinv.getChargecode());
			query.setParameter(5,custinv.getChargeamt() );
			query.setParameter(6,custinv.getSrvctax());
			query.setParameter(7,lmoamt.toString() );
			query.setParameter(8,msoamt.toString() );
			query.setParameter(9,apsflamt.toString() );
			query.setParameter(10,custinv.getInvfromdate() );
			query.setParameter(11,custinv.getInvtodate() );
			query.setParameter(12,custinv.getLmocode() );
			query.setParameter(13,custinv.getMsocode());
			query.setParameter(14,custinv.getInvyr() );
			query.setParameter(15,custinv.getInvmn() );
			
			
        	query.executeUpdate();
        	
        	result=true;
        	
        	LOGGER.info("END::updateInvamtSharedtls()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::updateInvamtSharedtls() " + e);
			result=false;
		}finally{
			query = null;
			
		}
		
		return result;
	}*/
	
	
	
	
	
	@Transactional
	public boolean insertInvamtShare(List<CustomerInvDtlsDTO>  custinvdtls, String dbUrl, String dbuser, String dbPassword, String dbbatchSize) {
		
		
		String url=dbUrl;
		
       Connection conn;
       
    		   try {
		conn = DriverManager.getConnection(url, dbuser,dbPassword );
	
      
        //query for inserting batch data
        String query = "insert into invamtsharedtls (invno, cafno, prodcode, chargecode, chargeamt,srvctax,lmoshareamt,msoshareamt,apsflshareamt,invfromdate,invtodate,lmocode,mspcode,invyr,invmn,custinvno)values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        PreparedStatement pStatement = conn.prepareStatement(query);
        int batchSize = Integer.parseInt(dbbatchSize);
      
        long startTime = System.currentTimeMillis();
        int count = 0;
        for (CustomerInvDtlsDTO custinv: custinvdtls) {
         
  
        	pStatement.setString(1,custinv.getInvno() );
			pStatement.setString(2,custinv.getAcctcafno() );
			pStatement.setString(3, custinv.getProdcode());
			pStatement.setString(4, custinv.getChargecode());
			pStatement.setString(5,custinv.getChargeamt() );
			pStatement.setString(6,custinv.getSrvctax());
			pStatement.setString(7,custinv.getLmoInvshare());
			pStatement.setString(8,custinv.getMsoInvshare() );
			pStatement.setString(9,custinv.getApsflInvshare());
			pStatement.setString(10,custinv.getInvfromdate() );
			pStatement.setString(11,custinv.getInvtodate() );
			pStatement.setString(12,custinv.getLmocode() );
			pStatement.setString(13,custinv.getMsocode());
			pStatement.setString(14,custinv.getInvyr() );
			pStatement.setString(15,custinv.getInvmn() );
			pStatement.setString(16,custinv.getCustinvno() );

  
        	
        	count++;
        	
            pStatement.addBatch();
          
            if (count % batchSize == 0) {
            	pStatement.executeBatch();
            }
        }
        
        

      pStatement.executeBatch() ; //for remaining batch queries if total record is odd no.
      
     // conn.commit();
        pStatement.close();
        LOGGER.info(" total records updated = " + count);
        conn.close();
        long endTime = System.currentTimeMillis();
        long elapsedTime = (endTime - startTime)/1000; //in secs
        LOGGER.info("elpsed time: "+elapsedTime);
	 
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
        
		return false;
        
	}
	

	@Transactional
	public boolean insertInvamtShareSummary(CustomerInvDtlsDTO custinvdtls) {
		String qry = null;
		Query query = null;
		
		boolean result;
		
		try {
			LOGGER.info("START::insertInvamtShareSummary()");
		
		qry ="insert into rev_summarytable (lmocode, msocode, invyr, invmn,apsflinv_share,msoinv_share,lmoinv_share,lmo_total_collected)values (?,?,?,?,?,?,?,?) ";
        	query = getEntityManager().createNativeQuery(qry);
			query.setParameter(1,custinvdtls.getLmocode());	
			query.setParameter(2, custinvdtls.getMsocode());
			query.setParameter(3, custinvdtls.getInvyr());
			query.setParameter(4,custinvdtls.getInvmn());
			query.setParameter(5,custinvdtls.getApsflmonthlyshare());
			query.setParameter(6,custinvdtls.getMsomonthlyshare());
			query.setParameter(7,custinvdtls.getLmomonthlyshare());
			query.setParameter(8,custinvdtls.getTotalcollected());
			
			
        	query.executeUpdate();
        	result=true;
        	
        	LOGGER.info("END::insertInvamtShareSummary()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::insertInvamtShareSummary() " + e);
			result=false;
		}finally{
			query = null;
			
		}
		
		return result;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getMonthWiseShares(String invyr, String invmn) {
		
		Query query = null;
		String whereClause = "";
		StringBuilder builder =null;
		List<Object[]> list = new ArrayList<>();

		try {
			if(invyr != null && !invyr.isEmpty() && invmn != null && !invmn.isEmpty()){
				whereClause =  " where invyr ='"+invyr+"' and invmn= '"+invmn+"' ";
				
			}
			
			builder = new StringBuilder(" select lmocode,msocode,lmoinv_share,msoinv_share,apsflinv_share,invyr,invmn,lmo_total_collected from rev_summarytable " +whereClause);
			LOGGER.info("builder QUery " + builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			list =  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			query = null;
		}
		return list;
		
	}
	
	
	
@Transactional
	public void setPaidFlagForOverPaidCustomer(String invyr, String invmn) {
		Query query1 = null;
		String whereClause="";
		if(invyr != null && !invyr.isEmpty() && invmn != null && !invmn.isEmpty())
			whereClause =  " and invyr ='"+invyr+"' and invmn= '"+invmn+"' ";
		StringBuilder builder1 = new StringBuilder("Update  custinv set prevpaidamt=1 where invamt+srvctax+prevbal-prevpymtreceived<0 "+whereClause);
		try {
			
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			query1.executeUpdate();
			
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		} 
		
	}


		 
	
	
}
