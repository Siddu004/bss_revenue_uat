/**
 * 
 */
package com.arbiva.apfgc.tenant.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Arbiva
 *
 */

public class UserMenuItemsRestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("User_Object")
	private UsersDTO usersDTOobj;
	
	@JsonProperty("Menu_Item_List")
	private List<MenuItemsDTO> menuItemList;

	public UsersDTO getUsersDTOobj() {
		return usersDTOobj;
	}

	public void setUsersDTOobj(UsersDTO usersDTOobj) {
		this.usersDTOobj = usersDTOobj;
	}

	public List<MenuItemsDTO> getMenuItemList() {
		return menuItemList;
	}

	public void setMenuItemList(List<MenuItemsDTO> menuItemList) {
		this.menuItemList = menuItemList;
	}

	

	
}
