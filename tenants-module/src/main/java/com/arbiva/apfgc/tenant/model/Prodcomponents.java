package com.arbiva.apfgc.tenant.model;

import java.text.ParseException;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "prodcomponents")
public class Prodcomponents extends Base {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProdcomponentsPK id;

	public Prodcomponents() {

	}

	/**
	 * @param prodcomponentsPK
	 * @param service
	 * @param product
	 * @param tenants
	 * @param request
	 * @param productInfoVo
	 * @throws ParseException
	 */
	public Prodcomponents(ProdcomponentsPK prodcomponentsPK, Products product, HttpServletRequest request)
			throws ParseException {
		this.setId(prodcomponentsPK);
		this.setProducts(product);
		this.setCreatedipaddr(request.getRemoteAddr());
		this.setCreatedon(Calendar.getInstance());
		this.setModifiedipaddr(request.getRemoteAddr());
		this.setModifiedon(Calendar.getInstance());
		this.setStatus(new Short("1"));
		this.setLockInDays(product.getLockInDays());
	}

	public Prodcomponents(ProdcomponentsPK prodComppk, MspChnlsStg mspChnlsStg, HttpServletRequest httpServletRequest) {
		this.setCoreSrvcCode("IPTV");
		this.setStatus((short) 1);
		this.setCreatedon(mspChnlsStg.getCreatedon());
		this.setCreatedby(mspChnlsStg.getMspcode());
		this.setCreatedipaddr(httpServletRequest.getRemoteAddr());
		this.setModifiedon(Calendar.getInstance());
		this.setId(prodComppk);
	}

	public ProdcomponentsPK getId() {
		return this.id;
	}

	public void setId(ProdcomponentsPK id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "tenantcode", referencedColumnName = "tenantcode", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "prodcode", referencedColumnName = "prodcode", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "effectivefrom", referencedColumnName = "effectivefrom", nullable = false, insertable = false, updatable = false) })
	private Products products;

	@Column(name = "coresrvccode")
	@NotNull
	private String coreSrvcCode;

	@Column(name = "lockindays")
	private Integer lockInDays;

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}

	public String getCoreSrvcCode() {
		return coreSrvcCode;
	}

	public void setCoreSrvcCode(String coreSrvcCode) {
		this.coreSrvcCode = coreSrvcCode;
	}

	public Integer getLockInDays() {
		return lockInDays;
	}

	public void setLockInDays(Integer lockInDays) {
		this.lockInDays = lockInDays;
	}
}
