/*package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantLicenses;
import com.arbiva.apfgc.tenant.vo.TenantVO;

public interface TenantLicensesService {
	
	public abstract void deleteTenantLicenses(TenantLicenses tenantLicenses);
	
	public abstract TenantLicenses findByTenantCode(String tenantCode);

	public abstract List<TenantLicenses> findAllTenantLicenses();

	public abstract void saveTenantLicenses(TenantVO tenantVO, Tenant tenant, Integer tenantId);

	public abstract TenantLicenses updateTenantLicenses(TenantLicenses tenantLicenses);

}
*/