package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.dao.CpeCharAgmtDao;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;

@Repository
public class CpeCharAgmtDaoImpl implements CpeCharAgmtDao {
	
	private static final Logger LOGGER = Logger.getLogger(CpeCharAgmtDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RevenueSharingTemplateMaster> getAllCpeTempl() {
		 List<RevenueSharingTemplateMaster> list = new ArrayList<RevenueSharingTemplateMaster>();
		 StringBuilder builder = new StringBuilder();
		 Query query = null;
		 try {
			 LOGGER.info("CpeCharAgmtDaoImpl :: getAllCpeTempl() :: START");
			 builder.append(" select * from rstmplmst where rstmpltypelov = 'CPE' ");
			 query = getEntityManager().createNativeQuery(builder.toString(), RevenueSharingTemplateMaster.class);
			 list = query.getResultList();
			LOGGER.info("CpeCharAgmtDaoImpl :: getAllCpeTempl() :: END");
			} catch (Exception e) {
				LOGGER.error("CpeCharAgmtDaoImpl :: getAllCpeTempl() :: " + e);
			} finally {
				query = null;
				builder = null;
				
			}
		  return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getChargeCodesByTemplate(String tmplCode) {
		 List<String> list = new ArrayList<String>();
		 StringBuilder builder = new StringBuilder();
		 Query query = null;
		 try {
				 LOGGER.info("CpeCharAgmtDaoImpl :: getChargeCodesByTemplate() :: START");
				   builder.append(" SELECT chargename FROM chrgrsdtl cad, chargecodes cc where cad.chargecode = cc.chargecode and rstmplcode = :tmplCode ");
				  query = getEntityManager().createNativeQuery(builder.toString());
				query.setParameter("tmplCode", tmplCode);
				list = query.getResultList();
				 LOGGER.info("CpeCharAgmtDaoImpl :: getChargeCodesByTemplate() :: END");
			} catch (Exception e) {
				 LOGGER.error("CpeCharAgmtDaoImpl :: getChargeCodesByTemplate() :: "+ e);
			} finally {
				query = null;
				builder = null;
			}
		  return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllCpeChargeArgmtsByTenant(String tanantCode) {
		 List<Object[]> list = new ArrayList<Object[]>();
		  StringBuilder builder = new StringBuilder();
		  Query query = null;
			try {
				 LOGGER.info("CpeCharAgmtDaoImpl :: findAllCpeChargeArgmtsByTenant() :: START");
				 builder.append("select ca.agruniqueid,ca.agrfdate,t.tenantname,t.tenanttypelov,ca.rstmplcode ");
				  builder.append(" from chrgrsagr ca,rsagrpartners rp,tenants t,rstmplmst rsm ");
				  builder.append(" where rp.agruniqueid = ca.agruniqueid");
				  builder.append(" and t.tenantcode = rp.partnercode ");
				  builder.append(" and ca.rstmplcode = rsm.rstmplcode ");
				  builder.append(" and rsm.rstmpltypelov = :tmplType ");
				  //builder.append(" and rp.partnercode = :tanantCode ");
				  query = getEntityManager().createNativeQuery(builder.toString());
				//query.setParameter("tanantCode", tanantCode);
				query.setParameter("tmplType", "CPE");
				list = query.getResultList();
				 LOGGER.info("CpeCharAgmtDaoImpl :: findAllCpeChargeArgmtsByTenant() :: END");
			} catch (Exception e) {
				LOGGER.error("CpeCharAgmtDaoImpl ::findAllCpeChargeArgmtsByTenant() " + e);
			} finally {
				query = null;
				builder = null;
			}
		  return list;
	}

}
