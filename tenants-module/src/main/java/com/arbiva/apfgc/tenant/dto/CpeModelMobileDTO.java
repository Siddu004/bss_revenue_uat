package com.arbiva.apfgc.tenant.dto;

public class CpeModelMobileDTO {
	
	private String cpeModel;
	private String cpeName;
	private String cpePrice;
	private String profileId;
	
	
	
	
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getCpeModel() {
		return cpeModel;
	}
	public void setCpeModel(String cpeModel) {
		this.cpeModel = cpeModel;
	}
	public String getCpeName() {
		return cpeName;
	}
	public void setCpeName(String cpeName) {
		this.cpeName = cpeName;
	}
	public String getCpePrice() {
		return cpePrice;
	}
	public void setCpePrice(String cpePrice) {
		this.cpePrice = cpePrice;
	}
	

}
