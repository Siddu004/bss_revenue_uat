/**
 * 
 */
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="oltportdtls")
@IdClass(OLTPortDetailsPK.class)
public class OLTPortDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Transient
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public OLTPortDetails()
	{
		
	}
	
	@Id
	@Column(name="pop_olt_serialno")
	private String popOltSerialno;
	
	@Id
	@Column(name="cardid")
	private int cardId;
	
	@Id
	@Column(name="portno")
	private Integer portNo;
	
	@Column(name="lmocode")
	private String lmocode;
	
	@Column(name="slots")
	private String slots;
	
	@Column(name="l1slots")
	private String l1Slots;
	
	@Column(name="l3slotsavlbl")
	private String l3SlotsAvlbl;
	
	@Column(name="l3slotsused")
	private String l3Slotsused;
	
	@Column(name="createdby")
	private String createdBy;
	

	@Column(name="modifiedby")
	private String modifiedBy;
	

	@Column(name="modifiedon")
	private Timestamp modifiedOn;
	
	@Column(name="createdon")
	private Timestamp createdOn;
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
	public String getCreatedOn() {
		if (createdOn!=null)
		return createdOn.toString();
		else
			return null;
	}

	public void setCreatedOn(String createdOn) {
		 Date parsedDate = null;
		 if (createdOn !=null){ 
				if(	createdOn!=""){
		try {
			parsedDate = df.parse(createdOn);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.createdOn = new java.sql.Timestamp(parsedDate.getTime());
		 }
		 }
	}

	public void setModifiedOn(String modifiedOn) {
		if (modifiedOn !=null){ 
			if(	modifiedOn!=""){
		 Date parsedDate = null;
		try {
			parsedDate = df.parse(modifiedOn);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.modifiedOn = new java.sql.Timestamp(parsedDate.getTime());
		}
		}
	}
	
	public String getModifiedOn() {
		if (modifiedOn!=null)
		return modifiedOn.toString();
		else
			return null;
	}

	public String getL1Slots() {
		return l1Slots;
	}

	public void setL1Slots(String l1Slots) {
		this.l1Slots = l1Slots;
	}

	public String getL3SlotsAvlbl() {
		return l3SlotsAvlbl;
	}

	public void setL3SlotsAvlbl(String l3SlotsAvlbl) {
		this.l3SlotsAvlbl = l3SlotsAvlbl;
	}

	public String getL3Slotsused() {
		return l3Slotsused;
	}

	public void setL3Slotsused(String l3Slotsused) {
		this.l3Slotsused = l3Slotsused;
	}

	public String getPopOltSerialno() {
		return popOltSerialno;
	}

	public void setPopOltSerialno(String popOltSerialno) {
		this.popOltSerialno = popOltSerialno;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Integer getPortNo() {
		return portNo;
	}

	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}

	public String getLmocode() {
		return lmocode;
	}

	public void setLmocode(String lmocode) {
		this.lmocode = lmocode;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}


	
}
