/**
 * 
 */
package com.arbiva.apfgc.tenant.pg.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="customermst")
public class Customer extends Base1 {
	
	private static final Logger logger = Logger.getLogger(Customer.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name = "custid")
	private Long custId;

	@Column(name = "custcode")
	private String custCode;
	
	@Column(name = "custuid")
	private String custUid;
	
	@Column(name = "custtypelov")
	private String custTypeLov;
	
	@Column(name = "enttypelov")
	private String custType;

	@Column(name = "officetypelov")
	private String officeTypeLov;

	@Column(name = "officeloc")
	private String officeLocation;

	@Column(name = "parentcustcode")
	private String parentcustcode;
	
	@Column(name = "regncode")
	private String regnCode;
	
	@Column(name = "panno")
	private String panNo;

	@Column(name = "tanno")
	private String tanNo;
	
	@Column(name = "aadharno")
	private String aadharNo;
	
	@Column(name = "titlelov")
	private String titleLov;
	
	@Column(name = "custname")
	private String custName;
	
	@Column(name = "mname")
	private String mName;
	
	@Column(name = "lname")
	private String lName;
	
	@Column(name = "fhname")
	private String fhName;
	
	@Column(name = "gender")
	private char gender;
	
	@Column(name = "pmntliabilityflag")
	private int pmntliabilityflag;

	@Column(name = "addr1")
	private String address1;

	@Column(name = "addr2")
	private String address2;

	@Column(name = "locality")
	private String locality;

	@Column(name = "area")
	private String area;
	
	@Column(name = "mandal")
	private String mandal;
	
	@Column(name = "district")
	private String district;

	@Column(name = "city_village")
	private String cityVillage;

	@Column(name = "state")
	private String state;

	@Column(name = "pin")
	private String pin;

	@Column(name = "stdcode")
	private String stdCode;

	@Column(name = "landline1")
	private String landLine1;

	@Column(name = "landline2")
	private String landLine2;

	@Column(name = "email1")
	private String email1;

	@Column(name = "email2")
	private String email2;

	@Column(name = "fax1")
	private String fax1;

	@Column(name = "fax2")
	private String fax2;

	@Column(name = "pocname")
	private String pocName;

	@Column(name = "pocmob1")
	private String pocMob1;

	@Column(name = "pocmob2")
	private String pocMob2;
	
	@Column(name = "bladdr1")
	private String blAddress1;

	@Column(name = "bladdr2")
	private String blAddress2;

	@Column(name = "bllocality")
	private String blLocality;

	@Column(name = "blarea")
	private String blArea;
	
	@Column(name = "blmandal")
	private String blMandal;
	
	@Column(name = "bldistrict")
	private String blDistrict;
	
	@Column(name = "custdistuid")
	private Integer custdistUid;
	
	@Column(name = "blcity_village")
	private String blCityVillage;

	@Column(name = "blstate")
	private String blState;

	@Column(name = "blpin")
	private String blPin;

	@Column(name = "blstdcode")
	private String blStdCode;

	@Column(name = "bllandline1")
	private String blLandLine1;

	@Column(name = "bllandline2")
	private String blLandLine2;

	@Column(name = "blemail1")
	private String blEmail1;

	@Column(name = "blemail2")
	private String blEmail2;

	@Column(name = "blfax1")
	private String blFax1;

	@Column(name = "blfax2")
	private String blFax2;

	@Column(name = "blpocname")
	private String blPocName;

	@Column(name = "blpocmob1")
	private String blPocMob1;

	@Column(name = "blpocmob2")
	private String blPocMob2;

	@Column(name = "billfreqlov")
	private String billfreqLov;
	
	@Column(name = "actdate")
	private Date actDate;
	
	@Column(name = "lmocode")
	private String lmoCode;
	
	@Column(name = "channellov")
	private String channelLov;
	
	@Column(name = "segmentlov")
	private String segmentLov;
	
	@Column(name = "dateofinc")
	private Date dateofinc;
	
	@Column(name = "blacklistflag")
	private char blacklistFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DEACTIVATEDON")
	private Calendar deActivatedOn;
	
	@Column(name = "DEACTIVATEDBY")
	private String deActivatedBy;
	
	@Column(name = "DEACTIVATEDIPADDR")
	private String deActivatedIpAddr;
	
	@Transient
	private String pocDesignation;
	
	@Transient
	private String dob;

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustUid() {
		return custUid;
	}

	public void setCustUid(String custUid) {
		this.custUid = custUid;
	}

	public String getCustTypeLov() {
		return custTypeLov;
	}

	public void setCustTypeLov(String custTypeLov) {
		this.custTypeLov = custTypeLov;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getOfficeTypeLov() {
		return officeTypeLov;
	}

	public void setOfficeTypeLov(String officeTypeLov) {
		this.officeTypeLov = officeTypeLov;
	}

	public String getOfficeLocation() {
		return officeLocation;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	public String getParentcustcode() {
		return parentcustcode;
	}

	public void setParentcustcode(String parentcustcode) {
		this.parentcustcode = parentcustcode;
	}

	public String getRegnCode() {
		return regnCode;
	}

	public void setRegnCode(String regnCode) {
		this.regnCode = regnCode;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getTanNo() {
		return tanNo;
	}

	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getTitleLov() {
		return titleLov;
	}

	public void setTitleLov(String titleLov) {
		this.titleLov = titleLov;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getFhName() {
		return fhName;
	}

	public void setFhName(String fhName) {
		this.fhName = fhName;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public int getPmntliabilityflag() {
		return pmntliabilityflag;
	}

	public void setPmntliabilityflag(int pmntliabilityflag) {
		this.pmntliabilityflag = pmntliabilityflag;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getMandal() {
		return mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCityVillage() {
		return cityVillage;
	}

	public void setCityVillage(String cityVillage) {
		this.cityVillage = cityVillage;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getLandLine1() {
		return landLine1;
	}

	public void setLandLine1(String landLine1) {
		this.landLine1 = landLine1;
	}

	public String getLandLine2() {
		return landLine2;
	}

	public void setLandLine2(String landLine2) {
		this.landLine2 = landLine2;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getFax1() {
		return fax1;
	}

	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}

	public String getFax2() {
		return fax2;
	}

	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getPocMob1() {
		return pocMob1;
	}

	public void setPocMob1(String pocMob1) {
		this.pocMob1 = pocMob1;
	}

	public String getPocMob2() {
		return pocMob2;
	}

	public void setPocMob2(String pocMob2) {
		this.pocMob2 = pocMob2;
	}

	public String getBlAddress1() {
		return blAddress1;
	}

	public void setBlAddress1(String blAddress1) {
		this.blAddress1 = blAddress1;
	}

	public String getBlAddress2() {
		return blAddress2;
	}

	public void setBlAddress2(String blAddress2) {
		this.blAddress2 = blAddress2;
	}

	public String getBlLocality() {
		return blLocality;
	}

	public void setBlLocality(String blLocality) {
		this.blLocality = blLocality;
	}

	public String getBlArea() {
		return blArea;
	}

	public void setBlArea(String blArea) {
		this.blArea = blArea;
	}

	public String getBlMandal() {
		return blMandal;
	}

	public void setBlMandal(String blMandal) {
		this.blMandal = blMandal;
	}

	public String getBlDistrict() {
		return blDistrict;
	}

	public void setBlDistrict(String blDistrict) {
		this.blDistrict = blDistrict;
	}
	
	public Integer getCustdistUid() {
		return custdistUid;
	}

	public void setCustdistUid(Integer custdistUid) {
		this.custdistUid = custdistUid;
	}

	public String getBlCityVillage() {
		return blCityVillage;
	}

	public void setBlCityVillage(String blCityVillage) {
		this.blCityVillage = blCityVillage;
	}

	public String getBlState() {
		return blState;
	}

	public void setBlState(String blState) {
		this.blState = blState;
	}

	public String getBlPin() {
		return blPin;
	}

	public void setBlPin(String blPin) {
		this.blPin = blPin;
	}

	public String getBlStdCode() {
		return blStdCode;
	}

	public void setBlStdCode(String blStdCode) {
		this.blStdCode = blStdCode;
	}

	public String getBlLandLine1() {
		return blLandLine1;
	}

	public void setBlLandLine1(String blLandLine1) {
		this.blLandLine1 = blLandLine1;
	}

	public String getBlLandLine2() {
		return blLandLine2;
	}

	public void setBlLandLine2(String blLandLine2) {
		this.blLandLine2 = blLandLine2;
	}

	public String getBlEmail1() {
		return blEmail1;
	}

	public void setBlEmail1(String blEmail1) {
		this.blEmail1 = blEmail1;
	}

	public String getBlEmail2() {
		return blEmail2;
	}

	public void setBlEmail2(String blEmail2) {
		this.blEmail2 = blEmail2;
	}

	public String getBlFax1() {
		return blFax1;
	}

	public void setBlFax1(String blFax1) {
		this.blFax1 = blFax1;
	}

	public String getBlFax2() {
		return blFax2;
	}

	public void setBlFax2(String blFax2) {
		this.blFax2 = blFax2;
	}

	public String getBlPocName() {
		return blPocName;
	}

	public void setBlPocName(String blPocName) {
		this.blPocName = blPocName;
	}

	public String getBlPocMob1() {
		return blPocMob1;
	}

	public void setBlPocMob1(String blPocMob1) {
		this.blPocMob1 = blPocMob1;
	}

	public String getBlPocMob2() {
		return blPocMob2;
	}

	public void setBlPocMob2(String blPocMob2) {
		this.blPocMob2 = blPocMob2;
	}

	public String getBillfreqLov() {
		return billfreqLov;
	}

	public void setBillfreqLov(String billfreqLov) {
		this.billfreqLov = billfreqLov;
	}

	public Date getActDate() {
		return actDate;
	}

	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public String getChannelLov() {
		return channelLov;
	}

	public void setChannelLov(String channelLov) {
		this.channelLov = channelLov;
	}

	public String getSegmentLov() {
		return segmentLov;
	}

	public void setSegmentLov(String segmentLov) {
		this.segmentLov = segmentLov;
	}

	public Date getDateofinc() {
		return dateofinc;
	}

	public void setDateofinc(Date dateofinc) {
		this.dateofinc = dateofinc;
	}

	public char getBlacklistFlag() {
		return blacklistFlag;
	}

	public void setBlacklistFlag(char blacklistFlag) {
		this.blacklistFlag = blacklistFlag;
	}

	public Calendar getDeActivatedOn() {
		return deActivatedOn;
	}

	public void setDeActivatedOn(Calendar deActivatedOn) {
		this.deActivatedOn = deActivatedOn;
	}

	public String getDeActivatedBy() {
		return deActivatedBy;
	}

	public void setDeActivatedBy(String deActivatedBy) {
		this.deActivatedBy = deActivatedBy;
	}

	public String getDeActivatedIpAddr() {
		return deActivatedIpAddr;
	}

	public void setDeActivatedIpAddr(String deActivatedIpAddr) {
		this.deActivatedIpAddr = deActivatedIpAddr;
	}

	public String getPocDesignation() {
		return pocDesignation;
	}

	public void setPocDesignation(String pocDesignation) {
		this.pocDesignation = pocDesignation;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
