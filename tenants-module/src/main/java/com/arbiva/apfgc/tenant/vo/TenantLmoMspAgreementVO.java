package com.arbiva.apfgc.tenant.vo;

import org.springframework.web.multipart.MultipartFile;

public class TenantLmoMspAgreementVO {
	
	private int tenantId;
	
	private String mspCode;
	
	private String lmoCode;
	
	private String agreementFrom;
	
	private String agreementTo;
	
	private MultipartFile agreementCopy;
	
	private String agreementDate;
	
	private Float depositAmount;
	
	private String depositMode;
	
	private String depositReferenceNo;
	
	private Float lmoWalletAmount;
	
	private String region;
	
	private Character regionType;
	
	private String coreserviceCode;
	
	private String reasons;	
	
	private String[] coreServiceMulti;

	public String[] getCoreServiceMulti() {
		return coreServiceMulti;
	}

	public void setCoreServiceMulti(String[] coreServiceMulti) {
		this.coreServiceMulti = coreServiceMulti;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getMspCode() {
		return mspCode;
	}

	public Character getRegionType() {
		return regionType;
	}

	public void setRegionType(Character regionType) {
		this.regionType = regionType;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getAgreementFrom() {
		return agreementFrom;
	}

	public void setAgreementFrom(String agreementFrom) {
		this.agreementFrom = agreementFrom;
	}

	public String getAgreementTo() {
		return agreementTo;
	}

	public void setAgreementTo(String agreementTo) {
		this.agreementTo = agreementTo;
	}

	public MultipartFile getAgreementCopy() {
		return agreementCopy;
	}

	public void setAgreementCopy(MultipartFile agreementCopy) {
		this.agreementCopy = agreementCopy;
	}

	public String getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}

	public Float getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Float depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getDepositMode() {
		return depositMode;
	}

	public void setDepositMode(String depositMode) {
		this.depositMode = depositMode;
	}

	public String getDepositReferenceNo() {
		return depositReferenceNo;
	}

	public void setDepositReferenceNo(String depositReferenceNo) {
		this.depositReferenceNo = depositReferenceNo;
	}

	public Float getLmoWalletAmount() {
		return lmoWalletAmount;
	}

	public void setLmoWalletAmount(Float lmoWalletAmount) {
		this.lmoWalletAmount = lmoWalletAmount;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCoreserviceCode() {
		return coreserviceCode;
	}

	public void setCoreserviceCode(String coreserviceCode) {
		this.coreserviceCode = coreserviceCode;
	}
	
}
