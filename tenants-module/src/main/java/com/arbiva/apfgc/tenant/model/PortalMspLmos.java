package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="portal_msp_lmos")
public class PortalMspLmos {

	@Id
	@Column(name="mso_lmo_id")
	private String mso_lmo_id;
	
	@Column(name="mso_lmo_id_s")
	private String mso_lmo_id_s;
	
	@Column(name="lmo_area_of_operation")
	private String lmo_area_of_operation;
	
	@Column(name="lmo_contact_person")
	private String lmo_contact_person;
	
	@Column(name="lmo_district_name")
	private String lmo_district_name;
	
	@Column(name="lmo_district_name_id")
	private String lmo_district_name_id;
	
	@Column(name="lmo_habitation")
	private String lmo_habitation;
	
	@Column(name="lmo_mandal_name")
	private String lmo_mandal_name;
	
	@Column(name="lmo_mandal_name_id")
	private String lmo_mandal_name_id;
	
	@Column(name="lmo_name")
	private String lmo_name;
	
	@Column(name="lmo_no_of_connections")
	private String lmo_no_of_connections;
	
	@Column(name="lmo_no_of_subscriptions")
	private String lmo_no_of_subscriptions;
	
	@Column(name="lmo_phone_number")
	private String lmo_phone_number;
	
	@Column(name="lmo_state")
	private String lmo_state;
	
	@Column(name="lmo_state_id")
	private String lmo_state_id;
	
	@Column(name="lmo_street")
	private String lmo_street;
	
	@Column(name="lmo_village_name")
	private String lmo_village_name;
	
	@Column(name="lmo_village_name_id")
	private String lmo_village_name_id;
	
	@Column(name="lmo_years_of_service")
	private String lmo_years_of_service;
	
	@Column(name="lmo_zipcode")
	private String lmo_zipcode;
	
	@Column(name="mso_enrollment_number")
	private String mso_enrollment_number;
	
	@Column(name="ref_id")
	private String ref_id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="noemidemandqty")
	private Long noEmiDemandQty;
	
	@Column(name="emi36demandqty")
	private Long emi36DemandQty;
	
	@Column(name="emi48demandqty")
	private Long emi48DemandQty;
	
	@Column(name="demanddate")
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Calendar demanddate;

	public String getMso_lmo_id() {
		return mso_lmo_id;
	}

	public void setMso_lmo_id(String mso_lmo_id) {
		this.mso_lmo_id = mso_lmo_id;
	}

	public String getMso_lmo_id_s() {
		return mso_lmo_id_s;
	}

	public void setMso_lmo_id_s(String mso_lmo_id_s) {
		this.mso_lmo_id_s = mso_lmo_id_s;
	}

	public String getLmo_area_of_operation() {
		return lmo_area_of_operation;
	}

	public void setLmo_area_of_operation(String lmo_area_of_operation) {
		this.lmo_area_of_operation = lmo_area_of_operation;
	}

	public String getLmo_contact_person() {
		return lmo_contact_person;
	}

	public void setLmo_contact_person(String lmo_contact_person) {
		this.lmo_contact_person = lmo_contact_person;
	}

	public String getLmo_district_name() {
		return lmo_district_name;
	}

	public void setLmo_district_name(String lmo_district_name) {
		this.lmo_district_name = lmo_district_name;
	}

	public String getLmo_district_name_id() {
		return lmo_district_name_id;
	}

	public void setLmo_district_name_id(String lmo_district_name_id) {
		this.lmo_district_name_id = lmo_district_name_id;
	}

	public String getLmo_habitation() {
		return lmo_habitation;
	}

	public void setLmo_habitation(String lmo_habitation) {
		this.lmo_habitation = lmo_habitation;
	}

	public String getLmo_mandal_name() {
		return lmo_mandal_name;
	}

	public void setLmo_mandal_name(String lmo_mandal_name) {
		this.lmo_mandal_name = lmo_mandal_name;
	}

	public String getLmo_mandal_name_id() {
		return lmo_mandal_name_id;
	}

	public void setLmo_mandal_name_id(String lmo_mandal_name_id) {
		this.lmo_mandal_name_id = lmo_mandal_name_id;
	}

	public String getLmo_name() {
		return lmo_name;
	}

	public void setLmo_name(String lmo_name) {
		this.lmo_name = lmo_name;
	}

	public String getLmo_no_of_connections() {
		return lmo_no_of_connections;
	}

	public void setLmo_no_of_connections(String lmo_no_of_connections) {
		this.lmo_no_of_connections = lmo_no_of_connections;
	}

	public String getLmo_no_of_subscriptions() {
		return lmo_no_of_subscriptions;
	}

	public void setLmo_no_of_subscriptions(String lmo_no_of_subscriptions) {
		this.lmo_no_of_subscriptions = lmo_no_of_subscriptions;
	}

	public String getLmo_phone_number() {
		return lmo_phone_number;
	}

	public void setLmo_phone_number(String lmo_phone_number) {
		this.lmo_phone_number = lmo_phone_number;
	}

	public String getLmo_state() {
		return lmo_state;
	}

	public void setLmo_state(String lmo_state) {
		this.lmo_state = lmo_state;
	}

	public String getLmo_state_id() {
		return lmo_state_id;
	}

	public void setLmo_state_id(String lmo_state_id) {
		this.lmo_state_id = lmo_state_id;
	}

	public String getLmo_street() {
		return lmo_street;
	}

	public void setLmo_street(String lmo_street) {
		this.lmo_street = lmo_street;
	}

	public String getLmo_village_name() {
		return lmo_village_name;
	}

	public void setLmo_village_name(String lmo_village_name) {
		this.lmo_village_name = lmo_village_name;
	}

	public String getLmo_village_name_id() {
		return lmo_village_name_id;
	}

	public void setLmo_village_name_id(String lmo_village_name_id) {
		this.lmo_village_name_id = lmo_village_name_id;
	}

	public String getLmo_years_of_service() {
		return lmo_years_of_service;
	}

	public void setLmo_years_of_service(String lmo_years_of_service) {
		this.lmo_years_of_service = lmo_years_of_service;
	}

	public String getLmo_zipcode() {
		return lmo_zipcode;
	}

	public void setLmo_zipcode(String lmo_zipcode) {
		this.lmo_zipcode = lmo_zipcode;
	}

	public String getMso_enrollment_number() {
		return mso_enrollment_number;
	}

	public void setMso_enrollment_number(String mso_enrollment_number) {
		this.mso_enrollment_number = mso_enrollment_number;
	}

	public String getRef_id() {
		return ref_id;
	}

	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public Long getNoEmiDemandQty() {
		return noEmiDemandQty;
	}

	public void setNoEmiDemandQty(Long noEmiDemandQty) {
		this.noEmiDemandQty = noEmiDemandQty;
	}

	public Long getEmi36DemandQty() {
		return emi36DemandQty;
	}

	public void setEmi36DemandQty(Long emi36DemandQty) {
		this.emi36DemandQty = emi36DemandQty;
	}

	public Long getEmi48DemandQty() {
		return emi48DemandQty;
	}

	public void setEmi48DemandQty(Long emi48DemandQty) {
		this.emi48DemandQty = emi48DemandQty;
	}

	public Calendar getDemanddate() {
		return demanddate;
	}

	public void setDemanddate(Calendar demanddate) {
		this.demanddate = demanddate;
	}

}
