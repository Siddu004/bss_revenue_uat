package com.arbiva.apfgc.tenant.pg.utils;

/**
 * {@link PaymentGatewayErrorCodes} holds the application specific error codes.
 * 
 * @author srinivasa
 *
 */
public class PaymentGatewayErrorCode {
	
	public enum PaymentGatewayErrorCodes {

		GAE001(400, "Internal Server Error"), 
		GAE002(400, "Invalid empty parameter"), 
		GAE003(400,	"Invalid Information"), 
		GAE004(400,	"Invalid Gateway Type"),
		PGREQ001(400, "Payment Request is not found"),
		PGRES001(400, "Mismatch in checksum info");

		private final int code;
		private final String description;

		private PaymentGatewayErrorCodes(int code, String description) {
			this.code = code;
			this.description = description;
		}

		public int getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

}
