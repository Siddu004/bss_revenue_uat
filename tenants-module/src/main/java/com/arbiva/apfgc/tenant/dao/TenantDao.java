/*package com.arbiva.apfgc.tenant.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.Tenant;;

public interface TenantDao extends JpaSpecificationExecutor<Tenant>, JpaRepository<Tenant, Integer> {

	@Query(value="SELECT * FROM tenants where tenantcode = ?1 ",nativeQuery=true)
	public abstract Tenant findByTenantCode(String tenantCode);
	
	@Query(value="SELECT * FROM tenants where tenanttypelov = 'MSP' ",nativeQuery=true)
	public abstract List<Tenant> findMspCodeByTenantType();
}
*/