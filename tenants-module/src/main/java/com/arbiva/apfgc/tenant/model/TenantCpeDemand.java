package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="tenantcpedmnd")
public class TenantCpeDemand {
	
	@Id 
	@Column(name = "dmndId")
	private Long dmndId;
	
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Column(name = "profile_id")
	private String profileId;
	
	@Column(name = "dmnddate")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date demandDate;
	
	@Column(name = "dmndqty")
	private Long demandQuntity;
	
	@Column(name = "dmndprice")
	private BigDecimal demandPrice;
	
	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    @NotNull
    private Short status;

	@Column(name = "createdon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar createdon;

	@Column(name = "createdby", length = 50)
    private String createdby;

	@Column(name = "createdipaddr", length = 20)
    private String createdipaddr;

	@Column(name = "modifiedon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar modifiedon;

	@Column(name = "modifiedby", length = 50)
    private String modifiedby;

	@Column(name = "modifiedipaddr", length = 20)
    private String modifiedipaddr;
	
	@ManyToOne
	@JoinColumn(name = "tenantcode", referencedColumnName = "tenantcode", nullable = false, insertable = false, updatable = false)
	private Tenant tenant;
	
	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id", nullable = false, insertable = false, updatable = false)
	private CpeProfileMaster cpeProMaster;
	

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Calendar getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Calendar modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedipaddr() {
		return modifiedipaddr;
	}

	public void setModifiedipaddr(String modifiedipaddr) {
		this.modifiedipaddr = modifiedipaddr;
	}

	public Long getDmndId() {
		return dmndId;
	}

	public void setDmndId(Long dmndId) {
		this.dmndId = dmndId;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Date getDemandDate() {
		return demandDate;
	}

	public void setDemandDate(Date demandDate) {
		this.demandDate = demandDate;
	}

	public Long getDemandQuntity() {
		return demandQuntity;
	}

	public void setDemandQuntity(Long demandQuntity) {
		this.demandQuntity = demandQuntity;
	}

	public BigDecimal getDemandPrice() {
		return demandPrice;
	}

	public void setDemandPrice(BigDecimal demandPrice) {
		this.demandPrice = demandPrice;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public CpeProfileMaster getCpeProMaster() {
		return cpeProMaster;
	}

	public void setCpeProMaster(CpeProfileMaster cpeProMaster) {
		this.cpeProMaster = cpeProMaster;
	}
}
