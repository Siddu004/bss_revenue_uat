package com.arbiva.apfgc.tenant.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tenant.dao.RsAgrPartnersDao;
import com.arbiva.apfgc.tenant.dao.RsAgrmntsDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.model.RsAgrPartners;
import com.arbiva.apfgc.tenant.model.RsAgrmnts;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.service.RsAgrmntsService;
import com.arbiva.apfgc.tenant.vo.ProductAgreementPartnersVO;
import com.arbiva.apfgc.tenant.vo.ProductAgreementVO;

@Service
public class RsAgrmntsServiceImpl implements RsAgrmntsService {

	private static final Logger LOGGER = Logger.getLogger(RsAgrmntsServiceImpl.class);

	@Autowired
	RsAgrmntsDao rsAgrmntsDao;

	@Autowired
	RsAgrPartnersDao rsAgrPartnersDao;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	TenantDaoImpl tenantDaoImpl;

	@Override
	@Transactional
	public String saveProductAgreement(List<ProductAgreementVO> productAgreementVOList) {
	
		RsAgrmnts rsAgreement = null;
		String returnValue = "";
		Long agreementNo;

		try {
			  agreementNo = storedProcedureDAO.executeStoredProcedure("PACKAGEAGREEMENTID");
			
			/*for(ProductAgreementVO  productAgreementVO : productAgreementVOList ){
				List<String> partnerCodesList = new ArrayList<String>();
				List<ProductAgreementPartnersVO> productAgreementPartnerslist = productAgreementVO.getPartnrsList();
				for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
					partnerCodesList.add(productAgreementPartnersVO.getPartnercode());
				}
				Collections.sort(partnerCodesList);
				rsAgreement = 	rsAgrmntsDao.findByPartnerList(partnerCodesList.toString(),productAgreementVO.getProdCode(),productAgreementVO.getTenantCode(), DateUtill.getCurrentDateInYYYYMMDD());

				if(rsAgreement == null){
					rsAgreement = new RsAgrmnts(productAgreementVO);
					
					rsAgreement.setAgruniqueid(agreementNo);
					rsAgreement.setPartnerList(partnerCodesList.toString());
					rsAgreement = rsAgrmntsDao.save(rsAgreement);
					LOGGER.info("Revenue Sharing Agreement Is Created .....");

				
					for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
						RsAgrPartners rsAgreementPartners = new RsAgrPartners(productAgreementPartnersVO, rsAgreement);
						rsAgrPartnersDao.save(rsAgreementPartners);
						LOGGER.info("Revenue Sharing Agreement Partners Is Created .....");
					}

					returnValue = "Success";
				}else{
					returnValue = productAgreementVO.getProdName();
					throw new Exception();
				}
			}*/
			  for(ProductAgreementVO  productAgreementVO : productAgreementVOList ){
				  LOGGER.info(productAgreementVO.getFlag());
				  LOGGER.info(productAgreementVO.getProdtype());
				  if(productAgreementVO.getFlag().equalsIgnoreCase("SR")){
					  
					  List<String> apsflList = new ArrayList<String>();
					  List<String> lmoList = new ArrayList<String>();
						List<ProductAgreementPartnersVO> productAgreementPartnerslist = productAgreementVO.getPartnrsList();
						for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
							String tenantType=productAgreementPartnersVO.getPartnertype();
							if(tenantType.equalsIgnoreCase("APSFL")){
								List<Tenant> tanantsList = tenantDaoImpl.findAllByTenantType(tenantType);
								for(Tenant tenant:tanantsList){
									apsflList.add(tenant.getTenantCode());
								}
							}
							if(tenantType.equalsIgnoreCase("LMO")){
								List<Tenant> tanantsList = tenantDaoImpl.findAllByTenantType(tenantType);
								for(Tenant tenant:tanantsList){
									lmoList.add(tenant.getTenantCode());
								}
							}
						}
						for(String apsflCode:apsflList){
							for(String lmoCode:lmoList){
								List<String> partnerCodesList = new ArrayList<String>();
								partnerCodesList.add(apsflCode);
								partnerCodesList.add(lmoCode);
								Long newagreemntNo = storedProcedureDAO.executeStoredProcedure("PACKAGEAGREEMENTID");
								Collections.sort(partnerCodesList);
								RsAgrmnts newrsAgreement = 	rsAgrmntsDao.findByPartnerList(partnerCodesList.toString(),productAgreementVO.getProdCode(),productAgreementVO.getTenantCode(), DateUtill.getCurrentDateInYYYYMMDD());
								
								if(newrsAgreement == null){
									newrsAgreement = new RsAgrmnts(productAgreementVO);
									newrsAgreement.setAgruniqueid(newagreemntNo);
									newrsAgreement.setPartnerList(partnerCodesList.toString());
									newrsAgreement = rsAgrmntsDao.save(newrsAgreement);
									LOGGER.info("Revenue Sharing Agreement Is Created .....");
									Short i=1;
									for (String partnrCode : partnerCodesList) {
										Tenant tenant=tenantDaoImpl.findByTenantCode(partnrCode);
										RsAgrPartners rsAgreementPartners = new RsAgrPartners();
										rsAgreementPartners.setAgrUniqueId(newrsAgreement.getAgruniqueid());
										rsAgreementPartners.setPartnerCode(partnrCode);
										rsAgreementPartners.setPartnerSlNo(i);
										rsAgreementPartners.setPartnerType(tenant.getTenantTypeLov());
										rsAgreementPartners.setStatus(new Short("1"));
										rsAgrPartnersDao.save(rsAgreementPartners);
										i++;
										LOGGER.info("Revenue Sharing Agreement Partners Is Created .....");
									}
									returnValue = "Success";
								}
							}
						}
							
				  }else{
					  List<String> partnerCodesList = new ArrayList<String>();
						List<ProductAgreementPartnersVO> productAgreementPartnerslist = productAgreementVO.getPartnrsList();
						for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
							partnerCodesList.add(productAgreementPartnersVO.getPartnercode());
						}
						Collections.sort(partnerCodesList);
						rsAgreement = 	rsAgrmntsDao.findByPartnerList(partnerCodesList.toString(),productAgreementVO.getProdCode(),productAgreementVO.getTenantCode(), DateUtill.getCurrentDateInYYYYMMDD());

						if(rsAgreement == null){
							rsAgreement = new RsAgrmnts(productAgreementVO);
							
							rsAgreement.setAgruniqueid(agreementNo);
							rsAgreement.setPartnerList(partnerCodesList.toString());
							rsAgreement = rsAgrmntsDao.save(rsAgreement);
							LOGGER.info("Revenue Sharing Agreement Is Created .....");

						
							for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
								RsAgrPartners rsAgreementPartners = new RsAgrPartners(productAgreementPartnersVO, rsAgreement);
								rsAgrPartnersDao.save(rsAgreementPartners);
								LOGGER.info("Revenue Sharing Agreement Partners Is Created .....");
							}

							returnValue = "Success";
						}else{
							returnValue = productAgreementVO.getProdName();
							throw new Exception();
						}
				  }
					
				}
			  
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		} finally {
			rsAgreement = null;
			agreementNo = null;
		}
		
		return returnValue;

	}

	@Override
	public List<ProductAgreementVO> viewAllPkgAgree(String tenantcode, String tenantType) {		
		
		List<Object[]> agrmntList = rsAgrPartnersDao.viewAllPkgAgree(tenantcode,tenantType);
		ProductAgreementVO agrmntVO ;

		List<ProductAgreementVO> agrmntVOList = new ArrayList<ProductAgreementVO>();
		for (Object[] object : agrmntList) {
			try {
				  agrmntVO = new ProductAgreementVO();
				agrmntVO.setAgreementId(object[0] == null ? "" : object[0].toString());
				agrmntVO.setRstmlCode(object[1] == null ? "" : object[1].toString());
				agrmntVO.setTenantCode(object[0] == null ? "" : object[2].toString());
				agrmntVO.setProdCode(object[3] == null ? "" : object[3].toString());
				agrmntVO.setProdName(object[4] == null ? "" : object[4].toString());
				agrmntVO.setProdtype(object[5] == null ? "" : object[5].toString());
				agrmntVO.setTenantName(object[6] == null ? "" : object[6].toString());
				agrmntVO.setLmoName(object[7] == null ? "" : object[7].toString());
				agrmntVO.setEffetiveFrom(object[8] == null ? "" : DateUtill.dateToString((Date) object[8]));
				agrmntVO.setCreatedBy(object[9] == null ? "" : object[9].toString());
				agrmntVO.setLockindays(object[10] == null ? "" : object[10].toString());
				agrmntVO.setProdCharge(object[11] == null ? "" : object[11].toString());
				agrmntVO.setTaxCompenent(object[12] == null ? "" : object[12].toString());
				agrmntVO.setDurationDays(object[13] == null ? "" : object[13].toString());
				agrmntVO.setCusttypelov(object[14] == null ? "" : object[14].toString());
				agrmntVO.setTotalCharge(new BigDecimal(agrmntVO.getProdCharge()).add(new BigDecimal(agrmntVO.getTaxCompenent())));
				
				if (agrmntVO.getProdtype().toString().equalsIgnoreCase("B"))
					agrmntVO.setProdtype("Bundle");
				else if (agrmntVO.getProdtype().toString().equalsIgnoreCase("O"))
					agrmntVO.setProdtype("One Time");
				else if (agrmntVO.getProdtype().toString().equalsIgnoreCase("A"))
					agrmntVO.setProdtype("Add On");
				agrmntVOList.add(agrmntVO);
			} catch (Exception ex) {

				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				agrmntList = null;
				agrmntVO = null;
			}
		}
		return agrmntVOList;
	}

}
