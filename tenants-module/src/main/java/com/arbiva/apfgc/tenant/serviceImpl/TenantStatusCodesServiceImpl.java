/*package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tenant.dao.TenantStatusCodesDao;
import com.arbiva.apfgc.tenant.model.TenantStatusCodes;
import com.arbiva.apfgc.tenant.service.TenantStatusCodesService;

@Service
@Transactional
public class TenantStatusCodesServiceImpl implements TenantStatusCodesService {

	@Autowired
	TenantStatusCodesDao tenantStatusCodesDao;

	@Override
	public void deleteTenantStatusCodes(TenantStatusCodes tenantStatusCodes) {
		tenantStatusCodesDao.delete(tenantStatusCodes);
	}

	@Override
	public List<TenantStatusCodes> findAllTenantStatusCodes() {
		return tenantStatusCodesDao.findAll();
	}

	@Override
	public void saveTenantStatusCodes(TenantStatusCodes tenantStatusCodes) {
		tenantStatusCodesDao.save(tenantStatusCodes);
	}

	@Override
	public TenantStatusCodes updateTenantStatusCodes(TenantStatusCodes tenantStatusCodes) {
		return tenantStatusCodesDao.save(tenantStatusCodes);
	}
	

}
*/