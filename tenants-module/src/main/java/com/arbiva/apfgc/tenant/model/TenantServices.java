/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

*//**
 * @author Lakshman
 *
 *//*
@Entity
@Table(name = "tenantsrvcs", schema = "apfgc")
@IdClass(TenantServicesPK.class)
public class TenantServices implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Id
	@Column(name="agrfdate")
	private Date agrFDate;

	@Id
	@Column(name="coresrvccode")
    private String coresrvcCode;

	@Column(name = "status")
	private Integer status;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private String modifiedBy;
	
	@Column(name = "modifiedipaddr")
	private String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private Calendar deactivatedDate;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpAddress;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumns({@JoinColumn(name="tenantcode",referencedColumnName="tenantcode", insertable=false,updatable=false), @JoinColumn(name="agrfdate", referencedColumnName="agrfdate", insertable=false,updatable=false)})
	private TenantAgreement tenantAgreement;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="coresrvccode",referencedColumnName="srvcCode", insertable=false,updatable=false)
	private CoreServices coreServices;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public String getCoresrvcCode() {
		return coresrvcCode;
	}

	public void setCoresrvcCode(String coresrvcCode) {
		this.coresrvcCode = coresrvcCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Calendar getDeactivatedDate() {
		return deactivatedDate;
	}

	public void setDeactivatedDate(Calendar deactivatedDate) {
		this.deactivatedDate = deactivatedDate;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpAddress() {
		return deactivatedIpAddress;
	}

	public void setDeactivatedIpAddress(String deactivatedIpAddress) {
		this.deactivatedIpAddress = deactivatedIpAddress;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public TenantAgreement getTenantAgreement() {
		return tenantAgreement;
	}

	public void setTenantAgreement(TenantAgreement tenantAgreement) {
		this.tenantAgreement = tenantAgreement;
	}

	public CoreServices getCoreServices() {
		return coreServices;
	}

	public void setCoreServices(CoreServices coreServices) {
		this.coreServices = coreServices;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
*/