package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.dto.CpeOrderSaveDTO;
import com.arbiva.apfgc.tenant.dto.CpeTypeMobileDTO;

public interface CpeService {

	List<CpeHelperDTO> findAllCpeMasterList();

	List<CpeHelperDTO> findAllCpeModelByCpeType(String cpeType);

	CpeHelperDTO getCpeChargesByProfileId(String profileId);

	String saveCpeOrders(CpeOrderSaveDTO cpeOrderSaveDTO);

	List<CpeHelperDTO> findAllCpeOrdersList(String tenantCode, String tenantType);

	CpeHelperDTO allocateCpe(String demandId);

	String saveCpeAllocation(CpeHelperDTO cpeHelperDTO);

	String cpePayment(CpeHelperDTO cpeHelperDTO);

	List<String> getAllCpeSnosByDlvId(String dlvId);

	List<CpeTypeMobileDTO> getCpeModelDetails();


	CpeHelperDTO saveList(CpeHelperDTO cpehelperDto);

	String processPyament(String amount, String deliveryId, long requestId);
	
	

}
