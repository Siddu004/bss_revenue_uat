/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantStatusCodes;
import com.arbiva.apfgc.tenant.model.TenantStatusCodesPK;

public interface TenantStatusCodesDao extends JpaSpecificationExecutor<TenantStatusCodes>, JpaRepository<TenantStatusCodes, TenantStatusCodesPK> {
	
	@Query(value="select * from tenantsagr t, apfgc.wipstages w where t.status = w.statuscode; ",nativeQuery=true)
	public abstract TenantStatusCodes findBystatusCode(Integer statusCode);

}
*/