/*package com.arbiva.apfgc.tenant.dao;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("storedprocedureDAO")
public class StoredProcedureDAO {

	@Autowired
	private EntityManager em;
	
	public BigInteger executeStoredProcedure(String idName) {
		StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("get_nextid");
		storedProcedure.registerStoredProcedureParameter("p_idname",String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("p_nextid", BigInteger.class, ParameterMode.OUT);
		storedProcedure.registerStoredProcedureParameter("p_result",String.class, ParameterMode.OUT);
		storedProcedure.setParameter("p_idname",idName);
		// execute SP
		storedProcedure.execute();
		// get result
		BigInteger incrementId = (BigInteger)storedProcedure.getOutputParameterValue("p_nextid");
		return incrementId;
	}
}
*/