package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;

public class AddlSrvcsPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String servCode;
	private Date effectivefrom;
	
	public AddlSrvcsPK() {

	}
	
	public AddlSrvcsPK(String servCode, Date effectivefrom) {
		this.servCode = servCode;
		this.effectivefrom = effectivefrom;
	}
	
	public String getServCode() {
		return servCode;
	}
	public void setServCode(String servCode) {
		this.servCode = servCode;
	}
	public Date getEffectivefrom() {
		return effectivefrom;
	}
	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectivefrom == null) ? 0 : effectivefrom.hashCode());
		result = prime * result + ((servCode == null) ? 0 : servCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddlSrvcsPK other = (AddlSrvcsPK) obj;
		if (effectivefrom == null) {
			if (other.effectivefrom != null)
				return false;
		} else if (!effectivefrom.equals(other.effectivefrom))
			return false;
		if (servCode == null) {
			if (other.servCode != null)
				return false;
		} else if (!servCode.equals(other.servCode))
			return false;
		return true;
	}
	
	
	
	

}
