package com.arbiva.apfgc.tenant.pg.utils;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * {@link RestTemplateUtils} holding the application specific constants.
 * 
 * @author srinivasa
 *
 */
@Component("restTemplateUtils")
public class RestTemplateUtils {

	private static final Logger LOGGER = Logger.getLogger(RestTemplateUtils.class);

	private static final String BASIC_AUTH = "%s:%s";
	private static final String BASIC_AUTH_HEADER = "Basic %s";

	/**
	 * 
	 * @param url
	 * @param username
	 * @param password
	 * @param requestBody
	 */
	public static <T> void postWithHTTPS(String url, String username, String password, T requestBody) {
		HttpHeaders headers = null;
		RestTemplate restTemplate = null;
		try {
			enableSSL();
			restTemplate = new RestTemplate();
			headers = getAuthHeaders(username, password);
			restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<String>(headers), Object.class,
					new HttpEntity<Object>(requestBody));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			headers = null;
			restTemplate = null;
		}
	}

	/**
	 * 
	 * @param url
	 * @param requestBody
	 *            xmlstring msg
	 */
	public static String post(String url, final String requestBody, String gateWayType) {
		String uri = null;
		try {
			
			if(StringUtils.equalsIgnoreCase(GatewayTypes.BILLDESK.name(), gateWayType)) {
				uri = url+"?msg="+requestBody;
				LOGGER.info("BillDesk Url With Payload ====>" + uri);
			}else{
				uri = url+"?xmlstring="+requestBody;
				LOGGER.info("Digit Secure Url With Payload ====>" + uri);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("RestTemplateUtils :: post() "+ e);
		}
		return uri;
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("serial")
	private static RestTemplate getRestTemplate() {
		return new RestTemplate(new ArrayList<HttpMessageConverter<?>>() {
			{
				add(new FormHttpMessageConverter());
				add(new StringHttpMessageConverter());
			}
		});
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	private static HttpHeaders getAuthHeaders(String username, String password) {
		String auth = null;
		String authHeader = null;
		byte[] encodedAuth = null;
		HttpHeaders headers = null;
		try {
			auth = String.format(BASIC_AUTH, username, password);
			encodedAuth = Base64.encodeBase64(auth.getBytes());
			authHeader = String.format(BASIC_AUTH_HEADER, new String(encodedAuth));

			headers = new HttpHeaders();
			headers.set("Accept", "application/json");
			headers.set("Authorization", authHeader);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			auth = null;
			authHeader = null;
			encodedAuth = null;
		}
		return headers;

	}

	/**
	 * 
	 */
	private static void enableSSL() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}
	}

}
