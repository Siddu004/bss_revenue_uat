package com.arbiva.apfgc.tenant.pg.businessserviceimpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;

/**
 * 
 * @author srinivasa
 *
 */
@Component("baseBusinessServiceImpl")
public class BaseBusinessServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(BaseBusinessServiceImpl.class);

	/**
	 * 
	 * @param e
	 * @param errorMessage
	 */
	public void handleExceptions(Exception e, String errorMessage) {
		if (e instanceof PaymentGatewayException) {
			throw (PaymentGatewayException) e;
		} else {
			throw new PaymentGatewayException(
					new PaymentGatewayErrorMessageDTO(
							PaymentGatewayErrorCodes.GAE001, errorMessage, e));
		}
	}

}
