package com.arbiva.apfgc.tenant.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.ApsflHelper;
import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tenant.dto.CpeDemandNoteDTO;
import com.arbiva.apfgc.tenant.dto.SmsDTO;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.serviceImpl.CpeDemandNoteServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantServiceImpl;


@RestController
public class CpeDemandNoteRestController {
	
	private static final Logger LOGGER = Logger.getLogger(CpeDemandNoteRestController.class);
	
	@Autowired
	CpeDemandNoteServiceImpl cpeDemandNoteServiceImpl;
	
	@Autowired
	TenantServiceImpl tenantServiceImpl;
	
	@Autowired
	IpAddressValues ipAddressValues;
	
	
	
	@RequestMapping(value = "/getAllLmosByTenantCodeMigrData", method = RequestMethod.GET)
	public List<Object[]> getAllLmosByTenantCodeMigrData(@RequestParam(value="tenantCode") String tenantCode) {
		List<Object[]> list = new ArrayList<>();
		try {
			list = cpeDemandNoteServiceImpl.getAllLmosByTenantCodeMigrData(tenantCode);
		} catch (Exception e) {
			LOGGER.error("cpeDemandNoteServiceImpl::getAllLmosByTenantCodeMigrData() " + e);
			e.printStackTrace();
		} finally {}
		return list;
	}
	
	@RequestMapping(value = "/lmoCpeRequestSavePortal", method = RequestMethod.POST)
	public String lmoCpeRequestSavePortal(@RequestBody CpeDemandNoteDTO cpeHelperDTO) {
		String returnVal = null;
		Map<String, Long> map ;
		HttpEntity<SmsDTO> httpEntity =  null;
		ResponseEntity<String> response = null;
		RestTemplate restTemplate = new RestTemplate();
		Long emi0 = 0L;
		Long emi36 = 0L;
		Long emi48 = 0L;
		String date = null;
		
		try {
			map = cpeDemandNoteServiceImpl.lmoCpeRequestSavePortal(cpeHelperDTO);
			
			//Getting Tenant Object By Tenant code
			Tenant tenant = tenantServiceImpl.findByTenantCode(cpeHelperDTO.getTenantCode());
			
			//Sending SMS To Tenant 
			emi0 = map.get("emi0") ==  null ? 0l : map.get("emi0");
			emi36 = map.get("emi36") ==  null ? 0l : map.get("emi36");
			emi48 = map.get("emi48") ==  null ? 0l : map.get("emi48");
			date = DateUtill.calendarTimeToYYYY_MMM_dd(Calendar.getInstance());
			if(emi0 > 0 || emi36 > 0 || emi48 >0 ){
				SmsDTO smsDto = new SmsDTO();
				smsDto.setMobileNo(tenant.getPocMobileNo1());
				smsDto.setMsg("Dear MSO, \n Thank You for placing CPE request for "+emi0+" quantity for (Rs 4000), "+emi36+" quantity for (Rs 1700) & "+emi48+" quantity for (Rs 500) on "+date+". \n--Team APSFL");
				httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd(), smsDto);
				String url = ipAddressValues.getComURL()+ "sendSMS";
				response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
				returnVal = response.getBody();
			}
			returnVal = "SUCCESS";
			
		} catch (Exception e) {
			LOGGER.error("cpeDemandNoteServiceImpl :: lmoCpeRequestSavePortal()" + e.getMessage());
			e.printStackTrace();
		} finally {
		}
		return returnVal;
	}

}
