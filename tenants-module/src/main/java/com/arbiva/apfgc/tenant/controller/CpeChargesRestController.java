package com.arbiva.apfgc.tenant.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.tenant.dto.CpeCharHelperDTO;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;
import com.arbiva.apfgc.tenant.service.CpeCharAgmtService;

@RestController
public class CpeChargesRestController {
	
	private static final Logger logger = Logger.getLogger(CpeChargesRestController.class);
	
	@Autowired
	CpeCharAgmtService cpeCharAgmtService;
	
	@RequestMapping(value = "/createCpeChrgAgmt", method = RequestMethod.GET)
	public CpeCharHelperDTO createCpeChrgAgmt() {
		List<RevenueSharingTemplateMaster> cpeTmplList = new ArrayList<>();
		CpeCharHelperDTO cpeCharHelperDTO = new CpeCharHelperDTO();
		try {
			logger.info("CpeChargesRestController::createCpeChrgAgmt():: START ");
			cpeTmplList = cpeCharAgmtService.getAllCpeTempl();
			cpeCharHelperDTO.setCpeTmplList(cpeTmplList);
			logger.info("CpeChargesRestController::createCpeChrgAgmt():: END ");
		} catch (Exception e) {
			logger.error("CpeChargesRestController::createCpeChrgAgmt() " + e);
			e.printStackTrace();
		} finally {
			cpeTmplList = null;
		}
		return cpeCharHelperDTO;
	}
	
	@RequestMapping(value = "/getChargeCodesByTemplate", method = RequestMethod.GET)
	public List<CpeCharHelperDTO> getChargeCodesByTemplate(@RequestParam("tmplCode") String tmplCode) {
		List<CpeCharHelperDTO> list = new ArrayList<CpeCharHelperDTO>();
		try {
			logger.info("CpeChargesRestController::getChargeCodesByTemplate():: START ");
			list = cpeCharAgmtService.getChargeCodesByTemplate(tmplCode);
			logger.info("CpeChargesRestController::getChargeCodesByTemplate():: END ");
		} catch (Exception e) {
			logger.error("CpeChargesRestController::getChargeCodesByTemplate() " + e);
			e.printStackTrace();
		} finally {}
		return list;
	}
	
	@RequestMapping(value = "/saveCpeChrgAgmt", method = RequestMethod.POST)
	public String saveCpeChrgAgmt(@RequestBody CpeCharHelperDTO cpeCharHelperDTO) {
		String responce = "";
		try {
			logger.info("CpeChargesRestController::saveCpeChrgAgmt():: START ");
			responce = cpeCharAgmtService.save(cpeCharHelperDTO);
			logger.info("CpeChargesRestController::saveCpeChrgAgmt():: End ");
		} catch (Exception e) {
			logger.error("CpeChargesRestController::saveCpeChrgAgmt() " + e);
			e.printStackTrace();
		} finally {}
		return responce;
	}
	
	@RequestMapping(value = "/viewCpeChrgAgmt", method = RequestMethod.GET)
	@ResponseBody
	public List<CpeCharHelperDTO> viewCpeChrgAgmt(@RequestParam("tanantCode") String tanantCode ) {
		List<CpeCharHelperDTO> responce =new ArrayList<>();
		try {
			logger.info("CpeChargesRestController:: viewCpeChrgAgmt() :: START ");
			responce = cpeCharAgmtService.viewCpeChrgAgmtByTenant(tanantCode);
			logger.info("CpeChargesRestController:: viewCpeChrgAgmt() :: END ");
		} catch (Exception e) {
			logger.error("CpeController::viewCpeChrgAgmt() " + e);
			e.printStackTrace();
		} finally {}
		return responce;
	}

}
