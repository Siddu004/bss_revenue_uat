/**
 * 
 *//*
package com.arbiva.apfgc.tenant.serviceImpl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tanent.util.FileUtil;
import com.arbiva.apfgc.tanent.util.TenantErrorCodes;
import com.arbiva.apfgc.tanent.util.TmsHelper;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantLmoMspAgreementDaoImpl;
import com.arbiva.apfgc.tenant.exception.TenantException;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreement;
import com.arbiva.apfgc.tenant.vo.TenantLmoMspAgreementVO;

*//**
 * @author Lakshman
 *
 *//*
@Service
public class TenantLmoMspAgreementServiceImpl {

	 public static final String mspPath = "C:/MspAgreementDocuments"; 

	@Autowired
	TenantLmoMspAgreementDaoImpl tenantLmoMspAgreementDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	TenantDaoImpl tenantDao;

	@Autowired
	TenantLMOServicesServiceImpl tenantLMOServicesService;

	@Autowired
	TenantServiceImpl tenantServiceImpl;

	@Value("${UMS-URL}")
	private String umsURL;

	@Value("${IMAGE_PATH}")
	private String mspPath;

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantLmoMspAgreementDaoImpl.class);

	@Transactional
	public void saveTenantLmoMspAgreement(TenantLmoMspAgreementVO tenantLmoMspAgreementVO, String action) throws IOException, ParseException {
		String userName = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		LOGGER.info("-- TenantLmoMspAgreementServiceImpl -- saveTenantLmoMspAgreement --" + action);
		if (userName != null || userName != "") {
				TenantLmoMspAgreement tenantLmoMspAgreement = null;
				LOGGER.info("The Value is :::::" + action);
				
				tenantLmoMspAgreement = tenantLmoMspAgreementDao.findByMspCode(tenantLmoMspAgreementVO.getMspCode(),
							DateUtill.stringtoString(tenantLmoMspAgreementVO.getAgreementFrom()), tenantLmoMspAgreementVO.getLmoCode());
				if( tenantLmoMspAgreement == null){
					tenantLmoMspAgreement = new TenantLmoMspAgreement();
					tenantLmoMspAgreement.setCratedIPAddress(httpServletRequest.getRemoteAddr());
					tenantLmoMspAgreement.setCreatedBy(userName);
					tenantLmoMspAgreement.setCreatedDate(Calendar.getInstance());
					tenantLmoMspAgreement.setModifiedDate(Calendar.getInstance());
				}else{
					tenantLmoMspAgreement.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
					tenantLmoMspAgreement.setModifiedBy(userName);
					tenantLmoMspAgreement.setModifiedDate(Calendar.getInstance());
					if(tenantLmoMspAgreementVO.getAgreementCopy().isEmpty())
						tenantLmoMspAgreement.setAgreementDocRef(tenantLmoMspAgreement.getAgreementDocRef());
				}
					

				if (!tenantLmoMspAgreementVO.getAgreementCopy().isEmpty()) {
					String docType = "LmoMspAgreement";
					String dbPath = FileUtil.saveImage(tenantLmoMspAgreementVO.getAgreementCopy().getOriginalFilename(), mspPath,
							tenantLmoMspAgreementVO.getAgreementCopy().getBytes(), userName,docType);
					tenantLmoMspAgreement.setAgreementDocRef(dbPath);
				}
				tenantLmoMspAgreement.setAgreementDate(DateUtill.stringtoDate(tenantLmoMspAgreementVO.getAgreementDate()));
				tenantLmoMspAgreement.setAgreementTo(DateUtill.stringtoDate(tenantLmoMspAgreementVO.getAgreementTo()));
				tenantLmoMspAgreement.setAgrFDate(DateUtill.stringtoDate(tenantLmoMspAgreementVO.getAgreementFrom()));
				tenantLmoMspAgreement.setDepositAmount(tenantLmoMspAgreementVO.getDepositAmount() == null ? 0 : tenantLmoMspAgreementVO.getDepositAmount());
				tenantLmoMspAgreement.setDepositMode(tenantLmoMspAgreementVO.getDepositMode());
				tenantLmoMspAgreement.setDepositRefno(tenantLmoMspAgreementVO.getDepositReferenceNo());
				tenantLmoMspAgreement.setDepositStatus(1);
				tenantLmoMspAgreement.setLmoCode(tenantLmoMspAgreementVO.getLmoCode());
				tenantLmoMspAgreement.setMspCode(tenantLmoMspAgreementVO.getMspCode());
				tenantLmoMspAgreement.setLmoWalletAmount(tenantLmoMspAgreementVO.getLmoWalletAmount());
				tenantLmoMspAgreement.setStatus(1);

				tenantLmoMspAgreementDao.saveTenantLmoMspAgreement(tenantLmoMspAgreement);
				LOGGER.info("The Tenant LMO/MSPAgreement Created Successfully.");

				tenantLMOServicesService.saveTenantLMOServices(tenantLmoMspAgreementVO, action);
				LOGGER.info("The Tenant LMO Services Created Successfully.");

		}
	}

	public TenantLmoMspAgreement findByMspCode(String mspCode, String effDate, String lmoCode) throws ParseException {
		TenantLmoMspAgreement tenantLmoMspAgreement = new TenantLmoMspAgreement();
		tenantLmoMspAgreement = tenantLmoMspAgreementDao.findByMspCode(mspCode, effDate, lmoCode);
		return tenantLmoMspAgreement;
	}

	
	 * public TenantLmoMspAgreement findByLmoCode(String lmoCode) {
	 * TenantLmoMspAgreement tenantLmoMspAgreement = new
	 * TenantLmoMspAgreement(); tenantLmoMspAgreement =
	 * tenantLmoMspAgreementDao.findByMspCode(lmoCode); return
	 * tenantLmoMspAgreement; }
	 

	public List<TenantLmoMspAgreement> findAllTenantLmoMspAgreements() {
		List<TenantLmoMspAgreement> tenantLmoMspAgreementList = new ArrayList<TenantLmoMspAgreement>();
		tenantLmoMspAgreementList = tenantLmoMspAgreementDao.findAllTenantLmoMspAgreements();
		return tenantLmoMspAgreementList;
	}


	@Transactional
	public void updateTenantLmoMspAgreementStauts(TenantLmoMspAgreement mspLmoAgreement, String action,
			TenantLmoMspAgreementVO tenantLmoMspAgreementVO, String reason) {

		if (action.equalsIgnoreCase("Approve")) {
			mspLmoAgreement.setStatus(2);
			Tenant tenanObj = tenantServiceImpl.findByTenantId(tenantLmoMspAgreementVO.getTenantId());
			String responce = TmsHelper.createUser(tenanObj, umsURL);
			if(responce.equalsIgnoreCase("exists"))
				throw new TenantException(TenantErrorCodes.TENT003, "User Is Already Created With The Same LoginId ");
		} else if (action.equalsIgnoreCase("Send Back")){
			mspLmoAgreement.setStatus(4);
			mspLmoAgreement.setReasons(reason == null ? "" : reason.trim());
		}
		else if (action.equalsIgnoreCase("Reject")){
			mspLmoAgreement.setStatus(3);
			mspLmoAgreement.setReasons(reason == null ? "" : reason.trim());
		}
			
		tenantLmoMspAgreementDao.saveTenantLmoMspAgreement(mspLmoAgreement);
		
	}
}
*/