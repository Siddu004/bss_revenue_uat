package com.arbiva.apfgc.tenant.vo;

import java.util.List;

import com.arbiva.apfgc.tenant.model.Tenant;

public class TemplatePartnerVO {
	
	private String templCode;
	
	private String tenantType;
	
	private int sno;
	
	private List<Tenant> tenantList;

	public String getTemplCode() {
		return templCode;
	}

	public void setTemplCode(String templCode) {
		this.templCode = templCode;
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public List<Tenant> getTenantList() {
		return tenantList;
	}

	public void setTenantList(List<Tenant> tenantList) {
		this.tenantList = tenantList;
	}
	
	

}
