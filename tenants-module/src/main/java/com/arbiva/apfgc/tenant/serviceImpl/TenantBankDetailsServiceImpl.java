package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tenant.daoImpl.TenantBankDetailsDaoImpl;
import com.arbiva.apfgc.tenant.model.TenantBankDetails;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@Service
public class TenantBankDetailsServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantBankDetailsDaoImpl.class);

	@Autowired
	TenantBankDetailsDaoImpl tenantBankDetailsDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	public List<TenantBankDetails> findAllTenantBankDetailss() {
		List<TenantBankDetails> tenantBankDetails = new ArrayList<TenantBankDetails>();
		tenantBankDetails = tenantBankDetailsDao.findAllTenantBankDetails();
		return tenantBankDetails;
	}

	public TenantBankDetails findByTenantCode(String tenantCode) {
		TenantBankDetails tenantBankDetails = new TenantBankDetails();
		tenantBankDetails = tenantBankDetailsDao.findByTenantCode(tenantCode);
		return tenantBankDetails;
	}

	public void saveTenantBankDetails(TenantVO tenantVO, Integer tenantId) {
		String userName = (String) httpServletRequest.getSession(false).getAttribute("username");
		TenantBankDetails tenantBankDetails;
		if (userName != null || userName != "") {
			try {
				  tenantBankDetails = new TenantBankDetails();
				if (tenantId == null) {
					tenantBankDetails = new TenantBankDetails();
					tenantBankDetails.setCratedIPAddress(httpServletRequest.getRemoteAddr());
					tenantBankDetails.setCreatedBy(userName);
					tenantBankDetails.setCreatedDate(Calendar.getInstance());
					tenantBankDetails.setStatus(1);
					tenantBankDetails.setModifiedDate(Calendar.getInstance());
				} else {
					tenantBankDetails = findByTenantCode(tenantVO.getTenantCode());
					tenantBankDetails.setModifiedBy(userName);
					tenantBankDetails.setModifiedDate(Calendar.getInstance());
					tenantBankDetails.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
					tenantBankDetails.setStatus(2);
				}
				tenantBankDetails.setTenantCode(tenantVO.getTenantCode());
				tenantBankDetails.setAccountNo(tenantVO.getAccountNo());
				tenantBankDetails.setIfscCode(tenantVO.getIfscCode());
				tenantBankDetails.setAcctTypelov(tenantVO.getAcctTypelov());
				tenantBankDetails.setBankNamelov(tenantVO.getBankNamelov());
				tenantBankDetails.setBranchName(tenantVO.getBranchName());

				tenantBankDetailsDao.saveTenantBankDetails(tenantBankDetails);
			} catch (Exception e) {
				LOGGER.error("TenantBankDetailsServiceImpl::saveTenantBankDetails() " + e);
				e.printStackTrace();
			} finally {
				userName = null;
				tenantBankDetails = null;
			}
		}
	}
}
