package com.arbiva.apfgc.tenant.model;

public class CorpusRequest {
	
	private String packageCode;
	private String packageName;
	private String serviceType;
	private String[] serviceIds;
	private String isGlobal;
	private String packagePrice;
	
	
	
	
	public String getPackagePrice() {
		return packagePrice;
	}
	public void setPackagePrice(String packagePrice) {
		this.packagePrice = packagePrice;
	}
	public String getIsGlobal() {
		return isGlobal;
	}
	public void setIsGlobal(String isGlobal) {
		this.isGlobal = isGlobal;
	}
	public String getPackageCode() {
		return packageCode;
	}
	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String[] getServiceIds() {
		return serviceIds;
	}
	public void setServiceIds(String[] serviceIds) {
		this.serviceIds = serviceIds;
	}
	
}
