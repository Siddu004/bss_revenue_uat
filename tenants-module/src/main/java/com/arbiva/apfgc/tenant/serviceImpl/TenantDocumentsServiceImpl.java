package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tanent.util.FileUtil;
import com.arbiva.apfgc.tenant.daoImpl.TenantDocumentsDaoImpl;
import com.arbiva.apfgc.tenant.model.TenantDocuments;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@Service
public class TenantDocumentsServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantDocumentsServiceImpl.class);

	@Autowired
	TenantDocumentsDaoImpl tenantDocumentsDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Value("${IMAGE_PATH}")
	private String basePath;

	@Transactional
	public void saveTenantDocument(TenantVO tenantVO, Integer tenantId) {

		TenantDocuments tenantDocuments =  new TenantDocuments();
		List<TenantDocuments> tenantDocumentsList ;
		String loginID = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		if (loginID != null || loginID != "") {
			try {
				  tenantDocumentsList = findByTenantCode(tenantVO.getTenantCode());
				if (!tenantVO.getLicenceId().isEmpty()) {
					//String[] docLovNameAndId = tenantVO.getDoclov().split(",");
					if (tenantId == null) {
						//tenantDocuments = new TenantDocuments();
						tenantDocuments.setCratedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setCreatedBy(loginID);
						tenantDocuments.setCreatedDate(Calendar.getInstance());
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setStatus(1);
					} else {
						for(TenantDocuments doc : tenantDocumentsList ){
							if(doc.getDocumentLovName().equalsIgnoreCase("GENERAL DOCUMENTS"))
							tenantDocuments = doc;
						}
						tenantDocuments.setModifiedBy(loginID);
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setStatus(1);
					}
					String dbPath = FileUtil.saveImage(tenantVO.getLicenceId().getOriginalFilename(), basePath,
							tenantVO.getLicenceId().getBytes(), loginID, "GENERAL DOCUMENTS");
					tenantDocuments.setTenantCode(tenantVO.getTenantCode());
					tenantDocuments.setDocumentLovName("GENERAL DOCUMENTS");
					tenantDocuments.setDocuniqueId(tenantVO.getLicenserefno());
					tenantDocuments.setDocmentLov("");
					tenantDocuments.setEffectiveFrom(DateUtill.stringtoDate(tenantVO.getEffectiveFrom()));
					tenantDocuments.setEffectiveTo(DateUtill.stringtoDate(tenantVO.getEffectiveTO()));
					tenantDocuments.setDocumentLocationReference(dbPath);
					tenantDocumentsDao.saveTenantDocument(tenantDocuments);
				}
				if (!tenantVO.getIdProof().isEmpty()) {
					//String[] docLovNameAndId = tenantVO.getDoclov1().split(",");
					if (tenantId == null) {
						//tenantDocuments = new TenantDocuments();
						tenantDocuments.setCratedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setCreatedBy(loginID);
						tenantDocuments.setCreatedDate(Calendar.getInstance());
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setStatus(1);
					} else {
						for(TenantDocuments doc : tenantDocumentsList ){
							if(doc.getDocumentLovName().equalsIgnoreCase("POI DOCUMENTS"))
							tenantDocuments = doc;
						}
						tenantDocuments.setModifiedBy(loginID);
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setStatus(1);
					}
					String dbPath = FileUtil.saveImage(tenantVO.getIdProof().getOriginalFilename(), basePath,
							tenantVO.getIdProof().getBytes(), loginID, "POI DOCUMENTS");
					tenantDocuments.setTenantCode(tenantVO.getTenantCode());
					tenantDocuments.setDocumentLovName("POI DOCUMENTS");
					tenantDocuments.setDocmentLov(tenantVO.getDoclov1());
					tenantDocuments.setDocuniqueId(tenantVO.getDocUniqueId1());
					tenantDocuments.setEffectiveFrom(DateUtill.stringtoDate(tenantVO.getEffectiveFrom1()));
					tenantDocuments.setEffectiveTo(DateUtill.stringtoDate(tenantVO.getEffectiveTO1()));
					tenantDocuments.setDocumentLocationReference(dbPath);
					tenantDocumentsDao.saveTenantDocument(tenantDocuments);
				}
				if (!tenantVO.getAddressProof().isEmpty()) {
					//String[] docLovNameAndId = tenantVO.getDoclov2().split(",");
					if (tenantId == null) {
						//tenantDocuments = new TenantDocuments();
						tenantDocuments.setCratedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setCreatedBy(loginID);
						tenantDocuments.setCreatedDate(Calendar.getInstance());
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setStatus(1);
					} else {
						for(TenantDocuments doc : tenantDocumentsList ){
							if(doc.getDocumentLovName().equalsIgnoreCase("POA DOCUMENTS"))
							tenantDocuments = doc;
						}
						tenantDocuments.setModifiedBy(loginID);
						tenantDocuments.setModifiedDate(Calendar.getInstance());
						tenantDocuments.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
						tenantDocuments.setStatus(1);
					}
					String dbPath = FileUtil.saveImage(tenantVO.getAddressProof().getOriginalFilename(), basePath,
							tenantVO.getAddressProof().getBytes(), loginID, "POA DOCUMENTS");
					tenantDocuments.setTenantCode(tenantVO.getTenantCode());
					tenantDocuments.setDocumentLovName("POA DOCUMENTS");
					tenantDocuments.setDocmentLov(tenantVO.getDoclov2());
					tenantDocuments.setDocuniqueId(tenantVO.getDocUniqueId2());
					tenantDocuments.setEffectiveFrom(DateUtill.stringtoDate(tenantVO.getEffectiveFrom2()));
					tenantDocuments.setEffectiveTo(DateUtill.stringtoDate(tenantVO.getEffectiveTO2()));
					tenantDocuments.setDocumentLocationReference(dbPath);
					tenantDocumentsDao.saveTenantDocument(tenantDocuments);
				}
			} catch (Exception e) {
				LOGGER.error("TenantDocumentsServiceImpl::saveTenantDocument() " + e);
				e.printStackTrace();
			} finally {
				tenantDocuments = null;
				loginID = null;
				tenantDocumentsList = null;
			}
		}
	}

	public List<TenantDocuments> findByTenantCode(String tenantCode) {
		List<TenantDocuments> tenantDocument = new ArrayList<TenantDocuments>();
		tenantDocument = tenantDocumentsDao.findByTenantCode(tenantCode);
		return tenantDocument;
	}
}
