package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="cpecharges")
@IdClass(CpeChargesIdClass.class)
public class CpeCharges {
	
	@Id
	@Column(name = "profile_id")
	private Long profileId;
	
	@Id
	@Column(name = "effectivefrom")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date effectiveFrom;

	@Column(name = "effectiveto")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date effectiveTo;
	
	@Column(name = "custcost")
	private BigDecimal custCost;
	
	@Column(name = "custrent")
	private BigDecimal custRent;
	
	@Column(name = "purchasecost")
	private BigDecimal purchaseCost;
	
	@Column(name = "instcharges")
	private BigDecimal instCharges;
	
	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id", nullable = false, insertable = false, updatable = false)
	private CpeProfileMaster cpeProfileMaster;

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public BigDecimal getCustCost() {
		return custCost;
	}

	public void setCustCost(BigDecimal custCost) {
		this.custCost = custCost;
	}

	public BigDecimal getCustRent() {
		return custRent;
	}

	public void setCustRent(BigDecimal custRent) {
		this.custRent = custRent;
	}

	public BigDecimal getPurchaseCost() {
		return purchaseCost;
	}

	public void setPurchaseCost(BigDecimal purchaseCost) {
		this.purchaseCost = purchaseCost;
	}

	public BigDecimal getInstCharges() {
		return instCharges;
	}

	public void setInstCharges(BigDecimal instCharges) {
		this.instCharges = instCharges;
	}

	public CpeProfileMaster getCpeProfileMaster() {
		return cpeProfileMaster;
	}

	public void setCpeProfileMaster(CpeProfileMaster cpeProfileMaster) {
		this.cpeProfileMaster = cpeProfileMaster;
	}
	
	

}
