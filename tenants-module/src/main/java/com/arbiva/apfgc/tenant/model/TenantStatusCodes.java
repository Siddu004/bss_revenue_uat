package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="wipstages")
@IdClass(TenantStatusCodesPK.class)
public class TenantStatusCodes implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "appcode")
	private String appCode;
	
	@Id
	@Column(name = "statuscode")
	private Integer statusCode;
	
	@Column(name = "statusdesc")
	private String statusDesc;
	
	@Column(name = "status")
	private Integer Status;
	
	
	
	public String getAppCode() {
		return appCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatus(Integer Status) {
		this.Status = Status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
