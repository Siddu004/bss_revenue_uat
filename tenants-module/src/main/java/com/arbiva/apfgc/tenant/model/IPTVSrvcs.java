package com.arbiva.apfgc.tenant.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

public class IPTVSrvcs {
	

    private String serviceId;
	
    private String code;
	
    private String name;
    
    private String language;
    
    @JsonIgnore
    private String broadcaster;
    
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getBroadcaster() {
		return broadcaster;
	}
	public void setBroadcaster(String broadcaster) {
		this.broadcaster = broadcaster;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
