package com.arbiva.apfgc.tenant.dto;

import java.io.Serializable;

public class AgreementDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int status;
	private String tenantCode;
	private String tenantName;
	private String tenantTypeLov;
	private String mobile;
	private Integer tenantId;
	private String createdBy;
	private String agreementType;
	private String mspOrTenantCode;
	private String effDate;
	private String lmoCode;
	
	public String getMspOrTenantCode() {
		return mspOrTenantCode;
	}
	public void setMspOrTenantCode(String mspOrTenantCode) {
		this.mspOrTenantCode = mspOrTenantCode;
	}
	public String getEffDate() {
		return effDate;
	}
	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}
	public String getLmoCode() {
		return lmoCode;
	}
	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}
	public String getAgreementType() {
		return agreementType;
	}
	public void setAgreementType(String agreementType) {
		this.agreementType = agreementType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantTypeLov() {
		return tenantTypeLov;
	}
	public void setTenantTypeLov(String tenantTypeLov) {
		this.tenantTypeLov = tenantTypeLov;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getTenantId() {
		return tenantId;
	}
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
