/**
 * 
 *//*
package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantLMOWallet;
import com.arbiva.apfgc.tenant.model.TenantLMOWalletPK;

*//**
 * @author Arbiva
 *
 *//*
public interface TenantLMOWalletDao extends JpaSpecificationExecutor<TenantLMOWallet>, JpaRepository<TenantLMOWallet, TenantLMOWalletPK> {

	@Query(value="SELECT * FROM lmowallet where mspcode = ?1 ",nativeQuery=true)
	public abstract TenantLMOWallet findByMspCode(String mspCode);
	
	@Query(value="SELECT * FROM lmowallet where lmocode = ?1 ",nativeQuery=true)
	public abstract TenantLMOWallet findByLmoCode(String lmoCode);
}
*/