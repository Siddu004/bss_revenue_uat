/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantWallet;

public interface TenantWalletDao extends JpaSpecificationExecutor<TenantWallet>, JpaRepository<TenantWallet, String> {

	@Query(value="SELECT * FROM tenantswallet where tenantcode = ?1 ",nativeQuery=true)
	public abstract TenantWallet findByTenantCode(String tenantCode);
}
*/