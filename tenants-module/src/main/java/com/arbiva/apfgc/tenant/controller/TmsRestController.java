package com.arbiva.apfgc.tenant.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.tanent.util.PaginationObject;
import com.arbiva.apfgc.tanent.util.Response;
import com.arbiva.apfgc.tanent.util.TmsHelper;
import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.dto.CreditLmoWallet;
import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.dto.MailServiceDTO;
import com.arbiva.apfgc.tenant.dto.OLT;
import com.arbiva.apfgc.tenant.dto.PageObject;
import com.arbiva.apfgc.tenant.dto.RevenueSharingDTO;
import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsDTO;
import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsListDTO;
import com.arbiva.apfgc.tenant.dto.TenantDTO;
import com.arbiva.apfgc.tenant.dto.TenantLMORevServiceDTO;
import com.arbiva.apfgc.tenant.dto.TmsHelperDTO;
import com.arbiva.apfgc.tenant.model.AdditionalService;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.Districts;
import com.arbiva.apfgc.tenant.model.Lovs;
import com.arbiva.apfgc.tenant.model.Mandals;
import com.arbiva.apfgc.tenant.model.MspChnlsStg;
import com.arbiva.apfgc.tenant.model.OLTPortDetails;
import com.arbiva.apfgc.tenant.model.Region;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantDocuments;
import com.arbiva.apfgc.tenant.model.TenantsWalletTransErrors;
import com.arbiva.apfgc.tenant.model.UploadHistory;
import com.arbiva.apfgc.tenant.service.RsAgrmntsService;
import com.arbiva.apfgc.tenant.serviceImpl.LovsServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.PortalAreaServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.PortalAssetsServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.RegionServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.RevenueSharingServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantBankDetailsServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantDocumentsServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantLicensesServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantWalletServiceImpl;
import com.arbiva.apfgc.tenant.vo.BlackListInfoVO;
import com.arbiva.apfgc.tenant.vo.BlackListResultVO;
import com.arbiva.apfgc.tenant.vo.CafsForBlockListVO;
import com.arbiva.apfgc.tenant.vo.CpeStockVO;
import com.arbiva.apfgc.tenant.vo.LMORevSummaryVO;
import com.arbiva.apfgc.tenant.vo.ProductAgreementVO;
import com.arbiva.apfgc.tenant.vo.TemplatePartnerVO;
import com.arbiva.apfgc.tenant.vo.TenantDataVO;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@RestController
public class TmsRestController {

	@Autowired
	TenantServiceImpl tenantService;

	@Autowired
	TenantWalletServiceImpl tenantWalletServiceImpl;

	@Autowired
	TenantDocumentsServiceImpl tenantDocumentsService;

	@Autowired
	TenantLicensesServiceImpl tenantLicensesService;

	@Autowired
	TenantBankDetailsServiceImpl tenantBankDetailsService;

	@Autowired
	TenantWalletServiceImpl tenantWalletService;

	@Autowired
	PortalAssetsServiceImpl portalassetsservice;

	@Autowired
	PortalAreaServiceImpl portalAreaService;

	@Autowired
	RevenueSharingServiceImpl revenueSharingServiceImpl;

	@Autowired
	RegionServiceImpl regionService;

	@Autowired
	LovsServiceImpl lovsService;

	@Autowired
	RsAgrmntsService rsAgrmntsService;

	@Autowired
	TmsHelper tmsHelper;

	@Value("${UMS-URL}")
	private String umsURL;

	private static final Logger logger = Logger.getLogger(TmsRestController.class);

	@RequestMapping(value = "/tmsHome", method = RequestMethod.POST)
	public PaginationObject<TenantDTO> home(@RequestBody TenantDataVO tenantDataVO){
		PaginationObject<TenantDTO> multiDataPageObject= new PaginationObject<>();
		try {
			logger.info("TmsRestController:: home() :: START ");
			
			multiDataPageObject.setAaData(tenantService.findAllTenantsBYCreatedBy(tenantDataVO.getLoginId(), tenantDataVO.getPageObject()));
			//Set Total display record
			multiDataPageObject.setiTotalDisplayRecords(Long.parseLong(multiDataPageObject.getAaData().get(0).getTotalDisplayCount()));
			multiDataPageObject.setiTotalRecords(Long.parseLong(multiDataPageObject.getAaData().get(0).getTotalDisplayCount()));
			
			logger.info("TmsRestController:: home() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController:: home()" + ex);
		} finally {
		}
		logger.debug("Welcome home! The client locale is {}.");

		return multiDataPageObject;
	}

	@RequestMapping(value = "/getApprovedLMOTenants", method = RequestMethod.GET)
	public List<TenantDTO> getApprovedLMOTenants(@RequestParam(value = "loginId") String loginId,
			@RequestParam(value = "tagName") String tagName) throws IOException {
		List<TenantDTO> tenantsList = new ArrayList<TenantDTO>();
		List<TenantDTO> result = new ArrayList<TenantDTO>();
		try {
			logger.info("TmsRestController:: getApprovedLMOTenants() :: START ");
			tenantsList = tenantService.getApprovedLMOTenants(loginId);
			for (TenantDTO tag : tenantsList) {
				if (tag.getTenantName().toLowerCase().contains(tagName.toLowerCase())) {
					result.add(tag);
				}
			}
			logger.info("TmsRestController:: getApprovedLMOTenants() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController:: getApprovedLMOTenants() " + ex);
		} finally {
			tenantsList = null;
		}
		logger.debug("Welcome home! The client locale is {}.");

		return result;
	}

	@RequestMapping(value = "/getLmoOlts", method = RequestMethod.GET)
	public TmsHelperDTO getLmoOlts(@RequestParam(value = "tenantId") String tenantId) throws IOException {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Tenant tenant = null;
		String tenantCode = null;
		Queue<String> queue = new LinkedList<>();
		String subStnList = null;
		List<OLT> oltLovs = null;
		List<OLT> oltLmoList = null;
		try {
			logger.info("TmsRestController:: getLmoOlts() :: START ");
			tenant = tenantService.findByTenantId(Integer.parseInt(tenantId));
			tenantCode = tenant.getTenantCode();
			queue.add(tenantService.findAllSubStationIdsByTenantCode(tenantCode).toString());
			subStnList = "'" + queue.toString().replace("[", "").replace("]", "").replace(" ", "").replace(",", "','")
					+ "'";
			oltLovs = tenantService.getLmoOltLovs(subStnList);
			oltLmoList = tenantService.getLmoOltList(tenantCode);
			tmsHelperDTO.setOltLovs(oltLovs);
			tmsHelperDTO.setOltLmoList(oltLmoList);
			logger.info("TmsRestController:: getLmoOlts() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController:: getLmoOlts() " + ex);
		} finally {
			tenant = null;
			tenantCode = null;
			queue = null;
			subStnList = null;
			oltLovs = null;
			oltLmoList = null;
		}
		return tmsHelperDTO;
	}

	@SuppressWarnings({ "rawtypes", "serial", "unchecked" })
	@RequestMapping(value = "/getOLTPortIds", method = RequestMethod.GET)
	public TmsHelperDTO getOLTPortIds(@RequestParam(value = "oltId") String oltId,
			@RequestParam("tenantCode") String tenantCode) throws IOException {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> map = new LinkedHashMap<>();
		Collection oltPortList = null;
		Collection masterList = null;
		List<String> opList = null;
		Tenant tenant = null;
		try {
			tenant = tenantService.findByTenantCode(tenantCode);
			logger.info("TmsRestController:: getOLTPortIds() :: START ");
			oltPortList = tenantService.getOLTPortIds(oltId);
			masterList = new ArrayList() {
				{
					add("1");
					add("2");
					add("3");
					add("4");
					add("5");
					add("6");
					add("7");
					add("8");

				}
			};

			if (!tenant.getTenantTypeLov().equalsIgnoreCase("APSFL"))
				masterList.remove("1");
			masterList.removeAll(oltPortList);
			opList = new ArrayList<String>(masterList);
			for (String s : opList)
				map.put(s, s);
			tmsHelperDTO.setOltPortIdList(map);
			logger.info("TmsRestController:: getOLTPortIds() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController:: getOLTPortIds()  " + ex);
		} finally {
			map = null;
			oltPortList = null;
			masterList = null;
			opList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/saveOltPorts", method = RequestMethod.POST)
	public String saveOltPorts(@RequestBody String jsonString) throws IOException {
		String result = "";
		try {
			logger.info("TmsRestController:: saveOltPorts() :: START");
			tenantService.saveOltPorts(jsonString);
			result = "success";
			logger.info("TmsRestController:: saveOltPorts() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: saveOltPorts() " + e);
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@RequestMapping(value = "/lmo", method = RequestMethod.GET)
	public TmsHelperDTO getLMOPage() {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> stateList = null;
		Map<String, String> apDistrictList = null;
		Map<String, String> districtList = new LinkedHashMap<>();
		Map<String, String> mandalList = new LinkedHashMap<>();
		Map<String, String> villageList = new LinkedHashMap<>();
		Map<String, String> cableList = null;
		Map<String, String> assetsList = null;
		List<Region> regionsList = null;
		List<Lovs> generalDoc = null;
		List<Lovs> poiDoc = null;
		List<Lovs> poaDoc = null;
		List<Lovs> tenantTpesList = null;
		try {
			logger.info("TmsRestController:: getLMOPage() :: START");
			stateList = tenantService.getstateList();
			apDistrictList = tenantService.getDistrictList("1");

			cableList = tenantService.getcableList();
			assetsList = tenantService.getassetsList();
			regionsList = regionService.findAllRegions();
			generalDoc = lovsService.findByLovsByLovName("GENERAL DOCUMENTS");
			poiDoc = lovsService.findByLovsByLovName("POI DOCUMENTS");
			poaDoc = lovsService.findByLovsByLovName("POA DOCUMENTS");
			tenantTpesList = lovsService.findByLovsByLovName("TENANT TYPES");

			tmsHelperDTO.setAssetsList(assetsList);
			tmsHelperDTO.setCableList(cableList);
			tmsHelperDTO.setDistrictList(districtList);
			tmsHelperDTO.setGeneralDoc(generalDoc);
			tmsHelperDTO.setMandalList(mandalList);
			tmsHelperDTO.setPoaDoc(poaDoc);
			tmsHelperDTO.setPoiDoc(poiDoc);
			tmsHelperDTO.setRegionsList(regionsList);
			tmsHelperDTO.setStateList(stateList);
			tmsHelperDTO.setTenantTpesList(tenantTpesList);
			tmsHelperDTO.setVillageList(villageList);
			tmsHelperDTO.setApDistrictList(apDistrictList);
			logger.info("TmsRestController:: getLMOPage() :: END");
		} catch (Exception e) {
			logger.error("TenantController::getLMOPage() " + e);
			e.printStackTrace();
		} finally {
			stateList = null;
			apDistrictList = null;
			districtList = null;
			mandalList = null;
			villageList = null;
			cableList = null;
			assetsList = null;
			regionsList = null;
			generalDoc = null;
			poiDoc = null;
			poaDoc = null;
			tenantTpesList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getDistrictList", method = RequestMethod.GET)
	public TmsHelperDTO getDistrictList(@RequestParam("stateID") String stateID) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> districtList = null;
		try {
			logger.info("TmsRestController:: getDistrictList() :: END");
			districtList = tenantService.getDistrictList(stateID);
			tmsHelperDTO.setDistrictList(districtList);
			logger.info("TmsRestController:: getDistrictList() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getDistrictList() " + e);
		} finally {
			districtList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getMandalList", method = RequestMethod.GET)
	public TmsHelperDTO getMandalList(@RequestParam("stateID") String stateID,
			@RequestParam("districtID") String districtID) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> mandalList = null;
		try {
			logger.info("TmsRestController:: getMandalList() :: START");
			mandalList = tenantService.getMandalList(stateID, districtID);
			tmsHelperDTO.setMandalList(mandalList);
			logger.info("TmsRestController:: getMandalList() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getMandalList " + e);
		} finally {
			mandalList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getmandalAndSubsList", method = RequestMethod.GET)
	public TmsHelperDTO getmandalAndSubsList(@RequestParam("stateID") String stateID,
			@RequestParam("districtID") String districtID) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, Map<String, String>> mandalAndSubsList = null;
		try {
			logger.info("TmsRestController:: getmandalAndSubsList() :: START");
			mandalAndSubsList = tenantService.getmandalAndSubsList(stateID, districtID);
			tmsHelperDTO.setMandalAndSubstationsList(mandalAndSubsList);
			logger.info("TmsRestController:: getmandalAndSubsList() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getmandalAndSubsList() :: " + e);
		} finally {
			mandalAndSubsList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getMandalbySubstation", method = RequestMethod.GET)
	public TmsHelperDTO getMandalbySubstation(@RequestParam("stateID") String stateID,
			@RequestParam("districtID") String districtID, @RequestParam("substationID") String substationID) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		String mandalID = null;
		try {
			logger.info("TmsRestController:: getMandalbySubstation() :: START");
			mandalID = tenantService.getMandalbySubstation(stateID, districtID, substationID);
			tmsHelperDTO.setMandalID(mandalID);
			logger.info("TmsRestController:: getMandalbySubstation() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getMandalbySubstation() :: " + e);
		} finally {
			mandalID = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getVillageList", method = RequestMethod.GET)
	public TmsHelperDTO getVillageList(@RequestParam("stateID") String stateID,
			@RequestParam("districtID") String districtID, @RequestParam("mandalID") String mandalID) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> villageList = null;
		try {
			logger.info("TmsRestController:: getVillageList() :: START");
			villageList = tenantService.getVillageList(stateID, districtID, mandalID);
			tmsHelperDTO.setVillageList(villageList);
		} catch (Exception e) {
			logger.error("TmsRestController:: getVillageList() " + e);
		} finally {
			villageList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getareaLovs", method = RequestMethod.GET)
	public TmsHelperDTO getareaLovs() {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> cableList = null;
		Map<String, String> stateList = null;
		try {
			logger.info("TmsRestController:: getareaLovs() :: START");
			cableList = tenantService.getcableList();
			stateList = tenantService.getstateList();
			tmsHelperDTO.setCableList(cableList);
			tmsHelperDTO.setStateList(stateList);
			logger.info("TmsRestController:: getareaLovs() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getareaLovs()" + e);
		} finally {
			cableList = null;
			stateList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getassetsLovs", method = RequestMethod.GET)
	public TmsHelperDTO getassetsLovs() {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> assetsList = null;
		Map<String, String> cableList = null;
		try {
			logger.info("TmsRestController:: getassetsLovs() :: START");
			cableList = tenantService.getcableList();
			assetsList = tenantService.getassetsList();

			tmsHelperDTO.setCableList(cableList);
			tmsHelperDTO.setAssetsList(assetsList);
			logger.info("TmsRestController:: getassetsLovs() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: getassetsLovs() " + e);
		} finally {
			assetsList = null;
			cableList = null;
		}
		return tmsHelperDTO;
	}

	@Consumes("multipart/form-data")
	@RequestMapping(value = "/createTenant", method = RequestMethod.POST)
	public TmsHelperDTO CreateTenant(@RequestBody TenantVO tenantVO) throws Exception {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		String loginId = null;
		List<TenantDTO> tenantsList = null;
		String type = null;
		try {
			logger.info("TmsRestController:: CreateTenant() :: START");
			loginId = tenantVO.getLoginId();
			tenantsList = tenantService.findAllTenantsBYCreatedBy(loginId, new PageObject());
			type = tenantService.saveTenant(tenantVO, tenantVO.getTenantId());
			tenantsList = tenantService.findAllTenantsBYCreatedBy(loginId, new PageObject());
			tmsHelperDTO.setStatus(type);
			tmsHelperDTO.setTenantsLis(tenantsList);
			logger.info("TmsRestController:: CreateTenant() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: CreateTenant() " + e);
			e.printStackTrace();
		} finally {
			loginId = null;
			tenantsList = null;
			type = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/showtenantDetails", method = RequestMethod.GET)
	public TmsHelperDTO getshowTenantDetailsPage(@RequestParam(value = "tenantId", required = false) Integer tenantId) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Tenant tenant = null;
		String tenantCode = null;
		TenantVO tenantVO = null;
		List<Region> regionsList = null;
		List<Lovs> generalDoc = null;
		List<Lovs> poiDoc = null;
		List<Lovs> poaDoc = null;
		List<Lovs> tenantTpesList = null;
		List<TenantDocuments> tenantDocuments = null;
		try {
			logger.info("TmsRestController:: getshowTenantDetailsPage() :: START");
			tenant = tenantService.findByTenantId(tenantId);
			tenantCode = tenant.getTenantCode();
			tenantVO = tenantService.findByTenantId(tenantId, tenantCode, tenant.getPortalEnrllmentno());
			regionsList = regionService.findAllRegions();
			generalDoc = lovsService.findByLovsByLovName("GENERAL DOCUMENTS");
			poiDoc = lovsService.findByLovsByLovName("POI DOCUMENTS");
			poaDoc = lovsService.findByLovsByLovName("POA DOCUMENTS");
			tenantTpesList = lovsService.findByLovsByLovName("TENANT TYPES");
			tenantDocuments = tenantDocumentsService.findByTenantCode(tenantCode);

			tmsHelperDTO.setPortalAssetsList(portalassetsservice.findByEnrollmentno(tenant.getPortalEnrllmentno()));
			tmsHelperDTO.setPortalAreasList(portalAreaService.findByEnrollmentno(tenant.getPortalEnrllmentno()));
			tmsHelperDTO.setTenantVo(tenantVO);
			tmsHelperDTO.setRegionsList(regionsList);
			tmsHelperDTO.setGeneralDoc(generalDoc);
			tmsHelperDTO.setPoaDoc(poaDoc);
			tmsHelperDTO.setPoiDoc(poiDoc);
			tmsHelperDTO.setTenantTpesList(tenantTpesList);
			tmsHelperDTO.setTenantDocuments(tenantDocuments);
			logger.info("TmsRestController:: getshowTenantDetailsPage() :: END");
		} catch (Exception e) {
			logger.error("TenantController::getshowTenantDetails() " + e);
			e.printStackTrace();
		} finally {
			tenant = null;
			tenantCode = null;
			tenantVO = null;
			regionsList = null;
			generalDoc = null;
			poiDoc = null;
			poaDoc = null;
			tenantTpesList = null;
			tenantDocuments = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/approveTenant", method = RequestMethod.GET)
	public TmsHelperDTO approveTenantAgreement(@RequestParam(value = "tenantId", required = false) Integer tenantId,
			@RequestParam(value = "action", required = false) String action,
			@RequestParam(value = "loginId", required = false) String loginId) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Tenant tenant = null;
		String response = null;
		try {
			logger.info("TmsRestController:: approveTenantAgreement() :: START");
			tenant = tenantService.findByTenantId(tenantId);
			response = tenantService.updateTenantStatus(tenant, action, loginId);
			tmsHelperDTO.setStatus(response);
			logger.info("TmsRestController:: approveTenantAgreement() :: END");
		} catch (Exception ex) {
			logger.error("TmsRestController:: approveTenantAgreement()" + ex);
			tmsHelperDTO.setStatus("error");
		} finally {
			tenant = null;
			response = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/revenueSharing", method = RequestMethod.GET)
	public TmsHelperDTO revenueSharing() {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> templatesLov = null;
		Map<String, String> templatesTypeLovs = null;
		Map<String, String> chargeCodesList = null;
		try {
			logger.info("TmsRestController:: revenueSharing() :: START");
			templatesLov = revenueSharingServiceImpl.getTemplatesLov();
			templatesTypeLovs = revenueSharingServiceImpl.getTenantTypeLovs("TEMPLATE TYPES");
			chargeCodesList = revenueSharingServiceImpl.getChargeCodesByChargeLevelFlage();
			tmsHelperDTO.setTemplatesLov(templatesLov);
			tmsHelperDTO.setTemplateTypesLov(templatesTypeLovs);
			tmsHelperDTO.setChargeCodeslist(chargeCodesList);
			logger.info("TmsRestController :: revenueSharing() :: END");
		} catch (Exception exception) {
			logger.error("TmsRestController:: revenueSharing()" + exception);
			exception.printStackTrace();
		} finally {
			templatesLov = null;
			templatesTypeLovs = null;
			chargeCodesList = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getRevenueSharingLovs", method = RequestMethod.GET)
	public TmsHelperDTO getRevenueSharingLovs(@RequestParam("tempCode") String tempCode) {
		logger.info(":::In getRevenueSharingLovs :::");
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		Map<String, String> tenantTypeLovs = null;
		Map<String, String> regionLovs = null;
		Map<String, String> templatesLov = null;
		Map<String, String> tenantTypeVals = null;
		try {
			logger.info("TmsRestController :: getRevenueSharingLovs() :: START");
			tenantTypeLovs = revenueSharingServiceImpl.getTenantTypeLovs("TENANT TYPES");
			regionLovs = revenueSharingServiceImpl.getRegionLovs();
			templatesLov = revenueSharingServiceImpl.getTemplatesLov();
			tenantTypeVals = revenueSharingServiceImpl.getTenantTypeVals(tempCode);
			tmsHelperDTO.setTenantTypeLovs(tenantTypeLovs);
			tmsHelperDTO.setRegionLovs(regionLovs);
			tmsHelperDTO.setTemplatesLov(templatesLov);
			tmsHelperDTO.setTenantTypeVals(tenantTypeVals);
			logger.info("TmsRestController :: getRevenueSharingLovs() :: END");
		} catch (Exception exception) {
			logger.error("TmsRestController :: getRevenueSharingLovs()" + exception);
			exception.printStackTrace();
		} finally {
			tenantTypeLovs = null;
			regionLovs = null;
			templatesLov = null;
			tenantTypeVals = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/saveTemplateMaster", method = RequestMethod.POST, headers = {
			"content-type=application/json" })
	public TmsHelperDTO saveTemplateMaster(HttpServletRequest request,
			@RequestBody RevenueSharingTemplateMaster rsTemplateMaster) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		String returnVal = null;
		String s = null;
		try {
			logger.info("TmsRestController :: saveTemplateMaster() :: START");
			returnVal = revenueSharingServiceImpl.saveTemplateMaster(rsTemplateMaster);
			tmsHelperDTO.setStatus(returnVal);
			logger.info("TmsRestController :: saveTemplateMaster() :: END");
		} catch (DataIntegrityViolationException e) {
			s = e.getCause().getCause().getCause().getMessage().toString();
			if (s.contains("Duplicate entry"))
				tmsHelperDTO.setStatus("DuplicateEntry");
			else {
				logger.error("TmsRestController::saveTemplateMaster() " + e);
				tmsHelperDTO.setStatus("failure");
				e.printStackTrace();
			}
		} catch (Exception exc) {
			logger.error("TmsRestController::saveTemplateMaster() " + exc);
			tmsHelperDTO.setStatus("failure");
			exc.printStackTrace();
		} finally {
			returnVal = null;
			s = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/saveTemplateDetails", method = RequestMethod.POST, headers = {
			"content-type=application/json" })
	public TmsHelperDTO saveTemplateDetails(HttpServletRequest request,
			@RequestBody RsTemplateDetailsDTO rsTemplateDetailsDTO) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: saveTemplateDetails() :: START");
			revenueSharingServiceImpl.saveTemplateDetails(rsTemplateDetailsDTO);
			tmsHelperDTO.setStatus("success");
			logger.info("TmsRestController :: saveTemplateDetails() :: END");
		} catch (Exception exc) {
			logger.error("tenantController::saveTemplateDetails() " + exc);
			tmsHelperDTO.setStatus("failure");
			exc.printStackTrace();
		} finally {
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getTemplateValues", method = RequestMethod.GET)
	public RevenueSharingTemplateMaster getTemplateValues(@RequestParam("tempCode") String tempCode) {
		RevenueSharingTemplateMaster rsTempMaster = null;
		try {
			logger.info("TmsRestController :: getTemplateValues() :: START");
			rsTempMaster = revenueSharingServiceImpl.getTemplateValues(tempCode);
			logger.info("TmsRestController :: getTemplateValues() :: END");
		} catch (Exception exception) {
			logger.error("TmsRestController :: getTemplateValues()" + exception);
			exception.printStackTrace();
		} finally {
		}
		return rsTempMaster;
	}

	@RequestMapping(value = "/getRegTempPercList", method = RequestMethod.GET)
	public List<RsTemplateDetailsListDTO> getRegTempPercList(@RequestParam("tempCode") String tempCode,
			@RequestParam("region") String region) {
		List<RsTemplateDetailsListDTO> rsTemplateList = null;
		try {
			logger.info("TmsRestController :: getRegTempPercList() :: START");
			rsTemplateList = revenueSharingServiceImpl.getRegTempPercList(tempCode, region);
			logger.info("TmsRestController :: getRegTempPercList() :: END");
		} catch (Exception exception) {
			logger.error("TmsRestController :: getRegTempPercList() " + exception);
			exception.printStackTrace();
		} finally {
		}
		return rsTemplateList;
	}

	@RequestMapping(value = "/saveProductAgreementURL", method = RequestMethod.POST)
	public TmsHelperDTO saveProductAgreement(@RequestBody List<ProductAgreementVO> productAgreementVO,
			HttpServletRequest request) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		String status = null;
		try {
			logger.info("TmsRestController :: saveProductAgreement() :: START");
			status = rsAgrmntsService.saveProductAgreement(productAgreementVO);
			tmsHelperDTO.setStatus(status);
			logger.info("TmsRestController :: saveProductAgreement() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController :: saveProductAgreement()" + e);
			e.printStackTrace();
		} finally {
			status = null;
		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/getTemplatesByCount", method = RequestMethod.GET)
	public List<RevenueSharingTemplateMaster> getTemplatesByCount(@RequestParam("count") Integer count) {
		return revenueSharingServiceImpl.getTemplatesByCount(count);
	}

	@RequestMapping(value = "/getTenantByTenantCode", method = RequestMethod.GET)
	public Tenant getTenantByTenantCode(@RequestParam("tenantCode") String tenantCode) {
		return tenantService.findByTenantCode(tenantCode);
	}

	@RequestMapping(value = "/getAllTenantTypeByTemplateCode", method = RequestMethod.GET)
	public List<RevenueSharingTemplateDetails> getAllTenantTypeByTemplateCode(@RequestParam("tmplCode") String tmplCode,
			@RequestParam("region") String region) {
		return revenueSharingServiceImpl.getAllTenantTypeByTemplateCode(tmplCode, region);
	}

	@RequestMapping(value = "/getAllTemplateRegions", method = RequestMethod.GET)
	@ResponseBody
	public List<Object[]> getAllTemplateRegions(@RequestParam("tmplCode") String tmplCode) {
		return revenueSharingServiceImpl.getAllTemplateRegions(tmplCode);
	}

	@RequestMapping(value = "/getAllTenantsByTemplCode", method = RequestMethod.GET)
	@ResponseBody
	public List<TemplatePartnerVO> getAllTenantsByTemplCode(@RequestParam("tmplCode") String tmplCode) {
		return revenueSharingServiceImpl.getAllTenantsByTemplCode(tmplCode);
	}

	@RequestMapping(value = "/getAllSimilarTemplatesByTemplCode", method = RequestMethod.GET)
	@ResponseBody
	public List<RevenueSharingTemplateMaster> getAllSimilarTemplatesByTemplCode(
			@RequestParam(value = "templCode") String templCode) {
		return revenueSharingServiceImpl.getAllSimilarTemplatesByTemplCode(templCode);
	}

	@RequestMapping(value = "/viewAllPkgAgree", method = RequestMethod.GET)
	public List<ProductAgreementVO> getviewAllPkgAgree(HttpServletRequest request,
			@RequestParam(value = "tenantcode") String tenantcode,
			@RequestParam(value = "tenantType") String tenantType) {
		List<ProductAgreementVO> prodAgreeList = new ArrayList<>();
		try {
			logger.info("TmsRestController :: getviewAllPkgAgree() :: START");
			prodAgreeList = rsAgrmntsService.viewAllPkgAgree(tenantcode, tenantType);
			logger.info("TmsRestController :: getviewAllPkgAgree() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController::getviewAllPkgAgree() " + e);
			e.printStackTrace();
		} finally {
		}
		return prodAgreeList;

	}

	@RequestMapping(value = "/sendMail", method = RequestMethod.GET)
	public List<MailServiceDTO> sendMail(@RequestParam(value = "tenantType", required = false) String tenantType) {
		List<MailServiceDTO> mailServiceList = new ArrayList<>();
		List<Tenant> tenantList = null;
		MailServiceDTO mailServiceDTO = null;
		String response = "";
		try {
			logger.info("TmsRestController :: sendMail() :: START ");
			tenantList = tenantService.getTempTenantsbyStatus(tenantType, 1);
			logger.info("TmsRestController Fetching Completed... tenantList size: " + tenantList.size());
			int i = 1;
			for (Tenant tenant : tenantList) {
				response = tmsHelper.createUser(tenant, umsURL, "apfadmin");
				mailServiceDTO = new MailServiceDTO();
				mailServiceDTO.setTenantCode(tenant.getTenantCode());
				mailServiceDTO.setEmail(tenant.getEmailId1());
				if (response.equalsIgnoreCase("successmail")) {
					mailServiceDTO.setStatus("Success");
					logger.info("Tenant Code: " + tenant.getTenantCode() + " Success: " + i);
				} else if (response.equalsIgnoreCase("failuremail")) {
					mailServiceDTO.setStatus("Fail");
					logger.info("Tenant Code: " + tenant.getTenantCode() + " Fail: " + i);
				}

				mailServiceList.add(mailServiceDTO);
				i++;
			}
			logger.info("TmsRestController :: sendMail() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController :: sendMail() " + ex);
		} finally {
			response = null;
			tenantList = null;
			mailServiceDTO = null;
		}
		return mailServiceList;
	}

	/**
	 * Andhra Bank APIs
	 */
	@RequestMapping(value = "/verifyLMO", method = RequestMethod.GET)
	public Map<String, Map<String, Object>> verifyLMO(HttpServletRequest request,
			@RequestParam(value = "LMOcode") String lmoCode,
			@RequestParam(value = "MobileNumber") String mobileNumber) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		try {
			logger.info("TenantCoTmsRestControllerntroller :: verifyLMO() :: START ");

			map = tenantService.verifyLMO(lmoCode, mobileNumber);
		} catch (Exception e) {
			logger.error("TenantRestController::verifyLMO() " + e);
			e.printStackTrace();
		} finally {
		}
		return map;

	}

	@RequestMapping(value = "/creditLmoWallet", method = RequestMethod.POST)
	public Map<String, Map<String, Object>> creditLmoWallet(@RequestBody CreditLmoWallet creditLmoWallet, HttpServletRequest request) {
		Map<String, Map<String, Object>> map = new LinkedHashMap<>();
		try {
			String encryptValue = request.getHeader("EncryptValue");
			creditLmoWallet.setEncryptVal(encryptValue);
			logger.info("TenantRestController :: creditLmoWallet() :: START ");
			map = tenantService.creditLmoWallet(creditLmoWallet);
			if (!map.get(Response.responseStatus.name()).get(Response.statusCode.name()).equals(201)) {
				TenantsWalletTransErrors tenantsWalletTransErrors = new TenantsWalletTransErrors(creditLmoWallet,map.get(Response.responseStatus.name()).get(Response.statusMessage.name()).toString() );
				tenantService.saveOrUpdate(tenantsWalletTransErrors);
			}
		} catch (Exception e) {
			logger.error("TenantRestController::creditLmoWallet() " + e);
			e.printStackTrace();
		} finally {
		}
		return map;
	}

	@RequestMapping(value = "/searchCafDetailsForBlockList", method = RequestMethod.GET)
	public BlackListResultVO searchCafDetailsForBlockList(HttpServletRequest request,
			@RequestParam(value = "mobileNo", required = false) String mobileNo,
			@RequestParam(value = "tenantcode", required = false) String tenantcode,
			@RequestParam(value = "stbNo", required = false) String stbNo,
			@RequestParam(value = "tenantType", required = false) String tenantType,
			@RequestParam(value = "stbMac", required = false) String stbMac,
			@RequestParam(value = "loginID") String loginID) {
		List<CafsForBlockListVO> cafList = new ArrayList<>();
		BlackListResultVO blackListResultVO = new BlackListResultVO();
		try {
			logger.info("TenantRestController :: searchCafDetailsForBlockList() :: START ");
			cafList = tenantService.searchCafDetailsForBlockList(mobileNo, stbNo, tenantcode, tenantType, stbMac);
			blackListResultVO.setCafsList(cafList);
			blackListResultVO.setBlackList(getBlockListedDetails(request, loginID));
			logger.info("TenantCoTmsRestControllerntroller :: searchCafDetailsForBlockList() :: END ");
		} catch (Exception e) {
			logger.error("TenantRestController :: searchCafDetailsForBlockList() " + e);
			e.printStackTrace();
		} finally {
		}
		return blackListResultVO;
	}

	@RequestMapping(value = "/BlackCustomer", method = RequestMethod.POST)
	public BlackListResultVO BlackCustomer(@RequestBody BlackListInfoVO blackListInfoVO, HttpServletRequest request) {
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		BlackListResultVO blackListResultVO = new BlackListResultVO();
		String msgResult = null;
		try {
			logger.info("TenantRestController :: BlackCustomer() :: START ");
			String countStatus = tenantService.checkCustomer(blackListInfoVO.getStbcafno());
			if (countStatus.equalsIgnoreCase("2") || countStatus.isEmpty()) {
				tenantService.BlackCustomer(blackListInfoVO, countStatus);
				msgResult = "success";
			} else if(countStatus.equalsIgnoreCase("1")){
				msgResult = "blocked";
			} else {
				msgResult = "exist";
			}
			BlackedcafList = tenantService.getBlockListedDetails(blackListInfoVO.getCreatedby());
			blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.info("TenantRestController :: BlackCustomer() :: END ");
		} catch (Exception e) {
			msgResult = "fail";
			BlackedcafList = tenantService.getBlockListedDetails(blackListInfoVO.getCreatedby());
			blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.error("TenantRestController::BlackCustomer() " + e);
			e.printStackTrace();
		} finally {
			BlackedcafList = null;
			msgResult = null;
		}
		return blackListResultVO;
	}

	@RequestMapping(value = "/getBlockListedDetails", method = RequestMethod.GET)
	public List<BlackListInfoVO> getBlockListedDetails(HttpServletRequest request,
			@RequestParam(value = "loginID") String loginID) {
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		try {
			logger.info("TenantRestController :: searchBlockedDetails() :: START ");
			BlackedcafList = tenantService.getBlockListedDetails(loginID);
			logger.info("TenantRestController :: searchBlockedDetails() :: END ");
		} catch (Exception e) {
			logger.error("TenantRestController :: searchBlockedDetails() " + e);
			e.printStackTrace();
		} finally {
		}
		return BlackedcafList;
	}

	@RequestMapping(value = "/viewDataToapproveBlockList", method = RequestMethod.GET)
	public List<BlackListInfoVO> viewDataToapproveBlockList(HttpServletRequest request,
			@RequestParam(value = "loginID") String loginID) {
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		try {
			logger.info("TenantRestController :: viewDataToapproveBlockList() :: START ");
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginID);
			logger.info("TenantRestController :: viewDataToapproveBlockList() :: END ");
		} catch (Exception e) {
			logger.error("TenantRestController::viewDataToapproveBlockList() " + e);
			e.printStackTrace();
		} finally {
		}
		return BlackedcafList;
	}

	@RequestMapping(value = "/MakeApproveBlackList", method = RequestMethod.GET)
	public BlackListResultVO MakeApproveBlackList(HttpServletRequest request,
			@RequestParam(value = "effectivefrom") String effectivefrom,
			@RequestParam(value = "loginid") String loginid,
			@RequestParam(value = "nwsubscode") String nwsubscode,
			@RequestParam(value = "stbcafno") String stbcafno){
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		BlackListResultVO blackListResultVO = new BlackListResultVO();
		String ApprovedIP = request.getRemoteAddr();
		String msgResult = null;
		try {
			logger.info("TenantRestController :: MakeApproveBlackList() :: START ");
			String msg = tenantService.MakeApproveBlackList(effectivefrom, loginid, ApprovedIP, nwsubscode, stbcafno);
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			msgResult = msg;
			blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.info("TenantRestController :: MakeApproveBlackList() :: END ");
		} catch (Exception e) {
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			msgResult = "fail";
			blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.error("TenantRestController :: MakeApproveBlackList() " + e);
			e.printStackTrace();
		} finally {
			BlackedcafList = null;
			ApprovedIP = null;
			msgResult = null;
		}
		return blackListResultVO;
	}

	@RequestMapping(value = "/blackListCorpusFail", method = RequestMethod.GET)
	public BlackListResultVO blackListCorpusFail(HttpServletRequest request, @RequestParam(value = "loginid") String loginid) {
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		BlackListResultVO blackListResultVO = new BlackListResultVO();
		String msgResult = null;
		try {
			logger.info("TenantRestController :: MakeApproveBlackList() :: START ");
			//String msg = tenantService.blackListCorpusFail(custid, effectivefrom, loginid, ApprovedIP, affectedcafs);
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			//msgResult = msg;
			//blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.info("TenantRestController :: MakeApproveBlackList() :: END ");
		} catch (Exception e) {
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			msgResult = "fail";
			blackListResultVO.setMsgResult(msgResult);
			blackListResultVO.setBlackList(BlackedcafList);
			logger.error("TenantRestController :: MakeApproveBlackList() " + e);
			e.printStackTrace();
		} finally {
			BlackedcafList = null;
			msgResult = null;
		}
		return blackListResultVO;
	}

	@RequestMapping(value = "/RejectBlackList", method = RequestMethod.GET)
	public List<BlackListInfoVO> RejectBlackList(HttpServletRequest request,
			@RequestParam(value = "effectivefrom") String effectivefrom,
			@RequestParam(value = "loginid") String loginid,
			@RequestParam(value = "nwsubscode") String nwsubscode,
			@RequestParam(value = "stbcafno") String stbcafno){
		List<BlackListInfoVO> BlackedcafList = new ArrayList<>();
		String ApprovedIP = request.getRemoteAddr();
		try {
			logger.info("TenantRestController :: RejectBlackList() :: START ");
			tenantService.RejectBlackList(effectivefrom, loginid, ApprovedIP, stbcafno);
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			logger.info("TenantRestController :: RejectBlackList() :: END ");
		} catch (Exception e) {
			BlackedcafList = tenantService.viewDataToapproveBlockList(loginid);
			logger.error("TenantRestController::RejectBlackList() " + e);
			e.printStackTrace();
		} finally {
			ApprovedIP = null;
		}
		return BlackedcafList;
	}

	@RequestMapping(value = "/getOltPortsByTenantCode", method = RequestMethod.GET)
	public List<String> getOltPortsByTenantCode(@RequestParam(value = "tenantcode") String tenantcode,
			@RequestParam(value = "oltSerno") String oltSerno) {
		List<String> portList = new ArrayList<>();
		try {
			logger.info("TenantRestController :: RejectBlackList() :: START ");
			portList = tenantService.getOltPortsByTenantCode(tenantcode, oltSerno);
			logger.info("TenantRestController :: RejectBlackList() :: END ");
		} catch (Exception e) {
			logger.error("TenantRestController::RejectBlackList() " + e);
			e.printStackTrace();
		} finally {
		}
		return portList;
	}

	@RequestMapping(value = "/getOltSrlNosByTenantCode", method = RequestMethod.GET)
	public List<Object[]> getOltSrlNosByTenantCode(@RequestParam(value = "tenantcode") String tenantcode) {
		List<Object[]> list = new ArrayList<>();
		try {
			logger.info("TenantRestController :: RejectBlackList() :: START ");
			list = tenantService.getOltSrlNosByTenantCode(tenantcode);
			logger.info("TenantRestController :: RejectBlackList() :: END ");
		} catch (Exception e) {
			logger.error("TenantRestController::RejectBlackList() " + e);
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	@RequestMapping(value = "/getL1SplitterByOltport", method = RequestMethod.GET)
	public TmsHelperDTO getL1SplitterByOltport(HttpServletRequest request, @RequestParam("oltPortNo") String oltPortNo,
			@RequestParam("oltSerNo") String oltSerNo, @RequestParam("tenantCode") String tenantCode) {
		Map<String, Object> map = null;
		TmsHelperDTO tmsHelper = new TmsHelperDTO();
		try {
			map = tenantService.getL1SplitterByOltport(oltPortNo, oltSerNo, tenantCode);
		} catch (Exception e) {
			logger.error("TmsController :: getL1SplitterByOltport() :: " + e);
			e.printStackTrace();
		}
		tmsHelper.setMap(map);
		return tmsHelper;
	}

	@RequestMapping(value = "/getL1SplitterIdsByOltport", method = RequestMethod.GET)
	public @ResponseBody List<String> getL1SplitterIdsByOltport(HttpServletRequest request,
			@RequestParam("l1Size") String l1Size, @RequestParam("oltSerNo") String oltSerNo,
			@RequestParam("oltPortNo") String oltPortNo, @RequestParam("tenantcode") String tenantCode) {
		List<String> list = null;
		try {
			list = tenantService.getL1SplitterIdsByOltport(l1Size, oltSerNo, oltPortNo, tenantCode);
		} catch (Exception e) {
			logger.error("TmsController :: getL1SplitterByOltport() :: " + e);
			e.printStackTrace();
		}

		return list;
	}

	@RequestMapping(value = "/getL2SplitterByL1Value", method = RequestMethod.GET)
	public List<String> getL2SplitterByL1Value(HttpServletRequest request, @RequestParam("oltPortNo") String oltPortNo,
			@RequestParam("oltSerNo") String oltSerNo, @RequestParam("tenantcode") String tenantCode) {
		List<String> list = null;
		try {
			list = tenantService.getL2SplitterByL1Value(tenantCode, oltPortNo, oltSerNo);
		} catch (Exception e) {
			logger.error("TmsController :: getL1SplitterByOltport() :: " + e);
			e.printStackTrace();
		}

		return list;
	}

	@RequestMapping(value = "/savePortSplitter", method = RequestMethod.GET)
	public String savePortSplitter(HttpServletRequest request, @RequestParam("oltSrno") String oltSrno,
			@RequestParam("oltportNo") String oltportNo, @RequestParam("l2PortSize") String l2PortSize,
			@RequestParam("tenantcode") String tenantCode, @RequestParam("l1PortId") String l1PortId,
			@RequestParam("l1SlotDtails") String l1SlotDtails) {

		String returnVal = null;
		try {

			returnVal = tenantService.savePortSplitter(tenantCode, oltportNo, oltSrno, l2PortSize, l1PortId,
					l1SlotDtails);
		} catch (Exception e) {
			returnVal = e.getMessage();
			logger.error("TmsController :: getL1SplitterByOltport() :: " + e);
			e.printStackTrace();
		}

		return returnVal;
	}

	@RequestMapping(value = "/mspChannelUpload", method = RequestMethod.POST)
	public TmsHelperDTO mspChannelUpload(HttpServletRequest request, @RequestParam(value = "loginID") String loginID,
			@RequestBody TmsHelperDTO tmsHelperDTO) throws Exception {
		TmsHelperDTO tmsHelper = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: mspChannelUpload() :: START");
			tmsHelper = tenantService.saveMspChnlsUploadHistory(tmsHelperDTO, loginID);
			logger.info("TmsRestController :: mspChannelUpload() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController::mspChannelUpload() " + e);
			e.printStackTrace();
		}
		return tmsHelper;
	}

	@RequestMapping(value = "/viewMspChannels", method = RequestMethod.GET)
	public TmsHelperDTO viewMspChannels() {
		List<UploadHistory> uploadHistory = new ArrayList<>();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: viewMspChannels() :: START");

			uploadHistory = tenantService.viewMspChannels();
			tmsHelperDTO.setUploadHistory(uploadHistory);
			logger.info("TmsRestController :: viewMspChannels() :: END");
		} catch (Exception e) {
			logger.info("The Exception is ComsRestController :: showcustomers" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}

	@RequestMapping(value = "/mspChannelsErrorDownload", method = RequestMethod.GET)
	public TmsHelperDTO mspChannelsErrorDownload(@RequestParam(value = "uploadid", required = false) String uploadid) {
		List<MspChnlsStg> channelErrorList = new ArrayList<>();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: mspChannelsErrorDownload() :: START");
			channelErrorList = tenantService.mspChannelUploadErrors(uploadid);
			logger.info("TmsRestController :: mspChannelsErrorDownload() :: END");
			tmsHelperDTO.setMspChnlsStgList(channelErrorList);
		} catch (Exception e) {
			logger.info("The Exception is TmsRestController :: mspChannelsErrorDownload" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/viewMspChannelByLoginId", method = RequestMethod.GET)
	public TmsHelperDTO viewMspChannelByLoginId(@RequestParam(value="loginId") String loginId) throws IOException {
		List<MspChnlsStg> channelsList = new ArrayList<>();
		List<AdditionalService> additionalServiceList = new ArrayList<>();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController:: home() :: START ");
			channelsList = tenantService.viewMspChannelByLoginId(loginId);
			additionalServiceList=tenantService.getpackageInfo(loginId);
			tmsHelperDTO.setAdditionalService(additionalServiceList);
			tmsHelperDTO.setMspChnlsStgList(channelsList);
			logger.info("TmsRestController:: home() :: END ");
		} catch (Exception ex) {
			logger.error("TmsRestController:: home()" +ex);
		}finally {}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/executeRevSharing", method = RequestMethod.GET)
	public TmsHelperDTO executeRevSharing(@RequestParam(value = "revDate", required = true) String revDate) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: executeRevSharing() :: START");
			tmsHelperDTO=tenantService.executeRevSharing(revDate);
			tmsHelperDTO.setRevenueSharingDTOList(tenantService.getRevenueSharingList(revDate));
			logger.info("TmsRestController :: executeRevSharing() :: END");
		} catch (Exception e) {
			logger.info("The Exception is TmsRestController :: executeRevSharing" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/achFileGenerate", method = RequestMethod.GET)
	public TmsHelperDTO achFileGenerate(@RequestParam(value = "revDate", required = true) String revDate) {
		List<RevenueSharingDTO> list;
		String returnVal = "";
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController :: achFileGenerate() :: START");
			list = tenantService.getRevenueSharingDTOList(revDate);
			returnVal = tenantService.achFileGenerate(revDate,list);
			
			tmsHelperDTO.setRevenueSharingDTOList(tenantService.getRevenueSharingList(revDate));
			tmsHelperDTO.setMessage(returnVal);
			logger.info("TmsRestController :: achFileGenerate() :: END");
		} catch (Exception e) {
			logger.info("The Exception is TmsRestController :: achFileGenerate" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/oltPortAllocation", method = RequestMethod.GET)
	public List<Districts> oltPortAllocation(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<Districts> districtList = new ArrayList<>();
		try {
			logger.info("TmsRestController:: oltPortAllocation() :: END");
			districtList = tenantService.getAllDistricts();
			logger.info("TmsRestController:: oltPortAllocation() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: oltPortAllocation() " + e);
		} finally {
		}
		return districtList;
	}
	
	@RequestMapping(value = "/getMandalsByDistrictId")
	public List<Mandals> getMandalsByDistrictId(
			@RequestParam(value = "districtId", required = false) Integer districtId) {
		List<Mandals> mandalsList = new ArrayList<>();
		try {
			logger.info("TmsRestController :: getMandalsByDistrictId() :: START");
			mandalsList = tenantService.getMandalsByDistrictId(districtId);
			logger.info("TmsRestController :: getMandalsByDistrictId() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController :: getMandalsByDistrictId() :::" + e);
			e.printStackTrace();
		} finally {

		}
		return mandalsList;
	}
	
	@RequestMapping(value = "/searchOltsDetails", method = RequestMethod.GET)
	public TmsHelperDTO searchOltsDetails(@RequestParam(value = "districtuid", required = false) Integer districtuid,
			@RequestParam(value = "mandalslno", required = false) Integer mandalslno) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		List<OLT> oltList = new ArrayList<>();
		List<Districts> districtList = new ArrayList<>();
		List<Mandals> mandalsList = new ArrayList<>();
		try {
			logger.info("TmsRestController :: searchOLTDetails() :: START");
			oltList = tenantService.searchOltsDetails(districtuid,mandalslno);
			districtList = tenantService.getAllDistricts();
			mandalsList = tenantService.getMandalsByDistrictId(districtuid);
			tmsHelperDTO.setDistrictsList(districtList);
			tmsHelperDTO.setMandalsList(mandalsList);
			tmsHelperDTO.setOltList(oltList);
			logger.info("TmsRestController :: searchCafDetails() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController::searchCafDetails() " + e);
			e.printStackTrace();
		} finally {
			oltList = null;
			districtList = null;
		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/getOltDetails", method = RequestMethod.GET)
	public TenantVO getOltDetails(@RequestParam(value = "suid", required = false) String suid,
				@RequestParam(value = "oltno", required = false) String oltno) {
		TenantVO tenantVO = new TenantVO();
		try {
			logger.info("TmsRestController :: getOltDetails() :: START");
			tenantVO =tenantService.getOltDetails(suid,oltno);
			logger.info("TmsRestController :: getOltDetails() :: END");
		} catch (Exception e) {
			logger.info(":::TmsRestController -- getOltDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return tenantVO;
	}
	
	@RequestMapping(value = "/savePortDetailsList", method = RequestMethod.POST)
	public TmsHelperDTO savePortDetailsList(@RequestBody List<OLTPortDetails> oLTPortDetails, HttpServletRequest request
		,@RequestParam(value = "loginID", required = false) String loginID)
			 {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		String status = null;
		try{
			logger.info("TmsRestController :: savePortDetailsList() :: START");
			  status =   tenantService.savePortDetailsList(oLTPortDetails, loginID);
			tmsHelperDTO.setStatus(status);
			logger.info("TmsRestController :: savePortDetailsList() :: END");
		} catch(Exception e)
		{  
			logger.error("TmsRestController :: savePortDetailsList()" + e);
			e.printStackTrace();
			} finally {
				status = null;
			}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/cpeAllocation", method = RequestMethod.GET)
	public TmsHelperDTO cpeAllocation(@RequestParam(value = "tenantCode", required = false) String tenantCode) {
		List<Tenant> msptenantList = new ArrayList<>();
		List<Tenant> lmotenantList = new ArrayList<>();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		try {
			logger.info("TmsRestController:: cpeAllocation() :: START");
			msptenantList = tenantService.getAllMspTenants();
			lmotenantList = tenantService.getAllLmoTenants();
			tmsHelperDTO.setMspList(msptenantList);
			tmsHelperDTO.setLmoList(lmotenantList);
			logger.info("TmsRestController:: cpeAllocation() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController:: cpeAllocation() " + e);
		} finally {
		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/searchcpeDetails", method = RequestMethod.POST)
	public TmsHelperDTO searchcpeDetails(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "tenantType", required = false) String tenantType,
			@RequestParam(value = "tenantCode", required = false) String tenantCode, 
			@RequestParam(value = "cpeType", required = false) String cpeType,
			@RequestParam(value = "cpeSerialNumber", required = false) String cpeSerialNumber,
			@RequestBody CpeHelperDTO cpeHelperDto
			 ) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		List<CpeStockVO> cpeList = new ArrayList<>();
		List<CpeStockVO> cpeFinalList = new ArrayList<>();
		
		
		try {
			logger.info("TmsRestController :: searchCafDetails() :: START");
			cpeList = tenantService.getCPEStockDetails(fromDate,toDate,tenantType,tenantCode,cpeType,cpeSerialNumber,cpeHelperDto.getCpeSlnoList());
			//code for cpe stock
			
				for (CpeStockVO cpeStockVO : cpeList) {
					
					if (cpeStockVO.getStatus().equals("2") || cpeStockVO.getStatus().equals("3") || cpeStockVO.getStatus().equals("99")) {//includes only caf not done cpe stock
						cpeFinalList.add(cpeStockVO);
					}
				}
				cpeList.clear();
			
		
			tmsHelperDTO.setCpeStockList(cpeFinalList);
			logger.info("TmsRestController :: searchCafDetails() :: END");
		} catch (Exception e) {
			logger.error("TmsRestController::searchCafDetails() " + e);
			e.printStackTrace();
		} finally {
			cpeList = null;
		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/updateCpeDetails", method = RequestMethod.POST)
	public TmsHelperDTO updateCpeDetails(@RequestBody List<CpeStockVO> cpeStockList,@RequestParam String tenantCode) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		
		String status="";
		try {
			logger.info("TmsRestController :: updateCpeDetails() :: START");
			status =  tenantService.updateCpeDetails(cpeStockList,tenantCode);
			tmsHelperDTO.setStatus(status);
			logger.info("TmsRestController :: updateCpeDetails() :: END");
		} catch (Exception e) {
			logger.error(":::TmsRestController -- updateCpeDetails :::" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}
	
	@RequestMapping(value = "/deleteCpeStocks", method = RequestMethod.POST)
	public CpeHelperDTO deleteCpeStocks(@RequestBody List<CpeStockVO> cpeStockList, @RequestParam  String tenantCode,@RequestParam  String loginid) {
		
		
		CpeHelperDTO cpeHelperDto = new CpeHelperDTO();
		cpeHelperDto.setTenantCode(tenantCode);
		String status="";
		try {
			logger.info("TmsRestController :: deleteCpeStocks() :: START");
			status =  tenantService.deleteCpeList(cpeHelperDto,cpeStockList,loginid);
			
			cpeHelperDto.setStatus(status);
			
			logger.info("TmsRestController :: deleteCpeStocks() :: END");
		} catch (Exception e) {
			logger.error(":::TmsRestController -- deleteCpeStocks ::" + e);
			e.printStackTrace();
		} 

		return cpeHelperDto;
	}
	

	
	@RequestMapping(value = "/UpdateCustInvList", method = RequestMethod.GET)
	public String UpdateCustInvList(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {

//		String fromDate="2017-12-01";
//		String toDate="2018-03-31";
		long monthFirstdate;
		long monthEndDate;
		try {
			 
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); 
			Calendar calendar=	 new GregorianCalendar();
			calendar.set(Integer.parseInt(invyr), Integer.parseInt(invmn)-1, 1);
			monthFirstdate = calendar.getTimeInMillis();
			 Date date=new Date(monthFirstdate);
		        SimpleDateFormat df2 = new SimpleDateFormat("yyyy/MM/dd");
		        String monthfirstdate = df2.format(date);
			
			Date convertedDate = dateFormat.parse(monthfirstdate);
			Calendar c = Calendar.getInstance();
			c.setTime(convertedDate);
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			monthEndDate=c.getTimeInMillis();
			 Date date2=new Date(monthEndDate);
			 String monthendDate = df2.format(date2);
			
			
			logger.info("TmsRestController :: UpdateCustInvList() :: START");
			tenantService.UpdateCustInvList(invyr,invmn,monthfirstdate,monthendDate);
			
			logger.info("TmsRestController :: UpdateCustInvList() :: END");
		} catch (Exception e) {
			logger.error(":::TmsRestController -- UpdateCustInvList :::" + e);
			e.printStackTrace();
		} finally {

		}
		return "";
	}
	
	
	@RequestMapping(value = "/getMonthWiseShares", method = RequestMethod.GET)
	public TmsHelperDTO getMonthWiseShares(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {
		List<CustomerInvDtlsDTO> custinvDtls = new ArrayList<CustomerInvDtlsDTO>();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
//		String fromDate="2017-12-01";
//		String toDate="2018-03-31";
		try {
			logger.info("TmsRestController :: getMonthWiseShares() :: START");
			custinvDtls=tenantService.getMonthWiseShares(invyr,invmn);
			tmsHelperDTO.setCustomerInvDtlsDTO(custinvDtls);
			
			logger.info("TmsRestController :: getMonthWiseShares() :: END");
		} catch (Exception e) {
			logger.error(":::TmsRestController -- getMonthWiseShares :::" + e);
			e.printStackTrace();
		} finally {

		}
		return tmsHelperDTO;
	}
	
	
	@RequestMapping(value = "/setMonthWiseShares", method = RequestMethod.GET)
	public String setMonthWiseShares(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {
		boolean success = true;
		String message="";

		try {
			logger.info("TmsRestController :: setMonthWiseShares() :: START");
			success=tenantService.setMonthWiseShares(invyr,invmn);
			if (success)
				message="Successfully Inserted";
			else
				message="Insert failed";
			logger.info("TmsRestController :: setMonthWiseShares() :: END");
		} catch (Exception e) {
			logger.error(":::TmsRestController -- setMonthWiseShares :::" + e);
			e.printStackTrace();
		} finally {

		}
		return message;
	}
	

	
	@RequestMapping(value = "/setPaidFlagForOverPaidCustomer", method = RequestMethod.GET)
	public String setPaidFlagForOverPaidCustomer(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {
		
		String message="";

		try {
			logger.info("TmsRestController :: setPaidFlagForOverPaidCustomer() :: START");
			tenantService.setPaidFlagForOverPaidCustomer(invyr,invmn);
			
				message="Successfully updated";
		
			logger.info("TmsRestController :: setPaidFlagForOverPaidCustomer() :: END");
		} catch (Exception e) {
			message="Insert failed";
			logger.error(":::TmsRestController -- setPaidFlagForOverPaidCustomer :::" + e);
			e.printStackTrace();
		} finally {

		}
		return message;
	}
	
	
	
	
	
	@RequestMapping(value = "/updateTenantsWalletOnFirst", method = RequestMethod.GET)
	public String updateTenantsWalletOnFirst(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {
		
		String message="";

		try {
			logger.info("TmsRestController :: updateTenantsWalletOnFirst() :: START");
			tenantWalletService.updateTenantsWalletOnFirst(invyr,invmn);
			
				message="Successfully updated";
		
			logger.info("TmsRestController :: updateTenantsWalletOnFirst() :: END");
		} catch (Exception e) {
			message="Insert failed";
			logger.error(":::TmsRestController -- updateTenantsWalletOnFirst :::" + e);
			e.printStackTrace();
		} finally {

		}
		return message;
	}
	
	
	
	@RequestMapping(value = "/updateTenantsWalletOnEndOfMonth", method = RequestMethod.GET)
	public String updateTenantsWalletOnEndOfMonth(@RequestParam(value = "invfromdate", required = false) String invyr,
			@RequestParam(value = "invtodate", required = false) String invmn) {
		
		String message="";

		try {
			logger.info("TmsRestController :: updateTenantsWalletOnEndOfMonth() :: START");
			if (tenantWalletService.updateTenantsWalletOnEndOfMonth(invyr,invmn))
			message="Successfully updated";
			else message="No Data";
				
		
			logger.info("TmsRestController :: updateTenantsWalletOnEndOfMonth() :: END");
		} catch (Exception e) {
			message="Insert failed";
			logger.error(":::TmsRestController -- updateTenantsWalletOnEndOfMonth :::" + e);
			e.printStackTrace();
		} finally {

		}
		return message;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * @author rajesh s
	 * @param logincode
	 * This API obtain data from rev_summary table on LMO basis
	 * @return monthly wise LMO apsfl  dues 
	 */
	@RequestMapping(value = "/getLMORevenueShare", method = RequestMethod.GET)
	public List<LMORevSummaryVO> getLMORevenueShare(@RequestParam("lmocode") String lmocode) {
		logger.info(":::In getLMORevenueShare :::");
		List<TenantLMORevServiceDTO> lmoRevserviceDTOList = null;
		List<LMORevSummaryVO> lmoSummaryVOList = new ArrayList<>();
		try {
			logger.info("TmsRestController :: getLMORevenueShare() :: START");
			lmoRevserviceDTOList = revenueSharingServiceImpl.getLMORevShare(lmocode);
			for (TenantLMORevServiceDTO tenantLMORevServiceDTO : lmoRevserviceDTOList) {
				LMORevSummaryVO lmoRevShare = new LMORevSummaryVO();
				lmoRevShare.setYear(tenantLMORevServiceDTO.getYear().toString());
				lmoRevShare.setMonth(tenantLMORevServiceDTO.getMonth().toString());
				lmoRevShare.setApsflShare(tenantLMORevServiceDTO.getApsflShare().toString());
				lmoSummaryVOList.add(lmoRevShare);
			}
			logger.info("TmsRestController :: getLMORevenueShare() :: END");
		} catch (Exception exception) {
			logger.error("TmsRestController :: getLMORevenueShare()" + exception);
			exception.printStackTrace();
		} finally {

		}
		return lmoSummaryVOList;
	}

	
	
	
	
	
		
	
}
