/*package com.arbiva.apfgc.tenant.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantDocsPK;
import com.arbiva.apfgc.tenant.model.TenantDocuments;

public interface TenantDocumentsDao extends JpaSpecificationExecutor<TenantDocuments>, JpaRepository<TenantDocuments, TenantDocsPK>{
	
	@Query(value="select * from tenantdocs where tenantcode=?1", nativeQuery=true)
	public abstract List<TenantDocuments> findByTenantCode(String tenantCode);
}
*/