/**
 * 
 */
package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.CoreServicesDao;
import com.arbiva.apfgc.tenant.model.CoreServices;

/**
 * @author Arbiva
 *
 */
@Component("coreServicesService")
@Transactional
public class CoreServicesServiceImpl {
	@Autowired
	CoreServicesDao coreServicesDao;
	
	/*@Autowired
	TenantServicesDaoImpl tenantServicesDaoImpl;*/
	
	public List<CoreServices> findAllCoreServices() {
		List<CoreServices> coreServices = new ArrayList<CoreServices>();
		coreServices = coreServicesDao.findAllCoreServices();
		return coreServices;
	}

	public List<CoreServices> findAllCoreServicesByTenantSrvcs(String tenantCode) {
		List<CoreServices> coreServices = coreServicesDao.findAllCoreServicesByTenantSrvcs(tenantCode);
		return coreServices;
	}

}
