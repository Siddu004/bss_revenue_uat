package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

public class ChargesAgmtIDClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long agruniqueId;
	
    private Calendar agrfDate;

	public Long getAgruniqueId() {
		return agruniqueId;
	}

	public void setAgruniqueId(Long agruniqueId) {
		this.agruniqueId = agruniqueId;
	}

	public Calendar getAgrfDate() {
		return agrfDate;
	}

	public void setAgrfDate(Calendar agrfDate) {
		this.agrfDate = agrfDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agrfDate == null) ? 0 : agrfDate.hashCode());
		result = prime * result + ((agruniqueId == null) ? 0 : agruniqueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChargesAgmtIDClass other = (ChargesAgmtIDClass) obj;
		if (agrfDate == null) {
			if (other.agrfDate != null)
				return false;
		} else if (!agrfDate.equals(other.agrfDate))
			return false;
		if (agruniqueId == null) {
			if (other.agruniqueId != null)
				return false;
		} else if (!agruniqueId.equals(other.agruniqueId))
			return false;
		return true;
	}
    
    

}
