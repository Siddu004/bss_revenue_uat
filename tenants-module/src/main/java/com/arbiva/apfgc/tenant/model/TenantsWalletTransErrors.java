package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.arbiva.apfgc.tenant.dto.CreditLmoWallet;

@Table(name = "tntswallettranerr")
@Entity
public class TenantsWalletTransErrors {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long errtranId;
	
	@Column(name ="trandate")
	private Calendar trandate;
	
	@Column(name ="tenantcode")
	private String tenantcode;
	
	@Column(name ="trantype")
	private String trantype;
	
	@Column(name ="crdb_flag")
	private Character crdb_flag;
	
	@Column(name ="tranamt")
	private BigDecimal tranamt;
	
	@Column(name ="tranmode")
	private String tranmode;

	@Column(name ="tranrefno")
	private String tranrefno;
	
	@Column(name ="walletadjamt")
	private BigDecimal walletadjamt;
	
	@Column(name ="crlimitadjamt")
	private BigDecimal crlimitadjamt;
	
	@Column(name ="errdesc")
	private String errdesc;
	
	@Column(name = "createdon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar createdon;

	@Column(name = "createdipaddr", length = 20)
    private String createdipaddr;
	
	public TenantsWalletTransErrors(){}

	public TenantsWalletTransErrors(CreditLmoWallet creditLmoWallet, String name) {
		this.setCrdb_flag('C');
		this.setCreatedon(Calendar.getInstance());
		this.setErrdesc(name);
		this.setTenantcode(creditLmoWallet.getLmoCode());
		this.setTranamt(new BigDecimal(creditLmoWallet.getAmountPaid()));
		this.setTrandate(Calendar.getInstance());
		this.setTranmode(creditLmoWallet.getTypeOfPayment());
		this.setTrantype("Wallet");
		this.setTranrefno(creditLmoWallet.getTranrefnumber());
	}

	public Long getErrtranId() {
		return errtranId;
	}

	public void setErrtranId(Long errtranId) {
		this.errtranId = errtranId;
	}

	public Calendar getTrandate() {
		return trandate;
	}

	public void setTrandate(Calendar trandate) {
		this.trandate = trandate;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getTrantype() {
		return trantype;
	}

	public void setTrantype(String trantype) {
		this.trantype = trantype;
	}

	public Character getCrdb_flag() {
		return crdb_flag;
	}

	public void setCrdb_flag(Character crdb_flag) {
		this.crdb_flag = crdb_flag;
	}

	public BigDecimal getTranamt() {
		return tranamt;
	}

	public void setTranamt(BigDecimal tranamt) {
		this.tranamt = tranamt;
	}

	public String getTranmode() {
		return tranmode;
	}

	public void setTranmode(String tranmode) {
		this.tranmode = tranmode;
	}

	public String getTranrefno() {
		return tranrefno;
	}

	public void setTranrefno(String tranrefno) {
		this.tranrefno = tranrefno;
	}

	public BigDecimal getWalletadjamt() {
		return walletadjamt;
	}

	public void setWalletadjamt(BigDecimal walletadjamt) {
		this.walletadjamt = walletadjamt;
	}

	public BigDecimal getCrlimitadjamt() {
		return crlimitadjamt;
	}

	public void setCrlimitadjamt(BigDecimal crlimitadjamt) {
		this.crlimitadjamt = crlimitadjamt;
	}

	public String getErrdesc() {
		return errdesc;
	}

	public void setErrdesc(String errdesc) {
		this.errdesc = errdesc;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}
	
	

}
