package com.arbiva.apfgc.tenant.pg.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tanent.util.SMSHelper;
import com.arbiva.apfgc.tenant.pg.businessservice.PaymentGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;

/**
 * 
 * @author srinivasa
 *
 */
@RestController
@RequestMapping("/pg")
public class PaymentGatewayController {
	
	private static final Logger LOGGER = Logger.getLogger(PaymentGatewayController.class);

	@Autowired
	private PaymentGatewayBusinessService paymentDatewayBusinessService;
	

	@Autowired
	IpAddressValues ipAddressValues;
	
	RestTemplate restTemplate = new RestTemplate();
	
	private static final String CLIENT_RESPONSE = "Client";
	private static final String SERVER_RESPONSE = "Server";

	/**
	 * 
	 * @param dto
	 * @param response
	 */
	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public String processPaymentRequest( @RequestBody RequestDTO dto, HttpServletResponse response) {
		String url = null;
		try {
			LOGGER.info("PaymentGatewayController :: processPaymentRequest() :: Start");
			url = paymentDatewayBusinessService.processPaymentRequest(dto);
			
			//TODO: pass error page url if anything went wrong.
			//response.sendRedirect(uri != null ? uri.toString() : "paymentError");
			LOGGER.info("PaymentGatewayController :: processPaymentRequest() :: End");
		} catch (Exception e) {
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processPaymentRequest()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processPaymentRequest()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Request",
								e));
			}
		}
		
		return url;
	}

	/**
	 * 
	 * @param paymentResponse
	 * @return
	 */
	@RequestMapping(value = "/bdresponse", method = RequestMethod.POST)
	public ResponseEntity<String> processBilldeskPaymentResponse( @RequestParam("msg") String paymentResponse) {
		String responseValue = "";
		try {
			LOGGER.info("PaymentGatewayController :: processBilldeskPaymentResponse() :: Start");
			LOGGER.info("Respose From BILLDESK ==> MSG ===> "+paymentResponse);
			responseValue = paymentDatewayBusinessService
					.processBilldeskPaymentResponse(paymentResponse, CLIENT_RESPONSE);
			LOGGER.info("PaymentGatewayController :: processBilldeskPaymentResponse() :: End");
		} catch (Exception e) {
			responseValue = "Failure";
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processBilldeskPaymentResponse()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processBilldeskPaymentResponse()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Response",
								e));
			}
		}
		return ResponseEntity.ok().body(responseValue);
	}
	
	/**
	 * 
	 * @param paymentResponse
	 * @return
	 */
	@RequestMapping(value = "/bdserverresponse", method = RequestMethod.POST)
	public ResponseEntity<String> processBilldeskPaymentServerResponse(  @RequestParam("msg") String paymentResponse) {
		try {
			LOGGER.info("PaymentGatewayController :: processBilldeskPaymentServerResponse() :: Start");
			LOGGER.info("BD Response :: "+paymentResponse);
			paymentDatewayBusinessService
					.processBilldeskPaymentResponse(paymentResponse, SERVER_RESPONSE);
			LOGGER.info("PaymentGatewayController :: processBilldeskPaymentServerResponse() :: End");
		} catch (Exception e) {
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processBilldeskPaymentServerResponse()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processBilldeskPaymentServerResponse()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Response",
								e));
			}
		}
		return ResponseEntity.ok()
				.body("Payment Response succefully processed");
	}
	
	/**
	 * 
	 * @param paymentResponse
	 * @return
	 */
	@RequestMapping(value = "/dsresponse", method = RequestMethod.POST)
	public ResponseEntity<String> processDigitSignaturePaymentResponse( @RequestParam("xmlstring") String paymentResponse) {
		String responseValue = "";
		LOGGER.info("Request From Digit Secure ======>"+paymentResponse);
		try {
			LOGGER.info("PaymentGatewayController :: processDigitSignaturePaymentResponse() :: Start");
			responseValue =	paymentDatewayBusinessService
					.processDigitSignaturePaymentResponse(paymentResponse, CLIENT_RESPONSE);
			LOGGER.info("PaymentGatewayController :: processDigitSignaturePaymentResponse() :: End");
		} catch (Exception e) {
			responseValue = "Failure";
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processDigitSignaturePaymentResponse()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processDigitSignaturePaymentResponse()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Response",
								e));
			}
		}
		LOGGER.info("responseValue : "+responseValue);
		return ResponseEntity.ok().body(responseValue);
	}
	
	/**
	 * 
	 * @param paymentResponse
	 * @return
	 */
	@RequestMapping(value = "/dsserverresponse", method = RequestMethod.POST, consumes = { MediaType.TEXT_PLAIN_VALUE }, produces = { MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<String> processDigitSignaturePaymentServerResponse(@RequestBody String paymentResponse) {
		try {
			LOGGER.info("PaymentGatewayController :: processDigitSignaturePaymentServerResponse() :: Start");
			paymentDatewayBusinessService
					.processDigitSignaturePaymentResponse(paymentResponse, SERVER_RESPONSE);
			LOGGER.info("PaymentGatewayController :: processDigitSignaturePaymentServerResponse() :: End");
		} catch (Exception e) {
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processDigitSignaturePaymentServerResponse()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processDigitSignaturePaymentServerResponse()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Response",
								e));
			}
		}
		return ResponseEntity.ok()
				.body("Payment Response succefully processed");
	}
	
	@RequestMapping(value = "/paymentError")
	public ResponseEntity<String> paymentError() {
		LOGGER.info("PaymentGatewayController :: paymentError() :: Start");
		return ResponseEntity.ok().body("Payment Success.........!!!!");
	}

	// ###################### These are testing API, we can remove these. ##############################
	
	@RequestMapping(value = "/test_request", method = RequestMethod.POST, consumes = { MediaType.TEXT_PLAIN_VALUE }, produces = { MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<String> processPaymentTestRequest(
			@RequestBody String paymentResponse) {
		System.out.println(paymentResponse);
		return ResponseEntity.ok()
				.body("Payment Response succefully processed");
	}
	
	@RequestMapping(value = "/request", method = RequestMethod.GET, produces = { MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<String> getPayment() {
		return ResponseEntity.ok().body("Payment Success.........!!!!");
	}
	
	/**
	 * 
	 * @param instance
	 * @return
	 */
	@RequestMapping(value = "/billdesk_request", method = RequestMethod.GET, produces = { MediaType.TEXT_PLAIN_VALUE })
	public void processPaymentRequest(@RequestParam(value="customer_id", required=true) String customerId
			, @RequestParam(value="amount", required=true) String amount,
			  @RequestParam(value="gateway_type", required=true) String gatewayType, HttpServletResponse response) {
		try {
			LOGGER.info("PaymentGatewayController :: processPaymentRequest() :: Start");
			RequestDTO dto = new RequestDTO();
			dto.setCustomerId(customerId);
			dto.setAmount(BigDecimal.valueOf(Long.valueOf(amount)));
			dto.setGatewayType(gatewayType);
			//URI uri = paymentDatewayBusinessService.processPaymentRequest(dto);
			//response.sendRedirect(uri.toString());
			LOGGER.info("PaymentGatewayController :: processPaymentRequest() :: End");
		} catch (Exception e) {
			if (e instanceof PaymentGatewayException) {
				LOGGER.error("PaymentGatewayController :: processPaymentRequest()"+((PaymentGatewayException) e).getErrorMessageDTO().toString(), e);
				throw (PaymentGatewayException) e;
			} else {
				LOGGER.error("PaymentGatewayController :: processPaymentRequest()", e);
				throw new PaymentGatewayException(
						new PaymentGatewayErrorMessageDTO(
								PaymentGatewayErrorCodes.GAE001,
								"Error occured while processing Payment Request",
								e));
			}
		}
	}
	
	@RequestMapping(value = "/excelDownload", method = RequestMethod.GET)
	public void excelDownload( HttpServletRequest request, HttpServletResponse response) {

		List<Object[]> paymentRequestList = paymentDatewayBusinessService.getPaymentRequestList();
		 
		 HSSFWorkbook workbook = new HSSFWorkbook();
		 HSSFSheet sheet = workbook.createSheet("PG Request");
		 
	        sheet.setDefaultColumnWidth(30);
	        CellStyle style = workbook.createCellStyle();
	        Font font = workbook.createFont();
	        font.setFontName("Arial");
	        style.setFillForegroundColor(HSSFColor.BLUE.index);
	        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        font.setColor(HSSFColor.WHITE.index);
	        style.setFont(font);
	        
	        // create header row
	        Row header = sheet.createRow(0);
	         
	        header.createCell(0).setCellValue("Req Id");
	        header.getCell(0).setCellStyle(style);
	         
	        header.createCell(1).setCellValue("Cust Id");
	        header.getCell(1).setCellStyle(style);
	         
	        header.createCell(2).setCellValue("Amount");
	        header.getCell(2).setCellStyle(style);
	         
	        header.createCell(3).setCellValue("Gateway Type");
	        header.getCell(3).setCellStyle(style);
	         
	        header.createCell(4).setCellValue("Email");
	        header.getCell(4).setCellStyle(style);
	        
	        header.createCell(5).setCellValue("Phone No");
	        header.getCell(5).setCellStyle(style);
	        
	        header.createCell(6).setCellValue("Request");
	        header.getCell(6).setCellStyle(style);
	        
	        header.createCell(7).setCellValue("Status");
	        header.getCell(7).setCellStyle(style);
	        
	        header.createCell(8).setCellValue("Created On");
	        header.getCell(8).setCellStyle(style);
	        
	        header.createCell(9).setCellValue("Created By");
	        header.getCell(9).setCellStyle(style);
	        
	        header.createCell(10).setCellValue("Modified On");
	        header.getCell(10).setCellStyle(style);
	        
	        header.createCell(11).setCellValue("Modified By");
	        header.getCell(11).setCellStyle(style);
	        // create data rows
	        int rowCount = 1;
	        try{         
		        for (Object[] pR : paymentRequestList) {
		        	 HSSFRow aRow = sheet.createRow(rowCount++);
		        	 
		        	 aRow.createCell(0).setCellValue( pR[0].toString());
		             aRow.createCell(1).setCellValue( pR[1].toString());
		             aRow.createCell(2).setCellValue(pR[2].toString());
		             aRow.createCell(3).setCellValue( pR[3].toString());
		             aRow.createCell(4).setCellValue( pR[4].toString());
		             aRow.createCell(5).setCellValue(pR[5].toString());
		             aRow.createCell(6).setCellValue(pR[6].toString());
		             aRow.createCell(7).setCellValue( pR[7].toString());
		             aRow.createCell(8).setCellValue( pR[8].toString());
		             aRow.createCell(9).setCellValue(pR[9].toString());	
		             aRow.createCell(10).setCellValue(pR[10].toString());
		             aRow.createCell(11).setCellValue(pR[11].toString());
	         }
				FileOutputStream out = 
						new FileOutputStream(new File("D://pgrequest.xls"));
				workbook.write(out);
				out.close();
				System.out.println("Excel written successfully..");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	
}
