package com.arbiva.apfgc.tenant.pg.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Component("BilldeskChecksumInfo")
public class BilldeskGatewayInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Value("${billdesk.merchant.id}")
	private String merchantId;

	@Value("${billdesk.checksum.key}")
	private String checksumKey;

	@Value("${billdesk.checksum.algorithm}")
	private String checksumAlg;

	@Value("${billdesk.security.id}")
	private String securityId;

	@Value("${billdesk.request.url}")
	private String requestUrl;

	@Value("${billdesk.return.url}")
	private String returnUrl;

	@Value("${billdesk.request.payload}")
	private String requestPayload;

	@Value("#{'${billdesk.response.status.codes}'.split(',')}")
	private List<String> billdeskResponseStatusCodes;
	
	@Value("${billdesk.request.payload.css}")
	private String requestPayloadCss;
	
	@Value("${billdesk.return.url.css}")
	private String returnUrlCss;
	
	@Value("${billdesk.request.payload.css.beforeLogin}")
	private String requestPayloadCssBeforeLogin;
	
	@Value("${billdesk.return.url.css.beforeLogin}")
	private String returnUrlCssBeforeLogin;

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the checksumKey
	 */
	public String getChecksumKey() {
		return checksumKey;
	}

	/**
	 * @param checksumKey
	 *            the checksumKey to set
	 */
	public void setChecksumKey(String checksumKey) {
		this.checksumKey = checksumKey;
	}

	/**
	 * @return the checksumAlg
	 */
	public String getChecksumAlg() {
		return checksumAlg;
	}

	/**
	 * @param checksumAlg
	 *            the checksumAlg to set
	 */
	public void setChecksumAlg(String checksumAlg) {
		this.checksumAlg = checksumAlg;
	}

	/**
	 * @return the securityId
	 */
	public String getSecurityId() {
		return securityId;
	}

	/**
	 * @param securityId
	 *            the securityId to set
	 */
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	/**
	 * @return the requestUrl
	 */
	public String getRequestUrl() {
		return requestUrl;
	}

	/**
	 * @param requestUrl
	 *            the requestUrl to set
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * @return the returnUrl
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *            the returnUrl to set
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * @return the requestPayload
	 */
	public String getRequestPayload() {
		return requestPayload;
	}

	/**
	 * @param requestPayload
	 *            the requestPayload to set
	 */
	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}

	/**
	 * @return the billdeskResponseStatusCodes
	 */
	public List<String> getBilldeskResponseStatusCodes() {
		return billdeskResponseStatusCodes;
	}

	/**
	 * @param billdeskResponseStatusCodes
	 *            the billdeskResponseStatusCodes to set
	 */
	public void setBilldeskResponseStatusCodes(
			List<String> billdeskResponseStatusCodes) {
		this.billdeskResponseStatusCodes = billdeskResponseStatusCodes;
	}
	

	public String getRequestPayloadCss() {
		return requestPayloadCss;
	}

	public void setRequestPayloadCss(String requestPayloadCss) {
		this.requestPayloadCss = requestPayloadCss;
	}
	

	public String getReturnUrlCss() {
		return returnUrlCss;
	}

	public void setReturnUrlCss(String returnUrlCss) {
		this.returnUrlCss = returnUrlCss;
	}

	
	public String getRequestPayloadCssBeforeLogin() {
		return requestPayloadCssBeforeLogin;
	}

	public void setRequestPayloadCssBeforeLogin(String requestPayloadCssBeforeLogin) {
		this.requestPayloadCssBeforeLogin = requestPayloadCssBeforeLogin;
	}

	public String getReturnUrlCssBeforeLogin() {
		return returnUrlCssBeforeLogin;
	}

	public void setReturnUrlCssBeforeLogin(String returnUrlCssBeforeLogin) {
		this.returnUrlCssBeforeLogin = returnUrlCssBeforeLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((billdeskResponseStatusCodes == null) ? 0
						: billdeskResponseStatusCodes.hashCode());
		result = prime * result
				+ ((checksumAlg == null) ? 0 : checksumAlg.hashCode());
		result = prime * result
				+ ((checksumKey == null) ? 0 : checksumKey.hashCode());
		result = prime * result
				+ ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result
				+ ((requestPayload == null) ? 0 : requestPayload.hashCode());
		result = prime * result
				+ ((requestUrl == null) ? 0 : requestUrl.hashCode());
		result = prime * result
				+ ((returnUrl == null) ? 0 : returnUrl.hashCode());
		result = prime * result
				+ ((securityId == null) ? 0 : securityId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BilldeskGatewayInfo other = (BilldeskGatewayInfo) obj;
		if (billdeskResponseStatusCodes == null) {
			if (other.billdeskResponseStatusCodes != null)
				return false;
		} else if (!billdeskResponseStatusCodes
				.equals(other.billdeskResponseStatusCodes))
			return false;
		if (checksumAlg == null) {
			if (other.checksumAlg != null)
				return false;
		} else if (!checksumAlg.equals(other.checksumAlg))
			return false;
		if (checksumKey == null) {
			if (other.checksumKey != null)
				return false;
		} else if (!checksumKey.equals(other.checksumKey))
			return false;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (requestPayload == null) {
			if (other.requestPayload != null)
				return false;
		} else if (!requestPayload.equals(other.requestPayload))
			return false;
		if (requestUrl == null) {
			if (other.requestUrl != null)
				return false;
		} else if (!requestUrl.equals(other.requestUrl))
			return false;
		if (returnUrl == null) {
			if (other.returnUrl != null)
				return false;
		} else if (!returnUrl.equals(other.returnUrl))
			return false;
		if (securityId == null) {
			if (other.securityId != null)
				return false;
		} else if (!securityId.equals(other.securityId))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BilldeskChecksumInfo [merchantId=" + merchantId
				+ ", checksumKey=" + checksumKey + ", checksumAlg="
				+ checksumAlg + ", securityId=" + securityId + ", requestUrl="
				+ requestUrl + ", returnUrl=" + returnUrl + ", requestPayload="
				+ requestPayload + ", billdeskResponseStatusCodes="
				+ billdeskResponseStatusCodes + "]";
	}

}