/**
 * 
 *//*
package com.arbiva.apfgc.tenant.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantServices;
import com.arbiva.apfgc.tenant.model.TenantServicesPK;

*//**
 * @author Arbiva
 *
 *//*
public interface TenantServicesDao extends JpaSpecificationExecutor<TenantServices>, JpaRepository<TenantServices, TenantServicesPK> {

	@Query(value="SELECT * FROM tenantsrvcs where tenantcode = ?1 ",nativeQuery=true)
	public abstract TenantServices findByTenantCode(String tenantCode);
	
	@Query(value="SELECT * FROM tenantsrvcs where tenantcode = ?1 and agrFDate= ?2 and coresrvcCode= ?3 ",nativeQuery=true)
	public abstract TenantServices findTenantService(String tenantCode, Date agrFDate, String coresrvcCode);	
	
}
*/