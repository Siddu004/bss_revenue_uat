package com.arbiva.apfgc.tenant.restWebServices;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tenant.controller.BaseController;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.serviceImpl.TenantServiceImpl;
import com.arbiva.apfgc.tenant.serviceImpl.TenantWalletServiceImpl;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@Service
@Path("/tenant")
public class TenantRestService extends BaseController {

	private static final Logger logger = Logger.getLogger(TenantRestService.class);
	
	ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR);
	
	@Autowired
	TenantServiceImpl tenantService;
	
	@Autowired
	TenantWalletServiceImpl tenantWalletService;

/*	
	@Autowired
	TenantServicesServiceImpl tenantServicesService;

	@Autowired
	TenantLMOServicesServiceImpl tenantLMOServicesService;

	@Autowired
	TenantAgreementServiceImpl tenantAgreementService;

	@Autowired
	TenantLmoMspAgreementServiceImpl tenantLmoMspAgreementService;*/

	/*
	 * For Fetching the Tenant Information by Tenant Code
	 */

	@GET
	@Path("/getTenantInfoByTenantCode")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTentInfoByTenantCode(@QueryParam(value = "tenantCode") String tenantCode) {
		Tenant tenant = null;
		try {
			logger.info("getting TentInfoByTenantCode !!!!");
			tenant = tenantService.findByTenantCode(tenantCode);
			builder = Response.status(Status.OK).entity(tenant);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		} finally {
			tenant = null;
		}
		return builder.build();
	}
	



	/*
	 * Get the Tenant Service
	 */

	
	/*@GET
	@Path("/getTenantService")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTenantService(@QueryParam(value = "tenantCode") String tenantCode,
			@QueryParam(value = "agrFDate") String agrFDate, @QueryParam(value = "coresrvcCode") String coresrvcCode) {
		TenantServices tenantServices = null;
		TenantServicesDTO tenantServicesDTO = null;
		try {
			logger.info("getting TenantService !!!!" + tenantCode + "!!!!" + agrFDate + "!!!!" + coresrvcCode);
			tenantServices = tenantServicesService.findTenantService(tenantCode, new SimpleDateFormat("yyyy-MM-dd").parse(agrFDate), coresrvcCode);
			tenantServicesDTO = new TenantServicesDTO(tenantServices);
			builder = Response.status(Status.OK).entity(tenantServicesDTO);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		}
		return builder.build();
	}*/	 

	/*
	 * Getting Tenant Services
	 */

	/*@GET
	@Path("/getAllTenantServices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTenantServices() {
		List<TenantServices> tenantServices = null;
		List<TenantServicesDTO> tenantServicesDTO = null;
		try {
			logger.info("getting All TenantServices !!!!");
			tenantServicesDTO = new ArrayList<TenantServicesDTO>();
			tenantServices = tenantServicesService.findAllTenantServicess();
			if (CollectionUtils.isNotEmpty(tenantServices)) {
				for (TenantServices services : tenantServices) {
					tenantServicesDTO.add(new TenantServicesDTO(services));
				}
			}
			builder = Response.status(Status.OK).entity(tenantServicesDTO);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		}
		return builder.build();
	}*/

	/*
	 * Getting ALL Tenant LMO Services
	 */

/*	@GET
	@Path("/getAllTenantLMOServices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTenantLMOServices() {
		List<TenantLMOServices> tenantLMOServices = null;
		List<TenantLMOServicesDTO> tenantLMOServicesDTO = null;
		try {
			tenantLMOServicesDTO = new ArrayList<TenantLMOServicesDTO>();
			logger.info("getting AllTenantLMOServices !!!!");
			tenantLMOServices = tenantLMOServicesService.findAllTenantLMOServices();
			logger.info("::: After Completion of TenantLmoServices ::: ");
			if (CollectionUtils.isNotEmpty(tenantLMOServices)) {
				for (TenantLMOServices services : tenantLMOServices) {
					tenantLMOServicesDTO.add(new TenantLMOServicesDTO(services));
				}
			}
			builder = Response.status(Status.OK).entity(tenantLMOServicesDTO);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		}

		return builder.build();
	}*/

	/*
	 * Save the Tenant
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/saveTenant")
	public Response saveTenant(@Context HttpServletRequest request, TenantVO tenantVO) {
		Integer tenantId = null;
		try {
			tenantService.saveTenant(tenantVO, tenantId);
			builder = Response.status(Status.OK);
		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		} finally {
			tenantId = null;
		}
		return builder.build();
	}

	/*
	 * For Saving the Tenant Agreement
	 */

	/*@POST
	@Path("/saveTenantAgreement")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTenantAgreement(@Context HttpServletRequest request, TenantAgreementVO tenantAgreementVO)
	{
		try {
			//String tenantcode=tenantAgreementVO.getTenantCode();
			String action = "Save";
			tenantAgreementService.saveTenantAgreement(tenantAgreementVO, action);
		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Fetching the All Tenant LMOServices
	 */
	/*@GET
	@Path("/getAllTenantLMOServices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTenantLMOServices(@QueryParam(value = "mspCode") String mspCode) {
		try {
			logger.info("getting AllTenantLMOServices !!!!");
			TenantLMOServices tenantLMOServices = tenantLMOServicesService.findByMspCode(mspCode);
			builder = Response.status(Status.OK).entity(tenantLMOServices);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		}
		return builder.build();
	}
*/
	/*
	 * For Saving Tenant LMO Services
	 */
/*	@POST
	@Path("/saveTenantLmoServices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTenantLmoServices(@Context HttpServletRequest request, TenantLmoMspAgreementVO tenantLmoMspAgreementVO)
	{
		try {
			String action = "Save";
			tenantLMOServicesService.saveTenantLMOServices(tenantLmoMspAgreementVO, action);
		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Saving Tenant LmoMsp Agreement
	 */
/*
	@POST
	@Path("/saveTenantLmoMspAgreement")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTenantLmoMspAgreement(@Context HttpServletRequest request, TenantLmoMspAgreementVO tenantLmoMspAgreementVO)
	{
		try {
			//String mspCode=tenantLmoMspAgreementVO.getMspCode();
			String action = "Save";
			tenantLmoMspAgreementService.saveTenantLmoMspAgreement(tenantLmoMspAgreementVO, action);
		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Fetching the All Tenant LMOServices
	 */
	/*@GET
	@Path("/getTenantService")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTenantService(@QueryParam(value = "tenantCode") String tenantCode) {
		try {
			logger.info("getting TenantServices !!!!");
			TenantServices tenantServices = tenantServicesService.findByTenantCode(tenantCode);
			builder = Response.status(Status.OK).entity(tenantServices);
		} catch (Exception ex) {
			builder = handleRestExceptions(ex, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Saving the TenantServices
	 */
/*	@POST
	@Path("/saveTenantServices")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTenantServices(@Context HttpServletRequest request, TenantAgreementVO tenantAgreementVO) {
		String action = "Save";
		try {
			tenantServicesService.saveTenantServices(tenantAgreementVO, action);

		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Saving the TenantWallet
	 */
	/*@POST
	@Path("/saveTenantWallet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTenantWallet(@Context HttpServletRequest request, TenantAgreementVO tenantAgreementVO) {
		try {
			String action = "Save";
			tenantWalletService.saveTenantWallet(tenantAgreementVO, action);
			builder = Response.status(Status.OK);

		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

	/*
	 * For Updating Tenant Wallet
	 */
	/*@POST
	@Path("/updateTenantWallet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateTenantWallet(@Valid @RequestBody TenantAgreementVO tenantAgreementVO,
			HttpServletRequest request, BindingResult bindingResult) 
	public Response updateTenantWallet(@Context HttpServletRequest request, TenantAgreementVO tenantAgreementVO)
	{
		try {
			String action = "update";
			tenantWalletService.saveTenantWallet(tenantAgreementVO, action);

		} catch (Exception exe) {
			builder = handleRestExceptions(exe, builder);
		}
		return builder.build();
	}*/

}
