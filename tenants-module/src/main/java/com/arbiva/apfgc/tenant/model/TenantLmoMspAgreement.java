/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

*//**
 * @author Arbiva
 *
 *//*
@Entity
@Table(name="msplmoagr", schema="apfgc")
@IdClass(TenantLmoMspAgreementPK.class)
public class TenantLmoMspAgreement implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="mspcode")
	private String mspCode;
	
	@Id
	@Column(name="lmocode")
	private String lmoCode;
	
	@Id
	@Column(name = "agrfdate")
	private Date agrFDate;
	
	@Column(name = "agrdate")
	private Date agreementDate;
	
	@Column(name = "agrtdate")
	private Date agreementTo;
	
	@Column(name = "agrdocref")
	private String agreementDocRef;
	
	@Column(name = "deposit_amt")
	private Float depositAmount;
	
	@Column(name = "lmowalletamt")
	private Float lmoWalletAmount;
	
	@Column(name = "deposit_mode")
	private String depositMode;
	
	@Column(name = "deposit_refno")
	private String depositRefno;
	
	@Column(name = "deposit_status")
	private int depositStatus;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "reasons")
	private String reasons;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private String modifiedBy;
	
	@Column(name = "modifiedipaddr")
	private String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private String deactivatedOn;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpaddr;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="mspcode", referencedColumnName="tenantcode", insertable=false,updatable=false)
	private Tenant tenantMsp;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="lmocode", referencedColumnName="tenantcode", insertable=false,updatable=false)
	private Tenant tenantLmo;
	
	@OneToMany(targetEntity=TenantLMOServices.class, mappedBy="tenantLmoMspAgreement", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<TenantLMOServices> tenantLMOServices;
	
	@OneToMany(targetEntity=TenantLMOWallet.class, mappedBy="tenantLmoMspAgreementWallet", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<TenantLMOWallet> tenantLMOWallet;
	
	public Tenant getTenantMsp() {
		return tenantMsp;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public void setTenantMsp(Tenant tenantMsp) {
		this.tenantMsp = tenantMsp;
	}

	public Tenant getTenantLmo() {
		return tenantLmo;
	}

	public void setTenantLmo(Tenant tenantLmo) {
		this.tenantLmo = tenantLmo;
	}
	
	public List<TenantLMOServices> getTenantLMOServices() {
		return tenantLMOServices;
	}

	public void setTenantLMOServices(List<TenantLMOServices> tenantLMOServices) {
		this.tenantLMOServices = tenantLMOServices;
	}

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public Date getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(Date agreementDate) {
		this.agreementDate = agreementDate;
	}

	public Date getAgreementTo() {
		return agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public String getAgreementDocRef() {
		return agreementDocRef;
	}

	public void setAgreementDocRef(String agreementDocRef) {
		this.agreementDocRef = agreementDocRef;
	}

	public Float getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Float depositAmount) {
		this.depositAmount = depositAmount;
	}

	public Float getLmoWalletAmount() {
		return lmoWalletAmount;
	}

	public void setLmoWalletAmount(Float lmoWalletAmount) {
		this.lmoWalletAmount = lmoWalletAmount;
	}

	public String getDepositMode() {
		return depositMode;
	}

	public void setDepositMode(String depositMode) {
		this.depositMode = depositMode;
	}

	public String getDepositRefno() {
		return depositRefno;
	}

	public void setDepositRefno(String depositRefno) {
		this.depositRefno = depositRefno;
	}

	public Integer getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}
	
	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpaddr() {
		return deactivatedIpaddr;
	}

	public void setDeactivatedIpaddr(String deactivatedIpaddr) {
		this.deactivatedIpaddr = deactivatedIpaddr;
	}

	public List<TenantLMOWallet> getTenantLMOWallet() {
		return tenantLMOWallet;
	}

	public void setTenantLMOWallet(List<TenantLMOWallet> tenantLMOWallet) {
		this.tenantLMOWallet = tenantLMOWallet;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setDepositStatus(int depositStatus) {
		this.depositStatus = depositStatus;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
*/