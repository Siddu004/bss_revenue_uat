package com.arbiva.apfgc.tenant.pg.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author srinivasa
 *
 */
public class BilldeskChecksumUtils {

	/**
	 * 
	 * @param message
	 * @param secret
	 * @return
	 */
	public static String getChecksum(String algorithm, String secret, String message) {
		Mac sha256_HMAC = null;
		SecretKeySpec secret_key = null;
		byte[] raw =  null;
		try {
			sha256_HMAC = Mac.getInstance(algorithm);
			secret_key = new SecretKeySpec(secret.getBytes(), algorithm);
			sha256_HMAC.init(secret_key);
			raw = sha256_HMAC.doFinal(message.getBytes());
			StringBuffer ls_sb = new StringBuffer();
			for (int i = 0; i < raw.length; i++)
				ls_sb.append(char2hex(raw[i]));
			return ls_sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			sha256_HMAC = null;
			secret_key = null;
			raw =  null;
		}
	}
	
	public static String char2hex(byte x)
	{
		char arr[] = { '0', '1', '2', '3',
					   '4', '5', '6', '7',
					   '8','9', 'A', 'B', 
					   'C', 'D', 'E', 'F' 
					 };
		char c[] = { arr[(x & 0xF0) >> 4], arr[x & 0x0F] };
		return (new String(c));
	}
	 
	 public static void main(String[] args) {
			try {
				String ChecksumKey = "P2Wtlylo6geq";
				System.out
						.println("HmacSHA256="
								+ getChecksum("HmacSHA256", 
										"MERCHANT|1000000000|NA|12.00|XXX|NA|NA|INR|DIRECT|R|NA|NA|NA|F|111111111|NA|NA|NA|NA|NA|NA|NA|",
										ChecksumKey));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	
}
