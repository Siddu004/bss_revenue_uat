/*package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tanent.util.FileUtil;
import com.arbiva.apfgc.tanent.util.TmsHelper;
import com.arbiva.apfgc.tenant.daoImpl.TenantAgreementDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.dto.AgreementDTO;
import com.arbiva.apfgc.tenant.dto.TenantDTO;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.vo.TenantAgreementVO;

@Service
public class TenantAgreementServiceImpl {

	 public static final String path = "C:/AgreementDocuments"; 

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	TenantDaoImpl tenantDao;

	@Autowired
	TenantWalletServiceImpl tenantWalletService;

	@Autowired
	TenantServicesServiceImpl tenantServicesService;

	@Autowired
	TenantServiceImpl tenantServiceImpl;

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantAgreementServiceImpl.class);

	@Autowired
	TenantAgreementDaoImpl tenantAgreementDao;

	@Value("${UMS-URL}")
	private String umsURL;

	@Value("${IMAGE_PATH}")
	private String imagePath;

	@Transactional
	public void saveTenantAgreement(TenantAgreementVO tenantAgreementVO, String action) throws Exception {
		String userName = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		if (userName != null || userName != "") {
			TenantAgreement tenantAgreement = null;
			tenantAgreement = tenantAgreementDao.findTenantAgreement(tenantAgreementVO.getTenantCode(),
							  DateUtill.stringtoString(tenantAgreementVO.getAgrFDate()));
			if(tenantAgreement == null){
				tenantAgreement = new TenantAgreement();
				tenantAgreement.setCratedIPAddress(httpServletRequest.getRemoteAddr());
				tenantAgreement.setCreatedBy(userName);
				tenantAgreement.setCreatedDate(Calendar.getInstance());
				tenantAgreement.setModifiedDate(Calendar.getInstance());
			}else{
				tenantAgreement.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
				tenantAgreement.setModifiedBy(userName);
				tenantAgreement.setModifiedDate(Calendar.getInstance());
				if(tenantAgreementVO.getAgreementCopy().isEmpty())
					tenantAgreement.setAgreementDocRef(tenantAgreement.getAgreementDocRef());
			}

			if (!tenantAgreementVO.getAgreementCopy().isEmpty()) {
				String docType = "TenantAgreement";
				String dbPath = FileUtil.saveImage(tenantAgreementVO.getAgreementCopy().getOriginalFilename(), imagePath,tenantAgreementVO.getAgreementCopy().getBytes(),userName,docType);
				tenantAgreement.setAgreementDocRef(dbPath);
			}
			tenantAgreement.setTenantCode(tenantAgreementVO.getTenantCode());
			tenantAgreement.setAgrFDate(DateUtill.stringtoDate(tenantAgreementVO.getAgrFDate()));
			tenantAgreement.setAgreementDate(DateUtill.stringtoDate(tenantAgreementVO.getAgreementDate()));
			tenantAgreement.setAgreementTo(DateUtill.stringtoDate(tenantAgreementVO.getAgreementTo()));
			tenantAgreement.setModifiedDate(Calendar.getInstance());
			tenantAgreement.setStatus(1);

			LOGGER.info("TenantAgreementServiceImpl::saveTenantAgreement()--> CreateTenant Agreement Module ::::");
			tenantAgreementDao.saveTenantAgreement(tenantAgreement);

			LOGGER.info("TenantAgreementServiceImpl::saveTenantAgreement()--> The Tenant Agreement Created Successfully.");

			tenantWalletService.saveTenantWallet(tenantAgreementVO, action);

			LOGGER.info("TenantAgreementServiceImpl::saveTenantAgreement()--> The Tenant Wallet Created Successfully.");

			tenantServicesService.saveTenantServices(tenantAgreementVO, action);

			LOGGER.info("TenantAgreementServiceImpl::saveTenantAgreement()--> The Tenant Services Created Successfully.");

		}
	}

	public List<TenantAgreement> findAllTenantAgreements() {
		List<TenantAgreement> tenantAgreements = new ArrayList<TenantAgreement>();
		tenantAgreements = tenantAgreementDao.findAllTenantAgreements();
		return tenantAgreements;
	}

	public List<Tenant> findAllTenantAgreementsforAdmin(String roleName, String createdBy) {
		List<Tenant> tenants = new ArrayList<Tenant>();
		tenants = tenantAgreementDao.findAllTenantAgreementsforAdmin(roleName, createdBy);
		return tenants;
	}

	public List<Tenant> findAllTenantsforAdmin(String roleName, String createdBy) {
		List<Tenant> tenants = new ArrayList<Tenant>();
		tenants = tenantAgreementDao.findAllTenantsforAdmin(roleName, createdBy);
		return tenants;
	}

	public TenantAgreement findByTenantCode(String tenantCode) {
		TenantAgreement tenantAgreement = new TenantAgreement();
		tenantAgreement = tenantAgreementDao.findByTenantCode(tenantCode);
		return tenantAgreement;
	}

	public List<AgreementDTO> findAllAgreements(String roleName, String loginID) {

		List<AgreementDTO> agreementlist = new ArrayList<AgreementDTO>();
		List<Object[]> agreeList;
		agreeList = tenantAgreementDao.findAllAgreements(roleName, loginID);

		for (Object[] obj : agreeList) {
			AgreementDTO agreementDTO = new AgreementDTO();
			agreementDTO.setStatus(Integer.parseInt(obj[0].toString()));
			agreementDTO.setTenantCode((String) obj[1]);
			agreementDTO.setTenantName((String) obj[2]);
			agreementDTO.setTenantTypeLov((String) obj[3]);
			agreementDTO.setMobile((String) obj[4]);
			agreementDTO.setTenantId((Integer) obj[5]);
			agreementDTO.setCreatedBy((String) obj[6]);
			agreementDTO.setAgreementType((String) obj[7]);
			agreementDTO.setMspOrTenantCode((String) obj[8]);
			agreementDTO.setEffDate(obj[9].toString());
			agreementDTO.setLmoCode((String) obj[10]);
			agreementlist.add(agreementDTO);
		}

		return agreementlist;
	}

	public TenantAgreement findTenantAgreement(String tenantCode, String fromDate) {
		return tenantAgreementDao.findTenantAgreement(tenantCode, fromDate);
	}

	@Transactional
	public void updateTenantAgreementStatus(TenantAgreement tenantAgreement, String action, int tenantId, String reason)throws Exception {
		if (action.equalsIgnoreCase("Approve")) {
			tenantAgreement.setStatus(2);
			Tenant tenanObj = tenantServiceImpl.findByTenantId(tenantId);
			String responce = TmsHelper.createUser(tenanObj, umsURL);
			if(responce.equalsIgnoreCase("exists"))
				throw new TenantException(TenantErrorCodes.TENT003, "User Is Already Created With The Same LoginId ");
		} else if (action.equalsIgnoreCase("Reject")){
			tenantAgreement.setReasons(reason == null ? "" : reason.trim());
			tenantAgreement.setStatus(3);
		}
		else if (action.equalsIgnoreCase("Send Back")){
			tenantAgreement.setStatus(4);
			tenantAgreement.setReasons(reason == null ? "" : reason.trim());
		}
			
		tenantAgreementDao.saveTenantAgreement(tenantAgreement);

	}

	public List<TenantDTO> findAllTenantsBYCreatedBy(String loginID) {
		List<Tenant> tenantsList = tenantAgreementDao.findAllTenantsBYCreatedBy(loginID);
		List<TenantDTO> tenantsDtoList = new ArrayList<TenantDTO>();
		for(Tenant tenant : tenantsList){
			TenantDTO tenantDTO = new TenantDTO(tenant);
			tenantsDtoList.add(tenantDTO);
		}
		return tenantsDtoList;
	}
}
*/