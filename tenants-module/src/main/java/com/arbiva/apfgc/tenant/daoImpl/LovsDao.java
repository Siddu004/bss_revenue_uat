/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.model.Lovs;

/**
 * @author Arbiva
 *
 */
@Component("lovsDao")
public class LovsDao {

	private static final Logger LOGGER = Logger.getLogger(CoreServicesDao.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	
	public List<Lovs> findByLovsByLovName(String lovName) {
		List<Lovs> lovs = new ArrayList<Lovs>();
		 StringBuilder builder = new StringBuilder();
		 TypedQuery<Lovs> query = null;
			try {
				 LOGGER.info("GlCodeDaoImpl :: findByLovsByLovName() :: SATRT");
		        builder.append(" FROM ").append(Lovs.class.getSimpleName()).append(" WHERE lovname ='"+lovName+"' AND status=1" );
			   query = getEntityManager().createQuery(builder.toString(), Lovs.class);
			   lovs = query.getResultList();
			   LOGGER.info("END::findByLovsByLovName()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByLovsByLovName() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return lovs;
	}

	@SuppressWarnings("unchecked")
	public List<String> findByL1PortSizeByLovName(String lovName) {
		List<String> lovs = new ArrayList<String>();
		 StringBuilder builder = new StringBuilder();
		 Query query = null;
			try {
				 LOGGER.info("GlCodeDaoImpl :: findByLovsByLovName() :: SATRT");
		        builder.append("select  lovvalue  from lovs WHERE lovname = '"+lovName+"' order by lovid asc  ");
			   query = getEntityManager().createNativeQuery(builder.toString());
			   lovs = query.getResultList();
			   LOGGER.info("END::findByL1PortSizeByLovName()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByL1PortSizeByLovName() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return lovs;
	}

}
