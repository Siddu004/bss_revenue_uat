package com.arbiva.apfgc.tenant.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.CpeStatus;
import com.arbiva.apfgc.tenant.dao.CpeDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.dto.CpeDeliveryDTO;
import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.dto.CpeModelMobileDTO;
import com.arbiva.apfgc.tenant.dto.CpeOrderSaveDTO;
import com.arbiva.apfgc.tenant.dto.CpeTypeMobileDTO;
import com.arbiva.apfgc.tenant.model.CpeCharges;
import com.arbiva.apfgc.tenant.model.CpeProfileMaster;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantCpeDemand;
import com.arbiva.apfgc.tenant.model.TenantCpeDlv;
import com.arbiva.apfgc.tenant.model.TenantCpePmnts;
import com.arbiva.apfgc.tenant.service.CpeService;

@Service
public class CpeServiceImpl implements CpeService {

	private static final Logger LOGGER = Logger.getLogger(CpeServiceImpl.class);

	@Autowired
	CpeDao cpeDao;
	
	@Autowired
	TenantDaoImpl tenantDao;
	

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Override
	public List<CpeHelperDTO> findAllCpeMasterList() {
		List<CpeHelperDTO> cpeHelperDTOList = new ArrayList<>();
		List<Object> cpemasterList = null;
		CpeHelperDTO cpeHelperDTO = null;
		try {
		  cpemasterList = cpeDao.findAllCpeMasterList();
			for (Object cpeType : cpemasterList) {
				 cpeHelperDTO = new CpeHelperDTO();
				cpeHelperDTO.setCpeTypeLov(cpeType.toString());
				cpeHelperDTOList.add(cpeHelperDTO);
			}

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpemasterList = null;
			cpeHelperDTO = null;
		}

		return cpeHelperDTOList;
	}

	@Override
	public List<CpeHelperDTO> findAllCpeModelByCpeType(String cpeType) {
		List<CpeProfileMaster> cpemasterList = cpeDao.findAllCpeModelByCpeType(cpeType);
		List<CpeHelperDTO> cpeHelperDTOList = new ArrayList<>();
		CpeHelperDTO cpeHelperDTO = null;
		try {

			for (CpeProfileMaster cpeProfileMaster : cpemasterList) {
				  cpeHelperDTO = new CpeHelperDTO();
				cpeHelperDTO.setCpeModel(cpeProfileMaster.getCpeModel());
				cpeHelperDTO.setCpeProfileName(cpeProfileMaster.getCpeProfilename());
				cpeHelperDTO.setProfileId(cpeProfileMaster.getProfileId().toString());
				cpeHelperDTOList.add(cpeHelperDTO);
			}

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpemasterList = null;
			cpeHelperDTO = null;
		}
		return cpeHelperDTOList;
	}

	@Override
	@Transactional
	public CpeHelperDTO getCpeChargesByProfileId(String profileId) {
		CpeHelperDTO cpeHelperDTO = new CpeHelperDTO();
		CpeCharges cpeCharges;
		try {
			cpeCharges = cpeDao.getCpeChargesByProfileId(profileId);
			cpeHelperDTO.setCustCost(cpeCharges.getCustCost().toString());
			cpeHelperDTO.setCustRent(cpeCharges.getCustRent().toString());
			cpeHelperDTO.setPurchaseCost(cpeCharges.getPurchaseCost().toString());
			cpeHelperDTO.setInstalationCost(cpeCharges.getInstCharges().toString());
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpeCharges = null;
		}
		return cpeHelperDTO;
	}

	@Override
	@Transactional
	public String saveCpeOrders(CpeOrderSaveDTO cpeOrderSaveDTO) {
		String responce = "";
		Long cpeDemandId ;
		TenantCpeDemand tenantCpeDemand ;

		try {
			for (CpeHelperDTO cpeHelperDTO : cpeOrderSaveDTO.getCpeModelsList()) {
				  cpeDemandId = storedProcedureDAO.executeStoredProcedure("CPEDEMANDID");
				  tenantCpeDemand = new TenantCpeDemand();
				tenantCpeDemand.setDmndId(cpeDemandId);
				tenantCpeDemand.setTenantCode(cpeOrderSaveDTO.getTenantCode());
				tenantCpeDemand.setDemandDate(Calendar.getInstance().getTime());
				tenantCpeDemand.setProfileId(cpeHelperDTO.getProfileId());
				tenantCpeDemand.setDemandQuntity(Long.parseLong(cpeHelperDTO.getQuantity()));
				tenantCpeDemand.setDemandPrice(new BigDecimal(cpeHelperDTO.getTotalCost()));
				tenantCpeDemand.setStatus(new Short("1"));
				tenantCpeDemand.setCreatedon(Calendar.getInstance());
				tenantCpeDemand.setCreatedby(cpeOrderSaveDTO.getLoginId());
				tenantCpeDemand.setModifiedon(Calendar.getInstance());
				cpeDao.saveCpeOrders(tenantCpeDemand);
			}
			responce = " <label style='color:#006600'> Order Is Created Successfully.... </label>";
		} catch (Exception ex) {
			responce = " <label style='color:#ff0000'> Failed To Create Order.... </label>";
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpeDemandId = null;
			tenantCpeDemand = null;
		}

		return responce;
	}

	@Override
	public List<CpeHelperDTO> findAllCpeOrdersList(String tenantCode, String tenantType) {
		List<CpeHelperDTO> cpeHelperDTOList = new ArrayList<>();
		List<Object[]> cpemasterList = null;
		CpeHelperDTO cpeHelperDTO;
		try {
			cpemasterList = cpeDao.findAllCpeOrdersList(tenantCode, tenantType);
			for (Object[] cpeType : cpemasterList) {
				  cpeHelperDTO = new CpeHelperDTO();
				cpeHelperDTO.setTenantName(cpeType[0].toString());
				cpeHelperDTO.setCpeTypeLov(cpeType[1].toString());
				cpeHelperDTO.setCpeModel(cpeType[2].toString());
				cpeHelperDTO.setPurchaseCost(cpeType[3].toString());
				cpeHelperDTO.setQuantity(cpeType[4].toString());
				cpeHelperDTO.setOrderedDate(cpeType[5].toString());
				cpeHelperDTO.setTotalCost(cpeType[6].toString());
				cpeHelperDTO.setDelvQuantity(cpeType[10].toString());
				cpeHelperDTO.setDemandId(cpeType[8].toString());
				cpeHelperDTO.setProfileId(cpeType[9].toString());
				cpeHelperDTO.setBlockedQuantity(cpeType[11].toString());
				cpeHelperDTOList.add(cpeHelperDTO);
			}

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpemasterList = null;
			cpeHelperDTO = null;
		}

		return cpeHelperDTOList;
	}

	@Override
	public CpeHelperDTO allocateCpe(String demandId) {
		CpeHelperDTO cpeHelperDTO = new CpeHelperDTO();
		List<CpeDeliveryDTO> cpeDeliveryList = new ArrayList<>();
		String availabileCpeCount = null;
		Object[] cpeDmnd;
		List<Object[]> cpeDlvList ;
		List<String> cpeSlnoList;

		try {
			 cpeDmnd = cpeDao.findCpeDemandByDemandId(demandId);
			cpeHelperDTO.setTenantName(cpeDmnd[0].toString());
			cpeHelperDTO.setCpeTypeLov(cpeDmnd[1].toString());
			cpeHelperDTO.setCpeModel(cpeDmnd[2].toString());
			cpeHelperDTO.setPurchaseCost(cpeDmnd[3].toString());
			cpeHelperDTO.setQuantity(cpeDmnd[4].toString());
			cpeHelperDTO.setOrderedDate(cpeDmnd[5].toString());
			cpeHelperDTO.setTotalCost(cpeDmnd[6].toString());
			cpeHelperDTO.setDelvQuantity(cpeDmnd[7].toString());
			cpeHelperDTO.setDemandId(cpeDmnd[8].toString());
			cpeHelperDTO.setProfileId(cpeDmnd[9].toString());
			cpeHelperDTO.setTenantCode(cpeDmnd[10].toString());
			cpeHelperDTO.setTenantType(cpeDmnd[11].toString());

			  cpeDlvList = cpeDao.findAllcpeDlvByDemandId(demandId);

			for (Object[] cpeType : cpeDlvList) {
				CpeDeliveryDTO cpeDeliveryDTO = new CpeDeliveryDTO();
				cpeDeliveryDTO.setDlvDate(cpeType[0].toString());
				cpeDeliveryDTO.setDlvQty(cpeType[1].toString());
				cpeDeliveryDTO.setDlvid(cpeType[2].toString());
				cpeDeliveryDTO.setIsPaid(cpeType[3] == null ? "NO" : cpeType[3].toString());
				cpeDeliveryList.add(cpeDeliveryDTO);
			}
			cpeHelperDTO.setCpeDeliveryList(cpeDeliveryList);
			  cpeSlnoList = cpeDao.getAvailabileCpeCountByProfileId(cpeHelperDTO.getProfileId());
			availabileCpeCount = String.valueOf(cpeSlnoList.size());
			cpeHelperDTO.setCpeSlnoList(cpeSlnoList);
			cpeHelperDTO.setCpeAvailability(availabileCpeCount);

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally {
			cpeDeliveryList = null;
			availabileCpeCount = null;
			cpeDmnd = null;
			cpeDlvList = null;
			cpeSlnoList = null;
		}

		return cpeHelperDTO;
	}

	@Override
	@Transactional
	public String saveCpeAllocation(CpeHelperDTO cpeHelperDTO) {
		StringBuilder result = new StringBuilder();
		Long cpeDlvId;
		TenantCpeDlv tenantCpeDlv ;
		CpeStock cpeStock;
		try {
			  cpeDlvId = storedProcedureDAO.executeStoredProcedure("CPEDELIVERYID");
			  tenantCpeDlv = new TenantCpeDlv();
			tenantCpeDlv.setDlvId(cpeDlvId);
			tenantCpeDlv.setDlvDate(Calendar.getInstance().getTime());
			tenantCpeDlv.setDlvQty(Long.valueOf(cpeHelperDTO.getCpeSlnoList().size()));
			tenantCpeDlv.setDmndId(Long.parseLong(cpeHelperDTO.getDemandId()));
			tenantCpeDlv.setStatus(new Short("0"));
			tenantCpeDlv.setCreatedon(Calendar.getInstance());
			tenantCpeDlv.setModifiedon(Calendar.getInstance());
			tenantCpeDlv = cpeDao.saveOrUpdate(tenantCpeDlv);
			for (String cpeSno : cpeHelperDTO.getCpeSlnoList()) {
				  cpeStock = cpeDao.getCpeByCpeSno(cpeSno);
				cpeStock.setDlvId(cpeDlvId);

				if (cpeHelperDTO.getTenantType().equalsIgnoreCase(CpeStatus.BLOCKED_FOR_MSP.getTenantType())) {
					cpeStock.setStatus(CpeStatus.BLOCKED_FOR_MSP.getStatus());
					cpeStock.setMspCode(cpeHelperDTO.getTenantCode());
					cpeStock.setLmoCode("");
				}

				if (cpeHelperDTO.getTenantType().equalsIgnoreCase(CpeStatus.BLOCKED_FOR_LMO.getTenantType())) {
					cpeStock.setStatus(CpeStatus.BLOCKED_FOR_LMO.getStatus());
					cpeStock.setLmoCode(cpeHelperDTO.getTenantCode());
					cpeStock.setMspCode("");
				}

				cpeStock.setModifiedon(Calendar.getInstance());
				cpeDao.saveOrUpdate(cpeStock);
			}
			result.append("CPE Allocated Successfully..");
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
			result.append("Failed To Allocate CPE");
		} finally {
			cpeDlvId = null;
			tenantCpeDlv = null;
			cpeStock = null;
		}
		return result.toString();
	}

	@Override
	@Transactional
	public String cpePayment(CpeHelperDTO cpeHelperDTO) {
		StringBuilder result = new StringBuilder();
		Long cpePaymentId;
		TenantCpePmnts tenantCpePmnts;
		try {
			  cpePaymentId = storedProcedureDAO.executeStoredProcedure("CPEPAYMENTID");
			  tenantCpePmnts = new TenantCpePmnts();
			tenantCpePmnts.setPmntId(cpePaymentId);
			tenantCpePmnts.setPmntAmt(new BigDecimal(cpeHelperDTO.getTotalCost()));
			tenantCpePmnts.setPmntDate(Calendar.getInstance());
			tenantCpePmnts.setDlvId(Long.parseLong(cpeHelperDTO.getDlvId()));
			tenantCpePmnts.setPmntMode(cpeHelperDTO.getPaymentMode());
			tenantCpePmnts.setPmntRefNo(cpePaymentId.toString());
			tenantCpePmnts.setStatus(new Short("1"));
			tenantCpePmnts = cpeDao.saveOrUpdate(tenantCpePmnts);

			cpeDao.updateCpeStock(tenantCpePmnts.getDlvId());
			cpeDao.updateCpeDelivery(tenantCpePmnts.getDlvId());
			result.append("Payment Done Successfully..");
		} catch (Exception ex) {
			result.append("Failed To Make Payment");
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
		} finally{
			cpePaymentId = null;
			tenantCpePmnts = null;
		}
		return result.toString();
	}

	@Override
	public List<String> getAllCpeSnosByDlvId(String dlvId) {
		return cpeDao.getAllCpeSnosByDlvId(dlvId);
	}

	@Override
	public List<CpeTypeMobileDTO> getCpeModelDetails() {
		List<CpeTypeMobileDTO> cpeTypesList = new ArrayList<>();
		CpeTypeMobileDTO cpeTypeMobileDTO = null;
		CpeModelMobileDTO cpeModelMobileDTO = null;
		List<CpeModelMobileDTO> cpeModelMobileDTOList = null;
		List<Object> cpemasterList;

		try {

			  cpemasterList = cpeDao.findAllCpeMasterList();
			for (Object cpeType : cpemasterList) {
				cpeModelMobileDTOList = new ArrayList<>();
				List<Object[]> cpeModelslist = cpeDao.findAllCpeModels(cpeType.toString());
				for (Object[] obj : cpeModelslist) {
					cpeModelMobileDTO = new CpeModelMobileDTO();
					cpeModelMobileDTO.setCpeModel(obj[0].toString());
					cpeModelMobileDTO.setCpeName(obj[1].toString());
					cpeModelMobileDTO.setCpePrice(obj[2].toString());
					cpeModelMobileDTO.setProfileId(obj[3].toString());
					cpeModelMobileDTOList.add(cpeModelMobileDTO);
				}

				cpeTypeMobileDTO = new CpeTypeMobileDTO();
				cpeTypeMobileDTO.setCpeType(cpeType.toString());
				cpeTypeMobileDTO.setCpeModels(cpeModelMobileDTOList);
				cpeTypesList.add(cpeTypeMobileDTO);
			}

		} catch (Exception e) {
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		} finally {
			cpeTypeMobileDTO = null;
			  cpeModelMobileDTO = null;
			  cpeModelMobileDTOList = null;
		}

		return cpeTypesList;
	}

	@Override
	@Transactional
	public CpeHelperDTO saveList(CpeHelperDTO cpehelperDto) {
		String response = "";
		Tenant tenant = null;
		Tenant uploadedBy = null;
		int status = 0;
		String lmoCode = null;
		String mspCode = null;
		List<String> cpeList = new ArrayList<>();
		List<String> cpeNewList = new ArrayList<>();
		List<CpeStock> newCpes = new ArrayList<>();
		CpeHelperDTO cpeHelperDTO = new CpeHelperDTO();
		try{
			uploadedBy = tenantDao.findByTenantCode(cpehelperDto.getTenantCode());
			tenant = tenantDao.findByTenantId(Integer.parseInt(cpehelperDto.getTenantId()));
			if(uploadedBy.getTenantTypeLov().equalsIgnoreCase("APSFL") || uploadedBy.getTenantTypeLov().equalsIgnoreCase("APFGC")){
				if(tenant.getTenantTypeLov().equalsIgnoreCase("LMO") || tenant.getTenantTypeLov().equalsIgnoreCase("SI")){
					status = 3;
					lmoCode = tenant.getTenantCode();
				}
				else if(tenant.getTenantTypeLov().equalsIgnoreCase("MSP")){
					status = 2;
					mspCode = tenant.getTenantCode();
				}
				
				for(CpeStock cpeStock : cpehelperDto.getCpeList()){
					CpeStock cpeStockObj = cpeDao.getCpeByCpeSnoAndMac(cpeStock.getCpeslno().trim(),cpeStock.getCpeMacAddr().trim());
					if(cpeStockObj == null){
						cpeStock.setProfileId(Long.parseLong(cpehelperDto.getProfileId()));
						cpeStock.setModifiedon(Calendar.getInstance());
						cpeStock.setStatus(status);
						cpeStock.setLmoCode(lmoCode);
						cpeStock.setMspCode(mspCode);
						cpeStock.setBatchDate(Calendar.getInstance().getTime());
						
						//cpeDao.saveOrUpdate(cpeStock);
						newCpes.add(cpeStock);//added for cpe stock siddharth
						cpeNewList.add(cpeStock.getCpeslno());
					}else{
						//cpeList.add(cpeStock.getCpeslno());
						cpeList.add(cpeStockObj.getCpeslno());
					}
					
				}
				
				if (cpeList.isEmpty()){
					for(CpeStock cpeStock :newCpes){
						
						cpeDao.saveOrUpdate(cpeStock,cpehelperDto.getTenantCode(),cpeStock.getCpeslno());
					}
					
					LOGGER.info("uploaded "+newCpes.size()+ " cpeStock. Uploaded by : " + uploadedBy.getTenantCode());
					
				}
						
				
				if(tenant.getTenantTypeLov().equalsIgnoreCase("LMO"))
					cpeDao.updateCpeBlockedAmount(cpeNewList.size(),cpehelperDto.getProfileId(),tenant.getTenantCode());
				
				//LOGGER.info("Duplicate CPE Serial Numbers Size:: "+cpeList.size());
				//LOGGER.info("Cpe Slno Already Assign To another Tenant :: "+cpeList.toString());
				response = cpeList.size() == 0 ? "<font color='green'> Cpe Stock List Uploaded Successfully..</font>" : " <font color='red'> Failed To upload some data(CPE Serial No / CPE MAC Address  Dublicate)</font> ";	
			}else if(uploadedBy.getTenantTypeLov().equalsIgnoreCase("MSP") || uploadedBy.getTenantTypeLov().equalsIgnoreCase("MSO")){
				
				for(CpeStock cpeStock : cpehelperDto.getCpeList()){
					CpeStock cpeStockObj = cpeDao.getCpeByCpeSnoAndMspCode(cpeStock.getCpeslno().trim(),uploadedBy.getTenantCode());
					if(cpeStockObj != null && (cpeStockObj.getLmoCode() ==  null || cpeStockObj.getLmoCode().isEmpty())){
						cpeStockObj.setModifiedon(Calendar.getInstance());
						cpeStockObj.setStatus(3);
						cpeStockObj.setLmoCode(tenant.getTenantCode());
						cpeStockObj.setBatchDate(Calendar.getInstance().getTime());
						//cpeDao.saveOrUpdate(cpeStockObj);
						newCpes.add(cpeStock);//added for cpe stock..Siddharth
						cpeNewList.add(cpeStock.getCpeslno());
					}else{
						//cpeList.add(cpeStock.getCpeslno());
						cpeList.add(cpeStockObj.getCpeslno());
					}
					
				}
				
				if (cpeList.isEmpty()){
					for(CpeStock cpeStock :newCpes){
						
						cpeDao.saveOrUpdate(cpeStock,cpehelperDto.getTenantCode(),cpeStock.getCpeslno());
					}
					
				}
				
				//cpeDao.updateCpeBlockedAmount(cpeNewList.size(),cpehelperDto.getProfileId(),tenant.getTenantCode());
				LOGGER.info("Duplicate CPE Serial Numbers Size:: "+cpeList.size());
				LOGGER.info("Cpe Slno Already Assign To another Tenant :: "+cpeList.toString());
				response = cpeList.size() == 0 ? "<font color='green'> Cpe Stock List Uploaded Successfully..</font>" : "<font color='red'> Failed To upload some data(CPE Serial No / CPE MAC Address  Dublicate)</font> ";
			}
			cpeHelperDTO.setCpeSlnoList(cpeList);
			cpeHelperDTO.setStatus(response);
			
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
			response = ex.getMessage();
		} finally {
			cpehelperDto = null;
			 tenant = null;
			 lmoCode = null;
			 mspCode = null;
		}
		return cpeHelperDTO;
	}

	@Override
	public String processPyament(String amount, String deliveryId, long requestId) {
		String responseValue= "";
		
		try{
			TenantCpePmnts tenantCpePmnts = new TenantCpePmnts();
			tenantCpePmnts.setPmntId(storedProcedureDAO.executeStoredProcedure("CPEPAYMENTID"));
			tenantCpePmnts.setPmntAmt(new BigDecimal(amount));
			tenantCpePmnts.setPmntDate(Calendar.getInstance());
			tenantCpePmnts.setDlvId(Long.parseLong(deliveryId));
			tenantCpePmnts.setPmntMode("Online");
			tenantCpePmnts.setPmntRefNo(String.valueOf(requestId));
			tenantCpePmnts.setStatus(new Short("1"));
			tenantCpePmnts = cpeDao.saveOrUpdate(tenantCpePmnts);
			cpeDao.updateCpeStock(tenantCpePmnts.getDlvId());
			cpeDao.updateCpeDelivery(tenantCpePmnts.getDlvId());
			responseValue = "Success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return responseValue;
	}

}
