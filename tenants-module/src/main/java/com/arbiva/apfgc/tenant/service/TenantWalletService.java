/*package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.model.TenantWallet;
import com.arbiva.apfgc.tenant.vo.TenantAgreementVO;

public interface TenantWalletService {
	
	public abstract void deleteTenantWallet(TenantWallet tenantWallet);

	public abstract TenantWallet findByTenantCode(String tenantCode);

	public abstract List<TenantWallet> findAllTenantWallets();

	public abstract void saveTenantWallet(TenantAgreementVO tenantAgreementVO, String action);

	public abstract TenantWallet updateTenantWallet(TenantAgreementVO tenantAgreementVO);
	
	public abstract TenantWallet updateTenantWallet(TenantAgreementVO tenantAgreementVO, String remoteIPAddr);
	
	public void saveTenantWallet(TenantAgreementVO tenantAgreementVO, String action,String remoteIPAddr);

}
*/