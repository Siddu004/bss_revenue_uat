package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.dao.RsAgrPartnersDao;
import com.arbiva.apfgc.tenant.model.RsAgrPartners;

@Repository
public class RsAgrPartnersDaoImpl implements RsAgrPartnersDao {

	
	private static final Logger LOGGER = Logger.getLogger(RsAgrPartnersDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	@Override
	public void save(RsAgrPartners rsAgreementPartners) {
		
		try{
			getEntityManager().merge(rsAgreementPartners);
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
		}
		
	}
	
	/*SELECT agruniqueid, rstmplcode,tenantcode, prodcode, prodname, prodtype, mspname,lmoname, effectivefrom, createdby, lockindays
      ,SPLIT_STRING(prodcharge,',',1) prodcharge,SPLIT_STRING(prodcharge,',',2) taxcomponent, durationdays, custtypelov 
 FROM (SELECT agr.agruniqueid, agr.rstmplcode ,agr.tenantcode ,agr.prodcode,pr.prodname,pr.prodtype 
   	         ,(SELECT tenantname FROM tenants t1,rsagrpartners prtnr1 WHERE prtnr1.agruniqueid = agr.agruniqueid AND t1.tenantcode=prtnr1.partnercode AND prtnr1.partnertype='MSP' AND prtnr1.status=1) mspname 
   	         ,(SELECT tenantname FROM tenants t2,rsagrpartners prtnr2 WHERE prtnr2.agruniqueid = agr.agruniqueid AND t2.tenantcode=prtnr2.partnercode AND prtnr2.partnertype='LMO' AND prtnr2.status=1) lmoname 
		     ,pr.effectivefrom,pr.createdby, pr.lockindays 
		     ,get_prodcharge ( pr.tenantcode,pr.prodcode,NULL,NULL,'1,2,3' ,DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'), NULL,'N') prodcharge 
		     ,pr.durationdays ,pr.custtypelov 
	     FROM rsagrpartners prtnr, rsagrmnts agr, products pr 
	    WHERE prtnr.partnercode='APSFL' AND prtnr.agruniqueid=agr.agruniqueid
          AND agr.status = 1 AND prtnr.status=1

		  AND agr.tenantcode = pr.tenantcode AND agr.prodcode = pr.prodcode 
		  AND agr.agrfdate =(SELECT MAX(agr2.agrfdate) FROM rsagrmnts agr2 
		                      WHERE agr2.tenantcode=agr.tenantcode AND agr2.prodcode=agr.prodcode 
		                        AND agr2.partnerlist=agr.partnerlist AND agr2.agrfdate <= CURRENT_DATE() 
			 		  	    ) 
		  AND CURRENT_DATE() BETWEEN pr.effectivefrom AND pr.effectiveto  ORDER BY pr.prodtype 
      ) X*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> viewAllPkgAgree(String tenantcode, String tenantType) {
		List<Object[]> rsAgrPrtList = new ArrayList<Object[]>();
		 Query query = null;
		StringBuilder builder = new StringBuilder();
		builder.append(" ");
		
		builder.append("SELECT agruniqueid, rstmplcode,tenantcode, prodcode, prodname, prodtype, mspname,lmoname, effectivefrom, createdby, lockindays");
		builder.append("  ,SPLIT_STRING(prodcharge,',',1) prodcharge,SPLIT_STRING(prodcharge,',',2) taxcomponent, durationdays, custtypelov ");
		builder.append(" FROM (SELECT agr.agruniqueid, agr.rstmplcode ,agr.tenantcode ,agr.prodcode,pr.prodname,pr.prodtype ");
		builder.append("      ,(SELECT tenantname FROM tenants t1,rsagrpartners prtnr1 WHERE prtnr1.agruniqueid = agr.agruniqueid AND t1.tenantcode=prtnr1.partnercode AND prtnr1.partnertype='MSP' AND prtnr1.status=1) mspname"); 
		builder.append("   ,(SELECT tenantname FROM tenants t2,rsagrpartners prtnr2 WHERE prtnr2.agruniqueid = agr.agruniqueid AND t2.tenantcode=prtnr2.partnercode AND prtnr2.partnertype='LMO' AND prtnr2.status=1) lmoname ");
		builder.append(" ,pr.effectivefrom,pr.createdby, pr.lockindays ");
		builder.append(" ,get_prodcharge ( pr.tenantcode,pr.prodcode,NULL,NULL,'1,2,3' ,DATE_FORMAT(CURRENT_DATE(),'%Y%m%d'), NULL,'N') prodcharge ");
		builder.append(" ,pr.durationdays ,pr.custtypelov ");
		builder.append(" FROM rsagrpartners prtnr, rsagrmnts agr, products pr ");
		builder.append(" WHERE prtnr.partnercode= :tenantCode AND prtnr.agruniqueid=agr.agruniqueid");
		builder.append(" AND agr.status = 1 AND prtnr.status=1");
		builder.append(" AND agr.tenantcode = pr.tenantcode AND agr.prodcode = pr.prodcode ");
		builder.append(" AND agr.agrfdate =(SELECT MAX(agr2.agrfdate) FROM rsagrmnts agr2 ");
		builder.append(" WHERE agr2.tenantcode=agr.tenantcode AND agr2.prodcode=agr.prodcode ");
		builder.append(" AND agr2.partnerlist=agr.partnerlist AND agr2.agrfdate <= CURRENT_DATE() ");
		builder.append(" ) ");
		builder.append("AND CURRENT_DATE() BETWEEN pr.effectivefrom AND pr.effectiveto  ORDER BY pr.prodtype"); 
		builder.append(") X");
		 try {
			 query = getEntityManager().createNativeQuery(builder.toString());
			 query.setParameter("tenantCode", tenantcode);
			 rsAgrPrtList = query.getResultList();
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::viewAllPkgAgree() " + e);
			} finally {
				query = null;
				builder = null;
			}
		  return rsAgrPrtList;
		  }

}
