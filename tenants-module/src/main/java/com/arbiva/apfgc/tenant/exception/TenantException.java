package com.arbiva.apfgc.tenant.exception;

import com.arbiva.apfgc.tanent.util.TenantErrorCodes;

public class TenantException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int errorCode;
	private String errorDescription;
	private String message;
	
	public TenantException(){
		super();
	}
	
	


	/**
	 * @param TENT003
	 * @param string
	 */
	public TenantException(TenantErrorCodes prod003, String message) {
		
		this.errorCode = prod003.getCode();
		this.errorDescription = prod003.getDescription();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
}
