package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.arbiva.apfgc.tenant.vo.ProductAgreementVO;

@Entity
@Table(name="rsagrmnts")
@IdClass(RsAgrmntsPk.class)
public class RsAgrmnts {

	@Id
	@Column(name = "agruniqueid")
	private Long agruniqueid;

	@Id
	@Column(name = "tenantcode")
	private String tenantcode;

	@Id
	@Column(name = "prodcode")
	private String prodcode;

	@Id
	@Column(name = "agrfdate")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "MM")
	private Date agrfdate;

	@Column(name = "rstmplcode")
	private String rstmplcode;
	
	@Column(name = "partnerlist")
	private String partnerList;

	@Column(name = "status", columnDefinition = "tinyint(1) default 1")
	@NotNull
	private Short status;

	
	@ManyToOne
	@JoinColumn(name = "rstmplcode", referencedColumnName = "rstmplcode", nullable = false, insertable = false, updatable = false)
	private RevenueSharingTemplateMaster rsTemplateMaster;
	
	public RsAgrmnts() {
	}

	public RsAgrmnts(ProductAgreementVO productAgreementVO) {
		
		this.setAgrfdate(Calendar.getInstance().getTime());
		this.setProdcode(productAgreementVO.getProdCode());
		this.setRstmplcode(productAgreementVO.getTmplCode());
		this.setStatus(new Short("1"));
		this.setTenantcode(productAgreementVO.getTenantCode());
	}

	/*public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}*/
	
	

	public RevenueSharingTemplateMaster getRsTemplateMaster() {
		return rsTemplateMaster;
	}

	public String getPartnerList() {
		return partnerList;
	}

	public void setPartnerList(String partnerList) {
		this.partnerList = partnerList;
	}

	public void setRsTemplateMaster(RevenueSharingTemplateMaster rsTemplateMaster) {
		this.rsTemplateMaster = rsTemplateMaster;
	}

	public Long getAgruniqueid() {
		return agruniqueid;
	}

	public void setAgruniqueid(Long agruniqueid) {
		this.agruniqueid = agruniqueid;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public Date getAgrfdate() {
		return agrfdate;
	}

	public void setAgrfdate(Date agrfdate) {
		this.agrfdate = agrfdate;
	}

	public String getRstmplcode() {
		return rstmplcode;
	}

	public void setRstmplcode(String rstmplcode) {
		this.rstmplcode = rstmplcode;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

}
