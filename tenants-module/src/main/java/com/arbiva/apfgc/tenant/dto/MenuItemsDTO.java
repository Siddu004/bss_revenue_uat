package com.arbiva.apfgc.tenant.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author Sai Suresh P
 *
 */
public class MenuItemsDTO implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("menuPathID")
	private String menuPathID;
	
	@JsonProperty("menuItem")
	private String menuItem;
	
	@JsonProperty("actionPath")
	private String actionPath;

	public String getMenuPathID() {
		return menuPathID;
	}

	public void setMenuPathID(String menuPathID) {
		this.menuPathID = menuPathID;
	}

	public String getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(String menuItem) {
		this.menuItem = menuItem;
	}

	public String getActionPath() {
		return actionPath;
	}

	public void setActionPath(String actionPath) {
		this.actionPath = actionPath;
	}
	
}
