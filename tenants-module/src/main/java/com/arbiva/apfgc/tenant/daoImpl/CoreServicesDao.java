/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.model.CoreServices;

/**
 * @author Arbiva
 *
 */
@Component("coreServicesDao")
public class CoreServicesDao {

	private static final Logger LOGGER = Logger.getLogger(CoreServicesDao.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public List<CoreServices> findAllCoreServices() {
		 List<CoreServices> coreServices = new ArrayList<CoreServices>();
		 StringBuilder builder = new StringBuilder();
			try {
				LOGGER.info("CoreServicesDao :: findAllCoreServices() :: START");
				 builder.append(" FROM ").append(CoreServices.class.getSimpleName());
				TypedQuery<CoreServices> query = getEntityManager().createQuery(builder.toString(), CoreServices.class);
				coreServices = query.getResultList();
				LOGGER.info("CoreServicesDao :: findAllCoreServices() :: END");
			} catch (Exception e) {
				LOGGER.error("CoreServicesDao :: findAllCoreServices() :: "+e);
			} finally {
				builder = null;
			}
		  return coreServices;
	}

	@SuppressWarnings("unchecked")
	public List<CoreServices> findAllCoreServicesByTenantSrvcs(String tenantCode) {
		 StringBuilder builder = new StringBuilder();
		 Query query = null;
		try{ 
			LOGGER.info("CoreServicesDao :: findAllCoreServicesByTenantSrvcs() :: START");
			 builder.append("SELECT * FROM coresrvcs  where srvccode in  (SELECT coresrvccode  FROM tenantsrvcs where tenantcode = '"+tenantCode+"');");
		     query = getEntityManager().createNativeQuery(builder.toString(), CoreServices.class);
			LOGGER.info("CoreServicesDao :: findAllCoreServicesByTenantSrvcs() :: START");
		} catch(Exception e){
		LOGGER.error("CoreServicesDao :: findAllCoreServicesByTenantSrvcs() "+e);
	} finally {
		builder = null;
	}
		
		return query.getResultList();
	}
}
