package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;
import com.arbiva.apfgc.tenant.vo.CpeStockVO;

@Repository
public class RevenueSharingDaoImpl {

	private static final Logger LOGGER = Logger.getLogger(RevenueSharingDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	@Transactional
	public List<Object[]> getTenantTypeLovs(String lovName) {
		List<Object[]> lovList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
			try {
				 LOGGER.info("RevenueSharingDaoImpl :: getTenantTypeLovs() :: SATRT");
				  builder.append("select lovid,lovvalue from lovs ");
				builder.append(" where lovname = :lovName ");
				builder.append(" order by lovseq asc ");
					 query = em.createNativeQuery(builder.toString());
					query.setParameter("lovName", lovName);
					lovList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION:: RevenueSharingDaoImpl::getTenantTypeLovs() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return lovList;
	}
	
	@Transactional
	public List<Object[]> getRegionLovs() {
		List<Object[]> lovList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select regioncode,regionname from taxregions ");
		builder.append(" where status = 1");
		builder.append(" order by regionname asc ");
		try{
			  query = em.createNativeQuery(builder.toString());
			lovList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getRegionLovs() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return lovList;
	}
	
	@Transactional
	public RevenueSharingTemplateMaster saveTemplateMaster(RevenueSharingTemplateMaster rsTemplateMaster){
		return getEntityManager().merge(rsTemplateMaster);
	}
	
	@Transactional
	public void saveTemplateDetails(RevenueSharingTemplateDetails rsTempDetails){
		getEntityManager().persist(rsTempDetails);
	}
	
	@Transactional
	public List<Object[]> getTemplatesLov() {
		List<Object[]> lovList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select rstmplcode,rstmplname from rstmplmst ");
		builder.append(" where status = 1");
		builder.append(" order by rstmplname asc ");
		try{
			  query = em.createNativeQuery(builder.toString());
			lovList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTemplatesLov() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return lovList;
	}
	
	@Transactional
	public RevenueSharingTemplateMaster getTemplateValues(String tempCode) {
		RevenueSharingTemplateMaster rsTempMaster = null;
		try{
			rsTempMaster = getEntityManager().find(RevenueSharingTemplateMaster.class, tempCode);
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTemplateValues() " + ex);
		} finally {}
		return rsTempMaster;
	}
	
	@Transactional
	public List<RevenueSharingTemplateDetails> getRegTempPercList(String tempCode, String region) {
		List<RevenueSharingTemplateDetails> rsTemplateDtlsList = new ArrayList<RevenueSharingTemplateDetails>();
		TypedQuery<RevenueSharingTemplateDetails> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(RevenueSharingTemplateDetails.class.getSimpleName());
		builder.append(" WHERE rstmplcode = :tempCode and regioncode = :region");
		builder.append(" order by tenantslno asc");
		try{
			 query = getEntityManager().createQuery(builder.toString(), RevenueSharingTemplateDetails.class);
			query.setParameter("tempCode", tempCode);
			query.setParameter("region", region);
			rsTemplateDtlsList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getRegTempPercList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return rsTemplateDtlsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RevenueSharingTemplateMaster> getTemplatesByCount(Integer count) {
		
		List<RevenueSharingTemplateMaster> rsTemplatesMst = new ArrayList<RevenueSharingTemplateMaster>();
		Query query = null;
		StringBuilder builder = new StringBuilder("SELECT * FROM rstmplmst where partnercnt ="+count+" and rstmpltypelov = 'PACKAGE' and status = 1 order by rstmplname");
		try{
			  query = em.createNativeQuery(builder.toString(), RevenueSharingTemplateMaster.class);
			rsTemplatesMst = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTemplatesByCount() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return rsTemplatesMst;
	}

	@SuppressWarnings("unchecked")
	public List<RevenueSharingTemplateDetails> getAllTenantTypeByTemplateCode(String tmplCode, String region) {
		List<RevenueSharingTemplateDetails> rsTemplatesMst = new ArrayList<RevenueSharingTemplateDetails>();
		Query query = null;
		
		StringBuilder builder = new StringBuilder("SELECT * FROM rstmpldtl where rstmplcode = '"+tmplCode+"' and status = 1 and regioncode = '"+region+"' order by tenanttype ");
		try{
			  query = em.createNativeQuery(builder.toString(), RevenueSharingTemplateDetails.class);
			rsTemplatesMst = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getAllTenantTypeByTemplateCode() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return rsTemplatesMst;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllTemplateRegions(String tmplCode) {
		List<Object[]> regionsList = new ArrayList<Object[]>();
		//StringBuilder builder = new StringBuilder("select distinct regioncode from rstmpldtl  where rstmplcode = '"+tmplCode+"'");
		
		StringBuilder builder = new StringBuilder("SELECT distinct r.regionname, r.regioncode FROM rstmpldtl t, taxregions r where rstmplcode = :tmplCode and t.regioncode = r.regioncode ");
		Query query = null;
		try{
			  query = em.createNativeQuery(builder.toString());
			query.setParameter("tmplCode", tmplCode);
			regionsList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getAllTenantTypeByTemplateCode() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return regionsList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllTenantsByTemplCode(String tmplCode) {
		List<Object[]> returnList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select distinct tenantslno , tenanttype from rstmpldtl where rstmplcode = '"+tmplCode+"'");
		try{
			  query = em.createNativeQuery(builder.toString());
			returnList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getAllTenantTypeByTemplateCode() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return returnList;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getTenantTypeVals(String tempCode) {
		List<Object[]> lovList = new ArrayList<Object[]>();
		Query query = null;
		StringBuilder builder = new StringBuilder("select distinct tenantslno, tenanttype ");
		builder.append(" from rstmpldtl ");
		builder.append(" where rstmplcode= :tempCode ");
		builder.append(" order by tenantslno asc ");
		try{
			  query = em.createNativeQuery(builder.toString());
			query.setParameter("tempCode", tempCode);
			lovList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return lovList;
	}

	@SuppressWarnings("unchecked")
	public List<RevenueSharingTemplateMaster> getAllSimilarTemplatesByTemplCode(String templCode) {
		Query query = null;
		List<RevenueSharingTemplateMaster> templList = new ArrayList<RevenueSharingTemplateMaster>();
		StringBuilder builder = new StringBuilder(" SELECT * FROM rstmplmst a WHERE STATUS=1 "
													+ "  AND checkforsamepartners( :tempCode ,a.rstmplcode)=0 AND rstmpltypelov='PACKAGE' ");
		try{
			  query = em.createNativeQuery(builder.toString(), RevenueSharingTemplateMaster.class);
			query.setParameter("tempCode", templCode);
			templList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		}  finally {
			query = null;
			builder = null;
		}
		return templList;
	}

	public List<Object[]> getChargeCodesByChargeLevelFlage() {
		List<Object[]> list = new ArrayList<Object[]>();
		Query query  = null;
		StringBuilder builder = new StringBuilder(" select * from chargecodes where chargelevelflag = 3 ");
		try{
			  query = em.createNativeQuery(builder.toString());
			list = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return list;
	}
	
	
	@Transactional
	public List<RevenueSharingTemplateDetails> getRevPercList() {
		List<RevenueSharingTemplateDetails> rsTemplateDtlsList = new ArrayList<RevenueSharingTemplateDetails>();
		TypedQuery<RevenueSharingTemplateDetails> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(RevenueSharingTemplateDetails.class.getSimpleName());
		builder.append(" WHERE regioncode = :region");
		builder.append(" order by tenantslno asc");
		try{
			 query = getEntityManager().createQuery(builder.toString(), RevenueSharingTemplateDetails.class);
	//		query.setParameter("tempCode", tempCode);
			query.setParameter("region", "GENERAL");
			rsTemplateDtlsList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getRevPercList() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return rsTemplateDtlsList;
	}
	

	
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getTemplateCodefromProductCode() {
		List<Object[]> list = new ArrayList<Object[]>();
		
		Query query  = null;
		StringBuilder builder = new StringBuilder(" select  distinct prodcode,partnerlist,rstmplcode  from rsagrmnts ");
		try{
			  query = em.createNativeQuery(builder.toString());
			list = query.getResultList();
			
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return list;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> getTemplateCodeCode(String prodcode, String lmocode) {
		List<Object[]> list = new ArrayList<Object[]>();
		
		Query query  = null;
		StringBuilder builder = new StringBuilder(" select  distinct prodcode,partnerlist,rstmplcode  from rsagrmnts where prodcode = '"+prodcode+"' and partnerlist like '%"+lmocode+"%'");
		try{
			  query = em.createNativeQuery(builder.toString());
			list = query.getResultList();
			
		}catch(Exception ex){
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@SuppressWarnings("unchecked")
	public List<Object[]> getLMOAPSFLShare(String lmocode) {

		List<Object[]> list = new ArrayList<Object[]>();

		Query query = null;
		StringBuilder builder = new StringBuilder(
				" select invyr,invmn,sum(apsflinv_share) from rev_summarytable where  lmocode='"+lmocode+"' group by invyr,invmn ");
		try {
			query = em.createNativeQuery(builder.toString());
			list = query.getResultList();

		} catch (Exception ex) {
			LOGGER.error("EXCEPTION::getTenantTypeVals() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return list;

	}
	
	
	
	
	
	
	
	

}
