/**
 * 
 *//*
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.TenantLMOServices;

*//**
 * @author Arbiva
 *
 *//*
@Repository
public class TenantLMOServicesDaoImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantLMOServicesDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public List<TenantLMOServices> findByMspCode(String mspCode, String effDate, String lmoCode) {
		List<TenantLMOServices> tenantLMOServices = new ArrayList<TenantLMOServices>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLMOServices.class.getSimpleName())
				.append(" WHERE mspcode=:mspCode AND agrfdate=:agrFdate AND lmocode=:lmoCode");
		try {
			LOGGER.info("START::findByMspCode()");
			TypedQuery<TenantLMOServices> query = getEntityManager().createQuery(builder.toString(),TenantLMOServices.class);
			query.setParameter("mspCode", mspCode);
			query.setParameter("agrFdate", effDate);
			query.setParameter("lmoCode", lmoCode);
			tenantLMOServices = query.getResultList();
			LOGGER.info("END::findByMspCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByMspCode() " + e);
		}
		return tenantLMOServices;

	}
	
	public TenantLMOServices findByLmoCode(String lmoCode) {
		TenantLMOServices tenantLMOServices = new TenantLMOServices();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLMOServices.class.getSimpleName()).append(" WHERE lmocode=:lmoCode");
		try {
			LOGGER.info("START::findByLmoCode()");
			TypedQuery<TenantLMOServices> query = getEntityManager().createQuery(builder.toString(),TenantLMOServices.class);
			query.setParameter("lmoCode", lmoCode);
			tenantLMOServices = query.getSingleResult();
			LOGGER.info("END::findByLmoCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByLmoCode() " + e);
		}
		return tenantLMOServices;

	}
	
	public List<TenantLMOServices> findAllTenantLMOServices() {
		List<TenantLMOServices> tenantLMOServicesList = new ArrayList<TenantLMOServices>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLMOServices.class.getSimpleName());
		try {
			LOGGER.info("START::findAllTenantLMOServices()");
			TypedQuery<TenantLMOServices> query = getEntityManager().createQuery(builder.toString(), TenantLMOServices.class);
			tenantLMOServicesList = query.getResultList();
			LOGGER.info("END::findAllTenantLMOServices()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenantLMOServices() " + e);
		}
		return tenantLMOServicesList;
	}
	
	public void saveTenantLMOServices(TenantLMOServices tenantLMOServices) {
		try {
			getEntityManager().merge(tenantLMOServices);
		} catch(Exception e) {
			LOGGER.error("EXCEPTION::saveTenantLMOServices() " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<String> findCoreServicesByMspCode(String mspCode, String effDate, String lmoCode) {
		StringBuilder builder = new StringBuilder("SELECT distinct c.srvcname FROM lmosrvcs ls , coresrvcs c where "
				+ " ls.mspcode = '"+mspCode+"' AND "
				+ " ls.lmocode = '"+lmoCode+"' AND "
				+ " ls.agrfdate = '"+effDate+"' AND "
				+ " ls.coresrvccode = c.srvccode");
		
		Query query = getEntityManager().createNativeQuery(builder.toString());
		return query.getResultList();
	}
}
*/