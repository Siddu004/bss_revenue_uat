package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.PortalMspLmos;

@Repository
public class CpeDemandNoteDaoImpl {
	
	private static final Logger LOGGER = Logger.getLogger(CpeDemandNoteDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getAllLmosByTenantCodeMigrData(String tenantCode) {
		List<Object[]> responce = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			builder.append(" SELECT 1, a.lmoname, IFNULL(b.noemidemandqty,0),IFNULL(b.emi36demandqty,0),IFNULL(b.emi48demandqty,0) FROM ");
			builder.append(" (SELECT DISTINCT LMO_NAME lmoname FROM portal_msp_lmos p WHERE p.mso_enrollment_number = :tenantCode )a LEFT JOIN ");
			builder.append(" (SELECT lmoname, SUM(noemidemandqty) noemidemandqty, SUM(emi36demandqty) emi36demandqty, SUM(emi48demandqty) emi48demandqty  ");
			builder.append(" from mspcpedmd m , mspcpedmddtl md  ");
			builder.append(" where md.dmdid = m.dmdid AND m.mspcode = :tenantCode ");
			builder.append(" group by lmoname)b ");
			builder.append(" ON a.lmoname = b.lmoname ");
			
			 query = getEntityManager().createNativeQuery(builder.toString());
			 query.setParameter("tenantCode", tenantCode);
			 responce = query.getResultList();
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getAllLmosByTenantCodeMigrData() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return responce;
	}
	
	@SuppressWarnings("rawtypes")
	public PortalMspLmos findByMsoLmoId(String msoLmoId) {
		Query query = null;
		StringBuilder builder = new StringBuilder("select * from portal_msp_lmos where mso_lmo_id = :msoLmoId");
		PortalMspLmos portalMspLmos = null;
		
		try{
			query = getEntityManager().createNativeQuery(builder.toString(), PortalMspLmos.class);
			query.setParameter("msoLmoId", msoLmoId);
			List list =  query.getResultList();
			if(list.size() > 0){
				portalMspLmos =   (PortalMspLmos)list.get(0) ;
			}
		
		}catch(Exception e){
			LOGGER.error("EXCEPTION::findByMsoLmoId() " + e);
		}
		
		return portalMspLmos;
	}
	
	 public <T> T saveOrUpdate(T t) {
	        return (T)this.getEntityManager().merge(t);
	    }


}
