package com.arbiva.apfgc.tenant.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditLmoWallet {

	@JsonProperty("LMOcode")
	private String lmoCode;
	
	@JsonProperty("Mobilenumber")
	private String mobileNumber;
	
	@JsonProperty("Amountpaid")
	private String amountPaid;
	
	@JsonProperty("Typeofpayment")
	private String typeOfPayment;
	
	@JsonProperty("Dateofpayment")
	private String dateOfPayment;
	
	@JsonProperty("Tranrefnumber")
	private String tranrefnumber;
	
	@JsonProperty("EncryptValue")
	private String encryptVal;
	


	public String getEncryptVal() {
		return encryptVal;
	}

	public void setEncryptVal(String encryptVal) {
		this.encryptVal = encryptVal;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getTypeOfPayment() {
		return typeOfPayment;
	}

	public void setTypeOfPayment(String typeOfPayment) {
		this.typeOfPayment = typeOfPayment;
	}

	public String getDateOfPayment() {
		return dateOfPayment;
	}

	public void setDateOfPayment(String dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}

	public String getTranrefnumber() {
		return tranrefnumber;
	}

	public void setTranrefnumber(String tranrefnumber) {
		this.tranrefnumber = tranrefnumber;
	}
}
