package com.arbiva.apfgc.tenant.dao;

import java.util.List;

import com.arbiva.apfgc.tenant.model.RsAgrPartners;

public interface RsAgrPartnersDao {

	void save(RsAgrPartners rsAgreementPartners);
	List<Object[]> viewAllPkgAgree(String tenantcode, String tenantType);

}
