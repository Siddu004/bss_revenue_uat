/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.model.TenantWallet;

/**
 * @author Lakshman
 *
 */
@Repository
public class TenantWalletDaoImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantWalletDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public TenantWallet findByTenantCode(String tenantCode) {
		TenantWallet tenantWallet = new TenantWallet();
		TypedQuery<TenantWallet> query = null;
		List result = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantWallet.class.getSimpleName()).append(" WHERE tenantcode=:tenantCode");
			try {
				LOGGER.info("START::findByTenantCode()");
				  query = getEntityManager().createQuery(builder.toString(), TenantWallet.class);
				query.setParameter("tenantCode", tenantCode);
				  result = query.getResultList();
				if(!result.isEmpty())
					tenantWallet = (TenantWallet) result.get(0);
				
				LOGGER.info("END::findByTenantCode()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::findByTenantCode() " + e);
			} finally {
				query = null;
				builder = null;
				result = null;
			}
		  return tenantWallet;
	}
	
	public void saveTenantWallet(TenantWallet tenantWallet) {
		try {
			getEntityManager().merge(tenantWallet);
		} catch(Exception e) {
			LOGGER.error("EXCEPTION::saveTenantWallet() " + e);
			e.printStackTrace();
		}
	}
	
	public List<TenantWallet> findAllTenantWallets() {
		List<TenantWallet> tenantWallet = new ArrayList<TenantWallet>();
		TypedQuery<TenantWallet> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantWallet.class.getSimpleName());
		try {
			LOGGER.info("START::findAllRegions()");
			 query = getEntityManager().createQuery(builder.toString(), TenantWallet.class);
			tenantWallet = query.getResultList();
			LOGGER.info("END::findAllRegions()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllRegions() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return tenantWallet;
	}
	
	
	
	@Transactional
	public void updateTenantsWalletOnFirst(CustomerInvDtlsDTO custdtl) {
		
		Query query1 = null;
		String whereClause="";
		BigDecimal apsflshare= new BigDecimal(custdtl.getApsflmonthlyshare());
		BigDecimal lmoshare=new BigDecimal(custdtl.getLmomonthlyshare());
		BigDecimal msoshare=new BigDecimal(custdtl.getMsomonthlyshare());		
		BigDecimal totalLmoAndMsoShare=msoshare.add(lmoshare);
		totalLmoAndMsoShare=totalLmoAndMsoShare.setScale(2, RoundingMode.HALF_UP);
		
		StringBuilder builder1 = new StringBuilder("Update  tenantswallet set wallet_lastdbamt='"+apsflshare+"' ,crlimitamt='"+totalLmoAndMsoShare);
		builder1.append("' where tenantcode='" +custdtl.getLmocode()+"'");
		try {
			
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			query1.executeUpdate();
			
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		} 
	}
	
	
	@Transactional
public void updateTenantsWalletOnEndOfMonth(String lmocode, String totalcollected) {
		
		Query query1 = null;
		
		BigDecimal totalAmtCollected=new BigDecimal(totalcollected);
		
		totalAmtCollected=totalAmtCollected.setScale(0, RoundingMode.HALF_UP);
		
		StringBuilder builder1 = new StringBuilder("Update  tenantswallet set wallet_lastcramt='"+totalAmtCollected+"'");
		builder1.append(" where tenantcode='" +lmocode);
		try {
			
			query1 = getEntityManager().createNativeQuery(builder1.toString());
			query1.executeUpdate();
			
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		} 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
