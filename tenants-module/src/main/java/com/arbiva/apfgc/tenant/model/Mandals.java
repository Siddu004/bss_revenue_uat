/**
 * 
 */
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="mandals")
public class Mandals implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="mandaluid")
	private Integer mandalUid;
	
	@Column(name="mandalname")
	private String mandalName;
	
	@Column(name="districtuid")
	private Integer districtUid;
	
	@Column(name="mandalslno")
	private Integer mandalSlno;
	
	@Column(name="stateid")
	private Integer stateId;
	
	@ManyToOne
	@JoinColumn(name = "districtuid", referencedColumnName = "districtuid", nullable = false, insertable=false, updatable=false)
	private Districts district;
	
	@ManyToOne
	@JoinColumn(name = "stateid", referencedColumnName = "stateid", nullable = false, insertable=false, updatable=false)
	private States states;
	
	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public States getStates() {
		return states;
	}

	public void setStates(States states) {
		this.states = states;
	}

	public Integer getMandalUid() {
		return mandalUid;
	}

	public void setMandalUid(Integer mandalUid) {
		this.mandalUid = mandalUid;
	}

	public String getMandalName() {
		return mandalName;
	}

	public void setMandalName(String mandalName) {
		this.mandalName = mandalName;
	}

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public Integer getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(Integer mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public Districts getDistrict() {
		return district;
	}

	public void setDistrict(Districts district) {
		this.district = district;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
