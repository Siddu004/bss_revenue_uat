package com.arbiva.apfgc.tenant.pg.utils;

import java.net.URI;
import java.net.URL;

public class PgHelper {
	
	private URI uri;
	
	private String requestBody;
	
	private String url;
	
	private String gateWayType;
	
	
	
	

	public String getGateWayType() {
		return gateWayType;
	}

	public void setGateWayType(String gateWayType) {
		this.gateWayType = gateWayType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}
	

}
