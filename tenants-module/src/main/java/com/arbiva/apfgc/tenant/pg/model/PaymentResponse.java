package com.arbiva.apfgc.tenant.pg.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Entity
@Table(name = "pgresponse")
public class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long requestId;
	private byte[] response;
	private String responseCode;
	private String responseBy;

	/**
	 * @return the requestId
	 */
	@Id
	@Column(name = "resid")
	public long getId() {
		return id;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the requestId
	 */
	@Column(name = "reqid")
	public long getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the content
	 */
	@Lob
	@Column(name = "response")
	public byte[] getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the content to set
	 */
	public void setResponse(byte[] response) {
		this.response = response;
	}

	/**
	 * @return the status
	 */
	@Column(name = "rescode")
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the status to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	/**
	 * @return the responseBy
	 */
	@Column(name = "resby")
	public String getResponseBy() {
		return responseBy;
	}

	/**
	 * @param responseBy the responseBy to set
	 */
	public void setResponseBy(String responseBy) {
		this.responseBy = responseBy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (requestId ^ (requestId >>> 32));
		result = prime * result + Arrays.hashCode(response);
		result = prime * result
				+ ((responseCode == null) ? 0 : responseCode.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentResponse other = (PaymentResponse) obj;
		if (id != other.id)
			return false;
		if (requestId != other.requestId)
			return false;
		if (!Arrays.equals(response, other.response))
			return false;
		if (responseCode == null) {
			if (other.responseCode != null)
				return false;
		} else if (!responseCode.equals(other.responseCode))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentResponse [id=" + id + ", requestId=" + requestId
				+ ", response=" + Arrays.toString(response) + ", responseCode="
				+ responseCode + "]";
	}

}