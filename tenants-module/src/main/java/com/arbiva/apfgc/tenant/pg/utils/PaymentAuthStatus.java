package com.arbiva.apfgc.tenant.pg.utils;

public enum PaymentAuthStatus {

	BILL_DEK_SUCCESS("0300","Success"),
	DIGIT_SECURE_SUCCESS("1000","Success"),
	BILL_DEK_ERRORMESSAGE("","Canceled By User"),
	DIGIT_SECURE_ERRORMESSGE("","Cancelled By Customer");
	
	private String code;
	private String desc;
	
	PaymentAuthStatus(String code, String description){
		this.code = code;
		this.desc= description;
	}
	
	public String getCode(){
		return code;
	}
	
	public String getDesc() {
		return desc;
	}

	public String desc(){
		return desc;
	}
}
