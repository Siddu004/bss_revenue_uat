package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

public class MspChnlsPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String mspcode;
	
	private String chnlcode;

	public String getMspcode() {
		return mspcode;
	}

	public void setMspcode(String mspcode) {
		this.mspcode = mspcode;
	}

	public String getChnlcode() {
		return chnlcode;
	}

	public void setChnlcode(String chnlcode) {
		this.chnlcode = chnlcode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mspcode == null) ? 0 : mspcode.hashCode());
		result = prime * result + ((chnlcode == null) ? 0 : chnlcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MspChnlsPK other = (MspChnlsPK) obj;
		if (mspcode == null) {
			if (other.mspcode != null)
				return false;
		} else if (!mspcode.equals(other.mspcode))
			return false;
		if (chnlcode == null) {
			if (other.chnlcode != null)
				return false;
		} else if (!chnlcode.equals(other.chnlcode))
			return false;
		return true;
	}
	

}
