/**
 * 
 *//*
package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tenant.dao.TenantLMOWalletDao;
import com.arbiva.apfgc.tenant.model.TenantLMOWallet;
import com.arbiva.apfgc.tenant.service.TenantLMOWalletService;

*//**
 * @author Arbiva
 *
 *//*
@Service
@Transactional
public class TenantLMOWalletServiceImpl implements TenantLMOWalletService {
	
	@Autowired
	TenantLMOWalletDao tenantLMOWalletDao;
	
	@Override
	public void deleteTenantLMOWallet(TenantLMOWallet tenantLMOWallet) {
		// TODO Auto-generated method stub
		tenantLMOWalletDao.delete(tenantLMOWallet);
		
	}

	@Override
	public List<TenantLMOWallet> findAllTenantLMOWallet() {
		// TODO Auto-generated method stub
		return tenantLMOWalletDao.findAll();
	}

	@Override
	public void saveTenantLMOWallet(TenantLMOWallet tenantLMOWallet) {
		// TODO Auto-generated method stub
		tenantLMOWalletDao.save(tenantLMOWallet);
		
	}

	@Override
	public TenantLMOWallet updateTenantLMOWallet(TenantLMOWallet tenantLMOWallet) {
		// TODO Auto-generated method stub
		return tenantLMOWalletDao.save(tenantLMOWallet);
	}

	@Override
	public TenantLMOWallet findByMspCode(String mspCode) {
		// TODO Auto-generated method stub
		return tenantLMOWalletDao.findByMspCode(mspCode);
	}

	@Override
	public TenantLMOWallet findByLmoCode(String lmoCode) {
		// TODO Auto-generated method stub
		return tenantLMOWalletDao.findByLmoCode(lmoCode);
	}

}
*/