package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.TenantDocuments;

@Repository
public class TenantDocumentsDaoImpl {
	
private static final Logger LOGGER = Logger.getLogger(TenantDocumentsDaoImpl.class);
		
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	public List<TenantDocuments> findByTenantCode(String tenantCode) {
		List<TenantDocuments> tenantDocuments = new ArrayList<TenantDocuments>();
		TypedQuery<TenantDocuments> query = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantDocuments.class.getSimpleName()).append(" WHERE tenantcode=:tenantCode");
		try {
			LOGGER.info("START::findByTenantDocumentsCode()");
			  query = getEntityManager().createQuery(builder.toString(), TenantDocuments.class);
			query.setParameter("tenantCode", tenantCode);
			tenantDocuments = query.getResultList();
			LOGGER.info("END::findByTenantDocumentsCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantDocumentsCode() " + e);
		}  finally {
			query = null;
			builder = null;
		}
		return tenantDocuments;
	}
	
	public void saveTenantDocument(TenantDocuments tenantDocuments) {
		try {
			getEntityManager().merge(tenantDocuments);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantDocumentsCode() " + e);
		} 
		
	}
}
