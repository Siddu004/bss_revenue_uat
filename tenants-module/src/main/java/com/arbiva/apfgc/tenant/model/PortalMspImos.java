package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "portal_msp_lmos")
public class PortalMspImos implements Serializable {
	
    private static final long serialVersionUID = 1L;
	
    public PortalMspImos() {
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "msplmoid")
	private Integer msplmoid;
	
	@Column(name = "msplmoids")
	private String msplmoids;
	
	@Column(name = "msp_enrollmentno")
	private String msp_enrollmentno;
	
	@Column(name = "lmo_name")
	private String lmo_name;
	
	@Column(name = "lmo_contactpPerson")
	private String lmo_contactpPerson;
	
	@Column(name = "lmo_phonenumber")
	private String lmo_phonenumber;
	
	@Column(name = "lmo_habitation")
	private String lmo_habitation;
	
	@Column(name = "lmo_street")
	private String lmo_street;
	
	@Column(name = "lmo_Village_Name_ID")
	private Integer lmo_Village_Name_ID;
	
	@Column(name = "lmo_Mandal_Name_ID")
	private Integer lmo_Mandal_Name_ID;
	
	@Column(name = "lmo_District_Name_ID")
	private Integer lmo_District_Name_ID;
	
	@Column(name = "lmo_State_ID")
	private Integer lmo_State_ID;
	
	@Column(name = "lmo_ZipCode")
	private String lmo_ZipCode;
	
	@Column(name = "lmo_srvc_yrs")
	private Integer lmo_srvc_yrs;
	
	@Column(name = "lmo_subscriber_cnt")
	private Integer lmo_subscriber_cnt;
	
	@Column(name = "lmo_conn_count")
	private Integer lmo_conn_count;
	
	@Column(name = "lmo_area")
	private String lmo_area;

	public Integer getMsplmoid() {
		return msplmoid;
	}

	public void setMsplmoid(Integer msplmoid) {
		this.msplmoid = msplmoid;
	}

	public String getMsplmoids() {
		return msplmoids;
	}

	public void setMsplmoids(String msplmoids) {
		this.msplmoids = msplmoids;
	}

	public String getMsp_enrollmentno() {
		return msp_enrollmentno;
	}

	public void setMsp_enrollmentno(String msp_enrollmentno) {
		this.msp_enrollmentno = msp_enrollmentno;
	}

	public String getLmo_name() {
		return lmo_name;
	}

	public void setLmo_name(String lmo_name) {
		this.lmo_name = lmo_name;
	}

	public String getLmo_contactpPerson() {
		return lmo_contactpPerson;
	}

	public void setLmo_contactpPerson(String lmo_contactpPerson) {
		this.lmo_contactpPerson = lmo_contactpPerson;
	}

	public String getLmo_phonenumber() {
		return lmo_phonenumber;
	}

	public void setLmo_phonenumber(String lmo_phonenumber) {
		this.lmo_phonenumber = lmo_phonenumber;
	}

	public String getLmo_habitation() {
		return lmo_habitation;
	}

	public void setLmo_habitation(String lmo_habitation) {
		this.lmo_habitation = lmo_habitation;
	}

	public String getLmo_street() {
		return lmo_street;
	}

	public void setLmo_street(String lmo_street) {
		this.lmo_street = lmo_street;
	}

	public Integer getLmo_Village_Name_ID() {
		return lmo_Village_Name_ID;
	}

	public void setLmo_Village_Name_ID(Integer lmo_Village_Name_ID) {
		this.lmo_Village_Name_ID = lmo_Village_Name_ID;
	}

	public Integer getLmo_Mandal_Name_ID() {
		return lmo_Mandal_Name_ID;
	}

	public void setLmo_Mandal_Name_ID(Integer lmo_Mandal_Name_ID) {
		this.lmo_Mandal_Name_ID = lmo_Mandal_Name_ID;
	}

	public Integer getLmo_District_Name_ID() {
		return lmo_District_Name_ID;
	}

	public void setLmo_District_Name_ID(Integer lmo_District_Name_ID) {
		this.lmo_District_Name_ID = lmo_District_Name_ID;
	}

	public Integer getLmo_State_ID() {
		return lmo_State_ID;
	}

	public void setLmo_State_ID(Integer lmo_State_ID) {
		this.lmo_State_ID = lmo_State_ID;
	}

	public String getLmo_ZipCode() {
		return lmo_ZipCode;
	}

	public void setLmo_ZipCode(String lmo_ZipCode) {
		this.lmo_ZipCode = lmo_ZipCode;
	}

	public Integer getLmo_srvc_yrs() {
		return lmo_srvc_yrs;
	}

	public void setLmo_srvc_yrs(Integer lmo_srvc_yrs) {
		this.lmo_srvc_yrs = lmo_srvc_yrs;
	}

	public Integer getLmo_subscriber_cnt() {
		return lmo_subscriber_cnt;
	}

	public void setLmo_subscriber_cnt(Integer lmo_subscriber_cnt) {
		this.lmo_subscriber_cnt = lmo_subscriber_cnt;
	}

	public Integer getLmo_conn_count() {
		return lmo_conn_count;
	}

	public void setLmo_conn_count(Integer lmo_conn_count) {
		this.lmo_conn_count = lmo_conn_count;
	}

	public String getLmo_area() {
		return lmo_area;
	}

	public void setLmo_area(String lmo_area) {
		this.lmo_area = lmo_area;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
