/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;

*//**
 * @author Arbiva
 *
 *//*
public class TenantLmoMspAgreementPK implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	
	private String mspCode;
	
	private String lmoCode;
	
	private Date agrFDate;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agrFDate == null) ? 0 : agrFDate.hashCode());
		result = prime * result + ((lmoCode == null) ? 0 : lmoCode.hashCode());
		result = prime * result + ((mspCode == null) ? 0 : mspCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantLmoMspAgreementPK other = (TenantLmoMspAgreementPK) obj;
		if (agrFDate == null) {
			if (other.agrFDate != null)
				return false;
		} else if (!agrFDate.equals(other.agrFDate))
			return false;
		if (lmoCode == null) {
			if (other.lmoCode != null)
				return false;
		} else if (!lmoCode.equals(other.lmoCode))
			return false;
		if (mspCode == null) {
			if (other.mspCode != null)
				return false;
		} else if (!mspCode.equals(other.mspCode))
			return false;
		return true;
	}
	
	

}
*/