/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantLicenses;

public interface TenantLicensesDao extends JpaSpecificationExecutor<TenantLicenses>, JpaRepository<TenantLicenses, String>{

	@Query(value="SELECT * FROM tenantlicenses where tenantcode = ?1 ",nativeQuery=true)
	public abstract TenantLicenses findByTenantCode(String tenantCode);
}
*/