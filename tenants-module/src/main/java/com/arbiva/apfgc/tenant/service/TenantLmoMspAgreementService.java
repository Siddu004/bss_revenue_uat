/**
 * 
 *//*
package com.arbiva.apfgc.tenant.service;

import java.text.ParseException;
import java.util.List;

import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreement;
import com.arbiva.apfgc.tenant.vo.TenantLmoMspAgreementVO;

*//**
 * @author Arbiva
 *
 *//*
public interface TenantLmoMspAgreementService {
	
	public abstract void deleteTenantLmoMspAgreement(TenantLmoMspAgreement tenantLmoMspAgreement);
	
	public abstract TenantLmoMspAgreement findByMspCode(String mspCode);
	
	public abstract TenantLmoMspAgreement findByLmoCode(String lmoCode);

	public abstract List<TenantLmoMspAgreement> findAllTenantLmoMspAgreement();

	public abstract void saveTenantLmoMspAgreement(TenantLmoMspAgreement tenantLmoMspAgreement);

	public abstract TenantLmoMspAgreement updateTenantLmoMspAgreement(TenantLmoMspAgreement tenantLmoMspAgreement);
	
//	public abstract void createTenantLmoMspAgreement(TenantLmoMspAgreementVO tenantLmoMspAgreementVO, String action) throws ParseException;
	
	
	public abstract void createTenantLmoMspAgreement(TenantLmoMspAgreementVO tenantLmoMspAgreementVO, String mspCode, String remoteIPAddr);
	

}
*/