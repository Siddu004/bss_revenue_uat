package com.arbiva.apfgc.tenant.model;

import java.util.List;

public class CorpusResponse {
	
	private CorpusResponceStatus responseStatus;
	
	private List<IPTVSrvcs> services;
	
	

	public CorpusResponceStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(CorpusResponceStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public List<IPTVSrvcs> getServices() {
		return services;
	}

	public void setServices(List<IPTVSrvcs> services) {
		this.services = services;
	}
	

}
