package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.Lovs;
import com.arbiva.apfgc.tenant.model.PortalAreas;

@Repository
public class PortalAreaDaoImpl {
	
private static final Logger LOGGER = Logger.getLogger(PortalAssetsDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	/*public List<PortalAreas> findByEnrollmentno(String Enrollmentno) {
		List<PortalAreas> portalAreas = new ArrayList<PortalAreas>();
		StringBuilder builder = new StringBuilder(" FROM ").append(PortalAreas.class.getSimpleName())
				.append(" WHERE Enrollmentno=:enrollmentno");
		try {
			LOGGER.info("START::findByEnrollmentno()");
			TypedQuery<PortalAreas> query = getEntityManager().createQuery(builder.toString(),PortalAreas.class);
			query.setParameter("enrollmentno", Enrollmentno);
			portalAreas = query.getResultList();
			LOGGER.info("END::findByEnrollmentno()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByEnrollmentno() " + e);
		}
		return portalAreas;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findByEnrollmentno(String Enrollmentno) {
		List<Object[]> areaList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		 Query query = null;
			try {
				 LOGGER.info("PortalAreaDaoImpl :: findByEnrollmentno() :: SATRT");
				 builder.append("select areaname,(select cabletypename from cabletypes where cabletypeid = pa.cabletypeid) cabletype, runningcablelen, "+
					" (select statename from states where stateid = pa.stateid) state, "+
					" (select districtname from districts where districtuid = pa.districtid) district, "+
					" (select mandalname from mandals where mandalslno = pa.mandalid and districtuid = pa.districtid and stateid = pa.stateid) mandal, "+
					" (select villagename from villages where villageuid = pa.villageid) village, "+
					" subscription_cnt,conn_cnt,digconn_cnt,anlconn_cnt "+
					" from portal_areas pa "+
					" where enrollmentno = :Enrollmentno ");
			  query = em.createNativeQuery(builder.toString());	
			query.setParameter("Enrollmentno", Enrollmentno);
			areaList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("PortalAreaDaoImpl::findByEnrollmentno() " + ex);
		}finally {
			query = null;
			builder = null;
		}
		return areaList;
	}
	
	public void savePortalAreas(PortalAreas portalAreas) {
		try{
			 LOGGER.info("PortalAreaDaoImpl :: savePortalAreas() :: SATRT");
		 	 getEntityManager().merge(portalAreas);
		} catch(Exception e)
		{
			 LOGGER.info("PortalAreaDaoImpl :: savePortalAreas() :"+e);
			 e.printStackTrace();
		}
	}
	
	public List<PortalAreas> findAllPortalAreas() {
		List<PortalAreas> portalAreas = new ArrayList<PortalAreas>();
		StringBuilder builder = new StringBuilder();
		TypedQuery<PortalAreas> query = null;
			try {
				 LOGGER.info("PortalAreaDaoImpl :: findByEnrollmentno() :: SATRT");
		     builder.append(" FROM ").append(PortalAreas.class.getSimpleName());
			query = getEntityManager().createQuery(builder.toString(), PortalAreas.class);
			portalAreas = query.getResultList();
			LOGGER.info("END::findAllPortalAreas()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllPortalAreas() " + e);
		}finally {
			query = null;
			builder = null;
		}
		return portalAreas;
	}
	
}


