package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="tenantcpedlv")
public class TenantCpeDlv {
	
	@Id 
	@Column(name = "dlvId")
	private Long dlvId;
	
	@Column(name = "dlvdate")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date dlvDate;
	
	@Column(name = "dlvqty")
	private Long dlvQty;
	
	@Column(name = "dmndid")
	private Long dmndId;
	
	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    @NotNull
    private Short status;

	@Column(name = "createdon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar createdon;

	@Column(name = "createdby", length = 50)
    private String createdby;

	@Column(name = "createdipaddr", length = 20)
    private String createdipaddr;

	@Column(name = "modifiedon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar modifiedon;

	@Column(name = "modifiedby", length = 50)
    private String modifiedby;

	@Column(name = "modifiedipaddr", length = 20)
    private String modifiedipaddr;
	
	@ManyToOne
	@JoinColumn(name = "dmndid", referencedColumnName = "dmndid", nullable = false, insertable = false, updatable = false)
	private TenantCpeDemand tenantCpeDemand;
	

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Calendar getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Calendar modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedipaddr() {
		return modifiedipaddr;
	}

	public void setModifiedipaddr(String modifiedipaddr) {
		this.modifiedipaddr = modifiedipaddr;
	}

	public Long getDlvId() {
		return dlvId;
	}

	public void setDlvId(Long dlvId) {
		this.dlvId = dlvId;
	}

	public Date getDlvDate() {
		return dlvDate;
	}

	public void setDlvDate(Date dlvDate) {
		this.dlvDate = dlvDate;
	}

	public Long getDlvQty() {
		return dlvQty;
	}

	public void setDlvQty(Long dlvQty) {
		this.dlvQty = dlvQty;
	}

	public Long getDmndId() {
		return dmndId;
	}

	public void setDmndId(Long dmndId) {
		this.dmndId = dmndId;
	}

	public TenantCpeDemand getTenantCpeDemand() {
		return tenantCpeDemand;
	}

	public void setTenantCpeDemand(TenantCpeDemand tenantCpeDemand) {
		this.tenantCpeDemand = tenantCpeDemand;
	}

}
