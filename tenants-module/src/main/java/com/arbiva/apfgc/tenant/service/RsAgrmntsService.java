package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.vo.ProductAgreementVO;

public interface RsAgrmntsService {

	String saveProductAgreement(List<ProductAgreementVO> productAgreementVO);

 List<ProductAgreementVO> viewAllPkgAgree(String tenantcode, String tenantType);
}
