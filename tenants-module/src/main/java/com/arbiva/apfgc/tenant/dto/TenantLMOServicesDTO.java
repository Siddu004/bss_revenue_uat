package com.arbiva.apfgc.tenant.dto;

import java.util.Calendar;
import java.util.Date;

import com.arbiva.apfgc.tenant.model.Region;

public class TenantLMOServicesDTO {

	
	public TenantLMOServicesDTO(){
		
	}
	private String mspCode;
	
	private String lmoCode;
	
	private Date agrFDate;
	
	private String coresrvcCode;
	
	private Region region;
	
	private Integer status;
	
	private Calendar createdDate;
	
	private String createdBy;
	
	private String cratedIPAddress;
	
	private Calendar modifiedDate;
	
	private String modifiedBy;
	
	private String modifiedIPAddress;
	
	private String deactivatedOn;
	
	private String deactivatedBy;
	
	private String deactivatedIpaddr;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public String getCoresrvcCode() {
		return coresrvcCode;
	}

	public void setCoresrvcCode(String coresrvcCode) {
		this.coresrvcCode = coresrvcCode;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpaddr() {
		return deactivatedIpaddr;
	}

	public void setDeactivatedIpaddr(String deactivatedIpaddr) {
		this.deactivatedIpaddr = deactivatedIpaddr;
	}
	
	
}
