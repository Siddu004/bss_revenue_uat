package com.arbiva.apfgc.tenant.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.dto.CpeOrderSaveDTO;
import com.arbiva.apfgc.tenant.dto.CpeTypeMobileDTO;
import com.arbiva.apfgc.tenant.dto.TmsHelperDTO;
import com.arbiva.apfgc.tenant.service.CpeService;
import com.arbiva.apfgc.tenant.serviceImpl.TenantServiceImpl;
import com.arbiva.apfgc.tenant.vo.CpeStockVO;

@RestController
public class CpeRestController {
	
	
	private static final Logger logger = Logger.getLogger(CpeRestController.class);
	
	@Autowired
	CpeService cpeService;
	
	@Autowired
	TenantServiceImpl tenantService;
	
	@RequestMapping(value = "/createCpeOrder", method = RequestMethod.GET)
	public List<CpeHelperDTO> createCpeOrder() {
		List<CpeHelperDTO> cpeMaderList = new ArrayList<>();
		try {
			logger.info("CpeRestController:: createCpeOrder() :: START ");
			cpeMaderList = cpeService.findAllCpeMasterList();
			logger.info("CpeRestController:: createCpeOrder() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController::createCpeOrder() " + e);
			e.printStackTrace();
		} finally {}
		return cpeMaderList;
	}
	
	
	
	@RequestMapping(value = "/getAllCpeModelByCpeType", method = RequestMethod.GET)
	public List<CpeHelperDTO> getAllCpeModelByCpeType(@RequestParam(value="cpeType") String cpeType) {
		List<CpeHelperDTO> cpeMasterList = new ArrayList<>();
		try {
			logger.info("CpeRestController:: getAllCpeModelByCpeType() :: START ");
			cpeMasterList = cpeService.findAllCpeModelByCpeType(cpeType);
			logger.info("CpeRestController:: getAllCpeModelByCpeType() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController::getAllCpeModelByCpeType() " + e);
			e.printStackTrace();
		} finally {}
		return cpeMasterList;
	}
	
	@RequestMapping(value = "/getCpeChargesByProfileId", method = RequestMethod.GET)
	@ResponseBody
	public CpeHelperDTO getCpeChargesByProfileId(@RequestParam(value="profileId") String profileId) {
		CpeHelperDTO cpeHelperDTO = new CpeHelperDTO();
		try {
			logger.info("CpeRestController:: getCpeChargesByProfileId() :: START ");
			cpeHelperDTO = cpeService.getCpeChargesByProfileId(profileId);
			logger.info("CpeRestController:: getCpeChargesByProfileId() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController::getCpeChargesByProfileId() " + e);
			e.printStackTrace();
		} finally {}
		return cpeHelperDTO;
	}
	
	@RequestMapping(value = "/saveCpeOrders", method = RequestMethod.POST)
	@ResponseBody
	public String saveCpeOrders(@RequestBody CpeOrderSaveDTO cpeOrderSaveDTO) {
		String responce = "";
		try {
			logger.info("CpeRestController:: saveCpeOrders() :: START ");
			responce = cpeService.saveCpeOrders(cpeOrderSaveDTO);
			logger.info("CpeRestController:: saveCpeOrders() :: END ");
		} catch (Exception e) {
			responce = " <label style='color:#ff0000'> Failed To Create Order.... </label>";
			logger.error("CpeController::saveCpeOrders() " + e);
			e.printStackTrace();
		} finally {}
		return responce;
	}
	
	@RequestMapping(value = "/viewApsflCpeOrder", method = RequestMethod.GET)
	public List<CpeHelperDTO> viewCpeOrder(Model model, @RequestParam(value="tenantCode") String tenantCode
											, @RequestParam(value="tenantType") String tenantType) {
		List<CpeHelperDTO> cpeMasterList = new ArrayList<>();
		try {
			logger.info("CpeRestController:: viewCpeOrder() :: START ");
			cpeMasterList = cpeService.findAllCpeOrdersList(tenantCode,tenantType);
			logger.info("CpeRestController:: viewCpeOrder() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController::viewCpeOrder() " + e);
			e.printStackTrace();
		} finally {}
		return cpeMasterList;
	}
	
	@RequestMapping(value = "/allocateCpe", method = RequestMethod.GET)
	public CpeHelperDTO allocateCpe(@RequestParam(value="demandId") String demandId) {
	CpeHelperDTO cpeHelperDTO = new CpeHelperDTO();
		try {
			logger.info("CpeRestController:: allocateCpe() :: START ");
			cpeHelperDTO = cpeService.allocateCpe(demandId);
			logger.info("CpeRestController:: allocateCpe() :: END ");
		} catch (Exception e) {
			logger.error("CpeController::allocateCpe() " + e);
			e.printStackTrace();
		} finally {}
		return cpeHelperDTO;
	}
	
	
	@RequestMapping(value = "/saveCpeAllocation",  method = RequestMethod.POST)
	public String saveCpeAllocation(@RequestBody CpeHelperDTO cpeHelperDTO){
		String result="";
		try {
			logger.info("CpeRestController:: saveCpeAllocation() :: START ");
			result = cpeService.saveCpeAllocation(cpeHelperDTO);
			logger.info("CpeRestController:: saveCpeAllocation() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController:: saveCpeAllocation() :: START ");
			e.printStackTrace();
			result = "Failed To Allocate CPE";
		} finally {}
		return result;
	}
	
	@RequestMapping(value = "/cpePayment",  method = RequestMethod.POST)
	public String cpePayment(@RequestBody CpeHelperDTO cpeHelperDTO){
		String result="";
		try {
			logger.info("CpeRestController:: cpePayment() :: START ");
			result = cpeService.cpePayment(cpeHelperDTO);
			logger.info("CpeRestController:: cpePayment() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController:: cpePayment()  "+e);
			e.printStackTrace();
			result = "Failed To Make Payment";
		} finally {}
		return result;
	}
	
	@RequestMapping(value = "/getCpeNosByDlvId", method = RequestMethod.GET)
	public List<String> getCpeNosByDlvId(Model model, @RequestParam(value = "dlvId") String dlvId) {
		List<String> responce = new ArrayList<>();
		try {
			logger.info("CpeRestController:: getCpeNosByDlvId() :: START ");
			responce = cpeService.getAllCpeSnosByDlvId(dlvId);
			logger.info("CpeRestController:: getCpeNosByDlvId() :: END ");
		} catch (Exception e) {
			logger.error("CpeController::getCpeNosByDlvId() " + e);
			e.printStackTrace();
		} finally {}
		return responce;
	}
	
	
	@RequestMapping(value = "/getCpeModelDetails", method = RequestMethod.GET)
	public List<CpeTypeMobileDTO> getCpeModelDetails() {
		List<CpeTypeMobileDTO> cpeModelsList = null ;
		try {
			logger.info("CpeRestController:: getCpeModelDetails() :: START ");
			cpeModelsList = cpeService.getCpeModelDetails();
			logger.info("CpeRestController:: getCpeModelDetails() :: END ");
		} catch (Exception e) {
			logger.error("CpeRestController::getCpeModelDetails() " + e);
			e.printStackTrace();
		} finally {}
		return cpeModelsList;
	}
	
	@RequestMapping(value = "/uploadCpeStockFile", method = RequestMethod.POST)
	public TmsHelperDTO uploadCpeStockFile(@RequestBody CpeHelperDTO cpehelperDto, Model model,HttpServletRequest request) {
		CpeHelperDTO cpeHelper = new CpeHelperDTO();
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		List<CpeStockVO> cpeErrorOrDuplicateList = new ArrayList<>();
		
		try{
			logger.info("CpeRestController:: uploadCpeStockFile() :: START ");
			
			cpeHelper = cpeService.saveList(cpehelperDto);
			
			if (!cpeHelper.getCpeSlnoList().isEmpty()){
			cpeErrorOrDuplicateList=tenantService.getCPEStockDetails("","","","","","",cpeHelper.getCpeSlnoList());
			
			}
			tmsHelperDTO.setCpeStockList(cpeErrorOrDuplicateList);
			tmsHelperDTO.setStatus(cpeHelper.getStatus());
			logger.info("CpeRestController:: uploadCpeStockFile() :: END ");
		}catch(Exception ex){
			cpeHelper.setStatus(ex.getMessage());
			logger.error("CpeRestController::uploadCpeStockFile() " + ex);
		}
		finally {}
	 return tmsHelperDTO;
	}

	
	@RequestMapping(value = "/searchUploadedcpeDetails", method = RequestMethod.POST)
	public TmsHelperDTO searchUploadCpeDetails( 
			@RequestBody CpeHelperDTO cpeHelperDto
			 ) {
		TmsHelperDTO tmsHelperDTO = new TmsHelperDTO();
		List<CpeStockVO> cpeList = new ArrayList<>();
		
		try {
			logger.info("CpeRestController :: searchUploadCpeDetails :: START");
			cpeList = tenantService.getCPEStockDetails("","","","","","",cpeHelperDto.getCpeSlnoList());
			tmsHelperDTO.setCpeStockList(cpeList);
			logger.info("CpeRestController :: searchUploadCpeDetails :: END");
		} catch (Exception e) {
			logger.error("CpeRestController::searchUploadCpeDetails " + e);
			e.printStackTrace();
		} finally {
			cpeList = null;
		}
		return tmsHelperDTO;
	}
	
}
