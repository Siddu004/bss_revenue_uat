package com.arbiva.apfgc.tenant.pg.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author srinivasa
 * 
 */
@MappedSuperclass
public abstract class Base implements Serializable {

	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Transient
	@JsonIgnore
	public abstract boolean isNew();
	// @Transient
	// @JsonIgnore
	//// public boolean isNew() {
	//// return (this.createdDate == null);
	//// }

	@PrePersist
	@PreUpdate
	void createAndUpdateAtCallback() {
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar now = Calendar.getInstance(tz);
		setUpdatedDate(now.getTime());
		if (isNew()) {
			setCreatedDate(now.getTime());
		}
	}
}
