/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.Region;

public interface RegionDao extends JpaSpecificationExecutor<Region>, JpaRepository<Region, String> {

	@Query(value="select * from regions where regionname=?1", nativeQuery=true)
	public abstract Region getRegionTypeByRegionName(String regionName);
}
*/