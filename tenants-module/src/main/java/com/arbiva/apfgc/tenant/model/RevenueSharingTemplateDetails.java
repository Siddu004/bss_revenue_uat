package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsDTO;
import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsListDTO;

@Entity
@Table(name = "rstmpldtl")
@IdClass(RsTemplateDetailsPK.class)
public class RevenueSharingTemplateDetails implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public RevenueSharingTemplateDetails ()
	{
		
	}
	
	public RevenueSharingTemplateDetails(RsTemplateDetailsDTO rsTempDtls,RsTemplateDetailsListDTO rsTempDtlsList)
	{
		this.rstmplCode = rsTempDtls.getRstmplCode();
		this.regioncode = rsTempDtls.getRegioncode();
		this.tenantslno = Integer.parseInt(rsTempDtlsList.getTenantslno());
		this.tenanttype = rsTempDtlsList.getTenanttype();
		this.revshareperc = new BigDecimal(rsTempDtlsList.getRevshareperc());
		this.effectivefrom = new Date();
		this.status = 1;
	}

	@Id
	@Column(name = "rstmplcode", unique = true, nullable = false)
	private String rstmplCode;
	
	@Id
	@Column(name = "tenantslno")
	private int tenantslno;
	
	@Column(name = "tenanttype")
	private String tenanttype;
	
	@Id
	@Column(name = "regioncode")
	private String regioncode;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectivefrom;
	
	@Column(name = "revshare_perc")
	private BigDecimal revshareperc;
	
	@Column(name = "status")
	private int status;
	
	@ManyToOne
    @JoinColumn(name = "rstmplcode", referencedColumnName = "rstmplcode", nullable = false, insertable = false, updatable = false)
	private RevenueSharingTemplateMaster revenueSharingTemplateMaster;

	
	public String getRstmplCode() {
		return rstmplCode;
	}

	public void setRstmplCode(String rstmplCode) {
		this.rstmplCode = rstmplCode;
	}

	public int getTenantslno() {
		return tenantslno;
	}

	public void setTenantslno(int tenantslno) {
		this.tenantslno = tenantslno;
	}

	public String getTenanttype() {
		return tenanttype;
	}

	public void setTenanttype(String tenanttype) {
		this.tenanttype = tenanttype;
	}

	public String getRegioncode() {
		return regioncode;
	}

	public void setRegioncode(String regioncode) {
		this.regioncode = regioncode;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	public BigDecimal getRevshareperc() {
		return revshareperc;
	}

	public void setRevshareperc(BigDecimal revshareperc) {
		this.revshareperc = revshareperc;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public RevenueSharingTemplateMaster getRevenueSharingTemplateMaster() {
		return revenueSharingTemplateMaster;
	}

	public void setRevenueSharingTemplateMaster(RevenueSharingTemplateMaster revenueSharingTemplateMaster) {
		this.revenueSharingTemplateMaster = revenueSharingTemplateMaster;
	}
}