package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

public class RsAgrPartnersPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long agrUniqueId;

	private String partnerType;

	private int partnerSlNo;

	public Long getAgrUniqueId() {
		return agrUniqueId;
	}

	public void setAgrUniqueId(Long agrUniqueId) {
		this.agrUniqueId = agrUniqueId;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public int getPartnerSlNo() {
		return partnerSlNo;
	}

	public void setPartnerSlNo(int partnerSlNo) {
		this.partnerSlNo = partnerSlNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agrUniqueId == null) ? 0 : agrUniqueId.hashCode());
		result = prime * result + partnerSlNo;
		result = prime * result + ((partnerType == null) ? 0 : partnerType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RsAgrPartnersPK other = (RsAgrPartnersPK) obj;
		if (agrUniqueId == null) {
			if (other.agrUniqueId != null)
				return false;
		} else if (!agrUniqueId.equals(other.agrUniqueId))
			return false;
		if (partnerSlNo != other.partnerSlNo)
			return false;
		if (partnerType == null) {
			if (other.partnerType != null)
				return false;
		} else if (!partnerType.equals(other.partnerType))
			return false;
		return true;
	}

	
}
