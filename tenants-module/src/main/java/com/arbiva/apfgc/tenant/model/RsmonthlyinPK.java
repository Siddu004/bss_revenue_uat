package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

public class RsmonthlyinPK implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String tenantcode;

	private String formon;

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getFormon() {
		return formon;
	}

	public void setFormon(String formon) {
		this.formon = formon;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tenantcode == null) ? 0 : tenantcode.hashCode());
		result = prime * result + ((formon == null) ? 0 : formon.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RsmonthlyinPK other = (RsmonthlyinPK) obj;
		
		if (tenantcode == null) {
			if (other.tenantcode != null)
				return false;
		} else if (!tenantcode.equals(other.tenantcode))
			return false;
		
		if (formon == null) {
			if (other.formon != null)
				return false;
		} else if (!formon.equals(other.formon))
			return false;
		
		return true;
	}


}
