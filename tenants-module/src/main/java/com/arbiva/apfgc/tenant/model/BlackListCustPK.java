package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author kiran
 *
 */
public class BlackListCustPK implements Serializable {

	private static final long serialVersionUID = 1L;

	private String stbcafno;

	private Date effectivefrom;

	public String getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(String stbcafno) {
		this.stbcafno = stbcafno;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectivefrom == null) ? 0 : effectivefrom.hashCode());
		result = prime * result + ((stbcafno == null) ? 0 : stbcafno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlackListCustPK other = (BlackListCustPK) obj;
		if (effectivefrom == null) {
			if (other.effectivefrom != null)
				return false;
		} else if (!effectivefrom.equals(other.effectivefrom))
			return false;
		if (stbcafno == null) {
			if (other.stbcafno != null)
				return false;
		} else if (!stbcafno.equals(other.stbcafno))
			return false;
		return true;
	}

	
}
