package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.arbiva.apfgc.tenant.model.MspChnlsStgPK;
import com.arbiva.apfgc.tenant.model.UploadHistory;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name="mspchnlsstg")
@IdClass(MspChnlsStgPK.class)
public class MspChnlsStg {
	
	@Id
	@Column(name = "uploadid")
	private Long uploadid;
	
	@Id
	@Column(name = "uploadrecno")
	private Long uploadrecno;
	
	@Column(name = "mspcode")
	private String mspcode;
	
	@Column(name = "chnlcode")
	private String chnlcode;
	
	@Column(name = "chnlname")
	private String chnlname;
	
	@Column(name = "pkgname")
	private String pkgname;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "upldstatus")
	private String upldstatus;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "MM")
	private Calendar createdon;
	
	@Column(name = "createdby")
	private String createdby;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uploadid", referencedColumnName = "uploadid", nullable = false, insertable=false, updatable=false)
	@JsonIgnore
	private UploadHistory uploadHistory;

	public UploadHistory getUploadHistory() {
		return uploadHistory;
	}

	public void setUploadHistory(UploadHistory uploadHistory) {
		this.uploadHistory = uploadHistory;
	}

	public Long getUploadid() {
		return uploadid;
	}

	public void setUploadid(Long uploadid) {
		this.uploadid = uploadid;
	}

	public Long getUploadrecno() {
		return uploadrecno;
	}

	public void setUploadrecno(Long uploadrecno) {
		this.uploadrecno = uploadrecno;
	}

	public String getMspcode() {
		return mspcode;
	}

	public void setMspcode(String mspcode) {
		this.mspcode = mspcode;
	}

	public String getChnlcode() {
		return chnlcode;
	}

	public void setChnlcode(String chnlcode) {
		this.chnlcode = chnlcode;
	}

	public String getChnlname() {
		return chnlname;
	}

	public void setChnlname(String chnlname) {
		this.chnlname = chnlname;
	}

	public String getPkgname() {
		return pkgname;
	}

	public void setPkgname(String pkgname) {
		this.pkgname = pkgname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpldstatus() {
		return upldstatus;
	}

	public void setUpldstatus(String upldstatus) {
		this.upldstatus = upldstatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	
	

}


