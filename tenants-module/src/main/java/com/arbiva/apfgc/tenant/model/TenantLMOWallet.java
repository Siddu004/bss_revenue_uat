/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

*//**
 * @author Arbiva
 *
 *//*
@Entity
@Table(name="lmowallet", schema="apfgc")
@IdClass(TenantLMOWalletPK.class)
public class TenantLMOWallet implements Serializable {
	
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mspcode")
	private String mspCode;
	
	@Id
	@Column(name="lmocode")
	private String lmoCode;
	
	@Column(name = "deposit_amt")
	private Float depositAmount;
	
	@Column(name = "walletamt")
	private Float walletAmount;
	
	@Column(name = "deposit_mode")
	private String depositMode;
	
	@Column(name = "deposit_refno")
	private String depositRefno;
	
	@Column(name = "deposit_status")
	private Integer depositStatus;
	
	@Column(name = "wallet_lastdbamt")
	private Float walletLastdbamt;
	
	@Column(name = "wallet_lastdbdate")
	private Calendar walletLastdbdate;
	
	@Column(name = "wallet_lastdbmode")
	private String walletLastdbmode;
	
	@Column(name = "wallet_lastdbid")
	private String walletLastdbid;
	
	@Column(name = "wallet_lastcramt")
	private Float walletLastcramt;
	
	@Column(name = "wallet_lastcrdate")
	private Calendar walletLastcrdate;
	
	@Column(name = "wallet_lastcrmode")
	private String walletLastcrmode;
	
	@Column(name = "wallet_lastcrid")
	private String walletLastcrid;
	
	@Column(name = "depost_lastdbamt")
	private Float depostLastdbamt;
	
	@Column(name = "depost_lastdbdate")
	private Calendar depostLastdbdate;
	
	@Column(name = "depost_lastdbmode")
	private String depostLastdbmode;
	
	@Column(name = "depost_lastdbid")
	private String depostLastdbid;
	
	@Column(name = "depost_lastcramt")
	private Float depostLastcramt;
	
	@Column(name = "depost_lastcrdate")
	private Calendar depostLastcrdate;
	
	@Column(name = "depost_lastcrmode")
	private String depostLastcrmode;
	
	@Column(name = "depost_lastcrid")
	private String depostLastcrid;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private Integer modifiedBy;
	
	@Column(name = "modifiedipaddr")
	private String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private String deactivatedOn;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpaddr;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumns({@JoinColumn(name="mspcode",insertable=false,updatable=false),@JoinColumn(name="lmocode", insertable=false,updatable=false)})
	private TenantLmoMspAgreement tenantLmoMspAgreementWallet;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Float getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Float depositAmount) {
		this.depositAmount = depositAmount;
	}

	public Float getWalletAmount() {
		return walletAmount;
	}

	public void setWalletAmount(Float walletAmount) {
		this.walletAmount = walletAmount;
	}

	public String getDepositMode() {
		return depositMode;
	}

	public void setDepositMode(String depositMode) {
		this.depositMode = depositMode;
	}

	public String getDepositRefno() {
		return depositRefno;
	}

	public void setDepositRefno(String depositRefno) {
		this.depositRefno = depositRefno;
	}

	public Integer getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}

	public Float getWalletLastdbamt() {
		return walletLastdbamt;
	}

	public void setWalletLastdbamt(Float walletLastdbamt) {
		this.walletLastdbamt = walletLastdbamt;
	}

	public Calendar getWalletLastdbdate() {
		return walletLastdbdate;
	}

	public void setWalletLastdbdate(Calendar walletLastdbdate) {
		this.walletLastdbdate = walletLastdbdate;
	}

	public String getWalletLastdbmode() {
		return walletLastdbmode;
	}

	public void setWalletLastdbmode(String walletLastdbmode) {
		this.walletLastdbmode = walletLastdbmode;
	}

	public String getWalletLastdbid() {
		return walletLastdbid;
	}

	public void setWalletLastdbid(String walletLastdbid) {
		this.walletLastdbid = walletLastdbid;
	}

	public Float getWalletLastcramt() {
		return walletLastcramt;
	}

	public void setWalletLastcramt(Float walletLastcramt) {
		this.walletLastcramt = walletLastcramt;
	}

	public Calendar getWalletLastcrdate() {
		return walletLastcrdate;
	}

	public void setWalletLastcrdate(Calendar walletLastcrdate) {
		this.walletLastcrdate = walletLastcrdate;
	}

	public String getWalletLastcrmode() {
		return walletLastcrmode;
	}

	public void setWalletLastcrmode(String walletLastcrmode) {
		this.walletLastcrmode = walletLastcrmode;
	}

	public String getWalletLastcrid() {
		return walletLastcrid;
	}

	public void setWalletLastcrid(String walletLastcrid) {
		this.walletLastcrid = walletLastcrid;
	}

	public Float getDepostLastdbamt() {
		return depostLastdbamt;
	}

	public void setDepostLastdbamt(Float depostLastdbamt) {
		this.depostLastdbamt = depostLastdbamt;
	}

	public Calendar getDepostLastdbdate() {
		return depostLastdbdate;
	}

	public void setDepostLastdbdate(Calendar depostLastdbdate) {
		this.depostLastdbdate = depostLastdbdate;
	}

	public String getDepostLastdbmode() {
		return depostLastdbmode;
	}

	public void setDepostLastdbmode(String depostLastdbmode) {
		this.depostLastdbmode = depostLastdbmode;
	}

	public String getDepostLastdbid() {
		return depostLastdbid;
	}

	public void setDepostLastdbid(String depostLastdbid) {
		this.depostLastdbid = depostLastdbid;
	}

	public Float getDepostLastcramt() {
		return depostLastcramt;
	}

	public void setDepostLastcramt(Float depostLastcramt) {
		this.depostLastcramt = depostLastcramt;
	}

	public Calendar getDepostLastcrdate() {
		return depostLastcrdate;
	}

	public void setDepostLastcrdate(Calendar depostLastcrdate) {
		this.depostLastcrdate = depostLastcrdate;
	}

	public String getDepostLastcrmode() {
		return depostLastcrmode;
	}

	public void setDepostLastcrmode(String depostLastcrmode) {
		this.depostLastcrmode = depostLastcrmode;
	}

	public String getDepostLastcrid() {
		return depostLastcrid;
	}

	public void setDepostLastcrid(String depostLastcrid) {
		this.depostLastcrid = depostLastcrid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpaddr() {
		return deactivatedIpaddr;
	}

	public void setDeactivatedIpaddr(String deactivatedIpaddr) {
		this.deactivatedIpaddr = deactivatedIpaddr;
	}

	public TenantLmoMspAgreement getTenantLmoMspAgreementWallet() {
		return tenantLmoMspAgreementWallet;
	}

	public void setTenantLmoMspAgreementWallet(TenantLmoMspAgreement tenantLmoMspAgreementWallet) {
		this.tenantLmoMspAgreementWallet = tenantLmoMspAgreementWallet;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
*/