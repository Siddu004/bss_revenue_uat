package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.dao.CpeCharAgmtDao;
import com.arbiva.apfgc.tenant.dao.CpeDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.dto.CpeCharHelperDTO;
import com.arbiva.apfgc.tenant.model.ChargesAgmt;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;
import com.arbiva.apfgc.tenant.model.RsAgrPartners;
import com.arbiva.apfgc.tenant.service.CpeCharAgmtService;
import com.arbiva.apfgc.tenant.vo.ProductAgreementPartnersVO;

@Service
public class CpeCharAgmtServiceImpl implements CpeCharAgmtService {
	
	private static final Logger LOGGER = Logger.getLogger(CpeCharAgmtServiceImpl.class);
	
	@Autowired
	CpeCharAgmtDao cpeCharAgmtDao;
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CpeDao cpeDao;

	@Override
	public List<RevenueSharingTemplateMaster> getAllCpeTempl() {
		return cpeCharAgmtDao.getAllCpeTempl();
	}

	@Override
	public List<CpeCharHelperDTO> getChargeCodesByTemplate(String tmplCode) {
		
		List<String> list =cpeCharAgmtDao.getChargeCodesByTemplate(tmplCode);
		List<CpeCharHelperDTO> returnList = new ArrayList<>();
		try {
		for(String chargeName : list){
			CpeCharHelperDTO cpeCharHelperDTO = new CpeCharHelperDTO();
			cpeCharHelperDTO.setChargeName(chargeName);
			returnList.add(cpeCharHelperDTO);
		}
		} catch(Exception e) {
			e.printStackTrace();
		} finally { 
			list = null;
		}
		return returnList;
	}

	@Override
	@Transactional
	public String save(CpeCharHelperDTO cpeCharHelperDTO) {
		
		String returnValue = "";
		
		Long agreementNo = storedProcedureDAO.executeStoredProcedure("CHARGEAGREEMENTID");
		List<String> partnerCodesList = new ArrayList<String>();
		List<ProductAgreementPartnersVO> productAgreementPartnerslist = cpeCharHelperDTO.getPartnrsList();
		ChargesAgmt  chargesAgmt = null;
		RsAgrPartners rsAgreementPartners = null;
		try{
		
			for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
				partnerCodesList.add(productAgreementPartnersVO.getPartnercode());
			}
			Collections.sort(partnerCodesList);
			
			chargesAgmt= new ChargesAgmt();
			chargesAgmt.setAgruniqueId(agreementNo);
			chargesAgmt.setAgrfDate(Calendar.getInstance());
			chargesAgmt.setPartnerList(partnerCodesList.toString());
			chargesAgmt.setRsTmplCode(cpeCharHelperDTO.getTemplCode());
			chargesAgmt.setStatus(new Short("1"));
			cpeDao.saveOrUpdate(chargesAgmt);
		
			for (ProductAgreementPartnersVO productAgreementPartnersVO : productAgreementPartnerslist) {
				 rsAgreementPartners = new RsAgrPartners();
				rsAgreementPartners.setAgrUniqueId(agreementNo);
				rsAgreementPartners.setPartnerCode(productAgreementPartnersVO.getPartnercode());
				rsAgreementPartners.setPartnerSlNo(productAgreementPartnersVO.getPartnertypeslno());
				rsAgreementPartners.setPartnerType(productAgreementPartnersVO.getPartnertype());
				rsAgreementPartners.setStatus(new Short("1"));
				cpeDao.saveOrUpdate(rsAgreementPartners);
			}
			
			returnValue = "Success";
			
		}catch(Exception ex){
			LOGGER.info(ex.getMessage());
			ex.printStackTrace();
			returnValue = "Failure";
		} finally {
			agreementNo = null;
			partnerCodesList = null;
			productAgreementPartnerslist = null;
			chargesAgmt = null;
			rsAgreementPartners = null;
		}
		
		
		
		return returnValue;
	}

	@Override
	public List<CpeCharHelperDTO> viewCpeChrgAgmtByTenant(String tanantCode) {
		List<CpeCharHelperDTO> responceList = new ArrayList<>();
		List<Object[]> list = cpeCharAgmtDao.findAllCpeChargeArgmtsByTenant(tanantCode);
		CpeCharHelperDTO cpeCharHelperDTO = null;
		try {
		for(Object[] obj : list){
			  cpeCharHelperDTO = new CpeCharHelperDTO();
			cpeCharHelperDTO.setAggrId(obj[0].toString());
			cpeCharHelperDTO.setAggrFDate(obj[1].toString());
			cpeCharHelperDTO.setTenantName(obj[2].toString());
			cpeCharHelperDTO.setTenantType(obj[3].toString());
			cpeCharHelperDTO.setTemplCode(obj[4].toString());
			responceList.add(cpeCharHelperDTO);
		}
		} catch(Exception e)
		{ e.printStackTrace();
		} finally {
			list = null;
			cpeCharHelperDTO = null;
		}
		return responceList;
	}

}
