package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

public class TenantsWalletTransId implements Serializable {
	
	private static final long serialVersionUID = 3745409564014823042L;
	
	private Calendar trandate;
	private String tenantcode;
	private String tranrefno;
	public Calendar getTrandate() {
		return trandate;
	}
	public void setTrandate(Calendar trandate) {
		this.trandate = trandate;
	}
	public String getTenantcode() {
		return tenantcode;
	}
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}
	public String getTranrefno() {
		return tranrefno;
	}
	public void setTranrefno(String tranrefno) {
		this.tranrefno = tranrefno;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tenantcode == null) ? 0 : tenantcode.hashCode());
		result = prime * result + ((trandate == null) ? 0 : trandate.hashCode());
		result = prime * result + ((tranrefno == null) ? 0 : tranrefno.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantsWalletTransId other = (TenantsWalletTransId) obj;
		if (tenantcode == null) {
			if (other.tenantcode != null)
				return false;
		} else if (!tenantcode.equals(other.tenantcode))
			return false;
		if (trandate == null) {
			if (other.trandate != null)
				return false;
		} else if (!trandate.equals(other.trandate))
			return false;
		if (tranrefno == null) {
			if (other.tranrefno != null)
				return false;
		} else if (!tranrefno.equals(other.tranrefno))
			return false;
		return true;
	}
	
	
	

}
