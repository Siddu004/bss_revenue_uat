/*package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.vo.TenantVO;

public interface TenantService {
	
	public abstract void deleteTenants(Tenant tenant);
	
	public abstract Tenant findByTenantCode(String tenantCode);

	public abstract Tenant findByTenantId(Integer tenantId);

	public abstract List<Tenant> findAllTenants();

	public abstract void saveTenant(Tenant tenant);

	public abstract Tenant updateTenant(Tenant tenant);
	
	public abstract void createTenants(TenantVO tenantVO, Integer tenantId);
	
	public abstract List<Tenant> findMspCodeByTenantType();
	
}
*/