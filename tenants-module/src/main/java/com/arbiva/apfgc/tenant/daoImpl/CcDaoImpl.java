
package com.arbiva.apfgc.tenant.daoImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.dao.CcDao;

@Repository
public class CcDaoImpl implements CcDao{

	private static final Logger LOGGER = Logger.getLogger(CcDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	
	@Override
	public <T> T saveOrUpdate(T t) {
		LOGGER.info("CcDaoImpl :: saveOrUpdate()");
		return getEntityManager().merge(t);
	}

	public String findAadharNo(String cafNo) {
		StringBuilder builder = null;
		Query query = null;
		String aadharNo = null;
		try {
			LOGGER.info("CcDaoImpl :: findAadharNo()");
			builder = new StringBuilder("SELECT aadharno FROM cafs WHERE cafno = '"+cafNo+"' ");
			LOGGER.info("CcDaoImpl :: findAadharNo() :: Builder Value ::"+builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			aadharNo = (String)query.getSingleResult();
			LOGGER.info("CcDaoImpl :: findAadharNo() :: Aadhar No ::"+aadharNo);
		} catch(Exception e) {
			LOGGER.error("CcDaoImpl :: findAadharNo() " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return aadharNo;
	}

	@Override
	public String findMobileNo(String custId) {
		StringBuilder builder = null;
		Query query = null;
		String mobileNo = null;
		try {
			LOGGER.info("CcDaoImpl :: findMobileNo()");
			builder = new StringBuilder("SELECT pocmob1 FROM customermst WHERE custid= '"+custId+"' ");
			LOGGER.info("CcDaoImpl :: findAadharNo() :: Builder Value ::"+builder);
			query = getEntityManager().createNativeQuery(builder.toString());
			mobileNo = (String)query.getSingleResult();
			LOGGER.info("CcDaoImpl :: findMobileNo() :: Mobile No ::"+mobileNo);
		} catch(Exception e) {
			LOGGER.error("CcDaoImpl :: findMobileNo() " +e);
			e.printStackTrace();
		}finally{
			builder = null;
			query = null;
		}
		return mobileNo;
	}

	@Override
	public boolean updatetenantswallet(String custId, String amount) {
		String qry = null;
		Query query = null;
		boolean result=false;
		
		try {
			LOGGER.info("START::updatetenantswallet()");
		
		    qry ="UPDATE tenantswallet SET `deposit_amt`='"+amount+"' WHERE `tenantcode`='"+custId+"'";
        	query = getEntityManager().createNativeQuery(qry);			
        	query.executeUpdate();
        	result=true;        	
        	LOGGER.info("END::updatetenantswallet()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::updatetenantswallet() " + e);
			result=false;
		}finally{
			query = null;			
		}
		
		return result;
	}
}
