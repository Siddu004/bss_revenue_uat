package com.arbiva.apfgc.tenant.model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.arbiva.apfgc.tanent.util.DateUtill;

@Entity
@Table(name = "products")
public class Products extends Base {
	

	private static final long serialVersionUID = 1L;
	
	public Products() {

	}
	
	public Products(ProductsPK productsPK, Tenant tenants,
			HttpServletRequest request) throws ParseException {
		this.setId(productsPK);
		//this.setGlcode(productInfoVo.getGlCodeIds());
		this.setCreatedipaddr(request.getRemoteAddr());
		this.setCreatedon(Calendar.getInstance());
		this.setModifiedipaddr(request.getRemoteAddr());
		this.setModifiedon(Calendar.getInstance());
		this.setStatus(new Short("1"));
		this.setTenant(tenants);
		}
	
	public Products(ProductsPK pk, MspChnlsStg mspChnlsStg, HttpServletRequest httpServletRequest) {
		
		this.setEffectiveTo(DateUtill.convertStringToDate("9999-12-31", "yyyy-MM-dd"));
	    this.setProdname(mspChnlsStg.getPkgname());
	    this.setProdType('A');
	    this.setValidfrom(Calendar.getInstance().getTime());
	    this.setValidto(DateUtill.convertStringToDate("9999-12-31", "yyyy-MM-dd"));
	    this.setDurationDays(0);
	    this.setLockInDays(0);
	    this.setGlobalflag("N");
	    this.setCreatedby(mspChnlsStg.getMspcode());
	    this.setStatus((short) 1);
	    this.setCreatedon(mspChnlsStg.getCreatedon());
	    this.setCreatedby(mspChnlsStg.getMspcode());
	    this.setCreatedipaddr(httpServletRequest.getRemoteAddr());
	    this.setModifiedon(Calendar.getInstance());
	    this.setCustTypeLov("I");
	    this.setId(pk);
	}

	@EmbeddedId
    private ProductsPK id;

	public ProductsPK getId() {
        return this.id;
    }

	public void setId(ProductsPK id) {
        this.id = id;
    }

	@ManyToOne
    @JoinColumn(name = "tenantcode", referencedColumnName = "tenantcode", nullable = false, insertable = false, updatable = false)
    private Tenant tenant;

	@Column(name = "prodname", length = 100, unique = true)
    @NotNull
    private String prodname;
	
	@Column(name = "custtypelov", length = 100)
    private String custTypeLov;

	@Column(name = "validfrom")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date validfrom;

	@Column(name = "validto")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date validto;
	
	@Column(name = "effectiveto")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date effectiveTo;
	
	@Column(name = "prodtype")
    private Character prodType;
    

	@Column(name = "lockindays")
    private Integer lockInDays;
	

	@Column(name = "durationdays")
    private Integer durationDays;
	
	@Column(name = "globalflag")
	private String globalflag;
/*    
	@Column(name = "glcode", length = 12)
    private String glcode;*/
	
	
	

	public Integer getDurationDays() {
		return durationDays;
	}

	public String getGlobalflag() {
		return globalflag;
	}

	public void setGlobalflag(String globalflag) {
		this.globalflag = globalflag;
	}

	public void setDurationDays(Integer durationDays) {
		this.durationDays = durationDays;
	}

/*	public String getGlcode() {
		return glcode;
	}

	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}*/

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public Character getProdType() {
		return prodType;
	}

	public void setProdType(Character prodType) {
		this.prodType = prodType;
	}

	public String getCustTypeLov() {
		return custTypeLov;
	}

	public void setCustTypeLov(String custTypeLov) {
		this.custTypeLov = custTypeLov;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public String getProdname() {
		return prodname;
	}

	public void setProdname(String prodname) {
		this.prodname = prodname;
	}

	public Date getValidfrom() {
		return validfrom;
	}

	public void setValidfrom(Date validfrom) {
		this.validfrom = validfrom;
	}

	public Date getValidto() {
		return validto;
	}

	public void setValidto(Date validto) {
		this.validto = validto;
	}

	public Integer getLockInDays() {
		return lockInDays;
	}

	public void setLockInDays(Integer lockInDays) {
		this.lockInDays = lockInDays;
	}

	
}
