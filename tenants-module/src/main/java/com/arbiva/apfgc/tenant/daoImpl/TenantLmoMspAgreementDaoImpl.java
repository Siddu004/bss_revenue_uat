/**
 * 
 *//*
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreement;

*//**
 * @author Arbiva
 *
 *//*
@Repository
public class TenantLmoMspAgreementDaoImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantLmoMspAgreementDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public TenantLmoMspAgreement findByMspCode(String mspCode, String effDate, String lmoCode) {

		
		 * StringBuilder builder = new StringBuilder(" FROM "
		 * ).append(TenantLmoMspAgreement.class.getSimpleName()).append(
		 * " WHERE mspcode=:mspCode"); builder.append(" AND agrfdate=:date");
		 * builder.append("And lmocode=:lmoCode"); try {
		 * LOGGER.info("START::findByMspCode()");
		 * TypedQuery<TenantLmoMspAgreement> query =
		 * getEntityManager().createQuery(builder.toString(),
		 * TenantLmoMspAgreement.class); query.setParameter("mspCode", mspCode);
		 * query.setParameter("date", effDate); query.setParameter("lmoCode",
		 * lmoCode); tenantLmoMspAgreement = query.getSingleResult();
		 * LOGGER.info("END::findByMspCode()"); } catch (Exception e) {
		 * LOGGER.error("EXCEPTION::findByMspCode() " + e); }
		 
		TenantLmoMspAgreement tenantLmoMspAgreement = null;
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLmoMspAgreement.class.getSimpleName()).append(
				" where mspcode='" + mspCode + "' and lmocode='" + lmoCode + "' and agrfdate='" + effDate + "' ");
		TypedQuery<TenantLmoMspAgreement> query = getEntityManager().createQuery(builder.toString(),
				TenantLmoMspAgreement.class);
		List results = query.getResultList();
		if (!results.isEmpty())
			tenantLmoMspAgreement = (TenantLmoMspAgreement) results.get(0);

			return tenantLmoMspAgreement;

	}

	public TenantLmoMspAgreement findByLmoCode(String lmoCode) {
		TenantLmoMspAgreement tenantLmoMspAgreement = new TenantLmoMspAgreement();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLmoMspAgreement.class.getSimpleName())
				.append(" WHERE lmocode=:lmoCode");
		try {
			LOGGER.info("START::findByLmoCode()");
			TypedQuery<TenantLmoMspAgreement> query = getEntityManager().createQuery(builder.toString(),
					TenantLmoMspAgreement.class);
			query.setParameter("lmoCode", lmoCode);
			tenantLmoMspAgreement = query.getSingleResult();
			LOGGER.info("END::findByLmoCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByLmoCode() " + e);
		}
		return tenantLmoMspAgreement;

	}

	public List<TenantLmoMspAgreement> findAllTenantLmoMspAgreements() {
		List<TenantLmoMspAgreement> tenantLmoMspAgreement = new ArrayList<TenantLmoMspAgreement>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantLmoMspAgreement.class.getSimpleName());
		try {
			LOGGER.info("START::findAllTenantLmoMspAgreement()");
			TypedQuery<TenantLmoMspAgreement> query = getEntityManager().createQuery(builder.toString(),
					TenantLmoMspAgreement.class);
			tenantLmoMspAgreement = query.getResultList();
			LOGGER.info("END::findAllTenantLmoMspAgreement()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenantLmoMspAgreement() " + e);
		}
		return tenantLmoMspAgreement;
	}

	public void saveTenantLmoMspAgreement(TenantLmoMspAgreement tenantLmoMspAgreement) {
		try {
			getEntityManager().merge(tenantLmoMspAgreement);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::saveTenantLmoMspAgreement() " + e);
			e.printStackTrace();
		}
	}
}
*/