/*package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.model.TenantDocuments;
import com.arbiva.apfgc.tenant.vo.TenantVO;

public interface TenantDocumentsService {
	
	public abstract void deleteTenantDocuments(TenantDocuments tenantDocuments);
	
	public abstract List<TenantDocuments> findByTenantCode(String tenantCode);

	public abstract List<TenantDocuments> findAllTenantDocuments();

	public abstract void saveTenantDocument(TenantVO tenantVO, Integer tenantId);

	public abstract TenantDocuments updateTenantDocuments(TenantDocuments tenantDocuments);


}
*/