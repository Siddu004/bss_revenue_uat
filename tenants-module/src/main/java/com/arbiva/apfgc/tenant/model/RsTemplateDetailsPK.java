package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;


public class RsTemplateDetailsPK implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	private String rstmplCode;
	private int tenantslno;
	private String regioncode;
	private Date effectivefrom;
	
	public String getRstmplCode() {
		return rstmplCode;
	}
	public void setRstmplCode(String rstmplCode) {
		this.rstmplCode = rstmplCode;
	}
	public int getTenantslno() {
		return tenantslno;
	}
	public void setTenantslno(int tenantslno) {
		this.tenantslno = tenantslno;
	}
	public String getRegioncode() {
		return regioncode;
	}
	public void setRegioncode(String regioncode) {
		this.regioncode = regioncode;
	}
	public Date getEffectivefrom() {
		return effectivefrom;
	}
	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectivefrom == null) ? 0 : effectivefrom.hashCode());
		result = prime * result + ((regioncode == null) ? 0 : regioncode.hashCode());
		result = prime * result + ((rstmplCode == null) ? 0 : rstmplCode.hashCode());
		result = prime * result + tenantslno;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RsTemplateDetailsPK other = (RsTemplateDetailsPK) obj;
		if (effectivefrom == null) {
			if (other.effectivefrom != null)
				return false;
		} else if (!effectivefrom.equals(other.effectivefrom))
			return false;
		if (regioncode == null) {
			if (other.regioncode != null)
				return false;
		} else if (!regioncode.equals(other.regioncode))
			return false;
		if (rstmplCode == null) {
			if (other.rstmplCode != null)
				return false;
		} else if (!rstmplCode.equals(other.rstmplCode))
			return false;
		if (tenantslno != other.tenantslno)
			return false;
		return true;
	}
}
