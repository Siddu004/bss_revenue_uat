package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tenant.daoImpl.PortalAssetsDaoImpl;
import com.arbiva.apfgc.tenant.model.PortalAssets;
import com.arbiva.apfgc.tenant.vo.ListOfPortalAssets;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@Service
public class PortalAssetsServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(PortalAssetsServiceImpl.class);

	@Autowired
	PortalAssetsDaoImpl portalassetsdao;

	@Autowired
	HttpServletRequest httpServletRequest;

	public List<PortalAssets> findAllPortalAssets() {
		List<PortalAssets> portalassets = new ArrayList<PortalAssets>();
		portalassets = portalassetsdao.findAllPortalAssets();
		return portalassets;
	}

	/*public List<PortalAssets> findByEnrollmentno(String Enrollmentno) {
		List<PortalAssets> portalassets = new ArrayList<PortalAssets>();
		portalassets = portalassetsdao.findByEnrollmentno(Enrollmentno);
		return portalassets;
	}*/
	
	public List<ListOfPortalAssets> findByEnrollmentno(String Enrollmentno) {
		List<ListOfPortalAssets> portalAssets = new ArrayList<>();
		List<Object[]> portalOdjects = portalassetsdao.findByEnrollmentno(Enrollmentno);
		ListOfPortalAssets pa;
		for (Object[] object : portalOdjects) {
			try {
				  pa = new ListOfPortalAssets();
				pa.setCabletypeid(object[0] == null ? "" : object[0].toString());
				pa.setAssettypeid(object[1] == null ? "" : object[1].toString());
				pa.setRoutename(object[2] == null ? "" : object[2].toString());
				pa.setSenttranstime(object[3] == null ? "" : object[3].toString());
				pa.setImieno(object[4] == null ? "" : object[4].toString());
				pa.setVersionno(object[5] == null ? "" : object[5].toString());
				portalAssets.add(pa);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				portalOdjects = null;
				pa = null;
			}
		}
		return portalAssets;
	}

	public void savePortalAssets(TenantVO tenantVO) {
		
		
		List<ListOfPortalAssets> portalAssetsList = tenantVO.getPassets();
		PortalAssets portalassets;
		for(ListOfPortalAssets portalAssets : portalAssetsList)
		{
			
			try {
				if (portalAssets.getCabletypeid() == null &&
						portalAssets.getAssettypeid() == null &&
						portalAssets.getRoutename() == null &&
						portalAssets.getRoutemap() == null &&
						portalAssets.getCapturedassets() == null &&
						portalAssets.getSenttranstime() == null &&
						portalAssets.getImieno() == null &&
						portalAssets.getVersionno() == null)
				{
					continue;
				}
				else
				{
					  portalassets = new PortalAssets();
					portalassets.setEnrollmentno(tenantVO.getPortalEnrllmentno());
					portalassets.setRoutemapid(portalAssets.getRoutemapid());
					portalassets.setCabletypeid(portalAssets.getCabletypeid() == null || portalAssets.getCabletypeid().isEmpty() ? 0 : Integer.valueOf(portalAssets.getCabletypeid()));
					portalassets.setAssettypeid(portalAssets.getAssettypeid() == null || portalAssets.getAssettypeid().isEmpty() ? 0 : Integer.valueOf(portalAssets.getAssettypeid()));
					portalassets.setRoutename(portalAssets.getRoutename());
					portalassets.setRoutemap(portalAssets.getRoutemap().getBytes());
					portalassets.setCapturedassets(portalAssets.getCapturedassets().getBytes());
					portalassets.setSenttranstime(portalAssets.getSenttranstime().isEmpty() ? null : DateUtill.stringtoDate((portalAssets.getSenttranstime())));
					portalassets.setImieno(portalAssets.getImieno());
					portalassets.setVersionno(portalAssets.getVersionno());
					portalassetsdao.savePortalAssets(portalassets);
				}
			} catch (Exception e) {
				LOGGER.error("PortalAssetsServiceImpl::savePortalAssets() " + e);
				e.printStackTrace();
			} /*finally {
				portalAssetsList = null;
				portalAssets = null;
				portalassets  = null;
			}*/
			
		}
		
			

	}

}
