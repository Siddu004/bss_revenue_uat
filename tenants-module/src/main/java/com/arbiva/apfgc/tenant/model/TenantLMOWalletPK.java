/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

*//**
 * @author Arbiva
 *
 *//*
public class TenantLMOWalletPK implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	
	private String mspCode;
	
	private String lmoCode;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lmoCode == null) ? 0 : lmoCode.hashCode());
		result = prime * result + ((mspCode == null) ? 0 : mspCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantLMOWalletPK other = (TenantLMOWalletPK) obj;
		if (lmoCode == null) {
			if (other.lmoCode != null)
				return false;
		} else if (!lmoCode.equals(other.lmoCode))
			return false;
		if (mspCode == null) {
			if (other.mspCode != null)
				return false;
		} else if (!mspCode.equals(other.mspCode))
			return false;
		return true;
	}
	
}
*/