package com.arbiva.apfgc.tenant.vo;

public class LMORevSummaryVO {

	private String year;
	private String month;
	private String apsflShare;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getApsflShare() {
		return apsflShare;
	}

	public void setApsflShare(String apsflShare) {
		this.apsflShare = apsflShare;
	}

}
