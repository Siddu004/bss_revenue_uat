package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.dto.CpeCharHelperDTO;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;

public interface CpeCharAgmtService {

	List<RevenueSharingTemplateMaster> getAllCpeTempl();

	List<CpeCharHelperDTO> getChargeCodesByTemplate(String tmplCode);

	String save(CpeCharHelperDTO cpeCharHelperDTO);

	List<CpeCharHelperDTO> viewCpeChrgAgmtByTenant(String tanantCode);

}
