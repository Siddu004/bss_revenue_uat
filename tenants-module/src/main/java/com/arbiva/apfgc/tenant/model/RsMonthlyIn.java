package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="rsmonthlyin")
@IdClass(RsmonthlyinPK.class)
public class RsMonthlyIn {
	
	@Id
	@Column(name = "tenantcode")
	private String tenantcode;

	@Id
	@Column(name = "formon")
	private String formon;

	@Column(name = "shareamt")
	private BigDecimal shareamt;
	
	@Column(name = "taxamt")
	private BigDecimal taxamt;
	
	@Column(name = "majorvillcnt")
	private int majorvillcnt;
	
	@Column(name = "minorvillcnt")
	private int minorvillcnt;
	
	@Column(name = "enttax")
	private BigDecimal enttax;
	
	@Column(name = "taxstr")
	private String taxstr;
	
	@Column(name = "grosspaidamt")
	private BigDecimal grosspaidamt;
	
	@Column(name = "netpaidamt")
	private BigDecimal netpaidamt;
	
	@Column(name = "transcount")
	private Long transcount;
	
	@Column(name = "lasttnttransid")
	private Long lasttnttransid;
	
	@Column(name = "lastadjtransid")
	private Long lastadjtransid;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "createdon")
	private Date createdon;
	
	@Column(name = "createdby")
	private String createdby;
	
	@Column(name = "createdipaddr")
	private String createdipaddr;
	
	@Column(name = "modifiedon")
	private Date modifiedon;
	
	@Column(name = "modifiedby")
	private String modifiedby;
	
	@Column(name = "modifiedipaddr")
	private String modifiedipaddr;

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getFormon() {
		return formon;
	}

	public void setFormon(String formon) {
		this.formon = formon;
	}

	public BigDecimal getShareamt() {
		return shareamt;
	}

	public void setShareamt(BigDecimal shareamt) {
		this.shareamt = shareamt;
	}

	public BigDecimal getTaxamt() {
		return taxamt;
	}

	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}

	public int getMajorvillcnt() {
		return majorvillcnt;
	}

	public void setMajorvillcnt(int majorvillcnt) {
		this.majorvillcnt = majorvillcnt;
	}

	public int getMinorvillcnt() {
		return minorvillcnt;
	}

	public void setMinorvillcnt(int minorvillcnt) {
		this.minorvillcnt = minorvillcnt;
	}

	public BigDecimal getEnttax() {
		return enttax;
	}

	public void setEnttax(BigDecimal enttax) {
		this.enttax = enttax;
	}

	public String getTaxstr() {
		return taxstr;
	}

	public void setTaxstr(String taxstr) {
		this.taxstr = taxstr;
	}

	public BigDecimal getGrosspaidamt() {
		return grosspaidamt;
	}

	public void setGrosspaidamt(BigDecimal grosspaidamt) {
		this.grosspaidamt = grosspaidamt;
	}

	public BigDecimal getNetpaidamt() {
		return netpaidamt;
	}

	public void setNetpaidamt(BigDecimal netpaidamt) {
		this.netpaidamt = netpaidamt;
	}

	public Long getTranscount() {
		return transcount;
	}

	public void setTranscount(Long transcount) {
		this.transcount = transcount;
	}

	public Long getLasttnttransid() {
		return lasttnttransid;
	}

	public void setLasttnttransid(Long lasttnttransid) {
		this.lasttnttransid = lasttnttransid;
	}

	public Long getLastadjtransid() {
		return lastadjtransid;
	}

	public void setLastadjtransid(Long lastadjtransid) {
		this.lastadjtransid = lastadjtransid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedipaddr() {
		return modifiedipaddr;
	}

	public void setModifiedipaddr(String modifiedipaddr) {
		this.modifiedipaddr = modifiedipaddr;
	}
	
}
