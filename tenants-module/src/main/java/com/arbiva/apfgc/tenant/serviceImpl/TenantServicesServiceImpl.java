/**
 * 
 *//*
package com.arbiva.apfgc.tenant.serviceImpl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tenant.daoImpl.TenantServicesDaoImpl;
import com.arbiva.apfgc.tenant.model.TenantServices;
import com.arbiva.apfgc.tenant.vo.TenantAgreementVO;

*//**
 * @author Arbiva
 *
 *//*
@Service
public class TenantServicesServiceImpl {

	@Autowired
	TenantServicesDaoImpl tenantServicesDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantServicesServiceImpl.class);

	@Transactional
	public void saveTenantServices(TenantAgreementVO tenantAgreementVO, String action) throws ParseException {
		String loginId = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		if (loginId != null || loginId != "") {
			
			List<TenantServices> tenantServicesList = tenantServicesDao.findByTenantCode(tenantAgreementVO.getTenantCode(),
										DateUtill.stringtoString(tenantAgreementVO.getAgrFDate()));
			for(TenantServices tenantServices : tenantServicesList){
				tenantServices.setStatus(0);
				tenantServicesDao.saveTenantServices(tenantServices);
			}
				for(String coreService : tenantAgreementVO.getCoreServiceMulti()){
					TenantServices tenantServices = new TenantServices();
					tenantServices.setTenantCode(tenantAgreementVO.getTenantCode());
					tenantServices.setAgrFDate(DateUtill.stringtoDate(tenantAgreementVO.getAgrFDate()));
					tenantServices.setCoresrvcCode(coreService);
					tenantServices.setCratedIPAddress(httpServletRequest.getRemoteAddr());
					tenantServices.setCreatedBy(loginId);
					tenantServices.setCreatedDate(Calendar.getInstance());
					tenantServices.setModifiedDate(Calendar.getInstance());
					tenantServices.setStatus(1);
					tenantServicesDao.saveTenantServices(tenantServices);
				}
				
		}
	}

	public List<TenantServices> findByTenantCode(String tenantCode, String effDate) {
		return tenantServicesDao.findByTenantCode(tenantCode, effDate);
	}

	public TenantServices findTenantService(String tenantCode, Date agrFDate, String coresrvcCode) {
		TenantServices tenantServices = new TenantServices();
		tenantServices = tenantServicesDao.findTenantService(tenantCode, agrFDate, coresrvcCode);
		return tenantServices;
	}

	public List<TenantServices> findAllTenantServicess() {
		List<TenantServices> tenantServicesList = new ArrayList<TenantServices>();
		tenantServicesList = tenantServicesDao.findAllTenantServicess();
		return tenantServicesList;
	}
}
*/