package com.arbiva.apfgc.tenant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.arbiva.apfgc.tenant.vo.ProductAgreementPartnersVO;

@Entity
@Table(name="rsagrpartners")
@IdClass(RsAgrPartnersPK.class)
public class RsAgrPartners {

	@Id
	@Column(name = "agruniqueid")
	private Long agrUniqueId;

	@Id
	@Column(name = "partnertype")
	private String partnerType;

	@Id
	@Column(name = "partnerslno")
	private int partnerSlNo;

	@Column(name = "partnercode")
	private String partnerCode;

	@Column(name = "status", columnDefinition = "tinyint(1) default 1")
	@NotNull
	private Short status;

	
	

	public RsAgrPartners() {
		// TODO Auto-generated constructor stub
	}
	public RsAgrPartners(ProductAgreementPartnersVO productAgreementPartnersVO, RsAgrmnts rsAgreement) {
		this.setPartnerCode(productAgreementPartnersVO.getPartnercode());
		this.setPartnerType(productAgreementPartnersVO.getPartnertype());
		this.setPartnerSlNo(productAgreementPartnersVO.getPartnertypeslno());
		this.setStatus(new Short("1"));
		this.setAgrUniqueId(rsAgreement.getAgruniqueid());
	}
	public Long getAgrUniqueId() {
		return agrUniqueId;
	}
	public void setAgrUniqueId(Long agrUniqueId) {
		this.agrUniqueId = agrUniqueId;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public int getPartnerSlNo() {
		return partnerSlNo;
	}
	public void setPartnerSlNo(int partnerSlNo) {
		this.partnerSlNo = partnerSlNo;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}
	
}
