package com.arbiva.apfgc.tenant.daoImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.dao.CpeDao;
import com.arbiva.apfgc.tenant.model.CpeCharges;
import com.arbiva.apfgc.tenant.model.CpeProfileMaster;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.TenantCpeDemand;

@Repository
public class CpeDaoImpl implements CpeDao {

	private static final Logger LOGGER = Logger.getLogger(CpeDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAllCpeMasterList() {
		List<Object> cpeMasterList = new ArrayList<Object>();
		StringBuilder builder = new StringBuilder();
		Query query =  null;

		try { 
			 LOGGER.info("CpeDaoImpl :: findAllCpeMasterList() :: START");
			builder.append(" SELECT distinct cpetypelov FROM cpe_profilemaster ");
			query = getEntityManager().createNativeQuery(builder.toString());
			cpeMasterList = query.getResultList();
			 LOGGER.info("CpeDaoImpl :: findAllCpeMasterList() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl :: findAllCpeMasterList() " + e);
		} finally {
			builder = null;
			 query =  null;
		}
		return cpeMasterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CpeProfileMaster> findAllCpeModelByCpeType(String cpeType) {
		List<CpeProfileMaster> cpeMasterList = new ArrayList<CpeProfileMaster>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: findAllCpeModelByCpeType() :: START");
			builder.append(" SELECT * FROM cpe_profilemaster where cpetypelov = '" + cpeType + "' ");
			query = getEntityManager().createNativeQuery(builder.toString(), CpeProfileMaster.class);
			cpeMasterList = query.getResultList();
			 LOGGER.info("CpeDaoImpl :: findAllCpeModelByCpeType() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::findAllCpeMasterList() " + e);
		} finally {
			builder = null;
			 query =  null;
		}
		return cpeMasterList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public CpeCharges getCpeChargesByProfileId(String profileId) {
		CpeCharges cpeCharges = new CpeCharges();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		List list = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getCpeChargesByProfileId() :: START");
		    builder.append(" SELECT * FROM cpecharges where profile_id = " + profileId + " and current_date()  between effectivefrom and effectiveto");
			 query = getEntityManager().createNativeQuery(builder.toString(), CpeCharges.class);
			 list = query.getResultList();
			if (list.size() > 0)
				cpeCharges = (CpeCharges) query.getResultList().get(0);
			    LOGGER.info("CpeDaoImpl :: getCpeChargesByProfileId() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::getCpeChargesByProfileId() " + e);
		} finally {
			 builder = null;
			  query = null;
			  list = null;
		}
		return cpeCharges;
	}

	@Override
	public void saveCpeOrders(TenantCpeDemand tenantCpeDemand) {
		try{
			 LOGGER.info("CpeDaoImpl :: saveCpeOrders() :: START");
			getEntityManager().merge(tenantCpeDemand);
			 LOGGER.info("CpeDaoImpl :: saveCpeOrders() :: END");
		}catch(Exception e)
		{			
			LOGGER.info("CpeDaoImpl :: saveCpeOrders() "+e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllCpeOrdersList(String tenantCode, String tenantType) {
		List<Object[]> cpeMasterList = new ArrayList<Object[]>();
		/*
		 * StringBuilder builder = new StringBuilder(
		 * " SELECT t.tenantname,cpm.cpetypelov,cpm.cpe_model,cpech.purchasecost,tcm.dmndqty,tcm.dmnddate,tcm.dmndprice"
		 * ); builder.append(
		 * " FROM tenantcpedmnd tcm, tenants t, cpe_profilemaster cpm, cpecharges cpech"
		 * ); builder.append(" where t.tenantcode = tcm.tenantcode");
		 * builder.append(" and tcm.profile_id = cpm.profile_id");
		 * builder.append(" and tcm.profile_id = cpech.profile_id");
		 * builder.append(
		 * " and current_date() between cpech.effectivefrom and cpech.effectiveto"
		 * );
		 */

		StringBuilder builder = new StringBuilder();
		Query query = null;
		
		try {
			 LOGGER.info("CpeDaoImpl :: findAllCpeOrdersList() :: START");
		if (tenantType.trim().equalsIgnoreCase("APFGC") || tenantType.trim().equalsIgnoreCase("APSFL")) {
			builder.append("select a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice, ifnull(SUM(b.dlvqty),0), a.dmndid, a.profile_id, ");
			builder.append(" SUM(CASE WHEN b.status = 1 THEN ifnull(dlvqty,0) ELSE 0 END) delivered,");
			builder.append(" SUM(CASE WHEN b.status = 0 THEN ifnull(dlvqty,0) ELSE 0 END) blocked");
			builder.append(" from");
			builder.append(" (SELECT t.tenantname,cpm.cpetypelov,cpm.cpe_model,cpech.purchasecost,tcm.dmndqty,tcm.dmnddate,tcm.dmndprice,tcm.dmndid,tcm.profile_id,tcm.status");
			builder.append(" FROM tenantcpedmnd tcm, tenants t, cpe_profilemaster cpm, cpecharges cpech");
			builder.append(" where t.tenantcode = tcm.tenantcode");
			builder.append(" and tcm.profile_id = cpm.profile_id");
			builder.append(" and tcm.profile_id = cpech.profile_id");
			builder.append(" and current_date() between cpech.effectivefrom and cpech.effectiveto) a ");
			builder.append(" LEFT JOIN tenantcpedlv b");
			builder.append(" on a.dmndid = b.dmndid");
			builder.append(" Group By a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice,a.dmndid,a.profile_id");
		} else {
			builder.append("select a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice, ifnull(SUM(b.dlvqty),0), a.dmndid, a.profile_id,");
			builder.append(" SUM(CASE WHEN b.status = 1 THEN ifnull(dlvqty,0) ELSE 0 END) delivered,");
			builder.append(" SUM(CASE WHEN b.status = 0 THEN ifnull(dlvqty,0) ELSE 0 END) blocked");
			builder.append(" from");
			builder.append(" (SELECT t.tenantname,cpm.cpetypelov,cpm.cpe_model,cpech.purchasecost,tcm.dmndqty,tcm.dmnddate,tcm.dmndprice,tcm.dmndid,tcm.profile_id");
			builder.append(" FROM tenantcpedmnd tcm, tenants t, cpe_profilemaster cpm, cpecharges cpech");
			builder.append(" where t.tenantcode = tcm.tenantcode");
			builder.append(" and tcm.profile_id = cpm.profile_id");
			builder.append(" and tcm.profile_id = cpech.profile_id");
			builder.append(" and tcm.tenantcode = '" + tenantCode + "'");
			builder.append(" and current_date() between cpech.effectivefrom and cpech.effectiveto) a ");
			builder.append(" LEFT JOIN tenantcpedlv b");
			builder.append(" on a.dmndid = b.dmndid");
			builder.append(" Group By a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice,a.dmndid,a.profile_id");
		}
			query = getEntityManager().createNativeQuery(builder.toString());
			cpeMasterList = query.getResultList();
			 LOGGER.info("CpeDaoImpl :: findAllCpeOrdersList() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::findAllCpeMasterList() " + e);
		} finally {
			builder = null;
			query = null;
		}
		return cpeMasterList;
	}

	@Override
	public Object[] findCpeDemandByDemandId(String demandId) {
		Object[] cpeDemand = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: findCpeDemandByDemandId() :: START");
			builder.append("select a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice, ifnull(SUM(b.dlvqty),0), a.dmndid,a.profile_id,a.tenantcode, a.tenanttypelov  from");
			builder.append(" (SELECT t.tenantname,cpm.cpetypelov,cpm.cpe_model,cpech.purchasecost,tcm.dmndqty,tcm.dmnddate,tcm.dmndprice,tcm.dmndid,tcm.profile_id, tcm.tenantcode, t.tenanttypelov");
			builder.append(" FROM tenantcpedmnd tcm, tenants t, cpe_profilemaster cpm, cpecharges cpech");
			builder.append(" where t.tenantcode = tcm.tenantcode");
			builder.append(" and tcm.profile_id = cpm.profile_id");
			builder.append(" and tcm.profile_id = cpech.profile_id");
			builder.append(" and tcm.dmndid = '" + demandId + "' ");
			builder.append(" and current_date() between cpech.effectivefrom and cpech.effectiveto) a ");
			builder.append(" LEFT JOIN tenantcpedlv b");
			builder.append(" on a.dmndid = b.dmndid");
			builder.append(" Group By a.tenantname,a.cpetypelov,a.cpe_model,a.purchasecost,a.dmndqty,a.dmnddate,a.dmndprice,a.dmndid,a.profile_id,a.tenantcode, a.tenanttypelov");
		
			query = getEntityManager().createNativeQuery(builder.toString());
			cpeDemand = (Object[]) query.getSingleResult();
			 LOGGER.info("CpeDaoImpl :: findCpeDemandByDemandId() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::findCpeDemandByDemandId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cpeDemand;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllcpeDlvByDemandId(String demandId) {
		List<Object[]> cpeMasterList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: findCpeDemandByDemandId() :: SATRT");
			builder.append(" select x.dlvdate,x.dlvqty,x.dlvid,tcp.pmntid  from ( select t.dlvdate,t.dlvqty,t.dlvid from  tenantcpedlv t where t.dmndid = :demandId)x LEFT JOIN");
			builder.append(" tenantcpepmnts tcp on tcp.dlvid = x.dlvid ");
			
			 query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("demandId", demandId);
			cpeMasterList = query.getResultList();
			 LOGGER.info("CpeDaoImpl :: findCpeDemandByDemandId() :: END");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllcpeDlvByDemandId() " + e);
		}finally {
			query = null;
			builder = null;
		}
		return cpeMasterList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAvailabileCpeCountByProfileId(String profileId) {
		List<String> cpeSlnList = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getAvailabileCpeCountByProfileId() :: SATRT");
		    builder.append(" select cpeslno from cpestock where profile_id = '" + profileId + "' And status = 1 ");
	
			query = getEntityManager().createNativeQuery(builder.toString());
			cpeSlnList = query.getResultList();
			LOGGER.info("END::getAvailabileCpeCountByProfileId()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::getAvailabileCpeCountByProfileId() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cpeSlnList;
	}
	
	@Override
	public <T> T saveOrUpdate(T t) {
		return getEntityManager().merge(t);
	}

	@Override
	public void saveOrUpdate(CpeStock cpeStock, String tenantCode,String cpesrlno) {
		
		try{
			
		StringBuilder builderLog = new StringBuilder();	
		getEntityManager().merge(cpeStock);
		builderLog.append("update cpestock_history SET user = '"+tenantCode+"'   where cpesrlno = '"+cpesrlno+"' and transaction_status='C'"  );
		Query queryLog=getEntityManager().createNativeQuery(builderLog.toString());
		queryLog.executeUpdate();
		}catch(Exception e){
			LOGGER.error("EXCEPTION :: saveOrUpdate() " + e);
		}
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public CpeStock getCpeByCpeSnoAndMac(String cpeSno, String cpeMacAdd) {
		CpeStock cpeStock = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSno() :: SATRT");
			builder.append(" select * from cpestock where cpeslno = :cpeslno or cpemacaddr = :cpeMac ");
		 
			query = getEntityManager().createNativeQuery(builder.toString(), CpeStock.class);
			query.setParameter("cpeslno", cpeSno);
			query.setParameter("cpeMac", cpeMacAdd);
			List list = query.getResultList();
			if(list.size() > 0)
				cpeStock = (CpeStock) list.get(0);
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSno() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::getCpeByCpeSno() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cpeStock;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public CpeStock getCpeByCpeSno(String cpeSno) {
		CpeStock cpeStock = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSno() :: SATRT");
			builder.append(" select * from cpestock where cpeslno = :cpeslno  ");
		 
			query = getEntityManager().createNativeQuery(builder.toString(), CpeStock.class);
			query.setParameter("cpeslno", cpeSno);
			List list = query.getResultList();
			if(list.size() > 0)
				cpeStock = (CpeStock) list.get(0);
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSno() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::getCpeByCpeSno() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cpeStock;
	}

	@Override
	public String findIsPaidByDlvId(String dlvid) {

		return null;
	}

	@Override
	public void updateCpeStock(long dlvId) {

		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: updateCpeStock() :: SATRT");
		builder.append(" update cpestock set status = status / 10 where dlvid = :dlvid ");
		query = getEntityManager().createNativeQuery(builder.toString());
		query.setParameter("dlvid", dlvId);
		query.executeUpdate();
		} catch(Exception e)
		{ 				
			LOGGER.info("CpeDaoImpl :: updateCpeStock() ::"+e);
			e.printStackTrace();
			} finally {
				query = null;
				builder = null;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllCpeSnosByDlvId(String dlvId) {
		List<String> responce = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getAllCpeSnosByDlvId() :: SATRT");
		     builder.append(" select cpeslno from cpestock where dlvid = :dlvId");
		 
			query = getEntityManager().createNativeQuery(builder.toString());
			query.setParameter("dlvId", dlvId);
			responce = query.getResultList();
			 LOGGER.info("CpeDaoImpl :: getAllCpeSnosByDlvId() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::getAllCpeSnosByDlvId() " + e);
		}finally {
			query = null;
			builder = null;
		}
		return responce;
	}

	@Override
	public void updateCpeDelivery(long dlvId) {
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: updateCpeDelivery() :: SATRT");
			 builder.append(" update tenantcpedlv set status = 1 where dlvid = :dlvid ");
		    query = getEntityManager().createNativeQuery(builder.toString());
		    query.setParameter("dlvid", dlvId);
		    query.executeUpdate();
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::updateCpeDelivery() " + e);
		}finally {
			query = null;
			builder = null;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllCpeModels(String cpeType) {
		
		List<Object[]> responce = new ArrayList<>();
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: findAllCpeModels() :: SATRT");
			 builder.append(" select cp.cpe_model,cp.cpe_profilename,cc.purchasecost, cp.profile_id from cpe_profilemaster cp, cpecharges cc ");
			 builder.append(" where cc.profile_id = cp.profile_id and cp.cpetypelov = :cpeType");
			 query = getEntityManager().createNativeQuery(builder.toString());
			 query.setParameter("cpeType", cpeType);
			 responce = query.getResultList();
			 LOGGER.info("END::findAllCpeModels()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllCpeModels() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return responce;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public CpeStock getCpeByCpeSnoAndMspCode(String cpeSno, String mspCode) {
		CpeStock cpeStock = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSnoAndMspCode() :: SATRT");
			builder.append(" select * from cpestock where cpeslno = :cpeSlno AND mspcode = :mspCode");
		 
			query = getEntityManager().createNativeQuery(builder.toString(), CpeStock.class);
			query.setParameter("cpeSlno", cpeSno);
			query.setParameter("mspCode", mspCode);
			
			List list = query.getResultList();
				if(list.size() > 0)
					cpeStock = (CpeStock)list.get(0);
			 LOGGER.info("CpeDaoImpl :: getCpeByCpeSnoAndMspCode() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::getCpeByCpeSnoAndMspCode() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return cpeStock;
	}

	@Override
	public void updateCpeBlockedAmount(int size, String profileId, String tenantCode) {
		
		StringBuilder builder = new StringBuilder();
		Query query = null;
		try {
			 LOGGER.info("CpeDaoImpl :: updateCpeBlockedAmount() :: SATRT");
			 
			builder.append(" UPDATE tenantswallet "
					+ " SET cpeblkamt = cpeblkamt + "
					+ " (SELECT (upfrontcharges * "+size+") chramt FROM cpecharges WHERE profile_id = "+profileId
					+ " AND CURRENT_DATE BETWEEN effectivefrom AND effectiveto)  "
					+ " WHERE tenantcode = '"+tenantCode+"'");
		 
			query = getEntityManager().createNativeQuery(builder.toString());
			
			query.executeUpdate();
			 LOGGER.info("CpeDaoImpl :: updateCpeBlockedAmount() :: END");
		} catch (Exception e) {
			LOGGER.error("CpeDaoImpl::updateCpeBlockedAmount() " + e);
		} finally {
			query = null;
			builder = null;
		}
	}
	@Override
	public void deleteCpeList(List<CpeStock> cpeStockList,String user) {
		StringBuilder builderLog = new StringBuilder();
		Timestamp date=null;
		try {
			LOGGER.info("CpeDaoImpl :: deleteCpeList() :: START");
	          for (CpeStock cpeStock : cpeStockList )
	          {
	        	  LOGGER.info("deleted ::  " + cpeStock.getCpeslno());
	        	  LOGGER.info("macAddress ::  " + cpeStock.getCpeMacAddr());
	        	  LOGGER.info("batchdate ::  " + cpeStock.getBatchDate());
	        	  getEntityManager().remove(cpeStock);
	        	  getEntityManager().flush();
	        	  builderLog.setLength(0);
	      			builderLog.append("update cpestock_history SET user = '"+user+"'   where cpesrlno = '"+cpeStock.getCpeslno()+"' and transaction_status='D'"  );
	      			Query queryLog=getEntityManager().createNativeQuery(builderLog.toString());
	      			queryLog.executeUpdate();
	      		  
	      			LOGGER.info("TenantDAOImpl :: updateLMOCpeDetails() :: END updating user in log table");
	        	  LOGGER.info("deleted at ::  " + new Date());
	        	  
	          }

			LOGGER.info("CpeDaoImpl :: deleteCpeList() :: END");
		} catch(Exception e){
			LOGGER.error("CpeDaoImpl :: deleteCpeList()" + e);
			
		}
	
	}
	
	

	
	
	

}
