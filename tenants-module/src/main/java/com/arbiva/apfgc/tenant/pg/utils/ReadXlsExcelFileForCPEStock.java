/*package com.arbiva.apfgc.tenant.pg.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tenant.model.CpeStock;

public class ReadXlsExcelFileForCPEStock {
	
	public static List<CpeStock> extractExcelFile(MultipartFile excelFile, Model model) {
		List<CpeStock> cpeStockList = new ArrayList<CpeStock>();
		FileInputStream fileInputStream = null;
		int numHeadRows = 1;
		int totalRows = 0;
		try {
			InputStream is = excelFile.getInputStream();
			XSSFWorkbook workBook = new XSSFWorkbook(is);
			XSSFSheet worksheet1 = workBook.getSheetAt(0);
			XSSFRow headerRow =  worksheet1.getRow(0); 
			int cellNum = headerRow.getPhysicalNumberOfCells();
			for(int i=0 ; i<cellNum; i++){
				switch(i){
					case 0 : 
						if( headerRow.getCell(i) == null || headerRow.getCell(i).getStringCellValue() == "" || (!headerRow.getCell(i).getStringCellValue().equalsIgnoreCase("Cep Serial No")))
							model.addAttribute("errorMsg", "Please upload a excel document in predefined format.");
						else 
							continue;
						break;
					case 1 : 
						if( headerRow.getCell(i) == null || headerRow.getCell(i).getStringCellValue() == "" || (!headerRow.getCell(i).getStringCellValue().equalsIgnoreCase("Cpe Mac Address")))
							model.addAttribute("errorMsg", "Please upload a excel document in predefined format.");
						else 
							continue;
						break;
					case 2 : 
						if( headerRow.getCell(i) == null || headerRow.getCell(i).getStringCellValue() == "" || (!headerRow.getCell(i).getStringCellValue().equalsIgnoreCase("Batch Id")))
							model.addAttribute("errorMsg", "Please upload a excel document in predefined format.");
						else 
							continue;
						break;
					case 3 : 
						if( headerRow.getCell(i) == null || headerRow.getCell(i).getStringCellValue() == "" || (!headerRow.getCell(i).getStringCellValue().equalsIgnoreCase("Batch Date")))
							model.addAttribute("errorMsg", "Please upload a excel document in predefined format.");
						else 
							continue;
						break;
					
					default:
						break;
				}
			}
			String  errorMsg =  (String)model.asMap().get("errorMsg");
			if(errorMsg == null || !(errorMsg.equalsIgnoreCase("The excel document is not a valid format.")) ||
					!(errorMsg.equalsIgnoreCase("Please upload a excel document in predefined format."))){
				Iterator rows = worksheet1.rowIterator();
				totalRows = worksheet1.getPhysicalNumberOfRows() - 1;
				int rowIndex = 1;
				loop:while (rows.hasNext()) {
					try {

						if (rowIndex <= numHeadRows) {
							rowIndex++;
							rows.next();
							continue;
						}
						
						Row row = (Row) rows.next();
						
						
						
						CpeStock cpeStock = new CpeStock();
						
						Boolean isEmpty = false;
						for (int index = 0; index < cellNum; index++) {
							switch (index) {
							case 0:
								if(row.getCell(index) == null || row.getCell(index).getCellType() == Cell.CELL_TYPE_BLANK || row.getCell(index).getCellType() == Cell.CELL_TYPE_ERROR)
									model.addAttribute("errorMsg", "This document has one or more fields blank or zero. Please check the upload document and try again.");
								else if(row.getCell(index) != null && row.getCell(index).getCellType() == Cell.CELL_TYPE_STRING)
									cpeStock.setCpeslno(row.getCell(index).getStringCellValue());
								else if(row.getCell(index) != null && row.getCell(index).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									 double name = row.getCell(index).getNumericCellValue();
									 cpeStock.setCpeslno(String.valueOf(name));
								}
								else continue;
								break;
							case 1:
								if(row.getCell(index) == null || row.getCell(index).getCellType() == Cell.CELL_TYPE_BLANK || row.getCell(index).getCellType() == Cell.CELL_TYPE_ERROR)
									model.addAttribute("errorMsg", "The  document has one or more fields blank or zero. Please check the upload document and try again.");
								else if(row.getCell(index) != null && row.getCell(index).getCellType() == Cell.CELL_TYPE_STRING)
									cpeStock.setCpeMacAddr(row.getCell(index).getStringCellValue());
								else if(row.getCell(index) != null && row.getCell(index).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									 double name = row.getCell(index).getNumericCellValue();
									 cpeStock.setCpeMacAddr(String.valueOf(name));
								}
								else continue;
								break;
							case 2:
								if(row.getCell(index) == null || row.getCell(index).getCellType() == Cell.CELL_TYPE_BLANK || row.getCell(index).getCellType() == Cell.CELL_TYPE_ERROR)
									 cpeStock.setBatchId(null);
								else if(row.getCell(index).getCellType() == Cell.CELL_TYPE_STRING)
									cpeStock.setBatchId(row.getCell(index).getStringCellValue());
								else if(row.getCell(index).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									 double name = row.getCell(index).getNumericCellValue();
									 cpeStock.setBatchId(String.valueOf(name));
								}
								else continue;
								break;
							case 3:
								
								if(row.getCell(index) == null || row.getCell(index).getCellType() == Cell.CELL_TYPE_BLANK || row.getCell(index).getCellType() == Cell.CELL_TYPE_ERROR)
									cpeStock.setBatchDate(null);
								else if(row.getCell(index).getCellType() == Cell.CELL_TYPE_STRING)
									cpeStock.setBatchDate(DateUtill.stringtoDate(row.getCell(index).getStringCellValue()));
								else if(row.getCell(index).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									 double name = row.getCell(index).getNumericCellValue();
									 cpeStock.setBatchDate(DateUtill.stringtoDate(String.valueOf(name)));
								}
								else continue;
								break;
							
							
							default:
								break;
							}
						}
						cpeStockList.add(cpeStock);
					} catch (Exception exception) {
						model.addAttribute("errorMsg", exception.getMessage());
					}
				}
			} 
		} catch (Exception exception) {
			return cpeStockList;
		} finally {
			try {
				if (fileInputStream != null)
					fileInputStream.close();
			} catch (IOException e) {
			}
		}

		return cpeStockList;

	}

}
*/