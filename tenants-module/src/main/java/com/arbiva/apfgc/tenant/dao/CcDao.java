package com.arbiva.apfgc.tenant.dao;


public interface CcDao {
	
	<T> T saveOrUpdate(T t);
	
	String findAadharNo(String cafNo);
	
	String findMobileNo(String custId);

	boolean updatetenantswallet(String custId, String amount);
	
}
