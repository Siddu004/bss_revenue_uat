/**
 * 
 */
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.model.PortalAssets;
import com.arbiva.apfgc.tenant.model.Region;

/**
 * @author Arbiva
 *
 */
@Component("regionDao")
public class RegionDaoImpl {

	private static final Logger LOGGER = Logger.getLogger(RegionDaoImpl.class);
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<Region> findAllRegions() {
		List<Region> regions = new ArrayList<Region>();
		StringBuilder builder = new StringBuilder();
		TypedQuery<Region> query = null;
			try {
				 LOGGER.info("RegionDaoImpl :: findAllRegions() :: SATRT");
				 builder.append(" FROM ").append(Region.class.getSimpleName());
				 query = getEntityManager().createQuery(builder.toString(), Region.class);
				 regions = query.getResultList();
				 LOGGER.info("END::findAllRegions()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION :: RegionDaoImpl::findAllRegions() " + e);
		} finally {
			query = null;
			builder = null;
		}
		return regions;
	}
	
	public Region getRegionTypeByRegionName(String regionName) {
		Region region = null;
		StringBuilder builder = new StringBuilder();
		Query query = null;
			try {
				 LOGGER.info("RegionDaoImpl :: findAllRegions() :: SATRT");
				 builder .append("select * from regions where regionname = '"+regionName+"' ");
		
				 query = getEntityManager().createNativeQuery(builder.toString(), Region.class);
				region = (Region) query.getSingleResult();
				LOGGER.info("END::getRegionTypeByRegionName()");
			} catch (Exception e) {
				LOGGER.error("EXCEPTION::getRegionTypeByRegionName() " + e);
			} finally {
				query = null;
				builder = null;
			}
		  return region;
		
	}
}
