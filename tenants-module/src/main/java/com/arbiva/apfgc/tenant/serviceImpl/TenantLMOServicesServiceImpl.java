/**
 * 
 *//*
package com.arbiva.apfgc.tenant.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tenant.daoImpl.RegionDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantLMOServicesDaoImpl;
import com.arbiva.apfgc.tenant.model.Region;
import com.arbiva.apfgc.tenant.model.RegionPK;
import com.arbiva.apfgc.tenant.model.TenantLMOServices;
import com.arbiva.apfgc.tenant.vo.TenantLmoMspAgreementVO;

*//**
 * @author Arbiva
 *
 *//*
@Service
public class TenantLMOServicesServiceImpl {

	@Autowired
	TenantLMOServicesDaoImpl tenantLMOServicesDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	RegionDaoImpl regionDao;

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantLMOServicesDaoImpl.class);

	@Transactional
	public void saveTenantLMOServices(TenantLmoMspAgreementVO tenantLmoMspAgreementVO, String action)
			throws ParseException {
		String userName = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		if (userName != null || userName != "") {

			List<TenantLMOServices> lmoServices = tenantLMOServicesDao.findByMspCode(
					tenantLmoMspAgreementVO.getMspCode(),DateUtill.stringtoString(tenantLmoMspAgreementVO.getAgreementFrom()),
					tenantLmoMspAgreementVO.getLmoCode());
			for (TenantLMOServices tenantLMOServices : lmoServices) {
				tenantLMOServices.setStatus(0);
				tenantLMOServicesDao.saveTenantLMOServices(tenantLMOServices);
			}

			Region region = regionDao.getRegionTypeByRegionName(tenantLmoMspAgreementVO.getRegion());
			RegionPK regionPK = new RegionPK(region.getId().getRegionType(), region.getId().getRegionCode());
			region.setId(regionPK);
			TenantLMOServices tenantLMOServices = new TenantLMOServices();
			tenantLMOServices = new TenantLMOServices();
			tenantLMOServices.setAgrFDate(DateUtill.stringtoDate(tenantLmoMspAgreementVO.getAgreementFrom()));
			tenantLMOServices.setLmoCode(tenantLmoMspAgreementVO.getLmoCode());
			tenantLMOServices.setMspCode(tenantLmoMspAgreementVO.getMspCode());
			tenantLMOServices.setRegion(region);
			tenantLMOServices.setCratedIPAddress(httpServletRequest.getRemoteAddr());
			tenantLMOServices.setCreatedBy(userName);
			tenantLMOServices.setCreatedDate(Calendar.getInstance());
			tenantLMOServices.setModifiedDate(Calendar.getInstance());
			tenantLMOServices.setStatus(1);

			for (String coreService : tenantLmoMspAgreementVO.getCoreServiceMulti()) {
				tenantLMOServices.setCoresrvcCode(coreService);
				tenantLMOServicesDao.saveTenantLMOServices(tenantLMOServices);
			}
			LOGGER.info(
					"TenantLMOServicesServiceImpl::saveTenantLMOServices()   Tenant Lmo Services Are Saved in lmosrvcs Table");
		}
	}

	public List<TenantLMOServices> findByMspCode(String mspCode, String effDate, String lmoCode) {
		return tenantLMOServicesDao.findByMspCode(mspCode, effDate, lmoCode);
	}

	public TenantLMOServices findByLmoCode(String lmoCode) {
		TenantLMOServices TenantLMOServices = new TenantLMOServices();
		TenantLMOServices = tenantLMOServicesDao.findByLmoCode(lmoCode);
		return TenantLMOServices;
	}

	public List<TenantLMOServices> findAllTenantLMOServices() {
		List<TenantLMOServices> TenantLMOServicesList = new ArrayList<TenantLMOServices>();
		TenantLMOServicesList = tenantLMOServicesDao.findAllTenantLMOServices();
		return TenantLMOServicesList;
	}

	public List<String> findCoreServicesByMspCode(String mspCode, String effDate, String lmoCode) {
		// TODO Auto-generated method stub
		return tenantLMOServicesDao.findCoreServicesByMspCode(mspCode,effDate,lmoCode);
	}
}
*/