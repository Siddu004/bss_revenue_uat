/*package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
@Entity
@Table(name = "tenantsagr", schema = "apfgc")
@IdClass(TenantAgreementPK.class)
public class TenantAgreement implements Serializable {
	
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Id
	@Column(name = "agrfdate")
	private Date agrFDate;
	
	@Column(name = "agrdate")
	private Date agreementDate;
	
	@Column(name = "agrtdate")
	private Date agreementTo;
	
	@Column(name = "agrdocref")
	private String agreementDocRef;
	
	@Column(name = "reasons")
	private String reasons;
	
	@Column(name = "status")
	private Integer  status;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private String modifiedBy;
	
	@Column(name = "modifiedipaddr")
	private String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private Calendar deactivatedDate;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpAddress;
	
	@OneToMany(targetEntity=TenantServices.class, mappedBy="tenantAgreement", fetch=FetchType.LAZY, cascade={CascadeType.ALL})
	private List<TenantServices> tenantServices;
	
	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public List<TenantServices> getTenantServices() {
		return tenantServices;
	}

	public void setTenantServices(List<TenantServices> tenantServices) {
		this.tenantServices = tenantServices;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public Date getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(Date agreementDate) {
		this.agreementDate = agreementDate;
	}

	public Date getAgreementTo() {
		return agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public String getAgreementDocRef() {
		return agreementDocRef;
	}

	public void setAgreementDocRef(String agreementDocRef) {
		this.agreementDocRef = agreementDocRef;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Calendar getDeactivatedDate() {
		return deactivatedDate;
	}

	public void setDeactivatedDate(Calendar deactivatedDate) {
		this.deactivatedDate = deactivatedDate;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpAddress() {
		return deactivatedIpAddress;
	}

	public void setDeactivatedIpAddress(String deactivatedIpAddress) {
		this.deactivatedIpAddress = deactivatedIpAddress;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
*/