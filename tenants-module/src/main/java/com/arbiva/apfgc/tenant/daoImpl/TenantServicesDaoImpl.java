/**
 * 
 *//*
package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.TenantServices;

*//**
 * @author Lakshman
 *
 *//*
@Repository
public class TenantServicesDaoImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenantServicesDaoImpl.class);

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	public List<TenantServices> findByTenantCode(String tenantCode, String effDate) {
		List<TenantServices> tenantServices = new ArrayList<TenantServices>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantServices.class.getSimpleName())
				.append(" WHERE tenantcode=:tenantCode and agrfdate=:agrFDate");
		try {
			LOGGER.info("START::findByTenantCode()");
			TypedQuery<TenantServices> query = getEntityManager().createQuery(builder.toString(), TenantServices.class);
			query.setParameter("tenantCode", tenantCode);
			query.setParameter("agrFDate", effDate);
			tenantServices = query.getResultList();
			LOGGER.info("END::findByTenantCode()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByTenantCode() " + e);
		}
		return tenantServices;
	}

	public TenantServices findTenantService(String tenantCode, Date agrFDate, String coresrvcCode) {
		TenantServices tenantServices = new TenantServices();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantServices.class.getSimpleName())
				.append(" WHERE tenantcode=:tenantCode and agrfdate=:agrFDate and coresrvccode=:coresrvcCode");
		try {
			LOGGER.info("START::findByTenantCode()");
			TypedQuery<TenantServices> query = getEntityManager().createQuery(builder.toString(), TenantServices.class);
			query.setParameter("tenantCode", tenantCode);
			query.setParameter("agrFDate", agrFDate);
			query.setParameter("coresrvcCode", coresrvcCode);
			tenantServices = query.getSingleResult();
			LOGGER.info("END::findTenantService()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findTenantService() " + e);
		}
		return tenantServices;
	}

	public List<TenantServices> findAllTenantServicess() {
		List<TenantServices> tenantServices = new ArrayList<TenantServices>();
		StringBuilder builder = new StringBuilder(" FROM ").append(TenantServices.class.getSimpleName());
		try {
			LOGGER.info("START::findAllTenantServices()");
			TypedQuery<TenantServices> query = getEntityManager().createQuery(builder.toString(), TenantServices.class);
			tenantServices = query.getResultList();
			LOGGER.info("END::findAllTenantServices()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllTenantServices() " + e);
		}
		return tenantServices;
	}

	public void saveTenantServices(TenantServices tenantServices) {
		try {
			getEntityManager().merge(tenantServices);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::saveTenantServices() " + e);
			e.printStackTrace();
		}
	}


}
*/