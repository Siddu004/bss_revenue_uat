/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

*//**
 * @author Arbiva
 *
 *//*
@Entity
@Table(name="lmosrvcs", schema="apfgc")
@IdClass(TenantLMOServicesPK.class)
public class TenantLMOServices implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="mspcode")
	private String mspCode;
	
	@Id
	@Column(name="lmocode")
	private String lmoCode;
	
	@Id
	@Column(name = "agrfdate")
	private Date agrFDate;
	
	@Id
	@Column(name="coresrvccode")
    private String coresrvcCode;
	
	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "regiontype", referencedColumnName = "regiontype", nullable = false), @JoinColumn(name = "regioncode", referencedColumnName = "regioncode", nullable = false) })
	private Region region;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "createdon")
	private Calendar createdDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Column(name = "createdipaddr")
	private String cratedIPAddress;
	
	@Column(name = "modifiedon")
	private Calendar modifiedDate;
	
	@Column(name = "modifiedby")
	private String modifiedBy;
	
	@Column(name = "modifiedipaddr")
	private String modifiedIPAddress;
	
	@Column(name = "deactivatedon")
	private String deactivatedOn;
	
	@Column(name = "deactivatedby")
	private String deactivatedBy;
	
	@Column(name = "deactivatedipaddr")
	private String deactivatedIpaddr;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumns({@JoinColumn(name="mspcode", referencedColumnName="mspcode", insertable=false,updatable=false),@JoinColumn(name="lmocode", referencedColumnName="lmocode", insertable=false,updatable=false), @JoinColumn(name="agrfdate", referencedColumnName="agrfdate", insertable=false,updatable=false)})
	private TenantLmoMspAgreement tenantLmoMspAgreement;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="coresrvccode", referencedColumnName="srvcCode", insertable=false,updatable=false)
	private CoreServices coreServices;

	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getLmoCode() {
		return lmoCode;
	}

	public void setLmoCode(String lmoCode) {
		this.lmoCode = lmoCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public String getCoresrvcCode() {
		return coresrvcCode;
	}

	public void setCoresrvcCode(String coresrvcCode) {
		this.coresrvcCode = coresrvcCode;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCratedIPAddress() {
		return cratedIPAddress;
	}

	public void setCratedIPAddress(String cratedIPAddress) {
		this.cratedIPAddress = cratedIPAddress;
	}

	public Calendar getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Calendar modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddress() {
		return modifiedIPAddress;
	}

	public void setModifiedIPAddress(String modifiedIPAddress) {
		this.modifiedIPAddress = modifiedIPAddress;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedIpaddr() {
		return deactivatedIpaddr;
	}

	public void setDeactivatedIpaddr(String deactivatedIpaddr) {
		this.deactivatedIpaddr = deactivatedIpaddr;
	}

	public TenantLmoMspAgreement getTenantLmoMspAgreement() {
		return tenantLmoMspAgreement;
	}

	public void setTenantLmoMspAgreement(TenantLmoMspAgreement tenantLmoMspAgreement) {
		this.tenantLmoMspAgreement = tenantLmoMspAgreement;
	}

	public CoreServices getCoreServices() {
		return coreServices;
	}

	public void setCoreServices(CoreServices coreServices) {
		this.coreServices = coreServices;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
*/