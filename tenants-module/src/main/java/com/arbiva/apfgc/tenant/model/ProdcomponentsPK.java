package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;



@Embeddable
public  class ProdcomponentsPK implements Serializable {

	@Column(name = "tenantcode", nullable = false, length = 20)
    private String tenantcode;

	@Column(name = "prodcode", nullable = false, length = 20)
    private String prodcode;

	@Column(name = "componenttype", nullable = false)
    private Character componenttype;

	@Column(name = "componentcode", nullable = false, length = 20)
    private String componentcode;
	
	@Column(name = "effectivefrom")
    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date effectiveFrom;


	public ProdcomponentsPK() {
        super();
    }
	
	public ProdcomponentsPK(String componentcode,String prodcode,String tenantcode) {
		this.componenttype='S';
		this.componentcode=componentcode;
		this.effectiveFrom = Calendar.getInstance().getTime();
		this.tenantcode = tenantcode;
		this.prodcode = prodcode;
    }

	/**
	 * @param tenants
	 * @param productInfoVo
	 * @param service
	 * @throws ParseException 
	 */


	public String getTenantcode() {
        return tenantcode;
    }

	public String getProdcode() {
        return prodcode;
    }

	public Character getComponenttype() {
        return componenttype;
    }

	public String getComponentcode() {
        return componentcode;
    }
	
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((componentcode == null) ? 0 : componentcode.hashCode());
		result = prime * result + ((componenttype == null) ? 0 : componenttype.hashCode());
		result = prime * result + ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
		result = prime * result + ((prodcode == null) ? 0 : prodcode.hashCode());
		result = prime * result + ((tenantcode == null) ? 0 : tenantcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdcomponentsPK other = (ProdcomponentsPK) obj;
		if (componentcode == null) {
			if (other.componentcode != null)
				return false;
		} else if (!componentcode.equals(other.componentcode))
			return false;
		if (componenttype == null) {
			if (other.componenttype != null)
				return false;
		} else if (!componenttype.equals(other.componenttype))
			return false;
		if (effectiveFrom == null) {
			if (other.effectiveFrom != null)
				return false;
		} else if (!effectiveFrom.equals(other.effectiveFrom))
			return false;
		if (prodcode == null) {
			if (other.prodcode != null)
				return false;
		} else if (!prodcode.equals(other.prodcode))
			return false;
		if (tenantcode == null) {
			if (other.tenantcode != null)
				return false;
		} else if (!tenantcode.equals(other.tenantcode))
			return false;
		return true;
	}

	
}
