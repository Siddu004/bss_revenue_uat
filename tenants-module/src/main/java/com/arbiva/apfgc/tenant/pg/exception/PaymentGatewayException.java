package com.arbiva.apfgc.tenant.pg.exception;

import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;

/**
 * {@link PaymentGatewayException} is an application specific exception to hold and
 * provide the proper error message object to end users.
 * 
 * @author srinivasa
 *
 */
public class PaymentGatewayException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private PaymentGatewayErrorMessageDTO errorMessageDTO;

	public PaymentGatewayException() {
		super();
	}

	public PaymentGatewayException(String message) {
		super(message);
	}

	public PaymentGatewayException(String message, Throwable cause) {
		super(message, cause);
	}

	public PaymentGatewayException(PaymentGatewayErrorMessageDTO messageDTO) {
		super();
		this.setErrorMessageDTO(messageDTO);
	}

	/**
	 * @return the ErrorMessageDTO
	 */
	public PaymentGatewayErrorMessageDTO getErrorMessageDTO() {
		return errorMessageDTO;
	}

	/**
	 * @param errorMessageDTO
	 *            the errorMessageDTO to set
	 */
	public void setErrorMessageDTO(PaymentGatewayErrorMessageDTO errorMessageDTO) {
		this.errorMessageDTO = errorMessageDTO;
	}

}
