package com.arbiva.apfgc.tenant.dao;

import java.util.List;

import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;

public interface CpeCharAgmtDao {

	List<RevenueSharingTemplateMaster> getAllCpeTempl();

	List<String> getChargeCodesByTemplate(String tmplCode);

	List<Object[]> findAllCpeChargeArgmtsByTenant(String tanantCode);

}
