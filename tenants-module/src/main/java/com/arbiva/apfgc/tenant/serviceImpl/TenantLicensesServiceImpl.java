package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.RegionDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantLicensesDaoImpl;
import com.arbiva.apfgc.tenant.model.Region;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantLicenses;
import com.arbiva.apfgc.tenant.vo.TenantVO;

@Service
public class TenantLicensesServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(TenantDaoImpl.class);

	@Autowired
	TenantLicensesDaoImpl tenantLicensesDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	RegionDaoImpl regionDao;
	

	@Transactional
	public void saveTenantLicenses(TenantVO tenantVO, Tenant tenant, Integer tenantId) {
		String userName = (String) httpServletRequest.getSession(false).getAttribute("username");
		Region region ;
		TenantLicenses tenantLicenses ;
		if (userName != null || userName != "") {
			try {
				  region = regionDao.getRegionTypeByRegionName(tenantVO.getRegion());
				  tenantLicenses = new TenantLicenses();
				if (tenantId == null) {
					tenantLicenses = new TenantLicenses();
					tenantLicenses.setCratedIPAddress(httpServletRequest.getRemoteAddr());
					tenantLicenses.setCreatedBy(userName);
					tenantLicenses.setCreatedDate(Calendar.getInstance());
					tenantLicenses.setModifiedDate(Calendar.getInstance());
					tenantLicenses.setStatus(1);
				} else {
					tenantLicenses = tenantLicensesDao.findByTenantCode(tenantVO.getTenantCode(),region);
					tenantLicenses.setModifiedBy(userName);
					tenantLicenses.setModifiedDate(Calendar.getInstance());
					tenantLicenses.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
					tenantLicenses.setStatus(1);
				}
				/*tenantLicenses.setLicenseAuthority(tenantVO.getLicenseAuthority());*/
				tenantLicenses.setLicenseexpDate(tenantVO.getEffectiveFrom());
				tenantLicenses.setLicenserefno(tenantVO.getLicenserefno());
				tenantLicenses.setRegion(region);
				tenantLicenses.setTenantcode(tenantVO.getTenantCode());
				tenantLicensesDao.saveTenantLicenses(tenantLicenses);
				tenantLicenses.setTenantObject(tenant);
			} catch (Exception e) {
				LOGGER.error("TenantLicensesServiceImpl::saveTenantLicenses() " + e);
				e.printStackTrace();
			} finally {
				userName = null;
				region = null;
				tenantLicenses = null;
			}
		}
	}


	public TenantLicenses findByTenantCode(String tenantCode) {
		return tenantLicensesDao.findByTenantCode(tenantCode);
	}
}
