package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="tenantcpepmnts")
public class TenantCpePmnts {
	
	@Id 
	@Column(name = "pmntId")
	private Long pmntId;
	
	@Column(name = "pmntdate")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar pmntDate;
	
	@Column(name = "pmntrefno")
	private String pmntRefNo;
	
	@Column(name = "pmntamt")
	private BigDecimal pmntAmt;

	@Column(name = "pmntmode")
	private String pmntMode;
	
	@Column(name = "dlvid")
	private long dlvId;
	
	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    @NotNull
    private Short status;
	
	@ManyToOne
	@JoinColumn(name = "dlvid", referencedColumnName = "dlvid", nullable = false, insertable = false, updatable = false)
	private TenantCpeDlv tenantCpeDlv;

	public Long getPmntId() {
		return pmntId;
	}

	public void setPmntId(Long pmntId) {
		this.pmntId = pmntId;
	}

	public Calendar getPmntDate() {
		return pmntDate;
	}

	public void setPmntDate(Calendar pmntDate) {
		this.pmntDate = pmntDate;
	}

	public String getPmntRefNo() {
		return pmntRefNo;
	}

	public void setPmntRefNo(String pmntRefNo) {
		this.pmntRefNo = pmntRefNo;
	}

	public BigDecimal getPmntAmt() {
		return pmntAmt;
	}

	public void setPmntAmt(BigDecimal pmntAmt) {
		this.pmntAmt = pmntAmt;
	}

	public String getPmntMode() {
		return pmntMode;
	}

	public void setPmntMode(String pmntMode) {
		this.pmntMode = pmntMode;
	}

	public long getDlvId() {
		return dlvId;
	}

	public void setDlvId(long dlvId) {
		this.dlvId = dlvId;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public TenantCpeDlv getTenantCpeDlv() {
		return tenantCpeDlv;
	}

	public void setTenantCpeDlv(TenantCpeDlv tenantCpeDlv) {
		this.tenantCpeDlv = tenantCpeDlv;
	}

}
