package com.arbiva.apfgc.tenant.pg.businessservice;

import java.util.List;

import org.springframework.stereotype.Component;

import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;

/**
 *
 * 
 * @author srinivasa
 *
 */
@Component("billdeskGatewayBusinessService")
public interface BilldeskGatewayBusinessService {

	public String processBilldeskPaymentRequest(RequestDTO requestDTO);

	public String processBilldeskPaymentResponse(String response, String responseBy);

	public List<Object[]> getPaymentRequestList();
}
