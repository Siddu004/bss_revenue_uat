package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="chrgrsagr")
@IdClass(ChargesAgmtIDClass.class)
public class ChargesAgmt {
	
	@Id
	@Column(name = "agruniqueid")
	private Long agruniqueId;
	
	@Id
	@Column(name = "agrfdate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Calendar agrfDate;
	
	@Column(name = "rstmplcode")
	private String rsTmplCode;
	
	@Column(name = "partnerlist")
	private String partnerList;
	
	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    private Short status;

	public Long getAgruniqueId() {
		return agruniqueId;
	}

	public void setAgruniqueId(Long agruniqueId) {
		this.agruniqueId = agruniqueId;
	}

	public Calendar getAgrfDate() {
		return agrfDate;
	}

	public void setAgrfDate(Calendar agrfDate) {
		this.agrfDate = agrfDate;
	}

	public String getRsTmplCode() {
		return rsTmplCode;
	}

	public void setRsTmplCode(String rsTmplCode) {
		this.rsTmplCode = rsTmplCode;
	}

	public String getPartnerList() {
		return partnerList;
	}

	public void setPartnerList(String partnerList) {
		this.partnerList = partnerList;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}
	
}
