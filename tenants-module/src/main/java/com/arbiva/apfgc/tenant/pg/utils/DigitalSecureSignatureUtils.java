package com.arbiva.apfgc.tenant.pg.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.xml.security.utils.Base64;

/**
 * 
 * @author srinivasa
 *
 */
public class DigitalSecureSignatureUtils {

	/**
	 * 
	 * @param content
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String sign(String content) throws NoSuchAlgorithmException {
		byte[] data = content.getBytes();
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		md.update(data);
		byte[] hash = md.digest();
		String encodeText = new String(Base64.encode(hash));
		return encodeText
				.replace("+", "-")
				.replace("/", "_")
				.replace("=", ",")
				.replace("\n", "");
	}

}
