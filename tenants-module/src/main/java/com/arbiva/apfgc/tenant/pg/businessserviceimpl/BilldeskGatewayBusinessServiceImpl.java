package com.arbiva.apfgc.tenant.pg.businessserviceimpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.ApsflHelper;
import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tenant.dao.CcDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.pg.businessservice.BilldeskGatewayBusinessService;
import com.arbiva.apfgc.tenant.pg.dao.PaymentGatewayDAO;
import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.dto.RequestDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.model.PaymentRequest;
import com.arbiva.apfgc.tenant.pg.model.PaymentResponse;
import com.arbiva.apfgc.tenant.pg.utils.BilldeskChecksumUtils;
import com.arbiva.apfgc.tenant.pg.utils.BilldeskGatewayInfo;
import com.arbiva.apfgc.tenant.pg.utils.PaymentAuthStatus;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;
import com.arbiva.apfgc.tenant.pg.utils.RestTemplateUtils;
import com.arbiva.apfgc.tenant.service.CcService;
import com.arbiva.apfgc.tenant.service.CpeService;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Component("billdeskGatewayBusinessServiceImpl")
public class BilldeskGatewayBusinessServiceImpl extends BaseBusinessServiceImpl
		implements BilldeskGatewayBusinessService {

	private static final Logger LOGGER = Logger.getLogger(BilldeskGatewayBusinessServiceImpl.class);
	
	@Autowired
	IpAddressValues ipAddressValues;
	
	@Autowired
	CcDao ccDao;
	
	RestTemplate restTemplate = new RestTemplate();

	// append checksum to request body with pipe separator
	private static final String BILLDESK_REQUEST_BODY_WITH_CHECKSUM = "%s|%s";

	private static final String PIPE_PATTERN_REGEX = Pattern.quote("|");

	@Autowired
	private PaymentGatewayDAO paymentGatewayDAO;

	@Autowired
	private BilldeskGatewayInfo BilldeskGatewayInfo;

	@Autowired
	private CpeService cpeService;
	
	@Autowired
	private CcService ccService;
	
	@Autowired
	private StoredProcedureDAO storedProcedureDAO;

	/**
	 * 
	 */
	@Transactional
	@Override
	public String processBilldeskPaymentRequest(RequestDTO requestDTO) {
		PaymentRequest request = null;
		String requestBody = null;
		String checksum = null;
		String url = null;
		try {
			LOGGER.info("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentRequest() :: Start");
			validateBilldeskPaymentRequest(requestDTO);
			String paymentType=requestDTO.getPaymentType();
			LOGGER.info("payement Type"+paymentType);
			LOGGER.info("Original Amount from UI ::"+requestDTO.getAmount());
			requestDTO.setAmount(requestDTO.getAmount());
			requestDTO.setRequestId(storedProcedureDAO.executeStoredProcedure("PGREQUESTID"));
			request = paymentGatewayDAO.savePaymentRequest(new PaymentRequest(requestDTO));
			requestDTO.setRequestId(request.getRequestId());
			
			if(paymentType.equalsIgnoreCase("onlineSelfCare")){
				requestBody = consolidatedBilldeskRequestMesssageCss(requestDTO);
			} else if(paymentType.equalsIgnoreCase("onlineSelfCareBeforeLogin")){
				requestBody = consolidatedBilldeskRequestMesssageCssBeforeLogin(requestDTO);
			} else {
				requestBody = consolidatedBilldeskRequestMesssage(requestDTO);
			}
			checksum = BilldeskChecksumUtils.getChecksum(BilldeskGatewayInfo.getChecksumAlg(),
					BilldeskGatewayInfo.getChecksumKey(), requestBody);
			requestBody = String.format(BILLDESK_REQUEST_BODY_WITH_CHECKSUM, requestBody, checksum);
			request.setRequest(requestBody.getBytes());
			paymentGatewayDAO.savePaymentRequest(request);
			LOGGER.info("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentRequest() :: End");
			url = RestTemplateUtils.post(BilldeskGatewayInfo.getRequestUrl(),requestBody, requestDTO.getGatewayType());
			LOGGER.info("Request Url :: "+url);
		} catch (Exception e) {
			LOGGER.error("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentRequest()" +e.getMessage());
			handleExceptions(e, "Error occured while processing Payment Request with Billdesk Gateway");
		} finally {
			request = null;
			requestBody = null;
			checksum = null;
		}

		return url;
	}

	/**
	 * 
	 */
	@Transactional
	@Override
	public String processBilldeskPaymentResponse(String response, String responseBy) {
		
		PaymentRequest request = null;
		String resposeChecksum = null;
		String calculatedChecksum = null;
		String responseWithoutCheckSume = null;
		PaymentResponse paymentResponse = null;
		String paymentType = null;
		String responseValue = "";
		String message="";
		String amount=this.getAmount(response);
		String Id = this.getCustId(response);
		String[] Nos = Id.split(",");
		String mobileNo=ccDao.findMobileNo(Nos[0]);
		try {
			LOGGER.info("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentResponse() :: Start");
			request = getPaymentRequestFromResponse(response);

			if (request.getStatus() == 0) {
				responseWithoutCheckSume = response.substring(0, response.lastIndexOf("|"));
				resposeChecksum = response.substring(response.lastIndexOf("|") + 1);
				calculatedChecksum = BilldeskChecksumUtils.getChecksum(BilldeskGatewayInfo.getChecksumAlg(),
						BilldeskGatewayInfo.getChecksumKey(), responseWithoutCheckSume);
				if (StringUtils.equalsIgnoreCase(resposeChecksum, calculatedChecksum)) {
					paymentResponse = new PaymentResponse();
					paymentResponse.setId(storedProcedureDAO.executeStoredProcedure("PGRESEPONSEID"));
					paymentResponse.setResponseBy(responseBy);
					paymentResponse.setResponse(response.getBytes());
					paymentResponse.setResponseCode(getHiddenResponseCode(response));
					paymentResponse.setRequestId(Long.valueOf(request.getRequestId()));
					paymentGatewayDAO.savePaymentResponse(paymentResponse);
					
					if (StringUtils.equalsIgnoreCase(this.getHiddenResponseCode(response),PaymentAuthStatus.BILL_DEK_SUCCESS.getCode())) {
						
						request.setStatus(1);
						paymentGatewayDAO.savePaymentRequest(request);
						
						paymentType = this.getPaymentType(response);
						if (paymentType.equalsIgnoreCase("cpe")) {
							LOGGER.info(" Creating record in tenantcpepmnts table");
							responseValue = cpeService.processPyament(this.getAmount(response),this.getDeliveryId(response),request.getRequestId());
						} else if  (paymentType.equalsIgnoreCase("onlineSelfCare") || paymentType.equalsIgnoreCase("onlineSelfCareBeforeLogin") || paymentType.equalsIgnoreCase("onlineSelfCareLMOAPSFLPayment") ){
							responseValue = ccService.processPyament(this.getAmount(response),this.getCustId(response),this.getTransactionId(response),this.getDeliveryId(response));
						} if  (paymentType.equalsIgnoreCase("onlineSelfCareLMOAPSFLPayment") && responseValue.equalsIgnoreCase("Success")){
							responseValue = ccService.processtenantswallet(this.getCustId(response),this.getAmount(response)); 
						LOGGER.info("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentResponse() :: End");
					} else if(!(StringUtils.equalsIgnoreCase(this.getHiddenResponseCode(response),PaymentAuthStatus.BILL_DEK_SUCCESS.getCode())) && (StringUtils.equalsIgnoreCase(this.getErrorMessage(response),PaymentAuthStatus.BILL_DEK_ERRORMESSAGE.getDesc()))){
						
						message = "Trasaction cancelled by the customer for the Account No: "+Nos[1];
						LOGGER.info(message);
					}else {
					         String originalAmt=amount.replaceFirst("^0+(?!$)", "");
						message = "Your Payment of Rs."+originalAmt+" is  Failed for the Account No: "+Nos[1];
						LOGGER.info(message);
						LOGGER.error("Error occured while processing Payment Response: ");
					}
				} else {
					LOGGER.error("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentResponse()");
					throw new PaymentGatewayException(
							new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.PGRES001,
									"Response checksum is not macthed with calculated checksum of a request id: "
											+ request.getRequestId()));
				}
			}
			
			HttpEntity<String> httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "sendSMS?mobileNo="+mobileNo+"&msg="+message;
			ResponseEntity<String> response1 = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			response1.getBody();
			LOGGER.info("Payment Failed message send to the customer");

		}
		}catch (Exception e) {
			LOGGER.error("BilldeskGatewayBusinessServiceImpl :: processBilldeskPaymentResponse()" +e);
			handleExceptions(e, "Error occured while processing Payment Response");
		} finally {
			request = null;
			paymentResponse = null;
			resposeChecksum = null;
			calculatedChecksum = null;
			responseWithoutCheckSume = null;

		}
		
		return responseValue;
	}

	/**
	 * @param response
	 * @return
	 */
	private String getAmount(String response) {
		return response.split(PIPE_PATTERN_REGEX)[4];
	}

	/**
	 * @param response
	 * @return
	 */
	private String getDeliveryId(String response) {
		return response.split(PIPE_PATTERN_REGEX)[18];
	}

	/**
	 * @param response
	 * @return
	 */
	private String getPaymentType(String response) {
		return response.split(PIPE_PATTERN_REGEX)[17];
	}
	
	private String getCustId(String response) {
		return response.split(PIPE_PATTERN_REGEX)[16];
	}
	
	private String getTransactionId(String response) {
		return response.split(PIPE_PATTERN_REGEX)[2];
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private PaymentRequest getPaymentRequestFromResponse(String response) {
		if (StringUtils.isBlank(response)) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Payment Response should not be null"));
		}
		String requestId = getHiddenRequestId(response);
		if (StringUtils.isBlank(requestId)) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002,
					"Request Id is not available as part of response body"));
		}
		PaymentRequest request = paymentGatewayDAO.findPaymentRequest(Long.valueOf(requestId));
		requestId = null;
		if (request == null) {
			throw new PaymentGatewayException(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.PGREQ001,
					"Payment Request is not exists for request id: " + requestId));
		}

		return request;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenResponseCode(String response) {
		return response.split(PIPE_PATTERN_REGEX)[14];
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private String getHiddenRequestId(String response) {
		return response.split(PIPE_PATTERN_REGEX)[1];
	}

	/**
	 * 
	 * @param dto
	 * @return
	 */
	private String consolidatedBilldeskRequestMesssage(RequestDTO dto) {
		return String.format(BilldeskGatewayInfo.getRequestPayload(), dto.getRequestId(), dto.getAmount(),
				dto.getCustomerId(), dto.getPaymentType(), dto.getDeliveryId());
	}
	
	private String consolidatedBilldeskRequestMesssageCss(RequestDTO dto) {
		return String.format(BilldeskGatewayInfo.getRequestPayloadCss(), dto.getRequestId(), dto.getAmount(),
				dto.getCustomerId(), dto.getPaymentType(), dto.getDeliveryId());
	}
	
	private String consolidatedBilldeskRequestMesssageCssBeforeLogin(RequestDTO dto) {
		return String.format(BilldeskGatewayInfo.getRequestPayloadCssBeforeLogin(), dto.getRequestId(), dto.getAmount(),
				dto.getCustomerId(), dto.getPaymentType(), dto.getDeliveryId());
	}

	/**
	 * 
	 * @param dto
	 */
	private void validateBilldeskPaymentRequest(RequestDTO dto) {
		StringBuilder builder = new StringBuilder();
		if (StringUtils.isBlank(dto.getCustomerId())) {
			builder.append("Customer Id should not be null").append(System.lineSeparator());
		}
		if (dto.getAmount() != null && dto.getAmount().compareTo(BigDecimal.ZERO) == 0) {
			builder.append("Payment Amount should not be null").append(System.lineSeparator());
		}

		if (builder.length() > 0) {
			throw new PaymentGatewayException(
					new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE002, builder.toString()));
		}
	}
	
	public List<Object[]> getPaymentRequestList() {
		return paymentGatewayDAO.getPaymentRequestList();
	}
	
	private String getErrorMessage(String response) {
		return response.split(PIPE_PATTERN_REGEX)[24];
	}
}
