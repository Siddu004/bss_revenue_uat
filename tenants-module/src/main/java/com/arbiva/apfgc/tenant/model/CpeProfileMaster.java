package com.arbiva.apfgc.tenant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="cpe_profilemaster")
public class CpeProfileMaster {
	
	@Id
	@Column(name = "profile_id")
	private Long profileId;
	
	@Column(name = "cpetypelov")
	private String cpeTypeLov;
	
	@Column(name = "cpe_model")
	private String cpeModel;
	
	@Column(name = "cpe_profilename")
	private String cpeProfilename;
	
	@Column(name = "cpe_modeldetails")
	private String cpeModeldetails;
	
	

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getCpeTypeLov() {
		return cpeTypeLov;
	}

	public void setCpeTypeLov(String cpeTypeLov) {
		this.cpeTypeLov = cpeTypeLov;
	}

	public String getCpeModel() {
		return cpeModel;
	}

	public void setCpeModel(String cpeModel) {
		this.cpeModel = cpeModel;
	}

	public String getCpeProfilename() {
		return cpeProfilename;
	}

	public void setCpeProfilename(String cpeProfilename) {
		this.cpeProfilename = cpeProfilename;
	}

	public String getCpeModeldetails() {
		return cpeModeldetails;
	}

	public void setCpeModeldetails(String cpeModeldetails) {
		this.cpeModeldetails = cpeModeldetails;
	}
	
}
