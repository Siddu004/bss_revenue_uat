package com.arbiva.apfgc.tenant.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

import com.arbiva.apfgc.tanent.util.DateUtill;

/**
 * 
 * @author srinivasa
 *
 */
@Entity
@Table(name = "srvcs")
@IdClass(AddlSrvcsPK.class)
public class AdditionalService extends Base {

	public AdditionalService() {
	}


	public AdditionalService(MspChnlsStg mspChnlsStg, HttpServletRequest httpServletRequest) {
		this.setEffectivefrom(Calendar.getInstance().getTime());
		this.setServCode(mspChnlsStg.getMspcode());
	    this.setServName(mspChnlsStg.getPkgname());
	    this.setEffectiveto(DateUtill.convertStringToDate("9999-12-31", "yyyy-MM-dd"));
	    this.setCoreServCode("IPTV");
	    this.setFtaFlag(0);
	    this.setFeaturecodes(mspChnlsStg.getChnlcode());
	    this.setStatus((short) 1);
	    this.setCreatedon(mspChnlsStg.getCreatedon());
	    this.setCreatedby(mspChnlsStg.getMspcode());
	    this.setCreatedipaddr(httpServletRequest.getRemoteAddr());
	    this.setModifiedon(mspChnlsStg.getCreatedon());
	    this.setMspCode(mspChnlsStg.getMspcode());
	}


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "srvccode")
	private String servCode;

	@Id
	@Column(name = "effectivefrom")
	private Date effectivefrom;
	
	@Column(name = "effectiveto")
	private Date effectiveto;
	
	@Column(name = "srvcname", nullable = false)
	private String servName;

	@Column(name = "coresrvccode", nullable = false)
	private String coreServCode;
	
	@Column(name = "featurecodes")
	private String featurecodes;
	
	@Column(name = "addchnls")
	private String addChnls;
	
	@Column(name = "delchnls")
	private String delChnls;
	
	@Column(name = "mspcode")
	private String mspCode;
	
	
	@Column(name = "ftaflag", columnDefinition="tinyint(4) default 0")
	private int ftaFlag;
	

	
	public String getMspCode() {
		return mspCode;
	}

	public void setMspCode(String mspCode) {
		this.mspCode = mspCode;
	}

	public String getAddChnls() {
		return addChnls;
	}

	public void setAddChnls(String addChnls) {
		this.addChnls = addChnls;
	}

	public String getDelChnls() {
		return delChnls;
	}

	public void setDelChnls(String delChnls) {
		this.delChnls = delChnls;
	}

	public int getFtaFlag() {
		return ftaFlag;
	}

	public void setFtaFlag(int ftaFlag) {
		this.ftaFlag = ftaFlag;
	}

	public String getFeaturecodes() {
		return featurecodes;
	}

	public void setFeaturecodes(String featurecodes) {
		this.featurecodes = featurecodes;
	}

	public String getServCode() {
		return servCode;
	}

	public void setServCode(String servCode) {
		this.servCode = servCode;
	}

	public String getServName() {
		return servName;
	}

	public void setServName(String servName) {
		this.servName = servName;
	}

	public String getCoreServCode() {
		return coreServCode;
	}

	public void setCoreServCode(String coreServCode) {
		this.coreServCode = coreServCode;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	public Date getEffectiveto() {
		return effectiveto;
	}

	public void setEffectiveto(Date effectiveto) {
		this.effectiveto = effectiveto;
	}
	
}
