package com.arbiva.apfgc.tenant.serviceImpl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.AESEncryption;
import com.arbiva.apfgc.tanent.util.DateUtill;
import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tanent.util.Response;
import com.arbiva.apfgc.tanent.util.TmsHelper;
import com.arbiva.apfgc.tenant.dao.CpeDao;
import com.arbiva.apfgc.tenant.dao.RsAgrmntsDao;
import com.arbiva.apfgc.tenant.daoImpl.RevenueSharingDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantWalletDaoImpl;
import com.arbiva.apfgc.tenant.dto.CpeHelperDTO;
import com.arbiva.apfgc.tenant.dto.CreditLmoWallet;
import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.dto.OLT;
import com.arbiva.apfgc.tenant.dto.PageObject;
import com.arbiva.apfgc.tenant.dto.RevenueSharingDTO;
import com.arbiva.apfgc.tenant.dto.TenantDTO;
import com.arbiva.apfgc.tenant.dto.TmsHelperDTO;
import com.arbiva.apfgc.tenant.dto.UploadHistDTO;
import com.arbiva.apfgc.tenant.model.AdditionalService;
import com.arbiva.apfgc.tenant.model.BlackListCust;
import com.arbiva.apfgc.tenant.model.CorpusRequest;
import com.arbiva.apfgc.tenant.model.CorpusResponse;
import com.arbiva.apfgc.tenant.model.CpeStock;
import com.arbiva.apfgc.tenant.model.Districts;
import com.arbiva.apfgc.tenant.model.Mandals;
import com.arbiva.apfgc.tenant.model.MspChnlsStg;
import com.arbiva.apfgc.tenant.model.OLTPortDetails;
import com.arbiva.apfgc.tenant.model.Prodcomponents;
import com.arbiva.apfgc.tenant.model.ProdcomponentsPK;
import com.arbiva.apfgc.tenant.model.Products;
import com.arbiva.apfgc.tenant.model.ProductsPK;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;
import com.arbiva.apfgc.tenant.model.RsMonthlyOut;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.model.TenantBankDetails;
import com.arbiva.apfgc.tenant.model.TenantDocuments;
import com.arbiva.apfgc.tenant.model.TenantLicenses;
import com.arbiva.apfgc.tenant.model.TenantWallet;
import com.arbiva.apfgc.tenant.model.TenantsWalletTrans;
import com.arbiva.apfgc.tenant.model.TenantsWalletTransErrors;
import com.arbiva.apfgc.tenant.model.UploadHistory;
import com.arbiva.apfgc.tenant.pg.utils.ComsEnumCodes;
import com.arbiva.apfgc.tenant.vo.BlackListInfoVO;
import com.arbiva.apfgc.tenant.vo.CafsForBlockListVO;
import com.arbiva.apfgc.tenant.vo.CpeStockVO;
import com.arbiva.apfgc.tenant.vo.TenantVO;


@Service
public class TenantServiceImpl {

	@Autowired
	TenantDaoImpl tenantDao;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	StoredProcedureDAO storedProcedureDAO;

	@Autowired
	TenantDocumentsServiceImpl tenantDocumentsService;

	@Autowired
	TenantBankDetailsServiceImpl tenantBankDetailsService;

	@Autowired
	TenantLicensesServiceImpl tenantLicensesService;

	@Autowired
	TenantWalletServiceImpl tenantWalletServiceImpl;

	@Autowired
	TenantWalletDaoImpl tenantWalletDao;

	@Autowired
	PortalAssetsServiceImpl portalassetsservice;

	@Autowired
	PortalAreaServiceImpl portalAreaService;

	@Autowired
	PortalAssetsServiceImpl portalAssetsServiceImpl;

	@Autowired
	PortalAreaServiceImpl portalAreaServiceImpl;

	@Autowired
	TmsHelper tmsHelper;

	@Autowired
	CpeDao cpeDao;

	@Autowired
	LovsServiceImpl lovsService;
	
	@Autowired
	RevenueSharingDaoImpl revenueSharingDaoImpl;
	
	@Autowired
	RsAgrmntsDao rsAgrmntsDao;
	
	
	@Autowired
	IpAddressValues ipAddressValues;

	@Value("${CORPUS-CALL-SAVE}")
	private String corpusCallSave;

	@Value("${CORPUS-CALL-UPDATE}")
	private String corpusCallUpdate;

	@Value("${CORPUS-USERNAME}")
	private String corpusUserName;

	@Value("${CORPUS-API-KEY}")
	private String corpusApiKey;

	@Value("${CORPUS-CALL-TO-GET-CHANNELS}")
	private String corpusCallToGetChannels;

	@Value("${UMS-URL}")
	private String umsURL;
	
	@Value("${ACCOUNTTYPE}")
	private String accountType;

	@Value("${USERNAME}")
	private String userName;

	@Value("${SPONSORBANKIFCCODE}")
	private String sponsorBankIfcCode;

	@Value("${PRODTYPE}")
	private String prodType;

	@Value("${USERNUMBER}")
	private String userNumber;
	
	@Value("${USERBANKACCOUNT}")
	private String userbankAccount;
	
	
	@Value("${db.url}")
	private String dbUrl;
	
	@Value("${db.user}")
	private String dbuser;
	
	
	@Value("${db.password}")
	private String dbPassword;
	
	@Value("${db.batch}")
	private String dbbatchSize;


	private static final Logger LOGGER = Logger.getLogger(TenantServiceImpl.class);

	// private static int autoIncrement = 1006;

	@Transactional
	public String saveTenant(TenantVO tenantVO, Integer tenantId) {
		String responce = null;
		String userName = (String) httpServletRequest.getSession(false).getAttribute("loginID");
		Tenant tenant;
		Long tenId;
		/*
		 * if (tenantId != null) tenantObj =
		 * tenantDao.findByTenantCode(tenantVO.getTenantCode()); if (tenantObj
		 * == null) {
		 */
		try {
			tenant = new Tenant();
			if (tenantId == null) {
				tenId = storedProcedureDAO.executeStoredProcedure("TENANTID");
				tenant = new Tenant();
				tenant.setCreatedBy(userName);
				tenant.setTenantId(tenId.intValue());
				tenant.setCreatedDate(Calendar.getInstance());
				tenant.setCratedIPAddress(httpServletRequest.getRemoteAddr());
				tenant.setStatus(1);
				tenant.setModifiedDate(Calendar.getInstance());
			} else {
				tenant = findByTenantId(tenantId);
				tenant.setModifiedBy(userName);
				tenant.setModifiedDate(Calendar.getInstance());
				tenant.setModifiedIPAddress(httpServletRequest.getRemoteAddr());
			}
			if (tenantDao.checkTenentCode(tenantVO.getTenantCode()).equalsIgnoreCase("Failure")) {
				return "duplicateTenentCode";
			}
			if (tenantVO.getTenantTypeLov().equalsIgnoreCase("APSFL")) {
				if (tenantDao.checkTenentType().equalsIgnoreCase("Failure")) {
					return "duplicateTenentType";
				}
			}
			tenant.setTenantCode(tenantVO.getTenantCode());
			tenant.setName(tenantVO.getName());
			tenant.setPortalEnrllmentno(tenantVO.getPortalEnrllmentno());
			tenant.setPortalPostalRegno(tenantVO.getPortalPostalRegno());
			/*
			 * if (!tenantVO.getPortalPostExpDate().isEmpty()) {
			 * tenant.setPortalPostExpDate(DateUtill.stringtoDate(tenantVO.
			 * getPortalPostExpDate())); }
			 */
			tenant.setPortalPostExpDate(tenantVO.getPortalPostExpDate());
			tenant.setAadharCardNo(tenantVO.getAadharCardNo());
			tenant.setTenantTypeLov(tenantVO.getTenantTypeLov());
			tenant.setAddress1(tenantVO.getAddress1());
			tenant.setAddress2(tenantVO.getAddress2());
			tenant.setArea(tenantVO.getArea());
			tenant.setCity(tenantVO.getCity());
			tenant.setEmailId1(tenantVO.getEmailId1());
			tenant.setEmailId2(tenantVO.getEmailId2());
			tenant.setFax1(tenantVO.getFax1());
			tenant.setFax2(tenantVO.getFax2());
			tenant.setGstNo(tenantVO.getGstNo());
			tenant.setLandline1(tenantVO.getLandline1());
			tenant.setLandline2(tenantVO.getLandline2());
			tenant.setLocality(tenantVO.getLocality());
			tenant.setLocalOfficeAddress1(tenantVO.getLocalOfficeAddress1());
			tenant.setLocalOfficeAddress2(tenantVO.getLocalOfficeAddress2());
			tenant.setLocalOfficeArea(tenantVO.getLocalOfficeArea());
			tenant.setLocalOfficeCity(tenantVO.getLocalOfficeCity());
			tenant.setLocalOfficeEmailId1(tenantVO.getLocalOfficeEmailId1());
			tenant.setLocalOfficeEmailId2(tenantVO.getLocalOfficeEmailId2());
			tenant.setLocalOfficeFax1(tenantVO.getLocalOfficeFax1());
			tenant.setLocalOfficeFax2(tenantVO.getLocalOfficeFax2());
			tenant.setLocalOfficeLandline1(tenantVO.getLocalOfficeLandline1());
			tenant.setLocalOfficeLandline2(tenantVO.getLocalOfficeLandline2());
			tenant.setLocalOfficeLocality(tenantVO.getLocalOfficeLocality());
			tenant.setLocalOfficePincode(tenantVO.getLocalOfficePincode());
			tenant.setLocalOfficePocMobileNo1(tenantVO.getLocalOfficePocMobileNo1());
			tenant.setLocalOfficePocMobileNo2(tenantVO.getLocalOfficePocMobileNo2());
			tenant.setLocalOfficePocName(tenantVO.getLocalOfficePocName());
			tenant.setLocalOfficeStateName(tenantVO.getLocalOfficeStateName());
			tenant.setLocalOfficestdcode(tenantVO.getLocalOfficestdcode());
			tenant.setPanNo(tenantVO.getPanNo());
			tenant.setPincode(tenantVO.getPincode());
			tenant.setPocMobileNo1(tenantVO.getPocMobileNo1());
			tenant.setPocMobileNo2(tenantVO.getPocMobileNo2());
			tenant.setPocName(tenantVO.getPocName());
			tenant.setStateName(tenantVO.getStateName());
			tenant.setStdcode(tenantVO.getStdcode());
			tenant.setTanNo(tenantVO.getTanNo());
			tenant.setTinNo(tenantVO.getTinNo());
			tenant.setVatNo(tenantVO.getVatNo());
			tenant.setPortalSubstn1Id(tenantVO.getPortalSubstn1Id());
			tenant.setPortalSubstn1Distance(tenantVO.getPortalSubstn1Distance());
			tenant.setPortalSubstn2Id(tenantVO.getPortalSubstn2Id());
			tenant.setPortalSubstn2Distance(tenantVO.getPortalSubstn2Distance());
			tenant.setPortalMsoName(tenantVO.getPortalMsoName());
			tenant.setPortalDasLicenceProvider(tenantVO.getPortalDasLicenceProvider());
			tenant.setPortalRgnmsp1(tenantVO.getPortalRgnmsp1());
			tenant.setPortalRegMsp1AssocYrs(tenantVO.getPortalRegMsp1AssocYrs());
			tenant.setPortalLocmsp1(tenantVO.getPortalLocmsp1());
			tenant.setPortalLocMsp1AssocYrs(tenantVO.getPortalLocMsp1AssocYrs());
			tenant.setPortalRgnMsp2(tenantVO.getPortalRgnMsp2());
			tenant.setPortalRegMsp2AssocYrs(tenantVO.getPortalRegMsp2AssocYrs());
			tenant.setPortalLocMsp2(tenantVO.getPortalLocMsp2());
			tenant.setPortalLocMsp2AssocYrs(tenantVO.getPortalLocMsp2AssocYrs());
			tenant.setPortalRgnMsp3(tenantVO.getPortalRgnMsp3());
			tenant.setPortalRegMsp3AssocYrs(tenantVO.getPortalRegMsp3AssocYrs());
			tenant.setPortalLocMsp3(tenantVO.getPortalLocMsp3());
			tenant.setPortalLocMsp3AssocYrs(tenantVO.getPortalLocMsp3AssocYrs());
			tenant.setPortalRgnMsp4(tenantVO.getPortalRgnMsp4());
			tenant.setPortalRegMsp4AssocYrs(tenantVO.getPortalRegMsp4AssocYrs());
			tenant.setPortalLocMsp4(tenantVO.getPortalLocMsp4());
			tenant.setPortalLocMsp4AssocYrs(tenantVO.getPortalLocMsp4AssocYrs());
			tenant.setPortalRgnMsp5(tenantVO.getPortalRgnMsp5());
			tenant.setPortalRegMsp5Assocyrs(tenantVO.getPortalRegMsp5Assocyrs());
			tenant.setPortalLocMsp5(tenantVO.getPortalLocMsp5());
			tenant.setPortalLocMsp5AssocYrs(tenantVO.getPortalLocMsp5AssocYrs());
			tenant.setPortalRgnMsp6(tenantVO.getPortalRgnMsp6());
			tenant.setPortalRegMsp6Assocyrs(tenantVO.getPortalRegMsp6Assocyrs());
			tenant.setPortalLocMsp6(tenantVO.getPortalLocMsp6());
			tenant.setPortalLocMsp6AssocYrs(tenantVO.getPortalLocMsp6AssocYrs());
			tenant.setPortalDgtConnCnt(tenantVO.getPortalDgtConnCnt());
			tenant.setPortalAnlConnCnt(tenantVO.getPortalAnlConnCnt());
			tenant.setPortalDasLicense(tenantVO.getPortalDasLicense());
			tenant.setPortalDasLicenceType(tenantVO.getPortalDasLicenceType());
			tenant.setPortalDasLicenseHolder(tenantVO.getPortalDasLicenseHolder());
			/*
			 * if (!tenantVO.getPortalDasLicenseExpDate().isEmpty()) {
			 * tenant.setPortalDasLicenseExpDate(DateUtill.stringtoDate(tenantVO
			 * .getPortalDasLicenseExpDate())); }
			 */
			tenant.setPortalDasLicenseExpDate(tenantVO.getPortalDasLicenseExpDate());
			tenant.setPortalPaychnlCnt(tenantVO.getPortalPaychnlCnt());
			tenant.setPortalCompanyType(tenantVO.getPortalCompanyType());
			tenant.setPortalPartnerName(tenantVO.getPortalPartnerName());
			tenant.setPortalHouseHoldCnt(tenantVO.getPortalHouseHoldCnt());
			tenant.setPortalMibLicenseNo(tenantVO.getPortalMibLicenseNo());
			/*
			 * if (!tenantVO.getPortalMibLicenseExpDate().isEmpty()) {
			 * tenant.setPortalMibLicenseExpDate(DateUtill.stringtoDate(tenantVO
			 * .getPortalMibLicenseExpDate())); }
			 */
			tenant.setPortalMibLicenseExpDate(tenantVO.getPortalMibLicenseExpDate());
			tenant = tenantDao.saveTenant(tenant);
			tenantVO.setTenantId(tenant.getTenantId());

			tenantDocumentsService.saveTenantDocument(tenantVO, tenantId);

			tenantLicensesService.saveTenantLicenses(tenantVO, tenant, tenantId);

			tenantBankDetailsService.saveTenantBankDetails(tenantVO, tenantId);

			tenantWalletServiceImpl.saveTenantWallet(tenantVO, tenantId);
			// if(tenantVO.getPassets() != null && !tenantVO.equals(""))
			if (!tenantVO.getPassets().isEmpty()) {
				portalAssetsServiceImpl.savePortalAssets(tenantVO);
			}
			if (tenantVO.getPareas() != null && !tenantVO.equals("")) {
				portalAreaServiceImpl.savePortalAreas(tenantVO);
			}
			responce = "Success";
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl::saveTenants() " + e);
			responce = "Failure";
			e.printStackTrace();
		}
		/*
		 * }else{ responce = "Exists"; }
		 */
		return responce;
	}

	public List<Tenant> findAllTenants() {
		List<Tenant> tenants = new ArrayList<Tenant>();
		tenants = tenantDao.findAllTenants();
		return tenants;
	}

	public Tenant findByTenantCode(String tenantCode) {
		Tenant tenant = new Tenant();
		tenant = tenantDao.findByTenantCode(tenantCode);
		return tenant;
	}

	public List<Tenant> findMspCodeByTenantType() {
		List<Tenant> tenants = new ArrayList<Tenant>();
		tenants = tenantDao.findMspCodeByTenantType();
		return tenants;
	}

	public List<Tenant> findLmoCodeByTenantType(String tenantCode) {
		List<Tenant> tenants = new ArrayList<Tenant>();
		tenants = tenantDao.findLmoCodeByTenantType(tenantCode);
		return tenants;
	}

	public Tenant findByTenantId(Integer tenantId) {
		Tenant tenant = new Tenant();
		tenant = tenantDao.findByTenantId(tenantId);
		return tenant;
	}

	public TenantVO findByTenantId(Integer tenantId, String tenantCode, String Enrollmentno) {
		Tenant tenant;
		TenantWallet tenantWallet;
		TenantVO tenantVO = null;
		TenantBankDetails tenantBankDetails;
		TenantLicenses tenantLicenses;
		List<TenantDocuments> tenantDocuments;
		try {
			tenant = tenantDao.findByTenantId(tenantId);
			tenantBankDetails = tenantBankDetailsService.findByTenantCode(tenantCode);
			tenantLicenses = tenantLicensesService.findByTenantCode(tenantCode);
			tenantWallet = tenantWalletServiceImpl.findByTenantCode(tenantCode);
			tenantDocuments = tenantDocumentsService.findByTenantCode(tenantCode);
			tenantVO = new TenantVO(tenant, tenantBankDetails, tenantLicenses, tenantDocuments, tenantWallet);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tenant = null;
			tenantWallet = null;
			tenantBankDetails = null;
			tenantLicenses = null;
			tenantDocuments = null;
		}
		return tenantVO;
	}

	@Transactional
	public String updateTenantStatus(Tenant tenant, String action, String loginId) {
		String response = "";
		try {
			if (action.equalsIgnoreCase("accept")) {
				tenant.setStatus(2);
				// TmsHelper tmsHelper = new TmsHelper();
				response = tmsHelper.createUser(tenant, ipAddressValues.getUmsURL(), loginId);
				if (response.equalsIgnoreCase("successmail") || response.equalsIgnoreCase("failuremail")) {
					tenantDao.saveTenant(tenant);
					response = "Success";
				} else
					response = "UserCreationFail";
			} else if (action.equalsIgnoreCase("reject")) {
				tenant.setStatus(3);
				tenantDao.saveTenant(tenant);
				response = "Success";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return response;
	}

	public List<TenantDTO> findAllTenantsBYCreatedBy(String loginID, PageObject pageObject) {
		List<Tenant> tenantsList;
		List<TenantDTO> tenantsDtoList = null;
		TenantDTO tenantDTO;
		try {
			tenantsList = tenantDao.findAllTenantsBYCreatedBy(loginID, pageObject);
			tenantsDtoList = new ArrayList<TenantDTO>();
			for (Tenant tenant : tenantsList) {
				tenantDTO = new TenantDTO(tenant);
				if(pageObject != null) {
					tenantDTO.setTotalDisplayCount(pageObject.getTotalDisplayCount());
					tenantDTO.setTenantType(pageObject.getTenantType());
				}
				tenantsDtoList.add(tenantDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tenantsList = null;
			tenantDTO = null;
		}
		return tenantsDtoList;
	}

	public List<TenantDTO> getApprovedLMOTenants(String loginID) {
		List<Tenant> tenantsList;
		List<TenantDTO> tenantsDtoList = null;
		TenantDTO tenantDTO;
		try {
			tenantsList = tenantDao.getApprovedLMOTenants(loginID);
			tenantsDtoList = new ArrayList<TenantDTO>();
			for (Tenant tenant : tenantsList) {
				tenantDTO = new TenantDTO();
				tenantDTO.setTenantName(tenant.getName());
				tenantDTO.setTenantId(tenant.getTenantId());
				tenantsDtoList.add(tenantDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tenantsList = null;
			tenantDTO = null;
		}
		return tenantsDtoList;
	}

	public Map<String, String> getDistrictList(String stateID) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getDistrictList(stateID);
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {

				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			// LOGGER.info(ex.getMessage());
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;
		}
		return map;
	}

	public Map<String, String> getMandalList(String stateID, String districtID) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getMandalList(stateID, districtID);
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			// LOGGER.info(ex.getMessage());
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;
		}
		return map;
	}

	public Map<String, Map<String, String>> getmandalAndSubsList(String stateID, String districtID) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		Map<String, String> maps = new LinkedHashMap<String, String>();
		Map<String, Map<String, String>> bothMandalAndSubstations = null;
		List<Object[]> los;

		List<Object[]> lovs = tenantDao.getMandalList(stateID, districtID);
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);
			}
			los = tenantDao.getSubstationsList(stateID, districtID);
			for (Object[] object : los) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				maps.put(s1, s2);
			}
			bothMandalAndSubstations = new LinkedHashMap<>();
			bothMandalAndSubstations.put("Mandals", map);
			bothMandalAndSubstations.put("Substations", maps);
		} catch (Exception ex) {
			ex.printStackTrace();
			// LOGGER.info(ex.getMessage());
		} finally {
			s1 = null;
			s2 = null;
			map = null;
			maps = null;
			los = null;
		}

		return bothMandalAndSubstations;
	}

	public String getMandalbySubstation(String stateID, String districtID, String substationID) {
		String mandalID = "";
		try {
			mandalID = tenantDao.getMandalbySubstation(stateID, districtID, substationID);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return mandalID;
	}

	public Map<String, String> getVillageList(String stateID, String districtID, String mandalID) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getVillageList(stateID, districtID, mandalID);
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;

		}
		return map;
	}

	public Map<String, String> getcableList() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getcableList();
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;
		}
		return map;
	}

	public Map<String, String> getassetsList() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getassetsList();
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;
		}
		return map;
	}

	public Map<String, String> getstateList() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<Object[]> lovs = tenantDao.getstateList();
		String s1 = "";
		String s2 = "";
		try {
			for (Object[] object : lovs) {
				s1 = object[0] == null ? "" : object[0].toString();
				s2 = object[1] == null ? "" : object[1].toString();
				map.put(s1, s2);

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			// LOGGER.info(ex.getMessage());
		} finally {
			s1 = null;
			s2 = null;
			lovs = null;
		}
		return map;
	}

	@Transactional
	public void saveOltPorts(String jsonString) {
		OLTPortDetails oltPortDetails;
		JSONArray array;
		JSONObject jsonObj;
		try {
			array = new JSONArray(jsonString);
			for (int i = 0; i < array.length(); i++) {
				jsonObj = array.getJSONObject(i);
				System.out.println(jsonObj.getString("tenantCode"));
				System.out.println(jsonObj.getString("oltId"));
				System.out.println(jsonObj.getString("oltPortId"));
				String arr[] = jsonObj.getString("oltPortId").replace("[", "").replace("]", "").replace("\"", "")
						.split(",");
				for (String portId : arr) {
					oltPortDetails = new OLTPortDetails();
					oltPortDetails.setLmocode(jsonObj.getString("tenantCode"));
					oltPortDetails.setPopOltSerialno(jsonObj.getString("oltId"));
					oltPortDetails.setPortNo(Integer.parseInt(portId));
					oltPortDetails.setCardId(1);
					oltPortDetails.setSlots(
							"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
					oltPortDetails = tenantDao.saveOltPorts(oltPortDetails);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			array = null;
			oltPortDetails = null;
			jsonObj = null;
		}
	}

	public List<OLT> getLmoOltLovs(String subStnList) {
		List<OLT> oltLovs = new ArrayList<>();
		try {
			oltLovs = tenantDao.getLmoOltLovs(subStnList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return oltLovs;
	}

	public List<String> getOLTPortIds(String oltId) {
		List<String> oltPortList = new ArrayList<>();
		try {
			oltPortList = tenantDao.getOLTPortIds(oltId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return oltPortList;
	}

	public List<OLT> getLmoOltList(String tenantCode) {
		List<OLT> oltLmoList = new ArrayList<>();
		try {
			oltLmoList = tenantDao.getLmoOltList(tenantCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return oltLmoList;
	}

	public List<Tenant> getTempTenantsbyStatus(String tenantType, int status) {
		List<Tenant> tenantList = new ArrayList<>();
		tenantList = tenantDao.getTempTenantsbyStatus(tenantType, status);
		return tenantList;
	}

	public Map<String, Map<String, Object>> verifyLMO(String lmoCode, String mobileNumber) {

		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		String mob1;
		String mob2;
		Tenant tenant;

		try {

			if (mobileNumber.length() == 10) {
				tenant = tenantDao.findByTenantCode(lmoCode);

				if (tenant != null) {

					mob1 = tenant.getPocMobileNo1();
					mob2 = tenant.getPocMobileNo2();
					if ((mob1 != null || mob2 != null) && (!mob1.equalsIgnoreCase("") || !mob2.equalsIgnoreCase(""))) {
						mob1 = mob1 == null ? "" : mob1;
						mob2 = mob2 == null ? "" : mob2;
						if (mob1.equalsIgnoreCase(mobileNumber) || mob2.equalsIgnoreCase(mobileNumber)) {
							innerMap = new LinkedHashMap<>();
							innerMap.put(Response.statusCode.name(), 201);
							innerMap.put(Response.statusMessage.name(), "Valid LMO");
						} else {
							innerMap = new LinkedHashMap<>();
							innerMap.put(Response.statusCode.name(), 403);
							innerMap.put(Response.statusMessage.name(), "Invalid Mobile Number");
						}
					} else {
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.name(), 402);
						innerMap.put(Response.statusMessage.name(), "Mobile Number does not exist");
					}
				} else {
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.name(), 401);
					innerMap.put(Response.statusMessage.name(), "LMO-ID Not Found");
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.name(), 403);
				innerMap.put(Response.statusMessage.name(), "Invalid Mobile Number");
			}

			returnMap.put(Response.responseStatus.name(), innerMap);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			innerMap = null;
			mob1 = null;
			mob2 = null;
			tenant = null;
		}
		return returnMap;
	}

	@Transactional
	public Map<String, Map<String, Object>> creditLmoWallet(CreditLmoWallet creditLmoWallet) {
		Map<String, Map<String, Object>> returnMap = new LinkedHashMap<>();
		Map<String, Object> innerMap = null;
		String mobileNumber = creditLmoWallet.getMobileNumber();
		String lmoCode = creditLmoWallet.getLmoCode();
		String mob1;
		String mob2;
		Tenant tenant;
		try {
			if (AESEncryption.validateEncryptedValue(creditLmoWallet)) {
				this.validateCreditLmoWallet(creditLmoWallet);
				if (mobileNumber.length() == 10) {
					tenant = tenantDao.findByTenantCode(lmoCode);

					if (tenant != null) {
						mob1 = tenant.getPocMobileNo1();
						mob2 = tenant.getPocMobileNo2();
						if ((mob1 != null || mob2 != null)
								&& (!mob1.equalsIgnoreCase("") || !mob2.equalsIgnoreCase(""))) {
							mob1 = mob1 == null ? "" : mob1;
							mob2 = mob2 == null ? "" : mob2;
							if (mob1.equalsIgnoreCase(mobileNumber) || mob2.equalsIgnoreCase(mobileNumber)) {
								if (creditLmoWallet.getAmountPaid().matches("[0-9]+")) {

									TenantWallet tenantWallet = tenantWalletDao.findByTenantCode(lmoCode);
									tenantWallet = tenantWallet == null ? new TenantWallet() : tenantWallet;
									BigDecimal val = tenantWallet.getWalletAmount() == null ? new BigDecimal("0")
											: tenantWallet.getWalletAmount();
									tenantWallet.setTenantCode(creditLmoWallet.getLmoCode());
									tenantWallet
											.setWalletAmount(val.add(new BigDecimal(creditLmoWallet.getAmountPaid())));
									tenantWallet.setWalletLastcramt(new BigDecimal(creditLmoWallet.getAmountPaid()));
									tenantWallet.setWalletLastcrdate(
											DateUtill.stringtoCalender(creditLmoWallet.getDateOfPayment()));
									tenantWallet.setWalletLastcrmode(creditLmoWallet.getTypeOfPayment());
									tenantWallet.setWalletLastcrid(creditLmoWallet.getTranrefnumber());
									tenantWallet.setStatus(1);
									tenantWallet.setCrLimitAmt(tenantWallet.getCrLimitAmt() == null
											? new BigDecimal("0") : tenantWallet.getCrLimitAmt());
									tenantWallet.setCrlimitLastdbamt(tenantWallet.getCrlimitLastdbamt() == null
											? new BigDecimal("0") : tenantWallet.getCrlimitLastdbamt());
									tenantWallet.setDepositAmount(tenantWallet.getDepositAmount() == null
											? new BigDecimal("0") : tenantWallet.getDepositAmount());
									tenantWallet.setUsedAmt(tenantWallet.getUsedAmt() == null ? new BigDecimal("0")
											: tenantWallet.getUsedAmt());
									tenantWallet.setWalletLastdbamt(tenantWallet.getWalletLastdbamt() == null
											? new BigDecimal("0") : tenantWallet.getWalletLastdbamt());
									tenantWallet.setDepostLastcramt(tenantWallet.getDepostLastcramt() == null
											? new BigDecimal("0") : tenantWallet.getDepostLastcramt());
									tenantWallet.setDepostLastdbamt(tenantWallet.getDepostLastdbamt() == null
											? new BigDecimal("0") : tenantWallet.getDepostLastdbamt());
									tenantWallet.setCreatedDate(Calendar.getInstance());
									tenantWallet.setModifiedDate(Calendar.getInstance());
									tenantWalletDao.saveTenantWallet(tenantWallet);

									TenantsWalletTrans tenantsWalletTrans = new TenantsWalletTrans();
									tenantsWalletTrans.setTrandate(Calendar.getInstance());
									tenantsWalletTrans.setTenantcode(creditLmoWallet.getLmoCode());
									tenantsWalletTrans.setTrantype("Wallet");
									tenantsWalletTrans.setCrdb_flag('C');
									tenantsWalletTrans.setTranamt(new BigDecimal(creditLmoWallet.getAmountPaid()));
									tenantsWalletTrans.setTranmode(creditLmoWallet.getTypeOfPayment());
									tenantsWalletTrans.setTranrefno(creditLmoWallet.getTranrefnumber());
									tenantsWalletTrans.setStatus(new Short("1"));
									tenantsWalletTrans.setCreatedon(Calendar.getInstance());
									tenantsWalletTrans.setModifiedon(Calendar.getInstance());
									cpeDao.saveOrUpdate(tenantsWalletTrans);

									innerMap = new LinkedHashMap<>();
									innerMap.put(Response.statusCode.name(), 201);
									innerMap.put(Response.statusMessage.name(), "Credited");

								} else {
									innerMap = new LinkedHashMap<>();
									innerMap.put(Response.statusCode.name(), 405);
									innerMap.put(Response.statusMessage.name(), "Invalid Amount");
								}
							} else {
								innerMap = new LinkedHashMap<>();
								innerMap.put(Response.statusCode.name(), 403);
								innerMap.put(Response.statusMessage.name(), "Invalid Mobile Number");
							}
						} else {
							innerMap = new LinkedHashMap<>();
							innerMap.put(Response.statusCode.name(), 402);
							innerMap.put(Response.statusMessage.name(), "Mobile Number does not exist");
						}
					} else {
						innerMap = new LinkedHashMap<>();
						innerMap.put(Response.statusCode.name(), 401);
						innerMap.put(Response.statusMessage.name(), "LMO-ID Not Found");
					}
				} else {
					innerMap = new LinkedHashMap<>();
					innerMap.put(Response.statusCode.name(), 403);
					innerMap.put(Response.statusMessage.name(), "Invalid Mobile Number");
				}
			} else {
				innerMap = new LinkedHashMap<>();
				innerMap.put(Response.statusCode.name(), 500);
				innerMap.put(Response.statusMessage.name(), "Invalid Encrypt Value");
			}
		} catch (Exception e) {
			innerMap = new LinkedHashMap<>();
			innerMap.put(Response.statusCode.name(), 500);
			innerMap.put(Response.statusMessage.name(), e.getMessage());
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			returnMap.put(Response.responseStatus.name(), innerMap);
			mob1 = null;
			mob2 = null;
			tenant = null;
			mobileNumber = null;
			lmoCode = null;
		}
		
		return returnMap;
	}

	private void validateCreditLmoWallet(CreditLmoWallet creditLmoWallet) {
		if (creditLmoWallet.getAmountPaid() == null || creditLmoWallet.getAmountPaid().equalsIgnoreCase(""))
			throw new RuntimeException("Amount Is Empty ");
		if (creditLmoWallet.getLmoCode() == null || creditLmoWallet.getLmoCode().equalsIgnoreCase(""))
			throw new RuntimeException("LMO Code Is Empty ");
		if (creditLmoWallet.getMobileNumber() == null || creditLmoWallet.getMobileNumber().equalsIgnoreCase(""))
			throw new RuntimeException("Mobile Number Is Empty ");
		if (creditLmoWallet.getTranrefnumber() == null || creditLmoWallet.getTranrefnumber().equalsIgnoreCase(""))
			throw new RuntimeException("Transaction Reference No Is Empty");
		if (creditLmoWallet.getDateOfPayment() == null || creditLmoWallet.getDateOfPayment().equalsIgnoreCase(""))
			throw new RuntimeException("Date Is Empty");
		/*
		 * if(creditLmoWallet.getDateOfPayment() == null ||
		 * creditLmoWallet.getDateOfPayment().equalsIgnoreCase("")) throw new
		 * RuntimeException("Date Is Empty");
		 */

	}

	public List<CafsForBlockListVO> searchCafDetailsForBlockList(String mobileNo, String stbNo, String tenantcode,
			String tenantType, String stbMac) {
		List<CafsForBlockListVO> cafList = new ArrayList<>();
		List<Object[]> cafInfoList;
		CafsForBlockListVO cafinfoVO;
		try {
			cafInfoList = tenantDao.searchCafDetailsForBlockList(mobileNo, stbNo, tenantcode, tenantType, stbMac);
			for (Object[] object : cafInfoList) {
				cafinfoVO = new CafsForBlockListVO();
				cafinfoVO.setCafno(Long.parseLong(object[0].toString()));
				cafinfoVO.setAadharno(object[1] == null || object[1].toString() == "" ? "NA" : object[1].toString());
				cafinfoVO.setFname(object[2] == null || object[2].toString() == "" ? "NA" : object[2].toString());
				cafinfoVO.setLname(object[3] == null || object[3].toString() == "" ? "NA" : object[3].toString());
				cafinfoVO.setStbslno(object[4] == null || object[4].toString() == "" ? "NA" : object[4].toString());
				cafinfoVO.setCustid(object[5] == null || object[5].toString() == "" ? "NA" : object[5].toString());
				cafinfoVO.setStatus(object[6] == null || object[6].toString() == "" ? "NA" : object[6].toString());
				cafinfoVO.setPhone(object[7] == null || object[7].toString() == "" ? "NA" : object[7].toString());
				cafinfoVO.setStbmac(object[8] == null || object[8].toString() == "" ? "NA" : object[8].toString());
				cafinfoVO.setStbcafno(object[8] == null || object[8].toString() == "" ? "NA" : object[9].toString());
				cafList.add(cafinfoVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			cafInfoList = null;
			cafinfoVO = null;
		}
		return cafList;
	}

	public String  checkCustomer(String stbcafno) {
		String countStatus = null;
		try {
			countStatus = tenantDao.checkCustomer(stbcafno);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
		}
		return countStatus;
	}

	@Transactional
	public void BlackCustomer(BlackListInfoVO blackListInfoVO, String countStatus) {

		BlackListCust blackListCust = new BlackListCust();
		try {
			blackListCust.setStbcafno(blackListInfoVO.getStbcafno());
			blackListCust.setApprovedby(null);
			blackListCust.setApprovedipaddr(null);
			blackListCust.setCreatedipaddr(blackListInfoVO.getCreatedipaddr());
			blackListCust.setApprovedon(Calendar.getInstance());
			blackListCust.setBlacklistflag('N');
			blackListCust.setCreatedby(blackListInfoVO.getCreatedby());
			blackListCust.setEffectivefrom(Calendar.getInstance().getTime());
			blackListCust.setStatus(0);
			blackListCust.setReason(blackListInfoVO.getReason());
			blackListCust.setCreatedon(Calendar.getInstance());
			tenantDao.saveBlackCustomer(blackListCust, countStatus);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			blackListCust = null;
		}
	}

	@Transactional
	public List<BlackListInfoVO> getBlockListedDetails(String loginID) {

		List<BlackListInfoVO> BlockedList = new ArrayList<>();
		List<Object[]> BlockedInfoList;
		BlackListInfoVO blockedinfoVO;
		try {
			BlockedInfoList = tenantDao.getBlockListedDetails(loginID);
			for (Object[] object : BlockedInfoList) {
				blockedinfoVO = new BlackListInfoVO();
				blockedinfoVO.setCustid(object[0] == null ? "NA" : object[0].toString());
				blockedinfoVO.setAffectedcafs(object[1] == null ? "NA" : object[1].toString());
				blockedinfoVO.setStbSlno(object[2] == null ? "NA" : object[2].toString());
				//blockedinfoVO.setEffectivefrom(object[3] == null ? "" : DateUtill.dateToString((Date) object[3]));
				blockedinfoVO.setEffectivefrom(object[3] == null ? "" : object[3].toString()); 
				blockedinfoVO.setReason(object[4] == null ? "NA" : object[4].toString());
				blockedinfoVO.setStatus(object[5] == null ? "NA" : object[5].toString());
				blockedinfoVO.setPhone(object[6] == null ? "NA" : object[6].toString());
				blockedinfoVO.setStbmac(object[7] == null ? "NA" : object[7].toString());
				blockedinfoVO.setApprovedon(object[8] == null ? "NA" : object[8].toString());
				BlockedList.add(blockedinfoVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			BlockedInfoList = null;
			blockedinfoVO = null;
		}
		return BlockedList;
	}

	@Transactional
	public List<BlackListInfoVO> viewDataToapproveBlockList(String loginID) {

		List<BlackListInfoVO> BlockedList = new ArrayList<>();
		List<Object[]> BlockedInfoList;
		BlackListInfoVO blockedinfoVO;
		try {
			BlockedInfoList = tenantDao.viewDataToapproveBlockList(loginID);
			for (Object[] object : BlockedInfoList) {
				blockedinfoVO = new BlackListInfoVO();
				blockedinfoVO.setStbcafno(object[0] == null ? "NA" : object[0].toString());
				//blockedinfoVO.setEffectivefrom(object[1] == null ? "" : DateUtill.dateToString((Date) object[1]));
				blockedinfoVO.setEffectivefrom(object[17] == null ? "" :  object[17].toString());
				blockedinfoVO.setBlacklistflag(object[2] == null ? "NA" : object[2].toString());
				blockedinfoVO.setReason(object[3] == null ? "NA" : object[3].toString());
				blockedinfoVO.setStatus(object[4] == null ? "NA" : object[4].toString());
				blockedinfoVO.setCreatedon(object[5] == null ? "NA" : DateUtill.dateToString((Date) object[5]));
				blockedinfoVO.setCreatedby(object[6] == null ? "NA" : object[6].toString());
				blockedinfoVO.setCreatedipaddr(object[7] == null ? "NA" : object[7].toString());
				//blockedinfoVO.setApprovedon(object[8] == null ? "NA" : DateUtill.dateToString((Date) object[8]));
				blockedinfoVO.setApprovedon(object[18] == null ? "NA" :  object[18].toString());
				blockedinfoVO.setApprovedby(object[9] == null ? "NA" : object[9].toString());
				blockedinfoVO.setApprovedipaddr(object[10] == null ? "NA" : object[10].toString());
				blockedinfoVO.setAadharno(object[11] == null ? "NA" : object[11].toString());
				blockedinfoVO.setPhone(object[12] == null ? "NA" : object[12].toString());
				blockedinfoVO.setStbSlno(object[13] == null ? "NA" : object[13].toString());
				blockedinfoVO.setStbmac(object[14] == null ? "NA" : object[14].toString());
				blockedinfoVO.setCafno(object[15] == null ? "NA" : object[15].toString());
				blockedinfoVO.setNwsubscode(object[16] == null ? "NA" : object[16].toString());
				BlockedList.add(blockedinfoVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			BlockedInfoList = null;
			blockedinfoVO = null;
		}
		return BlockedList;
	}

	@Transactional
	public String MakeApproveBlackList(String effectivefrom, String loginid, String ApprovedIP, String nwsubscode, String stbcafno ) {
		StringBuilder effectivefrom1 = new StringBuilder();
		String[] StringDate;
		String msg = null;
		try {
			StringDate = effectivefrom.split("/");
			effectivefrom1.append(StringDate[2]).append("-").append(StringDate[1]).append("-").append(StringDate[0]);
			tenantDao.MakeApproveBlackList(effectivefrom1.toString(), loginid, ApprovedIP, nwsubscode, stbcafno);
			msg = "success";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "fail";
			LOGGER.error(e.getMessage());
		} finally {
			StringDate = null;
			effectivefrom1 = null;
		}
		return msg;
	}

	@Transactional
	public String blackListCorpusFail(String custid, String effectivefrom, String loginid, String ApprovedIP,
			String affectedcafs) {
		StringBuilder effectivefrom1 = new StringBuilder();
		String[] StringDate;
		String msg = null;
		try {
			StringDate = effectivefrom.split("/");
			effectivefrom1.append(StringDate[2]).append("-").append(StringDate[0]).append("-").append(StringDate[1]);
			tenantDao.blackListCorpusFail(custid, effectivefrom1.toString(), loginid, ApprovedIP, affectedcafs);
			msg = "success";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "fail";
			LOGGER.error(e.getMessage());
		} finally {
			StringDate = null;
			effectivefrom1 = null;
		}
		return msg;
	}

	@Transactional
	public void RejectBlackList(String effectivefrom, String loginid, String ApprovedIP, String stbcafno) {
		StringBuilder effectivefrom1 = new StringBuilder();
		String[] StringDate;
		try {
			StringDate = effectivefrom.split("/");
			effectivefrom1.append(StringDate[2]).append("-").append(StringDate[0]).append("-").append(StringDate[1]);
			tenantDao.RejectBlackList(effectivefrom1.toString(), loginid, ApprovedIP, stbcafno);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			effectivefrom1 = null;
			StringDate = null;
		}
	}

	public List<String> findAllSubStationIdsByTenantCode(String tenantCode) {
		List<String> list = new ArrayList<>();
		try {
			list = tenantDao.findAllSubStationIdsByTenantCode(tenantCode);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
		}
		return list;
	}

	public List<String> getOltPortsByTenantCode(String tenantcode, String oltSerno) {
		List<String> list = new ArrayList<>();
		try {
			list = tenantDao.getOltPortsByTenantCode(tenantcode, oltSerno);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
		}
		return list;
	}

	public List<Object[]> getOltSrlNosByTenantCode(String tenantcode) {

		List<Object[]> list = new ArrayList<>();
		try {
			list = tenantDao.getOltSrlNosByTenantCode(tenantcode);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
		}
		return list;
	}

	@Transactional
	public Map<String, Object> getL1SplitterByOltport(String oltPortNo, String oltSerNo, String tenantCode) {
		List<String> L1PortSize = new ArrayList<>();
		List<String> L3PortIds = new ArrayList<>();
		List<String> L1PortIds = new ArrayList<>();
		String[] array = null;
		Map<String, Object> map = new HashMap<>();
		OLTPortDetails oltPortDetails = null;
		String l1SlotDetails = "";
		String l3SlotDetails = "";
		try {
			oltPortDetails = tenantDao.getL1SplitterByOltport(oltPortNo, oltSerNo, tenantCode);
			if (oltPortDetails.getL1Slots() == null || oltPortDetails.getL1Slots().trim().equalsIgnoreCase("")) {
				L1PortSize = lovsService.findByL1PortSizeByLovName("L1_PORT_SIZE");
			} else {
				l1SlotDetails = oltPortDetails.getL1Slots();
				array = oltPortDetails.getL1Slots().split(",");
				for (String l1Slot : array) {
					if (!l1Slot.contains("-"))
						L1PortIds.add(l1Slot);
				}
				l3SlotDetails = oltPortDetails.getL3Slotsused();
				if(l3SlotDetails!=null) {
					array = oltPortDetails.getL3Slotsused().split(",");
					for (String l3Slot : array) {
						if (!l3Slot.contains("-"))
							L3PortIds.add(l3Slot);
					}
				}
				
			}

			map.put("L1PortSize", L1PortSize);
			map.put("L1PortIds", L1PortIds);
		    map.put("L3PortIds", L1PortIds);	
			map.put("l1SlotDetails", l1SlotDetails);
			map.put("l1SlotDetailsport", l1SlotDetails+"@"+l3SlotDetails);
			map.put("l3SlotsDetails",l3SlotDetails);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			L1PortSize = null;
			L1PortIds = null;
			array = null;
			oltPortDetails = null;
		}
		return map;
	}
	@Transactional
	public List<String> getL2SplitterByL1Value(String tenantCode, String oltPortNo, String oltSerNo) {
		List<String> list = new ArrayList<>();
		OLTPortDetails oltPortDetails = null;
		int size = 0;
		try {
			oltPortDetails = tenantDao.getL1SplitterByOltport(oltPortNo, oltSerNo, tenantCode);
			size = oltPortDetails.getL1Slots().split(",").length;
			size = 128 / size;
			if (size == 1) {
				list.add(String.valueOf(1));
			} else {
				for (int i = 1; i < size; i++) {
					i = i * 2;
					list.add(String.valueOf(i));
					i = i - 1;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
		}
		return list;
	}

	@Transactional
	public List<String> getL1SplitterIdsByOltport(String l1Size, String oltSerNo, String oltPortNo, String tenantCode) {
		OLTPortDetails oltPortDetails = null;
		String values = null;
		List<String> list = new ArrayList<>();
		int size = 0;
		try {
			oltPortDetails = tenantDao.getL1SplitterByOltport(oltPortNo, oltSerNo, tenantCode);
			for (int i = 1; i <= Integer.parseInt(l1Size); i++) {
				if (values == null)
					values = String.valueOf(i);
				else
					values = values + ',' + String.valueOf(i);
			}
			oltPortDetails.setL1Slots(values);
			tenantDao.saveOrUpdate(oltPortDetails);
			size = values.split(",").length;

			for (int i = 1; i <= size; i++) {
				list.add(String.valueOf(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		} finally {
			oltPortDetails = null;
			values = null;
		}
		return list;
	}

	@Transactional
	public String savePortSplitter(String tenantCode, String oltportNo, String oltSrno, String l2PortSize,
			String l1PortId, String l1SlotDtails) {
		OLTPortDetails oltPortDetails = null;
		String values = null;
		String returnVal = null;
		String array[] = null;
		int L1Size = 0;
		int l2Size = 0;
		int l3Size = 0;
		try {
			oltPortDetails = tenantDao.getL1SplitterByOltport(oltportNo, oltSrno, tenantCode);
			if (!l1SlotDtails.contains("-")) {
				oltPortDetails.setL1Slots(l1SlotDtails);
				oltPortDetails = tenantDao.saveOrUpdate(oltPortDetails);
			}
			array = oltPortDetails.getL1Slots().split(",");
			L1Size = array.length;
			l2Size = 128 / L1Size;
			l3Size = l2Size / Integer.parseInt(l2PortSize);

			for (String port : array) {
				if (!port.contains("-")) {
					if (port.equalsIgnoreCase(l1PortId)) {
						port = l1PortId + "-" + l2PortSize + "-" + l3Size;
					}
				}
				if (values == null)
					values = port;
				else
					values = values + ',' + port;
			}
			oltPortDetails.setL1Slots(values);
			tenantDao.saveOrUpdate(oltPortDetails);
			returnVal = "Port Splitted Successfully.";
		} catch (Exception ex) {
			returnVal = ex.getMessage();
			LOGGER.error(ex.getMessage());
		}

		return returnVal;
	}

	@Transactional
	public TmsHelperDTO saveMspChnlsUploadHistory(TmsHelperDTO tmsHelperDTO, String loginID) {
		Map<String, Object> srvcMap = new HashMap<>();
		Map<String, Object> prodMap = new HashMap<>();
		Map<String, Object> prodCompMap = new HashMap<>();
		TmsHelperDTO tmsHelperDTO1 = new TmsHelperDTO();
		UploadHistory uploadHistory = new UploadHistory();
		List<MspChnlsStg> failedList = new ArrayList<>();
		List<String> channelList = new ArrayList<>();
		List<UploadHistory> channelssList;
		String status = "";
		String remarks = "";
		String upldstatus = "";
		String message = "";
		try {
			Long record = (long) 1;
			Long successrecord;
			Long failedrecord;
			Long uploadId = storedProcedureDAO.executeStoredProcedure("UPLOADID");
			uploadHistory.setUploadId(uploadId);
			uploadHistory.setUploadDate(Calendar.getInstance());
			uploadHistory.setFileName(tmsHelperDTO.getFileName());
			uploadHistory.setFileSize(tmsHelperDTO.getFileSize());
			uploadHistory.setNoofCols(Integer.parseInt(tmsHelperDTO.getNoOfCols()));
			uploadHistory.setNoofRows(Long.parseLong(tmsHelperDTO.getNoOfRows()));
			uploadHistory.setStatus(1);
			uploadHistory.setUploadedBy(loginID);
			uploadHistory.setUploadType("MSO CHANNELS");
			tenantDao.saveOrUpdate(uploadHistory);

			if (uploadHistory.getUploadId() != null) {
				for (MspChnlsStg mspChnlsStg : tmsHelperDTO.getMspChnlsStgList()) {
					mspChnlsStg.setChnlcode(mspChnlsStg.getChnlcode());
					mspChnlsStg.setChnlname(mspChnlsStg.getChnlname());
					mspChnlsStg.setMspcode(mspChnlsStg.getMspcode());
					mspChnlsStg.setUploadid(uploadId);
					mspChnlsStg.setUploadrecno(record);
					mspChnlsStg.setPkgname(mspChnlsStg.getMspcode());
					mspChnlsStg.setCreatedon(Calendar.getInstance());
					mspChnlsStg.setCreatedby(tmsHelperDTO.getLoginid());

					if (mspChnlsStg.getChnlcode() == null || mspChnlsStg.getChnlcode() == "") {
						remarks = "Channel Code is Empty";
						status = "ERROR";
						upldstatus = "10";
					} else if (mspChnlsStg.getChnlname() == null || mspChnlsStg.getChnlname() == "") {
						remarks = "Channel Name is Empty";
						status = "ERROR";
						upldstatus = "10";
					} else if (mspChnlsStg.getMspcode() == null || mspChnlsStg.getMspcode() == "") {
						remarks = "MSP Code is Empty";
						status = "ERROR";
						upldstatus = "10";
					} else {
						remarks = "true";
						status = "ACTIVE";
						upldstatus = "01";
					}

					mspChnlsStg.setRemarks(remarks);
					mspChnlsStg.setStatus(status);
					mspChnlsStg.setUpldstatus(upldstatus);
					mspChnlsStg = tenantDao.saveOrUpdate(mspChnlsStg);

					if (mspChnlsStg.getStatus().equalsIgnoreCase("ACTIVE")) {

						AdditionalService srvcsObj = tenantDao.findSrvcByMsoCode(mspChnlsStg.getMspcode());
						if (srvcsObj == null) {
							AdditionalService srvcs = null;
							srvcs = (AdditionalService) srvcMap.get(mspChnlsStg.getMspcode());
							if (srvcs == null) {
								srvcs = new AdditionalService(mspChnlsStg, httpServletRequest);

								ProductsPK pk = new ProductsPK(mspChnlsStg.getMspcode(), mspChnlsStg.getMspcode());
								Products prod = new Products(pk, mspChnlsStg, httpServletRequest);
								prodMap.put(mspChnlsStg.getMspcode(), prod);

								ProdcomponentsPK prodComppk = new ProdcomponentsPK(mspChnlsStg.getMspcode(),
										mspChnlsStg.getMspcode(), mspChnlsStg.getMspcode());
								Prodcomponents prodcomp = new Prodcomponents(prodComppk, mspChnlsStg,
										httpServletRequest);
								prodCompMap.put(mspChnlsStg.getMspcode(), prodcomp);
							} else
								srvcs.setFeaturecodes(srvcs.getFeaturecodes() + "," + mspChnlsStg.getChnlcode());
							srvcMap.put(mspChnlsStg.getMspcode(), srvcs);

						} else {
							mspChnlsStg.setRemarks(
									"Duplicate Entry Package Already Created With :: " + mspChnlsStg.getMspcode());
							mspChnlsStg.setStatus("ERROR");
							mspChnlsStg.setUpldstatus("10");
							mspChnlsStg = tenantDao.saveMspChnlsStage(mspChnlsStg);
							failedList.add(mspChnlsStg);
							channelList.add(mspChnlsStg.getChnlcode() + " " + mspChnlsStg.getChnlname());
						}

					} else {
						failedList.add(mspChnlsStg);
						channelList.add(mspChnlsStg.getChnlcode() + " " + mspChnlsStg.getChnlname());

					}
					record++;
				}

				for (Map.Entry<String, Object> srvc : srvcMap.entrySet()) {
					CorpusResponse corpusResponse = this.corpusSaveCall(srvc.getKey(),
							(AdditionalService) srvc.getValue());
					if (corpusResponse.getResponseStatus().getStatusCode().equalsIgnoreCase("202")) {
						tenantDao.saveOrUpdate((AdditionalService) srvc.getValue());
						Products prod = (Products) prodMap.get(srvc.getKey());
						if (prod != null)
							tenantDao.saveOrUpdate(prod);
						Prodcomponents prodComp = (Prodcomponents) prodCompMap.get(srvc.getKey());
						if (prodComp != null)
							tenantDao.saveOrUpdate(prodComp);
					} else {
						String mspCode = srvc.getKey();
						AdditionalService adds = (AdditionalService) srvc.getValue();
						String channelCodes[] = adds.getFeaturecodes().split(",");
						for (String code : channelCodes) {
							MspChnlsStg mspChnlsStg = tenantDao.getMspChannelsStage(uploadId, code, mspCode);
							mspChnlsStg.setRemarks(corpusResponse.getResponseStatus().getStatusMessage());
							mspChnlsStg.setStatus("ERROR");
							mspChnlsStg.setUpldstatus("10");
							mspChnlsStg = tenantDao.saveMspChnlsStage(mspChnlsStg);
							failedList.add(mspChnlsStg);
							channelList.add(mspChnlsStg.getChnlcode() + " " + mspChnlsStg.getChnlname());
						}
					}
				}
				failedrecord = (long) failedList.size();
				successrecord = (tmsHelperDTO.getMspChnlsStgList().size() - failedrecord);
				uploadHistory.setSuccessRecords(successrecord);
				tenantDao.saveOrUpdate(uploadHistory);
				message = !(failedrecord == 0)
						? successrecord + " MSO Channels(s) Uploaded Successfully and " + failedrecord
								+ " MSO Channels(s) could not be Uploaded"
						: successrecord + " Channels(s) Uploaded Successfully";
				tmsHelperDTO1.setMessage(message);
			}

		} catch (Exception e) {
			tmsHelperDTO1.setMessage(e.getMessage());
			LOGGER.error("TenantServiceImpl::saveMspChnlsUploadHistory() " + e);
			e.printStackTrace();
		} finally {
			channelssList = tenantDao.viewMspChannels();
			tmsHelperDTO1.setUploadHistory(channelssList);
		}
		return tmsHelperDTO1;

	}

	public CorpusResponse corpusSaveCall(String pkeCode, AdditionalService service)
			throws JsonParseException, JsonMappingException, IOException {

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		CorpusResponse corpusResponce = null;

		try {

			CorpusRequest corpusRequest = new CorpusRequest();
			corpusRequest.setPackageCode(pkeCode);
			corpusRequest.setPackageName(pkeCode);
			corpusRequest.setServiceType("LTV");
			corpusRequest.setIsGlobal("false");
			corpusRequest.setPackagePrice("0");
			String srvcFeatArray[] = service.getFeaturecodes().split(",");
			corpusRequest.setServiceIds(srvcFeatArray);

			headers.add("username", corpusUserName);
			headers.add("apikey", corpusApiKey);
			HttpEntity<CorpusRequest> httpEntity = new HttpEntity<CorpusRequest>(corpusRequest, headers);
			String url = corpusCallSave;
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
			String status = response.getBody();
			ObjectMapper mapper = new ObjectMapper();
			corpusResponce = mapper.readValue(status, CorpusResponse.class);
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl::corpusSaveCall() " + e);
			e.printStackTrace();
		}
		return corpusResponce;
	}

	public List<UploadHistory> viewMspChannels() {
		List<UploadHistory> channelList = new ArrayList<>();
		try {
			LOGGER.info("TenantServiceImpl :: viewMspChannels() :: Start");
			channelList = tenantDao.viewMspChannels();
			for (UploadHistory uploadHistory : channelList) {
				SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
				fmt.setCalendar(uploadHistory.getUploadDate());
				String dateFormatted = fmt.format(uploadHistory.getUploadDate().getTime());
				uploadHistory.setFileSize(dateFormatted);
			}
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: viewMspChannels() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return channelList;
	}

	@Transactional
	public List<MspChnlsStg> mspChannelUploadErrors(String uploadId) {
		List<MspChnlsStg> channelErrorList = new ArrayList<>();
		try {
			LOGGER.info("TenantServiceImpl :: mspChannelUploadErrors() :: Start");
			channelErrorList = tenantDao.mspChannelUploadErrors(uploadId);
			for (MspChnlsStg mspChnlsStg : channelErrorList) {
				SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
				fmt.setCalendar(mspChnlsStg.getCreatedon());
				String dateFormatted = fmt.format(mspChnlsStg.getCreatedon().getTime());
				mspChnlsStg.setStatus(dateFormatted);
			}
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: mspChannelUploadErrors() :: " + e);
			e.printStackTrace();
		} finally {

		}
		return channelErrorList;
	}


	public List<MspChnlsStg> viewMspChannelByLoginId(String loginId) {
		List<UploadHistDTO> uploadHist=new ArrayList<>();
		List<MspChnlsStg> mspChnlsStgList=new ArrayList<>();
		
		try {
			uploadHist = tenantDao.getSrvcInfo(loginId);
			for(UploadHistDTO uploadHistDTO:uploadHist)
			{
				MspChnlsStg mspChnlsStg=new MspChnlsStg();
				mspChnlsStg.setUploadid(Long.parseLong(uploadHistDTO.getUploadid()));
				mspChnlsStg.setUploadrecno(Long.parseLong(uploadHistDTO.getUploadrecno()));
				mspChnlsStg.setChnlcode(uploadHistDTO.getChnlcode());
				mspChnlsStg.setChnlname(uploadHistDTO.getChnlname());
				mspChnlsStg.setPkgname(uploadHistDTO.getPkgname());
				mspChnlsStgList.add(mspChnlsStg);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return mspChnlsStgList;
	}

	public List<AdditionalService> getpackageInfo(String loginId) {
		List<AdditionalService> additionalService=new ArrayList<>();
		try {
			additionalService = tenantDao.getpackageInfo(loginId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return additionalService;
	}


	@Transactional
	public void saveOrUpdate(TenantsWalletTransErrors tenantsWalletTransErrors) {
		tenantDao.saveOrUpdate(tenantsWalletTransErrors);
	}
	
	@Transactional
	public TmsHelperDTO executeRevSharing(String revDate) {
		String p_result = "";
		TmsHelperDTO tmsHelperDTO=new TmsHelperDTO();
		try {
			p_result = tenantDao.executeProcessrevshare(revDate);
			if(p_result.equalsIgnoreCase("0"))
				tmsHelperDTO.setMessage("Revenue sharing has been processed");
			else if(p_result.contains("1-"))
				tmsHelperDTO.setMessage("Revenue sharing is already processed");
			else
				tmsHelperDTO.setMessage(p_result);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return tmsHelperDTO;
	}
	
	@Transactional
	public String achFileGenerate(String date,List<RevenueSharingDTO> revenueSharingDTOList) {
		Calendar now = Calendar.getInstance();
		List<String> transList = new ArrayList<>();
		BigDecimal totalAmt = new BigDecimal("0");
		String returnVal = "";
		String dateDDMMYYYY = DateUtill.getDateInDDMMYYYY(Calendar.getInstance().getTime());
		String fileLocation = "/opt/"+date+"_"+now.get(Calendar.DAY_OF_MONTH)+"_"+now.get(Calendar.HOUR_OF_DAY)+"_"+now.get(Calendar.MINUTE)+"_"+now.get(Calendar.SECOND)+"_ACH.txt";
		File fout = new File(fileLocation);
		try{
			if(!revenueSharingDTOList.isEmpty()){
			for(RevenueSharingDTO revenue : revenueSharingDTOList){
				Long trnsId  = storedProcedureDAO.executeStoredProcedure("RSTENANTTRANSID");
				StringBuilder transBuilder = new StringBuilder();
				transBuilder.append(StringUtils.leftPad("23",2,"0")); 						//ACH Transaction Code
				transBuilder.append(StringUtils.rightPad(" ",9," "));						//Control
				transBuilder.append(StringUtils.rightPad(accountType,2,"0"));				//Destination Account Type			
				transBuilder.append(StringUtils.rightPad(" ",3," "));						//Ledger Folio Number
				transBuilder.append(StringUtils.rightPad(" ",15," "));						//Control
				transBuilder.append(StringUtils.rightPad(revenue.getTenantName(),40," ")); 	//Beneficiary Account Holder's Name	
				transBuilder.append(StringUtils.rightPad(" ",9," "));						//Control
				transBuilder.append(StringUtils.rightPad(" ",7," "));						//Control
				transBuilder.append(StringUtils.rightPad(userName,20," "));					//User Name	
				transBuilder.append(StringUtils.rightPad(" ",13," "));						//Control	
				transBuilder.append(StringUtils.leftPad(getGrossAmount(revenue),13,"0"));	//Amount
				transBuilder.append(StringUtils.rightPad(" ",10," "));						//Reserved (ACH Item Seq No.)
				transBuilder.append(StringUtils.rightPad(" ",10," "));						//Reserved (Checksum)
				transBuilder.append(StringUtils.rightPad(" ",1," "));						//Reserved (Flag for success / return)
				transBuilder.append(StringUtils.rightPad(" ",2," "));						//Reserved (Reason Code)
				transBuilder.append(StringUtils.rightPad(revenue.getIfscCode(),11," "));	//Destination Bank IFSC / MICR / IIN
				transBuilder.append(StringUtils.leftPad(revenue.getAccountNo(),35,"0"));	//Beneficiary's Bank Account number
				transBuilder.append(StringUtils.rightPad(sponsorBankIfcCode,11," "));		//Sponsor Bank IFSC / MICR / IIN
				transBuilder.append(StringUtils.leftPad(userNumber,18,"0"));				//User Number
				transBuilder.append(StringUtils.leftPad(trnsId.toString(),30,"0"));			//Transaction Reference
				transBuilder.append(StringUtils.rightPad(prodType,3," "));					//Product Type
				transBuilder.append(StringUtils.rightPad(" ",15,"0"));						//Beneficiary Aadhaar Number
				transBuilder.append(StringUtils.rightPad(" ",20," "));						//UMRN
				transBuilder.append(StringUtils.rightPad(" ",7," "));						//Filler
				totalAmt.add(new BigDecimal(getGrossAmount(revenue)));
				transList.add(transBuilder.toString());
				RsMonthlyOut rsMonthlyOut = new RsMonthlyOut(trnsId,revenue,date);
				tenantDao.saveOrUpdate(rsMonthlyOut);
				tenantDao.updateRsMonIn(revenue);
			}
			
			StringBuilder headerBuilder = new StringBuilder();
			headerBuilder.append(StringUtils.leftPad("12",2,"0")); 						//ACH transaction code
			headerBuilder.append(StringUtils.rightPad(" ",7," "));						//Control
			headerBuilder.append(StringUtils.rightPad(userName,40," "));				//User Name			
			headerBuilder.append(StringUtils.rightPad(" ",14," "));						//Control
			headerBuilder.append(StringUtils.leftPad(date,9,"0"));						//ACH File Number
			headerBuilder.append(StringUtils.rightPad(" ",9," ")); 						//Control
			headerBuilder.append(StringUtils.rightPad(" ",15," "));						//Control
			headerBuilder.append(StringUtils.rightPad(" ",3," "));						//Ledger Folio Number
			headerBuilder.append(StringUtils.leftPad("0",13,"0"));						//User Defined limit for individual items
			headerBuilder.append(StringUtils.leftPad(totalAmt.toString(),13,"0"));		//Total Amount in paise (Balancing Amount)
			headerBuilder.append(StringUtils.leftPad(dateDDMMYYYY,8,"0"));				//Settlement Date (DDMMYYYY)
			headerBuilder.append(StringUtils.rightPad(" ",10," "));						//Reserved (kept blank by user)
			headerBuilder.append(StringUtils.rightPad(" ",10," "));						//Reserved (kept blank by user)
			headerBuilder.append(StringUtils.rightPad(" ",3," "));						//Filler
			headerBuilder.append(StringUtils.rightPad(userNumber,18," "));				//User Number
			headerBuilder.append(StringUtils.rightPad(date,18," "));					//User Reference
			headerBuilder.append(StringUtils.leftPad(sponsorBankIfcCode,11,"0"));		//Sponsor Bank IFSC / MICR / IIN
			headerBuilder.append(StringUtils.rightPad(userbankAccount,35," "));			//User's Bank Account Number
			headerBuilder.append(StringUtils.rightPad(String.valueOf(transList.size()),9,"0"));				//Total Items
			headerBuilder.append(StringUtils.leftPad(" ",2," "));						//Settlement Cycle (Kept blank by User)
			headerBuilder.append(StringUtils.rightPad(" ",57," "));						//Filler
			
		
			
			
				try(FileOutputStream fos = new FileOutputStream(fout);
						BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos))){
						bw.write(headerBuilder.toString());
						bw.newLine();
					for(String trans : transList){
						bw.write(trans);
						bw.newLine();
					}
					returnVal = "New File Is Generated Successfully With Tenants Size :: "+revenueSharingDTOList.size();
				}catch(Exception e){
					LOGGER.error("TenantServiceImpl :: excelDownload() :: " + e);
					returnVal = "File Is Not Generated. Reason Is :: " + e.getMessage();
				}
			}else{
				returnVal = "File Is Not Generated.";
			}
			
		}catch(Exception e){
			LOGGER.error("TenantServiceImpl :: excelDownload() :: " + e);
		}
		return returnVal;
	}

	
	public String getGrossAmount(RevenueSharingDTO res){
		BigDecimal shareAmt = new BigDecimal(res.getShareamt());
		BigDecimal entTaxAmt = new BigDecimal(res.getEnttax());
		
		return shareAmt.subtract(entTaxAmt).toString();
	}

	public List<RevenueSharingDTO> getRevenueSharingDTOList(String revDate) {
		List<RevenueSharingDTO> revenueSharingDTOList = new ArrayList<>();
		try {
			revenueSharingDTOList = tenantDao.getRevSharingList(revDate);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return revenueSharingDTOList;
	}
	
	public List<RevenueSharingDTO> getRevenueSharingList(String revDate) {
		List<RevenueSharingDTO> revenueSharingDTOList = new ArrayList<>();
		try {
			revenueSharingDTOList = tenantDao.getRevenueSharingList(revDate);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
		return revenueSharingDTOList;
	}

	public List<Districts> getAllDistricts() {
		List<Districts> districtsList = new ArrayList<>();
		try {
			districtsList = tenantDao.findAllDistricts();
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return districtsList;
	}

	public List<Mandals> getMandalsByDistrictId(Integer districtId) {
		List<Mandals> MandalsList = new ArrayList<>();
		try {
			MandalsList = tenantDao.getMandalsByDistrictId(districtId);
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: getMandalsByDistrictId()" + e);
			e.printStackTrace();
		} finally {

		}
		return MandalsList;
	}

	public List<OLT> searchOltsDetails(Integer districtuid, Integer mandalslno) {
		List<OLT> oltList = new ArrayList<OLT>();
		List<Object[]> resultOltList = new ArrayList<>();
		try {
			resultOltList = tenantDao.searchOltsDetails(districtuid, mandalslno);
			for (Object[] list : resultOltList) {
				OLT olt = new OLT();
				olt.setSubstnuid(list[0].toString());
				olt.setSubstnname(list[1].toString());
				olt.setPopOltSerialno(list[2].toString());
				olt.setSubscriberCount(list[3].toString());
             /*   olt.setCreatedon(list[4].toString());
				olt.setCreatedby(list[5].toString());
				olt.setModifiedon(list[6].toString());*/
				oltList.add(olt);
			}
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: searchOltsDetails" + e);
			e.printStackTrace();
		} finally {
			resultOltList = null;
		}
		return oltList;
	}

	public TenantVO getOltDetails(String suid, String oltno) {
		TenantVO tenantVO = new TenantVO();
		List<Object[]> tenantList = new ArrayList<>();
		List<Object[]> portList = new ArrayList<>();
		List<Tenant> tenantList1 = new ArrayList<>();
		List<OLTPortDetails> portList1 = new ArrayList<>();
		Map<String, String> portSubscriberCount = new LinkedHashMap<String, String>();
		Map<String, String> Createdon = new LinkedHashMap<String, String>();
		Map<String, String> Createdby = new LinkedHashMap<String, String>();
		Map<String, String> Modifiedon = new LinkedHashMap<String, String>();
		Map<String, String> portLmoCode = new LinkedHashMap<String, String>();
		//DateFormat df = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		int cardNo = 0;

		try {
			tenantList = tenantDao.getTenantDetailBySubstnUid(suid);

			for (Object[] list : tenantList) {
				Tenant tenant = new Tenant();
				tenant.setTenantCode(list[0].toString());
				tenant.setName(list[1].toString());
				tenantList1.add(tenant);
			}
			portList = tenantDao.getProtDtlsByOltSrlNo(oltno);
			cardNo = tenantDao.getCardNo(oltno);
			//if(cardNo > 0 && cardNo <= 8) {
				portSubscriberCount.put("1", "");
				portSubscriberCount.put("2", "");
				portSubscriberCount.put("3", "");
				portSubscriberCount.put("4", "");
				portSubscriberCount.put("5", "");
				portSubscriberCount.put("6", "");
				portSubscriberCount.put("7", "");
				portSubscriberCount.put("8", "");
				
				
			//}
			
			if(cardNo > 90 && cardNo <= 110) {
				portSubscriberCount.put("9", "");
				portSubscriberCount.put("10", "");
				portSubscriberCount.put("11", "");
				portSubscriberCount.put("12", "");
				portSubscriberCount.put("13", "");
				portSubscriberCount.put("14", "");
				portSubscriberCount.put("15", "");
				portSubscriberCount.put("16", "");
			}
			
			

			for (String key : portSubscriberCount.keySet()) {
				for (Object[] list : portList) {
					OLTPortDetails oLTPortDetails = new OLTPortDetails();
					oLTPortDetails.setPopOltSerialno(list[0] == null ? "" : list[0].toString());
					oLTPortDetails.setSlots(list[1] == null ? "" : list[1].toString());
					oLTPortDetails.setCreatedOn(list[3].toString());
					oLTPortDetails.setCreatedBy((String) list[5]);
					oLTPortDetails.setModifiedOn(list[4].toString());
					String portNo = oLTPortDetails.getPopOltSerialno();
					String subsCount = oLTPortDetails.getSlots();
					

					if (portNo.equalsIgnoreCase(key)) {
						portSubscriberCount.put(key, subsCount);
						Createdon.put(key, oLTPortDetails.getCreatedOn());
						Createdby.put(key, oLTPortDetails.getCreatedBy());
						Modifiedon.put(key,oLTPortDetails.getModifiedOn());
						
						
						break;
					} else {
						portSubscriberCount.put(key, "0");
					}
				}
			}

			portLmoCode.put("1", "");
			portLmoCode.put("2", "");
			portLmoCode.put("3", "");
			portLmoCode.put("4", "");
			portLmoCode.put("5", "");
			portLmoCode.put("6", "");
			portLmoCode.put("7", "");
			portLmoCode.put("8", "");
			
			if(cardNo > 90 && cardNo <= 110) {
				portLmoCode.put("9", "");
				portLmoCode.put("10", "");
				portLmoCode.put("11", "");
				portLmoCode.put("12", "");
				portLmoCode.put("13", "");
				portLmoCode.put("14", "");
				portLmoCode.put("15", "");
				portLmoCode.put("16", "");
			}

			for (String key : portLmoCode.keySet()) {
				for (Object[] list : portList) {
					OLTPortDetails oLTPortDetails = new OLTPortDetails();
					oLTPortDetails.setPopOltSerialno(list[0] == null ? "" : list[0].toString());
					oLTPortDetails.setLmocode(list[2] == null ? "" : list[2].toString());
					String portNo = oLTPortDetails.getPopOltSerialno();
					String lmocode = oLTPortDetails.getLmocode();

					if (portNo.equalsIgnoreCase(key)) {
						portLmoCode.put(key, lmocode);
						break;
					} else {
						portLmoCode.put(key, "");
					}
				}
			}

			tenantVO.setoLTPortDetails(portList1);
			tenantVO.setTenantList(tenantList1);
			tenantVO.setPortSubscriberCount(portSubscriberCount);
			tenantVO.setPortCreatedOn(Createdon);
			tenantVO.setPortCreatedBy(Createdby);
			tenantVO.setPortModifiedOn(Modifiedon);
			tenantVO.setPortLmoCode(portLmoCode);

		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: getOltDetails" + e);
			e.printStackTrace();
		} finally {
			tenantList = null;
		}
		return tenantVO;
	}

	@Transactional
	public String savePortDetailsList(List<OLTPortDetails> oLTPortDetails, String loginID) {
		String success = "";
		String oltSerialNo = "";
		//DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		try {
			
			
			for (OLTPortDetails oLTPortDetail : oLTPortDetails) {
				oltSerialNo = oLTPortDetail.getPopOltSerialno();
				int cardId = tenantDao.getCardId(oltSerialNo);
				if (oLTPortDetail.getLmocode() != null && oLTPortDetail.getLmocode() != "") {
					OLTPortDetails olt = new OLTPortDetails();
					olt = tenantDao.getTenantByPortNo(oLTPortDetail.getPopOltSerialno(),
							oLTPortDetail.getPortNo().toString());
					if (olt.getLmocode() == null) {

						OLTPortDetails oLTDetails = new OLTPortDetails();
						oLTDetails.setPopOltSerialno(oLTPortDetail.getPopOltSerialno());
						oLTDetails.setLmocode(oLTPortDetail.getLmocode());
						oLTDetails.setPortNo(oLTPortDetail.getPortNo());
						
						//oLTDetails.setCardId(1);
						oLTDetails.setCardId(cardId);
						oLTDetails.setSlots(
								"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
						oLTDetails.setCreatedBy(loginID);

						oLTDetails.setCreatedOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						if (null==oLTPortDetail.getModifiedOn())
							oLTDetails.setModifiedOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						else
							oLTDetails.setModifiedOn(oLTPortDetail.getModifiedOn());
						
						olt = tenantDao.saveOltPorts(oLTDetails);
						success = "Saved Successfully";

					}

					else if (olt.getLmocode() != "" && Integer.parseInt(oLTPortDetail.getSlots()) == 0) {
						OLTPortDetails oLTDetails = new OLTPortDetails();
						oLTDetails.setPopOltSerialno(oLTPortDetail.getPopOltSerialno());
						oLTDetails.setLmocode(oLTPortDetail.getLmocode());
						oLTDetails.setPortNo(oLTPortDetail.getPortNo());
						//oLTDetails.setCardId(1);
						oLTDetails.setCardId(cardId);
						oLTDetails.setSlots(
								"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
						
						oLTDetails.setModifiedBy(loginID);
						oLTDetails.setModifiedOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						if (null==oLTPortDetail.getCreatedOn())
							oLTDetails.setCreatedOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						else
							oLTDetails.setCreatedOn(oLTPortDetail.getCreatedOn());
						if (null!=oLTPortDetail.getCreatedBy())
							oLTDetails.setCreatedBy(oLTPortDetail.getCreatedBy());
						tenantDao.saveOrUpdate(oLTDetails);
						success = "Updated Successfully";

					}
				}
			}

		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: savePortDetailsList" + e);
			e.printStackTrace();
		} finally {
		}
		return success;

	}
	
	public List<Tenant> getAllMspTenants() {
		List<Tenant> tenantList = new ArrayList<>();
		try {
			tenantList = tenantDao.findAllByTenantType(String.valueOf(ComsEnumCodes.MSP_Tenant_Type.getCode()));
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return tenantList;
	}
	
	public List<Tenant> getAllLmoTenants() {
		List<Tenant> tenantList = new ArrayList<>();
		try {
			tenantList = tenantDao.findAllByTenantType(String.valueOf(ComsEnumCodes.LMO_Tenant_Type.getCode()));
		} catch (Exception e) {
			LOGGER.error("TenantServiceImpl :: getAllDistricts()" + e);
			e.printStackTrace();
		} finally {

		}
		return tenantList;
	}
	
	public List<CpeStockVO> getCPEStockDetails(String fromDate, String toDate, String tenantType, String tenantCode, String cpeType,String cpeSerialNumber,List<String> cpeSearchStockList) {
		List<CpeStockVO> cpeList = new ArrayList<CpeStockVO>();
		List<Object[]> resultcpeList = new ArrayList<>();
		boolean exists=false;
		try {
			resultcpeList = tenantDao.getCpeDetails(fromDate,toDate,tenantType,tenantCode,cpeType,cpeSerialNumber, cpeSearchStockList);
			for (Object[] list : resultcpeList) {
				CpeStockVO cpe = new CpeStockVO();
				
				cpe.setCpeSrlno(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
				cpe.setMacAddress(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
				cpe.setProfileId(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
				cpe.setMspCode(list[5] == null || list[5].toString() == "" ? "NA" : list[5].toString());
				cpe.setLmoCode(list[6] == null || list[6].toString() == "" ? "NA" : list[6].toString());
				cpe.setCpeTypeLov(list[8] == null || list[8].toString() == "" ? "NA" : list[8].toString());
				cpe.setCpeProfileName(list[9] == null || list[9].toString() == "" ? "NA" : list[9].toString());
				cpe.setDistrictName(list[10] == null || list[10].toString() == "" ? "NA" : list[10].toString());
				cpe.setMandalName(list[11] == null || list[11].toString() == "" ? "NA" : list[11].toString());
				cpe.setVillageName(list[12] == null || list[12].toString() == "" ? "NA" : list[12].toString());
				cpe.setCpeModel(list[13] == null || list[13].toString() == "" ? "NA" : list[13].toString());
				cpe.setCpeBatchDate(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
				cpe.setStatus(list[7] == null || list[7].toString() == "" ? "" : list[7].toString());
				cpe.setCpeCafNo(list[14] == null || list[14].toString() == "" ? "" : list[14].toString());
				cpe.setTenantCode(list[15] == null || list[15].toString() == "" ? "" : list[15].toString());
			
				//added  to remove duplicates for cpe stock with both mso and lmo code filled
			 for (int j = 0; j < cpeList.size(); j++) {
						  
						  if (cpe.getCpeSrlno().equals(cpeList.get(j).getCpeSrlno()))
					   {
							  if (cpe.getTenantCode().contains("MSO")){
								  exists=true;  
							  break;
							  }else
							  {
								  cpeList.remove(j);
								  exists=false;
								  break;
							  }
							 
								  
					   }else
						   exists=false;
						  
					}
			 if (!exists)
				 cpeList.add(cpe); 

			}
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: getCPEStockDetails" + e);
			e.printStackTrace();
		} finally {
			resultcpeList = null;
		}
		return cpeList;
	}
	
	@Transactional
	public String updateCpeDetails(List<CpeStockVO> cpeStockList,String tenantCode) {
		String success = "";
		
		try {
			LOGGER.info("TenantServiceImpl :: updateCpeDetails() :: START");
			for(CpeStockVO cpeStockVO : cpeStockList)
			{
				Tenant tenant = tenantDao.findByTenantCode(cpeStockVO.getTenantCode());
				if(tenant.getTenantTypeLov().equalsIgnoreCase(String.valueOf(ComsEnumCodes.LMO_Tenant_Type.getCode()))){
					tenantDao.updateLMOCpeDetails(cpeStockVO.getCpeSrlno() , cpeStockVO.getTenantCode(),tenantCode);
					success = "UPDATED SUCCESSFULLY";
				}
				else if(tenant.getTenantTypeLov().equalsIgnoreCase(String.valueOf(ComsEnumCodes.MSP_Tenant_Type.getCode()))){
					tenantDao.updateMSPCpeDetails(cpeStockVO.getCpeSrlno() , cpeStockVO.getTenantCode(),tenantCode);
					success = "UPDATED SUCCESSFULLY";
				}
			}
			LOGGER.info("TenantServiceImpl :: updateCpeDetails() :: END");
		}
		catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: updateCpeDetails" + e);
			e.printStackTrace();
		} finally {
		}
		return success;

	}
	
	
	@Transactional
	public String deleteCpeList(CpeHelperDTO cpeHelperDto, List<CpeStockVO> cpeStockList,String loginid) {
		String response = "";
		
		Tenant deletedBy = null;
		List<CpeStock> cpeList = new ArrayList<>();
		List<CpeStock> cpeCafGeneratedList = new ArrayList<>();
		
		
		try{
			LOGGER.info("TenantServiceImpl :: deleteCpeList() :: START");
			
			deletedBy = tenantDao.findByTenantCode(cpeHelperDto.getTenantCode());
			
			
			
			if(deletedBy.getTenantTypeLov().equalsIgnoreCase("APSFL") || deletedBy.getTenantTypeLov().equalsIgnoreCase("APFGC")){
				
				for(CpeStockVO cpeStock :cpeStockList ){
					
					CpeStock cpeStockObj = cpeDao.getCpeByCpeSnoAndMac(cpeStock.getCpeSrlno().trim(),cpeStock.getMacAddress().trim());
					
					 if ((cpeStockObj.getCafNo()==null  && (cpeStockObj.getStatus() !=4 ) ))    {//|| (cpeStockObj.getStatus()==99)  //if caf is assigned status is 4
						 cpeList.add(cpeStockObj);
			
					 }
					 else
					 {
					 cpeCafGeneratedList.add(cpeStockObj);
					 LOGGER.info("deleteCpeList() :: " +cpeStockObj.getCpeslno()+ " is not deleted because caf is generated:: ");
					 }
				}
				if (cpeCafGeneratedList.size() == 0){
					
					cpeDao.deleteCpeList(cpeList,loginid);
					LOGGER.info("deleteCpeList() :: items deleted by:: "+loginid);
					response=cpeList.size()+" items deleted";
					
				}else
				{
					
					response=	" Failed To Delete as " +cpeCafGeneratedList.size()+"  item(s) have Caf generated";
					
				}
				
				
				
			}else if(deletedBy.getTenantTypeLov().equalsIgnoreCase("MSP") || deletedBy.getTenantTypeLov().equalsIgnoreCase("MSO")){
				//may add funtionality in future
					
			}
			
			cpeHelperDto.setStatus(response);
			
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
			response = ex.getMessage();
		}  
		
		return cpeHelperDto.getStatus();
	  }
	

	
	
	public List<CustomerInvDtlsDTO> UpdateCustInvList(String invyr, String invmn, String monthStartdate, String MonthEndDate) {
		List<CustomerInvDtlsDTO> custinvDtls = new ArrayList<CustomerInvDtlsDTO>();
		List<CustomerInvDtlsDTO> custinvDtls2= new ArrayList<CustomerInvDtlsDTO>();
	//	List<CustomerInvDtlsDTO> custinvtempDtls = new ArrayList<CustomerInvDtlsDTO>();
		List<RevenueSharingTemplateDetails> rsTemplateDtlsList = new ArrayList<RevenueSharingTemplateDetails>();
		List<Object[]> resultinvList = new ArrayList<>();
		
		BigDecimal apsflShare = null;
		BigDecimal lmoShare=null;
		BigDecimal msoShare=null;
		List<Object[]> productCodes = new ArrayList<>();
		// Map<String,String> tmplCodes = null;
		List<Object[]> custInvdateWithInvDates = new ArrayList<>();
		boolean isExistsProdCode=false;
		String tmplcode="";
		String msocode="";
        String [] arrOfStr;
        
       
	
		
		LOGGER.info("TenantServiceImpl :: UpdateCustInvList Start");
		productCodes=revenueSharingDaoImpl.getTemplateCodefromProductCode();
		
		LOGGER.info("TenantServiceImpl :: UpdateCustInvList received productcodes");
		
		
		
		try {
			resultinvList = tenantDao.getCustInvList(monthStartdate,MonthEndDate);
			for (Object[] list : resultinvList) {
				CustomerInvDtlsDTO custinv = new CustomerInvDtlsDTO();
				
				custinv.setAcctcafno(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
				custinv.setChargeamt(list[6] == null || list[6].toString() == "" ? "NA" : list[6].toString());
				custinv.setChargecode(list[5] == null || list[5].toString() == "" ? "NA" : list[5].toString());
				custinv.setCustinvno(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
				custinv.setInvno(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
				custinv.setPmntcustid(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
				custinv.setProdcode(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
				custinv.setSrvctax(list[7] == null || list[7].toString() == "" ? "NA" : list[7].toString());
				
				custinvDtls.add(custinv);
			}
			//custinvtempDtls.addAll(custinvDtls);
			rsTemplateDtlsList=  revenueSharingDaoImpl.getRevPercList();
			custInvdateWithInvDates=tenantDao.getCustInvListWithDates(invyr,invmn);
			LOGGER.info("TenantServiceImpl :: setting lmocode and invoice dates");
			
				for (CustomerInvDtlsDTO custinv : custinvDtls){
					for (Object[] list : custInvdateWithInvDates) {
					if (list[0] != null || list[0].toString() != "" ){
					if (custinv.getCustinvno().equals(list[0].toString())){
						if (list[1] != null ){
								if (list[1].toString() != "")
						custinv.setInvfromdate(list[1].toString());
								
						}
						if (list[2] != null)
						{ if( list[2].toString() != "")
							custinv.setInvtodate(list[2].toString());
						}
						if (list[3] != null )
						{ if (list[3].toString() != "")
							custinv.setInvyr(list[3].toString());
						}
						if (list[4] != null )
						{
							if( list[4].toString() != "")
							custinv.setInvmn(list[4].toString());
						}
						if (list[5] != null )
						{ 
							if( list[5].toString() != "")
							custinv.setLmocode(list[5].toString());
						}
						if (list[6]!=null && list[6]!="" && list[6].equals("GOVT")){
			            	 break;
			             }else{
						custinvDtls2.add(custinv);
						break;
			             }
						
						
						
						/*if (list[6] != null )
						{ if ( list[6].toString() != "")
							custinv.setEnttype(list[6].toString());
						}
						
						if (list[7] != null )
						{ if ( list[7].toString() != "")
							custinv.setCusttype(list[7].toString());
						
						
						}*/
						
						}
					
				}
				}
			}
			
			
			LOGGER.info("TenantServiceImpl :: UpdateCustInvList calculating revenue share amounts");		
			
			for (CustomerInvDtlsDTO custinv : custinvDtls2  ){
				apsflShare=apsflShare.ZERO;
				lmoShare=lmoShare.ZERO;
				msoShare=msoShare.ZERO;
			//	productCodes=revenueSharingDaoImpl.getTemplateCodeCode(custinv.getProdcode(),custinv.getLmocode());
				
				//if (tmplCodes.containsKey(custinv.getProdcode())){
				for (Object[] revObjects : productCodes)
				{
					
					
					if (revObjects[0]!=null && null!=custinv.getProdcode()&& revObjects[0].toString().equals(custinv.getProdcode())){
						if (null!=custinv.getLmocode()){
	
						if (revObjects[1]!=null  && revObjects[1].toString().contains(custinv.getLmocode())){
							arrOfStr = (revObjects[1].toString().split(","));

					        for (String a : arrOfStr)
					        {
					        	if (a.trim().startsWith("MSO")){
					        		msocode=a.substring(0, a.length()-1).trim();
					        		break;
					        	}
					        }
					       
							isExistsProdCode=true;
							if (revObjects[2]!=null){
					        tmplcode=revObjects[2].toString();
					        break;
							}
						}
						}
					}else 
					{
						isExistsProdCode=false;
						
						if (revObjects[1]!=null  && null!=custinv.getLmocode()&& revObjects[1].toString().contains(custinv.getLmocode())){
							arrOfStr = (revObjects[1].toString().split(","));

					        for (String a : arrOfStr)
					        {
					        	if (a.trim().startsWith("MSO")){
					        		msocode=a.substring(0, a.length()-1).trim();
					        		break;
					        	}
					        }
					       
							isExistsProdCode=true;
							if (revObjects[2]!=null){
					        tmplcode=revObjects[2].toString();
					        break;
							}
						} 
						
					}	
						
				  
				}	
				
			

				
				custinv.setMsocode(msocode);
				if (isExistsProdCode==true){
					if (custinv.getChargecode().equals("SRVCRENT")) {
						
					for (RevenueSharingTemplateDetails revSharingtmplt : rsTemplateDtlsList){
						if (revSharingtmplt.getRstmplCode().equals(tmplcode)){
							if (revSharingtmplt.getTenanttype().equals("APSFL")){
								apsflShare=revSharingtmplt.getRevshareperc();
								
							}else if (revSharingtmplt.getTenanttype().equals("LMO")){
								lmoShare=revSharingtmplt.getRevshareperc();
								
							}else if (revSharingtmplt.getTenanttype().equals("MSP")){
								msoShare=revSharingtmplt.getRevshareperc();
							
						    }
						
					    }
					}
					
				    
					
					BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
					
					apsflShare=  apsflShare.multiply(tmpchargeamt);
					lmoShare=  lmoShare.multiply(tmpchargeamt);
					
					msoShare=  msoShare.multiply(tmpchargeamt);
					apsflShare = apsflShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					lmoShare = lmoShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					msoShare = msoShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					
				     
					
					}else if (custinv.getChargecode().equals("CPEEMI")||custinv.getChargecode().equals("CPECOST") || custinv.getChargecode().equals("STBCOST")
							|| custinv.getChargecode().equals("SRVCDEP") || custinv.getChargecode().equals("LATEPMNT")){
					
						BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
						
						apsflShare=tmpchargeamt;
					}else if (custinv.getChargecode().equals("LOCALUSAGE") ||custinv.getChargecode().equals("STDUSAGE") ||custinv.getChargecode().equals("ISDUSAGE") ){
						for (RevenueSharingTemplateDetails revSharingtmplte : rsTemplateDtlsList){
						
                            if (revSharingtmplte.getRstmplCode().equals("Basic_voip_service")){
								if (revSharingtmplte.getTenanttype().equals("APSFL")){
									apsflShare=revSharingtmplte.getRevshareperc();
									
								}else if (revSharingtmplte.getTenanttype().equals("LMO")){
									lmoShare=revSharingtmplte.getRevshareperc();
									
								}else if (revSharingtmplte.getTenanttype().equals("MSP")){
									msoShare=revSharingtmplte.getRevshareperc();
								
							    }
							
						    }
						
					    }
								
						
						BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
						
						apsflShare=  apsflShare.multiply(tmpchargeamt);
						lmoShare=  lmoShare.multiply(tmpchargeamt);
						
						msoShare=  msoShare.multiply(tmpchargeamt);
						apsflShare = apsflShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						lmoShare = lmoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						msoShare = msoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						
						
					}
					
				}else if (custinv.getChargecode().equals("CPEEMI")||custinv.getChargecode().equals("CPECOST") || custinv.getChargecode().equals("STBCOST")
						|| custinv.getChargecode().equals("SRVCDEP") || custinv.getChargecode().equals("LATEPMNT")){
				
					BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
					
					apsflShare=tmpchargeamt;
					custinv.setLmocode("");
				}else if (custinv.getChargecode().equals("INSTALLATION")||custinv.getChargecode().equals("ENTTAX") || custinv.getChargecode().equals("VOD")){
					apsflShare=apsflShare.ZERO;
					lmoShare=lmoShare.ZERO;
					msoShare=msoShare.ZERO;
					
				}
				custinv.setApsflInvshare(String.valueOf(apsflShare));
				custinv.setLmoInvshare(String.valueOf(lmoShare));
				custinv.setMsoInvshare(String.valueOf(msoShare));
				
			//	tenantDao.updateInvamtSharedtls(custinv,apsflShare,lmoShare,msoShare);
				msocode="";
				 tmplcode="";

			}
			tenantDao.insertInvamtShare(custinvDtls2,dbUrl,dbuser,dbPassword,dbbatchSize);
			
		/*	resultinvList=null;
			 resultinvList = tenantDao.getRevenueDetailsSummary(invyr,invmn);
			 for (Object[] list : resultinvList) {
					CustomerInvDtlsDTO custinvdtls = new CustomerInvDtlsDTO();
					
					custinvdtls.setLmocode(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
					
					custinvdtls.setMsocode(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
					lmoShare= new BigDecimal(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
					msoShare=new BigDecimal(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
					apsflShare=new BigDecimal(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
					apsflShare=  apsflShare.multiply(new BigDecimal(18));
					lmoShare=  lmoShare.multiply(new BigDecimal(18));
					msoShare=  msoShare.multiply(new BigDecimal(18));
					apsflShare = apsflShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					lmoShare = lmoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					msoShare = msoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					custinvdtls.setLmomonthlyshare(lmoShare.toString());
					custinvdtls.setMsomonthlyshare(msoShare.toString());
					custinvdtls.setApsflmonthlyshare(apsflShare.toString());			
					custinvdtls.setInvyr( invyr== null || invyr== "" ? "NA" :invyr);
					custinvdtls.setInvmn(invmn == null || invmn == "" ? "NA" : invmn);
					tenantDao.insertInvamtShareSummary(custinvdtls);
					
				}*/
				
			 
			
			
			LOGGER.info("TenantServiceImpl :: UpdateCustInvList End");
			 
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: UpdateCustInvList" + e);
			e.printStackTrace();
		} finally {
			resultinvList = null;
		}
		return custinvDtls2;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	public List<CustomerInvDtlsDTO> UpdateCustInvList(String invyr, String invmn, String monthStartdate, String MonthEndDate) {
		List<CustomerInvDtlsDTO> custinvDtls = new ArrayList<CustomerInvDtlsDTO>();
	//	List<CustomerInvDtlsDTO> custinvtempDtls = new ArrayList<CustomerInvDtlsDTO>();
		List<RevenueSharingTemplateDetails> rsTemplateDtlsList = new ArrayList<RevenueSharingTemplateDetails>();
		List<Object[]> resultinvList = new ArrayList<>();
		List<String> packageCodes =new ArrayList<String>();
		BigDecimal apsflShare = null;
		BigDecimal lmoShare=null;
		BigDecimal msoShare=null;
		List<Object[]> productCodes = new ArrayList<>();
		//Map<String,String> tmplCodes = null;
		List<Object[]> custInvdateWithInvDates = new ArrayList<>();
		boolean isExistsProdCode=false;
		String tmplcode="";
		String msocode="";
        String [] arrOfStr;
        
       
	
		
		LOGGER.info("TenantServiceImpl :: UpdateCustInvList Start");
		productCodes=revenueSharingDaoImpl.getTemplateCodefromProductCode();
		
		LOGGER.info("TenantServiceImpl :: UpdateCustInvList received productcodes");
		
		
		
		try {
			resultinvList = tenantDao.getCustInvList(monthStartdate,MonthEndDate);
			LOGGER.info("TenantServiceImpl :: resultinvList count :" +resultinvList.size() );		
			for (Object[] list : resultinvList) {
				CustomerInvDtlsDTO custinv = new CustomerInvDtlsDTO();
				
				custinv.setAcctcafno(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
				custinv.setChargeamt(list[6] == null || list[6].toString() == "" ? "NA" : list[6].toString());
				custinv.setChargecode(list[5] == null || list[5].toString() == "" ? "NA" : list[5].toString());
				custinv.setCustinvno(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
				custinv.setInvno(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
				custinv.setPmntcustid(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
				custinv.setProdcode(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
				custinv.setSrvctax(list[7] == null || list[7].toString() == "" ? "NA" : list[7].toString());
				custinv.setInvtodate(list[8] == null || list[8].toString() == "" ? "NA" : list[8].toString());
				custinv.setInvfromdate(list[9] == null || list[9].toString() == "" ? "NA" : list[9].toString());
				custinv.setInvyr(list[10] == null || list[10].toString() == "" ? "NA" : list[10].toString());
				custinv.setInvmn(list[11] == null || list[11].toString() == "" ? "NA" : list[11].toString());
				custinv.setLmocode(list[12] == null || list[12].toString() == "" ? "NA" : list[12].toString());
	             if (list[13]!=null && list[13]!="" && list[13].equals("GOVT")){
	            	 //dont add
	             }else{
				custinvDtls.add(custinv);
	             }
			}
			//custinvtempDtls.addAll(custinvDtls);
			rsTemplateDtlsList=  revenueSharingDaoImpl.getRevPercList();
			LOGGER.info("TenantServiceImpl :: resultinvList count :" +custinvDtls.size() );
			
		//	custInvdateWithInvDates=tenantDao.getCustInvListWithDates(invyr,invmn);
			
			
			LOGGER.info("TenantServiceImpl :: UpdateCustInvList calculating revenue share amounts");		
			
			for (CustomerInvDtlsDTO custinv : custinvDtls  ){
				apsflShare=apsflShare.ZERO;
				lmoShare=lmoShare.ZERO;
				msoShare=msoShare.ZERO;
				//productCodes=revenueSharingDaoImpl.getTemplateCodeCode(custinv.getProdcode(),custinv.getLmocode());
				
				//if (tmplCodes.containsKey(custinv.getProdcode())){
				for (Object[] revObjects : productCodes)
				{
					
					
					if (revObjects[0]!=null && revObjects[0].toString().equals(custinv.getProdcode())){
						if (null!=custinv.getLmocode()){
	
						if (revObjects[1]!=null  && revObjects[1].toString().contains(custinv.getLmocode())){
							arrOfStr = (revObjects[1].toString().split(","));

					        for (String a : arrOfStr)
					        {
					        	if (a.trim().startsWith("MSO")){
					        		msocode=a.substring(0, a.length()-1).trim();
					        		break;
					        	}
					        }
					       
							isExistsProdCode=true;
							if (revObjects[2]!=null){
					        tmplcode=revObjects[2].toString();
					        break;
							}
						}
						}
					}else 
					{
						isExistsProdCode=false;
						
						if (revObjects[1]!=null  && revObjects[1].toString().contains(custinv.getLmocode())){
							arrOfStr = (revObjects[1].toString().split(","));

					        for (String a : arrOfStr)
					        {
					        	if (a.trim().startsWith("MSO")){
					        		msocode=a.substring(0, a.length()-1).trim();
					        		break;
					        	}
					        }
					       
							isExistsProdCode=true;
							if (revObjects[2]!=null){
					        tmplcode=revObjects[2].toString();
					        break;
							}
						}
						
						
						
						
					}
				  
				}	
				
				
				
				
				custinv.setMsocode(msocode);
				if (isExistsProdCode==true){
					if (custinv.getChargecode().equals("SRVCRENT")) {
						
					for (RevenueSharingTemplateDetails revSharingtmplt : rsTemplateDtlsList){
						if (revSharingtmplt.getRstmplCode().equals(tmplcode)){
							if (revSharingtmplt.getTenanttype().equals("APSFL")){
								apsflShare=revSharingtmplt.getRevshareperc();
								
							}else if (revSharingtmplt.getTenanttype().equals("LMO")){
								lmoShare=revSharingtmplt.getRevshareperc();
								
							}else if (revSharingtmplt.getTenanttype().equals("MSP")){
								msoShare=revSharingtmplt.getRevshareperc();
							
						    }
						
					    }
					}
					
				    
					
					BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
					
					apsflShare=  apsflShare.multiply(tmpchargeamt);
					lmoShare=  lmoShare.multiply(tmpchargeamt);
					
					msoShare=  msoShare.multiply(tmpchargeamt);
					apsflShare = apsflShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					lmoShare = lmoShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					msoShare = msoShare.divide(new BigDecimal(100),0, BigDecimal.ROUND_HALF_UP);
					
				     
					
					}else if (custinv.getChargecode().equals("CPEEMI")||custinv.getChargecode().equals("CPECOST") || custinv.getChargecode().equals("STBCOST")
							|| custinv.getChargecode().equals("SRVCDEP") || custinv.getChargecode().equals("LATEPMNT")){
					
						BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
						
						apsflShare=tmpchargeamt;
					}else if (custinv.getChargecode().equals("LOCALUSAGE") ||custinv.getChargecode().equals("STDUSAGE") ||custinv.getChargecode().equals("ISDUSAGE") ){
						for (RevenueSharingTemplateDetails revSharingtmplte : rsTemplateDtlsList){
						
                            if (revSharingtmplte.getRstmplCode().equals("Basic_voip_service")){
								if (revSharingtmplte.getTenanttype().equals("APSFL")){
									apsflShare=revSharingtmplte.getRevshareperc();
									
								}else if (revSharingtmplte.getTenanttype().equals("LMO")){
									lmoShare=revSharingtmplte.getRevshareperc();
									
								}else if (revSharingtmplte.getTenanttype().equals("MSP")){
									msoShare=revSharingtmplte.getRevshareperc();
								
							    }
							
						    }
						
					    }
								
						
						BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
						
						apsflShare=  apsflShare.multiply(tmpchargeamt);
						lmoShare=  lmoShare.multiply(tmpchargeamt);
						
						msoShare=  msoShare.multiply(tmpchargeamt);
						apsflShare = apsflShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						lmoShare = lmoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						msoShare = msoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
						
						
					}
					
				}else if (custinv.getChargecode().equals("CPEEMI")||custinv.getChargecode().equals("CPECOST") || custinv.getChargecode().equals("STBCOST")
						|| custinv.getChargecode().equals("SRVCDEP") || custinv.getChargecode().equals("LATEPMNT")){
				
					BigDecimal tmpchargeamt=new BigDecimal(custinv.getChargeamt());
					
					apsflShare=tmpchargeamt;
					
				}else if (custinv.getChargecode().equals("INSTALLATION")||custinv.getChargecode().equals("ENTTAX") || custinv.getChargecode().equals("VOD")){
					apsflShare=apsflShare.ZERO;
					lmoShare=lmoShare.ZERO;
					msoShare=msoShare.ZERO;
					
				}
				custinv.setApsflInvshare(String.valueOf(apsflShare));
				custinv.setLmoInvshare(String.valueOf(lmoShare));
				custinv.setMsoInvshare(String.valueOf(msoShare));
				
			//	tenantDao.updateInvamtSharedtls(custinv,apsflShare,lmoShare,msoShare);
				msocode="";
				 tmplcode="";

			}
			tenantDao.insertInvamtShare(custinvDtls,dbUrl,dbuser,dbPassword,dbbatchSize);
			
			resultinvList=null;
			 resultinvList = tenantDao.getRevenueDetailsSummary(invyr,invmn);
			 for (Object[] list : resultinvList) {
					CustomerInvDtlsDTO custinvdtls = new CustomerInvDtlsDTO();
					
					custinvdtls.setLmocode(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
					
					custinvdtls.setMsocode(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
					lmoShare= new BigDecimal(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
					msoShare=new BigDecimal(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
					apsflShare=new BigDecimal(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
					apsflShare=  apsflShare.multiply(new BigDecimal(18));
					lmoShare=  lmoShare.multiply(new BigDecimal(18));
					msoShare=  msoShare.multiply(new BigDecimal(18));
					apsflShare = apsflShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					lmoShare = lmoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					msoShare = msoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					custinvdtls.setLmomonthlyshare(lmoShare.toString());
					custinvdtls.setMsomonthlyshare(msoShare.toString());
					custinvdtls.setApsflmonthlyshare(apsflShare.toString());			
					custinvdtls.setInvyr( invyr== null || invyr== "" ? "NA" :invyr);
					custinvdtls.setInvmn(invmn == null || invmn == "" ? "NA" : invmn);
					tenantDao.insertInvamtShareSummary(custinvdtls);
					
				}
				
			 
			
			
			LOGGER.info("TenantServiceImpl :: UpdateCustInvList End");
			 
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: UpdateCustInvList" + e);
			e.printStackTrace();
		} finally {
			resultinvList = null;
		}
		return custinvDtls;
	}
	*/
	

public boolean setMonthWiseShares(String invyr, String invmn) {
	
	
	List<Object[]> resultinvList = new ArrayList<>();
	List<Object[]> resultinvpmntList = new ArrayList<>();
	
	BigDecimal apsflShare = null;
	BigDecimal lmoShare=null;
	BigDecimal msoShare=null;
	BigDecimal tmplmoShare=null;
	BigDecimal tmpmsoShare=null;
	BigDecimal tmpapsflShare=null;
	BigDecimal tmptotalpaid=null;
	BigDecimal totalpaid=null;
	String lmocode="";
	Map<String,String> lmototalcollected = new HashMap<String, String>();
	
	
	boolean success=false;
	
    try{
	
    resultinvList = tenantDao.getRevenueDetailsSummary(invyr,invmn);
    resultinvpmntList = tenantDao.getTotalForLMOForMonth(invyr,invmn);
    for (Object[] list : resultinvpmntList) {

		
    	tmptotalpaid = new BigDecimal(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
    	
    	
    	lmocode=(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
    	
    	if (lmototalcollected !=null && lmototalcollected.containsKey(lmocode)) {
    		totalpaid=totalpaid.add( tmptotalpaid);
    		totalpaid=totalpaid.setScale(0, RoundingMode.HALF_UP);
    		lmototalcollected.put(lmocode, String.valueOf(totalpaid));
    	} else {
    		lmototalcollected.put(lmocode, String.valueOf(tmptotalpaid));
    		totalpaid=tmptotalpaid;
    	}
    
    	
    }
    	
    
			 for (Object[] list : resultinvList) {
					CustomerInvDtlsDTO custinvdtls = new CustomerInvDtlsDTO();
					
					custinvdtls.setLmocode(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
					
					custinvdtls.setMsocode(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
					lmoShare= new BigDecimal(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
					msoShare=new BigDecimal(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
					apsflShare=new BigDecimal(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
					tmplmoShare=lmoShare;
					tmpmsoShare=msoShare;
					tmpapsflShare=apsflShare;
					apsflShare=  apsflShare.multiply(new BigDecimal(18));
					lmoShare=  lmoShare.multiply(new BigDecimal(18));
					msoShare=  msoShare.multiply(new BigDecimal(18));
					apsflShare = apsflShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					lmoShare = lmoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					msoShare = msoShare.divide(new BigDecimal(100),4, BigDecimal.ROUND_HALF_UP);
					apsflShare=apsflShare.add(tmpapsflShare);
					lmoShare=lmoShare.add( tmplmoShare);
					msoShare=msoShare.add(tmpmsoShare);
					msoShare=msoShare.setScale(0, RoundingMode.HALF_UP);
					lmoShare=lmoShare.setScale(0, RoundingMode.HALF_UP);
					apsflShare=apsflShare.setScale(0, RoundingMode.HALF_UP);
					
					custinvdtls.setLmomonthlyshare(lmoShare.toString());
					custinvdtls.setMsomonthlyshare(msoShare.toString());
					custinvdtls.setApsflmonthlyshare(apsflShare.toString());			
					custinvdtls.setInvyr( invyr== null || invyr== "" ? "NA" :invyr);
					custinvdtls.setInvmn(invmn == null || invmn == "" ? "NA" : invmn);	
					custinvdtls.setTotalcollected(lmototalcollected.get(custinvdtls.getLmocode()));
					
					success=tenantDao.insertInvamtShareSummary(custinvdtls);
			 }	
		

			
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: setMonthWiseShares" + e);
			e.printStackTrace();
		} finally {
			resultinvList = null;
		}
		return success;
	}



	
	public List<CustomerInvDtlsDTO> getMonthWiseShares(String invyr, String invmn) {
		
		List<Object[]> resultinvList = new ArrayList<>();
		List<CustomerInvDtlsDTO> custinvDtls = new ArrayList<CustomerInvDtlsDTO>();
		try {
			
			LOGGER.info("TenantServiceImpl :: getMonthWiseShares Start");
			
			resultinvList = tenantDao.getMonthWiseShares(invyr,invmn);
			
			
			for (Object[] list : resultinvList) {
				CustomerInvDtlsDTO custinvdtls = new CustomerInvDtlsDTO();
				
				custinvdtls.setLmocode(list[0] == null || list[0].toString() == "" ? "NA" : list[0].toString());
				
				custinvdtls.setMsocode(list[1] == null || list[1].toString() == "" ? "NA" : list[1].toString());
				
				custinvdtls.setLmomonthlyshare(list[2] == null || list[2].toString() == "" ? "NA" : list[2].toString());
				custinvdtls.setMsomonthlyshare(list[3] == null || list[3].toString() == "" ? "NA" : list[3].toString());
				custinvdtls.setApsflmonthlyshare(list[4] == null || list[4].toString() == "" ? "NA" : list[4].toString());
				custinvdtls.setInvyr(list[5] == null || list[5].toString() == "" ? "NA" : list[5].toString());
				custinvdtls.setInvmn(list[6] == null || list[6].toString() == "" ? "NA" : list[6].toString());
				custinvdtls.setTotalcollected(list[7] == null || list[7].toString() == "" ? "NA" : list[7].toString());
				custinvDtls.add(custinvdtls);
				
			}
			
		 
		
		
		LOGGER.info("TenantServiceImpl :: getMonthWiseShares End");
		 

			
		} catch (Exception e) {
			LOGGER.error("The Exception is TenantServiceImpl :: getMonthWiseShares" + e);
			e.printStackTrace();
		} finally {
			resultinvList = null;
		}
		return custinvDtls;
	}
	
	
	
	
	
public void setPaidFlagForOverPaidCustomer(String invyr, String invmn) {

	    try{
		
	  tenantDao.setPaidFlagForOverPaidCustomer(invyr,invmn);

			} catch (Exception e) {
				LOGGER.error("The Exception is TenantServiceImpl :: setPaidFlagForOverPaidCustomer" + e);
				e.printStackTrace();
			} 
			
		}


	



}
