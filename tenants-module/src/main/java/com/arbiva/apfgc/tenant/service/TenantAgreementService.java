/*package com.arbiva.apfgc.tenant.service;

import java.util.List;

import com.arbiva.apfgc.tenant.model.TenantAgreement;
import com.arbiva.apfgc.tenant.vo.TenantAgreementVO;

public interface TenantAgreementService {

	public abstract void deleteTenantAgreement(TenantAgreement tenantAgreement);

	public abstract TenantAgreement findByTenantCode(String tenantCode);

	public abstract List<TenantAgreement> findAllTenantAgreement();

	public abstract void saveTenantAgreement(TenantAgreement tenantAgreement);

	public abstract TenantAgreement updateTenantAgreement(TenantAgreement tenantAgreement);

	public abstract void createTenantAgreement(TenantAgreementVO tenantAgreementVO, String action);

	public abstract void createTenantAgreement(TenantAgreementVO tenantAgreementVO, String action, String remoteIPAddr);


}
*/