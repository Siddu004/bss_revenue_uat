/**
 * 
 */
package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.LovsDao;
import com.arbiva.apfgc.tenant.model.Lovs;

/**
 * @author Arbiva
 *
 */
@Component("lovsServive")
@Transactional
public class LovsServiceImpl {
	
	@Autowired
	LovsDao lovsDao;

	public List<Lovs> findByLovsByLovName(String lovName) {
		// TODO Auto-generated method stub
		return lovsDao.findByLovsByLovName(lovName);
	}

	public List<String> findByL1PortSizeByLovName(String lovName) {
		// TODO Auto-generated method stub
		return lovsDao.findByL1PortSizeByLovName(lovName);
	}
}
