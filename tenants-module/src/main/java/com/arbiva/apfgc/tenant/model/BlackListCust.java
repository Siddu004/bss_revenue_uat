/**
 * 
 */
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


/**
 * @author kiran
 *
 */
@Entity
@Table(name = "blackliststb")
@IdClass(BlackListCustPK.class)
public class BlackListCust implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "stbcafno")
	private String stbcafno;
	
	@Id
	@Column(name = "effectivefrom")
	private Date effectivefrom;

	@Column(name = "blacklistflag")
	private char blacklistflag;

	@Column(name = "reason")
	private String reason;

	@Column(name = "status")
	private int status;

	@Column(name = "createdon")
	private Calendar createdon;

	@Column(name = "createdby")
	private String createdby;

	@Column(name = "createdipaddr")
	private String createdipaddr;

	@Column(name = "approvedon")
	private Calendar approvedon;

	@Column(name = "approvedby")
	private String approvedby;

	@Column(name = "approvedipaddr")
	private String approvedipaddr;
	
	public String getStbcafno() {
		return stbcafno;
	}

	public void setStbcafno(String stbcafno) {
		this.stbcafno = stbcafno;
	}
	
	public char getBlacklistflag() {
		return blacklistflag;
	}

	public void setBlacklistflag(char blacklistflag) {
		this.blacklistflag = blacklistflag;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getEffectivefrom() {
		return effectivefrom;
	}

	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Calendar getApprovedon() {
		return approvedon;
	}

	public void setApprovedon(Calendar approvedon) {
		this.approvedon = approvedon;
	}

	public String getApprovedby() {
		return approvedby;
	}

	public void setApprovedby(String approvedby) {
		this.approvedby = approvedby;
	}

	public String getApprovedipaddr() {
		return approvedipaddr;
	}

	public void setApprovedipaddr(String approvedipaddr) {
		this.approvedipaddr = approvedipaddr;
	}

}
