package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tanent.util.ApsflHelper;
import com.arbiva.apfgc.tanent.util.IpAddressValues;
import com.arbiva.apfgc.tenant.dao.CcDao;
import com.arbiva.apfgc.tenant.daoImpl.StoredProcedureDAO;
import com.arbiva.apfgc.tenant.pg.model.OLPayment;
import com.arbiva.apfgc.tenant.pg.utils.ComsEnumCodes;
import com.arbiva.apfgc.tenant.service.CcService;

@Service
public class CcServiceImpl implements CcService{
	
	@Autowired
	StoredProcedureDAO storedProcedureDAO;
	
	@Autowired
	CcDao ccDao;
	
	@Autowired
	IpAddressValues ipAddressValues;
	
	RestTemplate restTemplate = new RestTemplate();
	
	private static final Logger LOGGER = Logger.getLogger(CcServiceImpl.class);
	
	@Override
	public String processPyament(String amount, String custId, String transactionId,String deliveryId) {

		String responseValue= "";
		Long paymentId;
		OLPayment oLPayment;
		String aadharNo;
		String mobileNo;
		String message;
		ResponseEntity<String> response;
		HttpEntity<String> httpEntity;
		
		try {
			LOGGER.info("CcServiceImpl :: processPyament() :: Start");
			paymentId = storedProcedureDAO.executeStoredProcedure(ComsEnumCodes.PAYMENT_ID.getCode());
			String Id = custId;
			String[] Nos = Id.split(",");
			aadharNo=ccDao.findAadharNo(Nos[1]);
			oLPayment = new OLPayment();
			LOGGER.info("setting Pmnt ID"+paymentId.longValue());
			oLPayment.setPmntId(paymentId.longValue());
			LOGGER.info("setting Cust ID"+Nos[0]);
			oLPayment.setCustId(Long.parseLong(Nos[0]));
			LOGGER.info("setting CAF NO"+Nos[1]);
			oLPayment.setAcctCafNo(Long.parseLong(Nos[1]));
			LOGGER.info("setting Pmnt Ref No"+transactionId);
			oLPayment.setPmntRefNo(transactionId);
			LOGGER.info("setting district Id"+deliveryId);
			oLPayment.setDistrictuid(Integer.parseInt(deliveryId));
			LOGGER.info("setting amount"+amount);
			oLPayment.setPmntAmt(Float.parseFloat(amount));
			LOGGER.info("setting Status");
			oLPayment.setStatus(ComsEnumCodes.MONTHLY_PAYMENT_STAUTS.getStatus());
			LOGGER.info("setting aadharno"+aadharNo);
			oLPayment.setAadharNo(aadharNo);
			oLPayment.setPmntMode("CARD");
			oLPayment.setCreatedOn(Calendar.getInstance());
			oLPayment.setModifiedOn(Calendar.getInstance());
			oLPayment.setPmntDate(Calendar.getInstance());
			LOGGER.info("Before Save to Olpayments"+oLPayment);
			oLPayment = ccDao.saveOrUpdate(oLPayment);
			LOGGER.info("Save Completed");
			//Payment Success Mail and Msg
			
			//Sending Message
			mobileNo=ccDao.findMobileNo(Nos[0]);
			message = "Your Payment of Rs."+amount+" is  Successful for the Account No: "+Nos[1];
			
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() +"sendSMS?mobileNo="+mobileNo+"&msg="+message;
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			
			LOGGER.info("Payment Success Message send to the customer");
			
			/*//Sending Mail
			sendingEmail.sendMail(fromEmail,message);*/
			
			responseValue = "Success";
			LOGGER.info("CcServiceImpl :: processPyament() :: End");
		}catch(Exception e) {
			LOGGER.error("CcServiceImpl :: processPyament()::" +e);
			responseValue = "Failure";
			e.printStackTrace();
		}
		
		return responseValue;
	}

	@Override
	@Transactional
	public String processtenantswallet(String custId, String amount) {
		boolean result=false;
		String responseValue=null;
		
		result=ccDao.updatetenantswallet(custId,amount);
		if(result == true)  
			responseValue = "Success";
		else
			responseValue = "Failure";
		return responseValue;
	}

}
