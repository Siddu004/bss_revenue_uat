package com.arbiva.apfgc.tenant.vo;

import org.springframework.web.multipart.MultipartFile;

public class TenantAgreementVO {

	private String tenantCode;
	
	private int tenantId;
	
	private String agrFDate;
	
	private String agreementTo;
	
	private String coreService;
	
	private MultipartFile agreementCopy;
	
	private String agreementDate;
	
	private Float depositAmount;
	
	private String depositMode;
	
	private String depositReferenceNo;
	
	private Float walletAmount;
	
	private String reasons;
	
	private String[] coreServiceMulti;
	
	
	public String[] getCoreServiceMulti() {
		return coreServiceMulti;
	}
	public void setCoreServiceMulti(String[] coreServiceMulti) {
		this.coreServiceMulti = coreServiceMulti;
	}
	public int getTenantId() {
		return tenantId;
	}
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public String getAgrFDate() {
		return agrFDate;
	}
	public void setAgrFDate(String agrFDate) {
		this.agrFDate = agrFDate;
	}
	public String getAgreementTo() {
		return agreementTo;
	}
	public void setAgreementTo(String agreementTo) {
		this.agreementTo = agreementTo;
	}
	public String getCoreService() {
		return coreService;
	}
	public void setCoreService(String coreService) {
		this.coreService = coreService;
	}
	public MultipartFile getAgreementCopy() {
		return agreementCopy;
	}
	public void setAgreementCopy(MultipartFile agreementCopy) {
		this.agreementCopy = agreementCopy;
	}
	public String getAgreementDate() {
		return agreementDate;
	}
	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}
	public Float getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(Float depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getDepositMode() {
		return depositMode;
	}
	public void setDepositMode(String depositMode) {
		this.depositMode = depositMode;
	}
	public String getDepositReferenceNo() {
		return depositReferenceNo;
	}
	public void setDepositReferenceNo(String depositReferenceNo) {
		this.depositReferenceNo = depositReferenceNo;
	}
	public Float getWalletAmount() {
		return walletAmount;
	}
	public void setWalletAmount(Float walletAmount) {
		this.walletAmount = walletAmount;
	}
	public String getReasons() {
		return reasons;
	}
	public void setReasons(String reasons) {
		this.reasons = reasons;
	}
	
}
