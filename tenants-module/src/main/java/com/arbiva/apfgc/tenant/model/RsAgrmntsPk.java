package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;

public class RsAgrmntsPk implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long agruniqueid;
	private String tenantcode;
	private String prodcode;
	private Date agrfdate;
	public Long getAgruniqueid() {
		return agruniqueid;
	}
	public void setAgruniqueid(Long agruniqueid) {
		this.agruniqueid = agruniqueid;
	}
	public String getTenantcode() {
		return tenantcode;
	}
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}
	public String getProdcode() {
		return prodcode;
	}
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}
	public Date getAgrfdate() {
		return agrfdate;
	}
	public void setAgrfdate(Date agrfdate) {
		this.agrfdate = agrfdate;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agrfdate == null) ? 0 : agrfdate.hashCode());
		result = prime * result + ((agruniqueid == null) ? 0 : agruniqueid.hashCode());
		result = prime * result + ((prodcode == null) ? 0 : prodcode.hashCode());
		result = prime * result + ((tenantcode == null) ? 0 : tenantcode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RsAgrmntsPk other = (RsAgrmntsPk) obj;
		if (agrfdate == null) {
			if (other.agrfdate != null)
				return false;
		} else if (!agrfdate.equals(other.agrfdate))
			return false;
		if (agruniqueid == null) {
			if (other.agruniqueid != null)
				return false;
		} else if (!agruniqueid.equals(other.agruniqueid))
			return false;
		if (prodcode == null) {
			if (other.prodcode != null)
				return false;
		} else if (!prodcode.equals(other.prodcode))
			return false;
		if (tenantcode == null) {
			if (other.tenantcode != null)
				return false;
		} else if (!tenantcode.equals(other.tenantcode))
			return false;
		return true;
	}

}
