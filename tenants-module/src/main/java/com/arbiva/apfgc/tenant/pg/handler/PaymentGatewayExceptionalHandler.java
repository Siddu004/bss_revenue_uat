package com.arbiva.apfgc.tenant.pg.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.arbiva.apfgc.tenant.pg.dto.PaymentGatewayErrorMessageDTO;
import com.arbiva.apfgc.tenant.pg.exception.PaymentGatewayException;
import com.arbiva.apfgc.tenant.pg.utils.PaymentGatewayErrorCode.PaymentGatewayErrorCodes;

/**
 * {@link PaymentGatewayExceptionalHandler} is a handle to handle the exceptions and
 * provide proper error response with status.
 * 
 * @author srinivasa
 *
 */
@ControllerAdvice
public class PaymentGatewayExceptionalHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<PaymentGatewayErrorMessageDTO> parseError(HttpServletRequest request, Exception exception) {
		if (exception instanceof PaymentGatewayException) {
			return ResponseEntity.badRequest().body(((PaymentGatewayException) exception).getErrorMessageDTO());
		}
		return ResponseEntity.badRequest().body(new PaymentGatewayErrorMessageDTO(PaymentGatewayErrorCodes.GAE001));
	}
}
