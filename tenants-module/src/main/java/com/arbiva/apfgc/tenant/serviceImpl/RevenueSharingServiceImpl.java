package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.dao.CpeDao;
import com.arbiva.apfgc.tenant.daoImpl.RevenueSharingDaoImpl;
import com.arbiva.apfgc.tenant.daoImpl.TenantDaoImpl;
import com.arbiva.apfgc.tenant.dto.CustomerInvDtlsDTO;
import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsDTO;
import com.arbiva.apfgc.tenant.dto.RsTemplateDetailsListDTO;
import com.arbiva.apfgc.tenant.dto.TenantLMORevServiceDTO;
import com.arbiva.apfgc.tenant.model.ChargeRsDetails;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateDetails;
import com.arbiva.apfgc.tenant.model.RevenueSharingTemplateMaster;
import com.arbiva.apfgc.tenant.model.Tenant;
import com.arbiva.apfgc.tenant.vo.CpeStockVO;
import com.arbiva.apfgc.tenant.vo.TemplatePartnerVO;

@Service
public class RevenueSharingServiceImpl {
	
	private static final Logger LOGGER = Logger.getLogger(RevenueSharingServiceImpl.class);
	
	@Autowired
	RevenueSharingDaoImpl revenueSharingDaoImpl;
	
	@Autowired
	TenantDaoImpl tenantDaoImpl;
	
	@Autowired
	CpeDao cpeDao;
	
	public Map<String,String> getTenantTypeLovs(String lovName) 
	{
		List<Object[]>  tenantTypeLovs =  revenueSharingDaoImpl.getTenantTypeLovs(lovName);
		Map<String,String> map = new LinkedHashMap<String, String>();
		String s1="";
		String s2="";
		for(Object[] object : tenantTypeLovs){
			try{
				s1= object[0] == null ? "" : object[0].toString();
				s2=object[1] == null ? "" : object[1].toString();
				map.put(s2,s2);
			}catch (Exception ex) {
				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				s1 = null;
				s2 = null;
			}
		}
		return map;
	}
	
	public Map<String,String> getRegionLovs() 
	{
		List<Object[]>  tenantTypeLovs =  revenueSharingDaoImpl.getRegionLovs();
		Map<String,String> map = new LinkedHashMap<String, String>();
		String s1="";
		String s2="";
		for(Object[] object : tenantTypeLovs){
			try{
				s1= object[0] == null ? "" : object[0].toString();
				s2=object[1] == null ? "" : object[1].toString();
				map.put(s1,s2);
			}catch (Exception ex) {
				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				s1 = null;
				s2 = null;
			}
		}
		return map;
	}
	
	@Transactional
	public String saveTemplateMaster(RevenueSharingTemplateMaster rsTemplateMaster) 
	{
		String returnVal = "";
		RevenueSharingTemplateMaster rsTemplateMasterOldObj ;
		ChargeRsDetails chargeRsDetails ;
		try{
			
			  rsTemplateMasterOldObj = revenueSharingDaoImpl.getTemplateValues(rsTemplateMaster.getRstmplCode());
			if(rsTemplateMasterOldObj == null){
				rsTemplateMaster.setStatus(1);
				 revenueSharingDaoImpl.saveTemplateMaster(rsTemplateMaster);
				if(rsTemplateMaster.getChargeCodes() != null){
					for(String chargeCode : rsTemplateMaster.getChargeCodes()){
						  chargeRsDetails = new ChargeRsDetails();
						chargeRsDetails.setChargeCode(chargeCode);
						chargeRsDetails.setRstmplCode(rsTemplateMaster.getRstmplCode());
						cpeDao.saveOrUpdate(chargeRsDetails);
					}
				}
				returnVal = "success";
			}else{
				returnVal = "DuplicateEntry";
			}
			
		}
		catch (Exception ex) {
			throw ex;
		} finally {
			rsTemplateMasterOldObj= null;
			chargeRsDetails = null;
		}
		return returnVal;
	}
	
	public void saveTemplateDetails(RsTemplateDetailsDTO rsTempDtls) 
	{
		try{
			for(RsTemplateDetailsListDTO rsTempDtlsList :rsTempDtls.getRsTemplateList())
			{
				RevenueSharingTemplateDetails rsTempDetails = new RevenueSharingTemplateDetails(rsTempDtls, rsTempDtlsList);
				revenueSharingDaoImpl.saveTemplateDetails(rsTempDetails);
			}
		}
		catch (Exception ex) {
			throw ex;
		} finally {
			
		}
	}
	
	public Map<String,String> getTemplatesLov()
	{
		List<Object[]>  tenantTypeLovs =  revenueSharingDaoImpl.getTemplatesLov();
		Map<String,String> map = new LinkedHashMap<String, String>();
		String s1="";
		String s2="";
		for(Object[] object : tenantTypeLovs){
			try{
				s1= object[0] == null ? "" : object[0].toString();
				s2=object[1] == null ? "" : object[1].toString();
				map.put(s1,s2);
			}catch (Exception ex) {
				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				s1 = null;
				s2 = null;
			}
		}
		return map;
	}

	public List<RevenueSharingTemplateMaster> getTemplatesByCount(Integer count) {
		
		return  revenueSharingDaoImpl.getTemplatesByCount(count);
		
	}

	public List<RevenueSharingTemplateDetails> getAllTenantTypeByTemplateCode(String tmplCode, String region) {
		// TODO Auto-generated method stub
		return revenueSharingDaoImpl.getAllTenantTypeByTemplateCode(tmplCode,region);
	}

	public List<Object[]> getAllTemplateRegions(String tmplCode) {
		// TODO Auto-generated method stub
		return revenueSharingDaoImpl.getAllTemplateRegions(tmplCode);
	}

	public List<TemplatePartnerVO> getAllTenantsByTemplCode(String tmplCode) {
		List<TemplatePartnerVO> templatePartnerList = new ArrayList<TemplatePartnerVO>();
		List<Object[]> tenantsTypes = revenueSharingDaoImpl.getAllTenantsByTemplCode(tmplCode);
		TemplatePartnerVO templatePartnerVO ;
		try {
		for(Object[] obj : tenantsTypes){
			List<Tenant> tanantsList = tenantDaoImpl.findAllByTenantType(obj[1].toString());
			  templatePartnerVO = new TemplatePartnerVO();
			templatePartnerVO.setSno(Integer.parseInt(obj[0].toString()));
			templatePartnerVO.setTemplCode(tmplCode);
			templatePartnerVO.setTenantType(obj[1].toString());
			templatePartnerVO.setTenantList(tanantsList);
			templatePartnerList.add(templatePartnerVO);
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		} finally {
			tenantsTypes = null;
			templatePartnerVO = null;
		}
		
		return templatePartnerList;
	}

	
	public RevenueSharingTemplateMaster getTemplateValues(String tempCode) 
	{
		RevenueSharingTemplateMaster rsTempMaster = null;
		try{
			rsTempMaster = revenueSharingDaoImpl.getTemplateValues(tempCode);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info(ex.getMessage());
		} finally {
			
		}
		return rsTempMaster;
	}
	
	public List<RsTemplateDetailsListDTO> getRegTempPercList(String tempCode, String region) 
	{
		List<RsTemplateDetailsListDTO> rsTemplateList = new ArrayList<>();
		List<RevenueSharingTemplateDetails> rsTemplateDtlsList = null;
		try{
			rsTemplateDtlsList = revenueSharingDaoImpl.getRegTempPercList(tempCode, region);
			for(RevenueSharingTemplateDetails rsTemp: rsTemplateDtlsList)
			{
				RsTemplateDetailsListDTO rsTempDtls = new RsTemplateDetailsListDTO(rsTemp);
				rsTemplateList.add(rsTempDtls);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info(ex.getMessage());
		} finally {
			rsTemplateDtlsList = null;
		}
		return rsTemplateList;
	}
	
	public Map<String,String> getTenantTypeVals(String tempCode) 
	{
		List<Object[]>  tenantTypeLovs =  revenueSharingDaoImpl.getTenantTypeVals(tempCode);
		Map<String,String> map = new LinkedHashMap<String, String>();
		String s1="";
		String s2="";
		for(Object[] object : tenantTypeLovs){
			try{
				s1= object[0] == null ? "" : object[0].toString();
				s2=object[1] == null ? "" : object[1].toString();
				map.put(s1,s2);
			}catch (Exception ex) {
				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				s1 = null;
				s2 = null;
				tenantTypeLovs = null;
			}
		}
		return map;
	}

	public List<RevenueSharingTemplateMaster> getAllSimilarTemplatesByTemplCode(String templCode) {
		return revenueSharingDaoImpl.getAllSimilarTemplatesByTemplCode(templCode);
	}

	public Map<String, String> getChargeCodesByChargeLevelFlage() {

		List<Object[]>  tenantTypeLovs =  revenueSharingDaoImpl.getChargeCodesByChargeLevelFlage();
		Map<String,String> map = new LinkedHashMap<String, String>();
		String s1="";
		String s2="";
		for(Object[] object : tenantTypeLovs){
			try{
				s1= object[0] == null ? "" : object[0].toString();
				s2=object[1] == null ? "" : object[1].toString();
				map.put(s1,s2);
			}catch (Exception ex) {
				ex.printStackTrace();
				LOGGER.info(ex.getMessage());
			} finally {
				s1 = null;
				s2 = null;
				tenantTypeLovs = null;
			}
		}
		return map;
	}

	public List<TenantLMORevServiceDTO> getLMORevShare(String lmocode) {

		List<TenantLMORevServiceDTO> lmoRevServiceDTOList = new ArrayList<>();

		List<Object[]> lmoapsflShareList = revenueSharingDaoImpl.getLMOAPSFLShare(lmocode);
		for (Object[] obj : lmoapsflShareList) {
				TenantLMORevServiceDTO lmoRevService = new TenantLMORevServiceDTO();			
			lmoRevService.setYear(Integer.parseInt(obj[0].toString()));
			lmoRevService.setMonth(Integer.parseInt(obj[1].toString()));
			lmoRevService.setApsflShare(Double.parseDouble(obj[2].toString()));
			lmoRevServiceDTOList.add(lmoRevService);
		}
		return lmoRevServiceDTOList;
	}

}
