/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantAgreement;
import com.arbiva.apfgc.tenant.model.TenantAgreementPK;


public interface TenantAgreementDao extends JpaSpecificationExecutor<TenantAgreement>, JpaRepository<TenantAgreement, TenantAgreementPK>{

	@Query(value="SELECT * FROM tenantsagr where tenantcode = ?1 ",nativeQuery=true)
	public abstract TenantAgreement findByTenantCode(String tenantCode);
}
*/