/**
 * 
 *//*
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Date;

*//**
 * @author Lakshman
 *
 *//*
public class TenantServicesPK implements Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

	private String tenantCode;
	
	private Date agrFDate;
	
	private String coresrvcCode;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public Date getAgrFDate() {
		return agrFDate;
	}

	public void setAgrFDate(Date agrFDate) {
		this.agrFDate = agrFDate;
	}

	public String getCoresrvcCode() {
		return coresrvcCode;
	}

	public void setCoresrvcCode(String coresrvcCode) {
		this.coresrvcCode = coresrvcCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agrFDate == null) ? 0 : agrFDate.hashCode());
		result = prime * result + ((coresrvcCode == null) ? 0 : coresrvcCode.hashCode());
		result = prime * result + ((tenantCode == null) ? 0 : tenantCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantServicesPK other = (TenantServicesPK) obj;
		if (agrFDate == null) {
			if (other.agrFDate != null)
				return false;
		} else if (!agrFDate.equals(other.agrFDate))
			return false;
		if (coresrvcCode == null) {
			if (other.coresrvcCode != null)
				return false;
		} else if (!coresrvcCode.equals(other.coresrvcCode))
			return false;
		if (tenantCode == null) {
			if (other.tenantCode != null)
				return false;
		} else if (!tenantCode.equals(other.tenantCode))
			return false;
		return true;
	}

}
*/