/**
 * 
 *//*
package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreement;
import com.arbiva.apfgc.tenant.model.TenantLmoMspAgreementPK;

*//**
 * @author Arbiva
 *
 *//*
public interface TenantLmoMspAgreementDao extends JpaSpecificationExecutor<TenantLmoMspAgreement>, JpaRepository<TenantLmoMspAgreement, TenantLmoMspAgreementPK>{

	@Query(value="SELECT * FROM msplmoagr where mspcode = ?1 ",nativeQuery=true)
	public abstract TenantLmoMspAgreement findByMspCode(String mspCode);
	
	@Query(value="SELECT * FROM msplmoagr where lmocode = ?1 ",nativeQuery=true)
	public abstract TenantLmoMspAgreement findByLmoCode(String lmoCode);
}
*/