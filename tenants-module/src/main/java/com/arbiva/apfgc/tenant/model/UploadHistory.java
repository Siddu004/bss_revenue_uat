package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "uploadhist")
public class UploadHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "uploadid")
	private Long uploadId;
	
	@Column(name = "uploadtype")
	private String uploadType;
	
	@Column(name = "filename")
	private String fileName;
	
	@Column(name = "filesize")
	private String fileSize;
	
	@Column(name = "noofcols")
	private Integer noofCols;
	
	@Column(name = "noofrows")
	private Long noofRows;
	
	@Column(name = "uploaddate")
	private Calendar uploadDate;
	
	@Column(name = "uploadedby")
	private String uploadedBy;
	
	@Column(name = "successrecs")
	private Long successRecords;
	
	@Column(name = "correctedrec")
	private Long correctedRecords;
	
	@Column(name = "status")
	private Integer status;

	public Long getUploadId() {
		return uploadId;
	}

	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}

	public String getUploadType() {
		return uploadType;
	}

	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getNoofCols() {
		return noofCols;
	}

	public void setNoofCols(Integer noofCols) {
		this.noofCols = noofCols;
	}

	public Long getNoofRows() {
		return noofRows;
	}

	public void setNoofRows(Long noofRows) {
		this.noofRows = noofRows;
	}

	public Calendar getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Calendar uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Long getSuccessRecords() {
		return successRecords;
	}

	public void setSuccessRecords(Long successRecords) {
		this.successRecords = successRecords;
	}

	public Long getCorrectedRecords() {
		return correctedRecords;
	}

	public void setCorrectedRecords(Long correctedRecords) {
		this.correctedRecords = correctedRecords;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
