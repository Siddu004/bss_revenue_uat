/**
 * 
 */
package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Lakshman
 *
 */
@Entity
@Table(name="substations")
public class Substations implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="substnuid")
	private String substnUid;
	
	@Column(name="substnname")
	private String substnName;
	
	@Column(name="substntype")
	private String substnType;
	
	@Column(name="districtuid")
	private Integer districtUid;
	
	@Column(name="mandalslno")
	private Integer mandalSlno;
	
	@ManyToOne
	@JoinColumns({@JoinColumn(name = "districtuid", referencedColumnName = "districtuid", nullable = false, insertable=false, updatable=false), @JoinColumn(name = "mandalslno", referencedColumnName = "mandalslno", nullable = false, insertable=false, updatable=false)})
	private Mandals mandal;
	
	public Mandals getMandal() {
		return mandal;
	}

	public void setMandal(Mandals mandal) {
		this.mandal = mandal;
	}
	
	public String getSubstnUid() {
		return substnUid;
	}

	public void setSubstnUid(String substnUid) {
		this.substnUid = substnUid;
	}

	public String getSubstnName() {
		return substnName;
	}

	public void setSubstnName(String substnName) {
		this.substnName = substnName;
	}

	public String getSubstnType() {
		return substnType;
	}

	public void setSubstnType(String substnType) {
		this.substnType = substnType;
	}

	public Integer getDistrictUid() {
		return districtUid;
	}

	public void setDistrictUid(Integer districtUid) {
		this.districtUid = districtUid;
	}

	public Integer getMandalSlno() {
		return mandalSlno;
	}

	public void setMandalSlno(Integer mandalSlno) {
		this.mandalSlno = mandalSlno;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
