/*package com.arbiva.apfgc.tenant.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.arbiva.apfgc.tenant.model.TenantBankDetails;

public interface TenantBankDetailsDao extends JpaSpecificationExecutor<TenantBankDetails>, JpaRepository<TenantBankDetails, String>{
	
	@Query(value="SELECT * FROM tenantbanks where tenantcode = ?1 ",nativeQuery=true)
	public abstract TenantBankDetails findByTenantCode(String tenantCode);
}
*/