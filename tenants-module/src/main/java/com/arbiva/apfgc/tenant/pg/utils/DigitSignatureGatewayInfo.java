package com.arbiva.apfgc.tenant.pg.utils;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author srinivasa
 *
 */
@Component("digitSignatureGatewayInfo")
public class DigitSignatureGatewayInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Value("${digitsecure.merchant.id}")
	private String merchantId;

	@Value("${digitsecure.merchant.name}")
	private String merchantName;

	@Value("${digitsecure.merchant.password}")
	private String password;

	@Value("${digitsecure.request.url}")
	private String requestUrl;

	@Value("${digitsecure.redirect.url}")
	private String returnUrl;

	@Value("${digitsecure.request.payload}")
	private String requestPayload;
	
	@Value("${digitsecure.request.payload.css}")
	private String requestPayloadCss;
	
	@Value("${digitsecure.redirect.url.css}")
	private String returnUrlCss;
	
	@Value("${digitsecure.request.payload.css.beforeLogin}")
	private String requestPayloadCssBeforeLogin;
	
	@Value("${digitsecure.redirect.url.css.beforeLogin}")
	private String returnUrlCssBeforeLogin;
	

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}

	/**
	 * @param merchantName
	 *            the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the requestUrl
	 */
	public String getRequestUrl() {
		return requestUrl;
	}

	/**
	 * @param requestUrl
	 *            the requestUrl to set
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * @return the returnUrl
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *            the returnUrl to set
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * @return the requestPayload
	 */
	public String getRequestPayload() {
		return requestPayload;
	}

	/**
	 * @param requestPayload
	 *            the requestPayload to set
	 */
	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}
	

	public String getRequestPayloadCss() {
		return requestPayloadCss;
	}

	public void setRequestPayloadCss(String requestPayloadCss) {
		this.requestPayloadCss = requestPayloadCss;
	}

	public String getReturnUrlCss() {
		return returnUrlCss;
	}

	public void setReturnUrlCss(String returnUrlCss) {
		this.returnUrlCss = returnUrlCss;
	}
	
	public String getRequestPayloadCssBeforeLogin() {
		return requestPayloadCssBeforeLogin;
	}

	public void setRequestPayloadCssBeforeLogin(String requestPayloadCssBeforeLogin) {
		this.requestPayloadCssBeforeLogin = requestPayloadCssBeforeLogin;
	}

	public String getReturnUrlCssBeforeLogin() {
		return returnUrlCssBeforeLogin;
	}

	public void setReturnUrlCssBeforeLogin(String returnUrlCssBeforeLogin) {
		this.returnUrlCssBeforeLogin = returnUrlCssBeforeLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result
				+ ((merchantName == null) ? 0 : merchantName.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((requestPayload == null) ? 0 : requestPayload.hashCode());
		result = prime * result
				+ ((requestUrl == null) ? 0 : requestUrl.hashCode());
		result = prime * result
				+ ((returnUrl == null) ? 0 : returnUrl.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitSignatureGatewayInfo other = (DigitSignatureGatewayInfo) obj;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (merchantName == null) {
			if (other.merchantName != null)
				return false;
		} else if (!merchantName.equals(other.merchantName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (requestPayload == null) {
			if (other.requestPayload != null)
				return false;
		} else if (!requestPayload.equals(other.requestPayload))
			return false;
		if (requestUrl == null) {
			if (other.requestUrl != null)
				return false;
		} else if (!requestUrl.equals(other.requestUrl))
			return false;
		if (returnUrl == null) {
			if (other.returnUrl != null)
				return false;
		} else if (!returnUrl.equals(other.returnUrl))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DigitSignatureGatewayInfo [merchantId=" + merchantId
				+ ", merchantName=" + merchantName + ", password=" + password
				+ ", requestUrl=" + requestUrl + ", returnUrl=" + returnUrl
				+ ", requestPayload=" + requestPayload + "]";
	}

}