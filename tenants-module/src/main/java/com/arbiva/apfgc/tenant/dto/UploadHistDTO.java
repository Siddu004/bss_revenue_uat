package com.arbiva.apfgc.tenant.dto;

public class UploadHistDTO {
	
   private String uploadid;

   private String uploadrecno;
	
	private String mspcode;
	
	private String chnlcode;
	
	private String chnlname;
	
	private String pkgname;
	
	private String srvccode;
	
	private String srvcname;
	
	private String featurecodes;

	public String getUploadid() {
		return uploadid;
	}

	public void setUploadid(String uploadid) {
		this.uploadid = uploadid;
	}

	public String getMspcode() {
		return mspcode;
	}

	public void setMspcode(String mspcode) {
		this.mspcode = mspcode;
	}

	public String getChnlcode() {
		return chnlcode;
	}

	public void setChnlcode(String chnlcode) {
		this.chnlcode = chnlcode;
	}

	public String getChnlname() {
		return chnlname;
	}

	public void setChnlname(String chnlname) {
		this.chnlname = chnlname;
	}

	public String getPkgname() {
		return pkgname;
	}

	public void setPkgname(String pkgname) {
		this.pkgname = pkgname;
	}

	public String getSrvccode() {
		return srvccode;
	}

	public void setSrvccode(String srvccode) {
		this.srvccode = srvccode;
	}

	public String getSrvcname() {
		return srvcname;
	}

	public void setSrvcname(String srvcname) {
		this.srvcname = srvcname;
	}

	public String getFeaturecodes() {
		return featurecodes;
	}

	public void setFeaturecodes(String featurecodes) {
		this.featurecodes = featurecodes;
	}

	public String getUploadrecno() {
		return uploadrecno;
	}

	public void setUploadrecno(String uploadrecno) {
		this.uploadrecno = uploadrecno;
	}
	

}
