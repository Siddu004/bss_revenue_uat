package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;


public class MspChnlsStgPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long uploadid;
	
	private Long uploadrecno;
	
	public Long getUploadid() {
		return uploadid;
	}

	public void setUploadid(Long uploadid) {
		this.uploadid = uploadid;
	}

	public Long getUploadrecno() {
		return uploadrecno;
	}

	public void setUploadrecno(Long uploadrecno) {
		this.uploadrecno = uploadrecno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uploadid == null) ? 0 : uploadid.hashCode());
		result = prime * result + ((uploadrecno == null) ? 0 : uploadrecno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MspChnlsStgPK other = (MspChnlsStgPK) obj;
		if (uploadid == null) {
			if (other.uploadid != null)
				return false;
		} else if (!uploadid.equals(other.uploadid))
			return false;
		if (uploadrecno == null) {
			if (other.uploadrecno != null)
				return false;
		} else if (!uploadrecno.equals(other.uploadrecno))
			return false;
		return true;
	}
	

}
