package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.arbiva.apfgc.tenant.dto.RevenueSharingDTO;

@Entity
@Table(name="rsmonthlyout")
public class RsMonthlyOut {
	

	@Id
	@Column(name = "rspayouttransid")
	private Long rsPayoutTransId;
	
	@Column(name = "trandate")
	private Calendar tranDate;
	
	@Column(name = "tenantcode")
	private String tenantCode;
	
	@Column(name = "formon")
	private String forMon;
	
	@Column(name = "grosspaidamt")
	private BigDecimal grosspaidamt;
	
	@Column(name = "netpaidamt")
	private BigDecimal netpaidamt;
	
	@Column(name = "reasonfordiff")
	private String reasonForDiff;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "createdon")
	private Date createdon;
	
	@Column(name = "createdby")
	private String createdby;
	
	@Column(name = "createdipaddr")
	private String createdipaddr;
	
	@Column(name = "modifiedon")
	private Date modifiedon;
	
	@Column(name = "modifiedby")
	private String modifiedby;
	
	@Column(name = "modifiedipaddr")
	private String modifiedipaddr;
	
	

	public RsMonthlyOut(){}
	
	public RsMonthlyOut(Long trnsId, RevenueSharingDTO revenue, String date) {
		
		this.setRsPayoutTransId(trnsId);
		this.setTranDate(Calendar.getInstance());
		this.setTenantCode(revenue.getTenantcode());
		this.setForMon(date);
		this.setGrosspaidamt(new BigDecimal(revenue.getShareamt()).subtract(new BigDecimal(revenue.getEnttax())));
		this.setNetpaidamt(this.getGrosspaidamt());
		this.setReasonForDiff("");
		this.setStatus(1);
		this.setCreatedon(Calendar.getInstance().getTime());
		this.setModifiedon(Calendar.getInstance().getTime());
	}

	public Long getRsPayoutTransId() {
		return rsPayoutTransId;
	}

	public void setRsPayoutTransId(Long rsPayoutTransId) {
		this.rsPayoutTransId = rsPayoutTransId;
	}

	public Calendar getTranDate() {
		return tranDate;
	}

	public void setTranDate(Calendar tranDate) {
		this.tranDate = tranDate;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getForMon() {
		return forMon;
	}

	public void setForMon(String forMon) {
		this.forMon = forMon;
	}

	public BigDecimal getGrosspaidamt() {
		return grosspaidamt;
	}

	public void setGrosspaidamt(BigDecimal grosspaidamt) {
		this.grosspaidamt = grosspaidamt;
	}

	public BigDecimal getNetpaidamt() {
		return netpaidamt;
	}

	public void setNetpaidamt(BigDecimal netpaidamt) {
		this.netpaidamt = netpaidamt;
	}

	public String getReasonForDiff() {
		return reasonForDiff;
	}

	public void setReasonForDiff(String reasonForDiff) {
		this.reasonForDiff = reasonForDiff;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedipaddr() {
		return modifiedipaddr;
	}

	public void setModifiedipaddr(String modifiedipaddr) {
		this.modifiedipaddr = modifiedipaddr;
	}
}
