package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @author Sai Suresh
 * 
 */
@MappedSuperclass
public abstract class Base implements Serializable {

	private static final long serialVersionUID = 1L;


	@Column(name = "status", columnDefinition="tinyint(1) default 1")
    @NotNull
    private Short status;

	@Column(name = "createdon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar createdon;

	@Column(name = "createdby", length = 50)
    private String createdby;

	@Column(name = "createdipaddr", length = 20)
    private String createdipaddr;

	@Column(name = "modifiedon")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar modifiedon;

	@Column(name = "modifiedby", length = 50)
    private String modifiedby;

	@Column(name = "modifiedipaddr", length = 20)
    private String modifiedipaddr;
	
	
	@Column(name = "deactivatedon")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar deactivatedon;

	@Column(name = "deactivatedby", length = 50)
    private String deactivatedby;

	@Column(name = "deactivatedipaddr", length = 20)
    private String deactivatedipaddr;
	

	public Calendar getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(Calendar deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}

	public String getDeactivatedipaddr() {
		return deactivatedipaddr;
	}

	public void setDeactivatedipaddr(String deactivatedipaddr) {
		this.deactivatedipaddr = deactivatedipaddr;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Calendar getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Calendar createdon) {
		this.createdon = createdon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreatedipaddr() {
		return createdipaddr;
	}

	public void setCreatedipaddr(String createdipaddr) {
		this.createdipaddr = createdipaddr;
	}

	public Calendar getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Calendar modifiedon) {
		this.modifiedon = modifiedon;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedipaddr() {
		return modifiedipaddr;
	}

	public void setModifiedipaddr(String modifiedipaddr) {
		this.modifiedipaddr = modifiedipaddr;
	}
	
	
	
	

	
	
	
}
