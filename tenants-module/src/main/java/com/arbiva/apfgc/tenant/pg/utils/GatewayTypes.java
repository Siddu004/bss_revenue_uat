package com.arbiva.apfgc.tenant.pg.utils;

public enum GatewayTypes {
	
	BILLDESK,
	DIGITSIGNATURE;
	
	private GatewayTypes() {
	}

}
