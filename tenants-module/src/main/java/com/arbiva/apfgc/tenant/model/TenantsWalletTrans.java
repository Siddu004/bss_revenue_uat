package com.arbiva.apfgc.tenant.model;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Table(name = "tenantswallettrans")
@Entity
@IdClass(TenantsWalletTransId.class)
public class TenantsWalletTrans extends Base {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name ="trandate")
	private Calendar trandate;
	@Id
	@Column(name ="tenantcode")
	private String tenantcode;
	@Id
	@Column(name ="tranrefno")
	private String tranrefno;
	
	@Column(name ="trantype")
	private String trantype;
	
	@Column(name ="crdb_flag")
	private Character crdb_flag;
	
	@Column(name ="tranamt")
	private BigDecimal tranamt;
	
	@Column(name ="tranmode")
	private String tranmode;
	
	@Column(name ="walletadjamt")
	private BigDecimal walletadjamt;
	
	@Column(name ="crlimitadjamt")
	private BigDecimal crlimitadjamt;
	
	

	public Calendar getTrandate() {
		return trandate;
	}

	public void setTrandate(Calendar trandate) {
		this.trandate = trandate;
	}

	public String getTenantcode() {
		return tenantcode;
	}

	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	public String getTranrefno() {
		return tranrefno;
	}

	public void setTranrefno(String tranrefno) {
		this.tranrefno = tranrefno;
	}

	public String getTrantype() {
		return trantype;
	}

	public void setTrantype(String trantype) {
		this.trantype = trantype;
	}

	public Character getCrdb_flag() {
		return crdb_flag;
	}

	public void setCrdb_flag(Character crdb_flag) {
		this.crdb_flag = crdb_flag;
	}

	public BigDecimal getTranamt() {
		return tranamt;
	}

	public void setTranamt(BigDecimal tranamt) {
		this.tranamt = tranamt;
	}

	public String getTranmode() {
		return tranmode;
	}

	public void setTranmode(String tranmode) {
		this.tranmode = tranmode;
	}

	public BigDecimal getWalletadjamt() {
		return walletadjamt;
	}

	public void setWalletadjamt(BigDecimal walletadjamt) {
		this.walletadjamt = walletadjamt;
	}

	public BigDecimal getCrlimitadjamt() {
		return crlimitadjamt;
	}

	public void setCrlimitadjamt(BigDecimal crlimitadjamt) {
		this.crlimitadjamt = crlimitadjamt;
	}
	

}
