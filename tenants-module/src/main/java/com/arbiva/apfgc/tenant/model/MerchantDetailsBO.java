package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MerchantDetailsBO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="merchantid")
	private String merchantid;
	
	@Column(name="custtype")
	private String custtype;
	
	@Column(name="merchantpwd")
	private String merchantpwd;

	public String getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	public String getCusttype() {
		return custtype;
	}

	public void setCusttype(String custtype) {
		this.custtype = custtype;
	}

	public String getMerchantpwd() {
		return merchantpwd;
	}

	public void setMerchantpwd(String merchantpwd) {
		this.merchantpwd = merchantpwd;
	}

}
