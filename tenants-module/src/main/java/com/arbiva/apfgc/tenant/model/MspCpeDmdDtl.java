package com.arbiva.apfgc.tenant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Table(name="mspcpedmddtl")
@Entity
@IdClass(value=MspCpeDmdDtlPK.class)
public class MspCpeDmdDtl {
	
	@Id
	@Column(name ="dmdid")
	private Long dmdId;
	
	@Id
	@Column(name ="lmoname")
	private String lmoName;
	
	@Column(name ="noemidemandqty")
	private Long noEmiDemandQty;
	
	@Column(name ="emi36demandqty")
	private Long emi36DemandQty;
	
	@Column(name ="emi48demandqty")
	private Long emi48DemandQty;
	

	public Long getDmdId() {
		return dmdId;
	}

	public void setDmdId(Long dmdId) {
		this.dmdId = dmdId;
	}

	public String getLmoName() {
		return lmoName;
	}

	public void setLmoName(String lmoName) {
		this.lmoName = lmoName;
	}

	public Long getNoEmiDemandQty() {
		return noEmiDemandQty;
	}

	public void setNoEmiDemandQty(Long noEmiDemandQty) {
		this.noEmiDemandQty = noEmiDemandQty;
	}

	public Long getEmi36DemandQty() {
		return emi36DemandQty;
	}

	public void setEmi36DemandQty(Long emi36DemandQty) {
		this.emi36DemandQty = emi36DemandQty;
	}

	public Long getEmi48DemandQty() {
		return emi48DemandQty;
	}

	public void setEmi48DemandQty(Long emi48DemandQty) {
		this.emi48DemandQty = emi48DemandQty;
	}
	
}
