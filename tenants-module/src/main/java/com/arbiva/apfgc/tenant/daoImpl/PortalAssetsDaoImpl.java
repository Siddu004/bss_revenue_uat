package com.arbiva.apfgc.tenant.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.model.PortalAreas;
import com.arbiva.apfgc.tenant.model.PortalAssets;

@Repository
public class PortalAssetsDaoImpl {
private static final Logger LOGGER = Logger.getLogger(PortalAssetsDaoImpl.class);
	
	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}
	
	/*public List<PortalAssets> findByEnrollmentno(String Enrollmentno) {
		
		List<PortalAssets> portalassets = new ArrayList<PortalAssets>();
		StringBuilder builder = new StringBuilder(" FROM ").append(PortalAssets.class.getSimpleName())
				.append(" WHERE Enrollmentno=:enrollmentno");
		try {
			LOGGER.info("START::findByEnrollmentno()");
			TypedQuery<PortalAssets> query = getEntityManager().createQuery(builder.toString(),PortalAssets.class);
			query.setParameter("enrollmentno", Enrollmentno);
			portalassets = query.getResultList();
			LOGGER.info("END::findByEnrollmentno()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findByEnrollmentno() " + e);
		}
		return portalassets;
	}*/
	
	public List<Object[]> findByEnrollmentno(String Enrollmentno) {
		List<Object[]> assetsList = new ArrayList<Object[]>();
		StringBuilder builder = new StringBuilder();
		 Query query = null;
			try {
				 LOGGER.info("PortalAssetsDaoImpl :: findByEnrollmentno() :: SATRT");
		          builder.append("select (select cabletypename from cabletypes where cabletypeid = pa.cabletypeid) cabletype, "+
								" (select assettypename from assettypes where assettypeid = pa.assettypeid) assettype, "+
								" routename, senttranstime, imieno, versionno "+
								" from portal_routes pa "+
								" where enrollmentno = :Enrollmentno ");
			query = em.createNativeQuery(builder.toString());	
			query.setParameter("Enrollmentno", Enrollmentno);
			assetsList = query.getResultList();
		}catch(Exception ex){
			LOGGER.error("PortalAssetsDaoImpl::findByEnrollmentno() " + ex);
		} finally {
			query = null;
			builder = null;
		}
		return assetsList;
	}
	
	public void savePortalAssets(PortalAssets portalassets) {
		getEntityManager().merge(portalassets);
	}
	
	public List<PortalAssets> findAllPortalAssets() {
		List<PortalAssets> portalassets = new ArrayList<PortalAssets>();
		StringBuilder builder = new StringBuilder();
		TypedQuery<PortalAssets> query = null;
			try {
				 LOGGER.info("PortalAssetsDaoImpl :: findAllPortalAssets() :: SATRT");
		  builder.append(" FROM ").append(PortalAssets.class.getSimpleName());
			LOGGER.info("START::findAllPortalAssets()");
			query = getEntityManager().createQuery(builder.toString(), PortalAssets.class);
			portalassets = query.getResultList();
			LOGGER.info("END::findAllPortalAssets()");
		} catch (Exception e) {
			LOGGER.error("EXCEPTION::findAllPortalAssets() " + e);
		}  finally {
			query = null;
			builder = null;
		}
		return portalassets;
	}
	
}
