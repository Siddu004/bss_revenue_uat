package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

public class MspCpeDmdDtlPK implements Serializable {
	
	private static final long serialVersionUID = -2171433401976053545L;

	private Long dmdId;
	
	private String lmoName;
	

	public Long getDmdId() {
		return dmdId;
	}

	public void setDmdId(Long dmdId) {
		this.dmdId = dmdId;
	}

	public String getLmoName() {
		return lmoName;
	}

	public void setLmoName(String lmoName) {
		this.lmoName = lmoName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dmdId == null) ? 0 : dmdId.hashCode());
		result = prime * result + ((lmoName == null) ? 0 : lmoName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MspCpeDmdDtlPK other = (MspCpeDmdDtlPK) obj;
		if (dmdId == null) {
			if (other.dmdId != null)
				return false;
		} else if (!dmdId.equals(other.dmdId))
			return false;
		if (lmoName == null) {
			if (other.lmoName != null)
				return false;
		} else if (!lmoName.equals(other.lmoName))
			return false;
		return true;
	}
	
	

}
