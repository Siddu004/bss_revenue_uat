package com.arbiva.apfgc.tenant.dto;

import java.util.List;

public class CpeTypeMobileDTO {
	
	private String cpeType;
	
	private List<CpeModelMobileDTO> cpeModels;

	public String getCpeType() {
		return cpeType;
	}

	public void setCpeType(String cpeType) {
		this.cpeType = cpeType;
	}

	public List<CpeModelMobileDTO> getCpeModels() {
		return cpeModels;
	}

	public void setCpeModels(List<CpeModelMobileDTO> cpeModels) {
		this.cpeModels = cpeModels;
	}
	
	

}
