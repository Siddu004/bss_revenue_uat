package com.arbiva.apfgc.tenant.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="chrgrsdtl")
@IdClass(ChargeRsDetailsId.class)
public class ChargeRsDetails implements Serializable {


	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name = "rstmplcode")
	private String rstmplCode;
	
	@Id
	@Column(name = "chargecode")
	private String chargeCode;

	public String getRstmplCode() {
		return rstmplCode;
	}

	public void setRstmplCode(String rstmplCode) {
		this.rstmplCode = rstmplCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	
	
	

}
