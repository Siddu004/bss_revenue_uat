package com.arbiva.apfgc.tenant.pg.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author srinivasa
 * 
 */
@MappedSuperclass
public abstract class ComBase implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "STATUS", columnDefinition="tinyint(1)")
	protected int status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDON")
	protected Calendar createdOn;

	@Column(name = "CREATEDBY")
	protected String createdBy;

	@Column(name = "CREATEDIPADDR")
	protected String createdIPAddr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIEDON")
	protected Calendar modifiedOn;

	@Column(name = "MODIFIEDBY")
	protected String modifiedBy;

	@Column(name = "MODIFIEDIPADDR")
	protected String modifiedIPAddr;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setModifiedOn(Calendar modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Calendar getModifiedOn() {
		return modifiedOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedIPAddr() {
		return createdIPAddr;
	}

	public void setCreatedIPAddr(String createdIPAddr) {
		this.createdIPAddr = createdIPAddr;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedIPAddr() {
		return modifiedIPAddr;
	}

	public void setModifiedIPAddr(String modifiedIPAddr) {
		this.modifiedIPAddr = modifiedIPAddr;
	}
	
	@Transient
	@JsonIgnore
	public boolean isNew() {
		return (this.createdOn == null);
	}

}
