package com.arbiva.apfgc.tenant.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.arbiva.apfgc.tenant.dao.RsAgrmntsDao;
import com.arbiva.apfgc.tenant.model.RsAgrmnts;

@Repository
public class RsAgrmntsDaoImpl implements RsAgrmntsDao {

	private static final Logger LOGGER = Logger.getLogger(RsAgrmntsDaoImpl.class);
	
	

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	private EntityManager getEntityManager() {
		return em;
	}

	@Override
	public RsAgrmnts save(RsAgrmnts rsAgreement) {
		try{
			rsAgreement = getEntityManager().merge(rsAgreement);
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
		} finally {
			
		}
		return rsAgreement;
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public RsAgrmnts findByPartnerList(String partnerCodesList,String prodCode, String tenatCode, String aggrFromDate) {
		RsAgrmnts rsAgrmnts = null;
		/*StringBuilder builder = new StringBuilder(" FROM ").append(RsAgrmnts.class.getSimpleName()).append(" where partnerlist =:partnerlist");
		builder.append(" and prodcode =:prodcode");
		builder.append(" and tenantcode =:tenantcode");
		builder.append(" and agrfdate =:agrfdate");*/
		Query query=null;
		StringBuilder builder = new StringBuilder(" SELECT * FROM rsagrmnts where partnerlist = :partnerlist and "
				+ " prodcode = :prodcode and "
				+ " tenantcode = :tenantcode and "
				+ " agrfdate = :agrfdate");
		try{
			 query = getEntityManager().createNativeQuery(builder.toString(), RsAgrmnts.class);
			query.setParameter("partnerlist", partnerCodesList);
			query.setParameter("prodcode", prodCode);
			query.setParameter("tenantcode", tenatCode);
			query.setParameter("agrfdate", aggrFromDate);
			List<RsAgrmnts> rsAgrmntsList = query.getResultList();
			if(!rsAgrmntsList.isEmpty())
				rsAgrmnts =  rsAgrmntsList.get(0);
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
			//rsAgrmnts = new RsAgrmnts();
		} finally {
			query = null;
			builder = null;
		}
		return rsAgrmnts;
	}
	
	@SuppressWarnings({ })
	public Integer findByPartnerList1(String partnerCodesList,String prodCode, String tenatCode, String aggrFromDate) {
		Integer count = null;
		Query query=null;
		StringBuilder builder = new StringBuilder(" SELECT * FROM rsagrmnts where partnerlist = '"+partnerCodesList+"' and "
				+ " prodcode = '"+prodCode+"' and "
				+ " tenantcode = '"+tenatCode+"' and "
				+ " agrfdate = '"+aggrFromDate+"'");
		try{
			query = getEntityManager().createNativeQuery(builder.toString());
			Integer result = Integer.parseInt(query.getSingleResult().toString());
			count = result == 0 ? null : result;
		}catch(Exception ex){
			LOGGER.error(ex.getMessage());
		} finally {
			query = null;
			builder = null;
		}
		return count;
	}

}
