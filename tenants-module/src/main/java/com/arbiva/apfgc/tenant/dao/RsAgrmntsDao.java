package com.arbiva.apfgc.tenant.dao;

import com.arbiva.apfgc.tenant.model.RsAgrmnts;

public interface RsAgrmntsDao {

	RsAgrmnts save(RsAgrmnts reAgreement);

	RsAgrmnts findByPartnerList(String partnersList, String prodCode, String tenantCode, String aggFromDate);

}
