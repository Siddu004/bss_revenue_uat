/**
 * 
 */
package com.arbiva.apfgc.tenant.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.arbiva.apfgc.tenant.daoImpl.GlCodeDaoImpl;
import com.arbiva.apfgc.tenant.model.GlCode;

/**
 * @author Arbiva
 *
 */
@Component("glcodeService")
@Transactional
public class GlCodeServiceImpl {
	
	@Autowired
	GlCodeDaoImpl glCodeDao;
	
	public List<GlCode> findAllGlCode() {
		List<GlCode> glCode = new ArrayList<GlCode>();
		glCode = glCodeDao.findAllGlCodes();
		return glCode;
	}
}
