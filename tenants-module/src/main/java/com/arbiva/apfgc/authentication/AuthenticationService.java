package com.arbiva.apfgc.authentication;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class AuthenticationService {
	private static final Logger logger = Logger.getLogger(AuthenticationService.class);
	
	public boolean authenticate(String authCredentials) {

		if (null == authCredentials)
			return false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic"+ " ", "");
		String usernameAndPassword = null;
		try {
			logger.info("AuthenticationService :: authenticate  :: START ");
			byte[] decodedBytes = Base64.decodeBase64(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
			logger.info("AuthenticationService :: authenticate  :: END ");
		} catch (IOException e) {
			logger.error("AuthenticationService :: authenticate " + e);
			e.printStackTrace();
		}
		finally {
			
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		boolean authenticationStatus = "admin".equals(username) && "admin".equals(password);
		
		return authenticationStatus;
	}
		
}