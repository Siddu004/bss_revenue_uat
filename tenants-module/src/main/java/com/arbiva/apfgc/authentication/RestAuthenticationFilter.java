package com.arbiva.apfgc.authentication;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


public class RestAuthenticationFilter implements javax.servlet.Filter {
	private static final Logger logger = Logger.getLogger(RestAuthenticationFilter.class);
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter) throws IOException, ServletException {
		try {
			logger.info("RestAuthenticationFilter :: doFilter  :: START ");
			if (request instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String authCredentials = httpServletRequest.getHeader(AUTHENTICATION_HEADER);
			
			//boolean flag = ((HttpServletRequest) request).getRequestURI().contains("bdserverresponse");
			boolean flag = ((HttpServletRequest) request).getRequestURI().contains("creditLmoWallet");
			
			AuthenticationService authenticationService = new AuthenticationService();
			boolean authenticationStatus = authenticationService.authenticate(authCredentials);
			if (authenticationStatus || flag) {
				filter.doFilter(request, response);
			} else {
				if (response instanceof HttpServletResponse) {
					HttpServletResponse httpServletResponse = (HttpServletResponse) response;
					httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}
		}
			logger.info("RestAuthenticationFilter :: doFilter  :: END ");
		}
		catch(Exception e){
			logger.error("RestAuthenticationFilter :: doFilter " + e);
		}finally {
			
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
