package com.arbiva.apfgc.tanent.util;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TaxPropertiesFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@Value("${tax.serviceTax}")
	private String serviceTax;
	
	@Value("${tax.swatchTax}")
	private String swatchTax;
	
	@Value("${tax.kisanTax}")
	private String kisanTax;
	
	

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getSwatchTax() {
		return swatchTax;
	}

	public void setSwatchTax(String swatchTax) {
		this.swatchTax = swatchTax;
	}

	public String getKisanTax() {
		return kisanTax;
	}

	public void setKisanTax(String kisanTax) {
		this.kisanTax = kisanTax;
	}
	
	

}
