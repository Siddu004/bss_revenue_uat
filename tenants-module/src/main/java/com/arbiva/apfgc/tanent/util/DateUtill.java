package com.arbiva.apfgc.tanent.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;


public class DateUtill {
	private static final Logger logger = Logger.getLogger(DateUtill.class);
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	private static final SimpleDateFormat DATE_FORMAT_TIME_ZONE = new SimpleDateFormat("MM/dd/yyyy HH:mm:ssz");
	private static final SimpleDateFormat DATE_FORMAT_TIME = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	private static final SimpleDateFormat DATE_FORMAT_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat DATE_FORMAT_DDMMYYYY = new SimpleDateFormat("ddMMyyyy");
	private static final SimpleDateFormat DATE_FORMAT_YYYYMM = new SimpleDateFormat("yyyyMM");
	private static final SimpleDateFormat DATE_FORMAT_STRING = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat DATE_FORMAT_YYYY_MM_dd = new SimpleDateFormat("yyyy-MMM-dd");

	public static Date getNextDateFromCurrentDate() throws ParseException {
		String tomorrowDate = null;
		Calendar c = Calendar.getInstance();
		try {
			logger.info("DateUtill :: getNextDateFromCurrentDate :: START");
			c.add(Calendar.DATE, 1); // number of days to add
			tomorrowDate = DATE_FORMAT.format(c.getTime());
			logger.info("DateUtill :: getNextDateFromCurrentDate :: END");
		} catch(Exception e) {
			logger.error("DateUtill :: getNextDateFromCurrentDate" +e);
		} finally {
			c = null;
		}
		return stringtoDate(tomorrowDate);
	}

	public static Calendar stringtoCalender(String datestr) throws ParseException {
		Calendar cal = Calendar.getInstance();
		try {
			logger.info("DateUtill :: stringtoCalender :: START");
			cal.setTime(DATE_FORMAT_TIME.parse(datestr));
			logger.info("DateUtill :: stringtoCalender :: END");
		} catch(Exception e)
		{
			logger.error("DateUtill :: stringtoCalender" +e);
		} finally {
			
		}
		return cal;
	}

	public static Calendar stringtoCalenderTime(String datestr) throws ParseException {
		Calendar cal = Calendar.getInstance();
		try {
			logger.info("DateUtill :: stringtoCalenderTime :: START");
			cal.setTime(DATE_FORMAT_TIME.parse(datestr));
			logger.info("DateUtill :: stringtoCalenderTime :: END");
		} catch (ParseException e) {
			logger.error("DateUtill :: stringtoCalenderTime " +e);
			e.printStackTrace();
		}
		finally {}

		return cal;
	}

	public static Date stringtoDate(String datestr) throws ParseException {

		return DATE_FORMAT.parse(datestr);

	}
	
	public static Date convertStringToDate(String dateInString, String dateFormat) {
		Date date = null;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		try {
			date = format.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	public static String stringtoString(String datestr) throws ParseException {
		String value = null;
		Date d1 = null;
		try {
			logger.info("DateUtill :: stringtoString :: START");
			d1 =  DATE_FORMAT.parse(datestr);
			value = DATE_FORMAT_STRING.format(d1);
			logger.info("DateUtill :: stringtoString :: END");
		} catch(Exception e)
		{
			logger.error("DateUtill :: stringtoString " +e);	
		} finally {
			d1 = null;
		}
		return value;

	}

	public static String dateToString(Date date) {
		return DATE_FORMAT.format(date);
	}

	public static Calendar dateTOCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		try {
			logger.info("DateUtill :: dateTOCalendar :: START");
			cal.setTime(date);
		}catch(Exception e){
			logger.error("DateUtill :: dateTOCalendar :: END");
		} finally {
			
		}
		return cal;
	}

	public static String calendarToString(Calendar calendar) {
		Date date = calendar.getTime();
		return DATE_FORMAT.format(date);
	}

	public static String calendarTimeToString(Calendar calendar) {
		Date date = calendar.getTime();
		return DATE_FORMAT_TIME_ZONE.format(date);
	}
	
	public static String calendarTimeToYYYY_MMM_dd(Calendar calendar) {
		Date date = calendar.getTime();
		return DATE_FORMAT_YYYY_MM_dd.format(date);
	}

	public static String getDateInYYYYMMDD(Date prodDate) {
		Date date = Calendar.getInstance().getTime();
		if (prodDate.after(date))
			return DATE_FORMAT_YYYYMMDD.format(prodDate);
		else
			return DATE_FORMAT_YYYYMMDD.format(date);
	}

	public static Date calendarToDate(Calendar calendar) {

		return calendar.getTime();
	}

	public static String millisToString(Long millis) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(millis);
		return calendarToString(cal);
	}

	public static String dateToStringTimeZone(Date date) {
		return DATE_FORMAT_TIME_ZONE.format(date);
	}

	public static String dateToStringTime(Date date) {
		return DATE_FORMAT_TIME.format(date);
	}

	public static int hoursDifference(Date date1, Date date2) {
		long diff = date1.getTime() - date2.getTime();
		return (int) diff / (60 * 60 * 1000);

	}

	public static int minutesDifference(Date date1, Date date2) {
		long diff = date1.getTime() - date2.getTime();
		int MILLI_TO_HOUR = (int) diff;
		return (int) (MILLI_TO_HOUR / (60 * 1000) % 60);
	}

	public static int hoursToMins(Date date1, Date date2) {
		long minute = Math.abs((date1.getTime() - date2.getTime()) / 60000);
		return (int) minute;
	}

	public static int daysDifference(Date date1, Date date2) {
		long diff = date1.getTime() - date2.getTime();
		int MILLI_TO_HOUR = (int) diff;
		return (int) (MILLI_TO_HOUR / (24 * 60 * 60 * 1000));
	}

	public static long gettimeDifference(Calendar cal) {
		
		Calendar currentCal = Calendar.getInstance();
		long milliSec1 = cal.getTimeInMillis();
		long milliSec2 = currentCal.getTimeInMillis();
		long timeDifInMilliSec = milliSec2 - milliSec1;
		long timeDifInMins = timeDifInMilliSec / (60 * 1000);
		return timeDifInMins;

	}



	public static String getCurrentDateInYYYYMMDD() {
		Calendar currentCal = Calendar.getInstance();
		Date date = currentCal.getTime();
		return DATE_FORMAT_STRING.format(date);
	}

	public static String getDateInDDMMYYYY(Date time) {
			return DATE_FORMAT_DDMMYYYY.format(time);
	}

	public static String getDateInDDMMYYYY(String date) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(DATE_FORMAT.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return DATE_FORMAT_DDMMYYYY.format(cal.getTime());
	}

	public static String getDateInyyyyMM(String date) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(DATE_FORMAT.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return DATE_FORMAT_YYYYMM.format(cal.getTime());
	}
}
