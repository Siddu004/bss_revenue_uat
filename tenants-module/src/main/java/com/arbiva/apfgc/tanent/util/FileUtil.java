package com.arbiva.apfgc.tanent.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;


public class FileUtil {
	
	private static final Logger logger = Logger.getLogger(FileUtil.class);
	
	public static String saveImage(String fileName, String basepath, byte[] bs, String loginId, String docType) throws IOException{
		String path = null;
		byte[] bytes = null;
		File directory = null;
		File serverFile = null;
		BufferedOutputStream stream = null;
		try {
			logger.info("FileUtil :: saveImage :: START");
			bytes = bs;
			// Creating the directory to store file
			// String rootPath = System.getProperty("user.home");
			directory = new File(basepath+ File.separator + loginId + File.separator + docType);
			if (!directory.exists())
				directory.mkdirs();

			// Create the file on server
			 path = new String(directory.getAbsolutePath()+ File.separator + fileName);
			serverFile = new File(path);
			if(bs==null)
			{
				serverFile.createNewFile();
				logger.error("FileUtil::saveImage() ");
			}
			stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
			logger.info("FileUtil :: saveImage :: END");
		} catch(Exception e) {
			logger.error("FileUtil::saveImage() " + e);
			e.printStackTrace();
		} finally {
			bytes = null;
			directory = null;
			serverFile = null;
		}
		logger.info("The Documents Stored in to folder.");
		return path;
	}

}
