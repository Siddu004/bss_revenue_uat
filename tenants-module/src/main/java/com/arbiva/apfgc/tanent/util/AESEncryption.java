package com.arbiva.apfgc.tanent.util;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arbiva.apfgc.tenant.dto.CreditLmoWallet;

@RestController
public class AESEncryption {
	
	private static final String KEY1 = "APSFL_ANDHRABANK" ;
	private static final String KEY2 = "ANDHRABANK_APSFL" ;

	public static String encrypt(String key1, String key2, String plainText) {
		String val = null;
        try {
            IvParameterSpec iv = new IvParameterSpec(key2.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key1.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            val=  Base64.encodeBase64String( cipher.doFinal(plainText.getBytes()));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return val;
    }

    public static String decrypt(String key, String initVector, String encrypted) {
    	String val = null;
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            val = new String(cipher.doFinal(Base64.decodeBase64(encrypted)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return val;
    }
    
    public static boolean validateEncryptedValue(CreditLmoWallet creditLmoWallet){
    	
          String concatVal = creditLmoWallet.getLmoCode()+creditLmoWallet.getAmountPaid();
          String decryptVal = decrypt(KEY1,KEY2,creditLmoWallet.getEncryptVal());
          if(decryptVal != null && concatVal.trim().equals(decryptVal.trim()))
        	  return true;
          else
        	  return false;
    }

    public static void main(String[] args) {
        
        String enc =  encrypt(KEY1, KEY2, "LMO1100");
        

        System.out.println("Encript "+enc);
        
        String desc = decrypt(KEY1, KEY2, enc);
        
        System.out.println("Decript "+desc);
    }
    
    
    @RequestMapping(value="/encryptValue")
    public static String encryptValue(@RequestParam("lmocode") String lmocode, @RequestParam("amount") String amount) {
    	String val = null;
        try {
        	val = encrypt(KEY1, KEY2, lmocode.concat(amount));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return val;
    }
}
