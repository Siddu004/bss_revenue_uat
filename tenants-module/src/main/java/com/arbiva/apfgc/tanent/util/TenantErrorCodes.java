package com.arbiva.apfgc.tanent.util;

public enum TenantErrorCodes {

	
	TENT001(500, "Internal Server Error"), 
	TENT002(400, "Invalid empty parameter"),
	TENT003(400, "Entity is already exists"),
	TENT004(400, "Entity not found"),
	TENT005(400, "Invalid Code"),
	TENT006(400, "Error occured while executing database query");
	

	private final int code;
	private final String description;

	private TenantErrorCodes(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
}
