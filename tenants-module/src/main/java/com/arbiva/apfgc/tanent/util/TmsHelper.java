/**
 * 
 */
package com.arbiva.apfgc.tanent.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.util.Base64;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apfgc.tenant.dto.UmsHelperDTO;
import com.arbiva.apfgc.tenant.dto.UserMenuItemsRestDTO;
import com.arbiva.apfgc.tenant.model.Tenant;

/**
 * @author Arbiva
 *
 */
@Service
public class TmsHelper {
	  
	@Value("${UMS-USERNAME}")
	private String umsUserName;

	@Value("${UMS-PWD}")
	private String umsPwd;
	
	@Value("${MAIL-URL}")
	private String mailUrl;
	
	//static final String APFGC_USER = "apfgc_user";
	private static final Logger logger = Logger.getLogger(TmsHelper.class);
	
	/*public static String checkCookies(HttpServletRequest request){
		
		Cookie[] cookies = request.getCookies();
		String signedTokenString = null;
		if (cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(APFGC_USER)) {
					signedTokenString = cookie.getValue();
				}
				logger.info("Cookies  :::", cookie);
			}
		}
		
		
		return signedTokenString;
	}*/
	
	public static String getUserNameFromCookies(HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();
		String userName = null;
		try {
			logger.info("TmsHelper :: getUserNameFromCookies :: START");
			if (cookies != null && cookies.length > 0) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("user")) {
						userName = cookie.getValue();
					}
					logger.info("Cookies  :::"+cookie);
				}
			}
			logger.info("TmsHelper :: getUserNameFromCookies :: END");
		} catch(Exception e){
			logger.error("TmsHelper :: getUserNameFromCookies " + e);
		} finally {
			cookies = null;
		}
		return userName;
	}
	
//	public static Model getMenuItemsDynamicallyByUserRole(String userName, Model uiModel){
//		
//		RestTemplate restTemplate = new RestTemplate();
//
//	//	String menuItemurl = "http://192.168.1.128:8095/apfgc/rest/UMS/getHeader?userName="+userName;
//		String menuItemurl = "http://localhost:8080/apfgc/rest/UMS/getHeader?userName="+userName;
//		UserMenuItemsRestDTO userMenuObj = restTemplate.getForObject(menuItemurl, UserMenuItemsRestDTO.class);
//		
//		uiModel.addAttribute("domain",userMenuObj.getUsersDTOobj().getDomain());
//		uiModel.addAttribute("tenantcode",userMenuObj.getUsersDTOobj().getTenantCode());
//		uiModel.addAttribute("tenantname",userMenuObj.getUsersDTOobj().getTenantName());
//		uiModel.addAttribute("moduleNameMap",userMenuObj.getMenuItemList());
//		uiModel.addAttribute("userName",userMenuObj.getUsersDTOobj().getUserName());
//		
//		return uiModel;
//	}
	
	public static Model getMenuItemsDynamicallyByUserRole(String userName, Model uiModel, String umsURL){
		
		RestTemplate restTemplate = new RestTemplate();
		String menuItemurl = null;
		UserMenuItemsRestDTO userMenuObj = null;
		try{
			logger.info("TmsHelper :: getMenuItemsDynamicallyByUserRole :: START");
			menuItemurl = umsURL+"rest/UMS/getHeader?userName="+userName;
			userMenuObj = restTemplate.getForObject(menuItemurl, UserMenuItemsRestDTO.class);
			
			uiModel.addAttribute("domain",userMenuObj.getUsersDTOobj().getDomain());
			uiModel.addAttribute("tenantcode",userMenuObj.getUsersDTOobj().getTenantCode());
			uiModel.addAttribute("tenantname",userMenuObj.getUsersDTOobj().getTenantName());
			uiModel.addAttribute("moduleNameMap",userMenuObj.getMenuItemList());
			uiModel.addAttribute("userName",userMenuObj.getUsersDTOobj().getUserName());
			uiModel.addAttribute("loginID",userMenuObj.getUsersDTOobj().getLoginID());
			logger.info("TmsHelper :: getMenuItemsDynamicallyByUserRole :: END");
		} catch(Exception e){
			logger.error("TmsHelper :: getMenuItemsDynamicallyByUserRole " +e);
		} finally {
			restTemplate = null;
			menuItemurl = null;
			userMenuObj = null;
		}
		return uiModel;
	}
	
	@SuppressWarnings("unchecked")
	public String createUser(Tenant tenantVO, String umsURL, String loginId){
		
		String response = "";
		String server = null;
		JSONObject obj = new JSONObject();
		String json = null;
		UmsHelperDTO umsHelperDTO = null;
		HttpStatus status = null;
		try 
		{	logger.info("TmsHelper :: createUser :: START");
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			server = umsURL + "saveUser";
			obj.put("userName", tenantVO.getName());
			obj.put("loginID", tenantVO.getTenantCode());
			obj.put("tenantCode", tenantVO.getTenantCode());
			obj.put("rmUserID", loginId);
			obj.put("emailID1", tenantVO.getEmailId1());
			obj.put("roleName", tenantVO.getTenantTypeLov()+"ADMIN");
			obj.put("tenantType", tenantVO.getTenantTypeLov());
			obj.put("pwdResetFlag", "1");
			obj.put("status", "1");
			obj.put("date", dateFormat.format(date));
			obj.put("url", mailUrl);
			json = obj.toJSONString();
			HttpEntity<String> requestEntity = getHttpEntity(umsUserName, umsPwd, json);
			RestTemplate rest = new RestTemplate();
			ResponseEntity<UmsHelperDTO> responseEntity = rest.exchange(server, HttpMethod.POST, requestEntity, UmsHelperDTO.class);
			umsHelperDTO = responseEntity.getBody();
			response = umsHelperDTO.getMessage();
			status = responseEntity.getStatusCode();
			//System.out.println(status);
			logger.info("TmsHelper :: createUser :: END");
		} catch (Exception e) {
			logger.error("TmsHelper :: createUser" +e);
			e.printStackTrace();
		} finally {
			server = null;
			obj = null;
			json = null;
			umsHelperDTO = null;
			status = null;
		}
		return response;
	}
	
	public HttpEntity<String> getHttpEntity(String username, String pwd, String json) {
		String plainCreds = null;
		byte[] plainCredsBytes = null;
		byte[] base64CredsBytes = null;
		String base64Creds = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			logger.info("TmsHelper :: getHttpEntity :: START");
			plainCreds = username + ":" + pwd;
			plainCredsBytes = plainCreds.getBytes();
			base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			base64Creds = new String(base64CredsBytes);
			headers.add("Authorization", "Basic " + base64Creds);
			headers.add("Content-Type", "application/json");
			logger.info("TmsHelper :: getHttpEntity :: START");
		} catch(Exception e) {
			logger.error("TmsHelper :: getHttpEntity " +e);
		}finally {
			plainCreds = null;
			plainCredsBytes = null;
			base64CredsBytes = null;
			base64Creds = null;
			base64Creds = null;
		}
		
		
		return new HttpEntity<String>(json, headers);
	}
}
