package com.arbiva.apfgc.tanent.util;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class SMSHelper {
	private static final Logger logger = Logger.getLogger(SMSHelper.class);
	RestTemplate restTemplate;
	IpAddressValues ipAddressValues;
	String mobileNo;
	String msg;
	
	public SMSHelper(IpAddressValues ipAddressValues,RestTemplate restTemplate,String mobileNo,String msg)
	{
		this.ipAddressValues=ipAddressValues;
		this.restTemplate=restTemplate;
		this.mobileNo=mobileNo;
		this.msg=msg;
	}
	
	public void sendSMS(String mobileNo,String msg)
    {
    		try{//logger.info(" in thread class");
	    		HttpEntity<String> httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
	    		String url = ipAddressValues.getComURL() + "sendPgSMS?mobileNo="+mobileNo+"&msg="+msg;
				ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
				response.getBody();
				logger.info("Response form SMS ::  "+response.getBody()); 
    		}catch(Exception e){
    			e.printStackTrace();
    		}
	}
}
