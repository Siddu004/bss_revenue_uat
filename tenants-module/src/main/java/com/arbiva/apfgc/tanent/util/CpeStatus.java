package com.arbiva.apfgc.tanent.util;

public enum CpeStatus {
	
	
	BLOCKED_FOR_MSP("MSP", 21), 
	BLOCKED_FOR_LMO("LMO", 31),
	ALLOCATED_FOR_MSP("MSP", 2), 
	ALLOCATED_FOR_LMO("LMO", 3);

	private String tenantType;
	private int status;
	

	CpeStatus(String name, int id) {
		this.status = id;
		this.tenantType = name;

	}

	public String getTenantType() {
		return tenantType;
	}


	public int getStatus() {
		return status;
	}

}
