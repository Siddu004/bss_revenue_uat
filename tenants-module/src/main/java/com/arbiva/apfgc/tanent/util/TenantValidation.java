package com.arbiva.apfgc.tanent.util;

import java.io.IOException;

import com.arbiva.apfgc.tenant.vo.TenantAgreementVO;
import com.arbiva.apfgc.tenant.vo.TenantLmoMspAgreementVO;
import com.arbiva.apfgc.tenant.vo.TenantVO;

public class TenantValidation {
	
	static String status = "false";
	
	public static String tenantPageValidation(TenantVO tenantVO) throws IOException {

		if ((!tenantVO.getName().isEmpty()  && !tenantVO.getTenantTypeLov().isEmpty() 
				&& !tenantVO.getTenantCode().isEmpty() 
				&& !tenantVO.getRegion().isEmpty()
				
				//&& !tenantVO.getLicenceId().isEmpty()
				&& !tenantVO.getLicenserefno().isEmpty()
				//&& !tenantVO.getDoclov().isEmpty()
				&& !tenantVO.getEffectiveFrom().isEmpty()
				&& !tenantVO.getEffectiveTO().isEmpty()
				
				&& !tenantVO.getDocUniqueId1().isEmpty()
				//&& !tenantVO.getDoclov1().isEmpty()
				//&& !tenantVO.getIdProof().isEmpty()
				&& !tenantVO.getEffectiveFrom1().isEmpty()
				&& !tenantVO.getEffectiveTO1().isEmpty()
				
				&& !tenantVO.getDocUniqueId2().isEmpty()
				//&& !tenantVO.getDoclov2().isEmpty()
				//&& !tenantVO.getAddressProof().isEmpty()
				&& !tenantVO.getEffectiveFrom2().isEmpty()
				&& !tenantVO.getEffectiveTO2().isEmpty()
				
				&& !tenantVO.getAccountNo().trim().equals("")
				&& !tenantVO.getIfscCode().isEmpty()
				&& !tenantVO.getAcctTypelov().isEmpty()
				&& !tenantVO.getBranchName().isEmpty()
				&& !tenantVO.getBankNamelov().isEmpty()
				
				
				
				
				&& tenantVO.getName().length()<=100
				&& tenantVO.getTenantCode().length()<=30
				/*&& tenantVO.getPanNo().length()==10
				&& tenantVO.getAadharCardNo().length()==12
				&& tenantVO.getTanNo().length()<=20
				&& tenantVO.getTinNo().length()<=20
				&& tenantVO.getGstNo().length()<=20
				&& tenantVO.getVatNo().length()<=20
				&& tenantVO.getAddress1().length()<=225
				&& tenantVO.getLocality().length()<=100
				&& tenantVO.getArea().length()<=50
				&& tenantVO.getCity().length()<=20
				&& tenantVO.getStateName().length()<=30
				&& tenantVO.getPincode().length()==6
				&& tenantVO.getStdcode().length()<=5
				&& tenantVO.getLandline1().length()<=10
				&& tenantVO.getEmailId1().length()<=100
				&& tenantVO.getFax1().length()<=10
				&& tenantVO.getPocName().length()<=50
				&& tenantVO.getPocMobileNo1().length()<=10
				&& tenantVO.getLocalOfficeAddress1().length()<=225
				&& tenantVO.getLocalOfficeLocality().length()<=150
				&& tenantVO.getLocalOfficeArea().length()<=50
				&& tenantVO.getLocalOfficeCity().length()<=20
				&& tenantVO.getLocalOfficeStateName().length()<=30
				&& tenantVO.getLocalOfficePincode().length()==6
				&& tenantVO.getLocalOfficestdcode().length()<=5
				&& tenantVO.getLocalOfficeLandline1().length()<=10
				&& tenantVO.getLocalOfficeEmailId1().length()<=100
				&& tenantVO.getLocalOfficeFax1().length()<=10
				&& tenantVO.getLocalOfficePocName().length()<=50
				&& tenantVO.getLocalOfficePocMobileNo1().length()==10*/
				)) {
			status = "true";
			return status;

		} else {
			return status;
		}

	}
	
	public static String lmoAgreementPageValidation(TenantAgreementVO tenantAgreementVO) throws IOException {

		if ((!tenantAgreementVO.getTenantCode().trim().equals("") 
				&& tenantAgreementVO.getCoreServiceMulti().length > 0
				&& !tenantAgreementVO.getAgrFDate().equals("")  
				&& !tenantAgreementVO.getAgreementTo().equals("")
				&& !tenantAgreementVO.getAgreementDate().equals("") 
				&& !tenantAgreementVO.getAgreementCopy().equals("")  
				&& !tenantAgreementVO.getWalletAmount().equals("")
				
				)) {
			status = "true";
			return status;

		} else {
			return status;
		}

	}
	
	public static String lmomspAgreementPageValidation(TenantLmoMspAgreementVO tenantLmoMspAgreementVO) throws IOException {

		if ((/*!tenantLmoMspAgreementVO.getLmoCode().equals("") 
				//&& !tenantLmoMspAgreementVO.getRegion().trim().equals("")
				&&*/ 
				 !tenantLmoMspAgreementVO.getAgreementFrom().equals("")
				&& !tenantLmoMspAgreementVO.getAgreementTo().equals("")  
				&& !tenantLmoMspAgreementVO.getAgreementDate().equals("")
				&& !tenantLmoMspAgreementVO.getAgreementCopy().equals("")  
				&& !tenantLmoMspAgreementVO.getLmoWalletAmount().equals("")
				
				)) {
			status = "true";
			return status;

		} else {
			return status;
		}

	}

}
