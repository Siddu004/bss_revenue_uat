package com.arbiva.apsfl;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arbiva.apsfl.util.ApsflHelper;
import com.arbiva.apsfl.util.IpAddressValues;

@RestController
public class SchedulerController {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	IpAddressValues ipAddressValues;

	private static final Logger LOGGER = Logger.getLogger(SchedulerController.class);

	@Scheduled(cron = "${cafWiseDemandJob}") // Trigger run on every day 6AM and
												// 6PM
	public void cafWiseDemand() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getApsflUserName(), ipAddressValues.getApsflPwd());
			String url = ipAddressValues.getApsflURL() + "cafWiseDemandReport";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("Caf Wise Demand Report ::  " + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${districtWiseDemandJob}") // Trigger run on every day
													// 6AM and 6PM
	public void districtWiseDemand() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getApsflUserName(), ipAddressValues.getApsflPwd());
			String url = ipAddressValues.getApsflURL() + "districtWiseDemandReport";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("District Wise Demand  Report ::  " + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${mSOWiseDemandJob}") // Trigger run on every day 6AM and
												// 6PM
	public void mSOWiseDemandReport() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getApsflUserName(), ipAddressValues.getApsflPwd());
			String url = ipAddressValues.getApsflURL() + "mSOWiseDemandReport";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("MSO Wise Demand Report ::  " + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${paymentJob}") // Trigger run on every 10 Minutes.
	public void mothlyPaymentStoreProcedure() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "mothlyPaymentStoreProcedure";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("Mothly Payment StoreProcedure ::  " + response);
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::mothlyPaymentStoreProcedure(): " + e);
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${provJob}") // Trigger run on every 1 Minute.
	public void processHSIMultiCastPackages() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "processHSIMultiCastPackages";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHSIMultiCastPackages ::  " + response);
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::processHSIMultiCastPackages(): " + e);
			e.printStackTrace();
		}
	}

	// mahesh-16-02-17 for AAA Server
	@Scheduled(cron = "${provJob}") // Trigger run on every 1 Minute
	public void processAAAServerService() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "processAAAServerService";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processAAAServerService :: " + response);
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::processAAAServerService(): " + e);
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${provJob}") // Trigger run on every 1 Minute.
	public void processIPTVCorpusServicePacks() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "processIPTVCorpusServicePacks";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processIPTVCorpusServicePacks ::  " + response);
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::processIPTVCorpusServicePacks(): " + e);
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${provJob}") // Trigger run on every 1 Minute.
	public void postProvisioningActivities() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "postProvisioningActivities";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("postProvisioningActivities ::  " + response);
		} catch (Exception e) {
			LOGGER.error("Exception occurred during SchedulerJobController::postProvisioningActivities(): " + e);
			e.printStackTrace();
		}
	}

	/*
	 * @Scheduled(cron="${provJob}") // Trigger run on every 1 Minute. public
	 * void suspendIPTVCorpusServicePacks() { HttpEntity<String> httpEntity =
	 * null; ResponseEntity<String> response = null; try { httpEntity =
	 * ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(),
	 * ipAddressValues.getComPwd()); String url =
	 * ipAddressValues.getComURL()+"suspendIPTVCorpusServicePacks"; response =
	 * restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class);
	 * LOGGER.info("Mothly Payment StoreProcedure ::  "+response); }
	 * catch(Exception e) { LOGGER.error(
	 * "Exception occurred during SchedulerJobController::processIPTVCorpusServicePacks(): "
	 * + e); e.printStackTrace(); } }
	 * 
	 * @Scheduled(cron="${provJob}") // Trigger run on every 1 Minute. public
	 * void resumeIPTVCorpusServicePacks() { HttpEntity<String> httpEntity =
	 * null; ResponseEntity<String> response = null; try { httpEntity =
	 * ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(),
	 * ipAddressValues.getComPwd()); String url =
	 * ipAddressValues.getComURL()+"resumeIPTVCorpusServicePacks"; response =
	 * restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class);
	 * LOGGER.info("Mothly Payment StoreProcedure ::  "+response); }
	 * catch(Exception e) { LOGGER.error(
	 * "Exception occurred during SchedulerJobController::processIPTVCorpusServicePacks(): "
	 * + e); e.printStackTrace(); } }
	 * 
	 * @Scheduled(cron="${provJob}") // Trigger run on every 1 Minute. public
	 * void deActivateIPTVCorpusServicePacks() { HttpEntity<String> httpEntity =
	 * null; ResponseEntity<String> response = null; try { httpEntity =
	 * ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(),
	 * ipAddressValues.getComPwd()); String url =
	 * ipAddressValues.getComURL()+"deActivateIPTVCorpusServicePacks"; response
	 * = restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class);
	 * LOGGER.info("Mothly Payment StoreProcedure ::  "+response); }
	 * catch(Exception e) { LOGGER.error(
	 * "Exception occurred during SchedulerJobController::processIPTVCorpusServicePacks(): "
	 * + e); e.printStackTrace(); } }
	 */

	@Scheduled(cron = "${districtWiseCafReportJob}") // Trigger run on every 30
														// Minute.
	public void districtWiseCafListEmail() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getApsflUserName(), ipAddressValues.getApsflPwd());
			String url = ipAddressValues.getApsflURL() + "districtWiseCafListEmail";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("District Wise Caf Report ::  " + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${fingerPrintMessageGloballyJob}") // Trigger run on
															// every 30 Minute.
	public void fingerPrintMsgGloballyJob() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getComUserName(), ipAddressValues.getComPwd());
			String url = ipAddressValues.getComURL() + "globallyMessageToFingerPrintDetailsJob";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("fingerPrintMsg Globally Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${processHsiRating1}")
	public void processHsiRating1() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating1";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 1::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Scheduled(cron = "${processHsiRating2}")
	public void processHsiRating2() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating2";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 2::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating3}")
	public void processHsiRating3() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating3";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 3::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating4}")
	public void processHsiRating4() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating4";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 4::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating5}")
	public void processHsiRating5() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating5";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 5::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating6}")
	public void processHsiRating6() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating6";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 6::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating7}")
	public void processHsiRating7() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating7";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 7::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating8}")
	public void processHsiRating8() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating8";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 8::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating9}")
	public void processHsiRating9() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating9";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 9::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "${processHsiRating10}")
	public void processHsiRating10() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processHSIRating10";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processHsiRating Job 8::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${processThrottling}")
	public void processThrottling() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processThrottling";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processThrottling Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${processThrottlingReverse}")
	public void processThrottlingReverse() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getHsiUserName(), ipAddressValues.getHsiPwd());
			String url = ipAddressValues.getHsiURL() + "processThrottlingReverse";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processThrottlingReverse Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * @Scheduled(cron="${aaaUsageReportEmail}") public void
	 * aaaUsageReportEmail() { HttpEntity<String> httpEntity = null;
	 * ResponseEntity<String> response = null; try { httpEntity =
	 * ApsflHelper.getHttpEntity(ipAddressValues.getApsflUserName(),
	 * ipAddressValues.getApsflPwd()); String url =
	 * ipAddressValues.getApsflURL()+"aaaUsageReportEmail"; response =
	 * restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class);
	 * LOGGER.info("processThrottlingReverse Job ::  "+response);
	 * }catch(Exception e) { e.printStackTrace(); } }
	 */

	@Scheduled(cron = "${processRating}")
	public void processRating() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getRatingUserName(), ipAddressValues.getRatingPwd());
			String url = ipAddressValues.getRatingURL() + "processRating";
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("processRating Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "${sendInvoiceSMSAndEmail}")
	public void sendInvoiceSMSAndEmail() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {

			Calendar c = Calendar.getInstance();

			String yearmonth = String.valueOf(c.get(Calendar.YEAR)) + "" + (c.get(Calendar.MONTH) <= 9
					? "0" + c.get(Calendar.MONTH) : String.valueOf(c.get(Calendar.MONTH)));

				if(c.get(Calendar.MONTH) == 0){
					 yearmonth=String.valueOf((c.get(Calendar.YEAR)-1))+"12";
				}
			
			  yearmonth="201710";
			
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getBeUserName(), ipAddressValues.getBePwd());
			String url = ipAddressValues.getBeURL() + "sendSmsAndEmailOnInvoices/" + yearmonth;
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("sendInvoiceSMSAndEmail Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//trigger to run on 10th of every month at 11.30 PM
	@Scheduled(cron = "${revenueSharing}")
	public void revenueSharing() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {

			Calendar now = Calendar.getInstance();
			int invyr = now.get(Calendar.YEAR);
			int invmn = now.get(Calendar.MONTH) ; // Note: zero based!

			
			
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getTmsUserName(), ipAddressValues.getTmsPwd());
			String url = ipAddressValues.getTmsURL() + "UpdateCustInvList?invfromdate="+String.valueOf(invyr)+ "&invtodate="+String.valueOf(invmn);
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("revenueSharing Job ::  " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	//trigger to be run after revenuesharing cron job
			@Scheduled(cron = "${updateoverpaid}")
			public void updateOverPaidCustomers() {
				HttpEntity<String> httpEntity = null;
				ResponseEntity<String> response = null;
				try {

					Calendar now = Calendar.getInstance();
					int invyr = now.get(Calendar.YEAR);
					int invmn = now.get(Calendar.MONTH) ; // Note: zero based!

					LOGGER.info("Jobs :: updateOverPaidCustomers() :: Start");
					httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getTmsUserName(), ipAddressValues.getTmsPwd());
					String url = ipAddressValues.getTmsURL() + "setPaidFlagForOverPaidCustomer?invfromdate="+String.valueOf(invyr)+ "&invtodate="+String.valueOf(invmn);
					response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
					LOGGER.info("Jobs :: updateOverPaidCustomers() :: end");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	
	
	
	
	
	//trigger to run on 15th of every month at 11.30 PM
	@Scheduled(cron = "${revenueSummary}")
	public void revenueSummary() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {

			Calendar now = Calendar.getInstance();
			int invyr = now.get(Calendar.YEAR);
			int invmn = now.get(Calendar.MONTH) ; // Note: zero based!

			LOGGER.info("Jobs :: revenueSummary() :: Start");
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getTmsUserName(), ipAddressValues.getTmsPwd());
			String url = ipAddressValues.getTmsURL() + "setMonthWiseShares?invfromdate="+String.valueOf(invyr)+ "&invtodate="+String.valueOf(invmn);
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("Jobs :: revenueSummary() :: end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	@Scheduled(cron = "${tenantwalletmonthbeginupdation}")
	public void updateTenantsWalletDebitAndCreditLimit() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {

			Calendar now = Calendar.getInstance();
			int invyr = now.get(Calendar.YEAR);
			int invmn = now.get(Calendar.MONTH) ; // Note: zero based!

			LOGGER.info("Jobs :: updateTenantsWalletDebitAndCreditLimit() :: Start");
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getTmsUserName(), ipAddressValues.getTmsPwd());
			String url = ipAddressValues.getTmsURL() + "updateTenantsWalletOnFirst?invfromdate="+String.valueOf(invyr)+ "&invtodate="+String.valueOf(invmn);
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("Jobs :: updateTenantsWalletDebitAndCreditLimit() :: end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	@Scheduled(cron = "${tenantwalletmonthendupdation}")
	public void tenantsWalletCreditUpdation() {
		HttpEntity<String> httpEntity = null;
		ResponseEntity<String> response = null;
		try {

			Calendar now = Calendar.getInstance();
			int invyr = now.get(Calendar.YEAR);
			int invmn = now.get(Calendar.MONTH) ; // Note: zero based!

			LOGGER.info("Jobs :: tenantwalletmonthendupdation() :: Start");
			httpEntity = ApsflHelper.getHttpEntity(ipAddressValues.getTmsUserName(), ipAddressValues.getTmsPwd());
			String url = ipAddressValues.getTmsURL() + "updateTenantsWalletOnEndOfMonth?invfromdate="+String.valueOf(invyr)+ "&invtodate="+String.valueOf(invmn);
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
			LOGGER.info("Jobs :: tenantwalletmonthendupdation() :: end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	

}
